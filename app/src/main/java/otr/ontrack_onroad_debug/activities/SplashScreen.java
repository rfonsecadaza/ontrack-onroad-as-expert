package otr.ontrack_onroad_debug.activities;

import otr.ontrack_onroad_debug.activities.R;
import otr.ontrack_onroad.managers.AuthenticationManager;
import otr.ontrack_onroad.managers.AuthenticationManagerListener;
import otr.ontrack_onroad.managers.NotificationProtocolManager;
import otr.ontrack_onroad.managers.NotificationProtocolManagerListener;
import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.parameters.PreferenceKeys;
import otr.ontrack_onroad.parameters.ServiceKeys;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

public class SplashScreen extends ActionBarActivity implements AuthenticationManagerListener, NotificationProtocolManagerListener
{
    private static int SPLASH_TIME_OUT = 3000;
    public static final String URL_BASE_CHATCARDS = "ApiMessage/getDirectory";

    private static TextView tvAuthenticationError;
    private static TextView tvVersion;
    private static Button bRetry;
    private static ProgressBar progressBar;

    private int status;
    private String error = null;

    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate(savedInstanceState);

        // getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getSupportActionBar( ).hide( );
        setContentView(R.layout.activity_splash_screen);

        tvAuthenticationError = ( TextView )findViewById(R.id.tvAuthenticationError);
        tvVersion = (TextView)findViewById(R.id.tvVersion);
        bRetry = ( Button )findViewById(R.id.bRetry);
        bRetry.setVisibility(View.INVISIBLE);

        progressBar = ( ProgressBar )findViewById( R.id.pbLoading );

        try {
            tvVersion.setText("v"+getPackageManager( ).getPackageInfo( getPackageName(), 0 ).versionName);
        } catch (NameNotFoundException e) {
            tvVersion.setText("Versión desconocida");
        }

        // Definir un context para el OnTrackSystem
        OnTrackManager.getInstance( ).setContext( this );
        // Inicializar base de datos
        OnTrackManager.getInstance( ).initializeDataBase( );

        // Esta actividad se vuelve un listener de
        // NotificationProtocolManagerListener
        // POR AHORA NO LO VOY A EJECUTAR, ESTA PARTE ESTï¿½ PENDIENTE DE CAMBIOS
        // NotificationProtocolManager.getInstance().addListener(this);

        AuthenticationManager.getInstance( ).setListener( this );

        SharedPreferences prefs = getSharedPreferences( PreferenceKeys.PREFERENCES_KEY, Context.MODE_PRIVATE );

        SharedPreferences.Editor editor = prefs.edit( );
        editor.remove( PreferenceKeys.USERNAME_KEY );
        editor.remove( PreferenceKeys.PASSWORD_KEY );
        editor.commit( );

        String token = prefs.getString( PreferenceKeys.TOKEN_KEY, null );
        if( token == null )
        {
            new Handler( ).postDelayed( new Runnable( )
            {
                @Override
                public void run( )
                {
                    Intent i;
                    i = new Intent( SplashScreen.this, Login.class );
                    startActivity( i );
                    finish();
                }
            }, SPLASH_TIME_OUT );
        }
        else
        {
            AuthenticationManager.getInstance( ).executeLoadRouteTask( AuthenticationManager.URL_BASE_DRIVER, ServiceKeys.TOKEN_KEY, token );

        }

        // Intent intent = new Intent( this, OntrackPhoneStateService.class );
        // startService( intent );
    }

    @Override
    public boolean onCreateOptionsMenu( Menu menu )
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater( ).inflate( R.menu.splash_screen, menu );
        return true;
    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item )
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId( );
        if( id == R.id.action_settings )
        {
            return true;
        }
        return super.onOptionsItemSelected( item );
    }



    public void attemptToAuthenticate( View v )
    {
        SharedPreferences prefs = getSharedPreferences( PreferenceKeys.PREFERENCES_KEY, Context.MODE_PRIVATE );
        String token = prefs.getString( PreferenceKeys.TOKEN_KEY, null );
        AuthenticationManager.getInstance( ).executeLoadRouteTask( AuthenticationManager.URL_BASE_DRIVER, ServiceKeys.TOKEN_KEY, token );

    }

    @Override
    public void onRoutePreloaded( )
    {
        if( bRetry != null )
        {
            bRetry.setVisibility( View.INVISIBLE );
        }
        if( progressBar != null )
        {
            progressBar.setVisibility( View.VISIBLE );
        }
        if( tvAuthenticationError != null )
        {
            tvAuthenticationError.setVisibility( View.INVISIBLE );
        }

    }

    @Override
    public void onRouteLoaded( )
    {
        // Intent i;
        // i = new Intent(SplashScreen.this, RouteList.class);
        // startActivity(i);

        AuthenticationManager.getInstance( ).executeRequestChatcardsTask( URL_BASE_CHATCARDS, ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance( ).getToken( ) );

    }

    @Override
    public void onRouteNotLoaded( )
    {
        // AuthenticationManager.getInstance().removeListener(this);
        Intent i;
        i = new Intent( SplashScreen.this, Login.class );
        startActivity( i );
        finish( );
    }

    @Override
    public void onAuthenticationError( String error )
    {
        tvAuthenticationError.setVisibility( View.VISIBLE );
        tvAuthenticationError.setText( error );
        bRetry.setVisibility( View.VISIBLE );
        progressBar.setVisibility( View.INVISIBLE );

    }

    @Override
    public void onProtocolExecuted( )
    {
        NotificationProtocolManager.getInstance( ).removeListener( this );

    }

    @Override
    public void onChatcardsLoaded( )
    {
        // try {
        // String savedNotificationsJSONString = OnTrackManager.getInstance()
        // .getSavedNotificationsJSONString();
        // if (savedNotificationsJSONString == null) {
        // // Si no hay notificaciones guardadas, pasar
        // // directamente a revisar notificaciones nuevas
        // NotificationProtocolManager
        // .executeRequestNewNotificationsTask();
        //
        // } else {
        // // Ejecutar el protocolo normal de notificaciones
        // NotificationProtocolManager.executeNotificationProtocolTask(
        // NotificationProtocolManager.URL_BASE_ACK_NOTIFICATIONS,
        // ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance()
        // .getToken(), ServiceKeys.NOTIFICATIONS_KEY,
        // savedNotificationsJSONString);
        // }
        // } catch (JSONException e) {
        // e.printStackTrace();
        // }

        // AuthenticationManager.getInstance().removeListener(this);
        Intent i = new Intent( this, RouteList.class );
        startActivity( i );
        finish( );

    }

    @Override
    public void onChatcardsError( String message )
    {
        // TODO Auto-generated method stub

    }
}
