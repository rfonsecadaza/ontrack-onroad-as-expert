package otr.ontrack_onroad_debug.activities;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.json.JSONException;

import otr.ontrack_onroad.fragments.HelpDialogFragment;
import otr.ontrack_onroad.tasks.ChangeStopTask;
import otr.ontrack_onroad.tasks.ChangeStopTrackablesTask;
import otr.ontrack_onroad.tasks.SendPanicMessageTask;
import otr.ontrack_onroad.adapters.EventListAdapter;
import otr.ontrack_onroad.adapters.SidebarAdapter;
import otr.ontrack_onroad.dao.ChatCard;
import otr.ontrack_onroad.dao.Event;
import otr.ontrack_onroad.dao.Message;
import otr.ontrack_onroad.fragments.AssistanceTypeDialogFragment;
import otr.ontrack_onroad.fragments.AssistanceWithNoveltiesFragment;
import otr.ontrack_onroad.fragments.CancelSkippedStopFragment;
import otr.ontrack_onroad.fragments.ChangeRouteDialogFragment;
import otr.ontrack_onroad.fragments.CloseAppDialogFragment;
import otr.ontrack_onroad.fragments.CloseSessionDialogFragment;
import otr.ontrack_onroad.fragments.EventFragment;
import otr.ontrack_onroad.fragments.ExcessiveSpeedFragment;
import otr.ontrack_onroad.fragments.MissingExtraTrackablesDialogFragment.MissingExtraTrackablesListener;
import otr.ontrack_onroad.fragments.PosterFragment;
import otr.ontrack_onroad.fragments.RecordTrackFragment;
import otr.ontrack_onroad.fragments.ReportFragment;
import otr.ontrack_onroad.fragments.SelectTrackablesForStopUpdateDialogFragment.SelectTrackablesForStopUpdateListener;
import otr.ontrack_onroad.fragments.StartRouteFragment;
import otr.ontrack_onroad.fragments.StopDetailsFragment;
import otr.ontrack_onroad.fragments.StopListFragment;
import otr.ontrack_onroad.fragments.StrangeStopFragment;
import otr.ontrack_onroad.fragments.WrongRouteDialogFragment;
import otr.ontrack_onroad.managers.CloseSessionManager;
import otr.ontrack_onroad.managers.CloseSessionManagerListener;
import otr.ontrack_onroad.managers.EventManager;
import otr.ontrack_onroad.managers.EventManagerListener;
import otr.ontrack_onroad.managers.MessageManager;
import otr.ontrack_onroad.managers.MessageManagerListener;
import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.managers.OnTrackManagerAsyncTasks;
import otr.ontrack_onroad.managers.OnTrackManagerListener;
import otr.ontrack_onroad.models.Organization;
import otr.ontrack_onroad.models.Point;
import otr.ontrack_onroad.models.Portion;
import otr.ontrack_onroad.models.Refuge;
import otr.ontrack_onroad.models.Report;
import otr.ontrack_onroad.models.Route;
import otr.ontrack_onroad.models.Stop;
import otr.ontrack_onroad.models.Trackable;
import otr.ontrack_onroad.parameters.GeneralMessages;
import otr.ontrack_onroad.parameters.Parameters;
import otr.ontrack_onroad.parameters.PreferenceKeys;
import otr.ontrack_onroad.parameters.ServiceKeys;
import otr.ontrack_onroad.parameters.WebServices;
import otr.ontrack_onroad.services.GPSService;
import otr.ontrack_onroad.services.GPSServiceListener;
import otr.ontrack_onroad.threads.SendLocationThread;
import otr.ontrack_onroad.utils.GeoPos;
import otr.ontrack_onroad.utils.JSONManager;
import otr.ontrack_onroad.utils.Timestamp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
//import com.google.android.gms.maps.CameraUpdate;
//import com.google.android.gms.maps.CameraUpdateFactory;
//import com.google.android.gms.maps.GoogleMap;
//import com.google.android.gms.maps.MapFragment;
//import com.google.android.gms.maps.Projection;
//import com.google.android.gms.maps.model.BitmapDescriptorFactory;
//import com.google.android.gms.maps.model.CameraPosition;
//import com.google.android.gms.maps.model.Circle;
//import com.google.android.gms.maps.model.CircleOptions;
//import com.google.android.gms.maps.model.LatLng;
//import com.google.android.gms.maps.model.LatLngBounds;
//import com.google.android.gms.maps.model.Marker;
//import com.google.android.gms.maps.model.MarkerOptions;
//import com.google.android.gms.maps.model.Polyline;
//import com.google.android.gms.maps.model.PolylineOptions;
import com.mapbox.mapboxsdk.annotations.Annotation;
import com.mapbox.mapboxsdk.annotations.Icon;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.annotations.PolygonOptions;
import com.mapbox.mapboxsdk.annotations.PolylineOptions;
import com.mapbox.mapboxsdk.constants.Style;
import com.mapbox.mapboxsdk.geometry.CoordinateBounds;
import com.mapbox.mapboxsdk.geometry.LatLng;
//import com.mapbox.mapboxsdk.overlay.Marker;
//import com.mapbox.mapboxsdk.overlay.PathOverlay;
import com.mapbox.mapboxsdk.views.MapView;

/******
 * CLASE PARA LA VISUALIZACIï¿½N DE LA RUTA QUE SE DEBE RECORRER, INFORMACIï¿½N
 * DE LOS PARADEROS Y SUS "TRACKABLES"
 * <p/>
 * En lo posible, sï¿½lo se manejan aspectos de interfaz grï¿½fica, pero
 * tambiï¿½n de la localizaciï¿½n del dispositivo El "back-end" de esta vista se
 * puede entender a partir de los mï¿½todos en OnTrackSystem Tambiï¿½n se
 * implementan mï¿½todos relacionados con la mensajerï¿½a instantï¿½nea (ver
 * ChatManager)
 * <p/>
 * <p/>
 * Nota: hay bastantes cosas aquï¿½ que podrï¿½an manejarse en otras clases. Por
 * cuestiones de tiempo no se han desacoplado, pero ahora que habrï¿½ un nuevo
 * desarrollador, se puede pensar en refinar todo esto
 **************/

public class MainActivity extends ActionBarActivity implements
        OnTrackManagerListener, OnItemClickListener, MessageManagerListener,
        EventManagerListener, OnClickListener, SelectTrackablesForStopUpdateListener, MissingExtraTrackablesListener, CloseSessionManagerListener, GPSServiceListener {

    // Constantes de interfaz grï¿½fica
    public static final int STOP_CIRCLE_DIAMETER = 120;

    // Constantes para los cuadrados que representan a los paraderos

    public static final long STOP_INDICATOR_ANIMATION_PERIOD = 300;

    // Constantes para el manejo de GPS
    public static final long SEND_LOCATION_PERIOD = 10000;
    public static final long SEND_LOCATION_TIMEOUT = 9000;
    public static final int MAXIMUM_TIME_BETWEEN_LOCATION_UPDATES = 5;
    private static final int MILLISECONDS_PER_SECOND = 1000;
    public static final int UPDATE_INTERVAL_IN_SECONDS = 1; // Frecuencia de
    // actualizaciï¿½n
    // en
    // segundos
    private static final long UPDATE_INTERVAL = MILLISECONDS_PER_SECOND
            * UPDATE_INTERVAL_IN_SECONDS;
    private static final int FASTEST_INTERVAL_IN_SECONDS = 1; // La frecuencia
    // mï¿½s
    // rï¿½pida de
    // actualizaciï¿½n,
    // en segundos
    private static final long FASTEST_INTERVAL = MILLISECONDS_PER_SECOND
            * FASTEST_INTERVAL_IN_SECONDS;

    private final static String HELP = "HELP";
    private final static String ALL_STUDENTS = "ALL_STUDENTS";
    private final static String REPORTS = "REPORTS";
    private final static String ANNOUNCEMENTS = "ANNOUNCEMENTS";
    private final static String FREE = "FREE";
    private final static String REFUGE = "REFUGE";
    private final static String PANIC = "PANIC";
    private final static String WAZE = "WAZE";

    // Atributos para manejo de localizaciï¿½n y mapas
//	private static GoogleMap mMap; // Chao GoogleMaps!
    private static MapView mMap;

    private ArrayList<Portion> portions;
    private PolygonOptions outterCircle;
    private PolygonOptions innerCircle;
    //	private Polyline portionLine;
    private MarkerOptions gpsMarker = null;
    private MarkerOptions gpsMarkerOriginal = null;
    private ArrayList<MarkerOptions> stopsMarkers;
    private boolean ready = false;
    private static boolean routeReady = false;
    private Timer sendLocationTimer;
    LocationRequest mLocationRequest;
    GoogleApiClient mLocationClient;
    boolean mUpdatesRequested;
    private int timeBetweenLocationUpdates = 0;
    private static boolean animationEnabled = true;

    // Elementos de interfaz grï¿½fica

    private ProgressDialog mProgressDialog;
    private static TextView tvLastStop;
    private static TextView tvNextStopTitle;
    private static TextView tvNextStop;
    private static TextView tvNextStopTime;
    private static TextView tvNextStopTimeTitle;
    private static TextView tvNotificacion;
    private static GridView gvStudentsCurrentStop;
    private static Button bStartRoute;
    private static TextView tvETA;
    private static LinearLayout llStopsContainer;
    private static RelativeLayout rlOutOfMapContainer;
    private static LinearLayout llStops;
    private static View vTouchableLayer;
    private static RelativeLayout rlComputingPortionOfRoute;
    private static TextView tvComputingPortionOfRoute;

    // Elementos de side bar
    private static DrawerLayout mDrawerLayout;
    private static ListView mDrawerList;
    private static ActionBarDrawerToggle mDrawerToggle;
    private static SidebarAdapter sidebarAdapter;

    // Elementos del sidebar derecho
    private static LinearLayout rlRightDrawer;
    private static ListView mDrawerListRight;
    private static ActionBarDrawerToggle mDrawerToggleRight;
    private static EventListAdapter eventsAdapter;

    // Botï¿½n para forzar llegada a paradero
    private static Button bAtStop;

    private static ImageView bHelp;

    private static ImageView ivAllStudentsList;

    private static ImageView ivAnnouncements;

    private static ImageView ivReports;

    private static ImageView ivNonAssignedStudents;

    private static ImageView ivPanic;

    // Opciones para escoger en el sidebar
    private static String[] sidebarTexts;

    // Constantes del poster de próximo paradero
    private static final int POSTER_DURATION = 8;

    // Constantes del poster de nuevo mensaje
    private static final int MESSAGE_DURATION = 16;

    // Indicador de visibilidad del activity
    private static boolean activityVisible;

    // Indicador de visibilidad de la lista de paraderos
    private static boolean stopListVisible;

    // Fragment del botón de inicio de ruta
    private static Fragment startRouteFragment;
    private static boolean vehicleLocated;

    // Fragment del cartel de prï¿½ximo paradero
    private static Fragment posterFragment;
    private static int posterCounter = 0;

    // Fragment de los detalles de paradero
    private static Fragment stopDetailsFragment;
    private static boolean stopDetailsShown;

    // Fragment de velocidad excesiva
    private static Fragment excessiveSpeedFragment;

    // Fragment de evento
    private static Fragment eventFragment;
    private static int eventCounter = 0;

    // Fragment de lista de paraderos
    private static Fragment stopListFragment;

    // Fragment de paraderos extraï¿½os
    private static Fragment strangeStopFragment;

    // Fragment para cancelar paradero ignorado
    private static Fragment cancelSkippedStopFragment;

    // Fragment de lista de estudiantes con novedades
    private static Fragment assistanceWithNoveltiesFragment; // Rafa

    // Voz
    TextToSpeech onTrackVoice;

    // Timer para animación de círculos
    Timer stopIndicatorAnimationTimer;

    private boolean startAutomatically;

//    private PathOverlay line;

    private ArrayList<LatLng> pointsToRefuge;


    // Nuevo service para GPS
    private GPSService myService;
    private boolean bound = false;



    private boolean prestartListSelected = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //Para textos en ecuatoriano

        Organization organization = OnTrackManager.getInstance().getUser().getOrganizationByID(OnTrackManager.getInstance().getRoutes().get(OnTrackManager.getInstance().getSelectedRoute()).getOrganizationId());
        boolean ecuatorian = organization.getCreatedBy().equals("superadmin@ontrackschool.ec");

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        getResources().getString(R.string.sidebar_my_route);

        startAutomatically = getIntent().getBooleanExtra("YAY", false);

        sidebarTexts = new String[]{"",
                getResources().getString(R.string.sidebar_my_route),
//				getResources().getString(R.string.sidebar_events),
                getResources().getString(R.string.sidebar_reports),
                getResources().getString(R.string.sidebar_messages),
                getResources().getString(R.string.sidebar_contact),
                getResources().getString(R.string.sidebar_log_out), ""};

        vehicleLocated = false;
        stopDetailsShown = false;
        stopListVisible = false;
        activityVisible = true;

        // Configuración de elementos básicos de interfaz gráfica
        Typeface tf = Typeface.createFromAsset(getAssets(),
                Parameters.LATO_FONT);
        // CONFIGURACIï¿½N DEL MAPA
        //mMap = ((MapFragment) getActivity().getFragmentManager()
        //		.findFragmentById(R.id.map)).getMap();
        //mMap.setTrafficEnabled(true);

        mMap = (MapView) findViewById(R.id.map);
        mMap.onCreate(savedInstanceState);
        mMap.setStyleUrl(Style.EMERALD);
//        mMap.setMapRotationEnabled(true);

        // CONFIGURACIï¿½N DEL SIDEBAR IZQUIERDO
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        // Sombra que aparece cuando se muestra el sidebar
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
                GravityCompat.START);

        sidebarAdapter = new SidebarAdapter(this, sidebarTexts);


        mDrawerList.setAdapter(sidebarAdapter);
        mDrawerList.setOnItemClickListener(new OnItemClickListener() {
            // Manejo de acciones del sidebar
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                mDrawerLayout.closeDrawers();
                Intent i;
                switch (position) {
                    case Parameters.OPTION_REPORTS:
                        if (OnTrackManager.getInstance().routeDidStart()) {
                            ReportFragment reportFragment = new ReportFragment();
                            reportFragment.show(getSupportFragmentManager(),
                                    Parameters.REPORT_DIALOG_TAG);
                        } else {
                            Toast.makeText(MainActivity.this,
                                    GeneralMessages.REPORT_FIRST_START_ROUTE,
                                    Toast.LENGTH_LONG).show();
                        }
                        break;

                    case Parameters.OPTION_ROUTE:
                        if (routeReady) {
//								i = new Intent(getActivity(), RouteProfile.class);
//								((MainActivity) getActivity())
//										.hideAllFragmentsForced();
//								startActivity(i);
                            ChangeRouteDialogFragment dialog = new ChangeRouteDialogFragment();

                            dialog.show(getSupportFragmentManager(),
                                    GeneralMessages.DIALOG_TITLE_CHANGE_ROUTE);
                        } else {
                            Toast.makeText(
                                    MainActivity.this,
                                    GeneralMessages.ROUTE_WAIT_UNTIL_DETAILS_LOADED,
                                    Toast.LENGTH_LONG).show();
                        }

                        break;
                    case Parameters.OPTION_CHAT:
                        i = new Intent(MainActivity.this, ChatList.class);
                        hideAllFragmentsForced();
                        startActivity(i);
                        break;
                    case Parameters.OPTION_CONTACT:
                        if (routeReady) {
//								i = new Intent(getActivity(), TrackableList.class);
//								((MainActivity) getActivity())
//										.hideAllFragmentsForced();
//								startActivity(i);
                            ((AssistanceWithNoveltiesFragment) assistanceWithNoveltiesFragment)
                                    .setType(AssistanceWithNoveltiesFragment.ASSISTANCE_FRAGMENT_TYPE_CHECK);
                            ((AssistanceWithNoveltiesFragment) assistanceWithNoveltiesFragment)
                                    .setStudentList(OnTrackManager.getInstance().getUser()
                                            .getRoute().getTrackables());
                            showAssistanceWithNovelties(OnTrackManager.getInstance().getUser()
                                    .getRoute().getAllTrackables());
                        } else {
                            Toast.makeText(
                                    MainActivity.this,
                                    GeneralMessages.ROUTE_WAIT_UNTIL_DETAILS_LOADED,
                                    Toast.LENGTH_LONG).show();
                        }
                        break;
                    case Parameters.OPTION_CLOSE_SESSION:
                        CloseSessionDialogFragment dialog = new CloseSessionDialogFragment();
                        dialog.show(getSupportFragmentManager(),
                                Parameters.CLOSE_SESSION_DIALOG_TAG);
                        break;
                    default:
                        break;

                }
            }
        });

        mDrawerToggle = new ActionBarDrawerToggle(this,
                mDrawerLayout, R.drawable.ic_launcher,
                R.string.drawer_open, R.string.drawer_close) {
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu();

            }

            public void onDrawerOpened(View drawerView) {
                invalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        getSupportActionBar()
                .setDisplayHomeAsUpEnabled(true);
        getSupportActionBar()
                .setHomeButtonEnabled(true);

        // CONFIGURACIï¿½N DEL SIDEBAR DERECHO
        rlRightDrawer = (LinearLayout) findViewById(R.id.rlRightDrawer);

        eventsAdapter = new EventListAdapter(this,
                OnTrackManager.getInstance().getEvents());

        mDrawerListRight = (ListView) findViewById(R.id.right_drawer);

        mDrawerListRight.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Intent i = new Intent(MainActivity.this, EventList.class);
                i.putExtra("event_id", id);
                startActivity(i);

            }

        });

        // Configura los elementos del sidebar derecho
        mDrawerListRight.setAdapter(eventsAdapter);

        mDrawerToggleRight = new ActionBarDrawerToggle(this,
                mDrawerLayout, R.drawable.ic_launcher,
                R.string.drawer_open, R.string.drawer_close) {
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu();

            }

            public void onDrawerOpened(View drawerView) {
                invalidateOptionsMenu();
            }
        };

        getSupportActionBar()
                .setDisplayHomeAsUpEnabled(true);
        getSupportActionBar()
                .setHomeButtonEnabled(true);

        vTouchableLayer = findViewById(R.id.vTouchableLayer);
        vTouchableLayer.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                if (OnTrackManager.getInstance().routeDidStart()) {
                    vTouchableLayer.setVisibility(View.GONE);
                    animationEnabled = false;
                }
                return false;
            }
        });

        ImageView ivSideBar = (ImageView) findViewById(R.id.image_view_sidebar);
        ivSideBar.setTag("SIDEBAR");
        ivSideBar.setOnClickListener(this);

//			ImageView ivEvents = (ImageView) rootView
//					.findViewById(R.id.image_view_events);
//			ivEvents.setTag("EVENTS");
//			ivEvents.setOnClickListener((MainActivity) getActivity());

        bAtStop = (Button) findViewById(R.id.bAtStop);
        bAtStop.setTypeface(tf);
        bAtStop.setText(ecuatorian?getResources().getString(R.string.vehicle_at_stop_live_ecuador):getResources().getString(R.string.vehicle_at_stop_live));

        bHelp = (ImageView) findViewById(R.id.bHelp);
        bHelp.setTag(HELP);
        bHelp.setOnClickListener(this);

        ivAllStudentsList = (ImageView) findViewById(R.id.ivAllStudentsList);
        ivAllStudentsList.setTag(ALL_STUDENTS);
        ivAllStudentsList.setOnClickListener(this);

        ivAnnouncements = (ImageView) findViewById(R.id.ivAnnouncements);
        ivAnnouncements.setTag(ANNOUNCEMENTS);
        ivAnnouncements.setOnClickListener(this);

        ivReports = (ImageView) findViewById(R.id.ivReports);
        ivReports.setTag(REPORTS);
        ivReports.setOnClickListener(this);

        ivNonAssignedStudents = (ImageView) findViewById(R.id.ivNonAssignedStudents);
        ivNonAssignedStudents.setTag(FREE);
        ivNonAssignedStudents.setOnClickListener(this);

        ivPanic = (ImageView) findViewById(R.id.ivPanic);
        ivPanic.setTag(PANIC);
        ivPanic.setOnClickListener(this);



        tvNextStopTime = (TextView) findViewById(R.id.tvNextStopTime);
        tvNextStopTime.setTypeface(tf);

        tvNextStopTimeTitle = (TextView) findViewById(R.id.tvNextStopTimeTitle);
        tvNextStopTimeTitle.setTypeface(tf);
        tvNextStopTimeTitle.setText(ecuatorian?getResources().getString(R.string.next_stop_label_ecuador):getResources().getString(R.string.next_stop_label));

        tvComputingPortionOfRoute = (TextView) findViewById(R.id.tvComputingPortionOfRoute);
        tvComputingPortionOfRoute.setTypeface(tf);

        rlComputingPortionOfRoute = (RelativeLayout) findViewById(R.id.rlComputingPortionOfRoute);

        if (organization.getCreatedBy().equals("superadmin@ontrackschool.ec")) {
            ImageView ivRefuge = (ImageView) findViewById(R.id.ivRefuge);
            ivRefuge.setTag(REFUGE);
            ivRefuge.setOnClickListener(this);
            ivRefuge.setVisibility(View.VISIBLE);

            OnTrackManager.getInstance().loadRefuges(this);

            for (Refuge refuge : OnTrackManager.getInstance().getRefuges()) {

                IconFactory mIconFactory = IconFactory.getInstance(this);
                Drawable mIconDrawable = getResources().getDrawable(getResources().getIdentifier(
                        "refuge_marker",
                        "drawable", getPackageName()));
                Icon icon = mIconFactory.fromDrawable(mIconDrawable);
                MarkerOptions marker =new MarkerOptions()
                        .position(new LatLng(refuge.getLatitude(), refuge.getLongitude())).title(refuge.getName()).snippet(refuge.getType() + "-" + refuge.getResponsible() + "-Cel:" + refuge.getMobilePhone())
                        .icon(icon);

                mMap.addMarker(marker);
            }
        }

        ImageView ivWaze = (ImageView) findViewById(R.id.ivWaze);
        ivWaze.setTag(WAZE);
        ivWaze.setOnClickListener(this);

        // Configuraciï¿½n del TTS
        // final Locale locSpanish = new Locale("spa", "MEX");

        onTrackVoice = new TextToSpeech(getApplicationContext(),
                new TextToSpeech.OnInitListener() {
                    @Override
                    public void onInit(int status) {
                        if (status != TextToSpeech.ERROR) {
                            // onTrackVoice.setLanguage(locSpanish);
                        } else {

                        }
                    }
                });
//		onTrackVoice.setPitch(8.0f * (float) Math.random());

        // La actividad actual se convierte en un "Listener" de OnTrackSystem
        OnTrackManager.getInstance().setOnTrackSystemListener(this);

        // La actividad actual se convierte en un "Listener" de ChatManager
        MessageManager.getInstance().addListener(this);

        // La actividad actual se convierte en un "Listener" de EventManager
        EventManager.getInstance().addListener(this);

        // La actividad actual se convierte en un "Listener" de CloseSessionManager
        CloseSessionManager.getInstance().addListener(this);

        // Arreglo de los marcadores que indican la ubicaciï¿½n de los paraderos
        stopsMarkers = new ArrayList<MarkerOptions>();





		/*
         * Llamado al servicio que solicita detalles de la ruta NOTA: Esta es la
		 * primera acciï¿½n que se hace al abrir la aplicaciï¿½n
		 */

        OnTrackManagerAsyncTasks.executeLoadDetailedRouteWithNoveltiesTask(
                OnTrackManagerAsyncTasks.URL_BASE_ROUTE_WITH_NOVELTIES,
                ServiceKeys.TOKEN_KEY,
                OnTrackManager.getInstance().getToken(),
                ServiceKeys.ID_ROUTE_KEY,
                OnTrackManager.getInstance().getRoutes()
                        .get(OnTrackManager.getInstance().getSelectedRoute())
                        .getId());

        /*********/

		/*
         * CONFIGURACIï¿½N DE LOS CARTELES INFORMATIVOS
		 */

        startRouteFragment = new StartRouteFragment();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.rlOutOfMapContainer, startRouteFragment)
                .setCustomAnimations(R.anim.startroute_up,
                        R.anim.startroute_upper).show(startRouteFragment)
                .commit();

        posterFragment = new PosterFragment();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.rlOutOfMapContainer, posterFragment)
                .setCustomAnimations(R.anim.nextstop, R.anim.nextstop_reverse)
                .hide(posterFragment).commit();

        stopDetailsFragment = new StopDetailsFragment();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.rlOutOfMapContainer, stopDetailsFragment)
                .setCustomAnimations(R.anim.stopdetails_down,
                        R.anim.stopdetails_up).hide(stopDetailsFragment)
                .commit();

//		assistanceWithNoveltiesFragment = new AssistanceWithNoveltiesFragment(
//				AssistanceWithNoveltiesFragment.ASSISTANCE_FRAGMENT_TYPE_CHECK);
        assistanceWithNoveltiesFragment = new AssistanceWithNoveltiesFragment();
        ((AssistanceWithNoveltiesFragment) assistanceWithNoveltiesFragment).setType(AssistanceWithNoveltiesFragment.ASSISTANCE_FRAGMENT_TYPE_CHECK);
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.rlOutOfMapContainer, assistanceWithNoveltiesFragment)
                .setCustomAnimations(R.anim.stopdetails_down,
                        R.anim.stopdetails_up)
                .hide(assistanceWithNoveltiesFragment).commit(); // Rafa

        excessiveSpeedFragment = new ExcessiveSpeedFragment();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.rlOutOfMapContainer, excessiveSpeedFragment)
                .setCustomAnimations(R.anim.stopdetails_down,
                        R.anim.stopdetails_up).hide(excessiveSpeedFragment)
                .commit();

        eventFragment = new EventFragment();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.rlOutOfMapContainer, eventFragment)
                .setCustomAnimations(R.anim.event_down, R.anim.event_up)
                .hide(eventFragment).commit();

        strangeStopFragment = new StrangeStopFragment();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.rlOutOfMapContainer, strangeStopFragment)
                .setCustomAnimations(R.anim.event_down, R.anim.event_up)
                .hide(strangeStopFragment).commit();

        cancelSkippedStopFragment = new CancelSkippedStopFragment();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.rlOutOfMapContainer, cancelSkippedStopFragment)
                .setCustomAnimations(R.anim.event_down, R.anim.event_up)
                .hide(cancelSkippedStopFragment).commit();

        /*********/

		/*
		 * CONFIGURACIï¿½N DEL ENVï¿½O DE LOCALIZACIï¿½N PERIï¿½DICA NOTA: cada
		 * cierta cantidad de tiempo se envï¿½a la localizaciï¿½n desde este
		 * activity. Se puede considerar poner esta acciï¿½n en un Service
		 */

        sendLocationTimer = new Timer();
        sendLocationTimer.schedule(new TimerTask() {
            public void run() {
                final OnTrackManager manager = OnTrackManager.getInstance();
                final Location location = manager.getCurrentLocation();

                if (location != null) {

                    if (manager.routeDidStart()) {
                        // Solo se hace el envï¿½o de localizaciï¿½n si el
                        // dispositivo
                        // ha sido ubicado
//						try {
//							OnTrackManagerAsyncTasks
//									.executeSendLocationTask(
//											OnTrackManagerAsyncTasks.URL_BASE_TRACKED_POINT,
//											ServiceKeys.TOKEN_KEY,
//											manager.getToken(),
//											ServiceKeys.TRACKED_POINTS_KEY,
//											manager.getStackedPointsJSONString())
//									.get(SEND_LOCATION_TIMEOUT,
//											TimeUnit.MILLISECONDS);
//						} catch (InterruptedException e) {
//							// TODO Auto-generated catch block
//							e.printStackTrace();
//						} catch (ExecutionException e) {
//							e.printStackTrace();
//						} catch (TimeoutException e) {
//							e.printStackTrace();
//						} catch (JSONException e) {
//							// e.printStackTrace();
//						}

                        // ACTUALIZACIÓN: Ahora se intentará enviar la localización usando Threads
                        ExecutorService executor = Executors.newSingleThreadExecutor();

                        try {
                            executor.submit(new SendLocationThread(OnTrackManagerAsyncTasks.URL_BASE_TRACKED_POINT,
                                    ServiceKeys.TOKEN_KEY,
                                    manager.getToken(),
                                    ServiceKeys.TRACKED_POINTS_KEY,
                                    manager.getStackedPointsJSONString())).get(SEND_LOCATION_TIMEOUT, TimeUnit.MILLISECONDS);
                        } catch (InterruptedException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (ExecutionException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (TimeoutException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        executor.shutdown();

                    }
                }
            }
        }, SEND_LOCATION_PERIOD, SEND_LOCATION_PERIOD);

        // Timer de animaciones de indicadores de paradero
        stopIndicatorAnimationTimer = new Timer();

        // Configurar el arreglo de Polylines
        this.portions = new ArrayList<>();

        getSupportActionBar().hide();

        // Se revisa si el gps está activo (excepto si se está simulando)

        if (!OnTrackManager.getInstance().isSimulating()) {
            final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                buildAlertMessageNoGps();
            }
        }


    }

    @Override
    protected void onStart() {
        super.onStart();
        // bind to Service
        if (!OnTrackManager.getInstance().isSimulating()) {
            Intent intent = new Intent(this, GPSService.class);
            bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
        }
        mMap.onStart();
    }

    /**
     * Callbacks for service binding, passed to bindService()
     */
    private ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            // cast the IBinder and get MyService instance
            GPSService.LocalBinder binder = (GPSService.LocalBinder) service;
            myService = binder.getService();
            bound = true;
            myService.setGPSServiceListener(MainActivity.this);
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            bound = false;
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Configuraciï¿½n del menï¿½ del action bar
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Se configura el botï¿½n del action bar para que muestre el sidebar
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        if (item.getItemId() == R.id.action_showevents) {
            mDrawerLayout.openDrawer(rlRightDrawer);
            mDrawerLayout.closeDrawer(mDrawerList);
        }
        return super.onOptionsItemSelected(item);
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getResources().getString(R.string.gps_disabled_alert))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.gps_enable_option), new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                });

        final AlertDialog alert = builder.create();
        alert.show();
    }

    /***********
     * IMPLEMENTACIï¿½N DE Mï¿½TODOS DE LA CLASE Activity
     *******/

//	@Override
//	protected void onStart() {
//		super.onStart();
//		// Cuando la actividad inicia, empieza la conexiï¿½n para hacer la
//		// localizaciï¿½n
//		mLocationClient.connect();
//	}

	/*
	 * Llamado cuando la actividad no es visible
	 */
    @Override
    protected void onStop() {
        activityVisible = false;
        super.onStop();
        mMap.onStop();
    }

    @Override
    protected void onResume() {
        activityVisible = true;

        // Ver si justo en el momento de reanudar la aplicaciï¿½n el vehï¿½culo
        // estï¿½
        // detenido en algï¿½n
        // paradero
        if (OnTrackManager.getInstance().vehicleInCurrentStop()) {
            ((StopDetailsFragment) stopDetailsFragment).setCurrent(true);
            showStopDetails(OnTrackManager.getInstance().getCurrentStop());
        }

        if (OnTrackManager.getInstance().routeDidStart()) {
            FragmentTransaction ft = getSupportFragmentManager()
                    .beginTransaction();

            // Esconder el fragment de inicio de ruta si la ruta ya ha iniciado
            ft.setCustomAnimations(R.anim.startroute_up,
                    R.anim.startroute_upper).hide(startRouteFragment).commit();
        }
        if (!stopListVisible) {
            if (stopListFragment != null) {

                getSupportFragmentManager().beginTransaction()
                        .add(R.id.llStopsContainer, stopListFragment).commit();
                stopListVisible = true;

            }
        }
        super.onResume();
        mMap.onResume();
    }

    @Override
    public void onPause()  {
        super.onPause();
        mMap.onPause();
    }

    /*******************/

    /**********
     * IMPLEMENTACIï¿½N DE Mï¿½TODOS DE LA CLASE OnTrackSystemListener
     ************/

    // Evento de error en alguno de los AsyncTask
    @Override
    public void onAsyncTaskError(String error) {
        final String mError = error;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MainActivity.this, mError, Toast.LENGTH_LONG).show();
            }
        });

    }

    @Override
    public void onETASUpdate() {
        updateETAS();
    }

    // Evento de detenciï¿½n del vehï¿½culo en un paradero
    @Override
    public void onVehicleJustStoppedAtStop(boolean isDelivery,
                                           Stop currentStop, int numberOfStops,
                                           int estimatedTimeFromCurrentStop) {

        // Para textos en ecuatoriano
        Organization organization = OnTrackManager.getInstance().
                getUser().getOrganizationByID(OnTrackManager.getInstance().getRoutes().get(OnTrackManager.getInstance().getSelectedRoute()).getOrganizationId());
        boolean ecuatorian = organization.getCreatedBy().equals("superadmin@ontrackschool.ec");

        bAtStop.setVisibility(View.GONE);


        String messageHeader;
        String messageDetails = (isDelivery ? GeneralMessages.DELIVERY
                : GeneralMessages.PICK_UP)
                + " "
                + (currentStop.numberOfTrackablesToPickOrLeaveAtStop() != 1 ? ""
                + currentStop.numberOfTrackablesToPickOrLeaveAtStop()
                + " " + GeneralMessages.TRACKABLE_TAG_PLURAL + "."
                : GeneralMessages.ONE_AS_DETERMINANT + " "
                + GeneralMessages.TRACKABLE_TAG_SINGULAR + ".");

        if (isDelivery) {
            if (currentStop.getOrder() == numberOfStops - 1) {
                if(ecuatorian){
                    messageHeader = getResources().getString(R.string.arriving_to_last_stop_header_ecuador);
                }else{
                    messageHeader = getResources().getString(R.string.arriving_to_last_stop_header);
                }
            } else {
                if (!currentStop.isSpecial()) {
                    if (ecuatorian) {
                        messageHeader = getResources().getString(R.string.arriving_to_stop_header_ecuador);
                    } else {
                        messageHeader = getResources().getString(R.string.arriving_to_stop_header);
                    }
                }else {
                    if (ecuatorian) {
                        messageHeader = getResources().getString(R.string.arriving_to_special_stop_header_ecuador);
                        messageDetails += " "
                                + getResources().getString(R.string.remember_report_delivery_ecuador);
                    } else {
                        messageHeader = getResources().getString(R.string.arriving_to_special_stop_header);
                        messageDetails += " "
                                + getResources().getString(R.string.remember_report_delivery);
                    }

                }
            }
        } else {
            if (currentStop.getOrder() == numberOfStops - 1) {
                messageHeader = getResources().getString(R.string.arriving_to_destination_header);
            } else {
                if (!currentStop.isSpecial())
                    if (ecuatorian) {
                        messageHeader = getResources().getString(R.string.arriving_to_stop_header_ecuador);
                    } else {
                        messageHeader = getResources().getString(R.string.arriving_to_stop_header);
                    }
                else {
                    if (ecuatorian) {
                        messageHeader = getResources().getString(R.string.arriving_to_special_stop_header_ecuador);
                        messageDetails += " "
                                + getResources().getString(R.string.remember_report_pick_up_ecuador);
                    } else {
                        messageHeader = getResources().getString(R.string.arriving_to_special_stop_header);
                        messageDetails += " "
                                + getResources().getString(R.string.remember_report_pick_up);
                    }

                }
            }
        }

        speak(messageHeader + messageDetails);

        stopIndicatorAnimationTimer.cancel();

        // this.portions
        // .get(currentStop.getOrder())
        // .getMarker()
        // .setIcon(
        // BitmapDescriptorFactory
        // .fromResource(R.drawable.stop_complete));

        // PORCIÓN DE RUTA A RECORRER

        Stop nextStop = OnTrackManager.getInstance().getNextStop();
        this.portions.get(nextStop.getOrder()).deleteLinesFromMap(mMap);

        ArrayList<Point> newPoints = nextStop.getSteps();

//        for (int z = 0; z < newPoints.size() - 1; z++) {
//            Point src = newPoints.get(z);
//            Point dest = newPoints.get(z + 1);
//
//            // Las siguientes líneas comentadas son para Google
//            //Polyline line;
//
//            //line = mMap
//            //		.addPolyline(new PolylineOptions()
//            //				.add(new LatLng(src.getLatitude(), src
//            //						.getLongitude()),
//            //						new LatLng(dest.getLatitude(), dest
//            //								.getLongitude())).width(10)
//            //				.color(0xff3689ce).geodesic(true).zIndex(1000));
//
////            PathOverlay line = new PathOverlay(Color.parseColor("#3689ce"), 5);
////
////            line.addPoint(new LatLng(src.getLatitude(), src
////                    .getLongitude()));
////            line.addPoint(new LatLng(dest.getLatitude(), dest
////                    .getLongitude()));
////
////            mMap.getOverlays().add(line);
//
////            this.portions.get(nextStop.getOrder()).addLine(line);
//
//        }

        ArrayList<LatLng> newPointsLatLng = new ArrayList<>();
        for(Point point: newPoints){
            newPointsLatLng.add(new LatLng(point.getLatitude(),point.getLongitude()));
        }

        this.portions.get(nextStop.getOrder()).setLines(new PolylineOptions().add(newPointsLatLng.toArray(new LatLng[newPointsLatLng.size()])).color(Color.parseColor("#3689ce"))
                .width(8));

        mMap.addPolyline(this.portions.get(nextStop.getOrder()).getLines());

        // Porción de ruta ya recorrida

        this.portions.get(currentStop.getOrder()).deleteLinesFromMap(mMap);
        newPoints = currentStop.getSteps();

//        for (int z = 0; z < newPoints.size() - 1; z++) {
//            Point src = newPoints.get(z);
//            Point dest = newPoints.get(z + 1);
//
//            // Las siguientes líneas para Google Maps
//            //Polyline line;
//
//            //line = mMap
//            //		.addPolyline(new PolylineOptions()
//            //				.add(new LatLng(src.getLatitude(), src
//            //						.getLongitude()),
//            //						new LatLng(dest.getLatitude(), dest
//            //								.getLongitude())).width(10)
//            //				.color(0xffBBBBBB).geodesic(true).zIndex(-1000));
//
////            PathOverlay line = new PathOverlay(Color.parseColor("#bbbbbb"), 5);
////
////            line.addPoint(new LatLng(src.getLatitude(), src
////                    .getLongitude()));
////            line.addPoint(new LatLng(dest.getLatitude(), dest
////                    .getLongitude()));
////
////            mMap.getOverlays().add(line);
//
////            this.portions.get(currentStop.getOrder()).addLine(line);
//            ;
//
////            this.portions.get(currentStop.getOrder()).addLine(line);
//
//        }

        newPointsLatLng = new ArrayList<>();
        for(Point point: newPoints){
            newPointsLatLng.add(new LatLng(point.getLatitude(),point.getLongitude()));
        }

        this.portions.get(currentStop.getOrder()).setLines(new PolylineOptions().add(newPointsLatLng.toArray(new LatLng[newPointsLatLng.size()])).color(Color.parseColor("#bbbbbb"))
                .width(8));

        mMap.addPolyline(this.portions.get(currentStop.getOrder()).getLines());

        stopIndicatorAnimationTimer = new Timer();

        ((StopListFragment) stopListFragment).setStopSelectionEnabled(false);
        updateCurrentAndNextStop();

        // El texto que se pronuncia por parte de la voz de OnTrack depende de
        // si la ruta es DELIVERY o PICK_UP, o si se estï¿½ llegando al ï¿½ltimo
        // paradero
        // Ademï¿½s se tiene en cuenta si el paradero es especial
        if (currentStop.getOrder() != numberOfStops - 1) {
            // Pinta el tiempo estimado de llegada al ï¿½ltimo paradero
            Calendar c = Calendar.getInstance();
            c.add(Calendar.SECOND, estimatedTimeFromCurrentStop);
            int minutes = c.get(Calendar.MINUTE);
            int hours = c.get(Calendar.HOUR_OF_DAY);
            // tvETA.setText(GeneralMessages.ETA + " " + hours + ":"
            // + (minutes < 10 ? "0" + minutes : minutes));
        }

        if (!nextStop.equals(currentStop)) {
            if (nextStop.isSpecial()) {
                ((StopListFragment) stopListFragment)
                        .setStopIndicatorAsSpecial(nextStop.getOrder());
            } else {
                ((StopListFragment) stopListFragment)
                        .setStopIndicatorAsNotComplete(nextStop.getOrder());
            }
        }

    }

    // Evento de dejar un paradero
    @Override
    public void onVehicleJustLeftCurrentStop(Stop nextStop) {

        if (activityVisible)
            hideStopDetailsAuto();
        ((StopDetailsFragment) stopDetailsFragment).setCurrent(false);
        if (activityVisible)
            showPoster(nextStop);

        // Para textos en ecuatoriano
        Organization organization = OnTrackManager.getInstance().
                getUser().getOrganizationByID(OnTrackManager.getInstance().getRoutes().get(OnTrackManager.getInstance().getSelectedRoute()).getOrganizationId());
        boolean ecuatorian = organization.getCreatedBy().equals("superadmin@ontrackschool.ec");

        String spokenText = ecuatorian?getResources().getString(R.string.next_stop_to_ecuador):getResources().getString(R.string.next_stop_to);
        if (OnTrackManager.getInstance().getCurrentStop().getOrder() != 0) {
            if (nextStop.getEstimatedTimeInMinutes()
                    - OnTrackManager.MAXIMUM_TIME_AT_STOP / 60 > 1) {
                spokenText += ""
                        + (nextStop.getEstimatedTimeInMinutes() - OnTrackManager.MAXIMUM_TIME_AT_STOP / 60)
                        + " " + GeneralMessages.MINUTES_PLURAL + ".";
            } else if (nextStop.getEstimatedTimeInMinutes()
                    - OnTrackManager.MAXIMUM_TIME_AT_STOP / 60 == 1) {
                spokenText += "" + GeneralMessages.ONE_AS_DETERMINANT + " "
                        + GeneralMessages.MINUTES_SINGULAR + ".";
            } else {
                spokenText += GeneralMessages.LESS_THAN_ONE_MINUTE + ".";
            }
        } else {
            if (nextStop.getEstimatedTimeInMinutes() > 1) {
                spokenText += "" + (nextStop.getEstimatedTimeInMinutes()) + " "
                        + GeneralMessages.MINUTES_PLURAL + ".";
            } else if (nextStop.getEstimatedTimeInMinutes() == 1) {
                spokenText += "" + GeneralMessages.ONE_AS_DETERMINANT + " "
                        + GeneralMessages.MINUTES_SINGULAR + ".";
            } else {
                spokenText += GeneralMessages.LESS_THAN_ONE_MINUTE + ".";
            }
        }

        speak(spokenText);
        ((StopListFragment) stopListFragment).setStopSelectionEnabled(true);
        ((StopListFragment) stopListFragment)
                .setStopListVisibleElementsFromStop(nextStop);

        drawCircleAroundStop(nextStop);
    }

    // Evento de inicio de la ruta
    @Override
    public void onRouteStarted(int currentStopOrder) {

        for (int i = 0; i <= currentStopOrder; i++) {
            ((StopListFragment) stopListFragment).setStopIndicatorAsComplete(i);
        }
    }

    // Evento de ruta detallada cargada
    @Override
    public void onDetailedRouteLoaded(Route route) {
        int numberOfStops = route.getStops().size();
        // mProgressDialog.dismiss();


        getSupportActionBar().setTitle(
                Parameters.ROUTE_ACTION_BAR_TITLE + " "
                        + route.getIdWithoutSuffix());


        LayoutInflater inflater = (LayoutInflater) getApplicationContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (numberOfStops >= 2) {
            // Mostrar la lista de paraderos
            stopListFragment = new StopListFragment();
            ((StopListFragment) stopListFragment).setParameters(route.getStops());
            if (activityVisible) {

                getSupportFragmentManager().beginTransaction()
                        .add(R.id.llStopsContainer, stopListFragment).commit();
                stopListVisible = true;
            }
        } else {
            WrongRouteDialogFragment dialog = new WrongRouteDialogFragment();
            dialog.show(getSupportFragmentManager(),
                    Parameters.WRONG_ROUTE_DIALOG_TAG);
        }

        if (startAutomatically) {
            if (OnTrackManager.getInstance().isWaitingGPS()) {
                ((StartRouteFragment) startRouteFragment)
                        .setFragmentTest(getResources().getString(R.string.waiting_location));
            } else {
                startRoute();
            }
        } else if ((!OnTrackManager.getInstance().isWaitingGPS() || OnTrackManager.getInstance().isSimulating())) {
            ((StartRouteFragment) startRouteFragment).hideProgressBar();
            ((StartRouteFragment) startRouteFragment)
                    .setFragmentTest(getResources().getString(R.string.start_route));

            ((StartRouteFragment) startRouteFragment).setButtonEnabled(true);
            OnTrackManager.getInstance().setWaitingGPS(false);
        } else {
            ((StartRouteFragment) startRouteFragment)
                    .setFragmentTest(getResources().getString(R.string.waiting_location));
        }

        OnTrackManager.getInstance().setRouteDetailsLoaded(true);
        routeReady = true;
    }

    // Evento del polï¿½gono de la ruta decodificado
    @Override
    public void onRouteDecoded(ArrayList<Stop> stops) {
        if (!vehicleLocated) {
            if (activityVisible) {
                FragmentTransaction ft = getSupportFragmentManager()
                        .beginTransaction();

                // Esconder el fragment de inicio de ruta
                ft.setCustomAnimations(R.anim.startroute_up,
                        R.anim.startroute_upper).hide(startRouteFragment)
                        .commit();
                ((StopListFragment) stopListFragment)
                        .setStopListVisibleElementsFromStop(OnTrackManager
                                .getInstance().getNextStop());
            }
            updateNextStopTime();
            vehicleLocated = true;
        }

        for (Stop stop : stops) {
            ArrayList<Point> newPoints = stop.getSteps();
            Portion currentPortion = new Portion(stop.getOrder());
            this.portions.add(currentPortion);

//            this.portions.get(stop.getOrder()).deleteLinesFromMap(mMap);


//            for (int z = 0; z < newPoints.size() - 1; z++) {
//                Point src = newPoints.get(z);
//                Point dest = newPoints.get(z + 1);
//                // Las siguientes líneas son para google maps
//                //	Polyline line;
//                //	if (stop.equals(OnTrackManager.getInstance().getNextStop())) {
//                //		line = mMap.addPolyline(new PolylineOptions()
//                //				.add(new LatLng(src.getLatitude(), src
//                //						.getLongitude()),
//                //						new LatLng(dest.getLatitude(), dest
//                //								.getLongitude())).width(10)
//                //				.color(0xff3689ce).geodesic(true).zIndex(1000));
//                //	} else {
//                //		line = mMap.addPolyline(new PolylineOptions()
//                //				.add(new LatLng(src.getLatitude(), src
//                //						.getLongitude()),
//                //						new LatLng(dest.getLatitude(), dest
//                //								.getLongitude())).width(10)
//                //				.color(0xffff80ff).geodesic(true).zIndex(-1000));
//                //	}
////                PathOverlay line;
//                if (stop.equals(OnTrackManager.getInstance().getNextStop())) {
////                    line = new PathOverlay(Color.parseColor("#3689ce"), 5);
////
////                    line.addPoint(new LatLng(src.getLatitude(), src
////                            .getLongitude()));
////                    line.addPoint(new LatLng(dest.getLatitude(), dest
////                            .getLongitude()));
////
////                    mMap.getOverlays().add(line);
//
//                } else {
////                    line = new PathOverlay(Color.parseColor("#ff80ff"), 5);
////                    line.setOverlayIndex(-1000);
////
////                    line.addPoint(new LatLng(src.getLatitude(), src
////                            .getLongitude()));
////                    line.addPoint(new LatLng(dest.getLatitude(), dest
////                            .getLongitude()));
////
////                    mMap.getOverlays().add(line);
//
//                }
////                currentPortion.addLine(line);
//
//            }

            ArrayList<LatLng> newPointsLatLng = new ArrayList<>();
            for(Point point: newPoints){
                newPointsLatLng.add(new LatLng(point.getLatitude(),point.getLongitude()));
            }

            if (stop.equals(OnTrackManager.getInstance().getNextStop())) {
                this.portions.get(stop.getOrder()).setLines(new PolylineOptions().add(newPointsLatLng.toArray(new LatLng[newPointsLatLng.size()])).color(Color.parseColor("#3689ce"))
                        .width(8));
            }else{
                this.portions.get(stop.getOrder()).setLines(new PolylineOptions().add(newPointsLatLng.toArray(new LatLng[newPointsLatLng.size()])).color(Color.parseColor("#ff80ff"))
                        .width(8));
            }

            if(newPointsLatLng.size()>0) {
                mMap.addPolyline(this.portions.get(stop.getOrder()).getLines());
            }


            LatLng latLng = new LatLng(stop.getLatitude(), stop.getLongitude());
            MarkerOptions marker;
            //if (stop.isSpecial()) {
            //	marker = mMap.addMarker(new MarkerOptions()
            //			.position(latLng)
            //			.title(Parameters.STOP_MARKER_TITLE)
            //			.icon(BitmapDescriptorFactory
            //					.fromResource(getResources().getIdentifier(
            //							"marker_orange_" + stop.getOrder(),
            //							"drawable", getPackageName())))
            //			.anchor(0.5f, 1.0f));
            //} else {
            //	marker = mMap.addMarker(new MarkerOptions()
            //			.position(latLng)
            //			.title(Parameters.STOP_MARKER_TITLE)
            //			.icon(BitmapDescriptorFactory
            //					.fromResource(getResources().getIdentifier(
            //							"marker_" + stop.getOrder(),
            //							"drawable", getPackageName())))
            //			.anchor(0.5f, 1.0f));
            //}

            if (stop.isSpecial()) {
//                marker = new Marker(mMap, Parameters.STOP_MARKER_TITLE, "", latLng);
//                marker.setMarker(getResources().getDrawable(getResources().getIdentifier(
//                        "marker_orange_" + stop.getOrder(),
//                        "drawable", getPackageName())));
                IconFactory mIconFactory = IconFactory.getInstance(this);
                Drawable mIconDrawable = getResources().getDrawable(getResources().getIdentifier(
                        "marker_orange_" + stop.getOrder(),
                        "drawable", getPackageName()));
                Icon icon = mIconFactory.fromDrawable(mIconDrawable);

                MarkerOptions stopMarker = new MarkerOptions()
                        .position(latLng)
                        .icon(icon);
                mMap.addMarker(stopMarker);


            } else {
//                marker = new Marker(mMap, Parameters.STOP_MARKER_TITLE, "", latLng);
//                marker.setMarker(getResources().getDrawable(getResources().getIdentifier(
//                        "marker_" + stop.getOrder(),
//                        "drawable", getPackageName())));
                IconFactory mIconFactory = IconFactory.getInstance(this);
                Drawable mIconDrawable = getResources().getDrawable(getResources().getIdentifier(
                        "marker_" + stop.getOrder(),
                        "drawable", getPackageName()));
                Icon icon = mIconFactory.fromDrawable(mIconDrawable);

                MarkerOptions stopMarker = new MarkerOptions()
                        .position(latLng)
                        .icon(icon);
                mMap.addMarker(stopMarker);
            }
//            mMap.addMarker(marker);
//            currentPortion.setMarker(marker);
//            this.portions.add(currentPortion);
        }

        // Se dibujan marcadores correspondientes a todos los paraderos

        int accumulatedEstimatedTime = 0;
        int accumulatedTimeInSeconds = 0;
        for (Stop stop : stops) {
            if (stop.trackablesToPickOrLeaveAtStop() && !stop.isCancelled()) {

                // El tiempo estimado de un paradero es el tiempo estimado
                // propio mï¿½s el acumulado de los otros paraderos
                accumulatedEstimatedTime += stop.getEstimatedTimeInMinutes();
                accumulatedTimeInSeconds += stop.getEstimatedTime();

            } else {
                ((StopListFragment) stopListFragment)
                        .setStopIndicatorAsCancelled(stop.getOrder());
            }

        }
        ((StopListFragment) stopListFragment).setStopSelectionEnabled(true);

        // Pinta el tiempo estimado de llegada al ï¿½ltimo paradero
        Calendar c = Calendar.getInstance();
        c.add(Calendar.SECOND, accumulatedTimeInSeconds);
        int minutes = c.get(Calendar.MINUTE);
        int hours = c.get(Calendar.HOUR_OF_DAY);
        // tvETA.setText(GeneralMessages.ETA + " " + hours + ":"
        // + (minutes < 10 ? "0" + minutes : minutes));

        // Pintar los cï¿½rculos alrededor del prï¿½ximo paradero
        drawCircleAroundStop(OnTrackManager.getInstance().getNextStop());

        getSupportActionBar().setSubtitle(
                Parameters.ROUTE_ACTION_BAR_IN_PROGRESS);

        Stop nextStop = OnTrackManager.getInstance().getNextStop();

        if (activityVisible)
            showPoster(nextStop);



        // Para textos en ecuatoriano
        Organization organization = OnTrackManager.getInstance().
                getUser().getOrganizationByID(OnTrackManager.getInstance().getRoutes().get(OnTrackManager.getInstance().getSelectedRoute()).getOrganizationId());
        boolean ecuatorian = organization.getCreatedBy().equals("superadmin@ontrackschool.ec");

        String spokenText = ecuatorian?getResources().getString(R.string.next_stop_to_ecuador):getResources().getString(R.string.next_stop_to);
        if (OnTrackManager.getInstance().getCurrentStop().getOrder() != 0) {
            if (nextStop.getEstimatedTimeInMinutes()
                    - OnTrackManager.MAXIMUM_TIME_AT_STOP / 60 > 1) {
                spokenText += ""
                        + (nextStop.getEstimatedTimeInMinutes() - OnTrackManager.MAXIMUM_TIME_AT_STOP / 60)
                        + " " + GeneralMessages.MINUTES_PLURAL + ".";
            } else if (nextStop.getEstimatedTimeInMinutes()
                    - OnTrackManager.MAXIMUM_TIME_AT_STOP / 60 == 1) {
                spokenText += "" + GeneralMessages.ONE_AS_DETERMINANT + " "
                        + GeneralMessages.MINUTES_SINGULAR + ".";
            } else {
                spokenText += GeneralMessages.LESS_THAN_ONE_MINUTE + ".";
            }
        } else {
            if (nextStop.getEstimatedTimeInMinutes() > 1) {
                spokenText += "" + (nextStop.getEstimatedTimeInMinutes()) + " "
                        + GeneralMessages.MINUTES_PLURAL + ".";
            } else if (nextStop.getEstimatedTimeInMinutes() == 1) {
                spokenText += "" + GeneralMessages.ONE_AS_DETERMINANT + " "
                        + GeneralMessages.MINUTES_SINGULAR + ".";
            } else {
                spokenText += GeneralMessages.LESS_THAN_ONE_MINUTE + ".";
            }
        }

        speak(spokenText);

    }

    // Evento del vehï¿½culo aproximï¿½ndose al prï¿½ximo paradero
    @Override
    public void onVehicleApproachingToNextStop(boolean isDelivery,
                                               Stop nextStop, int numberOfStops) {

        ((StopListFragment) stopListFragment)
                .setStopListVisibleElementsFromStop(nextStop);

        stopIndicatorAnimationTimer.schedule(new TimerTask() {
            boolean blue = true;

            // El cï¿½rculo correspondiente al paradero comienza a titilar
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        if (activityVisible) {
                            if (blue) {
                                ((StopListFragment) stopListFragment)
                                        .setStopIndicatorAsComplete(OnTrackManager
                                                .getInstance().getNextStop()
                                                .getOrder());
                                blue = false;
                            } else {
                                ((StopListFragment) stopListFragment)
                                        .setStopIndicatorAsNotComplete(OnTrackManager
                                                .getInstance().getNextStop()
                                                .getOrder());
                                blue = true;
                            }
                        }
                    }
                });
            }
        }, STOP_INDICATOR_ANIMATION_PERIOD, STOP_INDICATOR_ANIMATION_PERIOD);

        // Para textos en ecuatoriano
        Organization organization = OnTrackManager.getInstance().
                getUser().getOrganizationByID(OnTrackManager.getInstance().getRoutes().get(OnTrackManager.getInstance().getSelectedRoute()).getOrganizationId());
        boolean ecuatorian = organization.getCreatedBy().equals("superadmin@ontrackschool.ec");

        String messageHeader;
        String messageDetails = (isDelivery ? GeneralMessages.DELIVERY
                : GeneralMessages.PICK_UP)
                + " "
                + (nextStop.numberOfTrackablesToPickOrLeaveAtStop() != 1 ? ""
                + nextStop.numberOfTrackablesToPickOrLeaveAtStop()
                + " " + GeneralMessages.TRACKABLE_TAG_PLURAL + "."
                : GeneralMessages.ONE_AS_DETERMINANT + " "
                + GeneralMessages.TRACKABLE_TAG_SINGULAR + ".");

        if (isDelivery) {
            if (nextStop.getOrder() == numberOfStops - 1) {
                messageHeader = ecuatorian?getResources().getString(R.string.aproaching_to_last_stop_header_ecuador):getResources().getString(R.string.aproaching_to_last_stop_header);

            } else {
                if (!nextStop.isSpecial()) {
                    messageHeader = ecuatorian?getResources().getString(R.string.aproaching_to_stop_header_ecuador):getResources().getString(R.string.aproaching_to_stop_header);
                } else {
                    messageHeader = ecuatorian?getResources().getString(R.string.aproaching_to_special_stop_header_ecuador):getResources().getString(R.string.aproaching_to_special_stop_header);
                }
            }
        } else {
            if (nextStop.getOrder() == numberOfStops - 1) {
                messageHeader = ecuatorian?getResources().getString(R.string.aproaching_to_destination_header):getResources().getString(R.string.aproaching_to_destination_header);
                messageDetails = "";
            } else {
                if (!nextStop.isSpecial()) {
                    messageHeader = ecuatorian?getResources().getString(R.string.aproaching_to_stop_header_ecuador):getResources().getString(R.string.aproaching_to_stop_header);
                } else {
                    messageHeader = ecuatorian?getResources().getString(R.string.aproaching_to_special_stop_header_ecuador):getResources().getString(R.string.aproaching_to_special_stop_header);
                }
            }
        }

        speak(messageHeader + messageDetails);
        if (activityVisible)
            showPoster(nextStop);

    }

    // Evento de ruta finalizada
    @Override
    public void onRouteFinished() {
        ((StopDetailsFragment) stopDetailsFragment).setManualMode(true);
        ((StopDetailsFragment) stopDetailsFragment)
                .setFinishRouteActionToButton();

    }

    // Evento de velocidad excedida
    @Override
    public void onSpeedExceeded(int speed, boolean isSpeedExceeded) {
        if (!isSpeedExceeded) {
            speak(GeneralMessages.VOICE_EXCEEDING_SPEED);
        }
        if (activityVisible)
            showExcessiveSpeedFragment(speed);

    }

    // Evento de reporte de velocidad enviado
    @Override
    public void onSpeedReportSent() {
        speak(GeneralMessages.VOICE_SENDING_SPEED_REPORT);

    }

    // Evento de velocidad dentro de los lï¿½mites permitidos
    @Override
    public void onSpeedNormal(int speed) {
        if (activityVisible)
            hideExcessiveSpeedFragment();

    }


    /*******************/

    /**************
     * IMPLEMENTACIï¿½N DE ChatManagerListener
     **********************/

    // Evento de recepciï¿½n de un mensaje nuevo
    // NOTA: Serï¿½a bueno desacoplarlo de OnTrackSystemListener, pero como la
    // lï¿½gica va a cambiar pronto, prefiero dejarlo asï¿½
    @Override
    public void onReceiveMessage(final ArrayList<Long> idsOfModifiedChatCards) {
        // La voz dice la informaciï¿½n del ï¿½ltimo mensaje que llegï¿½

        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                if (idsOfModifiedChatCards.size() > 0) {
                    long id = idsOfModifiedChatCards.get(idsOfModifiedChatCards
                            .size() - 1);

                    // ChatCard currentChatCard = MessageManager.getInstance()
                    // .getChatCardById(id);
                    ChatCard currentChatCard = OnTrackManager.getInstance()
                            .getChatCardFromLocalDB(id);
                    Message message = currentChatCard.getMessages().get(
                            currentChatCard.getMessages().size() - 1);
                    speak(GeneralMessages.VOICE_NEW_MESSAGE_FROM
                            + message.getChatUser().getFullName() + ": "
                            + message.getMessageText());

                    sidebarAdapter.notifyDataSetChanged();
                    if (activityVisible)
                        showEventFragment(message);

                }
            }
        });

    }

    /****************************/


    public void processLocation(Location location) {
        // OBS 09-12-2014
        // OJO: HAY MUCHAS DE ESTAS COSAS QUE DEBERï¿½AN ESTAR EL OnTrackSystem,
        // revisar, el cï¿½digo se ve muy sucio asï¿½
        try {
//			if (!this.portions.isEmpty()) {
//				double offset = OnTrackManager.getInstance().isHasLeftStop() ? 0
//						: Stop.MAXIMUM_DISTANCE_TO_STOP;
//				ArrayList<Point> pointsForApproximation = new ArrayList<Point>();
//
//				// Por asincronï¿½a, es posible que los steps queden
//				// momentï¿½neamente
//				// vacï¿½os,
//				// lo cual es indeseable, acï¿½ se podrï¿½ hacer un control de
//				// rutas
//				// reales a ver si sirve el parche
//				if (OnTrackManager.getInstance().getCurrentStop().getSteps()
//						.size() > 0) {
//					pointsForApproximation.addAll(OnTrackManager.getInstance()
//							.getCurrentStop().getSteps());
//				}
//
//				if (OnTrackManager.getInstance().getNextStop().getSteps()
//						.size() > 0) {
//					pointsForApproximation.addAll(OnTrackManager.getInstance()
//							.getNextStop().getSteps());
//				}
//				if (GeoPos.convertLocationToPointOnRoute(location,
//						pointsForApproximation, offset)) {
//					OnTrackManager.getInstance().vehicleDeviatedFromRoute();
//					if (GeoPos.isLocationExtremelyDeviatedFromRoute(location,
//							pointsForApproximation)) {
//						OnTrackManager.getInstance()
//								.vehicleExtremelyDeviatedFromRoute();
//					}
//				} else {
//					OnTrackManager.getInstance().vehicleOnTrack();
//				}
//
//			}
//
//			OnTrackManager.getInstance().vehicleLocationChanged(location);

            double lat = location.getLatitude();
            double lon = location.getLongitude();

            LatLng latLng = new LatLng(lat, lon);
            // Las siguientes líneas son para google maps
            //if (gpsMarker == null) {

            //	gpsMarker = mMap.addMarker(new MarkerOptions()
            //			.position(latLng)
            //			.icon(BitmapDescriptorFactory
            //					.fromResource(R.drawable.position_arrow))
            //			.title(Parameters.VEHICLE_MARKER_TITLE)
            //			.anchor(0.5f, 0.5f).flat(true)
            //			.rotation(location.getBearing()));

            // mMap.animateCamera(CameraUpdateFactory
            //			.newLatLngZoom(latLng, 20));

            //} else {
//				animateMarker(gpsMarker, latLng, false);
            //gpsMarker.setRotation(location.getBearing());
            //gpsMarker.setPosition(latLng);
            // animateMarker(gpsMarkerOriginal, latLngOriginal, false);
            //CameraPosition cameraPosition = new CameraPosition.Builder()
            //	.target(latLng).zoom(19).bearing(location.getBearing())
            //	.tilt(50).build();

            //if (animationEnabled) {
            //mMap.animateCamera(CameraUpdateFactory
            //		.newCameraPosition(cameraPosition));
            //}
            if (gpsMarker == null) {

                IconFactory mIconFactory = IconFactory.getInstance(this);
                Drawable mIconDrawable = ContextCompat.getDrawable(this, R.drawable.position_arrow);
                Icon icon = mIconFactory.fromDrawable(mIconDrawable);

                gpsMarker = new MarkerOptions()
                        .position(latLng)
                        .icon(icon);
                mMap.addMarker(gpsMarker);


//                gpsMarker = new Marker(mMap, Parameters.STOP_MARKER_TITLE, "", latLng);
//
//                gpsMarker.setMarker(getResources().getDrawable(R.drawable.position_arrow));
//                mMap.addMarker(gpsMarker);
//
                mMap.setLatLng(latLng);
                mMap.setZoom(16.5);
                mMap.setBearing(location.getBearing());
                mMap.setTilt(45d,0l);
//                mMap.setMapOrientation(
//                        (location.getBearing() >= 0f && location.getBearing() < 180f) ?
//                                -location.getBearing() : 360f - location.getBearing()
//                );


            } else {
//                gpsMarker.setPoint(latLng);
//
//                if (animationEnabled) {
//                    mMap.setZoom(19);
//                    mMap.setCenter(latLng);
//                    mMap.setMapOrientation(
//                            (location.getBearing() >= 0f && location.getBearing() < 180f) ?
//                                    -location.getBearing() : 360f - location.getBearing()
//                    );
//                }

                IconFactory mIconFactory = IconFactory.getInstance(this);
                Drawable mIconDrawable = ContextCompat.getDrawable(this, R.drawable.position_arrow);
                Icon icon = mIconFactory.fromDrawable(mIconDrawable);
                mMap.removeMarker(gpsMarker.getMarker());

                gpsMarker = new MarkerOptions()
                        .position(latLng)
                        .icon(icon);
                mMap.addMarker(gpsMarker);
                if(animationEnabled) {
                    mMap.setLatLng(latLng);
                    mMap.setZoom(16.5);
                    mMap.setBearing(location.getBearing());
                    mMap.setTilt(45d, 0l);
                }
            }
        } catch (Exception e) {
            // No hacer nada. Si llega a suceder algo indeseable, pues el
            // usuario no se da cuenta, pero la aplicaciï¿½n no se jode

        }

    }

    /*************************/

    /**************************
     * FUNCIONES ASOCIADAS A ACCIONES DE BOTONES O ELEMENTOS DE LISTAS
     **********************************/

    // Dar inicio a una ruta
    public void startRoute(View v) {

        if (((StartRouteFragment) startRouteFragment).isButtonEnabled()) {

            // Primero se revisa si la ruta es de ida o de regreso, segï¿½n eso
            // decido si llamo a lista o no
            if (!OnTrackManager.getInstance().isDelivery()) {
                if (!OnTrackManager.getInstance().isSimulating()
                        && OnTrackManager.getInstance().isSimulationsAllowed()) {
                    RecordTrackFragment dialog = new RecordTrackFragment();
                    dialog.show(getSupportFragmentManager(),
                            Parameters.RECORD_SIMULATION_DIALOG_TAG);
                } else {
                    startRoute();
                }
            } else {
//				speak(GeneralMessages.VOICE_REPORT_TRACKABLE_NOT_IN_BUS);
                // AllTrackablesFragment dialog = new AllTrackablesFragment(
                // AllTrackablesFragment.TYPE_FIRST_CALL, "");
                // dialog.show(getSupportFragmentManager(),
                // "alltrackablesfirst");
//				configureAndShowAssistanceWithNoveltiesFragmentPrestart();
                speak(getResources().getString(R.string.voice_report_select_assistance_type_delivery));
                AssistanceTypeDialogFragment assistanceTypeDialogFragment = new AssistanceTypeDialogFragment();
                assistanceTypeDialogFragment.show(getSupportFragmentManager(), "assistancetypedialogfragment");
            }
        }
    }

    public void configureAndShowAssistanceWithNoveltiesFragmentPrestart() {
        if(OnTrackManager.getInstance().getUser().getRoute().getNumberOfUnknownExceptingNovelties() == 0) {
            speak("Por favor seleccione los estudiantes que estén dentro del vehículo. Aquellos estudiantes marcados con una x se reportarán como ausentes.");
        }else{
            speak("Por favor seleccione los estudiantes que no se encuentren en el vehículo. Aquellos estudiantes marcados con una x se reportarán como ausentes.");
        }
        ((AssistanceWithNoveltiesFragment) assistanceWithNoveltiesFragment)
                .setType(AssistanceWithNoveltiesFragment.ASSISTANCE_FRAGMENT_TYPE_PRESTART);
        showAssistanceWithNovelties(OnTrackManager.getInstance()
                .getUser().getRoute().getAllTrackables());
    }

    public void startRoute() {

        ((StartRouteFragment) startRouteFragment).showProgressBar();
        ((StartRouteFragment) startRouteFragment)
                .setFragmentTest(GeneralMessages.STARTING_ROUTE_MESSAGE);
        ((StartRouteFragment) startRouteFragment).setButtonEnabled(false);

        // La barra de paraderos se actualiza
        ((StopListFragment) stopListFragment).setStopIndicatorAsComplete(0);

        // Se solicita un footprint
        if(!OnTrackManager.getInstance().replacementEnabled()) {
            OnTrackManagerAsyncTasks.executeRequestFootprintTask(
                    OnTrackManagerAsyncTasks.URL_BASE_FOOTPRINT,
                    ServiceKeys.FK_ROUTE_KEY, OnTrackManager.getInstance()
                            .getUser().getRoute().getId(), ServiceKeys.TOKEN_KEY,
                    OnTrackManager.getInstance().getToken(), ServiceKeys.DATE_KEY,
                    Timestamp.completeTimestamp());
        }else{
            OnTrackManagerAsyncTasks.executeRequestFootprintTask(
                    OnTrackManagerAsyncTasks.URL_BASE_FOOTPRINT,
                    ServiceKeys.FK_ROUTE_KEY, OnTrackManager.getInstance()
                            .getUser().getRoute().getId(), ServiceKeys.TOKEN_KEY,
                    OnTrackManager.getInstance().getToken(), ServiceKeys.DATE_KEY,
                    Timestamp.completeTimestamp(),"DRIVER",OnTrackManager.getInstance().getReplacementDriver(),
                    "MONITOR", OnTrackManager.getInstance().getReplacementMonitor(),
                    "VEHICLE_PLATE",OnTrackManager.getInstance().getReplacementVehiclePlate());
        }

        ready = true;

        OnTrackManager.getInstance().getUser().getRoute()
                .setStartTime((Calendar) Calendar.getInstance().clone());
        if (OnTrackManager.getInstance().isSimulating()) {
            OnTrackManager.getInstance().sendSimulatedLocation();
        }

    }

//	public void sendSimulatedLocation() {
//		if (!OnTrackManager.getInstance().isRouteFinished()) {
//			List<SimLocation> list = OnTrackManager.getInstance()
//					.getRecordedPoints();
//			Location location = list.get(
//					OnTrackManager.getInstance().getSimulationCounter())
//					.toLocation();
//
////			processLocation(location);
//
//			OnTrackManager.getInstance().completeProcessLocation(location);
//			processLocation(location);
//
//			if (OnTrackManager.getInstance().getSimulationCounter() < OnTrackManager
//					.getInstance().getRecordedPoints().size() - 1) {
//				new Handler().postDelayed(
//						new Runnable() {
//							@Override
//							public void run() {
//								OnTrackManager.getInstance()
//										.increaseSimulationCounter();
//								sendSimulatedLocation();
//							}
//						},
//
//						list.get(
//								OnTrackManager.getInstance()
//										.getSimulationCounter() + 1).getTime()
//								- list.get(
//								OnTrackManager.getInstance()
//										.getSimulationCounter())
//								.getTime());
//			}
//		}
//	}

    // Centrar el mapa en la localizaciï¿½n actual
    public void centerInCurrentLocation(View v) {
        LatLng latLng = new LatLng(OnTrackManager.getInstance()
                .getCurrentLocation().getLatitude(), OnTrackManager
                .getInstance().getCurrentLocation().getLongitude());
        // La siguiente línea para google maps
        //mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 19));
//        mMap.setCenter(latLng);
//        mMap.setZoom(19);
        if(mMap !=null) {
            mMap.setLatLng(latLng);
            if(OnTrackManager.getInstance()
                    .getCurrentLocation()!=null){
            mMap.setBearing(OnTrackManager.getInstance()
                    .getCurrentLocation().getBearing());
            }
            mMap.setZoom(17);
            mMap.setTilt(45d, 0l);
        }
        vTouchableLayer.setVisibility(View.VISIBLE);
        animationEnabled = true;
    }

    // Versión sin clic
    public void centerInCurrentLocation() {
        if (OnTrackManager.getInstance().getCurrentLocation() != null) {
            LatLng latLng = new LatLng(OnTrackManager.getInstance()
                    .getCurrentLocation().getLatitude(), OnTrackManager
                    .getInstance().getCurrentLocation().getLongitude());
            // La siguiente línea para google maps
            //mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 19));
//            mMap.setCenter(latLng);
//            mMap.setZoom(19);
            mMap.setLatLng(latLng);
            mMap.setZoom(16.5);
            mMap.setBearing(OnTrackManager.getInstance()
                    .getCurrentLocation().getBearing());
            mMap.setTilt(45d,0l);
            vTouchableLayer.setVisibility(View.VISIBLE);
            animationEnabled = true;
        }
    }

    // Mostrar la ruta completa en el mapa
    public void showCompleteRoute(View v) {
        this.animationEnabled = false;
        ArrayList<Stop> stops = OnTrackManager.getInstance().getUser().getRoute().getStops();
        mMap.setBearing(0);
        mMap.setTilt(0d,0l);
        mMap.setVisibleCoordinateBounds( new CoordinateBounds( new LatLng( getTopLatitude(stops),
                getLeftLongitude(stops)), new LatLng( getBottomLatitude( stops ), getRightLongitude(stops ) )) , new RectF( 200, 0, 200, 0 ), true);

    }

    // Manejo de acciones del sidebar
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {

        mDrawerLayout.closeDrawers();
        Intent i;
        switch (position) {
            case Parameters.OPTION_REPORTS:

                if (OnTrackManager.getInstance().routeDidStart()) {
                    ReportFragment reportFragment = new ReportFragment();
                    reportFragment.show(getSupportFragmentManager(),
                            Parameters.REPORT_DIALOG_TAG);
                } else {
                    Toast.makeText(this, GeneralMessages.REPORT_FIRST_START_ROUTE,
                            Toast.LENGTH_LONG).show();
                }
                break;
//			case Parameters.OPTION_EVENTS:
//				break;
            case Parameters.OPTION_ROUTE:
                if (routeReady) {
                    i = new Intent(MainActivity.this, RouteProfile.class);
                    hideAllFragmentsForced();
                    startActivity(i);
                } else {
                    Toast.makeText(this,
                            GeneralMessages.ROUTE_WAIT_UNTIL_DETAILS_LOADED,
                            Toast.LENGTH_LONG).show();
                }

                break;
            case Parameters.OPTION_CHAT:
                i = new Intent(MainActivity.this, ChatList.class);
                hideAllFragmentsForced();
                startActivity(i);
                break;
            case Parameters.OPTION_CONTACT:
                if (routeReady) {
                    i = new Intent(MainActivity.this, TrackableList.class);
                    hideAllFragmentsForced();
                    startActivity(i);
                } else {
                    Toast.makeText(this,
                            GeneralMessages.ROUTE_WAIT_UNTIL_DETAILS_LOADED,
                            Toast.LENGTH_LONG).show();
                }
                break;
            case Parameters.OPTION_CLOSE_SESSION:
                CloseSessionDialogFragment dialog = new CloseSessionDialogFragment();
                dialog.show(getSupportFragmentManager(),
                        Parameters.CLOSE_SESSION_DIALOG_TAG);
                break;
            default:
                break;

        }
    }

    /******************************/

    /**************
     * FUNCIONES ASOCIADAS A LOS FRAGMENTS INFORMATIVOS
     ***************/

    public void showPoster(Stop stop) {
        ((PosterFragment) posterFragment).setNextStopText(stop.getDirection());
        ((PosterFragment) posterFragment).setNumberOfStudentsNextStop(stop
                .numberOfTrackablesToPickOrLeaveAtStop());
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.nextstop, R.anim.nextstop_reverse);
        ft.show(posterFragment);
        ft.commit();
        //

        final Timer t = new Timer();
        t.schedule(new TimerTask() {
            public void run() {
                posterCounter++;
                if (posterCounter == POSTER_DURATION) {
                    if (activityVisible)
                        hidePoster();

                    // Finalizar el timer del poster
                    posterCounter = 0;
                    t.cancel();
                    t.purge();

                }
            }

        }, 1000, 1000);

    }

    public void hidePoster() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.nextstop, R.anim.nextstop_reverse);
        ft.hide(posterFragment);
        ft.commit();
    }

    public void hidePosterForced() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.hide(posterFragment);
        ft.commit();
    }

    // Muestra los detalles de un paradero
    public void showStopDetails(final Stop stop) {
        // Para textos en ecuatoriano
        Organization organization = OnTrackManager.getInstance().
                getUser().getOrganizationByID(OnTrackManager.getInstance().getRoutes().get(OnTrackManager.getInstance().getSelectedRoute()).getOrganizationId());
        boolean ecuatorian = organization.getCreatedBy().equals("superadmin@ontrackschool.ec");

        // Instancia del OnTrackManager
        OnTrackManager manager = OnTrackManager.getInstance();

        // Inhabilita los comandos para manejar el mapa
        // Para google maps
        //mMap.getUiSettings().setAllGesturesEnabled(false);

        // Cancela las animaciones
        animationEnabled = false;

        // if (mMap.getCameraPosition().tilt != 0.0f
        // || mMap.getCameraPosition().zoom != 16) {
        //
        // CameraPosition cameraPosition = new CameraPosition.Builder()
        // .target(new LatLng(manager.getCurrentLocation()
        // .getLatitude(), manager.getCurrentLocation()
        // .getLongitude())).tilt(0).zoom(16).build();
        // mMap.animateCamera(
        // CameraUpdateFactory.newCameraPosition(cameraPosition), 100,
        // new CancelableCallback() {
        //
        // @Override
        // public void onFinish() {
        //
        // // Ajusta la posiciï¿½n del mapa a la del paradero
        // LatLng latLng = new LatLng(stop.getLatitude(), stop
        // .getLongitude());
        // android.graphics.Point latLngPoint = mMap
        // .getProjection().toScreenLocation(latLng);
        // android.graphics.Point latLngShiftedPoint = new
        // android.graphics.Point(
        // latLngPoint.x
        // + getWindowManager()
        // .getDefaultDisplay()
        // .getWidth() * 35 / 100,
        // latLngPoint.y);
        //
        // LatLng latLngShifted = mMap.getProjection()
        // .fromScreenLocation(latLngShiftedPoint);
        //
        // CameraPosition cameraPosition = new CameraPosition.Builder()
        // .target(latLngShifted).zoom(16).tilt(0)
        // .build();
        // mMap.animateCamera(CameraUpdateFactory
        // .newCameraPosition(cameraPosition));
        //
        // }
        //
        // @Override
        // public void onCancel() {
        // // TODO Auto-generated method stub
        //
        // }
        // });
        // } else {
        //
        // // Ajusta la posiciï¿½n del mapa a la del paradero
        // LatLng latLng = new LatLng(stop.getLatitude(), stop.getLongitude());
        // android.graphics.Point latLngPoint = mMap.getProjection()
        // .toScreenLocation(latLng);
        // android.graphics.Point latLngShiftedPoint = new
        // android.graphics.Point(
        // latLngPoint.x
        // + getWindowManager().getDefaultDisplay().getWidth()
        // * 35 / 100, latLngPoint.y);
        //
        // LatLng latLngShifted = mMap.getProjection().fromScreenLocation(
        // latLngShiftedPoint);
        //
        // CameraPosition cameraPosition = new CameraPosition.Builder()
        // .target(latLngShifted).zoom(16).tilt(0).build();
        // mMap.animateCamera(CameraUpdateFactory
        // .newCameraPosition(cameraPosition));
        // }

        // Cadena que determina el ï¿½ndice del paradero
        String indexString = OnTrackManager.getInstance()
                .getIndexOfTheStopAtLocation(stop.getLocation()) != 0 ? ""
                + OnTrackManager.getInstance().getIndexOfTheStopAtLocation(
                stop.getLocation()) : "";
        String stopHeader = ecuatorian?getResources().getString(R.string.stop_singular_caps_ecuador):getResources().getString(R.string.stop_singular_caps);
        ((StopDetailsFragment) stopDetailsFragment).setStop(stop);
        ((StopDetailsFragment) stopDetailsFragment).setStopNameText(stopHeader + " "
                + indexString);
        ((StopDetailsFragment) stopDetailsFragment).setStopDirectionText(stop
                .getDirection());

        if (stop.getOrder() != 0) {
            // Se decide si se muestra la hora estimada o la hora real de
            // llegada
            if (!stop.didStopHere()) {
                ((StopDetailsFragment) stopDetailsFragment)
                        .setStopTimeText(GeneralMessages.STOP_ETA
                                + Timestamp.convertToAmPm(manager
                                .getUser()
                                .getRoute()
                                .getEstimatedTimeToStop(stop.getOrder())));
            } else {
                ((StopDetailsFragment) stopDetailsFragment)
                        .setStopTimeText(GeneralMessages.STOP_ARRIVAL_TIME
                                + Timestamp.convertToAmPm(stop.getRealTime()
                                .split(" ")[1]));
            }
        } else {
            if (stop.didStopHere()) {
                ((StopDetailsFragment) stopDetailsFragment)
                        .setStopTimeText(GeneralMessages.STOP_DEPARTURE_TIME
                                + Timestamp.convertToAmPm(stop.getRealTime()
                                .split(" ")[1]));
            }
        }

        boolean isLastStopInPickUpRoute = OnTrackManager.getInstance()
                .getRoutes()
                .get(OnTrackManager.getInstance().getSelectedRoute()).getType() != Route.DELIVERY_CODE
                && (stop.getOrder() == OnTrackManager.getInstance().getUser()
                .getRoute().getStops().size() - 1);

        if (stop.didStopHere() || isLastStopInPickUpRoute
                || OnTrackManager.getInstance().isCloseToNextStop()
                || stop.isCancelled() || !stop.trackablesToPickOrLeaveAtStop()) {
            ((StopDetailsFragment) stopDetailsFragment).hideCancelStopButton();
            ((StopDetailsFragment) stopDetailsFragment).showHelpButton();
        } else {
            ((StopDetailsFragment) stopDetailsFragment).showCancelStopButton();
            ((StopDetailsFragment) stopDetailsFragment).hideHelpButton();
        }

        // Decide si muestra el botï¿½n de modo manual
        // 1. Si se estï¿½ cerca al prï¿½ximo paradero y ademï¿½s se estï¿½n
        // viendo los
        // detalles del prï¿½ximo paradero
        // 2. Si ya se terminï¿½ la ruta
        // 3. Si es un paradero especial
        if ((manager.isCloseToNextStop() && stop.equals(manager.getNextStop()))
                || manager.isRouteFinished()
                || (stop.isSpecial() && !manager.isHasLeftStop())) {
            ((StopDetailsFragment) stopDetailsFragment).setManualMode(true);
        } else {
            ((StopDetailsFragment) stopDetailsFragment).setManualMode(false);
        }

        // Decide las acciones que se le darï¿½n al botï¿½n de modo manual
        if (manager.isCloseToNextStop() && stop.equals(manager.getNextStop())) {
            ((StopDetailsFragment) stopDetailsFragment)
                    .setHasStoppedInStopActionToButton();
        } else if (manager.isRouteFinished()) {
            ((StopDetailsFragment) stopDetailsFragment)
                    .setFinishRouteActionToButton();
        } else if (stop.isSpecial() && !manager.isHasLeftStop()
                && !manager.isRouteFinished()) {
            ((StopDetailsFragment) stopDetailsFragment)
                    .setHasLeftStopActionToButton();
        }

        ((StopDetailsFragment) stopDetailsFragment).setStudentList(stop
                .getTrackables());

        // Muestra los detalles del paradero, con animaciï¿½n incluida
        if (!stopDetailsShown) {
            stopDetailsShown = true;
            FragmentTransaction ft = getSupportFragmentManager()
                    .beginTransaction();
            ft.setCustomAnimations(R.anim.stopdetails_down,
                    R.anim.stopdetails_up);
            ft.show(stopDetailsFragment);
            ft.commit();
        }

        // Decide si muestra el botï¿½n de cerrar o no
        if (((StopDetailsFragment) stopDetailsFragment).isCurrent()) {
            ((StopDetailsFragment) stopDetailsFragment).hideCloseButton();
        } else {
            ((StopDetailsFragment) stopDetailsFragment).showCloseButton();
        }

        if (stop.isSpecial()) {
            ((StopDetailsFragment) stopDetailsFragment)
                    .changeBackgroundColor(0xccef7a14);
        } else if (stop.isCancelled() || !stop.trackablesToPickOrLeaveAtStop()) {
            ((StopDetailsFragment) stopDetailsFragment)
                    .changeBackgroundColor(0xccff0000);
        } else {
            ((StopDetailsFragment) stopDetailsFragment)
                    .changeBackgroundColor(0xcc000000);
        }

    }

    public void hideStopDetailsAuto() {
        stopDetailsShown = false;
//		mMap.getUiSettings().setAllGesturesEnabled(true);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.stopdetails_down, R.anim.stopdetails_up);
        ft.hide(stopDetailsFragment);
        ft.commit();
        centerInCurrentLocation();
    }

    public void hideStopDetails(View v) {
        stopDetailsShown = false;
//		mMap.getUiSettings().setAllGesturesEnabled(true);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.stopdetails_down, R.anim.stopdetails_up);
        ft.hide(stopDetailsFragment);
        ft.commit();
        centerInCurrentLocation();
    }

    public void hideStopDetailsForced() {
        stopDetailsShown = false;
//		mMap.getUiSettings().setAllGesturesEnabled(true);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.hide(stopDetailsFragment);
        ft.commit();
        centerInCurrentLocation();
    }

    public void showExcessiveSpeedFragment(int speed) {

        ((ExcessiveSpeedFragment) excessiveSpeedFragment).setSpeed(speed);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.stopdetails_down, R.anim.stopdetails_up);
        ft.show(excessiveSpeedFragment);
        ft.commit();
    }

    public void hideExcessiveSpeedFragment() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.stopdetails_down, R.anim.stopdetails_up);
        ft.hide(excessiveSpeedFragment);
        ft.commit();
    }

    public void hideExcessiveSpeedFragmentForced() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.hide(excessiveSpeedFragment);
        ft.commit();
    }

    public void showAssistanceWithNovelties(ArrayList<Trackable> trackables) { // Rafa
        ((AssistanceWithNoveltiesFragment) assistanceWithNoveltiesFragment)
                .setStudentList(trackables);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.stopdetails_down, R.anim.stopdetails_up);
        ft.show(assistanceWithNoveltiesFragment);
        ft.commit();
    }

    public void hideAssistanceWithNovelties() { // Rafa
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.stopdetails_down, R.anim.stopdetails_up);
        ft.hide(assistanceWithNoveltiesFragment);
        ft.commit();
    }

    public void hideAssistanceWithNoveltiesForced() { // Rafa
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.hide(assistanceWithNoveltiesFragment);
        ft.commit();
    }

    public void showEventFragment(String sender, String message, int imageId) {
        ((EventFragment) eventFragment).setMessage(sender, message, imageId);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.event_down, R.anim.event_up);
        ft.show(eventFragment);
        ft.commit();

        final Timer t = new Timer();
        t.schedule(new TimerTask() {
            public void run() {
                eventCounter++;
                if (eventCounter == POSTER_DURATION) {
                    if (activityVisible)
                        hideEventFragment();

                    // Finalizar el timer del event poster
                    eventCounter = 0;
                    t.cancel();
                    t.purge();

                }
            }

        }, 1000, 1000);
    }

    public void showEventFragment(Event event) {
        ((EventFragment) eventFragment).setMessage(Parameters.ATTENTION_TITLE,
                event.getDescription(), R.drawable.parlante);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.event_down, R.anim.event_up);
        ft.show(eventFragment);
        ft.commit();

        final Timer t = new Timer();
        t.schedule(new TimerTask() {
            public void run() {
                eventCounter++;
                if (eventCounter == MESSAGE_DURATION) {
                    if (activityVisible)
                        hideEventFragment();

                    // Finalizar el timer del event poster
                    eventCounter = 0;
                    t.cancel();
                    t.purge();

                }
            }

        }, 1000, 1000);
    }

    public void showEventFragment(Message message) {

        ((EventFragment) eventFragment).setMessage(message);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.event_down, R.anim.event_up);
        ft.show(eventFragment);
        ft.commit();

        final Timer t = new Timer();
        t.schedule(new TimerTask() {
            public void run() {
                eventCounter++;
                if (eventCounter == POSTER_DURATION) {
                    if (activityVisible)
                        hideEventFragment();

                    // Finalizar el timer del event poster
                    eventCounter = 0;
                    t.cancel();
                    t.purge();

                }
            }

        }, 1000, 1000);
    }

    public void hideEventFragment() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.event_down, R.anim.event_up);
        ft.hide(eventFragment);
        ft.commit();
    }

    public void hideEventFragmentForced() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.hide(eventFragment);
        ft.commit();
    }

    public void showStrangeStopFragment(Stop stop, String message) {

        ((StrangeStopFragment) strangeStopFragment).setStop(stop);
        ((StrangeStopFragment) strangeStopFragment).setMessage(message);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.event_down, R.anim.event_up);
        ft.show(strangeStopFragment);
        ft.commit();

    }

    public void hideStrangeStopFragment() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.event_down, R.anim.event_up);
        ft.hide(strangeStopFragment);
        ft.commit();
    }

    public void hideStrangeStopFragmentForced() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.hide(strangeStopFragment);
        ft.commit();
    }

    public void showCancelSkippedStopFragment(Stop stop, String message) {

        ((CancelSkippedStopFragment) cancelSkippedStopFragment).setStop(stop);
        ((CancelSkippedStopFragment) cancelSkippedStopFragment)
                .setMessage(message);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.event_down, R.anim.event_up);
        ft.show(cancelSkippedStopFragment);
        ft.commit();

    }

    public void hideCancelSkippedStopFragment() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.event_down, R.anim.event_up);
        ft.hide(cancelSkippedStopFragment);
        ft.commit();
    }

    public void hideCancelSkippedStopFragmentForced() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.hide(cancelSkippedStopFragment);
        ft.commit();
    }

    // Esconder todos los fragments sin animaciï¿½n
    public void hideAllFragmentsForced() {
        hidePosterForced();
        hideStopDetailsForced();
        hideExcessiveSpeedFragmentForced();
        hideEventFragmentForced();
        hideStrangeStopFragmentForced();
        hideCancelSkippedStopFragmentForced();
        hideAssistanceWithNoveltiesForced();
    }

    // CIERRE DE SESIï¿½N

    // HUï¿½RFANOS (Categorï¿½a por definir)
    public void speak(String text) {
        final String textForTts = text;

        onTrackVoice.speak(textForTts, TextToSpeech.QUEUE_ADD, null);

    }

    // Actualizar parada actual y parada siguiente
    public void updateCurrentAndNextStop() {
        OnTrackManager manager = OnTrackManager.getInstance();
        Stop currentStop = manager.getCurrentStop();

        ((StopListFragment) stopListFragment)
                .setStopIndicatorAsComplete(currentStop.getOrder());
        ((StopListFragment) stopListFragment)
                .setStopListVisibleElementsFromStop(currentStop);
        ((StopDetailsFragment) stopDetailsFragment).setCurrent(true);
        if (activityVisible) {
            showStopDetails(currentStop);
        }

    }

    // Actualizar tiempos estimados
    private void updateETAS() {
        OnTrackManager manager = OnTrackManager.getInstance();
        int i = 0;

//        for (Marker marker : stopsMarkers) {
//            int eta = manager.getUser().getRoute().getStops().get(i)
//                    .getGoogleETAToStopFromCurrentLocation();
//            marker.setTitle(eta != -1 ? eta / 60 + " min" : "N.A");
//
//            i++;
//        }
    }

    // Animar el marcador
//	public void animateMarker(final Marker marker, final LatLng toPosition,
//			final boolean hideMarker) {
//		final Handler handler = new Handler();
//		final long start = SystemClock.uptimeMillis();
//		Projection proj = mMap.getProjection();
//		android.graphics.Point startPoint = proj.toScreenLocation(marker
//				.getPosition());
//		final LatLng startLatLng = proj.fromScreenLocation(startPoint);
//		final long duration = 500;
//
//		final Interpolator interpolator = new LinearInterpolator();
//
//		handler.post(new Runnable() {
//			@Override
//			public void run() {
//				long elapsed = SystemClock.uptimeMillis() - start;
//				// float t = interpolator.getInterpolation((float) elapsed
//				// / duration);
//				// double lng = t * toPosition.longitude + (1 - t)
//				// * startLatLng.longitude;
//				// double lat = t * toPosition.latitude + (1 - t)
//				// * startLatLng.latitude;
//
//				double lng = toPosition.longitude;
//
//				double lat = toPosition.latitude;
//
//				marker.setPosition(new LatLng(lat, lng));
//
//				// if (t < 1.0) {
//				// // Post again 16ms later.
//				// handler.postDelayed(this, 16);
//				// } else {
//				// if (hideMarker) {
//				// marker.setVisible(false);
//				// } else {
//				// marker.setVisible(true);
//				// }
//				// }
//			}
//		});
//	}

    @Override
    public void onEventGenerated(long id) {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                eventsAdapter.notifyDataSetChanged();
            }
        });

    }

    @Override
    public void onReceivePusherEvent(Event event) {

        final Event finalEvent = event;
        if (event.getCategory().equals(Event.ROUTE_MESSAGE_CODE)
                || event.getCategory().equals(Event.GENERAL_MESSAGE_CODE)) {
            EventManager.getInstance().setLastAnnouncement(finalEvent);
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    if (finalEvent != null) {
//							eventsAdapter.notifyDataSetChanged();
                        speak("Atención: " + finalEvent.getDescription());
                        if (activityVisible)
                            showEventFragment(finalEvent);
                    }
                }
            });

        }
    }

    @Override
    public void onReceivePusherReport(Report report) {
        final Report innerReport = report;
        runOnUiThread(new Runnable() {
                          @Override
                          public void run() {
                              int reportImageId = -1;
                              String category = innerReport.getCategory();
                              String message = GeneralMessages.REPORT_COORDINATOR_REVIEW;
                              if (category.equals(Report.ACCIDENT_REPORT_CODE)) {
                                  message += GeneralMessages.REPORT_ACCIDENT;
                                  reportImageId = R.drawable.accidente_forposter;

                              } else if (category.equals(Report.TRAFFIC_REPORT_CODE)) {
                                  message += GeneralMessages.REPORT_TRAFFIC;
                                  reportImageId = R.drawable.trafico_forposter;

                              } else if (category
                                      .equals(Report.MECHANICAL_FAILURE_REPORT_CODE)) {
                                  message += GeneralMessages.REPORT_MECHANICAL_FAILURE;
                                  reportImageId = R.drawable.mecanico_forposter;

                              } else if (category.equals(Report.WRONG_ROUTE_REPORT_CODE)) {
                                  message += GeneralMessages.REPORT_WRONG_ROUTE;
                                  reportImageId = R.drawable.desvio_forposter;

                              } else if (category.equals(Report.HEALTH_PROBLEM_REPORT_CODE)) {
                                  message += GeneralMessages.REPORT_HEALTH_PROBLEM;
                                  reportImageId = R.drawable.enfermo_forposter;

                              } else if (category
                                      .equals(Report.BEHAVIOUR_PROBLEM_REPORT_CODE)) {
                                  message += GeneralMessages.REPORT_HEALTH_PROBLEM;
                                  reportImageId = R.drawable.comportamiento_forposter;

                              }

                              int state = innerReport.getState();
                              if (state == Report.REPORT_STATE_PENDING) {
                                  message += ". " + GeneralMessages.REPORT_CHECKED;
                              } else if (state == Report.REPORT_STATE_ANSWERED) {
                                  message += " " + GeneralMessages.REPORT_ANSWER_IS + ": "
                                          + innerReport.getResponse();
                              }

                              speak(Parameters.ATTENTION_TITLE + ": " + message);
                              if (activityVisible)
                                  showEventFragment(Parameters.ATTENTION_TITLE, message,
                                          reportImageId);

                          }
                      }

        );

    }

    public void openEventList(View v) {
        Intent i = new Intent(this, EventList.class);
        startActivity(i);
    }

    @Override
    public void onMessagesRead(long chatCardId) {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                sidebarAdapter.notifyDataSetChanged();
            }
        });

    }

    @Override
    public void onBackPressed() {
        CloseAppDialogFragment dialog = new CloseAppDialogFragment();
        dialog.show(getSupportFragmentManager(),
                Parameters.WRONG_ROUTE_DIALOG_TAG);
    }

    @Override
    public void onLastInfoSent() {
        Intent i = new Intent(this, RouteList.class);
        startActivity(i);
        finish();

    }

    @Override
    protected void onDestroy() {
        EventManager.getInstance().removeListener(this);
        MessageManager.getInstance().removeListener(this);

        onTrackVoice.shutdown();
        getSupportFragmentManager().getFragments().clear();
        super.onDestroy();
        mMap.onDestroy();
    }

    @Override
    public void onStopCancelled(Stop stop) {
        ((StopListFragment) stopListFragment).setStopIndicatorAsCancelled(stop
                .getOrder());
        ((StopListFragment) stopListFragment)
                .setStopListVisibleElementsFromStop(OnTrackManager
                        .getInstance().getNextStop());
        hideStopDetailsAuto();

        Portion currentPortion = this.portions.get(stop.getOrder());
        currentPortion.deleteLinesFromMap(mMap);

        drawCircleAroundStop(OnTrackManager.getInstance().getNextStop());
    }

    @Override
    public void onPortionOfRouteDecoded(Stop stop) {
        rlComputingPortionOfRoute.setVisibility(View.GONE);

        Portion currentPortion = this.portions.get(stop.getOrder());
        currentPortion.deleteLinesFromMap(mMap);

        ArrayList<Point> newPoints = stop.getSteps();
        ArrayList<LatLng> newPointsLatLng = new ArrayList<>();

        for(Point point: newPoints){
            newPointsLatLng.add(new LatLng(point.getLatitude(),point.getLongitude()));
        }

//        currentPortion.clear();

        if (stop.equals(OnTrackManager.getInstance().getNextStop())) {
            this.portions.get(stop.getOrder()).setLines(new PolylineOptions().add(newPointsLatLng.toArray(new LatLng[newPointsLatLng.size()])).color(Color.parseColor("#3689ce"))
                    .width(8));
        }else{
            this.portions.get(stop.getOrder()).setLines(new PolylineOptions().add(newPointsLatLng.toArray(new LatLng[newPointsLatLng.size()])).color(Color.parseColor("#ff80ff"))
                    .width(8));
        }

        if(newPointsLatLng.size()>0) {
            mMap.addPolyline(this.portions.get(stop.getOrder()).getLines());
        }

//        ArrayList<Point> newPoints = stop.getSteps();
//        if (stop.equals(OnTrackManager.getInstance().getNextStop())) {
//            for (int z = 0; z < newPoints.size() - 1; z++) {
//                Point src = newPoints.get(z);
//                Point dest = newPoints.get(z + 1);
//                // Lo siguiente para google maps
//                //Polyline line = mMap.addPolyline(new PolylineOptions()
//                //	.add(new LatLng(src.getLatitude(), src.getLongitude()),
//                //			new LatLng(dest.getLatitude(), dest
//                //					.getLongitude())).width(10)
//                //	.color(0xff3689ce).geodesic(true).zIndex(1000));
//
////                PathOverlay line = new PathOverlay(Color.parseColor("#3689ce"), 5);
////
////                line.addPoint(new LatLng(src.getLatitude(), src
////                        .getLongitude()));
////                line.addPoint(new LatLng(dest.getLatitude(), dest
////                        .getLongitude()));
////
////                line.setOverlayIndex(1000);
////
////                mMap.getOverlays().add(line);
////                currentPortion.addLine(line);
//
//            }
//        } else {
//            for (int z = 0; z < newPoints.size() - 1; z++) {
//                Point src = newPoints.get(z);
//                Point dest = newPoints.get(z + 1);
//                // Lo siguiente para google maps
//                //Polyline line = mMap.addPolyline(new PolylineOptions()
//                //		.add(new LatLng(src.getLatitude(), src.getLongitude()),
//                //				new LatLng(dest.getLatitude(), dest
//                //						.getLongitude())).width(8)
//                //		.color(0xffff80ff).geodesic(true).zIndex(-1000));
//
////                PathOverlay line = new PathOverlay(Color.parseColor("#ff80ff"), 5);
//
////                line.addPoint(new LatLng(src.getLatitude(), src
////                        .getLongitude()));
////                line.addPoint(new LatLng(dest.getLatitude(), dest
////                        .getLongitude()));
////
////                line.setOverlayIndex(-1000);
////                mMap.getOverlays().add(line);
//
////                currentPortion.addLine(line);
//
//            }
//        }

    }

    public static boolean isAnimationEnabled() {
        return animationEnabled;
    }

    public static void setAnimationEnabled(boolean animationEnabled) {
        MainActivity.animationEnabled = animationEnabled;
    }

    public void drawCircleAroundStop(Stop stop) {

        LatLng latLng = new LatLng(stop.getLatitude(), stop.getLongitude());

        if (outterCircle != null) {
            mMap.removeAnnotation(outterCircle.getPolygon());
        }

        if (innerCircle != null) {
            mMap.removeAnnotation(innerCircle.getPolygon());
        }

//        if (stop.isSpecial()) {
//            CircleOverlay circleOverlay = new CircleOverlay(this.portions.get(stop.getOrder()).getMarker(), mMap, 0.0010776, 0x66ffd710);
//            mMap.getOverlays().add(circleOverlay);
//            outterCircle = circleOverlay;
//        } else {
//            CircleOverlay circleOverlay = new CircleOverlay(this.portions.get(stop.getOrder()).getMarker(), mMap, 0.0010776, 0x6666ACE1);
//            mMap.getOverlays().add(circleOverlay);
//            outterCircle = circleOverlay;
//        }
//
//        CircleOverlay circleOverlay = new CircleOverlay(this.portions.get(stop.getOrder()).getMarker(), mMap, 0.0002694, 0x66ff0000);
//        mMap.getOverlays().add(circleOverlay);
//        innerCircle = circleOverlay;
//		if (outterCircle != null) {
//			outterCircle.remove();
//		}
//
//		if (innerCircle != null) {
//			innerCircle.remove();
//		}
//

//
//		CircleOptions circleOptions;


//
        ArrayList<LatLng> outterCirclePolygon = GeoPos.polygonFromCenterDistanceAndSides(latLng,Stop.APPROACH_DISTANCE_TO_STOP,360);
		if (stop.isSpecial()) {
//			circleOptions = new CircleOptions().center(latLng)
//					.radius(Stop.APPROACH_DISTANCE_TO_STOP)
//					.fillColor(0x66ffd710).strokeColor(0xffd710);

//            mapView.addPolygon(new PolygonOptions()
//                    .addAll(polygon)
//                    .fillColor(Color.parseColor("#3bb2d0")));
            outterCircle = new PolygonOptions()
                    .addAll(outterCirclePolygon).alpha(0.6f)
                    .fillColor(Color.parseColor("#99ffd710"));
            mMap.addPolygon(outterCircle);
		} else {
//			circleOptions = new CircleOptions().center(latLng)
//					.radius(Stop.APPROACH_DISTANCE_TO_STOP)
//					.fillColor(0x6666ACE1).strokeColor(0x66ACE1);

            outterCircle = new PolygonOptions()
                    .addAll(outterCirclePolygon).alpha(0.6f)
                    .fillColor(Color.parseColor("#66ACE1"));
            mMap.addPolygon(outterCircle);
		}


        ArrayList<LatLng> innerCirclePolygon = GeoPos.polygonFromCenterDistanceAndSides(latLng,Stop.MAXIMUM_DISTANCE_TO_STOP,360);
        innerCircle = new PolygonOptions()
                .addAll(innerCirclePolygon).alpha(0.6f)
                .fillColor(Color.parseColor("#ff0000"));
        mMap.addPolygon(innerCircle);

//		// Dibuja ï¿½reas circulares cercanas a los paraderos
//		outterCircle = mMap.addCircle(circleOptions);
//		outterCircle.setStrokeWidth(2);
//
//		circleOptions = new CircleOptions().center(latLng)
//				.radius(Stop.MAXIMUM_DISTANCE_TO_STOP).fillColor(0x66ff0000)
//				.strokeColor(0xf00);
//
//		innerCircle = mMap.addCircle(circleOptions);
//		innerCircle.setStrokeWidth(2);
    }


    @Override
    public void onClick(View v) {

        //Para textos en ecuatoriano

        Organization organization = OnTrackManager.getInstance().getUser().getOrganizationByID(OnTrackManager.getInstance().getRoutes().get(OnTrackManager.getInstance().getSelectedRoute()).getOrganizationId());
        boolean ecuatorian = organization.getCreatedBy().equals("superadmin@ontrackschool.ec");
        String tag = v.getTag().toString();

        if (tag.equals("SIDEBAR")) {
            mDrawerLayout.openDrawer(mDrawerList);
            mDrawerLayout.closeDrawer(rlRightDrawer);
//		} else if (tag.equals("EVENTS")) {
//			mDrawerLayout.closeDrawer(mDrawerList);
//			mDrawerLayout.openDrawer(rlRightDrawer);
        } else if (tag.equals(HELP)) {
			HelpDialogFragment helpDialogFragment = new HelpDialogFragment( );
			helpDialogFragment.show(getFragmentManager(), "helpDialogFragment");
            //Toast.makeText(this, getResources().getString(R.string.help_unabled_toast), Toast.LENGTH_LONG).show();
        } else if (tag.equals(ALL_STUDENTS)) {
            ((AssistanceWithNoveltiesFragment) assistanceWithNoveltiesFragment)
                    .setType(AssistanceWithNoveltiesFragment.ASSISTANCE_FRAGMENT_TYPE_CHECK);
            ((AssistanceWithNoveltiesFragment) assistanceWithNoveltiesFragment)
                    .setStudentList(OnTrackManager.getInstance().getUser()
                            .getRoute().getTrackables());
            showAssistanceWithNovelties(OnTrackManager.getInstance().getUser()
                    .getRoute().getAllTrackables());
        } else if (tag.equals(ANNOUNCEMENTS)) {
            MessageManager.getInstance().playLastAnnouncement();
        } else if (tag.equals(REPORTS)) {
            if (OnTrackManager.getInstance().routeDidStart()) {
                ReportFragment reportFragment = new ReportFragment();
                reportFragment.show(this.getSupportFragmentManager(),
                        Parameters.REPORT_DIALOG_TAG);
            } else {
                Toast.makeText(this, GeneralMessages.REPORT_FIRST_START_ROUTE,
                        Toast.LENGTH_LONG).show();
            }
        } else if (tag.equals(FREE)) {
            ((AssistanceWithNoveltiesFragment) assistanceWithNoveltiesFragment)
                    .setType(AssistanceWithNoveltiesFragment.ASSISTANCE_FRAGMENT_TYPE_NOVELTY);
            ((AssistanceWithNoveltiesFragment) assistanceWithNoveltiesFragment)
                    .setStudentList(OnTrackManager.getInstance().getUser()
                            .getRoute().getTrackables());
            showAssistanceWithNovelties(OnTrackManager.getInstance().getUser()
                    .getRoute().getTrackablesFromNovelties());
        } else if (tag.equals(REFUGE)) {
            if (OnTrackManager.getInstance().getCurrentLocation() != null) {


                speak(getResources().getString(R.string.refuge_confirmation));
                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                alertDialog.setTitle(getResources().getString(R.string.refuge_title));
                alertDialog.setMessage(getResources().getString(R.string.refuge_confirmation));
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.yes),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                com.mapbox.mapboxsdk.geometry.LatLng latLng = new com.mapbox.mapboxsdk.geometry.LatLng(OnTrackManager.getInstance().getCurrentLocation().getLatitude(), OnTrackManager.getInstance().getCurrentLocation().getLongitude());

                                //Refuge closestRefuge = OnTrackManager.getInstance( ).getClosestRefuge( new com.mapbox.mapboxsdk.geometry.LatLng( -0.240697, -78.505366 ) );
                                Refuge closestRefuge = OnTrackManager.getInstance().getClosestRefuge(latLng);

                                //OnTrackManager.getInstance( ).showDirectionsToRefugeOnRoad( this, new com.mapbox.mapboxsdk.geometry.LatLng( -0.240697, -78.505366 ), closestRefuge );
                                OnTrackManager.getInstance().showDirectionsToRefugeOnRoad(MainActivity.this, latLng, closestRefuge);
                                dialog.dismiss();
                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getResources().getString(R.string.no),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            }
        } else if (tag.equals(PANIC)) {
            if (OnTrackManager.getInstance().getCurrentLocation() != null) {


                speak(getResources().getString(R.string.panic_confirmation));
                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                alertDialog.setTitle(getResources().getString(R.string.panic_title));
                alertDialog.setMessage(getResources().getString(R.string.panic_confirmation));
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.yes),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                com.mapbox.mapboxsdk.geometry.LatLng latLng = new com.mapbox.mapboxsdk.geometry.LatLng(OnTrackManager.getInstance().getCurrentLocation().getLatitude(), OnTrackManager.getInstance().getCurrentLocation().getLongitude());
                                SendPanicMessageTask task = new SendPanicMessageTask(MainActivity.this);
                                task.execute(OnTrackManagerAsyncTasks.URL_PANIC, ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance().getToken(),ServiceKeys.ID_ROUTE_KEY,
                                        OnTrackManager.getInstance().getUser().getRoute().getId(), ServiceKeys.LATITUDE_KEY,
                                        "" + OnTrackManager.getInstance().getCurrentLocation().getLatitude(),
                                        ServiceKeys.LONGITUDE_KEY, "" + OnTrackManager.getInstance().getCurrentLocation().getLongitude(), ServiceKeys.DATE_WROTE_KEY,Timestamp.completeTimestamp());
                                dialog.dismiss();
                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getResources().getString(R.string.no),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            }

        } else if (tag.equals(WAZE)) {

            boolean isLastStop = OnTrackManager.getInstance().getNextStop().getOrder() == OnTrackManager.getInstance().getUser().getRoute().getStops().size() - 1;
            if (OnTrackManager.getInstance().getCurrentStop().getOrder() == 0 || isLastStop) {
                speak(getResources().getString(ecuatorian?R.string.waze_instructions_spoken_ecuador:R.string.waze_instructions_spoken));
                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                alertDialog.setTitle(getResources().getString(R.string.waze_title));
                alertDialog.setMessage(getResources().getString(ecuatorian?R.string.waze_instructions_ecuador:R.string.waze_instructions));
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.yes),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    double latitude = OnTrackManager.getInstance().getNextStop().getLatitude();
                                    double longitude = OnTrackManager.getInstance().getNextStop().getLongitude();

                                    String url = "waze://?ll=" + latitude + "," + longitude + "&naviate=yes";
                                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                                    startActivity(intent);
                                } catch (ActivityNotFoundException e) {
                                    speak(getResources().getString(R.string.waze_notinstalled_spoken));
                                    AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                                    alertDialog.setTitle(getResources().getString(R.string.waze_title));
                                    alertDialog.setMessage(getResources().getString(R.string.waze_notinstalled));
                                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getResources().getString(R.string.create_vehicle_dialog_ok),
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.dismiss();
                                                }
                                            });
                                    alertDialog.show();
                                }
                                dialog.dismiss();
                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getResources().getString(R.string.no),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            } else {
                speak(getResources().getString(ecuatorian?R.string.waze_forbidden_spoken_ecuador:R.string.waze_forbidden_spoken));
                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                alertDialog.setTitle(getResources().getString(R.string.waze_title));
                alertDialog.setMessage(getResources().getString(ecuatorian?R.string.waze_forbidden_ecuador:R.string.waze_forbidden));
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getResources().getString(R.string.create_vehicle_dialog_ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            }


        }
    }

    @Override
    public void onVehicleVeryCloseToNextStop(Stop nextStop) {
        bAtStop.setVisibility(View.VISIBLE);
        final Stop nextStopForAction = nextStop;
        final Location currentLocation = OnTrackManager.getInstance()
                .getCurrentLocation();
        bAtStop.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

//				if (nextStopForAction.getLocation().distanceTo(currentLocation) > Stop.MAXIMUM_DISTANCE_TO_STOP
//						&& nextStopForAction.getLocation().distanceTo(
//						currentLocation) <= 100) {
//					SQLiteDatabase db = OnTrackManager.getInstance()
//							.getDatabaseHelper().getWritableDatabase();
//
//					ContentValues values = new ContentValues();
//					values.put(Stops.COLUMN_NAME_STOP_ID,
//							nextStopForAction.getId());
//					values.put(Stops.COLUMN_NAME_LATITUDE,
//							currentLocation.getLatitude());
//					values.put(Stops.COLUMN_NAME_LONGITUDE,
//							currentLocation.getLongitude());
//
//					long newRowId;
//					newRowId = db.insert(Stops.TABLE_NAME, null, values);
//
//					db.close();
//
//					db = OnTrackManager.getInstance().getDatabaseHelper()
//							.getReadableDatabase();
//
//					String[] projection = { Stops.COLUMN_NAME_LATITUDE,
//							Stops.COLUMN_NAME_LONGITUDE };
//
//					String selection = Stops.COLUMN_NAME_STOP_ID + "== ?";
//
//					String[] selectionArgs = { String.valueOf(nextStopForAction
//							.getId()) };
//
//					Cursor cursor = db.query(Stops.TABLE_NAME, projection,
//							selection, selectionArgs, null, null, null);
//
//					int count = 0;
//
//					while (cursor.moveToNext()) {
//						double latitude = cursor.getDouble(cursor
//								.getColumnIndex(Stops.COLUMN_NAME_LATITUDE));
//						double longitude = cursor.getDouble(cursor
//								.getColumnIndex(Stops.COLUMN_NAME_LONGITUDE));
//
//						Location location = new Location("");
//						location.setLatitude(latitude);
//						location.setLongitude(longitude);
//
//						if (location.distanceTo(currentLocation) <= 10) {
//							count++;
//						}
//					}
//
//					db.close();
//
//					if (count >= 3) {
//						AlertDialog.Builder builder = new AlertDialog.Builder(
//								MainActivity.this);
//						builder.setTitle("Paradero "
//								+ nextStopForAction.getOrder());
//						builder.setMessage("Hemos detectado que este paradero posiblemente esta mal ubicado. Desea asignar el paradero en esta posicion?");
//						builder.setPositiveButton("Si",
//								new DialogInterface.OnClickListener() {
//									@Override
//									public void onClick(DialogInterface dialog,
//														int which) {
//										PrepareStopsForStopUpdateLocationTask getStepsForStopUpdateLocation = new PrepareStopsForStopUpdateLocationTask( MainActivity.this, nextStopForAction );
//										getStepsForStopUpdateLocation.execute();
//									}
//								});
//						builder.setNegativeButton("No",
//								new DialogInterface.OnClickListener() {
//									@Override
//									public void onClick(DialogInterface dialog,
//														int which) {
//										// TODO Auto-generated method stub
//									}
//								});
//						builder.setCancelable(false);
//
//						AlertDialog alert = builder.create();
//						alert.show();
//					}
//				}

                OnTrackManager.getInstance()
                        .setCurrentLocationAsReferenceLocation();
                OnTrackManager.getInstance().vehicleJustStoppedAtStop(
                        nextStopForAction);
                // showStopDetails(nextStopForAction);
                bAtStop.setVisibility(View.GONE);
            }
        });

    }

    public void updateNextStopTime() {
        OnTrackManager manager = OnTrackManager.getInstance();
        tvNextStopTime.setText(Timestamp.convertToAmPm(manager.getUser()
                .getRoute()
                .getEstimatedTimeToStop(manager.getNextStop().getOrder())));
    }

    @Override
    public void onEstimatedTimesChanged() {
        updateNextStopTime();
    }

    @Override
    public void onPreComputingPortionOfRoute() {
        rlComputingPortionOfRoute.setVisibility(View.VISIBLE);

    }

    @Override
    public void onVehicleMightStopAtStrangeStop(Stop stop) {
        String message = "Se ha detectado que está detenido en el paradero "
                + stop.getOrder()
                + ". ¿Desea notificar su llegada a este paradero?";
        speak(message);
        if (activityVisible) {
            showStrangeStopFragment(stop, message);
        }
    }

    @Override
    public void onVehicleMightCancelSkippedStop(Stop stop) {
        String message = "Se ha detectado que no se ha detenido en el paradero "
                + stop.getOrder() + ". ¿Desea cancelarlo?";
        speak(message);
        if (activityVisible) {
            showCancelSkippedStopFragment(stop, message);
        }

    }

    @Override
    public void onVehicleStartedMoving() {
        if (activityVisible) {
            hideStrangeStopFragment();
        } else {
            hideStrangeStopFragmentForced();
        }

    }

    public void startAutomatically() {
        if (startAutomatically) {
            startRoute();
        }
    }

    @Override
    public void onContinue(ArrayList<Trackable> trackables, boolean permanent) {
        if (!trackables.isEmpty()) {
            int counter = 0;
            boolean found = false;

            for (int i = 0; i < OnTrackManager.getInstance().getUser().getRoute().getStops().size() && !found; i++)
            {
                Stop stop = OnTrackManager.getInstance().getUser().getRoute().getStops().get(i);
                counter = 0;

                for (Trackable trackable : stop.getTrackables())
                {
                    for (Trackable updateTrackable : trackables)
                    {
                        if (updateTrackable.getId().equals(trackable.getId()))
                        {
                            counter++;
                        }
                    }

                    if (counter == trackables.size())
                    {
                        String stopId = String.valueOf( stop.getId( ) );
                        String footprintId = String.valueOf( OnTrackManager.getInstance( ).getUser( ).getFootprint( ) );
                        String type = "1";
                        String latitude = String.valueOf( OnTrackManager.getInstance( ).getCurrentLocation( ).getLatitude( ) );
                        String longitude = String.valueOf( OnTrackManager.getInstance( ).getCurrentLocation( ).getLongitude( ) );
                        String order = OnTrackManager.getInstance( ).getNextStop( ) != null ? String.valueOf( OnTrackManager.getInstance( ).getNextStop( ).getOrder( ) ) : String.valueOf( OnTrackManager.getInstance( ).getCurrentStop( ).getOrder( ) + 1 );

                        ChangeStopTask changeStopTask = new ChangeStopTask( this );
                        changeStopTask.execute( WebServices.URL_CHANGE_STOP, ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance( ).getToken( ), ServiceKeys.STOP_CHANGES_KEY, JSONManager.getJSONForStopChange( stopId, footprintId, type, latitude, longitude, order, null ) );

                        //PrepareStopsForStopUpdateLocationTask getStepsForStopUpdateLocation = new PrepareStopsForStopUpdateLocationTask(MainActivity.this, stop);
                        //getStepsForStopUpdateLocation.execute();
                        found = true;
                        break;
                    }
                }
            }

            if (counter == 0)
            {
                if (permanent)
                {
                    String footprintId = String.valueOf( OnTrackManager.getInstance( ).getUser( ).getFootprint( ) );
                    String type = "0";
                    String latitude = String.valueOf( OnTrackManager.getInstance( ).getCurrentLocation( ).getLatitude( ) );
                    String longitude = String.valueOf( OnTrackManager.getInstance( ).getCurrentLocation( ).getLongitude( ) );
                    String order = OnTrackManager.getInstance( ).getNextStop( ) != null ? String.valueOf( OnTrackManager.getInstance( ).getNextStop( ).getOrder( ) ) : String.valueOf( OnTrackManager.getInstance( ).getCurrentStop( ).getOrder( ) + 1 );

                    ChangeStopTask changeStopTask = new ChangeStopTask( this );
                    changeStopTask.execute( WebServices.URL_CHANGE_STOP, ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance( ).getToken( ), ServiceKeys.STOP_CHANGES_KEY, JSONManager.getJSONForStopChange( null, footprintId, type, latitude, longitude, order, trackables ) );
                    //PrepareStopsForStopAdditionTask getStepsForStopAdditionTask = new PrepareStopsForStopAdditionTask(MainActivity.this, OnTrackManager.getInstance().getCurrentLocation().getLatitude(), OnTrackManager.getInstance()
                            //.getCurrentLocation().getLongitude(), trackables);
                    //getStepsForStopAdditionTask.execute();
                }
                else
                {
                    //TODO Notificar a los padres
                }
            }
        }
    }

    public Portion getPortion(int position) {
        return portions.get(position);
    }

    //public GoogleMap getMap() {
    //	return mMap;
    //}

    public MapView getMap() {
        return mMap;
    }

    @Override
    public void onNoveltiesLoaded() {
        //Para textos en ecuatoriano

        int totalNovelties = OnTrackManager.getInstance().getUser().getRoute()
                .numberOfTotalNoveties();

        if (totalNovelties > 0) {
            ((StartRouteFragment)startRouteFragment).showNoveltyViewer();
        }

    }

    public void configureNoveltyList(){
        int totalNovelties = OnTrackManager.getInstance().getUser().getRoute()
                .numberOfTotalNoveties();
        Organization organization = OnTrackManager.getInstance().getUser().getOrganizationByID(OnTrackManager.getInstance().getRoutes().get(OnTrackManager.getInstance().getSelectedRoute()).getOrganizationId());
        boolean ecuatorian = organization.getCreatedBy().equals("superadmin@ontrackschool.ec");
        String message = GeneralMessages.NOVELTY_SUMMARY_HEADER;
        message += totalNovelties == 1 ? GeneralMessages.ONE_AS_DETERMINANT_FEMALE
                + GeneralMessages.NOVELTY_TITLE + ": "
                : totalNovelties + GeneralMessages.NOVELTIES_TITLE + ": ";

        int numberOfNotAttendingNovelties = OnTrackManager.getInstance()
                .getUser().getRoute().numberOfNotAttendingNovelties();
        if (numberOfNotAttendingNovelties > 0) {
            message += numberOfNotAttendingNovelties == 1 ? GeneralMessages.ONE_AS_DETERMINANT
                    + " "
                    + GeneralMessages.TRACKABLE_TAG_SINGULAR
                    + GeneralMessages.NOVELTY_NOT_ASSISTING
                    : numberOfNotAttendingNovelties + " "
                    + GeneralMessages.TRACKABLE_TAG_PLURAL
                    + GeneralMessages.NOVELTY_NOT_ASSISTING_PLURAL;
            message += ". ";
        }

        int numberOfCarNovelties = OnTrackManager.getInstance()
                .getUser().getRoute().numberOfCarNovelties();
        if (numberOfCarNovelties > 0) {
            message += numberOfCarNovelties == 1 ? GeneralMessages.ONE_AS_DETERMINANT
                    + " "
                    + GeneralMessages.TRACKABLE_TAG_SINGULAR
                    + GeneralMessages.NOVELTY_CAR
                    : numberOfCarNovelties + " "
                    + GeneralMessages.TRACKABLE_TAG_PLURAL
                    + GeneralMessages.NOVELTY_CAR_PLURAL;
            message += ". ";
        }

        int numberOfOutOfRouteNovelties = OnTrackManager.getInstance()
                .getUser().getRoute().numberOfOutOfCurrentRouteNovelties();
        if (numberOfOutOfRouteNovelties > 0) {
            message += numberOfOutOfRouteNovelties == 1 ? GeneralMessages.ONE_AS_DETERMINANT
                    + " "
                    + GeneralMessages.TRACKABLE_TAG_SINGULAR
                    + GeneralMessages.NOVELTY_OUT_ROUTE_SINGULAR
                    : numberOfOutOfRouteNovelties + " "
                    + GeneralMessages.TRACKABLE_TAG_PLURAL
                    + GeneralMessages.NOVELTY_OUT_ROUTE_PLURAL;
            message += ". ";
        }

        int numberOfIntoRouteNovelties = OnTrackManager.getInstance()
                .getUser().getRoute().numberOfIntoCurrentRouteNovelties();
        if (numberOfIntoRouteNovelties > 0) {
            message += numberOfIntoRouteNovelties == 1 ? GeneralMessages.ONE_AS_DETERMINANT
                    + " "
                    + GeneralMessages.TRACKABLE_TAG_SINGULAR
                    + GeneralMessages.NOVELTY_IN_ROUTE_SINGULAR
                    : numberOfIntoRouteNovelties + " "
                    + GeneralMessages.TRACKABLE_TAG_PLURAL
                    + GeneralMessages.NOVELTY_IN_ROUTE_PLURAL;
            message += ". ";
        }

        int numberOfChangingStopNovelties = OnTrackManager.getInstance()
                .getUser().getRoute().numberOfChangingStopNovelties();
        if (numberOfChangingStopNovelties > 0) {
            if (OnTrackManager.getInstance().isDelivery()) {
                message += numberOfChangingStopNovelties == 1 ? GeneralMessages.ONE_AS_DETERMINANT
                        + " "
                        + GeneralMessages.TRACKABLE_TAG_SINGULAR
                        + (getResources().getString(ecuatorian?R.string.novelty_change_stop_delivery_ecuador:R.string.novelty_change_stop_delivery))
                        : numberOfChangingStopNovelties
                        + " "
                        + GeneralMessages.TRACKABLE_TAG_PLURAL
                        + (getResources().getString(ecuatorian?R.string.novelty_change_stop_delivery_plural_ecuador:R.string.novelty_change_stop_delivery_plural));
            } else {
                message += numberOfChangingStopNovelties == 1 ? GeneralMessages.ONE_AS_DETERMINANT
                        + " "
                        + GeneralMessages.TRACKABLE_TAG_SINGULAR
                        + (getResources().getString(ecuatorian?R.string.novelty_change_stop_pickup_ecuador:R.string.novelty_change_stop_pickup))
                        : numberOfChangingStopNovelties
                        + " "
                        + GeneralMessages.TRACKABLE_TAG_PLURAL
                        + (getResources().getString(ecuatorian?R.string.novelty_change_stop_pickup_plural_ecuador:R.string.novelty_change_stop_pickup_plural));
            }
            message += ". ";
        }

        speak(message);
        ((AssistanceWithNoveltiesFragment)assistanceWithNoveltiesFragment).setType(AssistanceWithNoveltiesFragment.ASSISTANCE_FRAGMENT_TYPE_NOVELTY_LIST);
        showAssistanceWithNovelties(OnTrackManager.getInstance().getUser()
                .getRoute().getAllTrackables());
    }

    @Override
    public void processSimulated(Location location) {
        processLocation(location);
    }

    @Override
    public void onContinue(ArrayList<Trackable> trackables, boolean adding, int stopID)
    {
        //Route route = OnTrackManager.getInstance().getUser().getRoute();
        //ArrayList<Stop> oldStops = new ArrayList<Stop>();

        if (adding)
        {
            String stopId = String.valueOf( stopID );
            String footprintId = String.valueOf( OnTrackManager.getInstance( ).getUser( ).getFootprint( ) );
            String type = "0";
            ChangeStopTrackablesTask changeStopTrackablesTask = new ChangeStopTrackablesTask( this );
            changeStopTrackablesTask.execute( WebServices.URL_CHANGE_STOP_TRACKABLES, ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance( ).getToken( ), ServiceKeys.TRACKABLE_CHANGES_KEY, JSONManager.getJSONForTrackablesStopChange( stopId, footprintId, type, trackables ) );
//            for (Trackable trackable : trackables) {
//                boolean added = false;
//
//                for (Stop stop : route.getStops()) {
//                    for (Trackable stopTrackable : stop.getTrackables()) {
//                        if (stopTrackable.getId().equals(trackable.getId())) {
//                            oldStops.add(stop);
//                            added = true;
//                        }
//                    }
//                }
//
//                if (!added) {
//                    oldStops.add(null);
//                }
//            }
//
//            AssignTrackableToStopTask assignTrackableToStopTask = new AssignTrackableToStopTask(this, stopID, trackables, oldStops);
//            assignTrackableToStopTask.execute(WebServices.URL_ASSIGN_TRACKABLE_TO_STOP, ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance().getToken(), ServiceKeys.ID_STOP_KEY, String.valueOf(stopID), Trackable.TRACKABLES_KEY, JSONManager.getJSONForTrackablesStopAddition(trackables, oldStops));
        }
        else
        {
            String stopId = String.valueOf( stopID );
            String footprintId = String.valueOf( OnTrackManager.getInstance( ).getUser( ).getFootprint( ) );
            String type = "1";
            ChangeStopTrackablesTask changeStopTrackablesTask = new ChangeStopTrackablesTask( this );
            changeStopTrackablesTask.execute( WebServices.URL_CHANGE_STOP_TRACKABLES, ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance( ).getToken( ), ServiceKeys.TRACKABLE_CHANGES_KEY, JSONManager.getJSONForTrackablesStopChange( stopId, footprintId, type, trackables ) );
//            RemoveTrackableFromStopTask removeTrackableFromStopTask = new RemoveTrackableFromStopTask(this, stopID, trackables);
//            removeTrackableFromStopTask.execute(WebServices.URL_REMOVE_TRACKABLE_FROM_STOP, ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance().getToken(), ServiceKeys.ID_STOP_KEY, String.valueOf(stopID), Trackable.TRACKABLES_KEY,
//                    JSONManager.getJSONForTrackablesStopRemoval(trackables));
        }
    }

    public ArrayList<Portion> getPortions() {
        return portions;
    }

    public void updateStopListFragmentBar() {
        ((StopListFragment) stopListFragment).renderCurrentPageStopIndicators();
    }

    public void updateStopListFragmentAddButton(Stop stop) {
        ((StopListFragment) stopListFragment).addButton(stop);
    }

    public void updateStopListFragmentOrganizeButtons() {
        ((StopListFragment) stopListFragment).organizeButtons();
    }

    public void updateStopDetailsFragmentRefreshTrackables() {
        ((StopDetailsFragment) stopDetailsFragment).updateTrackables();
    }

    @Override
    public void logOut() {
        CloseSessionManager.executeCloseSessionTask(
                OnTrackManagerAsyncTasks.URL_BASE_CLOSE_SESSION,
                ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance().getToken(),
                ServiceKeys.USER_DEVICE_ID_KEY, ""
                        + OnTrackManager.getInstance().getUserDeviceId());

    }

    @Override
    public void onSessionClosedInServer() {
        SharedPreferences prefs = getSharedPreferences(
                PreferenceKeys.PREFERENCES_KEY, Context.MODE_PRIVATE);
        OnTrackManager.getInstance().resetRoute();
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(PreferenceKeys.TOKEN_KEY);
        editor.remove(PreferenceKeys.USER_DEVICE_ID_KEY);
        editor.commit();
        Intent i = new Intent(MainActivity.this, Login.class);
        startActivity(i);
        finish();

    }


    public void setPointsToRefuge(ArrayList<LatLng> pointsToRefuge) {
        this.pointsToRefuge = pointsToRefuge;
    }

    public void drawRouteToRefuge() {
//        if (line != null) {
//            mMap.getOverlays().remove(line);
//        }
//
//        line = new PathOverlay(getResources().getColor(android.R.color.holo_green_dark), 4);
//


//        for (LatLng latLng : pointsToRefuge) {
//            line.addPoint(latLng);
//        }

        if(pointsToRefuge.size()>0) {
            mMap.addPolyline(new PolylineOptions().add(pointsToRefuge.toArray(new LatLng[pointsToRefuge.size()])).color(Color.parseColor("#669900")).width(8));
        }
//
//        mMap.getOverlays().add(line);
    }

    @Override
    public void onDisconnectLocation() {
//		if (mLocationClient.isConnected()) {
//			/*
//			 * Remove location updates for a listener. The current Activity is
//			 * the listener, so the argument is "this".
//			 */
//			LocationServices.FusedLocationApi.removeLocationUpdates(
//					mLocationClient, this);
//		}
//		/*
//		 * After disconnect() is called, the client is considered "dead".
//		 */
//		mLocationClient.disconnect();

    }

    @Override
    public void onServiceLocationChanged(Location location) {
        boolean isWaitingGPS = OnTrackManager.getInstance().isWaitingGPS();
        boolean isRouteDetailsLoaded = OnTrackManager.getInstance().isRouteDetailsLoaded();


        if (OnTrackManager.getInstance().isWaitingGPS() && OnTrackManager.getInstance().isRouteDetailsLoaded()) {
            if (!startAutomatically) {
                ((StartRouteFragment) startRouteFragment).hideProgressBar();
                ((StartRouteFragment) startRouteFragment)
                        .setFragmentTest(getResources().getString(R.string.start_route));
                ((StartRouteFragment) startRouteFragment).setButtonEnabled(true);
                OnTrackManager.getInstance().setWaitingGPS(false);
            } else {
                startRoute();
                OnTrackManager.getInstance().setWaitingGPS(false);
            }
//            OnTrackManager.getInstance().setWaitingGPS(false);
        }

        if (!OnTrackManager.getInstance().isSimulating()) {
            if (OnTrackManager.getInstance().isRouteStarting() && location != null) {
                processLocation(location);
            }
        }

    }


    public double getTopLatitude( ArrayList<Stop> stops )
    {
        double latitude = -Double.MAX_VALUE;

        for( Stop stop : stops )
        {
            if( stop.getLatitude() > latitude )
            {
                latitude = stop.getLatitude( );
            }
        }

        return latitude +  0.005;
    }

    public double getBottomLatitude( ArrayList<Stop> stops )
    {
        double latitude = Double.MAX_VALUE;

        for( Stop stop: stops )
        {
            if( stop.getLatitude() < latitude )
            {
                latitude = stop.getLatitude();
            }
        }

        return latitude - 0.005;
    }

    public double getRightLongitude(ArrayList<Stop> stops)
    {
        double longitude = -Double.MAX_VALUE;

        for( Stop stop : stops )
        {
            if( stop.getLongitude() > longitude )
            {
                longitude = stop.getLongitude();
            }
        }

        return longitude +   0.005;
    }

    public double getLeftLongitude( ArrayList<Stop> stops )
    {
        double longitude = Double.MAX_VALUE;

        for( Stop stop: stops)
        {
            if( stop.getLongitude() < longitude )
            {
                longitude = stop.getLongitude();
            }
        }

        return longitude -   0.005;
    }

    public boolean isPrestartListSelected() {
        return prestartListSelected;
    }

    public void setPrestartListSelected(boolean prestartListSelected) {
        this.prestartListSelected = prestartListSelected;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mMap.onSaveInstanceState(outState);
    }
}