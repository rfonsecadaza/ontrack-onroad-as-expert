package otr.ontrack_onroad_debug.activities;

import java.util.ArrayList;

import otr.ontrack_onroad_debug.activities.R;
import otr.ontrack_onroad.adapters.ProfileListAdapter;
import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.models.Stop;
import otr.ontrack_onroad.models.Trackable;
import otr.ontrack_onroad.models.User;
import otr.ontrack_onroad.parameters.Parameters;
import otr.ontrack_onroad.utils.RoundedImageView;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.nostra13.universalimageloader.core.ImageLoader;

public class TrackableProfile extends ActionBarActivity {

//	private static GoogleMap mMap;
	private static RoundedImageView ivUserImage;
	private static TextView tvUser;
	private static TextView tvStopNumber;
	private static ListView lvPeopleInCharge;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_trackable_profile);
		this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		// Obtener la información del trackable seleccionado
		Trackable trackable = OnTrackManager.getInstance()
				.getSelectedTrackableAsEntity();
		Stop stop = OnTrackManager.getInstance()
				.getStopOfSelectedTrackable();
		String fullName = trackable.getFullname();
		ArrayList<User> peopleInCharge = trackable.getUsers();


//			mMap = ((MapFragment) getActivity().getFragmentManager()
//					.findFragmentById(R.id.map)).getMap();
		LatLng latLng = new LatLng(stop.getLatitude(), stop.getLongitude());
//			Marker marker = mMap.addMarker(new MarkerOptions()
//					.position(latLng)
//					.title(Parameters.STOP_MARKER_TITLE)
//					.icon(BitmapDescriptorFactory
//							.fromResource(R.drawable.stop_marker)));
//			mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 19));

		ivUserImage = (RoundedImageView) findViewById(R.id.rivUserImage);
		ImageLoader.getInstance( ).displayImage( trackable.getImageURL( ), ivUserImage );
		tvUser = (TextView)findViewById(R.id.tvUser);
		tvUser.setText(fullName);
		tvStopNumber = (TextView)findViewById(R.id.tvStopNumber);
		tvStopNumber.setText(Parameters.STOP_MARKER_TITLE+" "+stop.getOrder());
		lvPeopleInCharge = (ListView) findViewById(R.id.lvPeopleInCharge);
		lvPeopleInCharge.setAdapter(new ProfileListAdapter(this,
				peopleInCharge));
	}

//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.trackable_profile, menu);
//		return true;
//	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == android.R.id.home) {
			finish();
		}
		return super.onOptionsItemSelected(item);
	}


}