package otr.ontrack_onroad_debug.activities;

import otr.ontrack_onroad_debug.activities.R;
import otr.ontrack_onroad.adapters.ProfileListAdapter;
import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.parameters.GeneralMessages;

import android.app.Fragment;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.model.Polyline;

public class RouteProfile extends ActionBarActivity {

	private static Polyline line;

//	private static GoogleMap mMap;
	private static ListView lvPeopleInCharge;
	private static TextView tvRouteNameProfile;
	private static TextView tvNumberOfStudents;
	private static TextView tvNumberOfStops;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_route_profile);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.route_profile, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.

		int id = item.getItemId();
		if (id == R.id.action_changeroute) {
			Intent i = new Intent(this, RouteList.class);
			startActivity(i);
		} else if (id == android.R.id.home) {
			finish();
		}

		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView;
			if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
				rootView = inflater.inflate(
						R.layout.fragment_route_profile_portrait, container,
						false);
			else
				rootView = inflater.inflate(R.layout.fragment_route_profile,
						container, false);

//			mMap = ((MapFragment) getActivity().getFragmentManager()
//					.findFragmentById(R.id.map)).getMap();
//
//			LatLngBounds.Builder builder = new LatLngBounds.Builder();
//			ArrayList<Stop> stops = OnTrackManager.getInstance().getUser()
//					.getRoute().getStops();
//
//			for (Stop stop : stops) {
//				LatLng latLng = new LatLng(stop
//						.getLatitude(),
//						stop.getLongitude());
//
//				Marker marker = mMap.addMarker(new MarkerOptions()
//						.position(latLng)
//						.title(Parameters.STOP_MARKER_TITLE)
//						.icon(BitmapDescriptorFactory
//								.fromResource(R.drawable.stop_marker)));
//
//				builder.include(marker.getPosition());
//
//			}
//
//			LatLngBounds bounds = builder.build();
//			int padding = 0; // offset from edges of the map in pixels
//			int boundingBoxWidth = 120;
//			int boundingBoxHeight = 120;
//			CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds,
//					boundingBoxWidth, boundingBoxHeight, padding);
//
//			mMap.animateCamera(cu);
//
//			// Dibuja la ruta únicamente si ya fue cargada
//			ArrayList<Point> points = OnTrackManager.getInstance().getUser()
//					.getRoute().getPoints();
//
//			for (int z = 0; z < points.size() - 1; z++) {
//				Point src = points.get(z);
//				Point dest = points.get(z + 1);
//				line = mMap.addPolyline(new PolylineOptions()
//						.add(new LatLng(src.getLatitude(),
//								src.getLongitude()),
//								new LatLng(dest
//										.getLatitude(), dest.getLongitude()))
//						.width(5).color(Color.RED).geodesic(true));
//
//			}

			lvPeopleInCharge = (ListView) rootView
					.findViewById(R.id.lvPeopleInCharge);
			lvPeopleInCharge.setAdapter(new ProfileListAdapter(getActivity(),
					OnTrackManager.getInstance().getUser()
							.peopleInChargeOfRoute()));

			tvRouteNameProfile = (TextView) rootView
					.findViewById(R.id.tvRouteNameProfile);
			tvRouteNameProfile.setText(OnTrackManager.getInstance().getUser()
					.getRoute().getIdWithoutSuffix());

			tvNumberOfStops = (TextView) rootView
					.findViewById(R.id.tvNumberOfStops);
			tvNumberOfStops.setText(OnTrackManager.getInstance().getUser()
					.getRoute().numberOfStops() != 1 ? ""
					+ OnTrackManager.getInstance().getUser().getRoute()
							.numberOfStops() + " "
					+ GeneralMessages.STOP_PLURAL : ""
					+ OnTrackManager.getInstance().getUser().getRoute()
							.numberOfStops() + " "
					+ GeneralMessages.STOP_SINGULAR);

			tvNumberOfStudents = (TextView) rootView.findViewById(R.id.tvRole);
			tvNumberOfStudents.setText(OnTrackManager.getInstance().getUser()
					.getRoute().numberOfStudents() != 1 ? ""
					+ OnTrackManager.getInstance().getUser().getRoute()
							.numberOfStudents() + " "
					+ GeneralMessages.TRACKABLE_TAG_PLURAL : ""
					+ OnTrackManager.getInstance().getUser().getRoute()
							.numberOfStudents() + " "
					+ GeneralMessages.TRACKABLE_TAG_SINGULAR);
			return rootView;
		}
	}

}
