package otr.ontrack_onroad_debug.activities;

import java.util.ArrayList;

import otr.ontrack_onroad_debug.activities.R;
import otr.ontrack_onroad.adapters.EventListAdapter;
import otr.ontrack_onroad.dao.Event;
import otr.ontrack_onroad.managers.EventManager;
import otr.ontrack_onroad.managers.EventManagerListener;
import otr.ontrack_onroad.managers.MessageManager;
import otr.ontrack_onroad.managers.MessageManagerListener;
import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.models.Report;
import otr.ontrack_onroad.parameters.GeneralMessages;
import otr.ontrack_onroad.parameters.Parameters;

import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class EventList extends ActionBarActivity implements
		OnItemClickListener, MessageManagerListener, EventManagerListener {

	private static ListView lvEvents;
	private static ImageView ivEventIcon;
	private static TextView tvEventCategory;
	private static TextView tvEventDate;
	private static TextView tvEventDetails;

	private static EventListAdapter eventsAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_event_list);

		this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}

		MessageManager.getInstance().addListener(this);

		// La actividad actual se convierte en un "Listener" de EventManager
		EventManager.getInstance().addListener(this);
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.event_list, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		} else if (id == android.R.id.home) {
			finish();
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_event_list,
					container, false);

			lvEvents = (ListView) rootView.findViewById(R.id.lvEvents);

			eventsAdapter = new EventListAdapter(getActivity(), OnTrackManager
					.getInstance().getEvents());

			lvEvents.setAdapter(eventsAdapter);
			lvEvents.setOnItemClickListener((OnItemClickListener) getActivity());

			ivEventIcon = (ImageView) rootView.findViewById(R.id.ivEventIcon);

			
			Typeface tf = Typeface.createFromAsset(getActivity().getAssets(),
					Parameters.FONT_TITLE);
			
			tvEventCategory = (TextView) rootView
					.findViewById(R.id.tvEventCategory);
			tvEventCategory.setTypeface(tf);
			tvEventDate = (TextView) rootView.findViewById(R.id.tvEventDate);
			tvEventDetails = (TextView) rootView
					.findViewById(R.id.tvEventDetails);
			
			long eventId = getActivity().getIntent().getLongExtra("event_id", -1);
			if(eventId != -1){
				Event event = OnTrackManager.getInstance().getEventFromLocalDB(eventId);
				tvEventDate.setText(event.getDate());
				
				if (event.getFkRoute() == "-1") {
					tvEventDetails.setText(event.getDescription());
				} else {
					tvEventDetails.setText(GeneralMessages.ROUTE + " "
							+ event.getFkRoute() + ": " + event.getDescription());
				}

				int drawableId = Event.getImageIdFromCategory(event.getCategory());
				if (drawableId != -1) {
					ivEventIcon.setImageResource(drawableId);
				}

				String categoryText = Event.getTextFromCategory(event.getCategory());
				if (categoryText != null) {
					tvEventCategory.setText(categoryText);
				} else {
					tvEventCategory.setText(GeneralMessages.EVENT_CATEGORY_UNKNOWN);
				}
			}

			return rootView;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		Event event = OnTrackManager.getInstance().getEventFromLocalDB(id);
		tvEventDate.setText(event.getDate());
		if (event.getFkRoute() == "-1") {
			tvEventDetails.setText(event.getDescription());
		} else {
			tvEventDetails.setText(GeneralMessages.ROUTE + " "
					+ event.getFkRoute() + ": " + event.getDescription());
		}

		int drawableId = Event.getImageIdFromCategory(event.getCategory());
		if (drawableId != -1) {
			ivEventIcon.setImageResource(drawableId);
		}

		String categoryText = Event.getTextFromCategory(event.getCategory());
		if (categoryText != null) {
			tvEventCategory.setText(categoryText);
		} else {
			tvEventCategory.setText(GeneralMessages.EVENT_CATEGORY_UNKNOWN);
		}

	}

	@Override
	public void onReceiveMessage(ArrayList<Long> idsOfModifiedChatCards) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onReceivePusherEvent(Event mEvent) {
		final Event event = mEvent;
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				eventsAdapter.notifyDataSetChanged();
					tvEventDate.setText(event.getDate());
					
					if (event.getFkRoute() == "-1") {
						tvEventDetails.setText(event.getDescription());
					} else {
						tvEventDetails.setText(GeneralMessages.ROUTE + " "
								+ event.getFkRoute() + ": " + event.getDescription());
					}

					int drawableId = Event.getImageIdFromCategory(event
							.getCategory());
					if (drawableId != -1) {
						ivEventIcon.setImageResource(drawableId);
					}

					String categoryText = Event.getTextFromCategory(event
							.getCategory());
					if (categoryText != null) {
						tvEventCategory.setText(categoryText);
					} else {
						tvEventCategory
								.setText(GeneralMessages.EVENT_CATEGORY_UNKNOWN);
					}
			}
		});

	}

	@Override
	public void onReceivePusherReport(Report report) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onEventGenerated(long eventId) {
		final long id = eventId;
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				eventsAdapter.notifyDataSetChanged();
				if (id != -1) {
					Event event = OnTrackManager.getInstance()
							.getEventFromLocalDB(id);
					tvEventDate.setText(event.getDate());
					tvEventDetails.setText(event.getDescription());

					int drawableId = Event.getImageIdFromCategory(event
							.getCategory());
					if (drawableId != -1) {
						ivEventIcon.setImageResource(drawableId);
					}

					String categoryText = Event.getTextFromCategory(event
							.getCategory());
					if (categoryText != null) {
						tvEventCategory.setText(categoryText);
					} else {
						tvEventCategory
								.setText(GeneralMessages.EVENT_CATEGORY_UNKNOWN);
					}
				}
			}
		});

	}

	@Override
	public void onMessagesRead(long chatCardId) {
		// TODO Auto-generated method stub
		
	}
}
