package otr.ontrack_onroad_debug.activities;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;

import otr.ontrack_onroad_debug.activities.R;
import otr.ontrack_onroad.adapters.MessageAdapter;
import otr.ontrack_onroad.dao.ChatCard;
import otr.ontrack_onroad.dao.ChatUser;
import otr.ontrack_onroad.dao.Event;
import otr.ontrack_onroad.dao.Message;
import otr.ontrack_onroad.managers.MessageManager;
import otr.ontrack_onroad.managers.MessageManagerListener;
import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.models.Report;
import otr.ontrack_onroad.models.User;
import otr.ontrack_onroad.parameters.GeneralMessages;
import otr.ontrack_onroad.parameters.ServiceKeys;
import otr.ontrack_onroad.utils.Timestamp;
import otr.ontrack_onroad.utils.WebRequestManager;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

public class Chat extends ActionBarActivity implements MessageManagerListener {

	private static ListView lvChat;
	private static EditText etMessage;
	private static List<Message> messages;
	private static MessageAdapter messageAdapter;

	// private ArrayList<Message> messages;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_chat);

		MessageManager.getInstance().addListener(this);

		// ChatCard chatCard = MessageManager.getInstance()
		// .getSelectedChatCardEntity();

		ChatCard chatCard = MessageManager.getInstance()
				.getSelectedChatCardEntity();

		// Deja todos los mensajes de este chat como leï¿½dos
		OnTrackManager.getInstance().setAllMessagesAsRead(chatCard.getId());

		// Reporta que los mensajes estï¿½n leidos a las entidades interesadas
		MessageManager.getInstance().reportMessagesRead(chatCard.getId());

		// String title = chatCard.getName() == null ?
		// chatCard.getUsers().get(0)
		// .getFullName() : chatCard.getName();
		String title = chatCard.getName();
		this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		this.getSupportActionBar().setTitle(title);

		messages = chatCard.getMessages();
		messageAdapter = new MessageAdapter(this, messages);

		lvChat = (ListView) findViewById(R.id.lvChat);
		lvChat.setAdapter(messageAdapter);
		etMessage = (EditText) findViewById(R.id.etMessage);

		lvChat.setSelection(messages.size() - 1);

	}

//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.chat, menu);
//		return true;
//	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_changeroute) {
			return true;
		} else if (id == android.R.id.home) {
			if (OnTrackManager.getInstance().getSelectedRoute() == -1) {
				Intent upIntent = new Intent(this, ChatList.class);
				startActivity(upIntent);
				finish();
			} else {
				finish();
			}
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void sendMessage(View v) {
		if (!etMessage.getText().toString().isEmpty()) {
			// Message message = new Message(etMessage.getText().toString(),
			// Timestamp.completeTimestamp(), false, new ChatUser(
			// OnTrackSystem.getInstance().getUser(), null));

			// Primero se revisa si hay algï¿½n usuario a nivel de base de datos
			// local que sea "yo" mismo.
			// Si no existe, lo crea
			User user = OnTrackManager.getInstance().getUser();

			ChatUser chatUser = OnTrackManager.getInstance()
					.getChatUserFromLocalDB(user.getId());

			if (chatUser == null) {
				chatUser = new ChatUser(null, user.getId(),
						user.getFirstName(), user.getLastName(),
						user.getContactAddress(), user.getMobilePhone(),
						user.getLanPhone(), user.getRole(), null);
				OnTrackManager.getInstance().insertChatUserToLocalDB(chatUser);
			}

			Message message = new Message(null, etMessage.getText().toString(),
					Timestamp.completeTimestamp(), false, true,
					chatUser.getId(), MessageManager.getInstance()
							.getSelectedChatCardEntity().getId());
			OnTrackManager.getInstance().insertMessageToLocalDB(message);
			addNewMessage(message);

			etMessage.setText("");
			int messageIndex = messageAdapter.getCount() - 1;
			SendSingleMessageTask task = new SendSingleMessageTask(messageIndex);
			try {
				task.execute(
						MessageManager.URL_BASE_SEND_MESSAGES,
						ServiceKeys.TOKEN_KEY,
						OnTrackManager.getInstance().getToken(),
						ServiceKeys.CHATCARDS_KEY,
						MessageManager
								.getInstance()
								.getMessagesToSendFromSelectedChatCardJSONString(
										messageIndex));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private class SendSingleMessageTask extends
			AsyncTask<String, Integer, String> {

		private int sentMessageIndex;

		public SendSingleMessageTask(int sentMessageIndex) {
			this.sentMessageIndex = sentMessageIndex;
		}

		@Override
		protected String doInBackground(String... args) {
			String response = null;
			try {
				response = WebRequestManager.executeTaskGet(args);
			} catch (IOException e) {
				response = GeneralMessages.HTTP_NOT_FOUND_ERROR;
			}

			return response;
		}

		@Override
		protected void onPostExecute(String result) {
//			Log.d("chat", result);
			if (result.equals(GeneralMessages.JSON_ERROR)
					|| result.equals(GeneralMessages.HTTP_CONNECTION_ERROR)
					|| result.equals(GeneralMessages.HTTP_NOT_FOUND_ERROR)) {
			} else {
				Message message = MessageManager.getInstance()
						.getSelectedChatCardEntity().getMessages()
						.get(sentMessageIndex);
				message.setSent(true);
				OnTrackManager.getInstance().updateMessageInLocalDB(message);
				messageAdapter.notifyDataSetChanged();
			}

			// lvChat.setSelection(messageAdapter.getCount()-1);
		}

		public int getSentMessageIndex() {
			return sentMessageIndex;
		}

		public void setSentMessageIndex(int sentMessageIndex) {
			this.sentMessageIndex = sentMessageIndex;
		}

	}

	void addNewMessage(Message m) {
		messages.add(m);
		messageAdapter.notifyDataSetChanged();
		lvChat.setSelection(messages.size() - 1);
	}

	@Override
	public void onReceiveMessage(ArrayList<Long> idsOfModifiedChatCards) {
		for (Long id : idsOfModifiedChatCards) {
			if (id.equals(MessageManager.getInstance().getSelectedChatCardEntity()
					.getServerId())) {
				
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						messageAdapter.notifyDataSetChanged();
						lvChat.setSelection(messages.size() - 1);
						// Dejar el ï¿½ltimo mensaje que llegï¿½ como leido, porque
						// estï¿½ abierta la ventana del chat
						OnTrackManager
								.getInstance()
								.getMessageDao()
								.update((Message) messageAdapter
										.getItem(messages.size() - 1));
					}
				});

			}
		}
	}

	@Override
	public void onReceivePusherEvent(Event event) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onReceivePusherReport(Report report) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onMessagesRead(long chatCardId) {
		// TODO Auto-generated method stub

	}

}

