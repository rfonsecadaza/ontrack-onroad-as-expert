package otr.ontrack_onroad_debug.activities;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

import org.json.JSONException;
import org.json.JSONObject;

import otr.ontrack_onroad_debug.activities.R;
import otr.ontrack_onroad.managers.AuthenticationManager;
import otr.ontrack_onroad.managers.AuthenticationManagerListener;
import otr.ontrack_onroad.managers.LoginManager;
import otr.ontrack_onroad.managers.LoginManagerListener;
import otr.ontrack_onroad.managers.NotificationProtocolManager;
import otr.ontrack_onroad.managers.NotificationProtocolManagerListener;
import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.parameters.GeneralMessages;
import otr.ontrack_onroad.parameters.Parameters;
import otr.ontrack_onroad.parameters.PreferenceKeys;
import otr.ontrack_onroad.parameters.ServiceKeys;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBarActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import android.Manifest;

public class Login extends ActionBarActivity implements OnClickListener,
        AuthenticationManagerListener, LoginManagerListener,
        NotificationProtocolManagerListener {

    // Constantes para permisos de Marshmallow
    private final static int READ_PHONE_STATE = 0;

    private final static int ACCESS_FINE_LOCATION = 1;

    private final static int WRITE_EXTERNAL_STORAGE = 2;
    //


    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public static final String URL_BASE_CHATCARDS = "ApiMessage/getDirectory";

    TextView mDisplay;
    GoogleCloudMessaging gcm;
    AtomicInteger msgId = new AtomicInteger();
    SharedPreferences prefs;
    Context context;

    String registrationId;

    private ProgressDialog mProgressDialog;
    public static final String URL_AUTHENTICATE = "ApiAuthentication/authenticate";

    private boolean isRequestingNewActivationCode = false;

    private static EditText etUser;
    private static EditText etPassword;
    private static Button bLogin;

    private String status = null;
    private String error = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_login);
        context = this;

        prefs = getGCMPreferences(context);
        // SharedPreferences.Editor editor = prefs.edit();
        // editor.clear();
        // editor.commit();

        // Esta actividad se vuelve un listener de
        // NotificationProtocolManagerListener
        // POR AHORA NO LO VOY A EJECUTAR, ESTA PARTE ESTÁ PENDIENTE DE CAMBIOS
//		NotificationProtocolManager.getInstance().addListener(this);

        AuthenticationManager.getInstance().setListener(this);
        LoginManager.getInstance().setListener(this);

        etUser = (EditText) findViewById(R.id.etUser);
        etPassword = (EditText) findViewById(R.id.etPassword);

        bLogin = (Button) findViewById(R.id.bLogin);
        bLogin.setOnClickListener(this);

        if (checkPlayServices()) {
            gcm = GoogleCloudMessaging.getInstance(this);
            registrationId = getRegistrationId(context);

            if (registrationId.isEmpty()) {
                registerInBackground();
            }
        } else {
            Log.i(Parameters.GCM_MESSAGE_TAG, GeneralMessages.GCM_NO_VALID_APK);
        }

        mProgressDialog = new ProgressDialog(this);

        mProgressDialog.setMessage("Un momento por favor...");
        mProgressDialog.setCancelable(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_changeroute) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.bLogin:
                if( ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED )
                {
                    ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_PHONE_STATE}, READ_PHONE_STATE);
                }
                else if( ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED )
                {
                    ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, ACCESS_FINE_LOCATION);
                }
                else
                {
                    login( );
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult( int requestCode, String permissions[], int[] grantResults )
    {
        switch( requestCode )
        {
            case READ_PHONE_STATE:
            {
                if( grantResults.length > 0 && grantResults[ 0 ] == PackageManager.PERMISSION_GRANTED )
                {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, ACCESS_FINE_LOCATION);
                }
                else
                {
                    Toast.makeText(this,"Lamentablemente sin este permiso la aplicación no puede funcionar", Toast.LENGTH_LONG).show();
                }
                break;
            }
            case ACCESS_FINE_LOCATION:
            {
                if( grantResults.length > 0 && grantResults[ 0 ] == PackageManager.PERMISSION_GRANTED )
                {
                    ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, WRITE_EXTERNAL_STORAGE);
                }
                else
                {
                    Toast.makeText(this, "Lamentablemente sin este permiso la aplicación no puede funcionar", Toast.LENGTH_LONG).show();
                }
                break;
            }
            case WRITE_EXTERNAL_STORAGE:
            {
                login( );
                break;
            }
            default:
            {
                break;
            }
        }
    }

    public void login(){
        // El botón de login hace la solicitud http para validar la
        // autenticación, utilizando el AsyncTask "CallWebServiceTask"
        OnTrackManager.getInstance().setUserString(
                etUser.getText().toString());

        String model = Build.MANUFACTURER + " " + Build.MODEL;

        // Obtención del código IMEI
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        String imeiCode = telephonyManager.getDeviceId();
//                String imeiCode ="bla";

        // Crear un JSON que contenga los datos del dispositivo
        JSONObject deviceObject = new JSONObject();


        try {
            deviceObject.put(ServiceKeys.BRAND_KEY,
                    Parameters.BRAND_ANDROID); // 0 por android
            deviceObject.put(ServiceKeys.MODEL_KEY, model);
            deviceObject.put(ServiceKeys.DEVICE_ID_KEY, imeiCode);
            deviceObject.put(ServiceKeys.DEVICE_TOKEN_KEY, registrationId);
            LoginManager.getInstance().executeLoginTask(URL_AUTHENTICATE,
                    ServiceKeys.ID_USER_KEY, etUser.getText().toString(),
                    ServiceKeys.PASSWORD_KEY,
                    etPassword.getText().toString(), ServiceKeys.TYPE_KEY,
                    Parameters.USER_TYPE_DRIVER, ServiceKeys.DEVICE_KEY,
                    deviceObject.toString());
        } catch (JSONException e) {
            Toast.makeText(this, GeneralMessages.JSON_ERROR,
                    Toast.LENGTH_LONG).show();
        }

    }

    /*********************
     * RECEPCIÓN DE NOTIFICACIONES PUSH
     ****************/

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(Parameters.GCM_MESSAGE_TAG,
                        GeneralMessages.GCM_NOT_COMPATIBLE);
                finish();
            }
            return false;
        }
        return true;
    }

    /**
     * Obtiene el id de registro para uso en GCM
     * <p/>
     * Si el resultado es vacío, debe hacer el registro
     *
     * @return registration ID, o cadena vacía en caso de que no haya ID de
     * registro
     */
    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        String registrationId = prefs.getString(
                PreferenceKeys.PROPERTY_REG_ID_KEY, "");
        if (registrationId.isEmpty()) {
            Log.i(Parameters.GCM_MESSAGE_TAG,
                    GeneralMessages.GCM_REG_ID_NOT_FOUND);
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = prefs.getInt(
                PreferenceKeys.PROPERTY_APP_VERSION_KEY, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i(Parameters.GCM_MESSAGE_TAG,
                    GeneralMessages.GCM_APP_VERSION_CHANGED);
            return "";
        }
        return registrationId;
    }

    private SharedPreferences getGCMPreferences(Context context) {
        return getSharedPreferences(PreferenceKeys.PREFERENCES_KEY,
                Context.MODE_PRIVATE);
    }

    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (NameNotFoundException e) {
            // should never happen
            throw new RuntimeException(GeneralMessages.GCM_PACKAGE_NOT_FOUND
                    + e);
        }
    }

    /**
     * Registra la aplicación en los servidores GCM
     * <p/>
     * Almacena el id de registro en SharedPreferences
     */
    private void registerInBackground() {
        new AsyncTask<Void, Integer, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(context);
                    }
                    registrationId = gcm.register(Parameters.GCM_SENDER_ID);
                    msg = GeneralMessages.GCM_DEVICE_REGISTERED;

                    // Guardar el id de registro en el dispositivo
                    storeRegistrationId(context, registrationId);

                } catch (IOException ex) {
                    msg = ex.getMessage();
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                Toast.makeText(Login.this, msg, Toast.LENGTH_LONG).show();
            }

        }.execute(null, null, null);

    }

    /**
     * Almacena el id de registro y la versión de la aplicación en los
     * SharedPreferences
     *
     * @param context contexto de la aplicación
     * @param regId   registration ID
     */
    private void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGCMPreferences(context);
        int appVersion = getAppVersion(context);

        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PreferenceKeys.PROPERTY_REG_ID_KEY, regId);
        editor.putInt(PreferenceKeys.PROPERTY_APP_VERSION_KEY, appVersion);
        editor.commit();
    }

    public void forgotPasswordAction(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Recuperación de contraseña").setMessage("Si selecciona la opción 'Recordar contraseña por correo', no podrá usar la aplicación hasta que la contraseña sea reestablecida. ¿Está seguro de que quiere continuar?")
                .setPositiveButton("Recordar contraseña por correo", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (!isRequestingNewActivationCode) {
                            isRequestingNewActivationCode = true;
                            // ForgotPasswordTask task = new ForgotPasswordTask();
                            // task.execute("ApiAuthentication/forgotPassword", "ID", etUser
                            // .getText().toString());
                            LoginManager.getInstance().executeForgotPasswordTask(
                                    "ApiAuthentication/forgotPassword", "ID",
                                    etUser.getText().toString());

                        }
                        dialog.dismiss();
                    }
                }).setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.show();

    }

    @Override
    public void onRoutePreloaded() {
        // TODO Auto-generated method stub

    }

    @Override
    public void onRouteLoaded() {
        AuthenticationManager.getInstance().executeRequestChatcardsTask(
                URL_BASE_CHATCARDS, ServiceKeys.TOKEN_KEY,
                OnTrackManager.getInstance().getToken());

    }

    @Override
    public void onRouteNotLoaded() {
        // TODO Auto-generated method stub

    }

    @Override
    public void onAuthenticationError(String error) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onPrelogin() {
        runOnUiThread(new Runnable() {
            public void run() {
                mProgressDialog.show();
            }
        });
    }

    @Override
    public void onLogin() {
//		LoginManager.getInstance().removeListener(this);
        String token = prefs.getString(PreferenceKeys.TOKEN_KEY, null);
        AuthenticationManager.getInstance().executeLoadRouteTask(
                AuthenticationManager.URL_BASE_DRIVER, ServiceKeys.TOKEN_KEY,
                token);

    }

    @Override
    public void onNotLogin() {
        mProgressDialog.dismiss();
        AlertDialog loginIncorrectDialog = new AlertDialog.Builder(Login.this)
                .create();
        loginIncorrectDialog.setTitle(GeneralMessages.DIALOG_TITLE_ERROR);
        loginIncorrectDialog.setMessage(GeneralMessages.WRONG_LOGIN);
        loginIncorrectDialog.setButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

        loginIncorrectDialog.show();

    }

    @Override
    public void onLoginError(String error) {
        mProgressDialog.dismiss();
        AlertDialog loginIncorrectDialog = new AlertDialog.Builder(Login.this)
                .create();
        loginIncorrectDialog.setTitle(GeneralMessages.DIALOG_TITLE_ERROR);
        loginIncorrectDialog.setMessage(error);
        loginIncorrectDialog.setButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

        loginIncorrectDialog.show();

    }

    @Override
    public void onForgotPasswordRequest(String message) {
        mProgressDialog.dismiss();
        isRequestingNewActivationCode = false;
        AlertDialog loginIncorrectDialog = new AlertDialog.Builder(Login.this)
                .create();
        loginIncorrectDialog.setTitle("Atención");
        loginIncorrectDialog.setMessage(message);
        loginIncorrectDialog.setButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

        loginIncorrectDialog.show();

    }

    @Override
    public void onProtocolExecuted() {
        NotificationProtocolManager.getInstance().removeListener(this);

    }

    @Override
    public void onChatcardsLoaded() {
//		try {
//			String savedNotificationsJSONString = OnTrackManager.getInstance()
//					.getSavedNotificationsJSONString();
//			if (savedNotificationsJSONString == null) {
//				// Si no hay notificaciones guardadas, pasar
//				// directamente a revisar notificaciones nuevas
//				NotificationProtocolManager
//						.executeRequestNewNotificationsTask();
//
//			} else {
//				// Ejecutar el protocolo normal de notificaciones
//				NotificationProtocolManager.executeNotificationProtocolTask(
//						NotificationProtocolManager.URL_BASE_ACK_NOTIFICATIONS,
//						ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance()
//								.getToken(), ServiceKeys.NOTIFICATIONS_KEY,
//						savedNotificationsJSONString);
//			}
//		} catch (JSONException e) {
//			e.printStackTrace();
//		}
//		
//		AuthenticationManager.getInstance().removeListener(this);
        Intent i = new Intent(Login.this, RouteList.class);
        startActivity(i);
        finish();


    }

    @Override
    public void onChatcardsError(String message) {
        // TODO Auto-generated method stub

    }

}

