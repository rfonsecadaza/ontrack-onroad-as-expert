package otr.ontrack_onroad_debug.activities;

import otr.ontrack_onroad_debug.activities.R;
import otr.ontrack_onroad.adapters.AssistanceStudentListAdapter;
import otr.ontrack_onroad.adapters.AssistanceStudentListItem;
import otr.ontrack_onroad.managers.OnTrackManager;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class TrackableList extends ActionBarActivity {

	private static ListView lvAllTrackables;
	private static AssistanceStudentListAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_trackable_list);
		this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.trackable_list, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == android.R.id.home) {
			finish();
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_alltrackables,
					container, false);
			lvAllTrackables = (ListView) rootView
					.findViewById(R.id.lvAllTrackables);

			adapter = new AssistanceStudentListAdapter(getActivity(),
					OnTrackManager.getInstance().getUser().getRoute()
							.getAllTrackablesWithStopHeaders());
			lvAllTrackables.setAdapter(adapter);

			final Intent i = new Intent(getActivity(), TrackableProfile.class);
			lvAllTrackables.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					if (!(((AssistanceStudentListItem) adapter.getItem(position))
							.isSection())) {
						OnTrackManager.getInstance().setSelectedTrackable(
								position);
						startActivity(i);
					}
				}
			});
			return rootView;
		}
	}
}
