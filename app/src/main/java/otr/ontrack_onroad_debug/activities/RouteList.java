package otr.ontrack_onroad_debug.activities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import otr.ontrack_onroad.dao.Message;
import otr.ontrack_onroad.fragments.EventFragment;
import otr.ontrack_onroad.fragments.ReplacementDialogFragment;
import otr.ontrack_onroad_debug.activities.R;
import otr.ontrack_onroad.adapters.RouteListAdapter;
import otr.ontrack_onroad.adapters.SidebarSoftAdapter;
import otr.ontrack_onroad.dao.Event;
import otr.ontrack_onroad.dao.Simulation;
import otr.ontrack_onroad.fragments.CloseSessionDialogFragment;
import otr.ontrack_onroad.fragments.RelaunchRouteDialogFragment;
import otr.ontrack_onroad.fragments.RelaunchRouteDialogFragment.RelaunchRouteDialogListener;
import otr.ontrack_onroad.fragments.SimulationListFragment;
import otr.ontrack_onroad.managers.CloseSessionManager;
import otr.ontrack_onroad.managers.CloseSessionManagerListener;
import otr.ontrack_onroad.managers.MessageManagerListener;
import otr.ontrack_onroad.managers.NotificationProtocolManager;
import otr.ontrack_onroad.managers.NotificationProtocolManagerListener;
import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.managers.OnTrackManagerAsyncTasks;
import otr.ontrack_onroad.models.Organization;
import otr.ontrack_onroad.models.Report;
import otr.ontrack_onroad.models.Route;
import otr.ontrack_onroad.parameters.GeneralMessages;
import otr.ontrack_onroad.parameters.Parameters;
import otr.ontrack_onroad.parameters.PreferenceKeys;
import otr.ontrack_onroad.parameters.ServiceKeys;
import otr.ontrack_onroad.parameters.WebServices;
import otr.ontrack_onroad.tasks.AddNewExceptionTask;
import otr.ontrack_onroad.tasks.GetAllRoutesForOrganizationTask;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.speech.tts.TextToSpeech.OnUtteranceCompletedListener;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.SoundEffectConstants;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class RouteList extends ActionBarActivity implements
		OnItemClickListener, MessageManagerListener,
		NotificationProtocolManagerListener, OnClickListener,
	    RelaunchRouteDialogListener,
		OnLongClickListener, android.content.DialogInterface.OnClickListener,
		CloseSessionManagerListener, OnItemSelectedListener {

	// Constantes del poster de próximo paradero
	private static final int POSTER_DURATION = 8;

	private static final int MINUTES = -10;

	public final static String ADD_ROUTE = "ADD_ROUTE";

	public final static String FREE_MODE_CHECKBOX = "ADD_ROUTE";

	public final static String EDIT_ROUTES = "EDIT_ROUTES";
	
	private final static String FILTERS = "FILTERS";

	private int selectedRoute = -1;

	private static ListView lvRoutes;

	private static CheckBox cbFreeMode;

	private ProgressDialog mProgressDialog;
	private static RouteListAdapter adapter;

	private static TextView speech;

	private static TextToSpeech textToSpeech;

	private String activeRouteID;

	private boolean startAutomatically;

	private boolean isEditionEnabled;

	private static ImageView ivEdition;

	private static String[] sidebarTexts;

	// Elementos de side bar
	private static DrawerLayout mDrawerLayout;
	private static ListView mDrawerList;
	private static ActionBarDrawerToggle mDrawerToggle;
	private static SidebarSoftAdapter sidebarAdapter;
	
	private static Spinner spinnerFilter;
	
	private static Spinner spinnerOrganizations;
	
	private static ImageView ivFilters;

	// Voz
	TextToSpeech onTrackVoice;

	// Fragment de evento
	private static Fragment eventFragment;
	private static int eventCounter = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		DisplayImageOptions displayImageOptions = new DisplayImageOptions.Builder( ).cacheInMemory( true ).cacheOnDisk( true ).build( );
		ImageLoaderConfiguration imageLoaderConfiguration = new ImageLoaderConfiguration.Builder( this ).defaultDisplayImageOptions( displayImageOptions ).build( );
		ImageLoader.getInstance( ).init( imageLoaderConfiguration );

		sidebarTexts = new String[] { "",
				getResources().getString(R.string.sidebar_messages),
				getResources().getString(R.string.sidebar_log_out), "" };



		SharedPreferences prefs = getSharedPreferences(
				PreferenceKeys.PREFERENCES_KEY, Context.MODE_PRIVATE);


		SharedPreferences.Editor editor = prefs.edit();

//		GetCurrentAppVersionTask getCurrentAppVersionTask = new GetCurrentAppVersionTask(
//				this);
//		getCurrentAppVersionTask.execute(
//				WebServices.URL_GET_CURRENT_APP_VERSION, ServiceKeys.TOKEN_KEY,
//				OnTrackManager.getInstance().getToken());

		String appVersion = "";




		try {
			appVersion = getPackageManager()
					.getPackageInfo(getPackageName(), 0).versionName;
		} catch (NameNotFoundException e) {
			appVersion = "N/A";
		}

		String exception = prefs.getString(PreferenceKeys.EXCEPTION, null);
		if (exception != null) {
			String androidVersion = android.os.Build.VERSION.RELEASE;
			String deviceType = android.os.Build.MANUFACTURER + " "
					+ android.os.Build.MODEL;
			String language = "Android";

			AddNewExceptionTask addNewExceptionTask = new AddNewExceptionTask();
			addNewExceptionTask.execute(WebServices.URL_ADD_NEW_EXCEPTION,
					ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance()
							.getToken(), ServiceKeys.EXCEPTION_KEY, exception,
					ServiceKeys.APP_VERSION_KEY, appVersion,
					ServiceKeys.ANDROID_VERSION_KEY, androidVersion,
					ServiceKeys.DEVICE_TYPE_KEY, deviceType,
					ServiceKeys.LANGUAGE_KEY, language);
			editor.remove(PreferenceKeys.EXCEPTION);
			editor.commit();
		}

		String activeRoute = prefs.getString(PreferenceKeys.ACTIVE_ROUTE, null);
		if (activeRoute != null) {
			activeRouteID = activeRoute;
			editor.remove(PreferenceKeys.ACTIVE_ROUTE);
			editor.commit();

			boolean tts = false;

//			if (tts) {
//				textToSpeech = new TextToSpeech(this, this);
//				textToSpeech.setOnUtteranceCompletedListener(this);
//			} else {
				RelaunchRouteDialogFragment relaunchRouteDialogFragment = RelaunchRouteDialogFragment
						.newInstance(activeRouteID);
				relaunchRouteDialogFragment.show(getSupportFragmentManager(),
						"relaunchRouteDialogFragment");
//			}
		}else{
			if(OnTrackManager.getInstance().isEnabledReplacement()) {
				ReplacementDialogFragment dialog = new ReplacementDialogFragment();
				dialog.setCancelable(false);
				dialog.show(getSupportFragmentManager(), "REPLACEMENT");
			}
		}

		setContentView(R.layout.activity_route_list);

		// Configuración de componentes gráficos básicos
		OnTrackManager.getInstance().setMyRoutes(new ArrayList<Route>());
		OnTrackManager.getInstance( ).getMyRoutes( ).addAll( OnTrackManager.getInstance( ).getRoutes( ) );

		lvRoutes = (ListView)findViewById(R.id.lvRoutes);

		adapter = new RouteListAdapter(this, OnTrackManager.getInstance( ).getRoutes( ));
		lvRoutes.setAdapter(adapter);
		lvRoutes.setOnItemClickListener(this);

		ImageView ivSideBar = (ImageView)
				findViewById(R.id.image_view_back);
		ivSideBar.setTag("SIDEBAR");
		ivSideBar.setOnClickListener((RouteList) this);

		cbFreeMode = (CheckBox)findViewById(R.id.cbFreeMode);
		cbFreeMode.setTag(FREE_MODE_CHECKBOX);
		cbFreeMode.setOnClickListener((OnClickListener)this);

//			speech = (TextView) rootView.findViewById(R.id.text_view_speech);
		ImageView ivAddRoute = (ImageView) findViewById(R.id.image_view_add_route);
		ivAddRoute.setTag(ADD_ROUTE);
		ivAddRoute.setOnClickListener((RouteList) this);

		ivEdition = (ImageView) findViewById(R.id.image_view_enable_edition);
		ivEdition.setTag(EDIT_ROUTES);
		ivEdition.setOnLongClickListener((RouteList)this);

		// CONFIGURACIï¿½N DEL SIDEBAR IZQUIERDO
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout_soft);
		mDrawerList = (ListView) findViewById(R.id.left_drawer_soft);

		// Sombra que aparece cuando se muestra el sidebar
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
				GravityCompat.START);

		sidebarAdapter = new SidebarSoftAdapter(this, sidebarTexts);

		mDrawerList.setAdapter(sidebarAdapter);
		mDrawerList.setOnItemClickListener(new OnItemClickListener() {
			// Manejo de acciones del sidebar
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
									int position, long id) {

				mDrawerLayout.closeDrawers();
				Intent i;
				switch (position) {
					case Parameters.OPTION_SOFT_CHAT:
						i = new Intent(RouteList.this, ChatList.class);
						startActivity(i);
						break;

					case Parameters.OPTION_SOFT_CLOSE_SESSION:
						CloseSessionDialogFragment dialog = new CloseSessionDialogFragment();
						dialog.show(getSupportFragmentManager(),
								Parameters.CLOSE_SESSION_DIALOG_TAG);
						break;
					default:
						break;

				}
			}
		});

		mDrawerToggle = new ActionBarDrawerToggle(this,
				mDrawerLayout, R.drawable.ic_launcher,
				R.string.drawer_open, R.string.drawer_close) {
			public void onDrawerClosed(View view) {
				RouteList.this.invalidateOptionsMenu();

			}

			public void onDrawerOpened(View drawerView) {
				RouteList.this.invalidateOptionsMenu();
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		getSupportActionBar()
				.setDisplayHomeAsUpEnabled(true);
		getSupportActionBar()
				.setHomeButtonEnabled(true);

		ivFilters = ( ImageView )findViewById(R.id.image_view_enable_filters);
		ivFilters.setOnClickListener( this );
		ivFilters.setTag( FILTERS );

		spinnerFilter = ( Spinner )findViewById(R.id.spinnerFilter);
		ArrayList<String> filters = new ArrayList<String>( );
		filters.add( getString( R.string.route_list_my_routes ) );
		filters.add( getString( R.string.route_list_all_routes ) );
		ArrayAdapter<String> filtersAdapter = new ArrayAdapter<String>( this, R.layout.spinner_filters, filters );
		filtersAdapter.setDropDownViewResource( R.layout.spinner_filters );
		spinnerFilter.setAdapter( filtersAdapter );
		spinnerFilter.setOnItemSelectedListener( this );

		spinnerOrganizations = ( Spinner )findViewById(R.id.spinnerOrganizations);
		ArrayList<String> organizations = new ArrayList<String>( );
		organizations.add( getString( R.string.route_list_all_organizations ) );

		if( OnTrackManager.getInstance( ).getUser( ).getOrganizations( ).size( ) > 1 )
		{
			organizations.addAll( OnTrackManager.getInstance( ).getUser( ).getOrganizationsNames( ) );
		}
		else
		{
			spinnerOrganizations.setVisibility( View.GONE );
		}

		ArrayAdapter<String> organizationsAdapter = new ArrayAdapter<String>( this, R.layout.spinner_filters, organizations );
		organizationsAdapter.setDropDownViewResource( R.layout.spinner_filters );
		spinnerOrganizations.setAdapter( organizationsAdapter );
		spinnerOrganizations.setOnItemSelectedListener( this );

		//


		getSupportActionBar().hide();

		// Este Activity se convierte en un listener de
		// NotificationProtocolManager
		NotificationProtocolManager.getInstance().addListener(this);

		// if (OnTrackManager.getInstance().getUser() == null) {

		// }

		// La actividad actual se convierte en un "Listener" de CloseSessionManager
	    CloseSessionManager.getInstance().addListener(this);

		eventFragment = new EventFragment();
		getSupportFragmentManager().beginTransaction()
				.add(R.id.rlRouteList, eventFragment)
				.setCustomAnimations(R.anim.event_down, R.anim.event_up)
				.hide(eventFragment).commit();

		onTrackVoice = new TextToSpeech(getApplicationContext(),
				new TextToSpeech.OnInitListener() {
					@Override
					public void onInit(int status) {
						if (status != TextToSpeech.ERROR) {
//							// Posible voz, en caso de que haya mensajes nuevos
							int newMessages = OnTrackManager.getInstance().numberOfUnreadMessages();
							if(newMessages > 0){
								if(newMessages==1){
									speak("Atención: Tiene un mensaje nuevo.");
									showEventFragment("Tiene 1 mensaje nuevo.");
								}else {
									speak("Atención: Tiene " + newMessages + " mensajes nuevos.");
									showEventFragment("Tiene " + newMessages + " mensajes nuevos.");
								}
							}
						} else {

						}
					}
				});


	}

	@Override
	public void onResume() {
		super.onResume();
		adapter.notifyDataSetChanged();

		isEditionEnabled = false;
		ivEdition.setImageDrawable(getResources().getDrawable(
				R.drawable.edition_disabled));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_changeroute) {
			return true;
		} else if (id == android.R.id.home) {
			finish();
		}
		return super.onOptionsItemSelected(item);
	}


	// AsyncTask para carga de informaciï¿½n del usuario autenticado y sus rutas

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {

		this.selectedRoute = position;

		if (isEditionEnabled) {
//			RouteActionDialogFragment routeActionDialogFragment = RouteActionDialogFragment
//					.newInstance(position);
//			routeActionDialogFragment.show(getFragmentManager(),
//					"routeActionDialogFragment");
		}
		else
		{

			Route route = ( Route )adapter.getItem(position);
			if ( route.getNumberOfStops( ) == 0 )
			{
				//Para textos en ecuatoriano

				Organization organization = OnTrackManager.getInstance().getUser().getOrganizationByID(OnTrackManager.getInstance().getRoutes().get(position).getOrganizationId());
				boolean ecuatorian = organization.getCreatedBy().equals("superadmin@ontrackschool.ec");

				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setTitle(getResources().getString(R.string.free_mode_title));
				builder.setMessage(getResources().getString(ecuatorian?R.string.free_mode_text_ecuador:R.string.free_mode_text));
				builder.setPositiveButton(GeneralMessages.YES, this);
				builder.setNegativeButton(GeneralMessages.NO, this);

				AlertDialog alert = builder.create();
				alert.show();
			}else if(route.isHasFinished()){
				Toast.makeText(this,getResources().getString(R.string.finished_route_error),Toast.LENGTH_LONG).show();
			}
			else
			{
				if (route.getSchedule() != null) {
//					if (OnTrackManager.getInstance().getSelectedRoute() != -1) {
//						if (this.selectedRoute != OnTrackManager.getInstance()
//								.getSelectedRoute()) {
//							ChangeRouteDialogFragment dialog = new ChangeRouteDialogFragment(
//									adapter.getItemIdAsString(position));
//							dialog.show(getSupportFragmentManager(),
//									GeneralMessages.DIALOG_TITLE_CHANGE_ROUTE);
//						} else {
//							Toast.makeText(this,
//									GeneralMessages.ROUTE_IN_PROGRESS,
//									Toast.LENGTH_LONG).show();
//						}
//
//					} else {
					if( route.shouldBeRunning( ) )
					{
						prepareAndStartNewRoute(adapter
								.getItemIdAsString(position));
					}
					else
					{

						AlertDialog alert = new AlertDialog.Builder( this ).create();
						alert.setTitle(getResources().getString(R.string.action_fragment_dialog_title_start_route_off_schedule));
						alert.setMessage( getResources().getString( R.string.action_fragment_dialog_message_start_route_off_schedule ) );
						alert.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.action_fragment_dialog_yes_start_route_off_schedule), new android.content.DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								prepareAndStartNewRoute(adapter
										.getItemIdAsString(selectedRoute));
							}
						});
						alert.setButton(AlertDialog.BUTTON_NEGATIVE, getResources().getString(R.string.action_fragment_dialog_no_start_route_off_schedule), new android.content.DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.dismiss();
							}
						});

						alert.show();
					}


//					}
				} else {
					Toast.makeText(
							this,
							getResources().getString(R.string.route_list_no_schedule),
							Toast.LENGTH_LONG).show();
				}
			}
		}

	}

	public void prepareAndStartNewRoute(String selectedRouteId) {
		// El atributo 'selectedRoute' de OnTrackSystem corresponde a la
		// posiciï¿½n en la lista 'routes' de la ruta seleccionada

		// Pregunta si hay simulaciones disponibles para esta ruta

		List<Simulation> list = OnTrackManager.getInstance()
				.getSimulationsOfRoute(selectedRouteId);

		if (OnTrackManager.getInstance().getSelectedRoute() != -1) {
			OnTrackManager.getInstance().resetRoute();
		}

		if (list == null
				|| !OnTrackManager.getInstance().isSimulationsAllowed()) {

			OnTrackManager.getInstance().setSelectedRoute(this.selectedRoute);
			// Abre el MainActivity. que muestra la informaciï¿½n detallada de
			// la
			// ruta
			Intent i = new Intent(RouteList.this, MainActivity.class);
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			i.putExtra("YAY", startAutomatically);
			startActivity(i);
			finish();
		} else {
			// Abre un diï¿½log para permitir seleccionar simulaciones
			SimulationListFragment dialog = new SimulationListFragment();
			dialog.setParameters(list,
					selectedRoute);
			dialog.show(getSupportFragmentManager(),
					Parameters.SIMULATIONS_DIALOG_TAG);
		}
	}

	public void prepareAndStartNewRouteFree(String selectedRouteId){
		if (OnTrackManager.getInstance().getSelectedRoute() != -1) {
			OnTrackManager.getInstance().resetRoute();
		}
		OnTrackManager.getInstance().setSelectedRoute(this.selectedRoute);
		// Abre el MainActivity. que muestra la informaciï¿½n detallada de
		// la
		// ruta
		Intent i = new Intent(RouteList.this, FreeMode.class);
		i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		i.putExtra("YAY", startAutomatically);
		startActivity(i);
		finish();
	}

	@Override
	public void onReceiveMessage(ArrayList<Long> idsOfModifiedChatCards) {

	}

	@Override
	public void onReceivePusherEvent(Event event) {

	}

	@Override
	public void onReceivePusherReport(Report report) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProtocolExecuted() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onMessagesRead(long chatCardId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onClick(View v) {
		// Toast.makeText( getApplicationContext( ),
		// "Prï¿½ximamente podrï¿½s crear rutas", Toast.LENGTH_SHORT ).show( );
		String tag = v.getTag().toString();

		if(tag.equals(FREE_MODE_CHECKBOX)){
			onItemSelected(null,null,-1,-1);

		}else if (tag.equals(ADD_ROUTE)) {
//			Intent intent = new Intent(this, CreateRoute.class);
//			startActivity(intent);
		} else if (tag.equals("SIDEBAR")) {
			mDrawerLayout.openDrawer(mDrawerList);
		}
		else if( tag.equals( FILTERS ) )
		{
		    if( spinnerFilter.getVisibility( ) == View.VISIBLE )
		    {
		        spinnerFilter.setSelection( 0 );
		        spinnerFilter.setVisibility( View.INVISIBLE );
		        if( OnTrackManager.getInstance( ).getUser( ).getOrganizations( ).size( ) > 1 )
                {
		            spinnerOrganizations.setSelection( 0 );
		            spinnerOrganizations.setVisibility( View.INVISIBLE );
                }
		    }
		    else
		    {
		        spinnerFilter.setVisibility( View.VISIBLE );
		        
		        if( OnTrackManager.getInstance( ).getUser( ).getOrganizations( ).size( ) > 1 )
		        {
		            spinnerOrganizations.setVisibility( View.VISIBLE );
		        }
		    }
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		switch (requestCode) {
		case 10: {
			if (resultCode == RESULT_OK && null != data) {
				ArrayList<String> result = data
						.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
				String response = result.get(0).toLowerCase();

				if (response.contains("si")) {
					startAutomatically = true;
					Route route = null;
					int position = -1;

					for (int i = 0; i < OnTrackManager.getInstance()
							.getRoutes().size(); i++) {
						Route actualRoute = OnTrackManager.getInstance()
								.getRoutes().get(i);

						if (actualRoute.getId().equals(activeRouteID)) {
							position = i;
							selectedRoute = i;
							route = actualRoute;
							break;
						}
					}

					if (route.getSchedule() != null) {
//						if (OnTrackManager.getInstance().getSelectedRoute() != -1) {
//							if (position != OnTrackManager.getInstance()
//									.getSelectedRoute()) {
//								ChangeRouteDialogFragment dialog = new ChangeRouteDialogFragment(
//										route.getId());
//								dialog.show(
//										getSupportFragmentManager(),
//										GeneralMessages.DIALOG_TITLE_CHANGE_ROUTE);
//							} else {
//								Toast.makeText(this,
//										GeneralMessages.ROUTE_IN_PROGRESS,
//										Toast.LENGTH_LONG).show();
//							}
//						} else {
							prepareAndStartNewRoute(route.getId());
//						}
					} else {
						Toast.makeText(
								this,
								getResources().getString(R.string.route_list_no_schedule),
								Toast.LENGTH_LONG).show();
					}
				}
			}
			break;
		}

		}
	}

	public RouteListAdapter getAdapter() {
		return adapter;
	}

	public static void suggestRoutes() {
		Calendar nowCalendar = Calendar.getInstance();
		nowCalendar.setTime(new Date());

		int nowDay = nowCalendar.get(Calendar.DAY_OF_WEEK);

		for (Route route : OnTrackManager.getInstance( ).getRoutes( )) {
			if (route.getSchedule() != null) {
				Calendar startCalendar = Calendar.getInstance();
				Calendar endCalendar = Calendar.getInstance();

				try {
					startCalendar.setTime(new SimpleDateFormat("KK:mm:ss")
							.parse(route.getSchedule().getStartTime()));
					endCalendar.setTime(new SimpleDateFormat("KK:mm:ss")
							.parse(route.getSchedule().getEndTime()));
				} catch (ParseException e) {

				}

				startCalendar.set(nowCalendar.get(Calendar.YEAR),
						nowCalendar.get(Calendar.MONTH),
						nowCalendar.get(Calendar.DAY_OF_MONTH));
				endCalendar.set(nowCalendar.get(Calendar.YEAR),
						nowCalendar.get(Calendar.MONTH),
						nowCalendar.get(Calendar.DAY_OF_MONTH));
				startCalendar.add(Calendar.MINUTE, MINUTES);

				if (nowCalendar.after(startCalendar)
						&& nowCalendar.before(endCalendar)) {
					if (nowDay == Calendar.MONDAY
							&& route.getSchedule().isMonday()) {
						route.setShouldBeRunning(true);
					} else if (nowDay == Calendar.TUESDAY
							&& route.getSchedule().isTuesday()) {
						route.setShouldBeRunning(true);
					} else if (nowDay == Calendar.WEDNESDAY
							&& route.getSchedule().isWednesday()) {
						route.setShouldBeRunning(true);
					} else if (nowDay == Calendar.THURSDAY
							&& route.getSchedule().isThursday()) {
						route.setShouldBeRunning(true);
					} else if (nowDay == Calendar.FRIDAY
							&& route.getSchedule().isFriday()) {
						route.setShouldBeRunning(true);
					} else if (nowDay == Calendar.SATURDAY
							&& route.getSchedule().isSaturday()) {
						route.setShouldBeRunning(true);
					} else if (nowDay == Calendar.SUNDAY
							&& route.getSchedule().isSunday()) {
						route.setShouldBeRunning(true);
					}
				}
			}
		}

		Collections.sort(OnTrackManager.getInstance( ).getRoutes( ),
				new Comparator<Route>() {
					@Override
					public int compare(Route r1, Route r2) {
						if (!r1.shouldBeRunning() && r2.shouldBeRunning()) {
							return 1;
						}
						if (r1.shouldBeRunning() && !r2.shouldBeRunning()) {
							return -1;
						}

						return 0;
					}
				});
	}

//	@Override
//	public void onInit(int status) {
//		if (status == TextToSpeech.SUCCESS) {
//			speak();
//		}
//	}

//	public void speak() {
//		HashMap<String, String> myHashAlarm = new HashMap();
//		myHashAlarm.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "endSpeak");
//		final String text = "La aplicacón se ceró inesperadamente. Desea volver a iniciar la ruta "
//				+ activeRouteID + "?";
//		textToSpeech.speak(text, TextToSpeech.QUEUE_ADD, myHashAlarm);
//	}

//	@Override
//	public void onUtteranceCompleted(String utteranceId) {
//		textToSpeech.shutdown();
//		Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
//		intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
//				RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
//		intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
//		intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Diga algo...");
//		try {
//			startActivityForResult(intent, 10);
//		} catch (ActivityNotFoundException a) {
//			Toast.makeText(
//					getApplicationContext(),
//					"Lo sentimos, este dispositivo no soporta la funcion de dictado",
//					Toast.LENGTH_SHORT).show();
//		}
//
//	}

	@Override
	public void onClick() {
		startAutomatically = true;
		Route route = null;
		int position = -1;

		for (int i = 0; i < OnTrackManager.getInstance().getRoutes().size(); i++) {
			Route actualRoute = OnTrackManager.getInstance().getRoutes().get(i);

			if (actualRoute.getId().equals(activeRouteID)) {
				position = i;
				selectedRoute = i;
				route = actualRoute;
				break;
			}
		}

		if (route.getSchedule() != null) {
//			if (OnTrackManager.getInstance().getSelectedRoute() != -1) {
//				if (position != OnTrackManager.getInstance().getSelectedRoute()) {
//					ChangeRouteDialogFragment dialog = new ChangeRouteDialogFragment(
//							route.getId());
//					dialog.show(getSupportFragmentManager(),
//							GeneralMessages.DIALOG_TITLE_CHANGE_ROUTE);
//				} else {
//					Toast.makeText(this, GeneralMessages.ROUTE_IN_PROGRESS,
//							Toast.LENGTH_LONG).show();
//				}
//			} else {
			if(route.getNumberOfStops()!=0) {
				prepareAndStartNewRoute(route.getId());
			}else{
				prepareAndStartNewRouteFree(route.getId());
			}
//			}
		} else {
			Toast.makeText(
					this,
					"Esta ruta no tiene horario asignado. Por favor comuníquese con el coordinador de rutas",
					Toast.LENGTH_LONG).show();
		}
	}

	@Override
	public boolean onLongClick(View v) {
		String tag = v.getTag().toString();
		v.playSoundEffect(SoundEffectConstants.CLICK);

		if (tag.equals(EDIT_ROUTES)) {
			if (isEditionEnabled) {
				isEditionEnabled = false;
				ivEdition.setImageDrawable(getResources().getDrawable(
						R.drawable.edition_disabled));
			} else {
				isEditionEnabled = true;
				ivEdition.setImageDrawable(getResources().getDrawable(
						R.drawable.edition_enabled));
			}
		}

		return true;
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		if (which == -1) {
			OnTrackManager.getInstance().setSelectedRoute(selectedRoute);
			Intent intent = new Intent(this, FreeMode.class);
			startActivity(intent);
            finish();
		}
	}

	@Override
	public void logOut() {
		CloseSessionManager.executeCloseSessionTask(
				OnTrackManagerAsyncTasks.URL_BASE_CLOSE_SESSION,
				ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance().getToken(),
				ServiceKeys.USER_DEVICE_ID_KEY, ""
						+ OnTrackManager.getInstance().getUserDeviceId());

	}

	@Override
	public void onSessionClosedInServer() {
		SharedPreferences prefs = getSharedPreferences(
				PreferenceKeys.PREFERENCES_KEY, Context.MODE_PRIVATE);
		OnTrackManager.getInstance().resetRoute();
		SharedPreferences.Editor editor = prefs.edit();
		editor.remove(PreferenceKeys.TOKEN_KEY);
		editor.remove(PreferenceKeys.USER_DEVICE_ID_KEY);
		editor.commit();
		Intent i = new Intent(RouteList.this, Login.class);
		startActivity(i);
		finish();

	}

    @Override
    public void onItemSelected( AdapterView<?> parent, View view, int position, long id )
    {
        OnTrackManager.getInstance( ).getRoutes( ).clear();

        if( spinnerFilter.getSelectedItemPosition( ) == 0 )
        {
            if( spinnerOrganizations.getSelectedItemPosition( ) != 0 )
            {
                String organizationName = ( String )spinnerOrganizations.getSelectedItem( );
                String organizationID = "";

                for( Organization organization : OnTrackManager.getInstance( ).getUser( ).getOrganizations( ) )
                {
                    if( organization.getTitle( ).equals( organizationName ) )
                    {
                        organizationID = organization.getId( );
                        break;
                    }
                }

				if(cbFreeMode.isChecked()) {
					for (Route route : OnTrackManager.getInstance().getMyRoutesFreeMode()) {
						if (route.getId().split("-")[1].equals(organizationID)) {
							OnTrackManager.getInstance().getRoutes().add(route);
						}
					}
				}else{
					for (Route route : OnTrackManager.getInstance().getMyRoutes()) {
						if (route.getId().split("-")[1].equals(organizationID)) {
							OnTrackManager.getInstance().getRoutes().add(route);
						}
					}
				}
            }
            else
            {
				if(cbFreeMode.isChecked()) {
					OnTrackManager.getInstance().getRoutes().addAll(OnTrackManager.getInstance().getMyRoutesFreeMode());
				}else {
					OnTrackManager.getInstance().getRoutes().addAll(OnTrackManager.getInstance().getMyRoutes());
				}
            }
        }
        else
        {
            if( spinnerOrganizations.getSelectedItemPosition( ) == 0 )
            {
                for( Organization organization : OnTrackManager.getInstance( ).getUser( ).getOrganizations( ) )
                {
                    if( organization.getRoutes( ) == null )
                    {
                        GetAllRoutesForOrganizationTask getAllRoutesForOrganizationTask = new GetAllRoutesForOrganizationTask( this, organization );
                        getAllRoutesForOrganizationTask.execute( WebServices.URL_GET_ALL_ROUTES_FOR_ORGANIZATION, ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance( ).getToken( ), ServiceKeys.ID_ORGANIZATION_KEY, organization.getId( ) );
                    }
                }
            }
            else
            {
                for( Organization organization : OnTrackManager.getInstance( ).getUser( ).getOrganizations( ) )
                {
                    if( organization.getRoutes( ) == null && organization.getTitle( ).equals( ( String )spinnerOrganizations.getSelectedItem( ) ) )
                    {
                        GetAllRoutesForOrganizationTask getAllRoutesForOrganizationTask = new GetAllRoutesForOrganizationTask( this, organization );
                        getAllRoutesForOrganizationTask.execute( WebServices.URL_GET_ALL_ROUTES_FOR_ORGANIZATION, ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance( ).getToken( ), ServiceKeys.ID_ORGANIZATION_KEY, organization.getId( ) );
                    }
                }
            }
            
            if( spinnerOrganizations.getSelectedItemPosition( ) != 0 )
            {
                String organizationName = ( String )spinnerOrganizations.getSelectedItem( );

                for( Organization organization : OnTrackManager.getInstance( ).getUser( ).getOrganizations( ) )
                {
                    if( organization.getRoutes( ) != null && organization.getTitle( ).equals( organizationName ) )
                    {
						if(cbFreeMode.isChecked()){
							OnTrackManager.getInstance().getRoutes().addAll(organization.getRoutesFreeMode());
						}else {
							OnTrackManager.getInstance().getRoutes().addAll(organization.getRoutes());
						}
                        break;
                    }
                }
            }
            else
            {
                for( Organization organization : OnTrackManager.getInstance( ).getUser( ).getOrganizations( ) )
                {
                    if( organization.getRoutes( ) != null )
                    {
						if(cbFreeMode.isChecked()){
							OnTrackManager.getInstance().getRoutes().addAll(organization.getRoutesFreeMode());
						}else {
							OnTrackManager.getInstance().getRoutes().addAll(organization.getRoutes());
						}
					}
                }
			}
		}

        suggestRoutes();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onNothingSelected( AdapterView<?> parent )
    {
        
    }
    
    public void routesLoaded( )
    {
        onItemSelected( null, null, -1, -1 );
    }

	public void speak(String text) {
		final String textForTts = text;

		onTrackVoice.speak(textForTts, TextToSpeech.QUEUE_ADD, null);

	}

	@Override
	protected void onDestroy() {
		onTrackVoice.shutdown();
		super.onDestroy();
	}

	public void showEventFragment(String message) {

		((EventFragment) eventFragment).setMessage(message);
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.setCustomAnimations(R.anim.event_down, R.anim.event_up);
		ft.show(eventFragment);
		ft.commit();

//		final Timer t = new Timer();
//		t.schedule(new TimerTask() {
//			public void run() {
//				eventCounter++;
//				if (eventCounter == POSTER_DURATION) {
//					hideEventFragment();
//
//					// Finalizar el timer del event poster
//					eventCounter = 0;
//					t.cancel();
//					t.purge();
//
//				}
//			}
//
//		}, 1000, 1000);
	}

	public void hideEventFragment() {
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.setCustomAnimations(R.anim.event_down, R.anim.event_up);
		ft.hide(eventFragment);
		ft.commit();
	}

	public void hideEventFragmentForced() {
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.hide(eventFragment);
		ft.commit();
	}

	public void gotoNewMessages(){
		hideEventFragmentForced();
		Intent i = new Intent(RouteList.this,ChatList.class);
		startActivity(i);
	}
}