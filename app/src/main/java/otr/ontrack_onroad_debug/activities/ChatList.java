package otr.ontrack_onroad_debug.activities;

import java.util.ArrayList;

import otr.ontrack_onroad_debug.activities.R;
import otr.ontrack_onroad.adapters.ChatListAdapter;
import otr.ontrack_onroad.dao.Event;
import otr.ontrack_onroad.managers.MessageManager;
import otr.ontrack_onroad.managers.MessageManagerListener;
import otr.ontrack_onroad.models.Report;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class ChatList extends ActionBarActivity implements OnItemClickListener,
		MessageManagerListener {
	private static ListView lvChatGroups;
	private static ChatListAdapter chatListAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_chat_list);
		this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		lvChatGroups = (ListView) findViewById(R.id.lvChatGroups);
		// lvChatGroups.setAdapter(new ChatListAdapter(getActivity(),
		// MessageManager.getInstance().getChatCards()));
		chatListAdapter = new ChatListAdapter(this, MessageManager.getInstance().getChatCards());

		lvChatGroups.setAdapter(chatListAdapter);
		lvChatGroups
				.setOnItemClickListener(this);

		// Este Activity se convierte en un listener de MessageManager
		MessageManager.getInstance().addListener(this);
	}

//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.chat_groups, menu);
//		return true;
//	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == android.R.id.home) {
			finish();
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		MessageManager.getInstance().setSelectedChatCard(id);
		Intent i = new Intent(ChatList.this, Chat.class);
		startActivity(i);
	}

	@Override
	public void onReceiveMessage(ArrayList<Long> idsOfModifiedChatCards) {
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				chatListAdapter.notifyDataSetChanged();
			}
		});

	}

	@Override
	public void onReceivePusherEvent(Event event) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onReceivePusherReport(Report report) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onMessagesRead(long chatCardId) {
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				chatListAdapter.notifyDataSetChanged();
			}
		});
		
	}

}
