package otr.ontrack_onroad_debug.activities;

import java.io.IOException;

import org.json.JSONException;

import otr.ontrack_onroad_debug.activities.R;
import otr.ontrack_onroad.managers.AuthenticationManager;
import otr.ontrack_onroad.managers.AuthenticationManagerListener;
import otr.ontrack_onroad.managers.NotificationProtocolManager;
import otr.ontrack_onroad.managers.NotificationProtocolManagerListener;
import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.parameters.GeneralMessages;
import otr.ontrack_onroad.parameters.Parameters;
import otr.ontrack_onroad.parameters.PreferenceKeys;
import otr.ontrack_onroad.parameters.ServiceKeys;
import otr.ontrack_onroad.utils.JSONManager;
import otr.ontrack_onroad.utils.WebRequestManager;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

public class NotificationLoader extends Activity implements
		AuthenticationManagerListener, NotificationProtocolManagerListener {

	public static final String URL_GET_SINGLE_NOTIFICATION = "ApiNotification/GetSingleNotification";
	public static final String URL_BASE_AUTHENTICATION = "ApiAuthentication/authenticate";

	public static final String URL_BASE_DRIVER = "ApiServices/GetDriverDetail";
	public static final String URL_BASE_CHATCARDS = "ApiMessage/getDirectory";

	private String notificationId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_notification_loader);

		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}

		AuthenticationManager.getInstance().setListener(this);

		// Esta actividad se vuelve un listener de
		// NotificationProtocolManagerListener
		NotificationProtocolManager.getInstance().addListener(this);

		notificationId = getIntent().getExtras().getString("notification_id");

		if (OnTrackManager.getInstance().getToken().equals("")) {

			// En caso de que la aplicación estuviera cerrada, al abrir una
			// nueva notificación se debe hacer lo siguiente:
			// 0. Inicializar la base de datos local
			// 1. Hacer la autenticación con nombre de usuario y contraseña
			// guardados
			// 2. Traer la información de usuario (rutas y horarios)
			// 3. Traer el directorio de mensajería
			// 4. Ejecutar el protocolo de notificaciones
			// 5. Cargar el tipo de notificación que fue abierta
			// 6. Abrir el Activity apropiado

			// Definir un context para el OnTrackSystem
			OnTrackManager.getInstance().setContext(this);
			// Inicializar base de datos
			OnTrackManager.getInstance().initializeDataBase();

			SharedPreferences prefs = getSharedPreferences(
					PreferenceKeys.PREFERENCES_KEY, Context.MODE_PRIVATE);
			String token = prefs.getString(PreferenceKeys.TOKEN_KEY, null);

			String registrationId = prefs.getString(
					PreferenceKeys.PROPERTY_REG_ID_KEY, "");
			// Log.d("REGISTRATION_ID", registrationId);

			if (token == null) {

			} else {
				AuthenticationManager.getInstance().executeLoadRouteTask(
						URL_BASE_DRIVER, ServiceKeys.TOKEN_KEY, token);
			}
		} else {
			LoadSingleNotificationTask task = new LoadSingleNotificationTask();
			task.execute(URL_GET_SINGLE_NOTIFICATION, ServiceKeys.TOKEN_KEY,
					OnTrackManager.getInstance().getToken(),
					ServiceKeys.ID_NOTIFICATION_KEY, notificationId);
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.notification_loader, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(
					R.layout.fragment_notification_loader, container, false);
			return rootView;
		}
	}

	private class LoadSingleNotificationTask extends
			AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... args) {
			String response = null;
			try {
				response = WebRequestManager.executeTask(args);

				// Carga los detalles de la ruta a partir de la cadena JSON
				// obtenida en la web
				// JSONManager.loadRoute(response, OnTrackSystem.getInstance()
				// .getSelectedRoute());

			} catch (IOException e) {
				response = GeneralMessages.HTTP_NOT_FOUND_ERROR;
			}

			return response;

		}

		@Override
		protected void onPostExecute(String result) {

			if (result.equals(GeneralMessages.HTTP_CONNECTION_ERROR)
					|| result.equals(GeneralMessages.JSON_ERROR)
					|| result.equals(GeneralMessages.HTTP_NOT_FOUND_ERROR)) {
				Toast.makeText(NotificationLoader.this, result,
						Toast.LENGTH_LONG).show();
			} else {
				try {
					int notificationCode = JSONManager
							.loadNotificationType(result);
					Intent i;
					if (notificationCode == Parameters.NOTIFICATION_TYPE_MESSAGE) {
						i = new Intent(NotificationLoader.this, Chat.class);
						startActivity(i);
						finish();
					} else if (notificationCode == Parameters.NOTIFICATION_TYPE_EVENT) {
						i = new Intent(NotificationLoader.this, EventList.class);
						startActivity(i);
						finish();
					}
				} catch (JSONException e) {
					Toast.makeText(NotificationLoader.this, e.getMessage(),
							Toast.LENGTH_LONG).show();
				}
			}
		}
	}

	
	@Override
	public void onProtocolExecuted() {
		LoadSingleNotificationTask task = new LoadSingleNotificationTask();
		task.execute(URL_GET_SINGLE_NOTIFICATION, ServiceKeys.TOKEN_KEY,
				OnTrackManager.getInstance().getToken(),
				ServiceKeys.ID_NOTIFICATION_KEY, notificationId);
		NotificationProtocolManager.getInstance().removeListener(this);

	}

	@Override
	public void onRoutePreloaded() {

	}

	@Override
	public void onRouteLoaded() {
		AuthenticationManager.getInstance().executeRequestChatcardsTask(
				URL_BASE_CHATCARDS, ServiceKeys.TOKEN_KEY,
				OnTrackManager.getInstance().getToken());

	}

	@Override
	public void onRouteNotLoaded() {
		Toast.makeText(NotificationLoader.this,
				GeneralMessages.ERROR_LOADING_ROUTES, Toast.LENGTH_LONG).show();

	}

	@Override
	public void onAuthenticationError(String error) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onChatcardsLoaded() {
		try {
			String savedNotificationsJSONString = OnTrackManager.getInstance()
					.getSavedNotificationsJSONString();
			if (savedNotificationsJSONString == null) {
				// Si no hay notificaciones guardadas, pasar
				// directamente a revisar notificaciones nuevas
				NotificationProtocolManager
						.executeRequestNewNotificationsTask();

			} else {
				// Ejecutar el protocolo normal de notificaciones
				NotificationProtocolManager.executeNotificationProtocolTask(
						NotificationProtocolManager.URL_BASE_ACK_NOTIFICATIONS,
						ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance()
								.getToken(), ServiceKeys.NOTIFICATIONS_KEY,
						savedNotificationsJSONString);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
//		AuthenticationManager.getInstance().removeListener(this);

	}

	@Override
	public void onChatcardsError(String message) {
		// TODO Auto-generated method stub

	}

}