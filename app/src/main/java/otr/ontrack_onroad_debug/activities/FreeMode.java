package otr.ontrack_onroad_debug.activities;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import otr.ontrack_onroad_debug.activities.R;
import otr.ontrack_onroad.dao.ChatCard;
import otr.ontrack_onroad.dao.Event;
import otr.ontrack_onroad.dao.Message;
import otr.ontrack_onroad.fragments.AssistanceTypeFreeDialogFragment;
import otr.ontrack_onroad.fragments.AssistanceWithNoveltiesFragment;
import otr.ontrack_onroad.fragments.CloseAppDialogFragment;
import otr.ontrack_onroad.fragments.CloseSessionDialogFragment;
import otr.ontrack_onroad.fragments.EventFragment;
import otr.ontrack_onroad.fragments.ReportFragment;
import otr.ontrack_onroad.fragments.StartRouteFragment;
import otr.ontrack_onroad.freeModeTasks.GetRouteTrackablesFreeModeTask;
import otr.ontrack_onroad.freeModeTasks.RequestFootprintTaskFreeMode;
import otr.ontrack_onroad.managers.EventManager;
import otr.ontrack_onroad.managers.FreeModeEventsManager;
import otr.ontrack_onroad.managers.FreeModeLocationManager;
import otr.ontrack_onroad.managers.FreeModeManager;
import otr.ontrack_onroad.managers.MessageManager;
import otr.ontrack_onroad.managers.MessageManagerListener;
import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.managers.OnTrackManagerAsyncTasks;
import otr.ontrack_onroad.managers.OnTrackManagerListener;
import otr.ontrack_onroad.models.Refuge;
import otr.ontrack_onroad.models.Report;
import otr.ontrack_onroad.models.Route;
import otr.ontrack_onroad.models.Stop;
import otr.ontrack_onroad.models.Trackable;
import otr.ontrack_onroad.parameters.GeneralMessages;
import otr.ontrack_onroad.parameters.Parameters;
import otr.ontrack_onroad.parameters.ServiceKeys;
import otr.ontrack_onroad.parameters.WebServices;
import otr.ontrack_onroad.utils.Timestamp;

import com.mapbox.mapboxsdk.constants.Style;
import com.mapbox.mapboxsdk.views.MapView;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;

public class FreeMode extends ActionBarActivity implements  OnClickListener, OnGlobalLayoutListener, View.OnTouchListener, OnTrackManagerListener, MessageManagerListener
{
    private final static String LEFT_MENU = "LEFT_MENU";

    private final static String MY_ROUTE = "MY_ROUTE";

    private final static String REPORTS = "REPORTS";

    private final static String MESSAGES = "MESSAGES";

    private final static String LOG_OUT = "LOG_OUT";

    private final static String ROUTE_FINISHED = "ROUTE_FINISHED";

    private final static String SHOW_TRACKABLES = "SHOW_TRACKABLES";

    private final static String GO_TO_REFUGE = "GO_TO_REFUGE";

    private final static String LAST_ANNOUNCEMENT = "LAST_ANNOUNCEMENT";

    private Route route;

    private ScrollView svLeftMenu;

    private MapView mvMap;

    private Fragment assistanceWithNoveltiesFragment;

    // Fragment del botón de inicio de ruta
    private static Fragment startRouteFragment;

    private static Fragment eventFragment;
    private static int eventCounter = 0;

    // Constantes del poster de próximo paradero
    private static final int POSTER_DURATION = 8;

    // Constantes del poster de nuevo mensaje
    private static final int MESSAGE_DURATION = 16;

    private ImageView ivGoToRefuge;

    // Voz
    private TextToSpeech onTrackVoice;

    private boolean startAutomatically;

    private ImageView ivAnnouncementsFree;
    private ImageView ivReportsFree;

    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_free_mode);

        getSupportActionBar().hide();

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        OnTrackManager.getInstance().setListener(this);

        // La actividad actual se convierte en un "Listener" de ChatManager
        MessageManager.getInstance().addListener(this);

        startAutomatically = getIntent().getBooleanExtra("YAY", false);

        onTrackVoice = new TextToSpeech(getApplicationContext(),
                new TextToSpeech.OnInitListener() {
                    @Override
                    public void onInit(int status) {
                        if (status != TextToSpeech.ERROR) {
                            // onTrackVoice.setLanguage(locSpanish);
                        } else {

                        }
                    }
                });

        route = OnTrackManager.getInstance( ).getRoutes().get(OnTrackManager.getInstance().getSelectedRoute());
        OnTrackManager.getInstance( ).getUser( ).setRoute( route );
        route.setStartTime( ( Calendar )Calendar.getInstance( ).clone( ) );
        FreeModeEventsManager.getInstance().setup(route);

        mvMap = ( MapView )findViewById( R.id.activity_free_mode_map );
        mvMap.onCreate(savedInstanceState);
        mvMap.setStyleUrl(Style.EMERALD);

        svLeftMenu = ( ScrollView )findViewById( R.id.activity_free_mode_left_menu );
        svLeftMenu.getViewTreeObserver( ).addOnGlobalLayoutListener(this);

        ImageView ivMenu = ( ImageView )findViewById( R.id.activity_free_mode_image_view_sidebar );
        ivMenu.setTag( LEFT_MENU );
        ivMenu.setOnClickListener(this);

        TextView tvRouteName = ( TextView )findViewById( R.id.activity_free_mode_route_name );
        tvRouteName.setText(route.getId().split("-")[0]);

        RelativeLayout rlMyRoute = ( RelativeLayout )findViewById( R.id.activity_free_mode_my_route );
        rlMyRoute.setTag( MY_ROUTE );
        rlMyRoute.setOnClickListener(this);

        RelativeLayout rlReports = ( RelativeLayout )findViewById( R.id.activity_free_mode_reports );
        rlReports.setTag( REPORTS );
        rlReports.setOnClickListener(this);

        RelativeLayout rlMessages = ( RelativeLayout )findViewById( R.id.activity_free_mode_messages );
        rlMessages.setTag( MESSAGES );
        rlMessages.setOnClickListener(this);

        RelativeLayout rlLogOut = ( RelativeLayout )findViewById( R.id.activity_free_mode_log_out );
        rlLogOut.setTag( LOG_OUT );
        rlLogOut.setOnClickListener(this);

        Button bRouteFinished = ( Button )findViewById( R.id.activity_free_mode_finish_route );
        bRouteFinished.setTag( ROUTE_FINISHED );
        bRouteFinished.setOnClickListener(this);

        ImageView ivShowTrackables = ( ImageView )findViewById( R.id.activity_free_mode_show_trackables );
        ivShowTrackables.setTag(SHOW_TRACKABLES);
        ivShowTrackables.setOnClickListener(this);

        ivAnnouncementsFree = (ImageView)findViewById(R.id.ivAnnouncementsFree);
        ivAnnouncementsFree.setTag(LAST_ANNOUNCEMENT);
        ivAnnouncementsFree.setOnClickListener(this);

        ivReportsFree = (ImageView)findViewById(R.id.ivReportsFree);
        ivReportsFree.setTag(REPORTS);
        ivReportsFree.setOnClickListener(this);

        ivGoToRefuge = ( ImageView )findViewById( R.id.activity_free_mode_go_to_refuge );
        ivGoToRefuge.setTag(GO_TO_REFUGE);
        ivGoToRefuge.setOnClickListener(this);

        GetRouteTrackablesFreeModeTask getRouteTrackablesFreeModeTask = new GetRouteTrackablesFreeModeTask( this );
        getRouteTrackablesFreeModeTask.execute(WebServices.URL_ROUTE_TRACKABLES, ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance().getToken(), ServiceKeys.ID_ROUTE_KEY, route.getId());

        startRouteFragment = new StartRouteFragment();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.rlOutOfMapContainer, startRouteFragment)
                .setCustomAnimations(R.anim.startroute_up,
                        R.anim.startroute_upper).show(startRouteFragment)
                .commit();

        assistanceWithNoveltiesFragment = new AssistanceWithNoveltiesFragment();
        ((AssistanceWithNoveltiesFragment)assistanceWithNoveltiesFragment).setType(AssistanceWithNoveltiesFragment.ASSISTANCE_FRAGMENT_TYPE_FREE);
        getSupportFragmentManager().beginTransaction( ).add( R.id.rlOutOfMapContainer, assistanceWithNoveltiesFragment ).setCustomAnimations( R.anim.stopdetails_down, R.anim.stopdetails_up ).hide( assistanceWithNoveltiesFragment )
                .commit( );

        eventFragment = new EventFragment();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.rlOutOfMapContainer, eventFragment)
                .setCustomAnimations(R.anim.event_down, R.anim.event_up)
                .hide(eventFragment).commit();



        mvMap.setOnTouchListener( this );
        FreeModeLocationManager.getInstance( ).setup( mvMap, this );

        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
        }
    }

    @Override
    public void onClick( View v )
    {
        String tag = v.getTag( ).toString( );

        if( tag.equals( LEFT_MENU ) )
        {
            if( svLeftMenu.getX( ) == 0 )
            {
                svLeftMenu.animate( ).translationX( -svLeftMenu.getWidth( ) );
            }
            else
            {
                svLeftMenu.scrollTo( 0, 0 );
                svLeftMenu.animate( ).translationX( 0 );
            }
        }
        else if( tag.equals( MY_ROUTE ) )
        {
            Intent intent = new Intent( this, RouteProfile.class );
            startActivity( intent );
        }
        else if( tag.equals( REPORTS ) )
        {
            ReportFragment reportFragment = new ReportFragment( );
            reportFragment.show( getSupportFragmentManager( ), Parameters.REPORT_DIALOG_TAG );
        }
        else if( tag.equals( MESSAGES ) )
        {
            Intent intent = new Intent( this, ChatList.class );
            startActivity( intent );
        }
        else if( tag.equals( LOG_OUT ) )
        {
            CloseSessionDialogFragment dialog = new CloseSessionDialogFragment( );
            dialog.show( getSupportFragmentManager( ), Parameters.CLOSE_SESSION_DIALOG_TAG );
        }
        else if( tag.equals( ROUTE_FINISHED ) )
        {
            FreeModeEventsManager.getInstance( ).sendRouteEndedEvent( );
            Intent i = new Intent(this, RouteList.class);
            startActivity(i);
            finish();
            OnTrackManager.getInstance().resetRoute();
            FreeModeManager.getInstance().reset();
        }
        else if( tag.equals( SHOW_TRACKABLES ) )
        {
            showAssistanceWithNovelties( FreeModeManager.getInstance( ).getRouteTrackables( ) );
        }else if(tag.equals(LAST_ANNOUNCEMENT)){
            MessageManager.getInstance().playLastAnnouncement();
        }
        else if( tag.equals( GO_TO_REFUGE ) )
        {
            if( FreeModeLocationManager.getInstance( ).getLocation( ) != null )
            {


                speak(getResources().getString(R.string.refuge_confirmation));
                AlertDialog alertDialog = new AlertDialog.Builder(FreeMode.this).create();
                alertDialog.setTitle(getResources().getString(R.string.refuge_title));
                alertDialog.setMessage(getResources().getString(R.string.refuge_confirmation));
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.yes),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                com.mapbox.mapboxsdk.geometry.LatLng latLng = new com.mapbox.mapboxsdk.geometry.LatLng( FreeModeLocationManager.getInstance( ).getLocation().getLatitude( ), FreeModeLocationManager.getInstance( ).getLocation().getLongitude( ) );

                                //Refuge closestRefuge = OnTrackManager.getInstance( ).getClosestRefuge( new com.mapbox.mapboxsdk.geometry.LatLng( -0.240697, -78.505366 ) );
                                Refuge closestRefuge = OnTrackManager.getInstance( ).getClosestRefuge( latLng );

                                //OnTrackManager.getInstance( ).showDirectionsToRefuge( this, new com.mapbox.mapboxsdk.geometry.LatLng( -0.240697, -78.505366 ), closestRefuge );
                                OnTrackManager.getInstance( ).showDirectionsToRefuge(FreeMode.this, latLng, closestRefuge);
                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getResources().getString(R.string.no),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            }
        }

    }

    @Override
    public void onGlobalLayout( )
    {
        svLeftMenu.setX(-svLeftMenu.getWidth());
    }

    public void showAssistanceWithNovelties( ArrayList<Trackable> trackables )
    {
        ( ( AssistanceWithNoveltiesFragment )assistanceWithNoveltiesFragment ).setStudentList( trackables );
        FragmentTransaction ft = getSupportFragmentManager( ).beginTransaction( );
        ft.setCustomAnimations( R.anim.stopdetails_down, R.anim.stopdetails_up );
        ft.show(assistanceWithNoveltiesFragment);
        ft.commit();
    }

    public void hideAssistanceWithNovelties( )
    {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.stopdetails_down, R.anim.stopdetails_up);
        ft.hide(assistanceWithNoveltiesFragment);
        ft.commit();
    }

    @Override
    public boolean onTouch( View v, MotionEvent event )
    {
        if( svLeftMenu.getX( ) == 0 )
        {
            svLeftMenu.animate( ).translationX( -svLeftMenu.getWidth( ) );
        }

        return false;
    }

    public void showButtonGoToRefuge( )
    {
        ivGoToRefuge.setVisibility(View.VISIBLE);
    }

    public void configureAndShowAssistanceWithNoveltiesFragmentPrestart(){
        ((AssistanceWithNoveltiesFragment) assistanceWithNoveltiesFragment)
                .setType(AssistanceWithNoveltiesFragment.ASSISTANCE_FRAGMENT_TYPE_PRESTART);
        showAssistanceWithNovelties(FreeModeManager.getInstance()
                .getRouteTrackables());
    }

    public void showAssistanceTypeDialog(){
        if(OnTrackManager.getInstance().isDelivery()){
            speak(getResources().getString(R.string.voice_report_select_assistance_type_delivery));
        }else{
            speak(getResources().getString(R.string.voice_report_select_assistance_type_pickup));
        }

        AssistanceTypeFreeDialogFragment assistanceTypeDialogFragment = new AssistanceTypeFreeDialogFragment();
        assistanceTypeDialogFragment.setCancelable(false);
        assistanceTypeDialogFragment.show(getSupportFragmentManager(), "assistancetypedialogfragment");
    }

    public void setAssistanceWithNoveltiesFragmentType(int type){
        ((AssistanceWithNoveltiesFragment)assistanceWithNoveltiesFragment).setType(type);
    }

    public void speak(String text) {
        final String textForTts = text;

        onTrackVoice.speak(textForTts, TextToSpeech.QUEUE_ADD, null);

    }

    public void onDetailedRouteLoaded(){
        if(startAutomatically) {
            if(OnTrackManager.getInstance().isWaitingGPS()){
                ((StartRouteFragment) startRouteFragment)
                        .setFragmentTest(getResources().getString(R.string.waiting_location));
            }else{
                startRoute();
            }
        }else if((!OnTrackManager.getInstance().isWaitingGPS() || OnTrackManager.getInstance().isSimulating())) {
            configureStartRouteFragmentForStart();
        }else{
            ((StartRouteFragment) startRouteFragment)
                    .setFragmentTest(getResources().getString(R.string.waiting_location));
        }

        OnTrackManager.getInstance().setRouteDetailsLoaded(true);
    }

    public void configureStartRouteFragmentForStart(){
        ((StartRouteFragment) startRouteFragment).hideProgressBar();
        ((StartRouteFragment) startRouteFragment)
                .setFragmentTest(getResources().getString(R.string.start_route));

        ((StartRouteFragment) startRouteFragment).setButtonEnabled(true);
        OnTrackManager.getInstance().setWaitingGPS(false);

    }

    public void startRoute(View v){
        if (((StartRouteFragment) startRouteFragment).isButtonEnabled()) {
            showAssistanceTypeDialog();
        }
    }

    public void startRoute(){
        ((StartRouteFragment) startRouteFragment).showProgressBar();
        ((StartRouteFragment) startRouteFragment)
                .setFragmentTest(GeneralMessages.STARTING_ROUTE_MESSAGE);
        ((StartRouteFragment) startRouteFragment).setButtonEnabled(false);
        RequestFootprintTaskFreeMode task = new RequestFootprintTaskFreeMode(this);
        try {
            task.execute(OnTrackManagerAsyncTasks.URL_BASE_FOOTPRINT, ServiceKeys.FK_ROUTE_KEY, route.getId(), ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance().getToken(), ServiceKeys.DATE_KEY,
                    Timestamp.completeTimestamp(), ServiceKeys.SELECTED_TRACKABLES_KEY, FreeModeManager.getInstance().getSelectedTrackablesJSONString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void onFootprintLoaded(){
        FragmentTransaction ft = getSupportFragmentManager()
                .beginTransaction();

        // Esconder el fragment de inicio de ruta
        ft.setCustomAnimations(R.anim.startroute_up,
                R.anim.startroute_upper).hide(startRouteFragment)
                .commit();

        if(OnTrackManager.getInstance().isDelivery()){
            if(!FreeModeManager.getInstance().isPreroute()) {
                speak(getResources().getString(R.string.free_mode_start_delivery));
            }else{
                speak(getResources().getString(R.string.free_mode_start_delivery_preroute));
            }
        }else{
            if(!FreeModeManager.getInstance().isPreroute()) {
                speak(getResources().getString(R.string.free_mode_start_pickup));
            }else{
                speak(getResources().getString(R.string.free_mode_start_pickup_preroute));
            }
        }
    }

    public boolean getStartAutomatically(){
        return this.startAutomatically;
    }

    @Override
    public void onETASUpdate() {

    }

    @Override
    public void onAsyncTaskError(String error) {
        final String mError = error;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(FreeMode.this, mError, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onDetailedRouteLoaded(Route route) {

    }

    @Override
    public void onRouteDecoded(ArrayList<Stop> stops) {

    }

    @Override
    public void onPortionOfRouteDecoded(Stop stop) {

    }

    @Override
    public void onVehicleJustStoppedAtStop(boolean isDelivery, Stop stop, int numberOfStops, int estimatedTimeFromCurrentStop) {

    }

    @Override
    public void onVehicleJustLeftCurrentStop(Stop nextStop) {

    }

    @Override
    public void onRouteStarted(int currentStopOrder) {

    }

    @Override
    public void onRouteFinished() {

    }

    @Override
    public void onSpeedExceeded(int speed, boolean isSpeedExceeded) {

    }

    @Override
    public void onSpeedReportSent() {

    }

    @Override
    public void onSpeedNormal(int speed) {

    }

    @Override
    public void onVehicleApproachingToNextStop(boolean isDelivery, Stop nextStop, int numberOfStops) {

    }

    @Override
    public void onDisconnectLocation() {

    }

    @Override
    public void onLastInfoSent() {

    }

    @Override
    public void onStopCancelled(Stop stop) {

    }

    @Override
    public void onVehicleVeryCloseToNextStop(Stop nextStop) {

    }

    @Override
    public void onEstimatedTimesChanged() {

    }

    @Override
    public void onPreComputingPortionOfRoute() {

    }

    @Override
    public void onVehicleMightStopAtStrangeStop(Stop stop) {

    }

    @Override
    public void onVehicleMightCancelSkippedStop(Stop stop) {

    }

    @Override
    public void onVehicleStartedMoving() {

    }

    @Override
    public void onNoveltiesLoaded() {

    }

    @Override
    public void processSimulated(Location location) {

    }



    @Override
    public void onReceiveMessage(final ArrayList<Long> idsOfModifiedChatCards) {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                if (idsOfModifiedChatCards.size() > 0) {
                    long id = idsOfModifiedChatCards.get(idsOfModifiedChatCards
                            .size() - 1);

                    // ChatCard currentChatCard = MessageManager.getInstance()
                    // .getChatCardById(id);
                    ChatCard currentChatCard = OnTrackManager.getInstance()
                            .getChatCardFromLocalDB(id);
                    Message message = currentChatCard.getMessages().get(
                            currentChatCard.getMessages().size() - 1);
                    speak(GeneralMessages.VOICE_NEW_MESSAGE_FROM
                            + message.getChatUser().getFullName() + ": "
                            + message.getMessageText());

                    showEventFragment(message);

                }
            }
        });
    }

    @Override
    public void onReceivePusherEvent(Event event) {
        final Event finalEvent = event;
        if (event.getCategory().equals(Event.ROUTE_MESSAGE_CODE)
                || event.getCategory().equals(Event.GENERAL_MESSAGE_CODE)) {
            EventManager.getInstance().setLastAnnouncement(finalEvent);
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    if (finalEvent != null) {
//							eventsAdapter.notifyDataSetChanged();
                        speak("Atención: " + finalEvent.getDescription());
                            showEventFragment(finalEvent);
                    }
                }
            });
        }
    }

    @Override
    public void onReceivePusherReport(Report report) {
        final Report innerReport = report;
        runOnUiThread(new Runnable() {
                          @Override
                          public void run() {
                              int reportImageId = -1;
                              String category = innerReport.getCategory();
                              String message = GeneralMessages.REPORT_COORDINATOR_REVIEW;
                              if (category.equals(Report.ACCIDENT_REPORT_CODE)) {
                                  message += GeneralMessages.REPORT_ACCIDENT;
                                  reportImageId = R.drawable.accidente_forposter;

                              } else if (category.equals(Report.TRAFFIC_REPORT_CODE)) {
                                  message += GeneralMessages.REPORT_TRAFFIC;
                                  reportImageId = R.drawable.trafico_forposter;

                              } else if (category
                                      .equals(Report.MECHANICAL_FAILURE_REPORT_CODE)) {
                                  message += GeneralMessages.REPORT_MECHANICAL_FAILURE;
                                  reportImageId = R.drawable.mecanico_forposter;

                              } else if (category.equals(Report.WRONG_ROUTE_REPORT_CODE)) {
                                  message += GeneralMessages.REPORT_WRONG_ROUTE;
                                  reportImageId = R.drawable.desvio_forposter;

                              } else if (category.equals(Report.HEALTH_PROBLEM_REPORT_CODE)) {
                                  message += GeneralMessages.REPORT_HEALTH_PROBLEM;
                                  reportImageId = R.drawable.enfermo_forposter;

                              } else if (category
                                      .equals(Report.BEHAVIOUR_PROBLEM_REPORT_CODE)) {
                                  message += GeneralMessages.REPORT_HEALTH_PROBLEM;
                                  reportImageId = R.drawable.comportamiento_forposter;

                              }

                              int state = innerReport.getState();
                              if (state == Report.REPORT_STATE_PENDING) {
                                  message += ". " + GeneralMessages.REPORT_CHECKED;
                              } else if (state == Report.REPORT_STATE_ANSWERED) {
                                  message += " " + GeneralMessages.REPORT_ANSWER_IS + ": "
                                          + innerReport.getResponse();
                              }

                              speak(Parameters.ATTENTION_TITLE + ": " + message);
                              showEventFragment(Parameters.ATTENTION_TITLE, message,
                                          reportImageId);

                          }
                      }

        );
    }

    @Override
    public void onMessagesRead(long chatCardId) {

    }

    public void showEventFragment(String sender, String message, int imageId) {
        ((EventFragment) eventFragment).setMessage(sender, message, imageId);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.event_down, R.anim.event_up);
        ft.show(eventFragment);
        ft.commit();

        final Timer t = new Timer();
        t.schedule(new TimerTask() {
            public void run() {
                eventCounter++;
                if (eventCounter == POSTER_DURATION) {
                        hideEventFragment();

                    // Finalizar el timer del event poster
                    eventCounter = 0;
                    t.cancel();
                    t.purge();

                }
            }

        }, 1000, 1000);
    }

    public void showEventFragment(Event event) {
        ((EventFragment) eventFragment).setMessage(Parameters.ATTENTION_TITLE,
                event.getDescription(), R.drawable.parlante);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.event_down, R.anim.event_up);
        ft.show(eventFragment);
        ft.commit();

        final Timer t = new Timer();
        t.schedule(new TimerTask() {
            public void run() {
                eventCounter++;
                if (eventCounter == MESSAGE_DURATION) {
                        hideEventFragment();

                    // Finalizar el timer del event poster
                    eventCounter = 0;
                    t.cancel();
                    t.purge();

                }
            }

        }, 1000, 1000);
    }

    public void showEventFragment(Message message) {

        ((EventFragment) eventFragment).setMessage(message);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.event_down, R.anim.event_up);
        ft.show(eventFragment);
        ft.commit();

        final Timer t = new Timer();
        t.schedule(new TimerTask() {
            public void run() {
                eventCounter++;
                if (eventCounter == POSTER_DURATION) {
                    hideEventFragment();

                    // Finalizar el timer del event poster
                    eventCounter = 0;
                    t.cancel();
                    t.purge();

                }
            }

        }, 1000, 1000);
    }

    public void hideEventFragment() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.event_down, R.anim.event_up);
        ft.hide(eventFragment);
        ft.commit();
    }

    public void hideEventFragmentForced() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.hide(eventFragment);
        ft.commit();
    }

    @Override
    protected void onDestroy() {
        MessageManager.getInstance().removeListener(this);

        onTrackVoice.shutdown();
        getSupportFragmentManager().getFragments().clear();
        super.onDestroy();
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getResources().getString(R.string.gps_disabled_alert))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.gps_enable_option), new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                });

        final AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onBackPressed() {
        CloseAppDialogFragment dialog = new CloseAppDialogFragment();
        dialog.show(getSupportFragmentManager(),
                Parameters.WRONG_ROUTE_DIALOG_TAG);
    }
}