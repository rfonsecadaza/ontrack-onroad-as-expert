package otr.ontrack_onroad.application;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.widget.Toast;

public class OntrackPhoneStateService extends Service
{
    private TelephonyManager telephonyManager;
    
    private long time;

    @Override
    public void onCreate( )
    {
        telephonyManager = ( TelephonyManager )getSystemService( TELEPHONY_SERVICE );
        telephonyManager.listen( new OntrackPhoneStateListener( this ), PhoneStateListener.LISTEN_SIGNAL_STRENGTHS );
        telephonyManager.listen( new OntrackPhoneStateListener( this ), PhoneStateListener.LISTEN_DATA_CONNECTION_STATE );
        
        time = 0;
        
        super.onCreate( );
    }

    @Override
    public IBinder onBind( Intent intent )
    {
        return null;
    }

    public void notifyPoorSignalStrength( )
    {
        long now = System.currentTimeMillis( );
        
        if( now - time > 180000 )
        {
            time = now;
            Toast toast = Toast.makeText( getApplicationContext( ), "La conexion esta muy debil, puede presentar fallas con la conexion a internet", Toast.LENGTH_SHORT );
            toast.show( );
        }
    }
    
    public void notifyPoorConnection( int networkType )
    {
        String message = "";
        
        if( networkType == TelephonyManager.NETWORK_TYPE_EDGE )
        {
            message = "La conexion a internet esta muy lenta, la aplicacion no funcionara correctamente";
        }
        else
        {
            message = "Es posible que presente una conexion lenta a internet en este momento, si el problema persiste comuniquese con servicio tecnico";
        }
        
        Toast toast = Toast.makeText( getApplicationContext( ), message, Toast.LENGTH_LONG );
        toast.show( );

    }
}
