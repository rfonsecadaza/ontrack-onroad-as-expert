package otr.ontrack_onroad.application;

import java.lang.Thread.UncaughtExceptionHandler;

import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.models.Route;
import otr.ontrack_onroad.parameters.PreferenceKeys;
import otr.ontrack_onroad_debug.activities.SplashScreen;
import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
//import android.support.multidex.MultiDex;

public class MyApplication extends Application
{
    // uncaught exception handler variable
    private UncaughtExceptionHandler defaultUEH;

    // handler listener
    private Thread.UncaughtExceptionHandler _unCaughtExceptionHandler = new Thread.UncaughtExceptionHandler( )
    {
        @Override
        public void uncaughtException( Thread thread, Throwable ex )
        {
            String message = ex.getMessage( ) + "\n\n";

            if( ex.getCause( ) != null )
            {
                for( int i = 0; i < ex.getCause( ).getStackTrace( ).length; i++ )
                {
                    StackTraceElement x = ex.getCause( ).getStackTrace( )[ i ];
                    message += x.toString( );
                }
                
                message += "\n\n";
            }

            for( int i = 0; i < ex.getStackTrace( ).length; i++ )
            {
                StackTraceElement x = ex.getStackTrace( )[ i ];
                message += x.toString( );
            }
            
            SharedPreferences prefs = getSharedPreferences( PreferenceKeys.PREFERENCES_KEY, Context.MODE_PRIVATE );
            SharedPreferences.Editor editor = prefs.edit( );
            editor.putString( PreferenceKeys.EXCEPTION, message );
            
            if( !OnTrackManager.getInstance( ).getRoutes( ).isEmpty( ) && OnTrackManager.getInstance( ).getSelectedRoute( ) >= 0 && OnTrackManager.getInstance( ).getSelectedRoute( ) < OnTrackManager.getInstance( ).getRoutes( ).size( ) )
            {
                Route route = OnTrackManager.getInstance( ).getRoutes( ).get( OnTrackManager.getInstance( ).getSelectedRoute( ) );
                boolean routeDidStart = OnTrackManager.getInstance( ).routeDidStart( );
                
                if( route != null && routeDidStart )
                {
                    editor.putString( PreferenceKeys.ACTIVE_ROUTE, route.getId( ) );
                }
            }
            
            editor.commit( );

            PendingIntent myActivity = PendingIntent.getActivity( getBaseContext( ), 192837, new Intent( getBaseContext( ), SplashScreen.class ), PendingIntent.FLAG_ONE_SHOT );

            AlarmManager alarmManager;
            alarmManager = ( AlarmManager )getSystemService( Context.ALARM_SERVICE );
            alarmManager.set( AlarmManager.ELAPSED_REALTIME_WAKEUP, 15000, myActivity );

            System.exit( 2 );
            defaultUEH.uncaughtException( thread, ex );
        }
    };

    public MyApplication( )
    {
        defaultUEH = Thread.getDefaultUncaughtExceptionHandler( );
        Thread.setDefaultUncaughtExceptionHandler( _unCaughtExceptionHandler );
    }

//    @Override
//    protected void attachBaseContext(Context base) {
//        super.attachBaseContext(base);
//        MultiDex.install(this);
//    }
}
