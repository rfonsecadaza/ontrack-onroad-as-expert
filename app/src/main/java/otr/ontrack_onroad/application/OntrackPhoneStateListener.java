package otr.ontrack_onroad.application;

import java.lang.reflect.Method;

import android.content.Context;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;

public class OntrackPhoneStateListener extends PhoneStateListener
{
    private Context context;

    public OntrackPhoneStateListener( Context context )
    {
        this.context = context;
    }

    @Override
    public void onSignalStrengthsChanged( SignalStrength signalStrength )
    {
        super.onSignalStrengthsChanged( signalStrength );
        
        int sStrength = 0;

        if( signalStrength.isGsm( ) )
        {
            sStrength = signalStrength.getGsmSignalStrength( );
        }

        if( sStrength == 99 )
        {
            try
            {
                Method[] methods = android.telephony.SignalStrength.class.getMethods( );
                for( Method mthd : methods )
                {
                    if( mthd.getName( ).equals( "getLteSignalStrength" ) )
                    {
                        sStrength = ( Integer )mthd.invoke( signalStrength );
                        break;
                    }
                }
            }
            catch( Exception e )
            {
                
            }
        }
        
        if( sStrength < 10 )
        {
            ( ( OntrackPhoneStateService )context ).notifyPoorSignalStrength( );
        }
    }

    @Override
    public void onDataConnectionStateChanged( int state, int networkType )
    {
        super.onDataConnectionStateChanged( state, networkType );

        if( state == TelephonyManager.DATA_CONNECTED )
        {
            if( networkType == TelephonyManager.NETWORK_TYPE_EDGE || networkType == TelephonyManager.NETWORK_TYPE_HSPA )
            {
                ( ( OntrackPhoneStateService )context ).notifyPoorConnection( networkType );
            }
        }
    }
}
