package otr.ontrack_onroad.dao;

import org.json.JSONException;
import org.json.JSONObject;

import otr.ontrack_onroad_debug.activities.R;


public class Event {

	public static final String EVENTS_KEY = "EVENTS";
	public static final String DATE_GENERATED_KEY = "DATE_GENERATED";
	public static final String FK_CATEGORY_KEY = "FK_CATEGORY";
	public static final String DESCRIPTION_KEY = "DESCRIPTION";
	public static final String DESTINATION_ROLE_KEY = "DESTINATION_ROLE";
	public static final String COPY_COORDINATOR_KEY = "COPY_COORDINATOR";
	public static final String FK_ROUTE_KEY = "FK_ROUTE";
	public static final String FK_STOP_KEY = "FK_STOP";
	public static final String FK_TRACKABLE_KEY = "FK_TRACKABLE";
	public static final String FK_FOOTPRINT_KEY = "FK_FOOTPRINT";
	public static final String FK_TRACKED_STOP_KEY = "FK_TRACKED_STOP";
	public static final String FK_TRACKED_STOP_TRACKABLE_KEY = "FK_TRACKED_STOP_TRACKABLE";
	public static final String FK_ORGANIZATION = "FK_ORGANIZATION";

	public static final String START_ROUTE_EVENT_CODE = "EV-F-01";
	public static final String END_ROUTE_EVENT_CODE = "EV-F-02";
	public static final String ROUTE_DELAYED_EVENT_CODE = "EV-F-03";
	public static final String ROUTE_ADVANCED_EVENT_CODE = "EV-F-04";
	public static final String DEVIATED_ROUTE_CODE = "EV-F-05";
	public static final String SPEED_EXCEEDED_CODE = "EV-F-06";
	public static final String GPS_ACCURACY_LOW_CODE = "EV-F-09";
	public static final String REFUGE_CODE = "EV-F-10";

	public static final String VEHICLE_APPROACHING_STOP_CODE = "EV-TS-01";
	public static final String VEHICLE_ARRIVED_STOP_CODE = "EV-TS-02";
	public static final String VEHICLE_LEFT_STOP_CODE = "EV-TS-03";

	public static final String GENERAL_MESSAGE_CODE = "EV-O-01";

	public static final String ROUTE_MESSAGE_CODE = "EV-R-07";
	
	public static final String START_ROUTE_EVENT_TEXT = "Inicio de ruta";
	public static final String END_ROUTE_EVENT_TEXT = "Fin de ruta";
	public static final String DEVIATED_ROUTE_TEXT = "Desvío de la ruta";
	public static final String SPEED_EXCEEDED_TEXT = "Exceso de velocidad";

	public static final String VEHICLE_APPROACHING_STOP_TEXT = "Vehículo cerca del paradero";
	public static final String VEHICLE_ARRIVED_STOP_TEXT = "Vehículo en el paradero";
	public static final String VEHICLE_LEFT_STOP_TEXT = "Vehículo saliendo del paradero";
	
	public static final String VEHICLE_200M_AWAY_FROM_STOP_CODE = "EV-TS-07";
	public static final String VEHICLE_500M_AWAY_FROM_STOP_CODE = "EV-TS-08";
	public static final String VEHICLE_1KM_AWAY_FROM_STOP_CODE = "EV-TS-09";
	
	public static final String VEHICLE_5MIN_AWAY_FROM_STOP_CODE = "EV-TS-05";
	public static final String VEHICLE_10MIN_AWAY_FROM_STOP_CODE = "EV-TS-06";

	public static final String GENERAL_MESSAGE_TEXT = "Mensaje del coordinador";

	public static final String ROUTE_MESSAGE_TEXT = "Mensaje del coordinador";

    private Long id;
    private String date;
    private String category;
    private String description;
    private Integer destinationRole;
    private Integer copyCoordinator;
    private String fkDestination;
    private String fkRoute;
    private Boolean read;

    public Event() {
    }

    public Event(Long id) {
        this.id = id;
    }

    public Event(Long id, String date, String category, String description, Integer destinationRole, Integer copyCoordinator, String fkDestination, String fkRoute, Boolean read) {
        this.id = id;
        this.date = date;
        this.category = category;
        this.description = description;
        this.destinationRole = destinationRole;
        this.copyCoordinator = copyCoordinator;
        this.fkDestination = fkDestination;
        this.fkRoute = fkRoute;
        this.read = read;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getDestinationRole() {
        return destinationRole;
    }

    public void setDestinationRole(Integer destinationRole) {
        this.destinationRole = destinationRole;
    }

    public Integer getCopyCoordinator() {
        return copyCoordinator;
    }

    public void setCopyCoordinator(Integer copyCoordinator) {
        this.copyCoordinator = copyCoordinator;
    }

    public String getFkDestination() {
        return fkDestination;
    }

    public void setFkDestination(String fkDestination) {
        this.fkDestination = fkDestination;
    }

    public String getFkRoute() {
        return fkRoute;
    }

    public void setFkRoute(String fkRoute) {
        this.fkRoute = fkRoute;
    }

    public Boolean getRead() {
        return read;
    }

    public void setRead(Boolean read) {
        this.read = read;
    }

	public String getEventJSONString(String selectedFkKey) throws JSONException {
		JSONObject jsonEvent = new JSONObject();
		jsonEvent.put(Event.DATE_GENERATED_KEY, this.getDate());
		jsonEvent.put(Event.FK_CATEGORY_KEY, this.getCategory());
		jsonEvent.put(Event.DESCRIPTION_KEY, this.getDescription());
		jsonEvent.put(Event.DESTINATION_ROLE_KEY, this.getDestinationRole());
		jsonEvent.put(Event.COPY_COORDINATOR_KEY,
				"" + this.getCopyCoordinator());

		jsonEvent.put(selectedFkKey, fkDestination);

		return "[" + jsonEvent.toString() + "]";

	}
	
	public String getEventJSONStringWithoutSquaredBrackets(String selectedFkKey) throws JSONException {
		JSONObject jsonEvent = new JSONObject();
		jsonEvent.put(Event.DATE_GENERATED_KEY, this.getDate());
		jsonEvent.put(Event.FK_CATEGORY_KEY, this.getCategory());
		jsonEvent.put(Event.DESCRIPTION_KEY, this.getDescription());
		jsonEvent.put(Event.DESTINATION_ROLE_KEY, this.getDestinationRole());
		jsonEvent.put(Event.COPY_COORDINATOR_KEY,
				"" + this.getCopyCoordinator());

		jsonEvent.put(selectedFkKey, fkDestination);

		return  jsonEvent.toString();
	}
	
	
	
	
	// Devolver el identificador de la imagen que corresponde a la categorï¿½a de evento
	public static int getImageIdFromCategory(String category){
		int id = -1;
		if(category.equals(DEVIATED_ROUTE_CODE)){
			id = R.drawable.desvio;
		}else if(category.equals(END_ROUTE_EVENT_CODE)){
			id = R.drawable.fin;
		}else if(category.equals(START_ROUTE_EVENT_CODE)){
			id = R.drawable.inicio;
		}else if(category.equals(VEHICLE_APPROACHING_STOP_CODE)){
			id = R.drawable.bus_esta_cerca;
		}else if(category.equals(VEHICLE_LEFT_STOP_CODE)){
			id = R.drawable.bus_se_fue;
		}else if(category.equals(VEHICLE_ARRIVED_STOP_CODE)){
			id = R.drawable.bus_llego;
		}else if(category.equals(SPEED_EXCEEDED_CODE)){
			id = R.drawable.alerta_velocidad;
		}else if(category.equals(GENERAL_MESSAGE_CODE) || category.equals(ROUTE_MESSAGE_CODE)){
			id = R.drawable.parlante;
		}
		
		return id;
		
	}
	
	// Devolver el texto que corresponde a la categorï¿½a de evento
		public static String getTextFromCategory(String category){
			String text = null;
			if(category.equals(DEVIATED_ROUTE_CODE)){
				text = DEVIATED_ROUTE_TEXT;
			}else if(category.equals(END_ROUTE_EVENT_CODE)){
				text = END_ROUTE_EVENT_TEXT;
			}else if(category.equals(START_ROUTE_EVENT_CODE)){
				text = START_ROUTE_EVENT_TEXT;
			}else if(category.equals(VEHICLE_APPROACHING_STOP_CODE)){
				text = VEHICLE_APPROACHING_STOP_TEXT;
			}else if(category.equals(VEHICLE_LEFT_STOP_CODE)){
				text = VEHICLE_LEFT_STOP_TEXT;
			}else if(category.equals(VEHICLE_ARRIVED_STOP_CODE)){
				text = VEHICLE_ARRIVED_STOP_TEXT;
			}else if(category.equals(SPEED_EXCEEDED_CODE)){
				text = SPEED_EXCEEDED_TEXT;
			}else if(category.equals(GENERAL_MESSAGE_CODE) || category.equals(ROUTE_MESSAGE_CODE)){
				text = GENERAL_MESSAGE_TEXT;
			}
			
			return text;
			
		}

}
