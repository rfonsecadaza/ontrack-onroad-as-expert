package otr.ontrack_onroad.dao;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.DaoConfig;

import otr.ontrack_onroad.dao.ChatUser;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table CHAT_USER.
*/
public class ChatUserDao extends AbstractDao<ChatUser, Long> {

    public static final String TABLENAME = "CHAT_USER";

    /**
     * Properties of entity ChatUser.<br/>
     * Can be used for QueryBuilder and for referencing column names.
    */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "_id");
        public final static Property UserId = new Property(1, String.class, "userId", false, "USER_ID");
        public final static Property FirstName = new Property(2, String.class, "firstName", false, "FIRST_NAME");
        public final static Property LastName = new Property(3, String.class, "lastName", false, "LAST_NAME");
        public final static Property ContactAddress = new Property(4, String.class, "contactAddress", false, "CONTACT_ADDRESS");
        public final static Property MobilePhone = new Property(5, String.class, "mobilePhone", false, "MOBILE_PHONE");
        public final static Property LanPhone = new Property(6, String.class, "lanPhone", false, "LAN_PHONE");
        public final static Property Role = new Property(7, String.class, "role", false, "ROLE");
        public final static Property LastSeen = new Property(8, String.class, "lastSeen", false, "LAST_SEEN");
    };


    public ChatUserDao(DaoConfig config) {
        super(config);
    }
    
    public ChatUserDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "'CHAT_USER' (" + //
                "'_id' INTEGER PRIMARY KEY ," + // 0: id
                "'USER_ID' TEXT," + // 1: userId
                "'FIRST_NAME' TEXT," + // 2: firstName
                "'LAST_NAME' TEXT," + // 3: lastName
                "'CONTACT_ADDRESS' TEXT," + // 4: contactAddress
                "'MOBILE_PHONE' TEXT," + // 5: mobilePhone
                "'LAN_PHONE' TEXT," + // 6: lanPhone
                "'ROLE' TEXT," + // 7: role
                "'LAST_SEEN' TEXT);"); // 8: lastSeen
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "'CHAT_USER'";
        db.execSQL(sql);
    }

    /** @inheritdoc */
    @Override
    protected void bindValues(SQLiteStatement stmt, ChatUser entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        String userId = entity.getUserId();
        if (userId != null) {
            stmt.bindString(2, userId);
        }
 
        String firstName = entity.getFirstName();
        if (firstName != null) {
            stmt.bindString(3, firstName);
        }
 
        String lastName = entity.getLastName();
        if (lastName != null) {
            stmt.bindString(4, lastName);
        }
 
        String contactAddress = entity.getContactAddress();
        if (contactAddress != null) {
            stmt.bindString(5, contactAddress);
        }
 
        String mobilePhone = entity.getMobilePhone();
        if (mobilePhone != null) {
            stmt.bindString(6, mobilePhone);
        }
 
        String lanPhone = entity.getLanPhone();
        if (lanPhone != null) {
            stmt.bindString(7, lanPhone);
        }
 
        String role = entity.getRole();
        if (role != null) {
            stmt.bindString(8, role);
        }
 
        String lastSeen = entity.getLastSeen();
        if (lastSeen != null) {
            stmt.bindString(9, lastSeen);
        }
    }

    /** @inheritdoc */
    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    /** @inheritdoc */
    @Override
    public ChatUser readEntity(Cursor cursor, int offset) {
        ChatUser entity = new ChatUser( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
            cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1), // userId
            cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2), // firstName
            cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3), // lastName
            cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4), // contactAddress
            cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5), // mobilePhone
            cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6), // lanPhone
            cursor.isNull(offset + 7) ? null : cursor.getString(offset + 7), // role
            cursor.isNull(offset + 8) ? null : cursor.getString(offset + 8) // lastSeen
        );
        return entity;
    }
     
    /** @inheritdoc */
    @Override
    public void readEntity(Cursor cursor, ChatUser entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setUserId(cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1));
        entity.setFirstName(cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2));
        entity.setLastName(cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3));
        entity.setContactAddress(cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4));
        entity.setMobilePhone(cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5));
        entity.setLanPhone(cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6));
        entity.setRole(cursor.isNull(offset + 7) ? null : cursor.getString(offset + 7));
        entity.setLastSeen(cursor.isNull(offset + 8) ? null : cursor.getString(offset + 8));
     }
    
    /** @inheritdoc */
    @Override
    protected Long updateKeyAfterInsert(ChatUser entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    /** @inheritdoc */
    @Override
    public Long getKey(ChatUser entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    /** @inheritdoc */
    @Override    
    protected boolean isEntityUpdateable() {
        return true;
    }
    
}
