package otr.ontrack_onroad.fragments;

import java.util.ArrayList;

import otr.ontrack_onroad.adapters.MissingExtraTrackablesAdapter;
import otr.ontrack_onroad.models.Trackable;
import otr.ontrack_onroad_debug.activities.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class MissingExtraTrackablesDialogFragment extends DialogFragment implements OnItemClickListener, OnClickListener
{
    private final static String CONTINUE = "CONTINUE";

    private final static String TRACKABLES = "TRACKABLES";
    
    private final static String ADDING = "ADDING";
    
    private MissingExtraTrackablesAdapter missingExtraTrackablesAdapter;
    
    private ArrayList<Trackable> trackables;
    
    private ArrayList<Trackable> selectedTrackables;
    
    private boolean adding;
    
    private int stopID;
    
    MissingExtraTrackablesListener listener;
    
    public interface MissingExtraTrackablesListener
    {
        public void onContinue( ArrayList<Trackable> trackables, boolean adding, int stopID );
    }

    public void setTrackablesAndAddingAndStopID( ArrayList<Trackable> trackables, boolean adding, int stopID){
        this.trackables = trackables;
        selectedTrackables = new ArrayList<Trackable>( );
        this.adding = adding;
        this.stopID = stopID;
    }
    
    @Override
    public void onAttach( Activity activity )
    {
        super.onAttach( activity );

        try
        {
            listener = ( MissingExtraTrackablesListener )activity;
        }
        catch( ClassCastException e )
        {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException( activity.toString( ) + " must implement SelectTrackablesForStopUpdateListener" );
        }
    }
    
    @Override
    public Dialog onCreateDialog( Bundle savedInstanceState )
    {        
        AlertDialog.Builder builder = new AlertDialog.Builder( getActivity( ) );

        LayoutInflater inflater = getActivity( ).getLayoutInflater( );

        if( adding )
        {
            builder.setTitle( "Seleccione los estudiantes que desea agregar al paradero" );
        }
        else
        {
            builder.setTitle( "Seleccione los estudiantes que desea eliminar del paradero" );
        }

        View dialogView = inflater.inflate( R.layout.dialog_missing_extra_trackables_fragment, null );

        builder.setView( dialogView );
        
        ListView lvStops = ( ListView )dialogView.findViewById( R.id.dialog_missing_extra_trackables_list_view );
        lvStops.setOnItemClickListener( this );
        missingExtraTrackablesAdapter = new MissingExtraTrackablesAdapter( getActivity( ), trackables );
        lvStops.setAdapter( missingExtraTrackablesAdapter );
        
        Button bContinue = ( Button )dialogView.findViewById( R.id.dialog_missing_extra_trackables_continue );
        bContinue.setTag( CONTINUE );
        bContinue.setOnClickListener( this );
        
        if( adding )
        {
            bContinue.setText( "Agregar" );
        }
        else
        {
            bContinue.setText( "Eliminar" );
        }
        
        return builder.create( );
    }

    @Override
    public void onItemClick( AdapterView<?> parent, View view, int position, long id )
    {
        Trackable trackable = ( ( Trackable )missingExtraTrackablesAdapter.getItem( position ) );
        
        if( trackable.isSelected( ) )
        {
            trackable.setSelected( false );
            selectedTrackables.remove( trackable );
        }
        else
        {
            trackable.setSelected( true );
            selectedTrackables.add( trackable );
        }

        missingExtraTrackablesAdapter.notifyDataSetChanged( );
    }

    @Override
    public void onClick( View v )
    {
        String tag = v.getTag( ).toString( );
        
        if( tag.equals( CONTINUE ) )
        {
            listener.onContinue( selectedTrackables, adding, stopID );

            for( Trackable trackable : trackables )
            {
                trackable.setSelected( false );
            }
        }
        
        MissingExtraTrackablesDialogFragment.this.getDialog( ).cancel( );
    } 
}