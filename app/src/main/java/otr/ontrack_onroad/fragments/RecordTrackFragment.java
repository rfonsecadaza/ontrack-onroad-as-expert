package otr.ontrack_onroad.fragments;

import otr.ontrack_onroad.dao.Simulation;
import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.utils.Timestamp;
import otr.ontrack_onroad_debug.activities.MainActivity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class RecordTrackFragment extends DialogFragment {

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Grabación de ruta")
				.setMessage(
						"¿Desea grabar el recorrido actual para usarlo en simulaciones?")
				.setPositiveButton("Sí", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						OnTrackManager.getInstance()
								.setRecordingSimulation(true);
						OnTrackManager.getInstance().setSimulation(
								new Simulation(null, OnTrackManager.getInstance()
												.getUser().getRoute().getId(),
										Timestamp.completeTimestamp()));
						((MainActivity) getActivity()).startRoute();
					}
				})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						OnTrackManager.getInstance().setRecordingSimulation(
								false);
						((MainActivity) getActivity()).startRoute();
					}
				});
		// Create the AlertDialog object and return it
		return builder.create();
	}

}
