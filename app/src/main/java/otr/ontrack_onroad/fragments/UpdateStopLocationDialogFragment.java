package otr.ontrack_onroad.fragments;

import otr.ontrack_onroad.adapters.UpdateStopLocationAdapter;
        import otr.ontrack_onroad.managers.OnTrackManager;
        import otr.ontrack_onroad.models.Organization;
        import otr.ontrack_onroad.models.Stop;
        import otr.ontrack_onroad.parameters.ServiceKeys;
        import otr.ontrack_onroad.parameters.WebServices;
        import otr.ontrack_onroad.tasks.ChangeStopTask;
        import otr.ontrack_onroad.tasks.PrepareStopsForStopUpdateLocationTask;
        import otr.ontrack_onroad.utils.JSONManager;
        import otr.ontrack_onroad_debug.activities.R;
import android.app.AlertDialog;
        import android.app.Dialog;
        import android.app.DialogFragment;
        import android.os.Bundle;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.widget.AdapterView;
        import android.widget.AdapterView.OnItemClickListener;
        import android.widget.ListView;

public class UpdateStopLocationDialogFragment extends DialogFragment implements OnItemClickListener
{
    private UpdateStopLocationAdapter updateStopLocationAdapter;

    @Override
    public Dialog onCreateDialog( Bundle savedInstanceState )
    {
        //Para textos en ecuatoriano
        Organization organization = OnTrackManager.getInstance( ).getUser( ).getOrganizationByID( OnTrackManager.getInstance( ).getRoutes( ).get( OnTrackManager.getInstance( ).getSelectedRoute( ) ).getOrganizationId( ) );
        final boolean ecuatorian = organization.getCreatedBy( ).equals( "superadmin@ontrackschool.ec" );

        AlertDialog.Builder builder = new AlertDialog.Builder( getActivity( ) );

        LayoutInflater inflater = getActivity( ).getLayoutInflater( );

        builder.setTitle( getActivity( ).getString( ecuatorian ? R.string.help_update_stop_location_title_ecuador : R.string.help_update_stop_location_title ) );

        View dialogView = inflater.inflate( R.layout.dialog_update_stop_location_fragment, null );

        builder.setView( dialogView );

        ListView lvStops = ( ListView )dialogView.findViewById( R.id.dialog_update_stop_location_fragment_list_view );
        lvStops.setOnItemClickListener( this );
        updateStopLocationAdapter = new UpdateStopLocationAdapter( getActivity( ) );
        lvStops.setAdapter( updateStopLocationAdapter );

        return builder.create( );
    }

    @Override
    public void onItemClick( AdapterView<?> parent, View view, int position, long id )
    {
        UpdateStopLocationDialogFragment.this.getDialog( ).cancel( );

        Stop stop = ( Stop )updateStopLocationAdapter.getItem( position );

        String stopId = String.valueOf( stop.getId( ) );
        String footprintId = String.valueOf( OnTrackManager.getInstance( ).getUser( ).getFootprint( ) );
        String type = "1";
        String latitude = String.valueOf( OnTrackManager.getInstance( ).getCurrentLocation( ).getLatitude( ) );
        String longitude = String.valueOf( OnTrackManager.getInstance( ).getCurrentLocation( ).getLongitude( ) );
        String order = OnTrackManager.getInstance( ).getNextStop( ) != null ? String.valueOf( OnTrackManager.getInstance( ).getNextStop( ).getOrder( ) ) : String.valueOf( OnTrackManager.getInstance( ).getCurrentStop( ).getOrder( ) + 1 );

        ChangeStopTask changeStopTask = new ChangeStopTask( getActivity( ) );
        changeStopTask.execute( WebServices.URL_CHANGE_STOP, ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance( ).getToken( ), ServiceKeys.STOP_CHANGES_KEY, JSONManager.getJSONForStopChange( stopId, footprintId, type, latitude, longitude, order, null ) );

        //PrepareStopsForStopUpdateLocationTask getStepsForStopUpdateLocation = new PrepareStopsForStopUpdateLocationTask( getActivity( ), stop );
        //GetStepsForStopUpdateLocationTask getStepsForStopUpdateLocation = new GetStepsForStopUpdateLocationTask( getActivity( ), stop.getId( ), OnTrackManager.getInstance( ).getCurrentLocation( ).getLatitude( ), OnTrackManager.getInstance( ).getCurrentLocation( ).getLongitude( ) );
        //getStepsForStopUpdateLocation.execute( );
        //UpdateStopLocationDialogFragment.this.getDialog( ).cancel( );
    }
}
