package otr.ontrack_onroad.fragments;

import java.util.ArrayList;

import otr.ontrack_onroad.adapters.StudentsCurrentStopAdapter;
import otr.ontrack_onroad.managers.FreeModeManager;
import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.models.Organization;
import otr.ontrack_onroad.models.Trackable;
import otr.ontrack_onroad.parameters.Parameters;
import otr.ontrack_onroad_debug.activities.FreeMode;
import otr.ontrack_onroad_debug.activities.MainActivity;
import otr.ontrack_onroad_debug.activities.R;
import otr.ontrack_onroad_debug.activities.TrackableProfile;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class AssistanceWithNoveltiesFragment extends Fragment implements
		OnItemClickListener {
	public static final int ASSISTANCE_FRAGMENT_TYPE_PRESTART = 0;
	public static final int ASSISTANCE_FRAGMENT_TYPE_CHECK = 1;
	public static final int ASSISTANCE_FRAGMENT_TYPE_FREE = 2;
	public static final int ASSISTANCE_FRAGMENT_TYPE_NOVELTY = 3;
	public static final int ASSISTANCE_FRAGMENT_TYPE_NOVELTY_LIST = 4;

	Context context;

	StudentsCurrentStopAdapter adapter;

	TextView tvAllStudents;
	RelativeLayout rlAllStudents;
	GridView gvStudentsWithNovelties;
	Button bReadyAssistance;


	private int type;


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {

		View rootView = inflater.inflate(
				R.layout.fragment_assistance_with_novelties, container, false);

		// path = new Path();

		// ClippingView r = new ClippingView(getActivity());

		tvAllStudents = (TextView)rootView.findViewById( R.id.tvAllStudents );

		rlAllStudents = (RelativeLayout)rootView.findViewById(R.id.rlAllStudents);

		gvStudentsWithNovelties = (GridView) rootView
				.findViewById(R.id.gvStudentsWithNovelties);

		adapter = new StudentsCurrentStopAdapter(getActivity(), null, StudentsCurrentStopAdapter.ADAPTER_TYPE_STOP_HEADERS);




		gvStudentsWithNovelties.setOnItemClickListener(this);
		bReadyAssistance = (Button) rootView
				.findViewById(R.id.bReadyAssistance);

		Typeface tf = Typeface.createFromAsset(getActivity().getAssets(),
				Parameters.LATO_FONT);

		tvAllStudents.setTypeface(tf);
		bReadyAssistance.setTypeface(tf);


		bReadyAssistance.setOnClickListener((new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() instanceof MainActivity) {

                    if (type == ASSISTANCE_FRAGMENT_TYPE_PRESTART) {

						if(OnTrackManager.getInstance().getUser().getRoute().getNumberOfUnknownExceptingNovelties() == 0
								&& OnTrackManager.getInstance().getUser().getRoute().getAllTrackables().size()>0){
							((MainActivity) getActivity()).speak("No puede iniciar ruta si todos los estudiantes están marcados como ausentes.");
							AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
							builder.setTitle("Llamado a lista").setMessage("No puede iniciar ruta si todos los estudiantes están marcados como ausentes")
									.setPositiveButton(R.string.create_route_elements_dialog_ok, new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog, int id) {
											dialog.dismiss();
										}
									});
							builder.show();

						}else {
							((MainActivity) getActivity()).hideAssistanceWithNovelties();
							if (!OnTrackManager.getInstance().isSimulating()
									&& OnTrackManager.getInstance()
									.isSimulationsAllowed()) {
								RecordTrackFragment dialog = new RecordTrackFragment();
								dialog.show(getActivity().getSupportFragmentManager(),
										Parameters.RECORD_SIMULATION_DIALOG_TAG);
							} else {
								ConfirmDeliveryStartFragment dialog = new ConfirmDeliveryStartFragment();
								dialog.show(getActivity().getSupportFragmentManager(),
										Parameters.CONFIRMDELIVERYSTART_DIALOG_TAG);
							}
						}
                    }else{
						( ( MainActivity )getActivity( ) ).hideAssistanceWithNovelties( );
					}
//                    type = ASSISTANCE_FRAGMENT_TYPE_PRESTART;


                } else {
                    if (type == ASSISTANCE_FRAGMENT_TYPE_PRESTART) {
						if(FreeModeManager.getInstance().getNumberOfUnknown() == 0
								&& adapter.getData().size()>0) {
							((FreeMode) getActivity()).speak("No puede iniciar ruta si todos los estudiantes están marcados como ausentes.");
							AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
							builder.setTitle("Llamado a lista").setMessage("No puede iniciar ruta si todos los estudiantes están marcados como ausentes")
									.setPositiveButton(R.string.create_route_elements_dialog_ok, new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog, int id) {
											dialog.dismiss();
										}
									});
							builder.show();
						}else{
							((FreeMode) getActivity()).speak(getResources().getString(R.string.free_mode_confirmation_text));
							ConfirmFreeStartFragment dialog = new ConfirmFreeStartFragment();
							dialog.show(getActivity().getSupportFragmentManager(),
									Parameters.CONFIRMDELIVERYSTART_DIALOG_TAG);
							bReadyAssistance.setText("INICIAR");
							( ( FreeMode )getActivity( ) ).hideAssistanceWithNovelties();
						}
                    }else{
						( ( FreeMode )getActivity( ) ).hideAssistanceWithNovelties();
					}

                }
            }
        }));

		return rootView;
	}

	public void setStudentList(ArrayList<Trackable> trackables) {
		adapter.setData(trackables);
		setCorrectTitle();
		gvStudentsWithNovelties.setAdapter(adapter);
		adapter.notifyDataSetChanged();
	}

	public void setCorrectTitle(){
//		tvAllStudents.setText("TODOS LOS ESTUDIANTES ("+trackables.size()+")");

		//Para textos en ecuatoriano

		Organization organization = OnTrackManager.getInstance().getUser().getOrganizationByID(OnTrackManager.getInstance().getRoutes().get(OnTrackManager.getInstance().getSelectedRoute()).getOrganizationId());
		boolean ecuatorian = organization.getCreatedBy().equals("superadmin@ontrackschool.ec");

		if(this.type == ASSISTANCE_FRAGMENT_TYPE_NOVELTY_LIST){
			int numberNovelties = OnTrackManager.getInstance().getUser().getRoute()
					.numberOfTotalNoveties();
//			String title = getResources().getString(ecuatorian?R.string.trackables_without_stop_ecuador:R.string.trackables_without_stop);
			String title =  getResources().getString(R.string.trackables_novelty_list);
			title += " ("+ numberNovelties+ ")";
			rlAllStudents.setBackgroundColor(getContext().getResources().getColor(R.color.solid_purple));
			tvAllStudents.setText(title);
			bReadyAssistance.setText("LISTO");
		}else if(this.type == ASSISTANCE_FRAGMENT_TYPE_PRESTART){
			int numberOfUnknown;
			if (getActivity() instanceof MainActivity){
				numberOfUnknown = OnTrackManager.getInstance().getUser().getRoute().getNumberOfUnknownExceptingNovelties();
			}else{
				numberOfUnknown = FreeModeManager.getInstance().getNumberOfUnknown();
			}
			int numberTrackables = adapter.getData().size();
			tvAllStudents.setText("LLAMADO A LISTA ("+numberOfUnknown+"/"+numberTrackables+")");
			rlAllStudents.setBackgroundColor(getContext().getResources().getColor(R.color.solid_green));
			bReadyAssistance.setText("INICIAR");
		}else if(this.type == ASSISTANCE_FRAGMENT_TYPE_NOVELTY) {
			String title = getResources().getString(ecuatorian ? R.string.trackables_without_stop_ecuador : R.string.trackables_without_stop);
			title += " (" + adapter.getData().size() + ")";
			tvAllStudents.setText(title);
			rlAllStudents.setBackgroundColor(getContext().getResources().getColor(R.color.solid_purple));
			bReadyAssistance.setText("LISTO");
		}else{
			tvAllStudents.setText("TODOS LOS ESTUDIANTES ("+adapter.getData().size()+")");
			rlAllStudents.setBackgroundColor(getContext().getResources().getColor(R.color.solid_orange));
			bReadyAssistance.setText("LISTO");
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View v, int position, long id) {

		if (this.type == ASSISTANCE_FRAGMENT_TYPE_PRESTART) {
			Trackable trackable = (Trackable) adapter.getItem(position);

			if (trackable != null) {
				// AssistanceDuringRouteFragment dialog = new
				// AssistanceDuringRouteFragment(
				// trackable, adapter);
				// dialog.show(getActivity().getSupportFragmentManager(),
				// "assistanceduringroute");
				if (trackable.getNovelty() == null) {

					if (trackable.getPicked() == Trackable.UNKNOWN) {
						trackable.setPicked(Trackable.NOT_AT_SCHOOL);
					} else if (trackable.getPicked() == Trackable.NOT_AT_SCHOOL)  {
						trackable.setPicked(Trackable.UNKNOWN);
					}

				} else {
					if (trackable.getNovelty().isNotAttending()
							|| trackable.getNovelty().isOutOfCurrentRoute() || trackable.getNovelty().isInCar()) {
						NoveltyDetailsDialogFragment dialog = new NoveltyDetailsDialogFragment();
						dialog.setTrackable(trackable);
						dialog.show(getActivity().getSupportFragmentManager(),
								"noveltydetails");
					} else {
						if (trackable.getPicked() == Trackable.UNKNOWN) {
							trackable.setPicked(Trackable.NOT_AT_SCHOOL);
						} else if (trackable.getPicked() == Trackable.NOT_AT_SCHOOL) {
							trackable
									.setPicked(Trackable.UNKNOWN);
						}
					}
				}
				refreshTitleForAssistance();

			}
			adapter.notifyDataSetChanged();
		} else if (this.type == ASSISTANCE_FRAGMENT_TYPE_CHECK || this.type == ASSISTANCE_FRAGMENT_TYPE_NOVELTY_LIST) {
			Trackable trackable = (Trackable) adapter.getItem(position);
			if (trackable.getNovelty() == null) {
				ArrayList<Trackable> allTrackables = OnTrackManager
						.getInstance().getUser().getRoute().getAllTrackables();

				// Buscar la posiciï¿½n en la que estï¿½ el trackable en el
				// arreglo
				// de
				// todos los trackables
				int pos = 0;
				for (Trackable currentTrackable : allTrackables) {
					if (trackable.equals(currentTrackable)) {
						break;
					}
					pos++;
				}

				OnTrackManager.getInstance().setSelectedTrackable(pos);
				Intent i = new Intent(getActivity(), TrackableProfile.class);
				startActivity(i);
			} else {
				NoveltyDetailsDialogFragment dialog = new NoveltyDetailsDialogFragment();
				dialog.setTrackable(trackable);
				dialog.show(getActivity().getSupportFragmentManager(),
						"noveltydetails");

			}
		} else if (this.type == ASSISTANCE_FRAGMENT_TYPE_FREE){
			//
            Trackable trackable = (Trackable) adapter.getItem(position);
            if(trackable.getPicked()!=Trackable.NOT_AT_SCHOOL) {
                AssistanceFreeModeDialogFragment dialog = new AssistanceFreeModeDialogFragment();
                dialog.setTrackableAndAdapter((Trackable) adapter.getItem(position), adapter);
                dialog.show(getActivity().getSupportFragmentManager(),
                        "freemode");
            }
		}else if (this.type == ASSISTANCE_FRAGMENT_TYPE_NOVELTY){
            //
            Trackable trackable = (Trackable) adapter.getItem(position);
            if(trackable.getPicked()!=Trackable.NOT_AT_SCHOOL) {
                AssistanceExtraDialogFragment dialog = new AssistanceExtraDialogFragment();
                dialog.setTrackableAndAdapter(trackable, adapter);
                dialog.show(getActivity().getSupportFragmentManager(),
                        "extra");
            }
        }

	}

	public void refreshTitleForAssistance() {
		int numberOfUnknown;
		if (getActivity() instanceof MainActivity){
			numberOfUnknown = OnTrackManager.getInstance().getUser().getRoute().getNumberOfUnknownExceptingNovelties();
		}else{
			numberOfUnknown = FreeModeManager.getInstance().getNumberOfUnknown();
		}

		int numberTrackables = adapter.getData().size();
		tvAllStudents.setText("LLAMADO A LISTA ("+numberOfUnknown+"/"+numberTrackables+")");
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
}