package otr.ontrack_onroad.fragments;

import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.models.Stop;
import otr.ontrack_onroad.parameters.Parameters;
import otr.ontrack_onroad_debug.activities.MainActivity;
import otr.ontrack_onroad_debug.activities.R;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class StrangeStopFragment extends Fragment {
	private TextView tvStrangeStopQuestion;
	private Button bConfirmStrangeStop;
	private Button bDenyStrangeStop;

	private Stop stop;


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_strange_stop,
				container, false);
		Typeface tf = Typeface.createFromAsset(getActivity().getAssets(),
				Parameters.LATO_FONT);

		tvStrangeStopQuestion = (TextView) rootView
				.findViewById(R.id.tvStrangeStopQuestion);
		tvStrangeStopQuestion.setTypeface(tf);
		bConfirmStrangeStop = (Button) rootView
				.findViewById(R.id.bConfirmStrangeStop);
		bConfirmStrangeStop.setTypeface(tf);
		bDenyStrangeStop = (Button) rootView
				.findViewById(R.id.bDenyStrangeStop);
		bDenyStrangeStop.setTypeface(tf);

		bConfirmStrangeStop.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				OnTrackManager.getInstance().vehicleJustStoppedAtStrangeStop(stop);
				((MainActivity) getActivity()).hideStrangeStopFragment();

			}
		});

		bDenyStrangeStop.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				((MainActivity) getActivity()).hideStrangeStopFragment();

			}
		});

		return rootView;
	}

	public Stop getStop() {
		return stop;
	}

	public void setStop(Stop stop) {
		this.stop = stop;
	}

	public void setMessage(String text) {
		tvStrangeStopQuestion.setText(text);
	}

}
