package otr.ontrack_onroad.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad_debug.activities.MainActivity;
import otr.ontrack_onroad_debug.activities.R;

/**
 * Created by rodrigoivanf on 24/09/2015.
 */
public class AssistanceTypeDialogFragment extends DialogFragment implements View.OnClickListener{
    private final static String UNNASSISTANTS = "UNNASSISTANTS";

    private final static String ASSISTANTS = "ASSISTANTS";

    private int position;

    private Activity activity;



    @Override
    public Dialog onCreateDialog( Bundle savedInstanceState )
    {
        AlertDialog.Builder builder = new AlertDialog.Builder( getActivity( ) );
        // Get the layout inflater
        LayoutInflater inflater = getActivity( ).getLayoutInflater( );

        builder.setTitle( getResources( ).getString( R.string.action_fragment_dialog_assistance_title ) );

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout

        View dialogView = inflater.inflate( R.layout.dialog_assistance_type_fragment, null );

        builder.setView(dialogView);

        Button bUnassistants = ( Button )dialogView.findViewById( R.id.dialog_assistance_type_fragment_unassistants );
        bUnassistants.setTag(UNNASSISTANTS);
        bUnassistants.setOnClickListener(this);

        Button bAssistants = ( Button )dialogView.findViewById( R.id.dialog_assistance_type_fragment_assistants );
        bAssistants.setTag(ASSISTANTS);
        bAssistants.setOnClickListener( this );

        activity = getActivity( );

        return builder.create( );
    }

    @Override
    public void onClick( View v )
    {
        ((MainActivity)getActivity()).setPrestartListSelected(true);
        String tag = v.getTag( ).toString( );

        if( tag.equals(UNNASSISTANTS) )
        {
            OnTrackManager.getInstance().getUser().getRoute().setAllTrackablesAsUnknown();
        }
        else if( tag.equals(ASSISTANTS) )
        {
           // TODO: Dejar todos los trackables como ausentes
            OnTrackManager.getInstance().getUser().getRoute().setAllTrackablesAsNotAsSchool();
        }


        ((MainActivity)getActivity()).configureAndShowAssistanceWithNoveltiesFragmentPrestart();
        AssistanceTypeDialogFragment.this.getDialog( ).cancel( );
    }


}
