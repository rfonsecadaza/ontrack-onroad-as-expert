package otr.ontrack_onroad.fragments;

import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.models.Organization;
import otr.ontrack_onroad.parameters.Parameters;
import otr.ontrack_onroad_debug.activities.R;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class PosterFragment extends Fragment {
	TextView tvNextStop;
	TextView tvNextStopTitle;
	TextView tvNumberOfStudentsNextStop;


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Para textos en ecuatoriano
		Organization organization = OnTrackManager.getInstance().
				getUser().getOrganizationByID(OnTrackManager.getInstance().getRoutes().get(OnTrackManager.getInstance().getSelectedRoute()).getOrganizationId());
		boolean ecuatorian = organization.getCreatedBy().equals("superadmin@ontrackschool.ec");

		View rootView = inflater.inflate(R.layout.fragment_poster, container,
				false);

		Typeface tf = Typeface.createFromAsset(getActivity().getAssets(),
				Parameters.LATO_FONT);
		tvNextStop = (TextView) rootView.findViewById(R.id.tvNextStop);
		tvNextStop.setTypeface(tf);

		
		tvNextStopTitle = (TextView) rootView.findViewById(R.id.tvNextStopTitle);
		tvNextStopTitle.setTypeface(tf);
		tvNextStopTitle.setText(ecuatorian ? getResources().getString(R.string.next_stop_label_ecuador) : getResources().getString(R.string.next_stop_label));
		
		tvNumberOfStudentsNextStop = (TextView) rootView
				.findViewById(R.id.tvNumberOfStudendsNextStop);
		tvNumberOfStudentsNextStop.setTypeface(tf);
		return rootView;
	}

	public void setNextStopText(String text) {
		tvNextStop.setText(text);
	}

	public void setNumberOfStudentsNextStop(int numberOfStudents) {
		tvNumberOfStudentsNextStop.setText("=" + numberOfStudents);
	}
}
