package otr.ontrack_onroad.fragments;

import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.parameters.GeneralMessages;
import otr.ontrack_onroad_debug.activities.ChatList;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class WrongRouteDialogFragment extends DialogFragment {
	
	 @Override
	    public Dialog onCreateDialog(Bundle savedInstanceState) {
	        // Use the Builder class for convenient dialog construction
		 	final Intent i = new Intent(getActivity(), ChatList.class);
	        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	        builder.setTitle(GeneralMessages.ERROR).setMessage(GeneralMessages.BAD_CONSTRUCTED_ROUTE_ERROR)
	               .setPositiveButton(GeneralMessages.DIALOG_SEND_MESSAGE, new DialogInterface.OnClickListener() {
	                   public void onClick(DialogInterface dialog, int id) {
	                	   startActivity(i);
	                	   OnTrackManager.getInstance().resetRoute();
	                	   getActivity().finish();
	                   }
	               })
	               .setNegativeButton(GeneralMessages.DIALOG_CLOSE_APP, new DialogInterface.OnClickListener() {
	                   public void onClick(DialogInterface dialog, int id) {
	                	   OnTrackManager.getInstance().resetRoute();
	                       getActivity().finish();
	                   }
	               });
	        // Create the AlertDialog object and return it
	        return builder.create();
	    }

}
