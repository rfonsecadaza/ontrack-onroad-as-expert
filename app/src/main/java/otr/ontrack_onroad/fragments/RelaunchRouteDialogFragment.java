package otr.ontrack_onroad.fragments;

import otr.ontrack_onroad_debug.activities.R;
import otr.ontrack_onroad_debug.activities.RouteList;
import android.app.Activity;
import android.app.Dialog;
import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class RelaunchRouteDialogFragment extends DialogFragment implements OnClickListener
{
    private final static String YES = "YES";
    
    private final static String NO = "NO";
    
    public final static String ROUTE_ID = "ROUTE_ID";
    
    private String routeID;
    
    RelaunchRouteDialogListener listener;
    
    public interface RelaunchRouteDialogListener
    {
        public void onClick( );
    }
    
    public static RelaunchRouteDialogFragment newInstance( String routeID )
    {
        RelaunchRouteDialogFragment f = new RelaunchRouteDialogFragment( );

        Bundle args = new Bundle( );
        args.putString( ROUTE_ID, routeID );
        f.setArguments( args );

        return f;
    }

    @Override
    public void onAttach( Activity activity )
    {
        super.onAttach( activity );

        routeID = getArguments( ).getString( ROUTE_ID ).split( "-" )[ 0 ];
        
        try
        {
            listener = ( RouteList )activity;
        }
        catch( ClassCastException e )
        {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException( activity.toString( ) + " must implement RelaunchRouteDialogListener" );
        }
    }
    
    @Override
    public Dialog onCreateDialog( Bundle savedInstanceState )
    {
        AlertDialog.Builder builder = new AlertDialog.Builder( getActivity( ) );
        // Get the layout inflater
        LayoutInflater inflater = getActivity( ).getLayoutInflater( );

        builder.setTitle( getResources( ).getString( R.string.relaunch_route_title ) );

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout

        View dialogView = inflater.inflate( R.layout.dialog_relaunch_route_fragment, null );

        builder.setView( dialogView );

        TextView tvMessage = ( TextView )dialogView.findViewById( R.id.dialog_relaunch_route_fragment_text );
        tvMessage.setText( getActivity( ).getString( R.string.relaunch_route_message, routeID ) );
        
        Button bYes = ( Button )dialogView.findViewById( R.id.dialog_relaunch_route_fragment_yes );
        bYes.setTag( YES );
        bYes.setOnClickListener( this );

        Button bNo = ( Button )dialogView.findViewById( R.id.dialog_relaunch_route_fragment_no );
        bNo.setTag( NO );
        bNo.setOnClickListener( this );

        return builder.create( );
    }

    @Override
    public void onClick( View v )
    {
        String tag = v.getTag( ).toString( );
        
        if( tag.equals( YES ) )
        {
            listener.onClick( );
        }
        else if( tag.equals( NO ) )
        {
            
        }
        
        RelaunchRouteDialogFragment.this.getDialog( ).cancel( );
    }
}
