package otr.ontrack_onroad.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import otr.ontrack_onroad.adapters.StudentsCurrentStopAdapter;
import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.managers.OnTrackManagerAsyncTasks;
import otr.ontrack_onroad.models.Trackable;
import otr.ontrack_onroad.parameters.GeneralMessages;
import otr.ontrack_onroad.parameters.ServiceKeys;

/**
 * Created by rodrigoivanf on 08/10/2015.
 */
public class AssistanceExtraDialogFragment extends DialogFragment{
    Trackable trackable;
    StudentsCurrentStopAdapter adapterToNotify;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction

        String message;
        String deliveryPart = OnTrackManager.getInstance().isDelivery() ? "dejado"
                : "recogido";
        message = GeneralMessages.THE_STUDENT + trackable.getFullname()
                + " debe ser " + deliveryPart + " en la siguiente dirección: ";
        if (trackable.getNovelty() != null) {
            if (!trackable.getNovelty().getStopAddress().equals("null")) {
                message += trackable.getNovelty().getStopAddress()+".";
            } else {
                message = "No se especificó en que dirección debe ser "
                        + deliveryPart + " el estudiante "
                        + trackable.getFullname() + ".";
            }
        }

        if (OnTrackManager.getInstance().isDelivery()) {
            message += " Si el estudiante ya fue recogido por algún acudiente en el lugar asignado, seleccione la opción 'Sí'. Si el estudiante no fue recogido por ningún acudiente en el lugar asignado, seleccione la opción 'No'.";
        } else {
            message += " Si el estudiante ya se subió al vehículo en el lugar asignado, seleccione la opción 'Sí'. Si el estudiante no llegó al lugar asignado, seleccione la opción 'No'.";
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        //((MainActivity) getActivity()).speak(message);
        builder.setTitle(GeneralMessages.DIALOG_NOT_ATVEHICLE_TITLE)
                .setMessage(message)
                .setNeutralButton(GeneralMessages.NO,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                trackable
                                        .setPicked(Trackable.NOT_ASSISTING_DELIVERED);
                                if (adapterToNotify != null) {
                                    adapterToNotify.notifyDataSetChanged();
                                }

                                OnTrackManagerAsyncTasks.executeSendFTStatesTask(OnTrackManagerAsyncTasks.URL_BASE_PICKED_FOOTPRINT_TRACKABLES, ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance().getToken(), ServiceKeys.FOOTPRINTTRACKABLES_KEY, trackable.getFootprintTrackablePickedJSONArrayNormalMode());
                            }
                        })
                .setNegativeButton(GeneralMessages.YES,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                trackable
                                        .setPicked(Trackable.ASSISTING_DELIVERED);
                                if (adapterToNotify != null) {
                                    adapterToNotify.notifyDataSetChanged();
                                }
                                OnTrackManagerAsyncTasks.executeSendFTStatesTask(OnTrackManagerAsyncTasks.URL_BASE_PICKED_FOOTPRINT_TRACKABLES, ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance().getToken(), ServiceKeys.FOOTPRINTTRACKABLES_KEY, trackable.getFootprintTrackablePickedJSONArrayNormalMode());
                            }
                        })
                .setPositiveButton("Cancelar",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dismiss();
                            }
                        });

        // Create the AlertDialog object and return it
        return builder.create();
    }

    public void setTrackableAndAdapter(Trackable trackable, StudentsCurrentStopAdapter adapterToNotify){
        this.trackable = trackable;
        this.adapterToNotify = adapterToNotify;
    }
}
