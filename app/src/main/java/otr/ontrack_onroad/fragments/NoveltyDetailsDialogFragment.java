package otr.ontrack_onroad.fragments;

import java.util.ArrayList;

import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.models.Novelty;
import otr.ontrack_onroad.models.Organization;
import otr.ontrack_onroad.models.Route;
import otr.ontrack_onroad.models.Trackable;
import otr.ontrack_onroad.parameters.GeneralMessages;
import otr.ontrack_onroad_debug.activities.MainActivity;
import otr.ontrack_onroad_debug.activities.R;
import otr.ontrack_onroad_debug.activities.TrackableProfile;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class NoveltyDetailsDialogFragment extends DialogFragment
{
private Trackable trackable;

	@Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
		//Para textos en ecuatoriano

		Organization organization = OnTrackManager.getInstance().getUser().getOrganizationByID(OnTrackManager.getInstance().getRoutes().get(OnTrackManager.getInstance().getSelectedRoute()).getOrganizationId());
		boolean ecuatorian = organization.getCreatedBy().equals("superadmin@ontrackschool.ec");

        // Use the Builder class for convenient dialog construction
		String message = GeneralMessages.THE_STUDENT+trackable.getFullname();
		
		Novelty novelty = trackable.getNovelty();
		if(novelty.getType()==Trackable.NOVELTY_TYPE_NOT_ATTENDING){
			message+= GeneralMessages.HAS_A_NOVELTY+GeneralMessages.NOVELTY_NOT_ASSISTING;
		}else if(novelty.getType()==Trackable.NOVELTY_TYPE_CAR){
			message+= GeneralMessages.HAS_A_NOVELTY+GeneralMessages.NOVELTY_CAR;
			if(!novelty.getParentName().equals("null") && !novelty.getIdentificationParent().equals("null") && !novelty.getHour().equals("null")){
				message += GeneralMessages.NOVELTY_PARENT + novelty.getParentName() + GeneralMessages.NOVELTY_IDENTIFICATION_PARENT + novelty.getIdentificationParent()
						+ GeneralMessages.NOVELTY_HOUR+novelty.getHour()+".";
			}
		}else{
			if(novelty.getFkRouteOrigin().equals(novelty.getFkRouteDestination())){
				if(OnTrackManager.getInstance().getUser().getRoute().getType()==Route.DELIVERY_CODE){
					message+= GeneralMessages.HAS_A_NOVELTY+(getResources().getString(ecuatorian? R.string.novelty_change_stop_delivery_ecuador:R.string.novelty_change_stop_delivery));
				}else{
					message+= GeneralMessages.HAS_A_NOVELTY+(getResources().getString(ecuatorian?R.string.novelty_change_stop_pickup_ecuador:R.string.novelty_change_stop_pickup));
				}
			}else if(novelty.getFkRouteOrigin().equals(OnTrackManager.getInstance().getUser().getRoute().getId())){
				message+= GeneralMessages.HAS_A_NOVELTY+GeneralMessages.NOVELTY_OUT_ROUTE+novelty.getFkRouteDestinationWithoutDash()+".";
			}else{
				message+= "\n"+GeneralMessages.HAS_A_NOVELTY+GeneralMessages.NOVELTY_IN_ROUTE;
			}
		}
		
		if(!novelty.getDescription().equals("null")){
			message += GeneralMessages.NOVELTY_DESCRIPTION+novelty.getDescription();
		}
		
		((MainActivity)getActivity()).speak(message);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(GeneralMessages.DIALOG_NOVELTY_DETAILS).setMessage(message)
               .setNegativeButton(GeneralMessages.OK, new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                	   dismiss();
                   }
               })
               .setPositiveButton(GeneralMessages.CHECK_PROFILE, new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                	   ArrayList<Trackable> allTrackables = OnTrackManager
       						.getInstance().getUser().getRoute().getAllTrackables();

       				// Buscar la posiciï¿½n en la que estï¿½ el trackable en el
       				// arreglo
       				// de
       				// todos los trackables
       				int pos = 0;
       				for (Trackable currentTrackable : allTrackables) {
       					if (trackable.equals(currentTrackable)) {
       						break;
       					}
       					pos++;
       				}

       				OnTrackManager.getInstance().setSelectedTrackable(pos);
       				Intent i = new Intent(getActivity(), TrackableProfile.class);
       				startActivity(i);
                   }
               });
        // Create the AlertDialog object and return it
        return builder.create();
    }

	public void setTrackable(Trackable trackable){
		this.trackable = trackable;
	}

}
