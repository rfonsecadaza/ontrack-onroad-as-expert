package otr.ontrack_onroad.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad_debug.activities.R;

/**
 * Created by windows on 26/07/2016.
 */
public class ReplacementDialogFragment extends DialogFragment {

    private EditText etDriver;
    private EditText etMonitor;
    private EditText etVehiclePlate;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState )
    {
        AlertDialog.Builder builder = new AlertDialog.Builder( getActivity( ) );
        // Get the layout inflater
        LayoutInflater inflater = getActivity( ).getLayoutInflater( );

        builder.setTitle("Datos del recorrido");

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout

        View dialogView = inflater.inflate( R.layout.dialog_replacement, null );

        etDriver = (EditText)dialogView.findViewById(R.id.etDriver);
        etDriver.setText(OnTrackManager.getInstance().getReplacementDriver());

        etMonitor = (EditText)dialogView.findViewById(R.id.etMonitor);
        etMonitor.setText(OnTrackManager.getInstance().getReplacementMonitor());

        etVehiclePlate = (EditText)dialogView.findViewById(R.id.etVehicle);
        etVehiclePlate.setText(OnTrackManager.getInstance().getReplacementVehiclePlate());

        builder.setView( dialogView );

        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                OnTrackManager.getInstance().setReplacementDriver(etDriver.getText().toString());
                OnTrackManager.getInstance().setReplacementMonitor(etMonitor.getText().toString());
                OnTrackManager.getInstance().setReplacementVehiclePlate(etVehiclePlate.getText().toString());
                dismiss();
            }
        });

        return builder.create( );
    }
}
