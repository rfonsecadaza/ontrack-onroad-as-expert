package otr.ontrack_onroad.fragments;

import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.models.Organization;
import otr.ontrack_onroad_debug.activities.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class HelpDialogFragment extends DialogFragment implements OnClickListener
{
    private final static String MISPLACED_STOP = "MISPLACED_STOP";

    private final static String MISSING_STOP = "MISSING_STOP";

    private Activity activity;

    @Override
    public Dialog onCreateDialog( Bundle savedInstanceState )
    {
        //Para textos en ecuatoriano
        Organization organization = OnTrackManager.getInstance().getUser().getOrganizationByID(OnTrackManager.getInstance().getRoutes().get(OnTrackManager.getInstance().getSelectedRoute()).getOrganizationId());
        final boolean ecuatorian = organization.getCreatedBy().equals("superadmin@ontrackschool.ec");

        AlertDialog.Builder builder = new AlertDialog.Builder( getActivity( ) );

        LayoutInflater inflater = getActivity( ).getLayoutInflater();

        builder.setTitle(getActivity().getString(R.string.help_dialog_title));

        View dialogView = inflater.inflate( R.layout.dialog_help_fragment, null );

        builder.setView(dialogView);

        Button bMisplacedStop = ( Button )dialogView.findViewById( R.id.dialog_help_fragment_misplaced_stop );
        bMisplacedStop.setTag(MISPLACED_STOP);
        bMisplacedStop.setText(getResources().getString(ecuatorian ? R.string.help_misplaced_stop_ecuador : R.string.help_misplaced_stop));
        bMisplacedStop.setOnClickListener(this);

        Button bMissingStop = ( Button )dialogView.findViewById( R.id.dialog_help_fragment_missing_stop );
        bMissingStop.setTag( MISSING_STOP );
        bMissingStop.setText(getResources().getString( ecuatorian?R.string.help_missing_stop_ecuador:R.string.help_missing_stop));
        bMissingStop.setOnClickListener( this );

        activity = getActivity( );

        return builder.create( );
    }

    @Override
    public void onClick( View v )
    {
        String tag = v.getTag( ).toString( );

        if( tag.equals( MISPLACED_STOP ) )
        {
            misplacedStop( );
        }
        else if( tag.equals( MISSING_STOP ) )
        {
            missingStop( );
        }

        HelpDialogFragment.this.getDialog( ).cancel( );
    }

    private void misplacedStop( )
    {
        //Para textos en ecuatoriano
        Organization organization = OnTrackManager.getInstance().getUser().getOrganizationByID(OnTrackManager.getInstance().getRoutes().get(OnTrackManager.getInstance().getSelectedRoute()).getOrganizationId());
        final boolean ecuatorian = organization.getCreatedBy().equals("superadmin@ontrackschool.ec");

        AlertDialog.Builder builder = new AlertDialog.Builder( activity );
        builder.setTitle( activity.getString( R.string.help_on_location_title ) );
        builder.setMessage( activity.getString( ecuatorian?R.string.help_on_location_message_ecuador:R.string.help_on_location_message ) );
        builder.setPositiveButton( activity.getString( R.string.help_on_location_yes ), new DialogInterface.OnClickListener( )
        {
            @Override
            public void onClick( DialogInterface dialog, int which )
            {
                AlertDialog.Builder builder = new AlertDialog.Builder( activity );
                builder.setTitle( activity.getString(ecuatorian? R.string.help_stop_exists_title_ecuador:R.string.help_stop_exists_title ) );
                builder.setMessage( activity.getString(ecuatorian? R.string.help_stop_exists_message_ecuador:R.string.help_stop_exists_message ) );
                builder.setPositiveButton( activity.getString( R.string.help_stop_exists_yes ), new DialogInterface.OnClickListener( )
                {
                    @Override
                    public void onClick( DialogInterface dialog, int which )
                    {
                        UpdateStopLocationDialogFragment updateStopLocationDialogFragment = new UpdateStopLocationDialogFragment( );
                        updateStopLocationDialogFragment.show( activity.getFragmentManager( ), "updateStopLocationDialogFragment" );
                    }
                } );
                builder.setNegativeButton( activity.getString( R.string.help_stop_exists_no ), new DialogInterface.OnClickListener( )
                {
                    @Override
                    public void onClick( DialogInterface dialog, int which )
                    {
                        SelectTrackablesForStopUpdateDialogFragment selectTrackablesForStopUpdateDialogFragment = new SelectTrackablesForStopUpdateDialogFragment( );
                        selectTrackablesForStopUpdateDialogFragment.setPermanent( true );
                        selectTrackablesForStopUpdateDialogFragment.show( activity.getFragmentManager( ), "selectTrackablesForStopUpdateDialogFragment" );
                    }
                } );
                builder.setCancelable( false );

                AlertDialog alert = builder.create( );
                alert.show( );
            }
        } );
        builder.setNegativeButton( activity.getString( R.string.help_on_location_no ), new DialogInterface.OnClickListener( )
        {
            @Override
            public void onClick( DialogInterface dialog, int which )
            {
                AlertDialog.Builder builder = new AlertDialog.Builder( activity );
                builder.setTitle( activity.getString( R.string.help_not_on_location_title ) );
                builder.setMessage( activity.getString(ecuatorian? R.string.help_not_on_location_message_ecuador: R.string.help_not_on_location_message ) );
                builder.setPositiveButton( activity.getString( R.string.help_not_on_location_ok ), new DialogInterface.OnClickListener( )
                {
                    @Override
                    public void onClick( DialogInterface dialog, int which )
                    {

                    }
                } );
                builder.setCancelable( false );

                AlertDialog alert = builder.create( );
                alert.show( );
            }
        } );
        builder.setCancelable( false );

        AlertDialog alert = builder.create( );
        alert.show( );
    }

    private void missingStop( )
    {
        AlertDialog.Builder builder = new AlertDialog.Builder( activity );
        builder.setTitle( activity.getString( R.string.help_on_location_title ) );
        builder.setMessage( activity.getString( R.string.help_on_location_message ) );
        builder.setPositiveButton( activity.getString( R.string.help_on_location_yes ), new DialogInterface.OnClickListener( )
        {
            @Override
            public void onClick( DialogInterface dialog, int which )
            {
                SelectTrackablesForStopUpdateDialogFragment selectTrackablesForStopUpdateDialogFragment = new SelectTrackablesForStopUpdateDialogFragment( );
                selectTrackablesForStopUpdateDialogFragment.setPermanent( true );
                selectTrackablesForStopUpdateDialogFragment.show( activity.getFragmentManager( ), "selectTrackablesForStopUpdateDialogFragment" );
            }
        } );
        builder.setNegativeButton( activity.getString( R.string.help_on_location_no ), new DialogInterface.OnClickListener( )
        {
            @Override
            public void onClick( DialogInterface dialog, int which )
            {
                AlertDialog.Builder builder = new AlertDialog.Builder( activity );
                builder.setTitle( activity.getString( R.string.help_not_on_location_title ) );
                builder.setMessage( activity.getString( R.string.help_not_on_location_message ) );
                builder.setPositiveButton( activity.getString( R.string.help_not_on_location_ok ), new DialogInterface.OnClickListener( )
                {
                    @Override
                    public void onClick( DialogInterface dialog, int which )
                    {

                    }
                } );
                builder.setCancelable( false );

                AlertDialog alert = builder.create( );
                alert.show( );
            }
        } );
        builder.setCancelable( false );

        AlertDialog alert = builder.create( );
        alert.show( );
    }
}
