package otr.ontrack_onroad.fragments;

import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.models.Stop;
import otr.ontrack_onroad.parameters.Parameters;
import otr.ontrack_onroad_debug.activities.MainActivity;
import otr.ontrack_onroad_debug.activities.R;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class CancelSkippedStopFragment extends Fragment {
	private TextView tvCancelSkippedStopText;
	private Button bCancelStop;
	private Button bDontCancelStop;

	private Stop stop;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_strange_stop,
				container, false);
		Typeface tf = Typeface.createFromAsset(getActivity().getAssets(),
				Parameters.LATO_FONT);

		tvCancelSkippedStopText = (TextView) rootView
				.findViewById(R.id.tvStrangeStopQuestion);
		tvCancelSkippedStopText.setTypeface(tf);
		bCancelStop = (Button) rootView
				.findViewById(R.id.bConfirmStrangeStop);
		bCancelStop.setTypeface(tf);
		bDontCancelStop = (Button) rootView
				.findViewById(R.id.bDenyStrangeStop);
		bDontCancelStop.setTypeface(tf);

		bCancelStop.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				OnTrackManager.getInstance().cancelStop(stop);
				((MainActivity) getActivity()).hideCancelSkippedStopFragment();
				

			}
		});

		bDontCancelStop.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				((MainActivity) getActivity()).hideCancelSkippedStopFragment();

			}
		});

		return rootView;
	}

	public Stop getStop() {
		return stop;
	}

	public void setStop(Stop stop) {
		this.stop = stop;
	}

	public void setMessage(String text) {
		tvCancelSkippedStopText.setText(text);
	}
}
