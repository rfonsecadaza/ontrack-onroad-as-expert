package otr.ontrack_onroad.fragments;

import otr.ontrack_onroad.parameters.Parameters;
import otr.ontrack_onroad_debug.activities.R;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class ExcessiveSpeedFragment extends Fragment {
	private TextView tvSpeed;
	private TextView tvExcessiveSpeedTitle;
	private TextView tvExcessiveSpeedSubtitle;
	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_excessive_speed,
				container, false);

		// path = new Path();

//		ClippingView r = new ClippingView(getActivity());
		
		Typeface tf = Typeface.createFromAsset(getActivity().getAssets(),
				Parameters.FONT_TITLE);

		tvSpeed = (TextView) rootView.findViewById(R.id.tvSpeed);
		tvSpeed.setTypeface(tf);
		tvExcessiveSpeedTitle=(TextView) rootView.findViewById(R.id.tvExcessiveSpeedTitle);
		tvExcessiveSpeedTitle.setTypeface(tf);
		tvExcessiveSpeedSubtitle=(TextView) rootView.findViewById(R.id.tvExcessiveSpeedSubtitle);
		tvExcessiveSpeedSubtitle.setTypeface(tf);

		return rootView;
	}
	
	public void setSpeed(int speed){
		tvSpeed.setText(""+speed);
	}
}
