package otr.ontrack_onroad.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;

import org.json.JSONException;

import otr.ontrack_onroad.adapters.StudentsCurrentStopAdapter;
import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.managers.OnTrackManagerAsyncTasks;
import otr.ontrack_onroad.models.Organization;
import otr.ontrack_onroad.models.Trackable;
import otr.ontrack_onroad.parameters.GeneralMessages;
import otr.ontrack_onroad.parameters.ServiceKeys;
import otr.ontrack_onroad_debug.activities.FreeMode;
import otr.ontrack_onroad_debug.activities.R;

/**
 * Created by windows on 23/03/2016.
 */
public class ForceStateDialogFragment extends DialogFragment {

    Trackable trackable;
    StudentsCurrentStopAdapter adapter;

    public void setTrackable(Trackable trackable){
        this.trackable = trackable;
    }
    public void setAdapter(StudentsCurrentStopAdapter adapter){this.adapter = adapter;}


    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //Para textos en ecuatoriano

        Organization organization = OnTrackManager.getInstance().getUser().getOrganizationByID(OnTrackManager.getInstance().getRoutes().get(OnTrackManager.getInstance().getSelectedRoute()).getOrganizationId());
        boolean ecuatorian = organization.getCreatedBy().equals("superadmin@ontrackschool.ec");

        boolean isDelivery = OnTrackManager.getInstance().isDelivery();

        String message = "";
        if(isDelivery){
            if(trackable.getPicked()==Trackable.ASSISTING_DELIVERED){
                if(ecuatorian){
                    message =  getResources().getString(R.string.force_state_delivery_from_delivered_ecuador);
                }else{
                    message =  getResources().getString(R.string.force_state_delivery_from_delivered);
                }
            }else if(trackable.getPicked()==Trackable.NOT_ASSISTING_DELIVERED){
                if(ecuatorian){
                    message =  getResources().getString(R.string.force_state_delivery_from_not_delivered_ecuador);
                }else{
                    message =  getResources().getString(R.string.force_state_delivery_from_not_delivered);
                }
            }
        }else{
            if(trackable.getPicked()==Trackable.ASSISTING_DELIVERED){
               message =  getResources().getString(R.string.force_state_pickup_from_pickedup);
            }else if(trackable.getPicked()==Trackable.NOT_ASSISTING_DELIVERED){
                message =  getResources().getString(R.string.force_state_pickup_from_not_pickedup);
            }
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getResources().getString(R.string.force_state_confirmation_title)+": "+trackable.getFullname())
                .setMessage(
                        message)
                .setPositiveButton(GeneralMessages.YES, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

//                        ((MainActivity) getActivity()).startRoute();
//                        ((FreeMode)getActivity()).setAssistanceWithNoveltiesFragmentType(AssistanceWithNoveltiesFragment.ASSISTANCE_FRAGMENT_TYPE_FREE);
//                        ( ( FreeMode )getActivity( ) ).hideAssistanceWithNovelties();
//                        ( ( FreeMode )getActivity( ) ).startRoute();
//                        if(OnTrackManager.getInstance().isDelivery()){
//                            ( ( FreeMode )getActivity( ) ).speak(getResources().getString(R.string.free_mode_start_delivery));
//                        }else{
//                            ( ( FreeMode )getActivity( ) ).speak(getResources().getString(R.string.free_mode_start_pickup));
//                        }
                        if(trackable.getPicked()==Trackable.ASSISTING_DELIVERED){
                            trackable.setPicked(Trackable.NOT_ASSISTING_DELIVERED);
                        }else{
                            trackable.setPicked(Trackable.ASSISTING_DELIVERED);
                        }

                        try
                        {
                            // Log.d("http",OnTrackManager.getInstance().getStopDataJSONString());
                            OnTrackManagerAsyncTasks.executeSendTrackedStopInfoTask( false, OnTrackManagerAsyncTasks.URL_BASE_TRACKED_STOP_INFO, ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance( ).getToken( ), ServiceKeys.TRACKED_STOPS_KEY, OnTrackManager
                                    .getInstance( ).getStopDataJSONString( ) );
                        }
                        catch( JSONException e )
                        {
                            OnTrackManager.getInstance().getListener().onAsyncTaskError( GeneralMessages.JSON_ERROR );
                        }

                        try
                        {
                            OnTrackManagerAsyncTasks.executeSendTSTStatesTask(OnTrackManagerAsyncTasks.URL_BASE_PICKED_TRACKABLES, ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance().getToken(), ServiceKeys.TRACKEDSTOPTRACKABLES_KEY,
                                   trackable.getTrackedStopTrackablePickedJSONArray());
                        }
                        catch( JSONException e )
                        {
                            // TODO Auto-generated catch block
                            e.printStackTrace( );
                        }
                        adapter.notifyDataSetChanged();
                    }
                })
                .setNegativeButton(GeneralMessages.NO, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dismiss();
                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }

}
