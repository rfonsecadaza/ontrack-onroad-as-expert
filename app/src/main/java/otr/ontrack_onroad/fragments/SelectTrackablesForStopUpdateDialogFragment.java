package otr.ontrack_onroad.fragments;

import java.util.ArrayList;

import otr.ontrack_onroad.adapters.SelectTrackablesForStopUpdateAdapter;
import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.models.Organization;
import otr.ontrack_onroad.models.Trackable;
import otr.ontrack_onroad_debug.activities.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;

public class SelectTrackablesForStopUpdateDialogFragment extends DialogFragment implements OnItemClickListener, OnClickListener
{
    private final static String CONTINUE = "CONTINUE";
    
    private SelectTrackablesForStopUpdateAdapter selectTrackablesForStopUpdateAdapter;
    
    private ArrayList<Trackable> trackables;
    
    private boolean permanent;
    
    SelectTrackablesForStopUpdateListener listener;
    
    public interface SelectTrackablesForStopUpdateListener
    {
        public void onContinue( ArrayList<Trackable> trackables, boolean permanent );
    }
    
//    public SelectTrackablesForStopUpdateDialogFragment( boolean permanent )
//    {
//        this.permanent = permanent;
//    }

    public void setPermanent(boolean permanent){
        this.permanent = permanent;
    }
    
    @Override
    public void onAttach( Activity activity )
    {
        super.onAttach( activity );

        try
        {
            listener = ( SelectTrackablesForStopUpdateListener )activity;
        }
        catch( ClassCastException e )
        {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException( activity.toString( ) + " must implement SelectTrackablesForStopUpdateListener" );
        }
    }
    
    @Override
    public Dialog onCreateDialog( Bundle savedInstanceState )
    {
        //Para textos en ecuatoriano
        Organization organization = OnTrackManager.getInstance().getUser().getOrganizationByID(OnTrackManager.getInstance().getRoutes().get(OnTrackManager.getInstance().getSelectedRoute()).getOrganizationId());
        final boolean ecuatorian = organization.getCreatedBy().equals("superadmin@ontrackschool.ec");

        trackables = new ArrayList<Trackable>( );
        
        AlertDialog.Builder builder = new AlertDialog.Builder( getActivity( ) );

        LayoutInflater inflater = getActivity( ).getLayoutInflater( );

        builder.setTitle( getActivity( ).getString(ecuatorian? R.string.help_select_trackables_for_stop_update_title_ecuador:R.string.help_select_trackables_for_stop_update_title ) );

        View dialogView = inflater.inflate( R.layout.dialog_select_trackables_for_stop_update_fragment, null );

        builder.setView( dialogView );
        
        ListView lvStops = ( ListView )dialogView.findViewById( R.id.dialog_select_trackables_for_stop_update_fragment_list_view );
        lvStops.setOnItemClickListener( this );
        selectTrackablesForStopUpdateAdapter = new SelectTrackablesForStopUpdateAdapter( getActivity( ) );
        lvStops.setAdapter( selectTrackablesForStopUpdateAdapter );
        
        Button bContinue = ( Button )dialogView.findViewById( R.id.dialog_select_trackables_for_stop_update_fragment_continue );
        bContinue.setTag( CONTINUE );
        bContinue.setOnClickListener( this );
        
        return builder.create( );
    }

    @Override
    public void onItemClick( AdapterView<?> parent, View view, int position, long id )
    {
        Trackable trackable = ( ( Trackable )selectTrackablesForStopUpdateAdapter.getItem( position ) );
        
        if( trackable.isSelected( ) )
        {
            trackable.setSelected( false );
            trackables.remove( trackable );
        }
        else
        {
            trackable.setSelected( true );
            trackables.add( trackable );
        }

        selectTrackablesForStopUpdateAdapter.notifyDataSetChanged( );
    }

    @Override
    public void onClick( View v )
    {
        String tag = v.getTag( ).toString( );
        
        if( tag.equals( CONTINUE ) )
        {
            listener.onContinue( trackables, permanent );

            for( Trackable trackable : trackables )
            {
                trackable.setSelected( false );
            }
        }
        
        SelectTrackablesForStopUpdateDialogFragment.this.getDialog( ).cancel( );
    } 
}
