package otr.ontrack_onroad.fragments;

import java.util.ArrayList;

import otr.ontrack_onroad.adapters.AssistanceStudentListAdapter;
import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.models.Trackable;
import otr.ontrack_onroad.parameters.GeneralMessages;
import otr.ontrack_onroad_debug.activities.MainActivity;
import otr.ontrack_onroad_debug.activities.TrackableProfile;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class AssistanceFragment extends DialogFragment {

	public static final int TYPE_BEFORE_START = 0;
	public static final int TYPE_UNASSISTANCE_REQUEST_APPROVED = 2;

	Trackable trackable;
	private int type;
	AssistanceStudentListAdapter adapterToNotify;


	public void setTrackableWithDefaultTypeAndAdapter(Trackable trackable){
		this.trackable = trackable;
		this.type = TYPE_BEFORE_START;
		this.adapterToNotify = null;
	}

	public void setTrackableAndTypeWithDefaultAdapter(Trackable trackable, int type){
		this.trackable = trackable;
		this.type = type;
		this.adapterToNotify = null;
	}

	public void setTrackableAndTypeAndAdapter(Trackable trackable, int type,
											  AssistanceStudentListAdapter adapterToNotify){
		this.trackable = trackable;
		this.type = type;
		this.adapterToNotify = adapterToNotify;
	}



	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// Use the Builder class for convenient dialog construction

		String message;
	   if(type==TYPE_BEFORE_START){
			message = GeneralMessages.DIALOG_THE_STUDENT_OPEN_QUOTATION+ trackable.getFullname()
					+ GeneralMessages.DIALOG_THE_STUDENT_CLOSE_QUOTATION_ATVEHICLE;
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			((MainActivity) getActivity()).speak(message);
			builder.setTitle(GeneralMessages.DIALOG_NOT_ATVEHICLE_TITLE)
					.setMessage(message)
					.setNeutralButton(GeneralMessages.NO,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									trackable
											.setPicked(Trackable.NOT_AT_SCHOOL);
									if (adapterToNotify != null) {
										adapterToNotify.notifyDataSetChanged();
									}
								}
							})
					.setNegativeButton(GeneralMessages.YES,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									trackable
											.setPicked(Trackable.UNKNOWN);
									if (adapterToNotify != null) {
										adapterToNotify.notifyDataSetChanged();
									}
								}
							})
					.setPositiveButton(GeneralMessages.CHECK_PROFILE,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									ArrayList<Trackable> allTrackables = OnTrackManager
											.getInstance().getUser().getRoute()
											.getAllTrackables();

									// Buscar la posiciï¿½n en la que estï¿½ el
									// trackable en el arreglo de todos los
									// trackables
									int pos = 0;
									for (Trackable currentTrackable : allTrackables) {
										if (trackable.equals(currentTrackable)) {
											break;
										}
										pos++;
									}

									OnTrackManager.getInstance()
											.setSelectedTrackable(pos);
									Intent i = new Intent(getActivity(),
											TrackableProfile.class);
									startActivity(i);
								}
							});

			// Create the AlertDialog object and return it
			return builder.create();
		}
	   else{
			message = GeneralMessages.DIALOG_CARETAKERS_OF_STUDENT_OPEN_SENTENCE + trackable.getFullname()
					+ GeneralMessages.DIALOG_CARETAKERS_OF_STUDENT_REQUEST_CLOSE_SENTENCE;
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			((MainActivity) getActivity()).speak(message);
			builder.setTitle(GeneralMessages.DIALOG_REQUEST_TITLE)
					.setMessage(message)
					.setPositiveButton(GeneralMessages.OK,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									dismiss();
								}
							});

			// Create the AlertDialog object and return it
			return builder.create();
		}
	}
}
