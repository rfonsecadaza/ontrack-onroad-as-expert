package otr.ontrack_onroad.fragments;

import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.models.Route;
import otr.ontrack_onroad.models.Stop;
import otr.ontrack_onroad_debug.activities.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class HelpTrackablesDialogFragment extends DialogFragment implements OnClickListener
{
    private final static String MISSING = "MISSING";

    private final static String EXTRA = "EXTRA";

    private final static String STOP_ID = "STOP_ID";

    private Activity activity;
    
    private int stopID;
    
    private Route route;
    
    public static HelpTrackablesDialogFragment newInstance( int stopID )
    {
        HelpTrackablesDialogFragment f = new HelpTrackablesDialogFragment( );

        Bundle args = new Bundle( );
        args.putInt( STOP_ID, stopID );
        f.setArguments( args );

        return f;
    }

    @Override
    public void onAttach( Activity activity )
    {
        super.onAttach( activity );

        stopID = getArguments( ).getInt( STOP_ID );
        route = OnTrackManager.getInstance( ).getRoutes( ).get( OnTrackManager.getInstance( ).getSelectedRoute( ) );
    }

    @Override
    public Dialog onCreateDialog( Bundle savedInstanceState )
    {
        AlertDialog.Builder builder = new AlertDialog.Builder( getActivity( ) );

        LayoutInflater inflater = getActivity( ).getLayoutInflater( );

        builder.setTitle( getActivity( ).getString( R.string.help_dialog_title ) );

        View dialogView = inflater.inflate( R.layout.dialog_help_trackables_fragment, null );

        builder.setView( dialogView );

        Button bMissing = ( Button )dialogView.findViewById( R.id.dialog_help_trackables_fragment_missing );
        bMissing.setTag( MISSING );
        bMissing.setOnClickListener( this );

        Button bExtra = ( Button )dialogView.findViewById( R.id.dialog_help_trackables_fragment_extra );
        bExtra.setTag( EXTRA );
        bExtra.setOnClickListener( this );
        
        activity = getActivity( );

        return builder.create( );
    }

    @Override
    public void onClick( View v )
    {
        String tag = v.getTag( ).toString( );
        MissingExtraTrackablesDialogFragment missingExtraTrackablesDialogFragment = null;

        if( tag.equals( MISSING ) )
        {
            missingExtraTrackablesDialogFragment = new MissingExtraTrackablesDialogFragment();
            missingExtraTrackablesDialogFragment.setTrackablesAndAddingAndStopID( route.getTrackables( ), true, stopID );
        }
        else if( tag.equals( EXTRA ) )
        {
            for( Stop stop : route.getStops( ) )
            {
                if( stop.getId( ) == stopID )
                {
                    missingExtraTrackablesDialogFragment = new MissingExtraTrackablesDialogFragment( );
                    missingExtraTrackablesDialogFragment.setTrackablesAndAddingAndStopID(stop.getTrackables( ), false, stopID );
                    break;
                }
            }
        }

        missingExtraTrackablesDialogFragment.show( getFragmentManager( ), "missingExtraTrackablesDialogFragment" );
        HelpTrackablesDialogFragment.this.getDialog( ).cancel( );
    }
}
