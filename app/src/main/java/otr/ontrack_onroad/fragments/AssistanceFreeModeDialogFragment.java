package otr.ontrack_onroad.fragments;

import otr.ontrack_onroad.adapters.StudentsCurrentStopAdapter;
import otr.ontrack_onroad.managers.FreeModeManager;
import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.managers.OnTrackManagerAsyncTasks;
import otr.ontrack_onroad.models.Trackable;
import otr.ontrack_onroad.parameters.ServiceKeys;
import otr.ontrack_onroad.threads.SendFTStatesThread;
import otr.ontrack_onroad_debug.activities.FreeMode;
import otr.ontrack_onroad_debug.activities.R;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

public class AssistanceFreeModeDialogFragment extends DialogFragment {

    Trackable trackable;
    StudentsCurrentStopAdapter adapterToNotify;

//	public AssistanceFreeModeDialogFragment(Trackable trackable,
//											StudentsCurrentStopAdapter adapterToNotify) {
//		this.trackable = trackable;
//		this.adapterToNotify = adapterToNotify;
//	}

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        builder.setTitle(getResources().getString(R.string.action_fragment_student_notification_title));

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout

        View dialogView = inflater.inflate(R.layout.dialog_free_notification_type, null);

        builder.setView(dialogView);

        Button bAssistingDelivered = (Button) dialogView.findViewById(R.id.dialog_free_notification_assisting_delivered);
        bAssistingDelivered.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                trackable
                        .setPicked(Trackable.ASSISTING_DELIVERED);
                if (adapterToNotify != null) {
                    adapterToNotify.notifyDataSetChanged();
                }
                OnTrackManagerAsyncTasks.executeSendFTStatesTask(OnTrackManagerAsyncTasks.URL_BASE_PICKED_FOOTPRINT_TRACKABLES, ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance().getToken(), ServiceKeys.FOOTPRINTTRACKABLES_KEY, trackable.getFootprintTrackablePickedJSONArray());

                dismiss();
            }
        });

        Button bNotAssistingDelivered = (Button) dialogView.findViewById(R.id.dialog_free_notification_notassisting_delivered);
        if(FreeModeManager.getInstance().isPreroute()){
            bNotAssistingDelivered.setVisibility(View.GONE);
        }
        bNotAssistingDelivered.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                trackable
                        .setPicked(Trackable.NOT_ASSISTING_DELIVERED);
                if (adapterToNotify != null) {
                    adapterToNotify.notifyDataSetChanged();
                }

//				OnTrackManagerAsyncTasks.executeSendFTStatesTask(OnTrackManagerAsyncTasks.URL_BASE_PICKED_FOOTPRINT_TRACKABLES, ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance().getToken(), ServiceKeys.FOOTPRINTTRACKABLES_KEY, trackable.getFootprintTrackablePickedJSONArray());
                SendFTStatesThread task = new SendFTStatesThread(OnTrackManagerAsyncTasks.URL_BASE_PICKED_FOOTPRINT_TRACKABLES,
                        ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance().getToken(), ServiceKeys.FOOTPRINTTRACKABLES_KEY, trackable.getFootprintTrackablePickedJSONArray());
                task.start();
                dismiss();
            }
        });
        if (!FreeModeManager.getInstance().isPreroute()) {
            if (OnTrackManager.getInstance().isDelivery()) {
                bAssistingDelivered.setText(getResources().getString(R.string.action_fragment_delivered));
                bNotAssistingDelivered.setText(getResources().getString(R.string.action_fragment_notdelivered));
            } else {
                bAssistingDelivered.setText(getResources().getString(R.string.action_fragment_assisting));
                bNotAssistingDelivered.setText(getResources().getString(R.string.action_fragment_not_assisting));
            }
        }else{
            if (OnTrackManager.getInstance().isDelivery()) {
                bAssistingDelivered.setText(getResources().getString(R.string.action_fragment_delivered_preroute));
//                bNotAssistingDelivered.setText(getResources().getString(R.string.action_fragment_notdelivered));
            } else {
                bAssistingDelivered.setText(getResources().getString(R.string.action_fragment_assisting_preroute));
//                bNotAssistingDelivered.setText(getResources().getString(R.string.action_fragment_not_assisting));
            }
        }

        Button bCancel = (Button) dialogView.findViewById(R.id.bCancelNotification);
        bCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });


        // Create the AlertDialog object and return it
        return builder.create();
    }

    public void setTrackableAndAdapter(Trackable trackable, StudentsCurrentStopAdapter adapterToNotify) {
        this.trackable = trackable;
        this.adapterToNotify = adapterToNotify;
    }
}
