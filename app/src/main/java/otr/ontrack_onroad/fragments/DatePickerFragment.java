package otr.ontrack_onroad.fragments;

import java.util.Calendar;

import otr.ontrack_onroad.fragments.TimePickerFragment.TimePickerDialogListener;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.DatePicker;

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener
{
    public interface DatePickerDialogListener
    {
        public void onDateSet( int year, int month, int day );
    }
    
    DatePickerDialogListener listener;
    
    @Override
    public void onAttach( Activity activity )
    {
        super.onAttach( activity );

        try
        {
            listener = ( DatePickerDialogListener )activity;
        }
        catch( ClassCastException e )
        {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException( activity.toString( ) + " must implement DatePickerDialogListener" );
        }
    }
    
    @Override
    public Dialog onCreateDialog( Bundle savedInstanceState )
    {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance( );
        int year = c.get( Calendar.YEAR );
        int month = c.get( Calendar.MONTH );
        int day = c.get( Calendar.DAY_OF_MONTH );

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog( getActivity( ), this, year, month, day );
    }

    public void onDateSet( DatePicker view, int year, int month, int day )
    {
        listener.onDateSet( year, month, day );
    }
}