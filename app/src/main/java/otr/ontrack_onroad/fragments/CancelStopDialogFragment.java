package otr.ontrack_onroad.fragments;

import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.models.Organization;
import otr.ontrack_onroad.models.Stop;
import otr.ontrack_onroad.parameters.GeneralMessages;
import otr.ontrack_onroad_debug.activities.MainActivity;
import otr.ontrack_onroad_debug.activities.R;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class CancelStopDialogFragment extends DialogFragment{
	private Stop stop;
	

	@Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        //Para textos en ecuatoriano

        Organization organization = OnTrackManager.getInstance().getUser().getOrganizationByID(OnTrackManager.getInstance().getRoutes().get(OnTrackManager.getInstance().getSelectedRoute()).getOrganizationId());
        boolean ecuatorian = organization.getCreatedBy().equals("superadmin@ontrackschool.ec");

        String messageIntro = getResources().getString(ecuatorian? R.string.dialog_cancel_stop_open_quotation_ecuador:R.string.dialog_cancel_stop_open_quotation);
		String message = messageIntro+stop.getOrder()+"? "+stop.namesOfTrackablesToPickOrLeaveAtStop();
		((MainActivity)getActivity()).speak(message);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getResources().getString(ecuatorian?R.string.dialog_cancel_stop_title_ecuador:R.string.dialog_cancel_stop_title)).setMessage(message)
               .setPositiveButton(GeneralMessages.YES, new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                	  OnTrackManager.getInstance().cancelStop(stop);
                   }
               })
               .setNegativeButton(GeneralMessages.NO, new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                       dismiss();
                   }
               });
        // Create the AlertDialog object and return it
        return builder.create();
    }

    public void setStop(Stop stop){
        this.stop = stop;
    }
}
