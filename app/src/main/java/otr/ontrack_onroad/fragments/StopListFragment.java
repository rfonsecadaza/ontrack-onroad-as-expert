package otr.ontrack_onroad.fragments;

import java.util.ArrayList;

import otr.ontrack_onroad.models.Stop;
import otr.ontrack_onroad.parameters.Parameters;
import otr.ontrack_onroad_debug.activities.MainActivity;
import otr.ontrack_onroad_debug.activities.R;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class StopListFragment extends Fragment {

	public static final int MAXIMUM_NUMBER_OF_STOPS_PER_PAGE = 15;

	private static int currentPage = 0;

	private ArrayList<Stop> stops;
	private ArrayList<RelativeLayout> stopButtons;

	private boolean stopSelectionEnabled = false;

	private RelativeLayout rlForward;
	private RelativeLayout rlBackward;

	private LinearLayout llStops;
	
	private LayoutInflater inflater;


	public void setParameters(ArrayList<Stop> stops){
		this.stops = stops;
		this.stopButtons = new ArrayList<RelativeLayout>();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

	    this.inflater = inflater;

		View rootView = inflater.inflate(R.layout.fragment_stop_list,
				container, false);
		llStops = (LinearLayout) rootView.findViewById(R.id.llStops);

		// if(this.stops.size() <= MAXIMUM_NUMBER_OF_STOPS_PER_PAGE){
		final int numberOfStops = this.stops.size();
		
		for (final Stop stop : this.stops) {
			final RelativeLayout rlStopSquare = (RelativeLayout) inflater
					.inflate(R.layout.stop_square, null);
			
			if(stop.isSpecial()){
				((RelativeLayout)rlStopSquare.findViewById(R.id.rlStopSquareInner)).setBackgroundColor(0xffef7a14);
			}
			
			TextView tvSquare = (TextView) rlStopSquare
					.findViewById(R.id.tvStopText);
			tvSquare.setText("" + stop.getOrder());
			Typeface tf = Typeface.createFromAsset(getActivity().getAssets(),
					Parameters.LATO_FONT);
			tvSquare.setTypeface(tf);
			rlStopSquare.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (stopSelectionEnabled) {
						((MainActivity)getActivity()).setAnimationEnabled(false); 
						((MainActivity) getActivity()).showStopDetails(stop);
					}

				}
			});

			rlStopSquare.setTag( stop.getId( ) );
			stopButtons.add(rlStopSquare);

			if (numberOfStops <= getResources().getInteger(R.integer.maximum_number_of_stops_per_page)) {
				llStops.addView(rlStopSquare, new LinearLayout.LayoutParams(0,
						LinearLayout.LayoutParams.MATCH_PARENT,
						1.0f / (numberOfStops)));
			}
		}

		if (numberOfStops >  getResources().getInteger(R.integer.maximum_number_of_stops_per_page)) {

			rlForward = (RelativeLayout) inflater.inflate(R.layout.stop_square,
					null);
			TextView tvSquare = (TextView) rlForward
					.findViewById(R.id.tvStopText);
			Typeface tf = Typeface.createFromAsset(getActivity().getAssets(),
					Parameters.LATO_FONT);
			tvSquare.setTypeface(tf);
			tvSquare.setText(">");
			rlForward.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (stopSelectionEnabled) {
						((MainActivity)getActivity()).setAnimationEnabled(false); 
						int totalPages = (int) Math.ceil(stopButtons.size()
								/  getResources().getInteger(R.integer.maximum_number_of_stops_per_page));
						if (currentPage < totalPages) {
							currentPage++;
						}
						renderCurrentPageStopIndicators();
					}

				}
			});
			
			rlBackward = (RelativeLayout) inflater.inflate(R.layout.stop_square,
					null);
			TextView tvSquareBack = (TextView) rlBackward
					.findViewById(R.id.tvStopText);
			tvSquareBack.setText("<");

			tvSquareBack.setTypeface(tf);
			rlBackward.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (stopSelectionEnabled) {
						((MainActivity)getActivity()).setAnimationEnabled(false); 
						if (currentPage > 0) {
							currentPage--;
						}
						renderCurrentPageStopIndicators();
					}

				}
			});
			renderCurrentPageStopIndicators();

		}

		return rootView;
	}

	public void renderStopIndicators(
			ArrayList<RelativeLayout> visibleStopButtons) {
//		deleteStopIndicatorsFromContainer();
		int numberOfStops = visibleStopButtons.size();
		for (RelativeLayout rlStopButton : visibleStopButtons) {
			llStops.addView(rlStopButton, new LinearLayout.LayoutParams(0,
					LinearLayout.LayoutParams.MATCH_PARENT,
					1.0f / (numberOfStops)));
		}

	}

	public void renderCurrentPageStopIndicators() {
		deleteStopIndicatorsFromContainer();
		
		// Primero se determina el nï¿½mero de pï¿½ginas
		int totalPages = (int) Math.ceil((float)stopButtons.size()
				/  getResources().getInteger(R.integer.maximum_number_of_stops_per_page));
		if(this.currentPage > 0){
			llStops.addView(rlBackward, new LinearLayout.LayoutParams(0,
					LinearLayout.LayoutParams.MATCH_PARENT,
					1.0f / ((float)getResources().getInteger(R.integer.maximum_number_of_stops_per_page))));
		}
		if (this.currentPage < totalPages - 1) {
			ArrayList<RelativeLayout> visibleStopButtons = new ArrayList<RelativeLayout>(
					stopButtons.subList(currentPage
							*  getResources().getInteger(R.integer.maximum_number_of_stops_per_page),
							 getResources().getInteger(R.integer.maximum_number_of_stops_per_page)
									* (currentPage + 1)));
			renderStopIndicators(visibleStopButtons);

			llStops.addView(rlForward, new LinearLayout.LayoutParams(0,
					LinearLayout.LayoutParams.MATCH_PARENT,
					1.0f / (float)( getResources().getInteger(R.integer.maximum_number_of_stops_per_page))));
		}else{
			ArrayList<RelativeLayout> visibleStopButtons = new ArrayList<RelativeLayout>(
					stopButtons.subList(currentPage
							*  getResources().getInteger(R.integer.maximum_number_of_stops_per_page),
							stops.size()));
			renderStopIndicators(visibleStopButtons);
			
		}
		
		
		

	}

	public void deleteStopIndicatorsFromContainer() {
		llStops.removeAllViews();
	}

	public boolean isStopSelectionEnabled() {
		return stopSelectionEnabled;
	}
	
	public int getStopPageFromOrder(int order){
		return order/ getResources().getInteger(R.integer.maximum_number_of_stops_per_page);
	}
	
	public void setStopListVisibleElementsFromStop(Stop stop) {
		int order = stop.getOrder();
		this.currentPage = getStopPageFromOrder(order);
		renderCurrentPageStopIndicators();
		
		
	}

	public void setStopSelectionEnabled(boolean stopSelectionEnabled) {
		this.stopSelectionEnabled = stopSelectionEnabled;
	}


	public void setStopIndicatorAsCancelled(int order) {
		((RelativeLayout)stopButtons.get(order).findViewById(R.id.rlStopSquareInner)).setBackgroundColor(0xffff0000);
		((TextView) stopButtons.get(order).findViewById(R.id.tvStopText))
				.setText("X");
		((TextView) stopButtons.get(order).findViewById(R.id.tvStopText)).setTextColor(0xffffffff);
//		stopButtons.get(order).setOnClickListener(null);
	}

	public void setStopIndicatorAsComplete(int order) {
		RelativeLayout ivCurrentStop = (RelativeLayout) stopButtons.get(order)
				.findViewById(R.id.rlStopSquareInner);
		ivCurrentStop.setBackgroundColor(0xff31A9E1);
//		((TextView) stopButtons.get(order).findViewById(R.id.tvStopText)).setTextColor(0xffffffff);
	}

	public void setStopIndicatorAsNotComplete(int order) {
		RelativeLayout ivCurrentStop = (RelativeLayout) stopButtons.get(order)
				.findViewById(R.id.rlStopSquareInner);
		ivCurrentStop.setBackgroundColor(0xff173642);
//		((TextView) stopButtons.get(order).findViewById(R.id.tvStopText)).setTextColor(0xff000000);
	}
	
	public void setStopIndicatorAsSpecial(int order){
		RelativeLayout ivCurrentStop = (RelativeLayout) stopButtons.get(order)
				.findViewById(R.id.rlStopSquareInner);
		ivCurrentStop.setBackgroundColor(0xffef7a14);
//		((TextView) stopButtons.get(order).findViewById(R.id.tvStopText)).setTextColor(0xff000000);
			
	}

//    @Override
//    public void onActivityCreated( @Nullable Bundle savedInstanceState )
//    {
//        // TODO Auto-generated method stub
//        super.onActivityCreated( savedInstanceState );
//
//        ( ( MainActivity )getActivity( ) ).startAutomatically( );
//    }
//
    public void organizeButtons( )
    {
        ArrayList<RelativeLayout> organizedStopButtons = new ArrayList<RelativeLayout>( );
        
        for( int i = 0; i < stops.size( ); i++ )
        {
            Stop stop = stops.get( i );
            
            for( int j = 0; j < stopButtons.size( ); j++ )
            {
                RelativeLayout rl = stopButtons.get( j );
                int id = Integer.parseInt( rl.getTag( ).toString( ) );
                
                if( id == stop.getId( ) )
                {
                    organizedStopButtons.add( rl );
                    TextView tv = ( TextView )rl.findViewById( R.id.tvStopText );
                    tv.setText( "" + i );
                    break;
                }
            }
        }
        
        stopButtons = organizedStopButtons;
    }
    
    public void addButton( final Stop stop )
    {
        final RelativeLayout rlStopSquare = (RelativeLayout) inflater
                .inflate(R.layout.stop_square, null);
        
        if(stop.isSpecial()){
            ((RelativeLayout)rlStopSquare.findViewById(R.id.rlStopSquareInner)).setBackgroundColor(0xffef7a14);
        }
        
        TextView tvSquare = (TextView) rlStopSquare
                .findViewById(R.id.tvStopText);
        tvSquare.setText("" + stop.getOrder());
        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(),
                Parameters.LATO_FONT);
        tvSquare.setTypeface(tf);
        rlStopSquare.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (stopSelectionEnabled) {
                    ((MainActivity)getActivity()).setAnimationEnabled(false); 
                    ((MainActivity) getActivity()).showStopDetails(stop);
                }

            }
        });
        
        rlStopSquare.setTag( stop.getId( ) );
        stopButtons.add( stop.getOrder( ), rlStopSquare );
        
        int i = 0;
        
        for( RelativeLayout rl : stopButtons )
        {
            TextView tv = ( TextView )rl.findViewById( R.id.tvStopText );
            tv.setText( "" + i );
            i++;
        }
    }
}
