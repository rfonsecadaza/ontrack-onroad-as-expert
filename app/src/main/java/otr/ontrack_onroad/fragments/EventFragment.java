package otr.ontrack_onroad.fragments;

import otr.ontrack_onroad.dao.Message;
import otr.ontrack_onroad.managers.MessageManager;
import otr.ontrack_onroad.utils.RoundedImageView;
import otr.ontrack_onroad.utils.Timestamp;
import otr.ontrack_onroad_debug.activities.R;
import otr.ontrack_onroad_debug.activities.RouteList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class EventFragment extends Fragment {
	private TextView tvEventSender;
	private TextView tvEventMessage;
	private TextView tvEventDate;
	
	private RoundedImageView rivEventIcon;

	private Button bViewNewMessages;
	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_event_message,
				container, false);

	

		tvEventSender = (TextView) rootView.findViewById(R.id.tvEventSender);
		tvEventMessage=(TextView) rootView.findViewById(R.id.tvEventMessage);
		tvEventDate=(TextView) rootView.findViewById(R.id.tvEventDate);
		rivEventIcon = (RoundedImageView)rootView.findViewById(R.id.rivUserImage);

		bViewNewMessages = (Button)rootView.findViewById(R.id.bViewNewMessages);
		bViewNewMessages.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(getActivity() instanceof RouteList){
					((RouteList)getActivity()).gotoNewMessages();
				}
			}
		});
		

		return rootView;
	}

	public void setMessage(String message){
		tvEventSender.setText("Atención");
		tvEventMessage.setText(message);
		tvEventDate.setText(Timestamp.partialTimestamp());
		bViewNewMessages.setVisibility(View.VISIBLE);
	}

	public void setMessage(String sender, String message, int imageId){
		tvEventSender.setText(sender);
		tvEventMessage.setText(message);
		tvEventDate.setText(Timestamp.partialTimestamp());
		rivEventIcon.setImageResource(imageId);
	}
	
	public void setMessage(Message message){
		tvEventSender.setText(message.getChatUser().getFullName());
		tvEventMessage.setText(message.getMessageText());
		tvEventDate.setText(message.getTime());
		rivEventIcon.setImageBitmap(MessageManager.getInstance().getChatCardById(message.getChatCardId()).getImage());
	}


	
}
