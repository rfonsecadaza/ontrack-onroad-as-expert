package otr.ontrack_onroad.fragments;

import org.json.JSONException;

import otr.ontrack_onroad.adapters.AssistanceStudentListAdapter;
import otr.ontrack_onroad.adapters.AssistanceStudentListItem;
import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.managers.OnTrackManagerAsyncTasks;
import otr.ontrack_onroad.managers.ReportManager;
import otr.ontrack_onroad.models.Report;
import otr.ontrack_onroad.models.Trackable;
import otr.ontrack_onroad.parameters.GeneralMessages;
import otr.ontrack_onroad.parameters.Parameters;
import otr.ontrack_onroad.parameters.ServiceKeys;
import otr.ontrack_onroad.utils.Timestamp;
import otr.ontrack_onroad_debug.activities.R;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;

public class AllTrackablesFragment extends DialogFragment {
	public static final int TYPE_HEALTH = 0;
	public static final int TYPE_BEHAVIOUR = 1;
	public static final int TYPE_FIRST_CALL = 2;

	private static int type;
	String details;
	ListView lvAllTrackables;
	Button bReadyToLeave;
	AssistanceStudentListAdapter adapter;

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		Dialog dialog = super.onCreateDialog(savedInstanceState);

		String title = "";
		if (this.type == TYPE_HEALTH) {
			title = GeneralMessages.REPORT_HEALTH_PROBLEMS_TITLE;
		} else if (this.type == TYPE_BEHAVIOUR) {
			title = GeneralMessages.REPORT_BEHAVIOUR_PROBLEMS_TITLE;
		} else {
			title = GeneralMessages.DIALOG_STUDENT_LIST_TITLE;
		}
		dialog.setTitle(title);
		return dialog;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_alltrackables,
				container, false);
		bReadyToLeave = (Button) rootView.findViewById(R.id.bReadyToLeave);

		lvAllTrackables = (ListView) rootView
				.findViewById(R.id.lvAllTrackables);
		adapter = new AssistanceStudentListAdapter(getActivity(),
				OnTrackManager.getInstance().getUser().getRoute()
						.getAllTrackablesWithStopHeaders());
		lvAllTrackables.setAdapter(adapter);

		ViewGroup.LayoutParams layoutParams = lvAllTrackables.getLayoutParams();

		if (type == TYPE_FIRST_CALL) {
			layoutParams.height = 200;
			lvAllTrackables.setLayoutParams(layoutParams);
			bReadyToLeave.setVisibility(View.VISIBLE);
			bReadyToLeave.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					dismiss();
					if (!OnTrackManager.getInstance().isSimulating()
							&& OnTrackManager.getInstance()
									.isSimulationsAllowed()) {
						RecordTrackFragment dialog = new RecordTrackFragment();
						dialog.show(getActivity().getSupportFragmentManager(),
								Parameters.RECORD_SIMULATION_DIALOG_TAG);
					} else {
						ConfirmDeliveryStartFragment dialog = new ConfirmDeliveryStartFragment();
						dialog.show(getActivity().getSupportFragmentManager(),
								Parameters.CONFIRMDELIVERYSTART_DIALOG_TAG);
					}

				}
			});
		}

		lvAllTrackables.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
									int position, long id) {
				if (!(((AssistanceStudentListItem) adapter.getItem(position)).isSection())) {
					if (type != TYPE_FIRST_CALL) {
						int fkDestination = (int) id;
						String category = type == TYPE_BEHAVIOUR ? Report.BEHAVIOUR_PROBLEM_REPORT_CODE
								: Report.HEALTH_PROBLEM_REPORT_CODE;
						Report report = new Report(null, null, Timestamp
								.completeTimestamp(), category, details,
								OnTrackManager.getInstance()
										.getCurrentLocation().getLatitude(),
								OnTrackManager.getInstance()
										.getCurrentLocation().getLongitude(),
								fkDestination,
								(long) OnTrackManager.getInstance().getUser()
										.getFootprint(), null,
								Report.REPORT_STATE_NOT_ANSWERED);

						try {
							ReportManager
									.getInstance()
									.executeSendReportTask(
											OnTrackManagerAsyncTasks.URL_BASE_ADD_REPORTS,
											ServiceKeys.TOKEN_KEY,
											OnTrackManager.getInstance()
													.getToken(),
											ServiceKeys.REPORTS_KEY,
											report.getReportJSONString(Report.FK_TRACKED_STOP_TRACKABLE_KEY));
						} catch (JSONException e) {
							e.printStackTrace();
						}
						dismiss();
					} else {
						Trackable trackable = (Trackable) adapter.getItem(position);
						AssistanceFragment dialog;
						if (trackable.getState() == Trackable.STATE_ATTENDANCE) {
							dialog = new AssistanceFragment();
							dialog.setTrackableAndTypeAndAdapter(trackable,
									AssistanceFragment.TYPE_BEFORE_START,
									adapter);
						} else {
							dialog = new AssistanceFragment();
							dialog.setTrackableAndTypeAndAdapter(trackable,
									AssistanceFragment.TYPE_UNASSISTANCE_REQUEST_APPROVED,
									adapter);
						}
						dialog.show(getActivity().getSupportFragmentManager(),
								Parameters.ASSISTANCE_DIALOG_TAG);

					}

				}
			}
		});

		return rootView;
	}

	public void setTypeAndDetails(int type, String details){
		this.type = type;
		this.details = details;
	}
}
