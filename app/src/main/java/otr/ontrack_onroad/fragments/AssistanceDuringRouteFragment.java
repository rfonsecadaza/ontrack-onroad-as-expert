package otr.ontrack_onroad.fragments;

import java.util.ArrayList;

import otr.ontrack_onroad.adapters.StudentsCurrentStopAdapter;
import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.models.Trackable;
import otr.ontrack_onroad.parameters.GeneralMessages;
import otr.ontrack_onroad_debug.activities.MainActivity;
import otr.ontrack_onroad_debug.activities.TrackableProfile;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class AssistanceDuringRouteFragment extends DialogFragment {

	Trackable trackable;
	StudentsCurrentStopAdapter adapterToNotify;


	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// Use the Builder class for convenient dialog construction

		String message;
			final boolean isDelivery = OnTrackManager.getInstance().isDelivery();
			if (isDelivery) {
				message = GeneralMessages.DIALOG_THE_STUDENT_OPEN_QUOTATION 
						+ trackable.getFullname()
						+ GeneralMessages.DIALOG_THE_STUDENT_CLOSE_QUOTATION_DELIVERED;
			} else {
				message = GeneralMessages.DIALOG_THE_STUDENT_OPEN_QUOTATION 
						+ trackable.getFullname() + GeneralMessages.DIALOG_THE_STUDENT_CLOSE_QUOTATION_PICKEDUP;
			}
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			((MainActivity) getActivity()).speak(message);
			builder.setTitle(
					isDelivery ? GeneralMessages.DIALOG_NOT_DELIVERED_TITLE
							: GeneralMessages.DIALOG_NOT_PICKEDUP_TITLE)
					.setMessage(message)
					.setNeutralButton(GeneralMessages.NO,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									trackable
											.setPicked(Trackable.NOT_ASSISTING_DELIVERED);
									if (adapterToNotify != null) {
										adapterToNotify.notifyDataSetChanged();
									}
								}
							})
					.setNegativeButton(GeneralMessages.YES,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									trackable
											.setPicked(Trackable.ASSISTING_DELIVERED);
									if (adapterToNotify != null) {
										adapterToNotify.notifyDataSetChanged();
									}
								}
							})
					.setPositiveButton(GeneralMessages.CHECK_PROFILE,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									ArrayList<Trackable> allTrackables = OnTrackManager
											.getInstance().getUser().getRoute()
											.getAllTrackables();

									// Buscar la posiciï¿½n en la que estï¿½ el
									// trackable en el arreglo de todos los
									// trackables
									int pos = 0;
									for (Trackable currentTrackable : allTrackables) {
										if (trackable.equals(currentTrackable)) {
											break;
										}
										pos++;
									}

									OnTrackManager.getInstance()
											.setSelectedTrackable(pos);
									Intent i = new Intent(getActivity(),
											TrackableProfile.class);
									startActivity(i);
								}
							});

			// Create the AlertDialog object and return it
			return builder.create();
		
	}

	public void setTrackable(Trackable trackable){
		this.trackable = trackable;
	}

	public void setTrackableAndAdapter(Trackable trackable, StudentsCurrentStopAdapter adapterToNotify){
		this.trackable = trackable;
		this.adapterToNotify = adapterToNotify;
	}
}
