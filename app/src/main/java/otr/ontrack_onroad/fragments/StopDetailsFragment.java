package otr.ontrack_onroad.fragments;

import java.util.ArrayList;

import org.json.JSONException;

import otr.ontrack_onroad.adapters.StudentListAdapter;
import otr.ontrack_onroad.adapters.StudentsCurrentStopAdapter;
import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.managers.OnTrackManagerAsyncTasks;
import otr.ontrack_onroad.models.Organization;
import otr.ontrack_onroad.models.Stop;
import otr.ontrack_onroad.models.Trackable;
import otr.ontrack_onroad.parameters.GeneralMessages;
import otr.ontrack_onroad.parameters.Parameters;
import otr.ontrack_onroad.parameters.ServiceKeys;
import otr.ontrack_onroad.parameters.WebServices;
import otr.ontrack_onroad.tasks.CheckTrackableRemovalFromStopTask;
import otr.ontrack_onroad_debug.activities.MainActivity;
import otr.ontrack_onroad_debug.activities.R;
import otr.ontrack_onroad_debug.activities.RouteList;
import otr.ontrack_onroad_debug.activities.TrackableProfile;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

//import com.google.android.gms.maps.StreetViewPanorama;

public class StopDetailsFragment extends Fragment implements
		OnItemClickListener, OnClickListener {

    private final static String HELP = "HELP";
    
	private boolean isCurrent = false;
	private Stop stop;
	
	LinearLayout llStopDetailsBackground;
	Button bClose;
	Button bManualMode;
	TextView tvStopName;
	TextView tvStopDirection;
	TextView tvStopTime;
	GridView gvStudentList;
//	StreetViewPanorama svp;
	ImageView ivStreetView;
	ImageView bCancelStop;
	StudentsCurrentStopAdapter adapter;
	StudentListAdapter adapterAll;
	private ProgressDialog mProgressDialog;
	ImageView ivStopHelp;

	// private static Path path;


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		// Para textos en ecuatoriano
		Organization organization = OnTrackManager.getInstance().
				getUser().getOrganizationByID(OnTrackManager.getInstance().getRoutes().get(OnTrackManager.getInstance().getSelectedRoute()).getOrganizationId());
		boolean ecuatorian = organization.getCreatedBy().equals("superadmin@ontrackschool.ec");

		View rootView = inflater.inflate(R.layout.fragment_stopdetails,
				container, false);

		// path = new Path();

		// ClippingView r = new ClippingView(getActivity());
		
		llStopDetailsBackground = (LinearLayout) rootView.findViewById(R.id.llStopDetailsBackground);

		Typeface tf = Typeface.createFromAsset(getActivity().getAssets(),
				Parameters.LATO_FONT);

		bCancelStop = (ImageView) rootView.findViewById(R.id.bCancelStopNew);
		bClose = (Button) rootView.findViewById(R.id.bStopInfoOk);
		bManualMode = (Button) rootView.findViewById(R.id.bManualMode);
		bManualMode.setVisibility(View.INVISIBLE);
		bManualMode.setTypeface(tf);

		tvStopName = (TextView) rootView.findViewById(R.id.tvStopName);
		tvStopName.setTypeface(tf);
		tvStopName.setText(ecuatorian?getResources().getString(R.string.stop_singular_caps_ecuador):getResources().getString(R.string.stop_singular_caps));

		tvStopDirection = (TextView) rootView
				.findViewById(R.id.tvStopDirection);
		tvStopDirection.setTypeface(tf);

		tvStopTime = (TextView) rootView.findViewById(R.id.tvStopTime);
		tvStopTime.setTypeface(tf);

		gvStudentList = (GridView) rootView.findViewById(R.id.gvStudentListBig);
		gvStudentList.setOnItemClickListener(this);
		
		ivStopHelp = (ImageView) rootView.findViewById(R.id.ivStopHelp);
		ivStopHelp.setTag( HELP );
		ivStopHelp.setOnClickListener( this );

		bCancelStop.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				CancelStopDialogFragment dialog = new CancelStopDialogFragment();
				dialog.setStop(stop);
				dialog.show(getActivity().getSupportFragmentManager(),
						Parameters.CANCELSTOP_DIALOG_TAG);
			}
		});

		bCancelStop.setOnLongClickListener(new OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {

				CancelStopDialogFragment dialog = new CancelStopDialogFragment();
				dialog.setStop(stop);
				dialog.show(getActivity().getSupportFragmentManager(),
						Parameters.CANCELSTOP_DIALOG_TAG);

				return true;
			}
		});

		setHasStoppedInStopActionToButton();

		adapter = new StudentsCurrentStopAdapter(getActivity(), null, StudentsCurrentStopAdapter.ADAPTER_TYPE_NORMAL);
		adapterAll = new StudentListAdapter(getActivity(), null);

		return rootView;
	}

	public void setStopNameText(String text) {
		tvStopName.setText(text);
	}

	public void setStopDirectionText(String text) {
		tvStopDirection.setText(text);
	}

	public void setStopTimeText(String text) {
		tvStopTime.setText(text);
	}

	public void setStudentList(ArrayList<Trackable> trackables) {
		adapter.setData(trackables);
		gvStudentList.setAdapter(adapter);
		if (trackables.size() > 4) {
			// gvStudentList.setNumColumns(4);
			gvStudentList.setScrollbarFadingEnabled(false);
		} else {
			// gvStudentList.setNumColumns(3);
			gvStudentList.setScrollbarFadingEnabled(false);
		}
		adapter.notifyDataSetChanged();
	}

	public void setManualMode(boolean manualMode) {
		if (manualMode) {
			bManualMode.setVisibility(View.VISIBLE);
		} else {
			bManualMode.setVisibility(View.INVISIBLE);
		}
	}

	public void setHasStoppedInStopActionToButton() {
		bManualMode.setText(GeneralMessages.VEHICLE_AT_STOP);
		bManualMode.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				OnTrackManager.getInstance()
						.setCurrentLocationAsReferenceLocation();
				Stop nextStop = OnTrackManager.getInstance().getNextStop();
				OnTrackManager.getInstance().vehicleJustStoppedAtStop(nextStop);

			}
		});
	}

	public void setHasLeftStopActionToButton() {
		Organization organization = OnTrackManager.getInstance().getUser().getOrganizationByID(OnTrackManager.getInstance().getRoutes().get(OnTrackManager.getInstance().getSelectedRoute()).getOrganizationId());
		boolean ecuatorian = organization.getCreatedBy().equals("superadmin@ontrackschool.ec");
		bManualMode.setText(ecuatorian?getResources().getString(R.string.vehicle_left_stop_ecuador):getResources().getString(R.string.vehicle_left_stop));
		bManualMode.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				OnTrackManager.getInstance().actionLeaveStop();

			}
		});
	}

	public void setFinishRouteActionToButton() {
		bManualMode.setText(GeneralMessages.FINISH_ROUTE);
		bManualMode.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mProgressDialog = ProgressDialog.show(getActivity(),
						GeneralMessages.FINISHING_ROUTE,
						GeneralMessages.PLEASE_WAIT, true);
				OnTrackManager.getInstance().getCurrentStop()
						.changeUnknownToPickedOrDeliveredTrackables();
				if (OnTrackManager.getInstance().getCurrentStop()
						.getTrackables().size() != 0) {
					try {
						OnTrackManagerAsyncTasks
								.executeSendTSTStatesTask(
										OnTrackManagerAsyncTasks.URL_BASE_PICKED_TRACKABLES,
										ServiceKeys.TOKEN_KEY,
										OnTrackManager.getInstance().getToken(),
										ServiceKeys.TRACKEDSTOPTRACKABLES_KEY,
										OnTrackManager
												.getInstance()
												.getCurrentStop()
												.getTrackedStopTrackablesJSONString());
					} catch (JSONException e) {
						((MainActivity) getActivity()).onAsyncTaskError(e
								.getMessage());
					}
				} else {
					Intent i = new Intent(getActivity(), RouteList.class);
					startActivity(i);
					getActivity().finish();
					OnTrackManager.getInstance().resetRoute();
				}

			}
		});
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View v, int position, long id) {

		if (isCurrent() && stop.didStopHere()) {
			hideCloseButton();
			Trackable trackable = OnTrackManager.getInstance().getCurrentStop()
					.getTrackableByIndex(position);
			if (trackable != null
					&& trackable.getPicked() != Trackable.NOT_AT_SCHOOL
					&& trackable.getState() != Trackable.STATE_NOT_ATTENDANCE) {
				// AssistanceDuringRouteFragment dialog = new
				// AssistanceDuringRouteFragment(
				// trackable, adapter);
				// dialog.show(getActivity().getSupportFragmentManager(),
				// "assistanceduringroute");
				if (trackable.getNovelty() == null) {
					if (trackable.getPicked() == Trackable.NOT_ASSISTING_DELIVERED) {
						trackable.setPicked(Trackable.UNKNOWN);
					} else {
						trackable.setPicked(Trackable.NOT_ASSISTING_DELIVERED);

						CheckTrackableRemovalFromStopTask checkTrackableRemovalFromStopTask = new CheckTrackableRemovalFromStopTask( getActivity( ), stop.getId( ), trackable );
						checkTrackableRemovalFromStopTask.execute( WebServices.URL_CHECK_TRACKABLE_REMOVAL_FROM_STOP, ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance( ).getToken( ), ServiceKeys.ID_ROUTE_KEY, OnTrackManager.getInstance( ).getUser( ).getRoute( ).getId( ), ServiceKeys.ID_STOP_KEY, String.valueOf( stop.getId( ) ), ServiceKeys.ID_TRACKABLE_KEY, trackable.getId( ) );
					}

				} else {
					if (trackable.getNovelty().isNotAttending()
							|| trackable.getNovelty().isOutOfCurrentRoute()) {
						NoveltyDetailsDialogFragment dialog = new NoveltyDetailsDialogFragment();
						dialog.setTrackable(trackable);
						dialog.show(getActivity().getSupportFragmentManager(),
								"noveltydetails");
					} else {
						if (trackable.getPicked() == Trackable.NOT_ASSISTING_DELIVERED) {
							trackable.setPicked(Trackable.UNKNOWN);
						} else {
							trackable
									.setPicked(Trackable.NOT_ASSISTING_DELIVERED);
						}
					}
				}
				adapter.notifyDataSetChanged();
			}
		} else {
			showCloseButton();
			Trackable trackable = this.stop.getTrackableByIndex(position);
			if(trackable.getPicked()==Trackable.ASSISTING_DELIVERED || trackable.getPicked()==Trackable.NOT_ASSISTING_DELIVERED){
				ForceStateDialogFragment dialog = new ForceStateDialogFragment();
				dialog.setTrackable(trackable);
				dialog.setAdapter(adapter);
				dialog.show(getActivity().getSupportFragmentManager(),
						"forcestate");
			}else {
				if (trackable.getNovelty() == null) {
					ArrayList<Trackable> allTrackables = OnTrackManager
							.getInstance().getUser().getRoute().getAllTrackables();

					// Buscar la posiciï¿½n en la que estï¿½ el trackable en el
					// arreglo
					// de
					// todos los trackables
					int pos = 0;
					for (Trackable currentTrackable : allTrackables) {
						if (trackable.equals(currentTrackable)) {
							break;
						}
						pos++;
					}

					OnTrackManager.getInstance().setSelectedTrackable(pos);
					Intent i = new Intent(getActivity(), TrackableProfile.class);
					startActivity(i);
				} else {
					NoveltyDetailsDialogFragment dialog = new NoveltyDetailsDialogFragment();
					dialog.setTrackable(trackable);
					dialog.show(getActivity().getSupportFragmentManager(),
							"noveltydetails");

				}
			}

		}

	}

	public void hideCloseButton() {
		bClose.setVisibility(View.INVISIBLE);
	}

	public void showCloseButton() {
		bClose.setVisibility(View.VISIBLE);
	}

	public boolean isCurrent() {
		return isCurrent;
	}

	public void setCurrent(boolean isCurrent) {
		this.isCurrent = isCurrent;
	}

	public Stop getStop() {
		return stop;
	}

	public void setStop(Stop stop) {
		this.stop = stop;
	}

	public void hideCancelStopButton() {
		bCancelStop.setVisibility(View.INVISIBLE);
	}

	public void showCancelStopButton() {
		bCancelStop.setVisibility(View.VISIBLE);
	}
	
	public void hideHelpButton() {
		ivStopHelp.setVisibility(View.GONE);
	}

	public void showHelpButton() {
		ivStopHelp.setVisibility(View.VISIBLE);
	}
	
	
	public void changeBackgroundColor(int color){
		llStopDetailsBackground.setBackgroundColor(color);
	}

    @Override
    public void onClick( View v )
    {
        String tag = v.getTag( ).toString( );
        
        if( tag.equals( HELP ) )
        {
            HelpTrackablesDialogFragment helpTrackablesDialogFragment = HelpTrackablesDialogFragment.newInstance( stop.getId( ) );
            helpTrackablesDialogFragment.show( getActivity( ).getFragmentManager( ), "helpTrackablesDialogFragment" );
        }
    }
	
    public void updateTrackables( )
    {
        adapter.notifyDataSetChanged( );
    }
}
