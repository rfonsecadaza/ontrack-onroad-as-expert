package otr.ontrack_onroad.fragments;

import otr.ontrack_onroad.managers.CloseSessionManagerListener;
import otr.ontrack_onroad.parameters.GeneralMessages;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class CloseSessionDialogFragment extends DialogFragment{
	 @Override
	    public Dialog onCreateDialog(Bundle savedInstanceState) {
	        // Use the Builder class for convenient dialog construction
	        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	        builder.setTitle(GeneralMessages.DIALOG_CLOSE_SESSION_TITLE).setMessage(GeneralMessages.DIALOG_CLOSE_SESSION_QUESTION)
	               .setPositiveButton(GeneralMessages.YES, new DialogInterface.OnClickListener() {
	                   public void onClick(DialogInterface dialog, int id) {
	                	  ((CloseSessionManagerListener)getActivity()).logOut();
	                   }
	               })
	               .setNegativeButton(GeneralMessages.NO, new DialogInterface.OnClickListener() {
	                   public void onClick(DialogInterface dialog, int id) {
	                       dismiss();
	                   }
	               });
	        // Create the AlertDialog object and return it
	        return builder.create();
	    }
}
