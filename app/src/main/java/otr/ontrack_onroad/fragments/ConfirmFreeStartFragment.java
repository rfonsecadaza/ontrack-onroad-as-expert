package otr.ontrack_onroad.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import otr.ontrack_onroad.parameters.GeneralMessages;
import otr.ontrack_onroad_debug.activities.FreeMode;
import otr.ontrack_onroad_debug.activities.R;

/**
 * Created by rodrigoivanf on 06/10/2015.
 */
public class ConfirmFreeStartFragment extends DialogFragment {
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getResources().getString(R.string.free_mode_confirmation_title))
                .setMessage(
                        getResources().getString(R.string.free_mode_confirmation_text))
                .setPositiveButton(GeneralMessages.YES, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

//                        ((MainActivity) getActivity()).startRoute();
                        ((FreeMode)getActivity()).setAssistanceWithNoveltiesFragmentType(AssistanceWithNoveltiesFragment.ASSISTANCE_FRAGMENT_TYPE_FREE);
                        ( ( FreeMode )getActivity( ) ).hideAssistanceWithNovelties();
                        ( ( FreeMode )getActivity( ) ).startRoute();
//                        if(OnTrackManager.getInstance().isDelivery()){
//                            ( ( FreeMode )getActivity( ) ).speak(getResources().getString(R.string.free_mode_start_delivery));
//                        }else{
//                            ( ( FreeMode )getActivity( ) ).speak(getResources().getString(R.string.free_mode_start_pickup));
//                        }
                    }
                })
                .setNegativeButton(GeneralMessages.NO, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dismiss();
                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }
}
