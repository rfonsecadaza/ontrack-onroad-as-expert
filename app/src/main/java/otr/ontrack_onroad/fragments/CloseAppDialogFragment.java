package otr.ontrack_onroad.fragments;

import otr.ontrack_onroad.managers.FreeModeManager;
import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.parameters.GeneralMessages;
import otr.ontrack_onroad_debug.activities.FreeMode;
import otr.ontrack_onroad_debug.activities.MainActivity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class CloseAppDialogFragment extends DialogFragment{
	@Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        if(getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).speak(GeneralMessages.DIALOG_CLOSE_APP_QUESTION);
        }else if(getActivity() instanceof FreeMode){
            ((FreeMode) getActivity()).speak(GeneralMessages.DIALOG_CLOSE_APP_QUESTION);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(GeneralMessages.DIALOG_CLOSE_APP_TITLE).setMessage(GeneralMessages.DIALOG_CLOSE_APP_QUESTION)
               .setPositiveButton(GeneralMessages.YES, new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                	  getActivity().finish();
                	  OnTrackManager.getInstance().resetAllData();
                      FreeModeManager.getInstance().reset();
                   }
               })
               .setNegativeButton(GeneralMessages.NO, new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                       dismiss();
                   }
               });
        // Create the AlertDialog object and return it
        return builder.create();
    }

}
