package otr.ontrack_onroad.fragments;

import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad_debug.activities.R;
import otr.ontrack_onroad_debug.activities.RouteList;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class ChangeRouteDialogFragment extends DialogFragment{
	
//	String selectedRouteId;
//	public ChangeRouteDialogFragment(String selectedRouteId){
//		this.selectedRouteId = selectedRouteId;
//	}
	 @Override
	    public Dialog onCreateDialog(Bundle savedInstanceState) {
	        // Use the Builder class for convenient dialog construction
	        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	        builder.setTitle(getResources().getString(R.string.action_change_route_title)).setMessage(getResources().getString(R.string.action_change_route_text))
	               .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
	                   public void onClick(DialogInterface dialog, int id) {
//	                       ((RouteList)getActivity()).prepareAndStartNewRoute(selectedRouteId);
						   getActivity().startActivity(new Intent(getActivity(),RouteList.class));
						   getActivity().finish();
						   OnTrackManager.getInstance().resetRoute();
	                   }
	               })
	               .setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
	                   public void onClick(DialogInterface dialog, int id) {
	                       // User cancelled the dialog
	                   }
	               });
	        // Create the AlertDialog object and return it
	        return builder.create();
	    }
}
