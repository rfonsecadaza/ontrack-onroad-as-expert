package otr.ontrack_onroad.fragments;

import otr.ontrack_onroad.parameters.GeneralMessages;
import otr.ontrack_onroad_debug.activities.MainActivity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class ConfirmDeliveryStartFragment extends DialogFragment {
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		((MainActivity) getActivity()).speak(GeneralMessages.DIALOG_CONFIRM_DELIVERY_QUESTION);
		builder.setTitle(GeneralMessages.DIALOG_CONFIRM_DELIVERY_TITLE)
				.setMessage(
						GeneralMessages.DIALOG_CONFIRM_DELIVERY_QUESTION)
				.setPositiveButton(GeneralMessages.YES, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						
						((MainActivity) getActivity()).startRoute();
					}
				})
				.setNegativeButton(GeneralMessages.NO, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dismiss();
					}
				});
		// Create the AlertDialog object and return it
		return builder.create();
	}
}
