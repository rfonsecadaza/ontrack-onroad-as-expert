package otr.ontrack_onroad.fragments;

import java.util.ArrayList;
import java.util.List;

import otr.ontrack_onroad.adapters.SimulationAdapter;
import otr.ontrack_onroad.dao.SimLocation;
import otr.ontrack_onroad.dao.Simulation;
import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad_debug.activities.MainActivity;
import otr.ontrack_onroad_debug.activities.R;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;

public class SimulationListFragment extends DialogFragment implements
		OnClickListener, OnItemClickListener {

	int selectedRoute;
	List<Simulation> simulations;
	List<SimLocation> simLocations;
//	SupportMapFragment mapFragment;
//	GoogleMap map;
	ListView lvSimulations;

	Button bSimulate;
	Button bDontSimulate;
	Button bCancel;

	SimulationAdapter adapter;

	public void setParameters(List<Simulation> simulations,
							  int selectedRoute){
		this.selectedRoute = selectedRoute;
		this.simulations = simulations;
		this.simLocations = new ArrayList<SimLocation>();
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		Dialog dialog = super.onCreateDialog(savedInstanceState);
		dialog.setTitle("Seleccionar ruta simulada");
		return dialog;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_simulations,
				container, false);

		// Se agrega el fragment de mapa programï¿½ticamente, por la recomendaciï¿½n de poner
		// fragment dentro de fragment por xml
//		mapFragment = new SupportMapFragment();
		

//		FragmentTransaction transaction = getChildFragmentManager()
//				.beginTransaction();
//		transaction.add(R.id.rlMap, mapFragment).commit();
//
		

		bSimulate = (Button) rootView.findViewById(R.id.bSimulate);
		bSimulate.setOnClickListener(this);
		bDontSimulate = (Button) rootView.findViewById(R.id.bDontSimulate);
		bDontSimulate.setOnClickListener(this);
		bCancel = (Button) rootView.findViewById(R.id.bCancel);
		bCancel.setOnClickListener(this);

		adapter = new SimulationAdapter(getActivity(), this.simulations);
		lvSimulations = (ListView) rootView.findViewById(R.id.lvSimulations);
		lvSimulations.setAdapter(adapter);

		lvSimulations.setOnItemClickListener(this);

		return rootView;
	}

	@Override
	public void onClick(View v) {
		Intent i = new Intent(getActivity(), MainActivity.class);
		i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		switch (v.getId()) {
		case R.id.bSimulate:
			dismiss();
			OnTrackManager.getInstance().setSelectedRoute(selectedRoute);
			OnTrackManager.getInstance().setRecordedPoints(simLocations);
			OnTrackManager.getInstance().setRecordingSimulation(false);
			OnTrackManager.getInstance().setSimulating(true);
			startActivity(i);
			getActivity().finish();
			break;
		case R.id.bDontSimulate:
			dismiss();
			OnTrackManager.getInstance().setSelectedRoute(selectedRoute);
			OnTrackManager.getInstance().setRecordingSimulation(false);
			OnTrackManager.getInstance().setSimulating(false);
			startActivity(i);
			getActivity().finish();
			break;
		case R.id.bCancel:
			// No dejar ninguna ruta seleccionada
			if (!OnTrackManager.getInstance().routeDidStart()) {
				OnTrackManager.getInstance().setSelectedRoute(-1);
			}
			dismiss();
			break;
		default:
			dismiss();
			break;
		}

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view,
			int position, long id) {
		// El mapa se obtiene despuï¿½s de haber cargado la vista
//		map = ((SupportMapFragment) mapFragment).getMap();
//		map.clear();

		// Llamar los puntos de la simulaciï¿½n seleccionada
		simLocations = OnTrackManager.getInstance()
				.getSimLocationsOfSimulation(
						(int) adapter.getItemId(position));
//
//		LatLngBounds.Builder builder = new LatLngBounds.Builder();
//
//		for (int z = 0; z < simLocations.size() - 1; z++) {
//			SimLocation src = simLocations.get(z);
//			SimLocation dest = simLocations.get(z + 1);
//			Polyline line = map.addPolyline(new PolylineOptions()
//					.add(new LatLng(src.getLatitude(), src
//							.getLongitude()),
//							new LatLng(dest.getLatitude(), dest
//									.getLongitude())).width(5)
//					.color(Color.RED).geodesic(true));
//
//			builder.include(new LatLng(src.getLatitude(), src
//					.getLongitude()));
//		}
//
//		LatLng last = new LatLng(simLocations.get(
//				simLocations.size() - 1).getLatitude(), simLocations
//				.get(simLocations.size() - 1).getLongitude());
//		builder.include(last);
//
//		LatLngBounds bounds = builder.build();
//		int padding = 0; // offset from edges of the map in pixels
//		int boundingBoxWidth = 120;
//		int boundingBoxHeight = 120;
//		CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds,
//				boundingBoxWidth, boundingBoxHeight, padding);
//
//		map.animateCamera(cu);

		
	}
}
