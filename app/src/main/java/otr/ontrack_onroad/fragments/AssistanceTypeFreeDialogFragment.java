package otr.ontrack_onroad.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import otr.ontrack_onroad.managers.FreeModeManager;
import otr.ontrack_onroad.parameters.Parameters;
import otr.ontrack_onroad_debug.activities.FreeMode;
import otr.ontrack_onroad_debug.activities.R;

/**
 * Created by rodrigoivanf on 06/10/2015.
 */
public class AssistanceTypeFreeDialogFragment extends DialogFragment implements View.OnClickListener {
    private final static String UNNASSISTANTS = "UNNASSISTANTS";

    private final static String ASSISTANTS = "ASSISTANTS";

    private final static String PREROUTE = "PREROUTE";

    private int position;

    private Activity activity;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        builder.setTitle(getResources().getString(R.string.action_fragment_dialog_assistance_title));

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout

        View dialogView = inflater.inflate(R.layout.dialog_assistance_type_free_fragment, null);

        builder.setView(dialogView);

        Button bUnassistants = (Button) dialogView.findViewById(R.id.dialog_assistance_type_fragment_unassistants);
        bUnassistants.setTag(UNNASSISTANTS);
        bUnassistants.setOnClickListener(this);

        Button bAssistants = (Button) dialogView.findViewById(R.id.dialog_assistance_type_fragment_assistants);
        bAssistants.setTag(ASSISTANTS);
        bAssistants.setOnClickListener(this);

        Button bPreRoute = (Button) dialogView.findViewById(R.id.dialog_assistance_type_fragment_preroute);
        bPreRoute.setTag(PREROUTE);
        bPreRoute.setOnClickListener(this);

        activity = getActivity();

        return builder.create();
    }

    @Override
    public void onClick(View v) {
        String tag = v.getTag().toString();

        if (tag.equals(UNNASSISTANTS)) {
            FreeModeManager.getInstance().setAllTrackablesAsUnknown();
            FreeModeManager.getInstance().setPreroute(false);
            ((FreeMode) getActivity()).configureAndShowAssistanceWithNoveltiesFragmentPrestart();
        } else if (tag.equals(ASSISTANTS)) {
            // TODO: Dejar todos los trackables como ausentes
            FreeModeManager.getInstance().setAllTrackablesAsNotAtSchool();
            FreeModeManager.getInstance().setPreroute(false);
            ((FreeMode) getActivity()).configureAndShowAssistanceWithNoveltiesFragmentPrestart();

        } else if (tag.equals(PREROUTE)) {
            FreeModeManager.getInstance().setAllTrackablesAsUnknown();
            FreeModeManager.getInstance().setPreroute(true);
            ((FreeMode) getActivity()).speak(getResources().getString(R.string.free_mode_preroute_confirmation_text));
            ConfirmFreePreRouteDialogFragment dialog = new ConfirmFreePreRouteDialogFragment();
            dialog.show(getActivity().getSupportFragmentManager(),
                    Parameters.CONFIRMPREROUTE_DIALOG_TAG);
        }

        AssistanceTypeFreeDialogFragment.this.getDialog().cancel();


    }
}
