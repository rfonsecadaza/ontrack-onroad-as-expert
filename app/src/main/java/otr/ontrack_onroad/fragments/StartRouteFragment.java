package otr.ontrack_onroad.fragments;

import otr.ontrack_onroad.parameters.Parameters;
import otr.ontrack_onroad_debug.activities.MainActivity;
import otr.ontrack_onroad_debug.activities.R;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class StartRouteFragment extends Fragment {
	boolean buttonEnabled = false;
	
	

	ProgressBar pbLoadingRoute;
	TextView tvLoadingText;
	TextView tvTodayNovelties;
	RelativeLayout rlTodayNovelties;

	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_startroute, container, false);
		
		Typeface tf = Typeface.createFromAsset(getActivity().getAssets(),
				Parameters.LATO_FONT);
		tvLoadingText = (TextView)rootView.findViewById(R.id.tvLoadingText);
		tvLoadingText.setTypeface(tf);
		pbLoadingRoute = (ProgressBar)rootView.findViewById(R.id.pbSendingMessage);
		tvTodayNovelties = (TextView)rootView.findViewById(R.id.tvTodayNovelties);
		tvTodayNovelties.setTypeface(tf);

		rlTodayNovelties = (RelativeLayout)rootView.findViewById(R.id.rlTodayNovelties);

		rlTodayNovelties.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(!((MainActivity)getActivity()).isPrestartListSelected()) {
					((MainActivity) getActivity()).configureNoveltyList();
				}
			}
		});
		
		return rootView;
	}
	
	public void hideProgressBar(){
		pbLoadingRoute.setVisibility(View.INVISIBLE);
	}
	
	public void showProgressBar(){
		pbLoadingRoute.setVisibility(View.VISIBLE);
	}
	
	public void setFragmentTest(String text){
		tvLoadingText.setText(text);
	}
	
//	public void requestStartRouteToActivity (View v){
//		MainActivity activity = (MainActivity) getActivity();
//		activity.startRoute();
//		
//	}
	

	public boolean isButtonEnabled() {
		return buttonEnabled;
	}

	public void showNoveltyViewer(){
		tvTodayNovelties.setVisibility(View.VISIBLE);
	}
	public void hideNoveltyViewer(){
		tvTodayNovelties.setVisibility(View.GONE);
	}



	public void setButtonEnabled(boolean buttonEnabled) {
		this.buttonEnabled = buttonEnabled;
	}
}
