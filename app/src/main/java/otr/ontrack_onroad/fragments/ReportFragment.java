package otr.ontrack_onroad.fragments;

import org.json.JSONException;

import otr.ontrack_onroad.managers.FreeModeLocationManager;
import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.managers.OnTrackManagerAsyncTasks;
import otr.ontrack_onroad.managers.ReportManager;
import otr.ontrack_onroad.models.Report;
import otr.ontrack_onroad.parameters.Parameters;
import otr.ontrack_onroad.parameters.ServiceKeys;
import otr.ontrack_onroad.utils.Timestamp;
import otr.ontrack_onroad.utils.ToggleButtonGroupTableLayout;
import otr.ontrack_onroad_debug.activities.MainActivity;
import otr.ontrack_onroad_debug.activities.R;
import android.app.Dialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ReportFragment extends DialogFragment {

	ToggleButtonGroupTableLayout rgReports;
	EditText etReportDetails;
	Button bSendReport;

	TextView tvReportsTitle;
	TextView tvReportDetails;

	String category = null;
	String description = null;

	boolean involvesStudent = false;
	int studentReportType;
	int fkDestination;

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		Dialog dialog = super.onCreateDialog(savedInstanceState);
		dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		return dialog;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_reports, container,
				false);

		Typeface tf = Typeface.createFromAsset(getActivity().getAssets(),
				Parameters.LATO_FONT);

		tvReportsTitle = (TextView) rootView.findViewById(R.id.tvReportsTitle);
		tvReportsTitle.setTypeface(tf);

		tvReportDetails = (TextView) rootView
				.findViewById(R.id.tvReportDetails);
		tvReportDetails.setTypeface(tf);

		rgReports = (ToggleButtonGroupTableLayout) rootView
				.findViewById(R.id.rgReports);
		etReportDetails = (EditText) rootView
				.findViewById(R.id.etReportDetails);
		bSendReport = (Button) rootView.findViewById(R.id.bSendReport);
		bSendReport.setTypeface(tf);
		bSendReport.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				switch (rgReports.getCheckedRadioButtonId()) {
				case R.id.rbWrongRoute:
					description = "Ruta incorrecta";
					category = Report.WRONG_ROUTE_REPORT_CODE;
					fkDestination = OnTrackManager.getInstance().getUser()
							.getFootprint();
					involvesStudent = false;
					break;
				case R.id.rbTraffic:
					description = "Tráfico";
					category = Report.TRAFFIC_REPORT_CODE;
					fkDestination = OnTrackManager.getInstance().getUser()
							.getFootprint();
					involvesStudent = false;
					break;
				case R.id.rbMechanicFailure:
					description = "Falla mecánica";
					category = Report.MECHANICAL_FAILURE_REPORT_CODE;
					fkDestination = OnTrackManager.getInstance().getUser()
							.getFootprint();
					involvesStudent = false;
					break;
				case R.id.rbAccident:
					description = "Accidente";
					category = Report.ACCIDENT_REPORT_CODE;
					fkDestination = OnTrackManager.getInstance().getUser()
							.getFootprint();
					involvesStudent = false;
					break;
				case R.id.rbHealthProblem:
					description = "Problema de salud";
					category = Report.HEALTH_PROBLEM_REPORT_CODE;
					// TODO: Escoger el estudiante con problemas de salud
					fkDestination = OnTrackManager.getInstance().getUser()
							.getFootprint();
					involvesStudent = true;
					studentReportType = AllTrackablesFragment.TYPE_HEALTH;
					break;
				case R.id.rbBehaviorProblem:
					description = "Problema de comportamiento";
					category = Report.BEHAVIOUR_PROBLEM_REPORT_CODE;
					// TODO: Escoger el estudiante con problemas de
					// comportamiento
					fkDestination = OnTrackManager.getInstance().getUser()
							.getFootprint();
					involvesStudent = true;
					studentReportType = AllTrackablesFragment.TYPE_BEHAVIOUR;
					break;
				default:
					break;
				}

				if (!involvesStudent) {
					if (category != null) {
						String details = etReportDetails.getText().toString();
						if (details.isEmpty()) {
							details = Parameters.REPORT_NO_DETAILS;
						}
						// Report report = new Report(Timestamp
						// .completeTimestamp(), category, details,
						// OnTrackSystem
						// .getInstance().getCurrentLocation()
						// .getLatitude(), OnTrackSystem.getInstance()
						// .getCurrentLocation().getLongitude(), ""
						// + fkDestination);

						Report report;
						if(getActivity() instanceof MainActivity) {

							report = new Report(null, null, Timestamp
									.completeTimestamp(), category, details,
									OnTrackManager.getInstance()
											.getCurrentLocation().getLatitude(),
									OnTrackManager.getInstance()
											.getCurrentLocation().getLongitude(),
									fkDestination, (long) fkDestination, null, Report.REPORT_STATE_NOT_ANSWERED);
						}else{
							report = new Report(null, null, Timestamp
									.completeTimestamp(), category, details,
									FreeModeLocationManager.getInstance()
											.getLocation().getLatitude(),
									FreeModeLocationManager.getInstance()
											.getLocation().getLongitude(),
									fkDestination, (long) fkDestination, null, Report.REPORT_STATE_NOT_ANSWERED);
						}

						try {
							ReportManager
									.getInstance()
									.executeSendReportTask(
											OnTrackManagerAsyncTasks.URL_BASE_ADD_REPORTS,
											ServiceKeys.TOKEN_KEY,
											OnTrackManager.getInstance()
													.getToken(),
											ServiceKeys.REPORTS_KEY,
											report.getReportJSONString(Report.FK_FOOTPRINT_KEY));
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				} else {
					AllTrackablesFragment dialog = new AllTrackablesFragment();
					dialog.setTypeAndDetails(studentReportType, etReportDetails.getText()
							.toString());
					dialog.show(getActivity().getSupportFragmentManager(),
							"alltrackables");
				}

				dismiss();

			}
		});
		return rootView;
	}

//	@Override
//	public void onStart() {
//		// TODO Auto-generated method stub
//		super.onStart();
//		// safety check
//		if (getDialog() == null) {
//		 return;
//		}
//		
//		WindowManager wm = (WindowManager)getActivity().getSystemService(Context.WINDOW_SERVICE);
//		Display display = wm.getDefaultDisplay();
//
//		int dialogWidth = display.getWidth();
//		int dialogHeight = display.getHeight();
//
//		getDialog().getWindow().setLayout(dialogWidth, dialogHeight-80);
//	}
}
