package otr.ontrack_onroad.managers;

public interface CloseSessionManagerListener {
	
	public void logOut();
	
	public void onSessionClosedInServer();

}
