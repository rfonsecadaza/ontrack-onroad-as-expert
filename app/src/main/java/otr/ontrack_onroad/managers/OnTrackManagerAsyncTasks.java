package otr.ontrack_onroad.managers;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import otr.ontrack_onroad.dao.Event;
import otr.ontrack_onroad.models.Point;
import otr.ontrack_onroad.models.Stop;
import otr.ontrack_onroad.parameters.GeneralMessages;
import otr.ontrack_onroad.parameters.Parameters;
import otr.ontrack_onroad.parameters.ServiceKeys;
import otr.ontrack_onroad.threads.SendTSTStatesThread;
import otr.ontrack_onroad.threads.SendTrackedStopInfoThread;
import otr.ontrack_onroad.utils.JSONManager;
import otr.ontrack_onroad.utils.Timestamp;
import otr.ontrack_onroad.utils.WebRequestManager;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;

public class OnTrackManagerAsyncTasks {

	// Carga los detalles de la ruta seleccionada
	public static final String URL_BASE_ROUTE = "ApiServices/GetDriverRouteDetail";
	// Carga los detalles de la ruta seleccionada con las novedades
	public static final String URL_BASE_ROUTE_WITH_NOVELTIES = "ApiServices/GetRouteDetailWithNovelties";

	// Notifica el inicio de la ruta para la creaciï¿½n de un footprint
	public static final String URL_BASE_FOOTPRINT = "ApiTracker/StartRouteMove";

	// Envï¿½a un conjunto de posiciones rastreadas en el dispositivo
	public static final String URL_BASE_TRACKED_POINT = "ApiTracker/ActualPosition";

	// Envï¿½a un conjunto de reportes
	public static final String URL_BASE_ADD_REPORTS = "ApiReport/addReports";

	// Envï¿½a el estado de los TrackeStopTrackables
	public static final String URL_BASE_PICKED_TRACKABLES = "ApiTracker/PickedTrackables";

	// Recupera los puntos rastreados
	public static final String URL_BASE_GIVE_COMPLETE_TRACK = "ApiTracker/GiveCompleteTrack";

	// Recupera el último punto rastreado
	public static final String URL_BASE_GIVE_LAST_TRACKED_POINT = "ApiTracker/GiveLastTrackedPoint";

	// Cierra sesiï¿½n
	public static final String URL_BASE_CLOSE_SESSION = "ApiAuthentication/Logout";

	// Envï¿½a un conjunto de eventos generados por el dispositivo
	public static final String URL_BASE_ADD_EVENTS = "ApiEvent/AddEvents";

	// Envï¿½a la informaciï¿½n de los trackedStops
	public static final String URL_BASE_TRACKED_STOP_INFO = "ApiTracker/trackedStopsInformation";

	// Notifica el inicio de la ruta para la creaciï¿½n de un footprint
	public static final String URL_BASE_FOOTPRINTINFO = "ApiTracker/footprintInformation";

	// Trae la lista de las novedades del día
	public static final String URL_BASE_GET_NOVELTIES = "ApiNovelty/GetNoveltiesOfRoute";

	// Envía información de los footprintTrackables
	public static final String URL_BASE_PICKED_FOOTPRINT_TRACKABLES = "ApiTracker/PickedFootprintTrackables";

	// Base para las solicitudes al servicio googleDirections
	public static final String URL_GOOGLE_DIRECTIONS_BASE = "http://maps.googleapis.com/maps/api/directions/json?";

	// Base para las solicitudes al servicio mapBoxDirections (Que quién demonios sabe hasta cuando usaremos porque Google nos jodió)
	public static final String URL_MAPBOX_DIRECTIONS_BASE = "https://api.mapbox.com/v4/directions/mapbox.driving/";

	// Base para botón de pánico
	public static final String URL_PANIC = "ApiMessage/SendPanicButtonMessage";

	// AsyncTask para cargar la informaciï¿½n detallada de la ruta seleccionada
	// en
	// la actividad RouteList

	private static class LoadDetailedRouteTask extends
			AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... args) {
			String response = null;
			try {
				response = WebRequestManager.executeTask(args);

				// Carga los detalles de la ruta a partir de la cadena JSON
				// obtenida en la web
				JSONManager.loadRoute(response, OnTrackManager.getInstance()
						.getSelectedRoute());

			} catch (JSONException e) {
				e.printStackTrace();
				response = GeneralMessages.JSON_ERROR;
			} catch (IOException e) {
				response = GeneralMessages.HTTP_NOT_FOUND_ERROR;
			}

			return response;

		}

		@Override
		protected void onPostExecute(String result) {

			if (result.equals(GeneralMessages.HTTP_CONNECTION_ERROR)
					|| result.equals(GeneralMessages.JSON_ERROR)
					|| result.equals(GeneralMessages.HTTP_NOT_FOUND_ERROR)) {
				// En caso de error, mostrar un mensaje informativo
				OnTrackManager.getInstance().getListener()
						.onAsyncTaskError(result);
			} else {

				// Si la lectura del JSON fue correcta, proceder a mostrar la
				// informaciï¿½n de la ruta en pantalla

				/******
				 * SOLICITUD DE INFORMACIï¿½N A LOS SERVICIOS WEB DE GOOGLE
				 * DIRECTIONS
				 *******/
				// Se hacen tantas solicitudes como paraderos despuï¿½s del
				// origen
				// En el estado de PostExecute de este mï¿½todo, se inicializa
				// un
				// contador llamado
				// googleDirectionsRequests en 0
				OnTrackManager.getInstance().setGoogleDirectionsRequests(0);
				OnTrackManager.getInstance().setNextGoogleDirectionsRequest(1);

				// Deberï¿½a siempre asumirse que hay al menos dos paraderos: un
				// origen y un destino
				if (OnTrackManager.getInstance().getUser().getRoute()
						.getStops().size() >= 2) {
					Stop nextStop = OnTrackManager.getInstance().getUser()
							.getRoute().getStops().get(1);
					Stop currentStop = OnTrackManager.getInstance().getUser()
							.getRoute().getStops().get(0);
					OnTrackManager.getInstance().setNextStop(nextStop);
					OnTrackManager.getInstance().setCurrentStop(currentStop);

				}

				// Se llama al servicio que carga las novedades del día
//				executeRequestNoveltiesTask(
//						OnTrackManagerAsyncTasks.URL_BASE_GET_NOVELTIES,
//						ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance()
//								.getToken(), ServiceKeys.ID_ROUTE_KEY,
//						OnTrackManager.getInstance().getUser().getRoute()
//								.getId());



				// Actualiza la interfaz gráfica de los listeners
				OnTrackManager
						.getInstance()
						.getListener()
						.onDetailedRouteLoaded(
								OnTrackManager.getInstance().getUser()
										.getRoute());

			}
		}
	}

	public static void executeLoadDetailedRouteTask(String... args) {
		LoadDetailedRouteTask task = new LoadDetailedRouteTask();
		task.execute(args);
	}

	private static class LoadDetailedRouteWithNoveltiesTask extends
			AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... args) {
			String response = null;
			try {
				response = WebRequestManager.executeTask(args);

				// Carga los detalles de la ruta a partir de la cadena JSON
				// obtenida en la web
				JSONManager.loadRouteWithNovelties(response, OnTrackManager
						.getInstance().getSelectedRoute());

			} catch (JSONException e) {
				e.printStackTrace();
				response = GeneralMessages.JSON_ERROR;
			} catch (IOException e) {
				response = GeneralMessages.HTTP_NOT_FOUND_ERROR;
			}

			return response;

		}

		@Override
		protected void onPostExecute(String result) {

			if (result.equals(GeneralMessages.HTTP_CONNECTION_ERROR)
					|| result.equals(GeneralMessages.JSON_ERROR)
					|| result.equals(GeneralMessages.HTTP_NOT_FOUND_ERROR)) {
				// En caso de error, mostrar un mensaje informativo
				OnTrackManager.getInstance().getListener()
						.onAsyncTaskError(result);
			} else {

				// Si la lectura del JSON fue correcta, proceder a mostrar la
				// informaciï¿½n de la ruta en pantalla

				/******
				 * SOLICITUD DE INFORMACIï¿½N A LOS SERVICIOS WEB DE GOOGLE
				 * DIRECTIONS
				 *******/
				// Se hacen tantas solicitudes como paraderos despuï¿½s del
				// origen
				// En el estado de PostExecute de este mï¿½todo, se inicializa
				// un
				// contador llamado
				// googleDirectionsRequests en 0
				OnTrackManager.getInstance().setGoogleDirectionsRequests(0);
				OnTrackManager.getInstance().setNextGoogleDirectionsRequest(1);

				// Deberï¿½a siempre asumirse que hay al menos dos paraderos: un
				// origen y un destino
				if (OnTrackManager.getInstance().getUser().getRoute()
						.getStops().size() >= 2) {
					Stop nextStop = OnTrackManager.getInstance().getUser()
							.getRoute().getStops().get(1);
					Stop currentStop = OnTrackManager.getInstance().getUser()
							.getRoute().getStops().get(0);
					OnTrackManager.getInstance().setNextStop(nextStop);
					OnTrackManager.getInstance().setCurrentStop(currentStop);

				}

				// Actualiza la interfaz gráfica de los listeners
				OnTrackManager
						.getInstance()
						.getListener()
						.onDetailedRouteLoaded(
								OnTrackManager.getInstance().getUser()
										.getRoute());

				OnTrackManager
						.getInstance()
						.getListener()
						.onNoveltiesLoaded();

			}
		}
	}

	public static void executeLoadDetailedRouteWithNoveltiesTask(String... args) {
		LoadDetailedRouteWithNoveltiesTask task = new LoadDetailedRouteWithNoveltiesTask();
		task.execute(args);
	}

	// AsyncTask para la generaciï¿½n de un footprint para el rastreo de la ruta
	private static class RequestNoveltiesTask extends
			AsyncTask<String, Integer, String> {
		@Override
		protected String doInBackground(String... args) {
			String response = null;
			try {
				response = WebRequestManager.executeTask(args);


			} catch (IOException e) {
				response = GeneralMessages.HTTP_NOT_FOUND_ERROR;
			}

			return response;

		}

		@Override
		protected void onPostExecute(String result) {

			if (result.equals(GeneralMessages.HTTP_CONNECTION_ERROR)
					|| result.equals(GeneralMessages.HTTP_NOT_FOUND_ERROR)) {
				// En caso de error, mostrar un mensaje informativo
				OnTrackManager.getInstance().getListener()
						.onAsyncTaskError(result);
			} else {
				try {
					JSONManager.loadNovelties(result);
					OnTrackManager.getInstance().getListener()
							.onNoveltiesLoaded();
				} catch (JSONException e) {
					e.printStackTrace();
					OnTrackManager.getInstance().getListener()
							.onAsyncTaskError(GeneralMessages.JSON_ERROR);
				}
			}
		}
	}

	public static void executeRequestNoveltiesTask(String... params) {
		RequestNoveltiesTask task = new RequestNoveltiesTask();
		task.execute(params);
	}

	// AsyncTask para la generación de un footprint para el rastreo de la ruta

	private static class RequestFootprintTask extends
			AsyncTask<String, Integer, String> {

		@Override
		protected void onPreExecute(){
			OnTrackManager.getInstance().setRouteStarting(true);
		}

		@Override
		protected String doInBackground(String... args) {
			String response = null;
			try {
				response = WebRequestManager.executeTaskGet(args);
				JSONManager.loadFootprint(response);
			} catch (IOException e) {
				response = GeneralMessages.HTTP_NOT_FOUND_ERROR;
			} catch (JSONException e) {
				e.printStackTrace();
				response = GeneralMessages.JSON_ERROR;
			}

			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			// mProgressDialog.dismiss();

			if (result.equals(GeneralMessages.JSON_ERROR)
					|| result.equals(GeneralMessages.HTTP_CONNECTION_ERROR)
					|| result.equals(GeneralMessages.HTTP_NOT_FOUND_ERROR)) {
				OnTrackManager.getInstance().getListener()
						.onAsyncTaskError(result);
			} else {
				OnTrackManager.getInstance().initializeRoute();

				// Cargar todos los puntos ya "trackeados"
				// Nota: ¿por qué se necesita cargar este servicio?
//				executeRequestCompleteTrackTask(URL_BASE_GIVE_COMPLETE_TRACK,
//						ServiceKeys.DATE_KEY, Timestamp.completeTimestamp(),
//						ServiceKeys.ID_FOOTPRINT_KEY, ""
//								+ OnTrackManager.getInstance().getUser()
//								.getFootprint(), ServiceKeys.TOKEN_KEY,
//						OnTrackManager.getInstance().getToken());

				// NOTA: Por ahora se probará con el servicio que solo carga el último punto rastreado
				// No creo que haya ningún problema y además hace que el inicio de ruta sea más rápido
				// En caso de notar algún comportamiento anormal, se pueden quitar los comentarios de
				// las líneas de arriba



				if (!OnTrackManager.getInstance().isRouteReloaded()) {
					// Lo siguiente se hace únicamente si es la primera vez que se
					// inicia la ruta

					// Evento de inicio de ruta
					Event event = new Event(null,
							Timestamp.completeTimestamp(),
							Event.START_ROUTE_EVENT_CODE,
							GeneralMessages.ROUTE_HAS_STARTED, 0, 1, ""
							+ OnTrackManager.getInstance().getUser()
							.getFootprint(),
							OnTrackManager.getInstance().getUser().getRoute()
									.getId(), false);
					// EventManager.getInstance().getEvents().add(event);
					// EventManager.getInstance().getStackedEvents().add(event);

					// Guardar evento en db local
					OnTrackManager.getInstance().getEvents().add(event);
					OnTrackManager.getInstance().getEventDao().insert(event);

					// Notificar a los listener de eventos que hay una nueva
					// notificación
					EventManager.getInstance().notifyEventGenerated(
							event.getId());

					// try {
					try {
						EventManager
								.getInstance()
								.executeSendEventTask(
										URL_BASE_ADD_EVENTS,
										ServiceKeys.TOKEN_KEY,
										OnTrackManager.getInstance().getToken(),
										ServiceKeys.EVENTS_KEY,
										event.getEventJSONString(Event.FK_FOOTPRINT_KEY));
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}


					// Se envía el estado de los trackables, no hay problema con mandarlo en paralelo con
					// el evento de inicio de ruta
					try {
						OnTrackManagerAsyncTasks
								.executeSendTSTStatesTask(
										OnTrackManagerAsyncTasks.URL_BASE_PICKED_TRACKABLES,
										ServiceKeys.TOKEN_KEY,
										OnTrackManager.getInstance().getToken(),
										ServiceKeys.TRACKEDSTOPTRACKABLES_KEY,
										OnTrackManager
												.getInstance()
												.getUser()
												.getRoute()
												.getTrackedStopTrackablesJSONString());
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					doMapboxDirectionsRequest(OnTrackManager.getInstance()
							.getGoogleDirectionsRequests());

				}else{
					// Si la ruta ya había iniciado previamente, se debería intentar cargar
					// el último punto ya rastreado
					executeRequestCompleteTrackTask(URL_BASE_GIVE_LAST_TRACKED_POINT,
							ServiceKeys.ID_FOOTPRINT_KEY, ""
									+ OnTrackManager.getInstance().getUser()
									.getFootprint(), ServiceKeys.TOKEN_KEY,
							OnTrackManager.getInstance().getToken());
				}

				//doGoogleDirectionsRequest(OnTrackManager.getInstance()
				//	.getGoogleDirectionsRequests());


			}
		}
	}

	private static class RequestFootprintTask2 extends
			AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... args) {
			String response = null;
			try {
				response = WebRequestManager.executeTask(args);
				JSONManager.loadFootprint2(response);
			} catch (IOException e) {
				response = GeneralMessages.HTTP_NOT_FOUND_ERROR;
			} catch (JSONException e) {
				e.printStackTrace();
				response = GeneralMessages.JSON_ERROR;
			}

			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			// mProgressDialog.dismiss();

			if (result.equals(GeneralMessages.JSON_ERROR)
					|| result.equals(GeneralMessages.HTTP_CONNECTION_ERROR)
					|| result.equals(GeneralMessages.HTTP_NOT_FOUND_ERROR)) {
				OnTrackManager.getInstance().getListener()
						.onAsyncTaskError(result);
			} else {
				OnTrackManager.getInstance().initializeRoute2();

				// Cargar todos los puntos ya "trackeados"
//				executeRequestCompleteTrackTask(URL_BASE_GIVE_COMPLETE_TRACK,
//						ServiceKeys.DATE_KEY, Timestamp.completeTimestamp(),
//						ServiceKeys.ID_FOOTPRINT_KEY, ""
//								+ OnTrackManager.getInstance().getUser()
//								.getFootprint(), ServiceKeys.TOKEN_KEY,
//						OnTrackManager.getInstance().getToken());

				if (!OnTrackManager.getInstance().isRouteReloaded()) {
					// Evento de inicio de ruta y envï¿½o
					Event event = new Event(null,
							Timestamp.completeTimestamp(),
							Event.START_ROUTE_EVENT_CODE,
							GeneralMessages.ROUTE_HAS_STARTED, 0, 1, ""
							+ OnTrackManager.getInstance().getUser()
							.getFootprint(),
							OnTrackManager.getInstance().getUser().getRoute()
									.getId(), false);
					// EventManager.getInstance().getEvents().add(event);
					// EventManager.getInstance().getStackedEvents().add(event);

					// Guardar evento en db local
					OnTrackManager.getInstance().getEvents().add(event);
					OnTrackManager.getInstance().getEventDao().insert(event);

					// Notificar a los listener de eventos que hay una nueva
					// notificaciï¿½n
					EventManager.getInstance().notifyEventGenerated(
							event.getId());

					// try {
					try {
						EventManager
								.getInstance()
								.executeSendEventTask(
										URL_BASE_ADD_EVENTS,
										ServiceKeys.TOKEN_KEY,
										OnTrackManager.getInstance().getToken(),
										ServiceKeys.EVENTS_KEY,
										event.getEventJSONString(Event.FK_FOOTPRINT_KEY));
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					try {
						OnTrackManagerAsyncTasks
								.executeSendTSTStatesTask2(
										OnTrackManagerAsyncTasks.URL_BASE_PICKED_TRACKABLES,
										ServiceKeys.TOKEN_KEY,
										OnTrackManager.getInstance().getToken(),
										ServiceKeys.TRACKEDSTOPTRACKABLES_KEY,
										OnTrackManager
												.getInstance()
												.getUser()
												.getRoute()
												.getTrackedStopTrackablesJSONString2());
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			}
		}
	}

	public static void executeRequestFootprintTask(String... args) {
		RequestFootprintTask task = new RequestFootprintTask();
		task.execute(args);
	}

	public static void executeRequestFootprintTask2(String... args) {
		RequestFootprintTask2 task = new RequestFootprintTask2();
		task.execute(args);
	}

	private static class RequestCompleteTrackTask extends
			AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			String response = null;
			try {
				response = WebRequestManager.executeTask(params);
				JSONManager.loadCompleteTrack(response);
				doMapboxDirectionsRequest(OnTrackManager.getInstance()
						.getGoogleDirectionsRequests());
			} catch (IOException e) {
				response = GeneralMessages.HTTP_NOT_FOUND_ERROR;
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return response;
		}

	}

	public static void executeRequestCompleteTrackTask(String... args) {
		RequestCompleteTrackTask task = new RequestCompleteTrackTask();
		task.execute(args);
	}

	// AsyncTask para la actualizaciï¿½n de la informaciï¿½n del footprint
	private static class SendFootprintInfoTask extends
			AsyncTask<String, Integer, String> {
		@Override
		protected String doInBackground(String... args) {
			String response = null;
			try {
				response = WebRequestManager.executeTask(args);
			} catch (IOException e) {
				response = GeneralMessages.HTTP_NOT_FOUND_ERROR;
			}

			return response;

		}

		@Override
		protected void onPostExecute(String result) {
			if (result.equals(GeneralMessages.JSON_ERROR)
					|| result.equals(GeneralMessages.HTTP_CONNECTION_ERROR)
					|| result.equals(GeneralMessages.HTTP_NOT_FOUND_ERROR)) {
				OnTrackManager.getInstance().getListener()
						.onAsyncTaskError(result);
			}
		}
	}

	// AsyncTask para llamar los servicios de Google Directions, obtener tiempos
	// estimados y trazado de la ruta

	private static class DecodeRouteTask extends
			AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... args) {

			String response = "";
			if (args.length > 0) {
				try {
					response = WebRequestManager
							.executeTaskGetWithoutFormatting(args);
					return response;
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
			return response;

		}

		@Override
		protected void onPostExecute(String result) {

			int googleDirectionsRequests = OnTrackManager.getInstance()
					.getGoogleDirectionsRequests();
			int nextGoogleDirectionRequest = OnTrackManager.getInstance()
					.getNextGoogleDirectionsRequest();

			if (!result.equals("")) {
				//			JSONManager.loadRouteFromGoogleDirections(result,
				//					googleDirectionsRequests, nextGoogleDirectionRequest);
				JSONManager.loadRouteFromMapboxDirections(result,
						googleDirectionsRequests, nextGoogleDirectionRequest);

			}

			OnTrackManager.getInstance().analizeEstimatedTimes(
					nextGoogleDirectionRequest);

			OnTrackManager.getInstance().setGoogleDirectionsRequests(
					nextGoogleDirectionRequest);

			if (OnTrackManager.getInstance().getGoogleDirectionsRequests() < OnTrackManager
					.getInstance().getUser().getRoute().getStops().size() - 1) {
				// Si todavï¿½a no se han procesado todos los paraderos, se
				// siguen
				// haciendo solicitudes a Google Directions
				//doGoogleDirectionsRequest(OnTrackManager.getInstance()
				//	.getGoogleDirectionsRequests());
				doMapboxDirectionsRequest(OnTrackManager.getInstance()
						.getGoogleDirectionsRequests());
			} else {
				// Notificar que la ruta ya ha iniciado

				// Una vez todos los puntos han sido procesados, se procede a
				// dibujarlos sobre el mapa

				ArrayList<Stop> stops = OnTrackManager.getInstance().getUser()
						.getRoute().getStops();

				// Limpia los steps de todos aquellos paraderos no relevantes
				for (Stop exploredStop : stops) {
					if (!exploredStop.trackablesToPickOrLeaveAtStop()
							|| exploredStop.isCancelled()) {
						exploredStop.getSteps().clear();
					}
				}

				OnTrackManager.getInstance().getListener()
						.onRouteDecoded(stops);

				SendTrackedStopInfoTask task = new SendTrackedStopInfoTask(true);
				try {
					task.execute(URL_BASE_TRACKED_STOP_INFO,
							ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance()
									.getToken(), ServiceKeys.TRACKED_STOPS_KEY,
							OnTrackManager.getInstance()
									.getStopDataJSONString());
				} catch (JSONException e) {
					OnTrackManager.getInstance().getListener()
							.onAsyncTaskError(e.getMessage());
				}
				// LatLng latLng = new
				// LatLng(Double.parseDouble(stops.get(0).getLatitude()),
				// Double.parseDouble(stops.get(0).getLongitude()));
				// mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,
				// 20));

				// Actualiza la informaciï¿½n del footprint
				if (!OnTrackManager.getInstance().isRouteReloaded()) {
					SendFootprintInfoTask footprintInfoTask = new SendFootprintInfoTask();
					footprintInfoTask.execute(URL_BASE_FOOTPRINTINFO,
							ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance()
									.getToken(), ServiceKeys.FK_FOOTPRINT_KEY,
							""
									+ OnTrackManager.getInstance().getUser()
									.getFootprint(),
							ServiceKeys.ESTIMATED_DISTANCE_KEY, ""
									+ OnTrackManager.getInstance()
									.totalDistanceOfRoute(),
							ServiceKeys.ESTIMATED_TIME_KEY, OnTrackManager
									.getInstance()
									.getEstimatedArrivalTimeOfRoute());
				}

				// Inicia ruta
				OnTrackManager.getInstance().startRoute();

			}
		}
	}

	// AsyncTask para el envï¿½o de tiempos estimados por Google Directions
	// al servidor de OnTrack

	private static class SendTrackedStopInfoTask extends
			AsyncTask<String, Integer, String> {

		private boolean isNewDistanceFlag;

		public SendTrackedStopInfoTask(boolean isNewDistanceFlag) {
			this.isNewDistanceFlag = isNewDistanceFlag;
		}

		@Override
		protected String doInBackground(String... params) {
			String result;
			try {
				result = WebRequestManager.executeTask(params);
//				Log.d("result",result);
			} catch (IOException e) {
				result = e.getMessage();
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			try
			{
				if( result.equals( GeneralMessages.JSON_ERROR )
						|| result.equals( GeneralMessages.HTTP_CONNECTION_ERROR )
						|| result.contains( GeneralMessages.HTTP_NOT_FOUND_ERROR ) )
				{
					OnTrackManager.getInstance( ).getListener( )
							.onAsyncTaskError( result );
				}
				else
				{
					if( this.isNewDistanceFlag )
						OnTrackManager.getInstance( ).increaseDistanceFlag( );
				}
			}
			catch( Exception e )
			{
				OnTrackManager.getInstance( ).getListener( )
						.onAsyncTaskError( "No fue posible calcular la ruta" );
			}
		}

	}

	public static void executeSendTrackedStopInfoTask(
			boolean isNewDistanceFlag, String... args) {
//		SendTrackedStopInfoTask task = new SendTrackedStopInfoTask(
//				isNewDistanceFlag);
//		task.execute(args);
		SendTrackedStopInfoThread task = new SendTrackedStopInfoThread(isNewDistanceFlag,args);
		task.start();

	}

	// AsyncTask para el envï¿½o de localizaciï¿½n en tiempo real
	// al servidor de OnTrack

	private static class SendLocationTask extends
			AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(final String... args) {
			String result;
			try {
				result = WebRequestManager.executeTask(args);
				return result;
			} catch (IOException e) {
				return GeneralMessages.HTTP_NOT_FOUND_ERROR;
			}
		}

		@Override
		protected void onPostExecute(String result) {
			if (result.equals(GeneralMessages.HTTP_CONNECTION_ERROR)
					|| result.contains(GeneralMessages.HTTP_NOT_FOUND_ERROR)) {
				OnTrackManager.getInstance().getListener()
						.onAsyncTaskError(result);
			} else {

				int status;
				try {

					JSONObject jsonResult = new JSONObject(result);
					status = jsonResult.getInt(ServiceKeys.STATUS_KEY);
					if (status != 1) {
						String error = jsonResult
								.getString(ServiceKeys.ERROR_KEY);
						OnTrackManager.getInstance().getListener()
								.onAsyncTaskError(error);
					} else {
						OnTrackManager.getInstance().deleteStackedPoints();
					}
				} catch (JSONException e) {
					e.printStackTrace();
					OnTrackManager.getInstance().getListener()
							.onAsyncTaskError(e.getMessage());
				}

			}
		}
	}

	private static class SendLocationTask2 extends
			AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(final String... args) {
			String result;
			try {
				result = WebRequestManager.executeTask(args);
				return result;
			} catch (IOException e) {
				return GeneralMessages.HTTP_NOT_FOUND_ERROR;
			}
		}

		@Override
		protected void onPostExecute(String result) {
			if (result.equals(GeneralMessages.HTTP_CONNECTION_ERROR)
					|| result.contains(GeneralMessages.HTTP_NOT_FOUND_ERROR)) {

			} else {

				int status;
				try {

					JSONObject jsonResult = new JSONObject(result);
					status = jsonResult.getInt(ServiceKeys.STATUS_KEY);
					if (status != 1) {
						String error = jsonResult
								.getString(ServiceKeys.ERROR_KEY);
					} else {
						FreeModeLocationManager.getInstance()
								.getStackedPoints().clear();
					}
				} catch (JSONException e) {

				}

			}
		}
	}

	public static AsyncTask<String, Integer, String> executeSendLocationTask(
			String... args) {
		SendLocationTask task = new SendLocationTask();
		return task.execute(args);
	}

	public static AsyncTask<String, Integer, String> executeSendLocationTask2(
			String... args) {
		SendLocationTask2 task = new SendLocationTask2();
		return task.execute(args);
	}

	// AsyncTask para el envï¿½o de tiempos de llegada reales
	// al servidor de OnTrack
	private class SendRealTimesTask extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			String result;
			try {
				result = WebRequestManager.executeTask(params);
			} catch (IOException e) {
				result = e.getMessage();
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			if (result.equals(GeneralMessages.JSON_ERROR)
					|| result.equals(GeneralMessages.HTTP_CONNECTION_ERROR)
					|| result.contains(GeneralMessages.HTTP_NOT_FOUND_ERROR)) {
				OnTrackManager.getInstance().getListener()
						.onAsyncTaskError(result);
			}
		}

	}

	public void executeSendRealTimesTask(String... args) {
		SendRealTimesTask task = new SendRealTimesTask();
		task.execute(args);
	}

	// AsyncTask para el envï¿½o de estado de los TrackedStopTrackables
	// Obsoleto! Se sustituirá con un Thread

	private static class SendTSTStatesTask extends
			AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			String result;
			try {
				result = WebRequestManager.executeTask(params);
			} catch (IOException e) {
				result = e.getMessage();
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			if (result.equals(GeneralMessages.JSON_ERROR)
					|| result.equals(GeneralMessages.HTTP_CONNECTION_ERROR)
					|| result.contains(GeneralMessages.HTTP_NOT_FOUND_ERROR)) {
				OnTrackManager.getInstance().getListener()
						.onAsyncTaskError(result);
			} else {
				// timeFlag avisa que ya estï¿½ lista la informaciï¿½n de los
				// tracked stops en el paradero actual
				if (!OnTrackManager.getInstance().isRouteFinished()) {

					OnTrackManager.getInstance().setTimeFlag(
							OnTrackManager.getInstance().getCurrentStop()
									.getOrder());
				} else {
					OnTrackManager.getInstance().getListener().onLastInfoSent();
					OnTrackManager.getInstance().resetRoute();
				}
			}
		}

	}

//	// Thread para el envï¿½o de estado de los TrackedStopTrackables
//	private static class SendTSTStatesThread extends Thread implements Runnable{
//
////        long startTime;
////        long endTime;
//
//		private String[] params;
//		private String result;
//
//		public SendTSTStatesThread(String... params) {
//			this.params = params;
//		}
//
//		@Override
//		public void run() {
//			String result;
//			try {
//				result = WebRequestManager.executeTask(params);
//			} catch (HttpHostConnectException e) {
//				result = e.getLocalizedMessage();
//			} catch (IOException e) {
//				result = e.getMessage();
//			}
//			postExecute();
//		}
//
//		public void postExecute() {
//			if (result.equals(GeneralMessages.JSON_ERROR)
//					|| result.equals(GeneralMessages.HTTP_CONNECTION_ERROR)
//					|| result.contains(GeneralMessages.HTTP_NOT_FOUND_ERROR)) {
//				OnTrackManager.getInstance().getListener()
//						.onAsyncTaskError(result);
//			} else {
//				// timeFlag avisa que ya estï¿½ lista la informaciï¿½n de los
//				// tracked stops en el paradero actual
//				if (!OnTrackManager.getInstance().isRouteFinished()) {
//
//					OnTrackManager.getInstance().setTimeFlag(
//							OnTrackManager.getInstance().getCurrentStop()
//									.getOrder());
//				} else {
//					OnTrackManager.getInstance().getListener().onLastInfoSent();
//					OnTrackManager.getInstance().resetRoute();
//				}
//			}
//		}
//
//	}




	// AsyncTask para el envï¿½o de estado de los TrackedStopTrackables
	private static class SendTSTStatesTask2 extends
			AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			String result;
			try {
				result = WebRequestManager.executeTask(params);
			} catch (IOException e) {
				result = e.getMessage();
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			if (result.equals(GeneralMessages.JSON_ERROR)
					|| result.equals(GeneralMessages.HTTP_CONNECTION_ERROR)
					|| result.contains(GeneralMessages.HTTP_NOT_FOUND_ERROR)) {
				OnTrackManager.getInstance().getListener()
						.onAsyncTaskError(result);
			}
		}

	}

	public static void executeSendTSTStatesTask(String... args) {
//		SendTSTStatesTask task = new SendTSTStatesTask();
//		task.execute(args);
		SendTSTStatesThread task = new SendTSTStatesThread(args);
		task.start();
	}

	public static void executeSendTSTStatesTask2(String... args) {
		SendTSTStatesTask2 task = new SendTSTStatesTask2();
		task.execute(args);
	}

	// AsyncTask para el envï¿½o de estado de los TrackedStopTrackables
	private static class SendFTStatesTask extends
			AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			String result;
			try {
				result = WebRequestManager.executeTask(params);
			} catch (IOException e) {
				result = GeneralMessages.HTTP_NOT_FOUND_ERROR;
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			if (result.equals(GeneralMessages.JSON_ERROR)
					|| result.equals(GeneralMessages.HTTP_CONNECTION_ERROR)
					|| result.contains(GeneralMessages.HTTP_NOT_FOUND_ERROR)) {
				OnTrackManager.getInstance().getListener()
						.onAsyncTaskError(result);
			} else {
				// Acciones por decidir, en este caso
				// no se sabe cómo se enviará la información de los footprint
				// trackables por pusher todavía
				OnTrackManager.getInstance().increaseTrackableFlag();
			}
		}

	}

	public static void executeSendFTStatesTask(String... args) {
		SendFTStatesTask task = new SendFTStatesTask();
		task.execute(args);
	}

	public static int doGoogleDirectionsRequest(int currentRequest) {
		// La siguiente variable modela la solicitud http hacia el web service
		// de Google Directions
		int newCurrentRequest = currentRequest + 1;

		ArrayList<String> query = new ArrayList<String>();

		// ArrayList<Stop> stops =
		// OnTrackSystem.getInstance().getUser().getRoute().getStops();

		// Para la primera solicitud, el origen es el primer paradero de la
		// lista de paraderos que ya ha sido cargada
		Stop origin = OnTrackManager.getInstance().getUser().getRoute()
				.getStops().get(currentRequest);

		// Para la primera solicitud, el destino es el paradero siguiente en
		// la
		// lista de paraderos que ya ha sido cargada

		Stop destination = OnTrackManager.getInstance().getUser().getRoute()
				.getStops().get(newCurrentRequest);

		boolean trackablesToPickOrLeaveAtStop = destination
				.trackablesToPickOrLeaveAtStop();

		boolean isCancelled = destination.isCancelled();

		boolean googleDirectionsRequired = !trackablesToPickOrLeaveAtStop
				|| isCancelled;

		// boolean googleDirectionsRequired = true;
		OnTrackManager.getInstance().setNextGoogleDirectionsRequest(
				newCurrentRequest);
		while ((!trackablesToPickOrLeaveAtStop || isCancelled)
				&& newCurrentRequest < OnTrackManager.getInstance().getUser()
				.getRoute().getStops().size()) {

			newCurrentRequest++;
			OnTrackManager.getInstance().setNextGoogleDirectionsRequest(
					newCurrentRequest);

			if (newCurrentRequest >= OnTrackManager.getInstance().getUser()
					.getRoute().getStops().size()) {
				break;
			} else {

				destination = OnTrackManager.getInstance().getUser().getRoute()
						.getStops().get(newCurrentRequest);
				trackablesToPickOrLeaveAtStop = destination
						.trackablesToPickOrLeaveAtStop();
				isCancelled = destination.isCancelled();
			}

		}

		// Si en el destination no hay estudiantes para recoger o dejar, se
		// pasa al siguiente paradero
		if (googleDirectionsRequired) {

			if (newCurrentRequest < OnTrackManager.getInstance().getUser()
					.getRoute().getStops().size()) {
				// TODO: Ignorar paraderos vacíos

				// destination.getSteps().clear();

				// Se aï¿½ade el URL base a la solicitud a Google directions
				query.add(URL_GOOGLE_DIRECTIONS_BASE);

				// Se aï¿½ade el parï¿½metro origin y su valor
				query.add("origin=" + origin.getLocation().getLatitude() + ","
						+ origin.getLocation().getLongitude());

				// Modela la cadena con la lista de waypoints
				String waypoints = "";

				// Los waypoints se van guardando en una lista
				ArrayList<Point> waypointsArray = destination.getWayPoints();
				if (waypointsArray.size() == 0) {
					waypoints = "";
				} else {
					waypoints = "&waypoints=";
				}
				for (int i = 0; i < waypointsArray.size(); i++) {
					Point waypoint = waypointsArray.get(i);
					if (i < waypointsArray.size() - 1) {
						try {
							waypoints += waypoint.getLatitude() + ","
									+ waypoint.getLongitude()
									+ URLEncoder.encode("|", "UTF-8");
						} catch (UnsupportedEncodingException e) {
							e.printStackTrace();
						}
					} else {
						waypoints += waypoint.getLatitude() + ","
								+ waypoint.getLongitude();
					}
				}

				// Se aï¿½ade el parï¿½metro destination y su valor
				query.add("&destination="
						+ destination.getLocation().getLatitude() + ","
						+ destination.getLocation().getLongitude());

				// Se aï¿½aden todos los waypoints intermedios
				query.add(waypoints);

				// Se especifica que la solicitud procede de un dispositivo con
				// sensor
				// de ubicaciï¿½n
				query.add("&sensor=true");
//				query.add("&key=AIzaSyBqRpShriwzdQjqsfVsevDQ1xrL5klm_j4");
				DecodeRouteTask decodeRouteTask = new DecodeRouteTask();
				decodeRouteTask
						.execute(query.toArray(new String[query.size()]));

			} else {
				DecodeRouteTask decodeRouteTask = new DecodeRouteTask();
				decodeRouteTask.execute();
			}
		} else {

			DecodeRouteTask decodeRouteTask = new DecodeRouteTask();
			decodeRouteTask.execute();
		}

		return newCurrentRequest;
	}

	// Qué tristeza, pero Google nos jodió: acá empieza la migración a Mapbox

	public static int doMapboxDirectionsRequest(int currentRequest) {
		// La siguiente variable modela la solicitud http hacia el web service
		// de Google Directions
		int newCurrentRequest = currentRequest + 1;

		String queryString = "";

		// Para la primera solicitud, el origen es el primer paradero de la
		// lista de paraderos que ya ha sido cargada
		Stop origin = OnTrackManager.getInstance().getUser().getRoute()
				.getStops().get(currentRequest);

		// Para la primera solicitud, el destino es el paradero siguiente en
		// la
		// lista de paraderos que ya ha sido cargada

		Stop destination = OnTrackManager.getInstance().getUser().getRoute()
				.getStops().get(newCurrentRequest);

		boolean trackablesToPickOrLeaveAtStop = destination
				.trackablesToPickOrLeaveAtStop();

		boolean isCancelled = destination.isCancelled();

		boolean hasSteps = destination.hasSteps();

		boolean googleDirectionsRequired = !trackablesToPickOrLeaveAtStop
				|| isCancelled || !hasSteps;

		OnTrackManager.getInstance().setNextGoogleDirectionsRequest(
				newCurrentRequest);
		while ((!trackablesToPickOrLeaveAtStop || isCancelled)
				&& newCurrentRequest < OnTrackManager.getInstance().getUser()
				.getRoute().getStops().size()) {

			newCurrentRequest++;
			OnTrackManager.getInstance().setNextGoogleDirectionsRequest(
					newCurrentRequest);

			if (newCurrentRequest >= OnTrackManager.getInstance().getUser()
					.getRoute().getStops().size()) {
				break;
			} else {

				destination = OnTrackManager.getInstance().getUser().getRoute()
						.getStops().get(newCurrentRequest);
				trackablesToPickOrLeaveAtStop = destination
						.trackablesToPickOrLeaveAtStop();
				isCancelled = destination.isCancelled();
			}

		}

		// Si en el destination no hay estudiantes para recoger o dejar, se
		// pasa al siguiente paradero
		if (googleDirectionsRequired) {
			if (newCurrentRequest < OnTrackManager.getInstance().getUser()
					.getRoute().getStops().size()) {

				// Se añade el URL base a la solicitud a Mapbox directions
				queryString += URL_MAPBOX_DIRECTIONS_BASE;


				// Se añade el parámetro origin y su valor
				queryString += ""+origin.getLocation().getLongitude()+","+origin.getLocation().getLatitude()+";";

				// Modela la cadena con la lista de waypoints
				String waypoints = "";

				// Los waypoints se van guardando en una lista
				ArrayList<Point> waypointsArray = destination.getWayPoints();
				if (waypointsArray.size() > 0) {
					for (int i = 0; i < waypointsArray.size(); i++) {
						Point waypoint = waypointsArray.get(i);
						waypoints += ""+waypoint.getLongitude() + ","
								+ waypoint.getLatitude()+";";

					}
				}

				queryString += waypoints;


				// Se añade el parámetro destination y su valor
				queryString += ""+destination.getLocation().getLongitude() + ","
						+ destination.getLocation().getLatitude();

				queryString += ".json?access_token="+ Parameters.MAP_BOX_TOKEN;
				queryString += "&geometry=polyline";



				// Se especifica que la solicitud procede de un dispositivo con
				// sensor
				// de ubicación

				DecodeRouteTask decodeRouteTask = new DecodeRouteTask();
				decodeRouteTask
						.execute(queryString);

			} else {
				DecodeRouteTask decodeRouteTask = new DecodeRouteTask();
				decodeRouteTask.execute();
			}
		} else {
			DecodeRouteTask decodeRouteTask = new DecodeRouteTask();
			decodeRouteTask.execute();
		}

		return newCurrentRequest;
	}
// Quitar el siguiente comentario del bloque de código si se usa google maps
/* 

	public static void decodePortionOfRoute(Location origin, Stop destination,
			boolean useWayPoints) {
		ArrayList<String> query = new ArrayList<String>();
		// Se aï¿½ade el URL base a la solicitud a Google directions
		query.add(URL_GOOGLE_DIRECTIONS_BASE);

		// Se aï¿½ade el parï¿½metro origin y su valor
		query.add("origin=" + origin.getLatitude() + ","
				+ origin.getLongitude());

		// Modela la cadena con la lista de waypoints
		String waypoints = "";

		// Los waypoints se van guardando en una lista
		ArrayList<Point> waypointsArray = destination.getWayPoints();
		if (useWayPoints) {
			if (waypointsArray.size() == 0) {
				waypoints = "";
			} else {
				waypoints = "&waypoints=";
			}
			for (int i = 0; i < waypointsArray.size(); i++) {
				Point waypoint = waypointsArray.get(i);
				if (i < waypointsArray.size() - 1) {
					try {
						waypoints += waypoint.getLatitude() + ","
								+ waypoint.getLongitude()
								+ URLEncoder.encode("|", "UTF-8");
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}
				} else {
					waypoints += waypoint.getLatitude() + ","
							+ waypoint.getLongitude();
				}
			}
		}

		// Se aï¿½ade el parï¿½metro destination y su valor
		query.add("&destination=" + destination.getLocation().getLatitude()
				+ "," + destination.getLocation().getLongitude());

		// Se aï¿½aden todos los waypoints intermedios
		query.add(waypoints);

		// Se especifica que la solicitud procede de un dispositivo con
		// sensor
		// de ubicaciï¿½n
		query.add("&sensor=true");
//		query.add("&key=AIzaSyBqRpShriwzdQjqsfVsevDQ1xrL5klm_j4");
		DecodePortionOfRouteTask task = new DecodePortionOfRouteTask(
				destination);
		task.execute(query.toArray(new String[query.size()]));

	} */

	public static void decodePortionOfRoute(Location origin, Stop destination,
											boolean useWayPoints) {
		String queryString = "";
		// Se añade el URL base a la solicitud a Mapbox directions
		queryString += URL_MAPBOX_DIRECTIONS_BASE;


		// Se añade el parámetro origin y su valor
		queryString += ""+origin.getLongitude()+","+origin.getLatitude()+";";

		// Modela la cadena con la lista de waypoints
		String waypoints = "";

		// Los waypoints se van guardando en una lista
		if(useWayPoints) {
			ArrayList<Point> waypointsArray = destination.getWayPoints();
			if (waypointsArray.size() > 0) {
				for (int i = 0; i < waypointsArray.size(); i++) {
					Point waypoint = waypointsArray.get(i);
					waypoints += "" + waypoint.getLongitude() + ","
							+ waypoint.getLatitude() + ";";

				}
			}
		}


		queryString += waypoints;


		// Se añade el parámetro destination y su valor
		queryString += ""+destination.getLocation().getLongitude() + ","
				+ destination.getLocation().getLatitude();

		queryString += ".json?access_token="+ Parameters.MAP_BOX_TOKEN;
		queryString += "&geometry=polyline";

		DecodePortionOfRouteTask task = new DecodePortionOfRouteTask(
				destination);
		task.execute(queryString);

	}

	/****************************************************/
	// Ésta es la tarea asíncrona que recalcula una porción de la ruta
	// Se indicará paso a paso lo que hace la tarea

	private static class DecodePortionOfRouteTask extends
			AsyncTask<String, Integer, String> {

		private Stop stop;

		public DecodePortionOfRouteTask(Stop stop) {
			this.stop = stop;
		}

		@Override
		protected String doInBackground(String... params) {
			String response = "";
			if (params.length > 0) {
				try {
					// Se hace el llamado al servicio que trae los puntos decodificados
					response = WebRequestManager
							.executeTaskGetWithoutFormatting(params);
					return response;
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			if (!result.equals("")) {
				//JSONManager
				//	.loadPortionOfRouteFromGoogleDirections(result, stop);

				// Se decodifica la información que acaba de llegar
				JSONManager
						.loadPortionOfRouteFromMapboxDirections(result, stop);

				// Se le avisa al activity que escucha que la ruta fue decodificada
				OnTrackManager.getInstance().getListener()
						.onPortionOfRouteDecoded(stop);

				// Se envía la información renovada de los stops y sus nuevos puntos para que quede guardada en el servidor
				SendTrackedStopInfoTask task = new SendTrackedStopInfoTask(true);
				try {
					task.execute(URL_BASE_TRACKED_STOP_INFO,
							ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance()
									.getToken(), ServiceKeys.TRACKED_STOPS_KEY,
							OnTrackManager.getInstance()
									.getStopDataJSONString());
				} catch (JSONException e) {
					OnTrackManager.getInstance().getListener()
							.onAsyncTaskError(e.getMessage());
				}


				// Se pone que no se desvió de la ruta ni se desvió en extremo, la verdad no recuerdo para qué se debe hacer esto
				OnTrackManager.getInstance().setDeviatedFromRoute(false);
				OnTrackManager.getInstance().setExtremelyDeviatedFromRoute(
						false);


				// Se envía la información actualizada del footprint, por cuestiones de tiempos y distancias estimadas
				SendFootprintInfoTask footprintInfoTask = new SendFootprintInfoTask();
				footprintInfoTask
						.execute(URL_BASE_FOOTPRINTINFO, ServiceKeys.TOKEN_KEY,
								OnTrackManager.getInstance().getToken(),
								ServiceKeys.FK_FOOTPRINT_KEY, ""
										+ OnTrackManager.getInstance()
										.getUser().getFootprint(),
								ServiceKeys.ESTIMATED_DISTANCE_KEY, ""
										+ OnTrackManager.getInstance()
										.totalDistanceOfRoute(),
								ServiceKeys.ESTIMATED_TIME_KEY, OnTrackManager
										.getInstance()
										.getEstimatedArrivalTimeOfRoute());
			}
		}

	}

// Quitar el comentario del siguiente bloque si se usa google maps
/*	public static void decodePortionOfRouteForDeviation(Location origin,
			Stop destination, boolean useWayPoints) {
		ArrayList<String> query = new ArrayList<String>();
		// Se aï¿½ade el URL base a la solicitud a Google directions
		query.add(URL_GOOGLE_DIRECTIONS_BASE);

		// Se aï¿½ade el parï¿½metro origin y su valor
		query.add("origin=" + origin.getLatitude() + ","
				+ origin.getLongitude());

		// Modela la cadena con la lista de waypoints
		String waypoints = "";

		// Los waypoints se van guardando en una lista
		ArrayList<Point> waypointsArray = destination.getWayPoints();
		if (useWayPoints) {
			if (waypointsArray.size() == 0) {
				waypoints = "";
			} else {
				waypoints = "&waypoints=";
			}
			for (int i = 0; i < waypointsArray.size(); i++) {
				Point waypoint = waypointsArray.get(i);
				if (i < waypointsArray.size() - 1) {
					try {
						waypoints += waypoint.getLatitude() + ","
								+ waypoint.getLongitude()
								+ URLEncoder.encode("|", "UTF-8");
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}
				} else {
					waypoints += waypoint.getLatitude() + ","
							+ waypoint.getLongitude();
				}
			}
		}

		// Se aï¿½ade el parï¿½metro destination y su valor
		query.add("&destination=" + destination.getLocation().getLatitude()
				+ "," + destination.getLocation().getLongitude());

		// Se aï¿½aden todos los waypoints intermedios
		query.add(waypoints);

		// Se especifica que la solicitud procede de un dispositivo con
		// sensor
		// de ubicaciï¿½n
		query.add("&sensor=true");
//		query.add("&key=AIzaSyBqRpShriwzdQjqsfVsevDQ1xrL5klm_j4");
		DecodePortionOfRouteForDeviationTask task = new DecodePortionOfRouteForDeviationTask(
				destination);
		task.execute(query.toArray(new String[query.size()]));

	}
    */

	public static void decodePortionOfRouteForDeviation(Location origin,
														Stop destination, boolean useWayPoints) {
		String queryString = "";
		// Se añade el URL base a la solicitud a Mapbox directions
		queryString += URL_MAPBOX_DIRECTIONS_BASE;


		// Se añade el parámetro origin y su valor
		queryString += ""+origin.getLongitude()+","+origin.getLatitude()+";";

		// Modela la cadena con la lista de waypoints
		String waypoints = "";

		// Los waypoints se van guardando en una lista
		if(useWayPoints) {
			ArrayList<Point> waypointsArray = destination.getWayPoints();
			if (waypointsArray.size() > 0) {
				for (int i = 0; i < waypointsArray.size(); i++) {
					Point waypoint = waypointsArray.get(i);
					waypoints += "" + waypoint.getLongitude() + ","
							+ waypoint.getLatitude() + ";";

				}
			}
		}

		queryString += waypoints;


		// Se añade el parámetro destination y su valor
		queryString += ""+destination.getLocation().getLongitude() + ","
				+ destination.getLocation().getLatitude();

		queryString += ".json?access_token="+ Parameters.MAP_BOX_TOKEN;
		queryString += "&geometry=polyline";
		DecodePortionOfRouteForDeviationTask task = new DecodePortionOfRouteForDeviationTask(
				destination);
		task.execute(queryString);

	}

	private static class DecodePortionOfRouteForDeviationTask extends
			AsyncTask<String, Integer, String> {

		private Stop stop;

		public DecodePortionOfRouteForDeviationTask(Stop stop) {
			this.stop = stop;
		}

		@Override
		protected String doInBackground(String... params) {
			String response = "";
			if (params.length > 0) {
				try {
					response = WebRequestManager
							.executeTaskGetWithoutFormatting(params);
					return response;
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			if (!result.equals("")) {
				//JSONManager.loadPortionOfRouteFromGoogleDirectionsForDeviation(
				//		result, stop);
				JSONManager.loadPortionOfRouteFromMapboxDirectionsForDeviation(
						result, stop);
				OnTrackManager.getInstance().getListener()
						.onPortionOfRouteDecoded(stop);
				// Evento de desviaciï¿½n de la ruta
				Event event = new Event(null, Timestamp.completeTimestamp(),
						Event.DEVIATED_ROUTE_CODE,
						GeneralMessages.DEVIATED_FROM_ROUTE, 0, 1, ""
						+ OnTrackManager.getInstance().getUser()
						.getFootprint(), OnTrackManager
						.getInstance().getUser().getRoute().getId(),
						false);
				// EventManager.getInstance().getEvents().add(event);
				// EventManager.getInstance().getStackedEvents().add(event);

				// Guardar evento en db local
				OnTrackManager.getInstance().getEvents().add(event);
				OnTrackManager.getInstance().getEventDao().insert(event);

				// Notificar a los listener de eventos que hay una nueva
				// notificaciï¿½n
				EventManager.getInstance().notifyEventGenerated(event.getId());

				try {
					EventManager.getInstance().executeSendEventTask(
							OnTrackManagerAsyncTasks.URL_BASE_ADD_EVENTS,
							ServiceKeys.TOKEN_KEY,
							OnTrackManager.getInstance().getToken(),
							ServiceKeys.EVENTS_KEY,
							event.getEventJSONString(Event.FK_FOOTPRINT_KEY));
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				SendTrackedStopInfoTask task = new SendTrackedStopInfoTask(true);
				try {
					task.execute(URL_BASE_TRACKED_STOP_INFO,
							ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance()
									.getToken(), ServiceKeys.TRACKED_STOPS_KEY,
							OnTrackManager.getInstance()
									.getStopDataJSONString());
				} catch (JSONException e) {
					OnTrackManager.getInstance().getListener()
							.onAsyncTaskError(e.getMessage());
				}

				OnTrackManager.getInstance().setDeviatedFromRoute(false);
				OnTrackManager.getInstance().setExtremelyDeviatedFromRoute(
						false);

				SendFootprintInfoTask footprintInfoTask = new SendFootprintInfoTask();
				footprintInfoTask
						.execute(URL_BASE_FOOTPRINTINFO, ServiceKeys.TOKEN_KEY,
								OnTrackManager.getInstance().getToken(),
								ServiceKeys.FK_FOOTPRINT_KEY, ""
										+ OnTrackManager.getInstance()
										.getUser().getFootprint(),
								ServiceKeys.ESTIMATED_DISTANCE_KEY, ""
										+ OnTrackManager.getInstance()
										.totalDistanceOfRoute(),
								ServiceKeys.ESTIMATED_TIME_KEY, OnTrackManager
										.getInstance()
										.getEstimatedArrivalTimeOfRoute());
			}
		}

	}

}
