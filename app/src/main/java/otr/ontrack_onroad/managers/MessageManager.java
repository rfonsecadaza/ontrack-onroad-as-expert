package otr.ontrack_onroad.managers;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import otr.ontrack_onroad.dao.ChatCard;
import otr.ontrack_onroad.dao.Event;
import otr.ontrack_onroad.dao.Message;
import otr.ontrack_onroad.models.Report;
import otr.ontrack_onroad.parameters.PusherParameters;
import otr.ontrack_onroad.parameters.ServiceKeys;
import otr.ontrack_onroad.utils.JSONManager;
import android.location.Location;
import android.util.Log;

import com.pusher.client.Pusher;
import com.pusher.client.channel.Channel;
import com.pusher.client.channel.SubscriptionEventListener;
import com.pusher.client.connection.ConnectionEventListener;
import com.pusher.client.connection.ConnectionState;
import com.pusher.client.connection.ConnectionStateChange;

public class MessageManager {
	// Este síngleton se encarga de manejar todas las entidades relacionadas con
	// el chat
	// Puede tener varios listeners

	public static final String URL_BASE_SEND_MESSAGES = "ApiMessage/sendMessages";
	public static final String URL_BASE_GET_MESSAGES = "ApiMessage/getMessages";

	public static final long CHECK_PUSHER_PERIOD = 2000;
	private static MessageManager mInstance = null;

	private ArrayList<MessageManagerListener> listeners;
	private List<ChatCard> chatCards;
	private long selectedChatCard;
	private String lastDateNewMessages;

	// Pusher
	private static Pusher pusher;
	private static Channel channel;

	private long lastEventId = -1;

	public String getLastDateNewMessages() {
		return lastDateNewMessages;
	}

	public void setLastDateNewMessages(String lastDateNewMessages) {
		this.lastDateNewMessages = lastDateNewMessages;
	}

	private static boolean enabledToGetMessages;

	private static boolean onceConnected = false;

	private MessageManager() {
		this.setChatCards(new ArrayList<ChatCard>());
		this.listeners = new ArrayList<MessageManagerListener>();
		this.lastDateNewMessages = "";
		this.enabledToGetMessages = true;

		if (!onceConnected) {

			pusher = new Pusher(PusherParameters.API_KEY);

			pusher.connect(new ConnectionEventListener() {

				@Override
				public void onError(String message, String code, Exception e) {
					System.out.println("There was a problem connecting!");
				}

				@Override
				public void onConnectionStateChange(ConnectionStateChange change) {

					// En este momento no se usa el onceConnected, pero tal vez
					// en el futuro se usará
					if (change.getCurrentState() == ConnectionState.CONNECTED) {
						onceConnected = true;
					}

				}

			}, ConnectionState.ALL);

			channel = pusher.subscribe(""
					+ OnTrackManager.getInstance().getUser().getId());

			//
			channel.bind(PusherParameters.EVENT_NEW_MESSAGE,
					new SubscriptionEventListener() {

						@Override
						public void onEvent(String channel, String event,
								String data) {
							try {
								ArrayList<Long> ids = JSONManager
										.loadMessageFromNotification(data);
								for (MessageManagerListener listener : MessageManager.this.listeners)
									listener.onReceiveMessage(ids);
							} catch (JSONException e) {
								e.printStackTrace();
							}

						}
					});

			channel.bind(PusherParameters.EVENT_NEW_EVENT,
					new SubscriptionEventListener() {

						@Override
						public void onEvent(String channel, String event,
								String data) {
							try {
								Event ontrackEvent = JSONManager.loadEvent(data);
								if(ontrackEvent != null) {
									if (ontrackEvent.getCategory().equals(Event.ROUTE_MESSAGE_CODE)
											|| ontrackEvent.getCategory().equals(Event.GENERAL_MESSAGE_CODE)) {
										EventManager.getInstance().setLastAnnouncement(ontrackEvent);
										for (MessageManagerListener listener : MessageManager.this.listeners)
											listener.onReceivePusherEvent(ontrackEvent);
									}
								}
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						}
					});

			channel.bind(PusherParameters.EVENT_NEW_REPORT_RESPONSE,
					new SubscriptionEventListener() {

						@Override
						public void onEvent(String channel, String event,
											String data) {
							try {
								Report report = JSONManager.loadReport(data);

								for (MessageManagerListener listener : MessageManager.this.listeners)
									listener.onReceivePusherReport(report);
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						}
					});
		}
		// CONFIGURACIÓN DE REVISIÓN PERIÓDICA DEL ESTADO DEL PUSHER
		Timer checkConnectionStateTimer = new Timer();
		checkConnectionStateTimer.schedule(new TimerTask() {
			public void run() {
				if (pusher.getConnection().getState() != ConnectionState.CONNECTED) {
					pusher.connect();
				}

			}
		}, CHECK_PUSHER_PERIOD, CHECK_PUSHER_PERIOD);
	}

	public static MessageManager getInstance() {
		if (mInstance == null) {
			mInstance = new MessageManager();
		}
		return mInstance;
	}

	public ArrayList<MessageManagerListener> getListeners() {
		return listeners;
	}

	public void setListeners(ArrayList<MessageManagerListener> listeners) {
		this.listeners = listeners;
	}

	public List<ChatCard> getChatCards() {
		return chatCards;
	}

	public void setChatCards(List<ChatCard> chatCards) {
		this.chatCards = chatCards;
	}

	public long getSelectedChatCard() {
		return selectedChatCard;
	}

	public void setSelectedChatCard(long selectedChatCard) {
		this.selectedChatCard = selectedChatCard;
	}

	public ChatCard getSelectedChatCardEntity() {
		return OnTrackManager.getInstance().getChatCardFromLocalDB(
				selectedChatCard);
	}

	public ChatCard getChatCardById(long id) {

		for (ChatCard chatCard : this.chatCards) {
			if (chatCard.getId() == id)
				return chatCard;
		}
		return null;
	}

	public String getMessagesToSendJSONString() throws JSONException {
		JSONObject jsonObject = new JSONObject();
		for (ChatCard chatCard : this.chatCards) {
			JSONObject jsonChatCard = new JSONObject();

			if (chatCard.getMessages().size() != 1) {
				for (Message message : chatCard.getMessages()) {
					JSONObject jsonMessage = new JSONObject();
					jsonMessage.put(ServiceKeys.MESSAGE_KEY,
							message.getMessageText());
					jsonMessage.put(ServiceKeys.DATE_WROTE_KEY,
							message.getDate());
					jsonChatCard.accumulate(ServiceKeys.MESSAGES_KEY,
							jsonMessage);

				}
			} else {
				Message message = chatCard.getMessages().get(0);
				JSONObject jsonMessage = new JSONObject();
				jsonMessage.put(ServiceKeys.MESSAGE_KEY,
						message.getMessageText());
				jsonMessage.put(ServiceKeys.DATE_WROTE_KEY, message.getDate());
				JSONArray jsonHack = new JSONArray();
				jsonHack.put(jsonMessage);
				jsonChatCard.put(ServiceKeys.MESSAGES_KEY, jsonHack);

			}
			jsonChatCard.put(ServiceKeys.ID_KEY, chatCard.getId());
			jsonObject.accumulate(ServiceKeys.CHATCARDS_KEY, jsonChatCard);
		}
		// Log.d("Opción 1", jsonObject.toString());
		// Log.d("Opcion 3","{"+jsonObject.getString("TRACKED_STOPS")+"}");

		if (this.chatCards.size() == 1) {
//			Log.d("JSON de chatcards",
//					"[" + jsonObject.getString(ServiceKeys.CHATCARDS_KEY) + "]");
			return "[" + jsonObject.getString(ServiceKeys.CHATCARDS_KEY) + "]";
		} else {
//			Log.d("JSON de chatcards de más de uno",
//					jsonObject.getString(ServiceKeys.CHATCARDS_KEY));
			return jsonObject.getString(ServiceKeys.CHATCARDS_KEY);
		}

	}

	public String getMessagesToSendFromSelectedChatCardJSONString()
			throws JSONException {

		ChatCard chatCard = getSelectedChatCardEntity();
		JSONObject jsonObject = new JSONObject();
		JSONObject jsonChatCard = new JSONObject();

		if (chatCard.getMessages().size() != 1) {
			for (Message message : chatCard.getMessages()) {
				JSONObject jsonMessage = new JSONObject();
				jsonMessage.put(ServiceKeys.MESSAGE_KEY,
						message.getMessageText());
				jsonMessage.put(ServiceKeys.DATE_WROTE_KEY, message.getDate());
				jsonChatCard.put(ServiceKeys.MESSAGES_KEY, jsonMessage);

			}
		} else {
			Message message = chatCard.getMessages().get(0);
			JSONObject jsonMessage = new JSONObject();
			jsonMessage.put(ServiceKeys.MESSAGE_KEY, message.getMessageText());
			jsonMessage.put(ServiceKeys.DATE_WROTE_KEY, message.getDate());
			JSONArray jsonHack = new JSONArray();
			jsonHack.put(jsonMessage);
			jsonChatCard.put(ServiceKeys.MESSAGES_KEY, jsonHack);

		}
		jsonChatCard.put(ServiceKeys.ID_KEY, chatCard.getId());
		jsonObject.accumulate(ServiceKeys.CHATCARDS_KEY, jsonChatCard);

		// Log.d("Opción 1", jsonObject.toString());
		// Log.d("Opcion 3","{"+jsonObject.getString("TRACKED_STOPS")+"}");

//		Log.d("JSON de chatcards",
//				jsonObject.getString(ServiceKeys.CHATCARDS_KEY));
		return "[" + jsonObject.getString(ServiceKeys.CHATCARDS_KEY) + "]";

	}

	public String getMessagesToSendFromSelectedChatCardJSONString(
			int messageIndex) throws JSONException {

		ChatCard chatCard = getSelectedChatCardEntity();
		Message message = chatCard.getMessages().get(messageIndex);
		JSONObject jsonObject = new JSONObject();
		JSONObject jsonChatCard = new JSONObject();

		JSONObject jsonMessage = new JSONObject();
		jsonMessage.put(ServiceKeys.MESSAGE_KEY, message.getMessageText());
		jsonMessage.put(ServiceKeys.DATE_WROTE_KEY, message.getDate());
		JSONArray jsonHack = new JSONArray();
		jsonHack.put(jsonMessage);
		jsonChatCard.put(ServiceKeys.MESSAGES_KEY, jsonHack);

		jsonChatCard.put(ServiceKeys.ID_KEY, chatCard.getServerId());
		jsonObject.accumulate(ServiceKeys.CHATCARDS_KEY, jsonChatCard);

		return "[" + jsonObject.getString(ServiceKeys.CHATCARDS_KEY) + "]";

	}

	public void addListener(MessageManagerListener listener) {
		this.listeners.add(listener);
	}

	public int getPositionOfChatCardById(int id) {
		int position = 0;
		for (ChatCard chatcard : this.chatCards) {
			if (chatcard.getId() == id) {
				return position;
			}
			position++;
		}
		return -1;
	}

	public String idsOfChatCards() {
		String ids = "";
		for (ChatCard chatCard : this.chatCards) {
			ids += "" + chatCard.getId() + " ";
		}
		return ids;
	}

	public void reportMessagesRead(long chatCardId) {
		for (MessageManagerListener listener : this.listeners)
			listener.onMessagesRead(chatCardId);
	}

	public void removeListener(MessageManagerListener listener) {
		listeners.remove(listener);
	}

	public void playLastAnnouncement() {
		if(EventManager.getInstance().getLastAnnouncement()!=null) {
			for (MessageManagerListener listener : MessageManager.this.listeners) {
				listener.onReceivePusherEvent(EventManager.getInstance().getLastAnnouncement());
			}
		}
	}

}
