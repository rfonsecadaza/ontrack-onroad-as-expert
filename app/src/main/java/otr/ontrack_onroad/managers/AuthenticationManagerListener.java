package otr.ontrack_onroad.managers;

public interface AuthenticationManagerListener {
	
	public void onRoutePreloaded();
	public void onRouteLoaded();
	public void onRouteNotLoaded();
	public void onAuthenticationError(String error);
	public void onChatcardsLoaded();
	public void onChatcardsError(String message);
}
