package otr.ontrack_onroad.managers;

import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;

import otr.ontrack_onroad.parameters.GeneralMessages;
import otr.ontrack_onroad.parameters.Parameters;
import otr.ontrack_onroad.parameters.ServiceKeys;
import otr.ontrack_onroad.utils.JSONManager;
import otr.ontrack_onroad.utils.WebRequestManager;

import android.os.AsyncTask;

public class LoginManager {
	private static LoginManager mInstance = null;

	private LoginManagerListener listener;

	private LoginManager() {
	}

	public static LoginManager getInstance() {
		if (mInstance == null) {
			mInstance = new LoginManager();
		}
		return mInstance;
	}

	public void setListener(LoginManagerListener listener) {
		this.listener = listener;
	}
	
//	public void removeListener(LoginManagerListener listener){
//		this.listeners.remove(listener);
//	}


	private class LoginTask extends AsyncTask<String, Integer, String> {

		@Override
		protected void onPreExecute() {
				LoginManager.this.listener.onPrelogin();
		}

		@Override
		protected String doInBackground(String... args) {
			String result = "";
			try {
				result = WebRequestManager.executeTask(args);

			} catch (IOException e) {
				result = GeneralMessages.HTTP_NOT_FOUND_ERROR;
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			if (result.equals(GeneralMessages.HTTP_CONNECTION_ERROR)
					|| result.equals(GeneralMessages.HTTP_NOT_FOUND_ERROR)) {

				LoginManager.this.listener.onLoginError(result);

				// loginIncorrectDialog
				// .setTitle(GeneralMessages.DIALOG_TITLE_ERROR);
				// loginIncorrectDialog.setMessage(result);
				// loginIncorrectDialog.setButton("OK",
				// new DialogInterface.OnClickListener() {
				// public void onClick(DialogInterface dialog,
				// int which) {
				// }
				// });
				//
				// loginIncorrectDialog.show();

			} else {
				// Si el usuario es válido...
				int status;
				try {
					status = JSONManager.login(result);
					if (status == Parameters.STATUS_CORRECT) {
						LoginManager.this.listener.onLogin();

					} else {
						// Si el nombre de usuario o contraseï¿½a son incorrectos,
						// o
						// si se
						// intentï¿½ iniciar sesiï¿½n para OnTrack-Provider
						LoginManager.this.listener.onNotLogin();
					}
				} catch (JSONException e) {
					LoginManager.this.listener.onLoginError(GeneralMessages.JSON_ERROR);
				}

			}
		}

	}

	public void executeLoginTask(String... params) {
		LoginTask task = new LoginTask();
		task.execute(params);
	}

	private class ForgotPasswordTask extends AsyncTask<String, Integer, String> {

		@Override
		protected void onPreExecute() {
			LoginManager.this.listener.onPrelogin();
		}

		@Override
		protected String doInBackground(String... params) {
			String result = "";
			try {
				return WebRequestManager.executeTaskGet(params);
			} catch (IOException e) {
				return e.getMessage();
			}
		}

		@Override
		protected void onPostExecute(String result) {
			JSONObject jsonObject;
//			isRequestingNewActivationCode = false;
			try {
				jsonObject = new JSONObject(result);
				String message = jsonObject.getString(ServiceKeys.ERROR_KEY);
				
				LoginManager.this.listener.onForgotPasswordRequest(message);
			} catch (JSONException e) {
				LoginManager.this.listener.onForgotPasswordRequest(GeneralMessages.JSON_ERROR);
			}

		}

	}
	
	public void executeForgotPasswordTask(String...params){
		ForgotPasswordTask task = new ForgotPasswordTask();
		task.execute(params);
	}

}
