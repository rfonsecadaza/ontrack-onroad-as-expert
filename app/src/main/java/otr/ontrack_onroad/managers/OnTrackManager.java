package otr.ontrack_onroad.managers;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.crypto.AEADBadTagException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import otr.ontrack_onroad.dao.ChatCard;
import otr.ontrack_onroad.dao.ChatCardDao;
import otr.ontrack_onroad.dao.ChatUser;
import otr.ontrack_onroad.dao.ChatUserDao;
import otr.ontrack_onroad.dao.DaoMaster;
import otr.ontrack_onroad.dao.DaoSession;
import otr.ontrack_onroad.dao.Event;
import otr.ontrack_onroad.dao.EventDao;
import otr.ontrack_onroad.dao.Message;
import otr.ontrack_onroad.dao.MessageDao;
import otr.ontrack_onroad.dao.Notification;
import otr.ontrack_onroad.dao.NotificationDao;
import otr.ontrack_onroad.dao.SimLocation;
import otr.ontrack_onroad.dao.SimLocationDao;
import otr.ontrack_onroad.dao.Simulation;
import otr.ontrack_onroad.dao.SimulationDao;
import otr.ontrack_onroad.models.Point;
import otr.ontrack_onroad.models.Refuge;
import otr.ontrack_onroad.models.Route;
import otr.ontrack_onroad.models.Stop;
import otr.ontrack_onroad.models.Trackable;
import otr.ontrack_onroad.models.TrackedPoint;
import otr.ontrack_onroad.models.User;
import otr.ontrack_onroad.parameters.GeneralMessages;
import otr.ontrack_onroad.parameters.Parameters;
import otr.ontrack_onroad.parameters.ServiceKeys;
import otr.ontrack_onroad.tasks.GetDirectionsToRefugeTask;
import otr.ontrack_onroad.utils.Chronometer;
import otr.ontrack_onroad.utils.GeoPos;
import otr.ontrack_onroad.utils.Timestamp;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import com.mapbox.mapboxsdk.geometry.LatLng;

import de.greenrobot.dao.query.Query;

public class OnTrackManager
{

    // Prohibir simulaciones en la versión final
    private boolean simulationsAllowed = true;

    public static final int MAXIMUM_SPEED_IN_KM_H = 80;
    public static final int MAXIMUM_TIME_IN_EXCEEDED_SPEED = 12; // En segundos
    public static final int TIMER_PERIOD = 1000; // Milliseconds
    public static final int MAXIMUM_TIME_AT_STOP = 60; // En segundos

    public static final int MAXIMUM_STACKED_POINTS = 15;

    public static final int DEVIATION_COUNTER_LIMIT = 3;

    public static final int MAXIMUM_POINTS_AT_ZERO_STOP = 5;

    public static final int MAXIMUM_GPS_ACCURACY_ALLOWED = 60;

    public static final int MAXIMUM_DELAY_ALLOWED_IN_MINUTES = 5;
    public static final int MAXIMUM_ADVANCE_ALLOWED_IN_MINUTES = 5;

    // Máximo de veces permitido que se puede saltar el siguiente paradero
    private static final int MAXIMUM_SKIPPED_NEXT_STOP = 2;

    // Periodo de envío de datos (debe ser mayor o igual a 1)
    private static final int SEND_LOCATION_INTERVAL = 3;

    // Periodo de envío lento de datos
    private static final int SLOW_SEND_LOCATION_INTERVAL = 30;

    // ESTADOS DEL CICLO DE INICIO DE RUTA
    public static final int START_CYCLE_PRELOAD = -1;
    public static final int START_CYCLE_NOT_STARTED = 0;
    public static final int START_CYCLE_REQUESTING_FOOTPRINT = 1;
    public static final int START_CYCLE_SENDING_TST_INFO = 2;
    public static final int START_CYCLE_RELOADING_ROUTE = 2;
    public static final int START_CYCLE_COMPUTING_ROUTE = 3;
    public static final int START_CYCLE_SENDING_TRACKED_STOP_INFO = 4;
    public static final int START_CYCLE_FINISHED = 5;

    public static final int START_CYCLE_TOTAL_STEPS = 5;

    private static OnTrackManager mInstance = null;
    private Location previousLocation;
    private double previousSpeed = -1;

    private OnTrackManagerListener listener;

    // Context
    private Context context;

    // Verificación del estado de la ruta
    private boolean routeDetailsLoaded;
    private boolean routeStarting;
    private boolean routeStarted;
    private boolean routeReloaded;
    private boolean routeFinished;
    private boolean stopped;
    private boolean closeToNextStop;
    private boolean hasLeftStop;
    private boolean speedExceeded;
    private boolean deviatedFromRoute;
    private boolean extremelyDeviatedFromRoute;
    private boolean gpsAccuracyLow;
    private boolean alertTime;



    private boolean delaySent;
    private boolean aheadSent;


    private boolean waitingGPS;

    // Información del usuario y sus rutas
    private User user;
    private String userString = null;
    private String token;
    private int userDeviceId;
    private ArrayList<Route> routes; // Lista sin detalles de todas las rutas
    // que maneja el conductor
    private int selectedRoute; // Identificador de la ruta seleccionada en el
    // arreglo routes (posici�n)

    private int selectedTrackable; // Identificador de un trackable seleccionado
    // en el arreglo de todos los trackables de
    // la ruta (posici�n)

    // Cron�metros
    private Chronometer vehicleStoppedChronometer;
    private Chronometer timeSinceLastStopChronometer;
    private Chronometer totalRouteTimeChronometer;

    // Informaci�n de localizaci�n y paraderos
    private Location currentLocation = null;
    private Location referenceLocation;
    private Stop currentStop; // �ltimo paradero en el que estuvo el
    // veh�culo
    private Stop nextStop; // Pr�ximo paradero
    private ArrayList<Stop> trackedStops; // Lista de todos los paraderos en los
    // que se ha detenido el bus

    // Informaci�n de rastreo
    private ArrayList<TrackedPoint> trackedPoints;
    private ArrayList<TrackedPoint> stackedPoints;

    // Chat
    private ArrayList<ChatCard> chatCards;

    // Otras (uso por definir)
    private boolean useCalculationsFromGoogle;
    private int totalStoppedSeconds;
    private int timeDelayed; // En minutos
    private static int googleDirectionsRequests;
    private static int nextGoogleDirectionsRequest;

    // Base de datos general
    private DaoMaster daoMaster;
    private DaoMaster.DevOpenHelper helper;
    private DaoSession daoSession;
    private SQLiteDatabase db;
    private SimulationDao simulationDao;
    private SimLocationDao simLocationDao;
    private EventDao eventDao;
    private Simulation simulation;
    private NotificationDao notificationDao;
    private ChatCardDao chatCardDao;
    private MessageDao messageDao;
    private ChatUserDao chatUserDao;

    // Lista de eventos
    private List<Event> events;

    // Activar o desactivar simulaci�n
    private boolean isSimulating;
    private boolean isRecordingSimulation = false;
    private int simulationCounter;
    private List<SimLocation> recordedPoints;

    // FLAGS: para avisarle a los que ven el recorrido sobre diferentes eventos
    // de la ruta
    private int timeFlag;
    private int distanceFlag;
    private int stopFlag;
    private int trackableFlag;



    // Estado del ciclo de inicio de ruta
    private int startCycleState;

    // Algo para detectar la desviaci�n que no se ha utilizado
    private int deviationCounter;

    // Contador de puntos en velocidad 0
    private int zeroSpeedCounter;

    // Contador de veces que salt� el paradero siguiente
    private int nextStopSkipCounter;

    // Contador de nuevos puntos generados
    private int newPointsGenerated;

    private boolean isAheadInTime;

    private ArrayList<Route> myRoutes;

    private ArrayList<Refuge> refuges;

    public boolean isEnabledReplacement() {
        return enabledReplacement;
    }

    public void setEnabledReplacement(boolean enabledReplacement) {
        this.enabledReplacement = enabledReplacement;
    }

    // Datos de relevos
    private boolean enabledReplacement = false;

    private String replacementDriver = null;
    private String replacementMonitor = null;
    private String replacementVehiclePlate = null;

    private OnTrackManager( )
    {
        this.token = "";
        this.setUserDeviceId( -1 );
        routeStarted = false;
        setRouteFinished(false);
        this.routeDetailsLoaded = false;
        this.routeStarting = false;

        stopped = false;
        closeToNextStop = false;
        hasLeftStop = true;
        setSpeedExceeded( false );
        deviatedFromRoute = false;
        extremelyDeviatedFromRoute = false;
        gpsAccuracyLow = false;

        vehicleStoppedChronometer = new Chronometer( );
        timeSinceLastStopChronometer = new Chronometer( );
        totalRouteTimeChronometer = new Chronometer( );

        trackedStops = new ArrayList<Stop>( );
        trackedPoints = new ArrayList<TrackedPoint>( );
        stackedPoints = new ArrayList<TrackedPoint>( );

        chatCards = new ArrayList<ChatCard>( );

        useCalculationsFromGoogle = true;

        this.selectedRoute = -1;
        this.selectedTrackable = -1;

        this.routeReloaded = false;

        this.waitingGPS = true;

        setTotalStoppedSeconds( 0 );

        this.setSimulating(false);
        this.setRecordingSimulation(false);
        this.setSimulationCounter(0);

        this.timeDelayed = 0;

        this.timeFlag = -1;
        this.distanceFlag = -1;
        this.stopFlag = -1;
        this.trackableFlag = -1;

        this.startCycleState = START_CYCLE_PRELOAD;

        this.deviationCounter = 0;

        this.zeroSpeedCounter = 0;

        this.nextStopSkipCounter = 0;

        this.referenceLocation = null;

        this.alertTime = false;

        this.aheadSent = false;
        this.delaySent = false;

        this.isAheadInTime = false;

        this.newPointsGenerated = 0;

        refuges = new ArrayList<>( );

    }

    public OnTrackManagerListener getListener( )
    {
        return listener;
    }

    public void setListener( OnTrackManagerListener listener )
    {
        this.listener = listener;
    }

    public EventDao getEventDao( )
    {
        return eventDao;
    }

    public void setEventDao( EventDao eventDao )
    {
        this.eventDao = eventDao;
    }

    // Inicializar base de datos
    public void initializeDataBase( )
    {

        // context.deleteDatabase("simulation-db");

        helper = new DaoMaster.DevOpenHelper( this.context, "simulation-db", null );
        db = helper.getWritableDatabase( );

        daoMaster = new DaoMaster( db );
        daoSession = daoMaster.newSession( );

        simulationDao = daoSession.getSimulationDao( );
        simLocationDao = daoSession.getSimLocationDao( );
        notificationDao = daoSession.getNotificationDao( );
        chatCardDao = daoSession.getChatCardDao( );
        messageDao = daoSession.getMessageDao( );
        chatUserDao = daoSession.getChatUserDao( );

        eventDao = daoSession.getEventDao( );

        Query eventQuery = eventDao.queryBuilder( ).build( );
        this.events = eventQuery.list( );

        // Borrar db, en casos de emergencia
        // daoSession.getSimLocationDao().deleteAll();
        // daoSession.getSimulationDao().deleteAll();
        daoSession.getEventDao().deleteAll();
        // daoSession.getChatCardDao().deleteAll();
        // daoSession.getChatUserDao().deleteAll();
    }

    // Actualizar lista de eventos
    public void updateEvents( )
    {
        Query eventQuery = eventDao.queryBuilder().build( );
        // this.events.clear();
        this.events = eventQuery.list( );
    }

    // Obtener la lista de simulaciones relacionadas con la ruta seleccionada
    public List<Simulation> getSimulationsOfRoute( String routeId )
    {
        Query query = simulationDao.queryBuilder( ).where(SimulationDao.Properties.RouteId.eq(routeId)).build( );
        if( query.list( ).size( ) != 0 )
            return query.list( );
        return null;
    }

    public List<SimLocation> getSimLocationsOfSimulation( int simulationId )
    {
        Query query = simLocationDao.queryBuilder( ).where(SimLocationDao.Properties.SimulationId.eq(simulationId)).build( );

        // Asigna la consulta a los puntos guardados
        this.recordedPoints = query.list( );
        return query.list( );
    }

    public void resetRoute( )
    {

        routeStarted = false;
        setRouteFinished(false);
        setSpeedExceeded( false );
        this.routeDetailsLoaded = false;
        this.routeStarting = false;

        stopped = false;
        closeToNextStop = false;
        hasLeftStop = true;
        deviatedFromRoute = false;
        extremelyDeviatedFromRoute = false;
        gpsAccuracyLow = false;

        this.waitingGPS = true;

        vehicleStoppedChronometer = new Chronometer( );
        timeSinceLastStopChronometer = new Chronometer( );
        totalRouteTimeChronometer = new Chronometer( );

        trackedStops = new ArrayList<Stop>( );
        trackedPoints = new ArrayList<TrackedPoint>( );
        stackedPoints = new ArrayList<TrackedPoint>( );

        chatCards = new ArrayList<ChatCard>( );

        useCalculationsFromGoogle = true;

        setTotalStoppedSeconds(0);

        this.user.setRoute( null );

        this.currentStop = null;
        this.nextStop = null;

        this.selectedRoute = -1;
        this.selectedTrackable = -1;
        this.googleDirectionsRequests = 0;
        this.setNextGoogleDirectionsRequest( 1 );

        if( listener != null )
            listener.onDisconnectLocation( );

        this.currentLocation = null;

        this.routeReloaded = false;

        this.setSimulating(false);
        this.setRecordingSimulation(false);
        this.setSimulationCounter( 0 );

        this.timeFlag = -1;
        this.distanceFlag = -1;
        this.stopFlag = -1;
        this.trackableFlag = -1;

        this.startCycleState = START_CYCLE_PRELOAD;

        this.deviationCounter = 0;

        this.zeroSpeedCounter = 0;

        this.nextStopSkipCounter = 0;

        this.referenceLocation = null;

        this.alertTime = false;

        this.aheadSent = false;
        this.delaySent = false;

        this.isAheadInTime = false;

        this.newPointsGenerated = 0;

    }

    public void resetAllData( )
    {
        resetRoute( );
        this.token = "";
        this.user = null;
        this.userDeviceId = -1;
        this.userString = null;
        this.routes = null;
        this.listener = null;
    }


    public static OnTrackManager getInstance( )
    {
        if( mInstance == null )
        {
            mInstance = new OnTrackManager( );
        }
        return mInstance;
    }

    public Context getContext( )
    {
        return context;
    }

    public void setContext( Context context )
    {
        this.context = context;
    }

    public boolean isWaitingGPS() {
        return this.waitingGPS;
    }

    public void setWaitingGPS(boolean waitingGPS) {
        this.waitingGPS = waitingGPS;
    }


    public boolean isRouteStarting() {
        return routeStarting;
    }

    public void setRouteStarting(boolean routeStarting) {
        this.routeStarting = routeStarting;
    }

    public boolean isRouteDetailsLoaded() {
        return routeDetailsLoaded;
    }

    public void setRouteDetailsLoaded(boolean routeDetailsLoaded) {
        this.routeDetailsLoaded = routeDetailsLoaded;
    }

    public boolean isDelaySent() {
        return delaySent;
    }

    public void setDelaySent(boolean delaySent) {
        this.delaySent = delaySent;
    }

    public boolean isAheadSent() {
        return aheadSent;
    }

    public void setAheadSent(boolean aheadSent) {
        this.aheadSent = aheadSent;
    }



    public boolean isRecordingSimulation( )
    {
        return isRecordingSimulation;
    }

    public void setRecordingSimulation( boolean isRecordingSimulation )
    {
        this.isRecordingSimulation = isRecordingSimulation;
    }

    public boolean isSimulating( )
    {
        return isSimulating;
    }

    public void setSimulating( boolean isSimulating )
    {
        this.isSimulating = isSimulating;
    }

    public boolean isHasLeftStop( )
    {
        return hasLeftStop;
    }

    public void setHasLeftStop( boolean hasLeftStop )
    {
        this.hasLeftStop = hasLeftStop;
    }

    public boolean isDeviatedFromRoute( )
    {
        return deviatedFromRoute;
    }

    public void setDeviatedFromRoute( boolean deviatedFromRoute )
    {
        this.deviatedFromRoute = deviatedFromRoute;
    }

    public boolean isExtremelyDeviatedFromRoute( )
    {
        return extremelyDeviatedFromRoute;
    }

    public void setExtremelyDeviatedFromRoute( boolean extremelyDeviatedFromRoute )
    {
        this.extremelyDeviatedFromRoute = extremelyDeviatedFromRoute;
    }

    public boolean isGpsAccuracyLow( )
    {
        return gpsAccuracyLow;
    }

    public void setGpsAccuracyLow( boolean gpsAccuracyLow )
    {
        this.gpsAccuracyLow = gpsAccuracyLow;
    }

    public int getSimulationCounter( )
    {
        return simulationCounter;
    }

    public void setSimulationCounter( int simulationCounter )
    {
        this.simulationCounter = simulationCounter;
    }

    public void increaseSimulationCounter( )
    {
        this.simulationCounter++;
    }

    public List<SimLocation> getRecordedPoints( )
    {
        return recordedPoints;
    }

    public void setRecordedPoints( List<SimLocation> recordedPoints )
    {
        this.recordedPoints = recordedPoints;
    }

    public Simulation getSimulation( )
    {
        return simulation;
    }

    public void setSimulation( Simulation simulation )
    {
        this.simulation = simulation;
        simulationDao.insert( simulation );
    }

    public SimLocationDao getSimLocationDao( )
    {
        return simLocationDao;
    }

    public void setSimLocationDao( SimLocationDao simLocationDao )
    {
        this.simLocationDao = simLocationDao;
    }

    public NotificationDao getNotificationDao( )
    {
        return notificationDao;
    }

    public void setNotificationDao( NotificationDao notificationDao )
    {
        this.notificationDao = notificationDao;
    }

    public void insertSimLocation( Location location )
    {
        SimLocation simLocation = new SimLocation( null, location, simulation.getId( ) );
        simLocationDao.insert( simLocation );
    }

    public long insertEventToLocalDB( Event event )
    {
        eventDao.insert( event );
        return event.getId( );
    }

    public Event getEventFromLocalDB( long id )
    {
        Query query = eventDao.queryBuilder( ).where( EventDao.Properties.Id.eq( id ) ).build( );
        if( query.list( ).size( ) > 0 )
        {
            Event event = ( Event )query.list( ).get( 0 );
            return event;
        }
        return null;
    }

    public long insertNotificationToLocalDB( Notification notification )
    {
        notificationDao.insert(notification);
        return notification.getId( );
    }

    public void deleteNotificationsFromLocalDB( )
    {
        daoSession.getNotificationDao( ).deleteAll();
    }

    public ChatCardDao getChatCardDao( )
    {
        return chatCardDao;
    }

    public void setChatCardDao( ChatCardDao chatCardDao )
    {
        this.chatCardDao = chatCardDao;
    }

    public long insertChatCardToLocalDB( ChatCard chatCard )
    {
        chatCardDao.insert( chatCard );
        return chatCard.getId( );
    }

    public ChatCard getChatCardFromLocalDB( long serverId )
    {
        Query query = chatCardDao.queryBuilder( ).where( ChatCardDao.Properties.ServerId.eq( serverId ) ).build( );
        if( query.list( ).size( ) > 0 )
        {
            ChatCard chatCard = ( ChatCard )query.list( ).get( 0 );
            return chatCard;
        }
        return null;
    }

    public void deleteChatCardFromLocalDB( ChatCard chatCard )
    {
        chatCardDao.delete(chatCard);
    }

    public List<ChatCard> getChatCardListFromLocalDB( )
    {
        Query query = chatCardDao.queryBuilder().build( );
        return query.list( );
    }

    public MessageDao getMessageDao( )
    {
        return messageDao;
    }

    public void setMessageDao( MessageDao messageDao )
    {
        this.messageDao = messageDao;
    }

    public long insertMessageToLocalDB( Message message )
    {
        messageDao.insert(message);
        return message.getId( );
    }

    public void updateMessageInLocalDB( Message message )
    {
        messageDao.update(message);
    }

    // Obtiene la lista de todos los mensajes no le�dos en todos los chatCards
    public List<Message> getAllUnreadMessages( )
    {
        Query query = messageDao.queryBuilder( ).where(MessageDao.Properties.Read.eq(false)).build();
        return query.list();
    }

    // Obtiene la lista de todos los mensajes no leidos en un chatcard
    public List<Message> getAllUnreadMessagesInChatCard( long id )
    {
        Query query = messageDao.queryBuilder( ).where( MessageDao.Properties.ChatCardId.eq( id ), MessageDao.Properties.Read.eq( false ) ).build();
        return query.list( );
    }

    // Obtiene el número de mensajes no leídos en el chat
    public int numberOfUnreadMessages(){
        Query query = messageDao.queryBuilder( ).where(MessageDao.Properties.Read.eq( false ) ).build();
        return query.list( ).size();
    }

    // Deja como leidos todos los mensajes no leidos de un chatcard
    public void setAllMessagesAsRead( long chatCardId )
    {
        List<Message> messages = getAllUnreadMessagesInChatCard(chatCardId);
        for( Message message : messages )
        {
            message.setRead( true );
            messageDao.update( message );
        }
    }

    // Deja como le�do un evento
    public void setEventAsRead( Event event )
    {
        event.setRead( true );
        eventDao.update(event);
    }

    // Obtiene la lista de todos los eventos no leidos
    public List<Event> getAllUnreadEvents( )
    {
        Query query = eventDao.queryBuilder( ).where( EventDao.Properties.Read.eq( false ) ).build();
        return query.list( );
    }

    public ChatUserDao getChatUser( )
    {
        return chatUserDao;
    }

    public void setChatUser( ChatUserDao chatUserDao )
    {
        this.chatUserDao = chatUserDao;
    }

    public long insertChatUserToLocalDB( ChatUser chatUser )
    {
        chatUserDao.insert( chatUser );
        return chatUser.getId( );
    }

    public ChatUser getChatUserFromLocalDB( String userId )
    {
        Query query = chatUserDao.queryBuilder( ).where( ChatUserDao.Properties.UserId.eq( userId ) ).build( );
        if( query.list( ).size( ) > 0 )
        {
            ChatUser chatUser = ( ChatUser )query.list( ).get( 0 );
            return chatUser;
        }
        return null;
    }


    public String getReplacementDriver() {
        return replacementDriver;
    }

    public void setReplacementDriver(String replacementDriver) {
        this.replacementDriver = replacementDriver;
    }

    public String getReplacementMonitor() {
        return replacementMonitor;
    }

    public void setReplacementMonitor(String replacementMonitor) {
        this.replacementMonitor = replacementMonitor;
    }

    public String getReplacementVehiclePlate() {
        return replacementVehiclePlate;
    }

    public void setReplacementVehiclePlate(String replacementVehiclePlate) {
        this.replacementVehiclePlate = replacementVehiclePlate;
    }

    public boolean replacementEnabled(){
        return this.replacementDriver!=null && this.replacementMonitor!=null && this.replacementVehiclePlate!=null;
    }

    public void vehicleCloseToNextStop(Location currentLocation )
    {

        // Revisa �nicamente si la ruta ha iniciado oficialmente
        if( routeStarted )
        {
            if( !routeFinished )
            {
                if( !closeToNextStop )
                {
                    if( hasLeftStop )
                    {
                        if( currentLocation.distanceTo( nextStop.getLocation( ) ) < Stop.APPROACH_DISTANCE_TO_STOP )
                        {
                            // En caso de que el paradero sea especial, se asume
                            // que
                            // el bus ya lleg� al paradero!

                            boolean noNextStop = checkCorrectNextStopWithoutUpdating(nextStop.getOrder());
                            boolean isPickUp = !OnTrackManager.getInstance().isDelivery();
                            boolean mustForceArrival = (noNextStop && isPickUp && !nextStop.didStopHere( ));


                            if( mustForceArrival ||(nextStop.isSpecial( ) && !nextStop.didStopHere( )))
                            {
                                vehicleJustStoppedAtStop( nextStop );
                            }
                            else
                            {

                                // Evento de llegada al paradero
                                Event event = new Event( null, Timestamp.completeTimestamp( ), Event.VEHICLE_APPROACHING_STOP_CODE, GeneralMessages.CLOSE_TO_NEXT_STOP, 0, 1, "" + nextStop.getTrackedStopId( ), OnTrackManager.getInstance( ).getUser( )
                                        .getRoute( ).getId( ), false );

                                // Guardar evento en db local
                                OnTrackManager.getInstance( ).getEvents( ).add( event );
//                                eventDao.insert( event );

                                // Notificar a los listener de eventos que hay
                                // una
                                // nueva
                                // notificaci�n
//                                EventManager.getInstance( ).notifyEventGenerated( event.getId( ) );

                                // EventManager.getInstance().getEvents().add(event);
                                // EventManager.getInstance().getStackedEvents().add(event);

                                try
                                {
                                    EventManager.getInstance( ).executeSendEventTask( OnTrackManagerAsyncTasks.URL_BASE_ADD_EVENTS, ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance( ).getToken( ), ServiceKeys.EVENTS_KEY,
                                            event.getEventJSONString( Event.FK_TRACKED_STOP_KEY ) );
                                }
                                catch( JSONException e )
                                {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace( );
                                }
                                listener.onVehicleApproachingToNextStop( this.isDelivery( ), this.getNextStop( ), this.getUser( ).getRoute( ).getStops( ).size( ) );
                                closeToNextStop = true;
                            }
                        }
                    }
                }
            }
        }
    }

    public void vehicleVeryCloseToNextStop( Location currentLocation )
    {

        // Revisa �nicamente si la ruta ha iniciado oficialmente
        if( routeStarted )
        {
            if( !routeFinished )
            {
                if( closeToNextStop )
                {
                    if( hasLeftStop )
                    {
                        if( currentLocation.distanceTo( nextStop.getLocation( ) ) < Stop.CLOSE_DISTANCE_TO_STOP )
                        {

                            listener.onVehicleVeryCloseToNextStop( nextStop );

                        }
                    }
                }
            }
        }
    }

    // Verifica si el veh�culo est� actualmente en el currentStop, y el
    // current
    // stop no es el primero
    public boolean vehicleInCurrentStop( )
    {
        if( currentStop != null && currentLocation != null )
        {
            if( currentStop.getOrder( ) != 0 )
            {
                if( currentStop.getLocation( ).distanceTo( currentLocation ) < Stop.MAXIMUM_DISTANCE_TO_STOP && currentStop.didStopHere())
                    return true;
            }
        }
        return false;
    }

    public void vehicleJustLeftCurrentStop( Location currentLocation )
    {
        // Revisar �nicamente si la ruta ha iniciado oficialmente
        if( routeStarted )
        {
            if( !routeFinished )
            {
                if( !hasLeftStop )
                {
                    if( !currentStop.isSpecial( ) )
                    {
                        if( currentLocation.distanceTo( referenceLocation ) > Stop.MAXIMUM_DISTANCE_TO_STOP )
                        {
                            actionLeaveStop();
                        }
                    }
                }
            }
        }
    }

    public void actionLeaveStop( )
    {
        if( this.nextStopSkipCounter >= MAXIMUM_SKIPPED_NEXT_STOP )
        {
            listener.onVehicleMightCancelSkippedStop( this.nextStop );
        }

        hasLeftStop = true;
        setReferenceLocation( nextStop.getLocation( ) );

        // Evento de salida del paradero
        Event event = new Event( null, Timestamp.completeTimestamp( ), Event.VEHICLE_LEFT_STOP_CODE, GeneralMessages.HAS_LEFT_STOP, 0, 1, "" + currentStop.getTrackedStopId( ), OnTrackManager.getInstance( ).getUser( ).getRoute( ).getId( ), false );
        // EventManager.getInstance().getEvents().add(event);
        // EventManager.getInstance().getStackedEvents().add(event);

        // Guardar evento en db local
        OnTrackManager.getInstance( ).getEvents( ).add( event );
//        eventDao.insert( event );

        // Notificar a los listener de eventos que hay una
        // nueva
        // notificaci�n
//        EventManager.getInstance( ).notifyEventGenerated( event.getId( ) );

        try
        {
            EventManager.getInstance( ).executeSendEventTask( OnTrackManagerAsyncTasks.URL_BASE_ADD_EVENTS, ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance( ).getToken( ), ServiceKeys.EVENTS_KEY,
                    event.getEventJSONString( Event.FK_TRACKED_STOP_KEY ) );
        }
        catch( JSONException e )
        {
            // TODO Auto-generated catch block
            e.printStackTrace( );
        }
        listener.onVehicleJustLeftCurrentStop( nextStop );

        // Revisa el estado de los TrackedStopTrackables y
        // env�a
        // el
        // reporte
        currentStop.changeUnknownToPickedOrDeliveredTrackables( );

        currentStop.setDepartureTime(Timestamp.completeTimestamp());

        try
        {
            // Log.d("http",OnTrackManager.getInstance().getStopDataJSONString());
            OnTrackManagerAsyncTasks.executeSendTrackedStopInfoTask( false, OnTrackManagerAsyncTasks.URL_BASE_TRACKED_STOP_INFO, ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance( ).getToken( ), ServiceKeys.TRACKED_STOPS_KEY, OnTrackManager
                    .getInstance( ).getStopDataJSONString( ) );
        }
        catch( JSONException e )
        {
            listener.onAsyncTaskError( GeneralMessages.JSON_ERROR );
        }
        try
        {
            OnTrackManagerAsyncTasks.executeSendTSTStatesTask( OnTrackManagerAsyncTasks.URL_BASE_PICKED_TRACKABLES, ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance( ).getToken( ), ServiceKeys.TRACKEDSTOPTRACKABLES_KEY,
                    currentStop.getTrackedStopTrackablesJSONString( ) );
        }
        catch( JSONException e )
        {
            // TODO Auto-generated catch block
            e.printStackTrace( );
        }
    }

    public void vehicleDeviatedFromRoute( )
    {
        if( !this.deviatedFromRoute )
        {
            // OnTrackManagerAsyncTasks.decodePortionOfRouteForDeviation(
            // this.currentLocation, this.nextStop, false);
            // Evento de desviaci�n de la ruta
            Event event = new Event( null, Timestamp.completeTimestamp( ), Event.DEVIATED_ROUTE_CODE, GeneralMessages.DEVIATED_FROM_ROUTE, 0, 1, "" + OnTrackManager.getInstance( ).getUser( ).getFootprint( ), OnTrackManager.getInstance( ).getUser( )
                    .getRoute( ).getId( ), false );
            // EventManager.getInstance().getEvents().add(event);
            // EventManager.getInstance().getStackedEvents().add(event);

            // Guardar evento en db local
            OnTrackManager.getInstance( ).getEvents( ).add( event );
            OnTrackManager.getInstance( ).getEventDao( ).insert( event );

            // Notificar a los listener de eventos que hay una nueva
            // notificaci�n
            EventManager.getInstance( ).notifyEventGenerated( event.getId( ) );

            try
            {
                EventManager.getInstance( ).executeSendEventTask( OnTrackManagerAsyncTasks.URL_BASE_ADD_EVENTS, ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance( ).getToken( ), ServiceKeys.EVENTS_KEY,
                        event.getEventJSONString( Event.FK_FOOTPRINT_KEY ) );
            }
            catch( JSONException e )
            {
                // TODO Auto-generated catch block
                e.printStackTrace( );
            }

            this.deviatedFromRoute = true;
        }
    }

    public void vehicleExtremelyDeviatedFromRoute( )
    {

        // Estan son las condiciones para considerar el rec�lculo de ruta
        boolean enabledToRecomputePortionOfRoute = ( this.currentStop.getOrder( ) == 0 && this.currentStop.getLocation( ).distanceTo( this.currentLocation ) > 0.10 * this.currentStop.getDistance( ) );

        if( enabledToRecomputePortionOfRoute )
        {

            if( !this.extremelyDeviatedFromRoute )
            {
                listener.onPreComputingPortionOfRoute( );
                OnTrackManagerAsyncTasks.decodePortionOfRouteForDeviation( this.currentLocation, nextStop, false );
                this.extremelyDeviatedFromRoute = true;
            }
        }
    }

    public void vehicleOnTrack( )
    {
        this.deviatedFromRoute = false;
        this.deviationCounter = 0;
    }

    public void vehicleStopped(Location currentLocation) {
        stopped = true;
        vehicleStoppedChronometer.start();

        // Verificar si el veh�culo se detuvo en el siguiente paradero de la
        // lista
        if (hasLeftStop) {
            if (nextStop.getLocation().distanceTo(currentLocation) < Stop.MAXIMUM_DISTANCE_TO_STOP
                    && !nextStop.isDidStopHere()) {
                vehicleJustStoppedAtStop(nextStop);
            }

            // Verificar si est� detenido en un paradero extra�o

            ArrayList<Stop> remainingStops = this.user.getRoute()
                    .getStopsWhereVehicleHasntStoppedExceptingNext(
                            this.nextStop);
            for (Stop stop : remainingStops) {
                if (stop.getLocation().distanceTo(currentLocation) < Stop.VERY_CLOSE_DISTANCE_TO_STOP) {
                    listener.onVehicleMightStopAtStrangeStop(stop);
                    break;
                }
            }
        }
        // }
    }
    public void vehicleStartedMoving( )
    {
        if( currentLocation.hasSpeed( ) && previousSpeed > 10 )
        {
            listener.onVehicleStartedMoving( );
        }
        stopped = false;
        vehicleStoppedChronometer.stop( );
        totalStoppedSeconds += vehicleStoppedChronometer.getSeconds( );

    }

    public void initializeRoute( )
    {
        totalRouteTimeChronometer.start( );
        // Deber�a siempre asumirse que hay al menos dos paraderos: un
        // origen y un destino
        if( user.getRoute( ).getStops( ).size( ) >= 2 )
        {
            currentStop = user.getRoute( ).getLastStopWhereVehicleStopped( );
            int currentStopOrder;
            if( currentStop != null )
            {
                currentStopOrder = currentStop.getOrder( );
            }
            else
            {
                currentStopOrder = 0;
                currentStop = user.getRoute( ).getStops( ).get( 0 );
            }
            this.timeFlag = currentStopOrder;
            this.stopFlag = currentStopOrder;
            this.googleDirectionsRequests = currentStopOrder;
            checkCorrectNextStop( currentStopOrder );
            this.referenceLocation = nextStop.getLocation( );

            // A la lista de paraderos rastreados, se a�ade el paradero
            // origen
            trackedStops.add( currentStop );
            listener.onRouteStarted( currentStopOrder );
        }
        else
        {
            listener.onAsyncTaskError( GeneralMessages.BAD_CONSTRUCTED_ROUTE_ERROR );
        }
    }

    public void initializeRoute2( )
    {
        totalRouteTimeChronometer.start( );
    }

    /**
     * M�todo que puede ser llamado �nicamente cuando toda la informaci�n de la ruta ha sido cargada
     */
    public void startRoute( )
    {
        if( !routeStarted )
        {
            routeStarted = true;
        }
    }

    // Se ejecuta cuando el bus llega al paradero
    public void vehicleJustStoppedAtStop( Stop stop )
    {
        this.nextStopSkipCounter = 0;

        closeToNextStop = false;
        hasLeftStop = false;

        // Los siguientes eventos podr�an ser enviados cuando un veh�culo
        // llega a un paradero:
        // 1. Que el veh�culo lleg� al paradero (siempre se manda)
        // 2. Que la ruta ha terminado
        // 3. Que la ruta est� adelantada
        // 4. Que la ruta est� atrasada

        // El siguiente arreglo guarda las cadenas JSON de los eventos que
        // ser�n enviados
        ArrayList<String> eventStrings = new ArrayList<String>( );

        currentStop = stop;
        currentStop.setDidStopHere( true );

        int order = currentStop.getOrder( );

        this.stopFlag = order;

        currentStop.setRealTime( Timestamp.completeTimestamp( ) );

        try
        {
            // Log.d("http",OnTrackManager.getInstance().getStopDataJSONString());
            OnTrackManagerAsyncTasks.executeSendTrackedStopInfoTask( false, OnTrackManagerAsyncTasks.URL_BASE_TRACKED_STOP_INFO, ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance( ).getToken( ), ServiceKeys.TRACKED_STOPS_KEY, OnTrackManager
                    .getInstance( ).getStopDataJSONString( ) );
        }
        catch( JSONException e )
        {
            listener.onAsyncTaskError( GeneralMessages.JSON_ERROR );
        }

        // Evento de llegada al paradero
        String message = stop.isSpecial( ) ? GeneralMessages.HAS_ARRIVED_TO_SPECIAL_STOP : GeneralMessages.HAS_ARRIVED_TO_STOP;

        Event event = new Event( null, Timestamp.completeTimestamp( ), Event.VEHICLE_ARRIVED_STOP_CODE, message, 0, 1, "" + currentStop.getTrackedStopId( ), OnTrackManager.getInstance( ).getUser( ).getRoute( ).getId( ), false );
        // EventManager.getInstance().getEvents().add(event);
        // EventManager.getInstance().getStackedEvents().add(event);
        // Guardar evento en dblocal
        OnTrackManager.getInstance( ).getEvents( ).add( event );
//        eventDao.insert( event );

        // Notificar a los listener de eventos que hay una nueva
        // notificaci�n
//        EventManager.getInstance( ).notifyEventGenerated( event.getId( ) );

        // A�adir la cadena JSON del evento de llegada al paradero al
        // paquete que se enviar� al final
        try
        {
            eventStrings.add( event.getEventJSONStringWithoutSquaredBrackets( Event.FK_TRACKED_STOP_KEY ) );
        }
        catch( JSONException e1 )
        {
            // TODO Auto-generated catch block
            e1.printStackTrace( );
        }

        if( order < user.getRoute( ).getStops( ).size( ) - 1 )
        {
            // Actualizar la siguiente parada solo si existe
            boolean noNextStop = checkCorrectNextStop( order );

            if( noNextStop )
            {
                this.routeFinished = true;
                listener.onRouteFinished( );

                // Evento de finalizaci�n de ruta
                Event finalEvent = new Event( null, Timestamp.completeTimestamp( ), Event.END_ROUTE_EVENT_CODE, GeneralMessages.ROUTE_HAS_FINISHED, 0, 1, "" + this.user.getFootprint( ), OnTrackManager.getInstance( ).getUser( ).getRoute( ).getId( ),
                        false );
                // Guardar evento en dblocal
//                eventDao.insert( finalEvent );

                // A�adir el evento de fin de ruta a la lista de eventos que
                // ser�n enviados al final
                try
                {
                    eventStrings.add( finalEvent.getEventJSONStringWithoutSquaredBrackets( Event.FK_FOOTPRINT_KEY ) );
                }
                catch( JSONException e1 )
                {
                    // TODO Auto-generated catch block
                    e1.printStackTrace( );
                }

            }

            nextStop.addDelayToEstimatedTime( getTotalDelayInSeconds( ) );
            listener.onEstimatedTimesChanged( );

            // Verificar si se debe enviar el evento de atraso o adelanto de
            // la ruta
            if( timeDelayed >= MAXIMUM_DELAY_ALLOWED_IN_MINUTES )
            {
                if(!this.delaySent) {
                    // Evento de atraso de ruta
                    Event delayEvent = new Event(null, Timestamp.completeTimestamp(), Event.ROUTE_DELAYED_EVENT_CODE, GeneralMessages.ROUTE_IS_DELAYED, 0, 1, "" + this.user.getFootprint(),
                            OnTrackManager.getInstance().getUser().getRoute().getId(), false);
                    // Guardar evento en dblocal
//                eventDao.insert( delayEvent );

                    // A�adir el evento de retraso de ruta a la lista de eventos
                    // que ser�n enviados al final
                    try {
                        eventStrings.add(delayEvent.getEventJSONStringWithoutSquaredBrackets(Event.FK_FOOTPRINT_KEY));
                        this.delaySent = true;
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                }
            }
            else if( timeDelayed <= -MAXIMUM_ADVANCE_ALLOWED_IN_MINUTES )
            {
                if(!this.aheadSent) {
                    // Evento de adelanto de ruta
                    Event advanceEvent = new Event(null, Timestamp.completeTimestamp(), Event.ROUTE_ADVANCED_EVENT_CODE, GeneralMessages.ROUTE_IS_ADVANCED, 0, 1, "" + this.user.getFootprint(), OnTrackManager.getInstance().getUser().getRoute()
                            .getId(), false);
                    // Guardar evento en dblocal
//                eventDao.insert( advanceEvent );

                    try {
                        eventStrings.add(advanceEvent.getEventJSONStringWithoutSquaredBrackets(Event.FK_FOOTPRINT_KEY));
                        this.aheadSent = true;
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                }
            }

        }
        else
        {
            this.routeFinished = true;
            listener.onRouteFinished();

            // Evento de finalizaci�n de ruta
            Event finalEvent = new Event( null, Timestamp.completeTimestamp( ), Event.END_ROUTE_EVENT_CODE, GeneralMessages.ROUTE_HAS_FINISHED, 0, 1, "" + this.user.getFootprint( ), OnTrackManager.getInstance( ).getUser( ).getRoute( ).getId( ),
                    false );
            // Guardar evento en dblocal
//            eventDao.insert( finalEvent );
            // A�adir el evento de fin de ruta a la lista de eventos que
            // ser�n enviados al final
            try
            {
                eventStrings.add( finalEvent.getEventJSONStringWithoutSquaredBrackets( Event.FK_FOOTPRINT_KEY ) );
            }
            catch( JSONException e1 )
            {
                // TODO Auto-generated catch block
                e1.printStackTrace( );
            }
        }

        // Enviar todos los eventos pertinentes
        EventManager.getInstance( ).executeSendEventTask( OnTrackManagerAsyncTasks.URL_BASE_ADD_EVENTS, ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance( ).getToken( ), ServiceKeys.EVENTS_KEY, this.getPackageOfEventsJSONString( eventStrings ) );

        trackedStops.add( stop );

        // Avisar al listener que el veh�culo se detuvo en el paradero
        listener.onVehicleJustStoppedAtStop( this.isDelivery( ), this.currentStop, this.getUser( ).getRoute( ).getStops( ).size( ), this.getEstimatedTimeFromCurrentStop( ) );
    }

    // Se ejecuta cuando el bus llega al paradero
    public void vehicleJustStoppedAtStrangeStop( Stop stop )
    {
        this.nextStopSkipCounter++;

        setReferenceLocation( this.currentLocation );
        closeToNextStop = false;
        hasLeftStop = false;

        // Los siguientes eventos podr�an ser enviados cuando un veh�culo
        // llega a un paradero:
        // 1. Que el veh�culo lleg� al paradero (siempre se manda)
        // 2. Que la ruta ha terminado
        // 3. Que la ruta est� adelantada
        // 4. Que la ruta est� atrasada

        // El siguiente arreglo guarda las cadenas JSON de los eventos que
        // ser�n enviados
        ArrayList<String> eventStrings = new ArrayList<String>( );

        currentStop = stop;
        currentStop.setDidStopHere( true );

        int order = currentStop.getOrder( );

        this.stopFlag = order;

        currentStop.setRealTime( Timestamp.completeTimestamp( ) );

        try
        {
            // Log.d("http",OnTrackManager.getInstance().getStopDataJSONString());
            OnTrackManagerAsyncTasks.executeSendTrackedStopInfoTask( false, OnTrackManagerAsyncTasks.URL_BASE_TRACKED_STOP_INFO, ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance( ).getToken( ), ServiceKeys.TRACKED_STOPS_KEY, OnTrackManager
                    .getInstance( ).getStopDataJSONString( ) );
        }
        catch( JSONException e )
        {
            listener.onAsyncTaskError( GeneralMessages.JSON_ERROR );
        }

        // Evento de aproximaci�n al paradero
        String message = stop.isSpecial( ) ? GeneralMessages.HAS_ARRIVED_TO_SPECIAL_STOP : GeneralMessages.HAS_ARRIVED_TO_STOP;

        Event event = new Event( null, Timestamp.completeTimestamp( ), Event.VEHICLE_ARRIVED_STOP_CODE, message, 0, 1, "" + currentStop.getTrackedStopId( ), OnTrackManager.getInstance( ).getUser( ).getRoute( ).getId( ), false );
        // EventManager.getInstance().getEvents().add(event);
        // EventManager.getInstance().getStackedEvents().add(event);
        // Guardar evento en dblocal
        OnTrackManager.getInstance( ).getEvents( ).add( event );
//        eventDao.insert( event );

        // Notificar a los listener de eventos que hay una nueva
        // notificaci�n
//        EventManager.getInstance( ).notifyEventGenerated( event.getId( ) );

        // A�adir la cadena JSON del evento de llegada al paradero al
        // paquete que se enviar� al final
        try
        {
            eventStrings.add( event.getEventJSONStringWithoutSquaredBrackets( Event.FK_TRACKED_STOP_KEY ) );
        }
        catch( JSONException e1 )
        {
            // TODO Auto-generated catch block
            e1.printStackTrace( );
        }

        if( order < user.getRoute( ).getStops( ).size( ) - 1 )
        {

            listener.onEstimatedTimesChanged( );

        }
        else
        {
            this.routeFinished = true;
            listener.onRouteFinished( );

            // Evento de finalizaci�n de ruta
            Event finalEvent = new Event( null, Timestamp.completeTimestamp( ), Event.END_ROUTE_EVENT_CODE, GeneralMessages.ROUTE_HAS_FINISHED, 0, 1, "" + this.user.getFootprint( ), OnTrackManager.getInstance( ).getUser( ).getRoute( ).getId( ),
                    false );
            // Guardar evento en dblocal
//            eventDao.insert( finalEvent );
            // A�adir el evento de fin de ruta a la lista de eventos que
            // ser�n enviados al final
            try
            {
                eventStrings.add( finalEvent.getEventJSONStringWithoutSquaredBrackets( Event.FK_FOOTPRINT_KEY ) );
            }
            catch( JSONException e1 )
            {
                // TODO Auto-generated catch block
                e1.printStackTrace( );
            }
        }

        // Enviar todos los eventos pertinentes
        EventManager.getInstance( ).executeSendEventTask( OnTrackManagerAsyncTasks.URL_BASE_ADD_EVENTS, ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance( ).getToken( ), ServiceKeys.EVENTS_KEY, this.getPackageOfEventsJSONString( eventStrings ) );

        trackedStops.add( stop );

        // Avisar al listener que el veh�culo se detuvo en el paradero
        listener.onVehicleJustStoppedAtStop( this.isDelivery( ), this.currentStop, this.getUser( ).getRoute( ).getStops( ).size( ), this.getEstimatedTimeFromCurrentStop( ) );
    }

    private void calculateETASFromStopUsingGoogle( Stop currentStop )
    {
        boolean calculate = false;
        boolean noWaypointsYet = true;

        ArrayList<String> query = new ArrayList<String>( );
        ArrayList<Stop> wayPointsArray = new ArrayList<Stop>( );

        query.add( "http://maps.googleapis.com/maps/api/directions/json?" );

        if( currentStop != null )
        {
            query.add( "origin=" + currentStop.getLocation( ).getLatitude( ) + "," + currentStop.getLocation( ).getLongitude( ) );
        }
        else
        {
            currentStop = user.getRoute( ).getStops( ).get( 0 );

            query.add( "origin=" + currentLocation.getLatitude( ) + "," + currentLocation.getLongitude( ) );
        }

        String waypoints = "";

        int size = user.getRoute( ).getStops( ).size( );
        Stop destination = user.getRoute( ).getStops( ).get( size != 0 ? size - 1 : 0 );

        for( Stop stop : user.getRoute( ).getStops( ) )
        {
            if( stop == currentStop )
                calculate = true;

            if( calculate )
            {
                if( noWaypointsYet )
                {
                    waypoints = "&waypoints=" + stop.getLocation( ).getLatitude( ) + "," + stop.getLocation( ).getLongitude( );
                    noWaypointsYet = false;
                }
                else if( stop != destination )
                {
                    try
                    {
                        waypoints += URLEncoder.encode( "|", "UTF-8" ) + stop.getLocation( ).getLatitude( ) + "," + stop.getLocation( ).getLongitude( );
                    }
                    catch( UnsupportedEncodingException e )
                    {
                        e.printStackTrace( );
                    }
                }
                wayPointsArray.add( stop );
            }
        }
        query.add( "&destination=" + destination.getLocation( ).getLatitude( ) + "," + destination.getLocation( ).getLongitude( ) );

        query.add( waypoints );

        query.add( "&departure_time=" + java.lang.System.currentTimeMillis( ) / 1000 );

        query.add( "&mode=walking" ); // PRUEBAS

        query.add( "&sensor=true" );

        // CalculateETASUsingGoogleTask task = new
        // CalculateETASUsingGoogleTask(wayPointsArray.toArray(new
        // Stop[wayPointsArray.size()]));
        // task.execute(query.toArray(new String[query.size()]));
    }

    public void vehicleLocationChanged( Location currentLocation )
    {
        // Los siguientes eventos podr�an ocurrir cuando se detecta un cambio
        // de
        // localizaci�n del veh�culo
        // 1. El veh�culo est� a x metros del paradero
        // 2. El veh�culo est� x minutos del paradero
        // 3. El veh�culo ha excedido la velocidad permitida
        // 4. La precisi�n del GPS es baja


        final ArrayList<String> eventStrings = new ArrayList<String>( );

        this.currentLocation = currentLocation;

        this.newPointsGenerated++;

        if( !isSimulating )
        {
            if( isRecordingSimulation )
            {
                // Guardar la localizaci�n en la base de datos
                SimLocation simLocation = new SimLocation( null, currentLocation, simulation.getId( ) );
                simLocationDao.insert( simLocation );
            }
        }
        if( routeStarted )
        {

            // Verificar si la velocidad est� siendo excedida
            checkSpeedExceeded( eventStrings );

            // Verificar si la precisi�n del gps es baja
            checkGpsAccuraccyLow( eventStrings );

            if( previousSpeed != -1 && previousLocation != null )
            {
                if( currentLocation.getSpeed( ) == 0.0 && previousSpeed > 0 )
                {
                    // El veh�culo se detuvo si su velocidad actual es 0 y
                    // su
                    // velocidad previa no era 0
                    // En caso de que esto falle, se compara la localizaci�n
                    // actual
                    // y previa
                    // Si son muy cercanas, se considera que hubo una
                    // detenci�n

                    vehicleStopped( currentLocation );

                }
                else if( currentLocation.hasSpeed( ) && previousSpeed == 0 )
                {
                    // El veh�culo comenz� a andar
                    vehicleStartedMoving( );
                }

            }

            TrackedPoint trackedPoint = registerNewTrackedPoint( );

            previousLocation = currentLocation;
            previousSpeed = currentLocation.getSpeed( );

            // Verificar si ya estoy cerca al pr�ximo paradero
            vehicleCloseToNextStop( currentLocation );

            // Verificar si ya estoy muy cerca al pr�ximo paradero
            vehicleVeryCloseToNextStop( currentLocation );

            // Verificar si ya estoy lejos del paradero actual
            vehicleJustLeftCurrentStop( currentLocation );

            // Analizar si hay un retraso antes de llegar al primer paradero
            analyzePotentialDelayBeforeFirstStop( eventStrings );

            // Analizar si hay un adelanto antes de llegar al primer paradero
            analyzePotentialAheadInTimeBeforeFirstStop( eventStrings );

            // Verificar si se deben generar eventos de tiempo y distancia
            checkPotentialDistanceAndTimeEvents( trackedPoint, eventStrings );

            // Enviar los eventos generados en todo el proceso
            if( eventStrings.size( ) != 0 )
            {
                EventManager.getInstance( ).executeSendEventTask( OnTrackManagerAsyncTasks.URL_BASE_ADD_EVENTS, ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance( ).getToken( ), ServiceKeys.EVENTS_KEY,
                        this.getPackageOfEventsJSONString( eventStrings ) );
            }
        }

    }

    public int getTimeSinceVehicleStopped( )
    {
        return ( int )vehicleStoppedChronometer.getSeconds( );
    }

    public int getLastVehicleTotalStopTime( )
    {
        return ( int )vehicleStoppedChronometer.getSeconds( );
    }

    public int getTimeSinceLastStop( )
    {
        return ( int )timeSinceLastStopChronometer.getSeconds( );
    }

    public int getTimeSinceRouteStarted( )
    {
        return ( int )totalRouteTimeChronometer.getSeconds( );
    }

    public boolean routeDidStart( )
    {
        return routeStarted;
    }

    public int getTimeDelayed( )
    {
        return timeDelayed;
    }

    public Stop searchStopWithLocation( Location location )
    {
        for( Stop stop : user.getRoute( ).getStops( ) )
        {
            if( stop.getLocation( ).getLatitude( ) == location.getLatitude( ) && stop.getLocation( ).getLongitude( ) == location.getLongitude( ) )
            {
                return stop;
            }
        }
        return null;
    }

    public int getIndexOfTheStopAtLocation( Location location )
    {
        int i = 0;
        for( Stop stop : user.getRoute( ).getStops( ) )
        {
            // El m�todo es llamado �nicamente con localizaciones de
            // paraderos;
            // en caso de que no ocurra, se retorna -1
            if( stop.getLocation( ).getLatitude( ) == location.getLatitude( ) && stop.getLocation( ).getLongitude( ) == location.getLongitude( ) )
            {
                return i;
            }
            i++;
        }
        return -1;
    }

    // Asignar un listener a esta clase
    public void setOnTrackSystemListener( OnTrackManagerListener s )
    {
        this.listener = s;
    }

//    // Obtiene una imagen de muestra de un trackable real
//    public Bitmap getSampleImageFromTrackable( )
//    {
//        return this.user.getRoute( ).getStops( ).get( 0 ).getTrackables( ).get( 0 ).getImage( );
//    }

    public String getStopDataJSONString( ) throws JSONException
    {
        JSONObject jsonObject = new JSONObject( );
        float estimatedDistance = 0.0f;

        Calendar c = Calendar.getInstance( );
        int seconds, minutes, hours, year, month, day;
        Date date;

        for( Stop stop : this.user.getRoute( ).getStops( ) )
        {
            estimatedDistance += stop.getDistance( );
            JSONObject jsonStop = new JSONObject( );

            jsonStop.put( ServiceKeys.ID_KEY, stop.getTrackedStopId() );

            // Se le a�ade a la fecha actual el n�mero de segundos que tarda
            // un
            // veh�culo en llegar desde el origen hasta el paradero,
            // �nicamente
            // si el real time es nulo
            if( stop.getRealTime( ) == null )
            {
                // c.add(Calendar.SECOND, stop.getEstimatedTime());
                //
                seconds = c.get( Calendar.SECOND );
                minutes = c.get( Calendar.MINUTE );
                hours = c.get( Calendar.HOUR_OF_DAY );
                date = c.getTime( );
                year = date.getYear( ) + 1900;
                month = date.getMonth( ) + 1;
                day = date.getDate( );
                //
                // String timestamp = "" + year + "-"
                // + (month < 10 ? "0" + month : month) + "-"
                // + (day < 10 ? "0" + day : day) + " "
                // + (hours < 10 ? "0" + hours : hours) + ":"
                // + (minutes < 10 ? "0" + minutes : minutes) + ":"
                // + (seconds < 10 ? "0" + seconds : seconds);

                String timestamp = "" + year + "-" + ( month < 10 ? "0" + month : month ) + "-" + ( day < 10 ? "0" + day : day );

                timestamp += " " + this.user.getRoute( ).getEstimatedTimeToStop( stop.getOrder( ) );

                // Para volver a tener la fecha actual, se le restan los
                // segundos
                // previamente a�adidos
                // c.add(Calendar.SECOND,-stop.getEstimatedTime());

                jsonStop.put( ServiceKeys.ESTIMATED_TIME_KEY, timestamp );
            }
            else
            {
                jsonStop.put( ServiceKeys.REAL_TIME_KEY, stop.getRealTime( ) );
            }
            if( !routeReloaded )
            {
                jsonStop.put( ServiceKeys.ESTIMATED_DISTANCE_KEY, estimatedDistance );
            }

            if(stop.getDepartureTime()!=null){
                jsonStop.put( ServiceKeys.DEPARTURE_TIME_KEY, stop.getDepartureTime() );
            }

            jsonStop.put( ServiceKeys.STATE_KEY, stop.isCancelled( ) ? 1 : 0 );

//            if( stop.getSteps( ).size( ) > 0 )
//            {
//                int counter = 0;
//                for( Point point : stop.getSteps( ) )
//                {
//
//                    JSONObject jsonPoint = new JSONObject( );
//                    jsonPoint.put( ServiceKeys.LATITUDE_KEY, point.getLatitude( ) );
//                    jsonPoint.put( ServiceKeys.LONGITUDE_KEY, point.getLongitude( ) );
//                    jsonPoint.put( ServiceKeys.POSITION_KEY, counter );
//                    jsonStop.accumulate( ServiceKeys.TRACKED_STOP_STEPS_KEY, jsonPoint );
//                    counter++;
//
//                }
//
//            }
            jsonStop.put(ServiceKeys.TRACKED_STOP_STEPS_KEY, getStepDataJSONString(stop));

            // jsonStop.put("ESTIMATED_TIME",timestamp);
            jsonObject.accumulate( ServiceKeys.TRACKED_STOPS_KEY, jsonStop );
        }
        // ("Opci�n 1", jsonObject.toString());
//         Log.d("Opcion 3","{"+jsonObject.getString("TRACKED_STOPS")+"}");
        if( this.user.getRoute( ).getStops( ).size() == 1 ) {
            String cleanedString = jsonObject.getString(ServiceKeys.TRACKED_STOPS_KEY).replace("\""+"[]"+"\"", "[]");
            cleanedString = cleanedString.replace("\\","");
            cleanedString = cleanedString.replace("\""+"[", "[");
            cleanedString = cleanedString.replace("]"+"\"", "]");
            return "[" + cleanedString + "]";
        }else {
            String cleanedString = jsonObject.getString(ServiceKeys.TRACKED_STOPS_KEY).replace("\""+"[]"+"\"", "[]");
            cleanedString = cleanedString.replace("\\","");
            cleanedString = cleanedString.replace("\""+"[", "[");
            cleanedString = cleanedString.replace("]"+"\"", "]");
            return cleanedString;
        }
    }


    public String getStepDataJSONString(Stop stop) throws JSONException{
        JSONObject jsonSteps = new JSONObject( );
        int counter = 0;
        for( Point point : stop.getSteps( ) )
        {

            JSONObject jsonPoint = new JSONObject( );
            jsonPoint.put( ServiceKeys.LATITUDE_KEY, point.getLatitude() );
            jsonPoint.put( ServiceKeys.LONGITUDE_KEY, point.getLongitude( ) );
            jsonPoint.put( ServiceKeys.POSITION_KEY, counter );
            jsonSteps.accumulate( ServiceKeys.TRACKED_STOP_STEPS_KEY, jsonPoint );
            counter++;

        }
        if( stop.getSteps().size() == 0 ){
            return "[]";
        }else if ( stop.getSteps( ).size() == 1 ){
            return "[" + jsonSteps.getString( ServiceKeys.TRACKED_STOP_STEPS_KEY ) + "]";
        }else{
            return jsonSteps.getString( ServiceKeys.TRACKED_STOP_STEPS_KEY );
        }
    }


    public String getStackedPointsJSONString( ) throws JSONException
    {
        JSONObject jsonObject = new JSONObject( );

        // Se clona el arreglo de puntos pendientes para evitar problemas de
        // concurrencia
        ArrayList<TrackedPoint> clonedPoints = ( ArrayList<TrackedPoint> )this.stackedPoints.clone( );

        for( TrackedPoint stackedPoint : clonedPoints )
        {
            JSONObject jsonStackedPoint = new JSONObject( );
            jsonStackedPoint.put( ServiceKeys.FK_FOOTPRINT_KEY, "" + OnTrackManager.getInstance( ).getUser( ).getFootprint( ) );
            jsonStackedPoint.put( ServiceKeys.LONGITUDE_KEY, "" + stackedPoint.getLongitude( ) );
            jsonStackedPoint.put( ServiceKeys.LATITUDE_KEY, stackedPoint.getLatitude( ) );
            jsonStackedPoint.put( ServiceKeys.ALTITUDE_KEY, stackedPoint.getAltitude( ) );
            jsonStackedPoint.put( ServiceKeys.DATE_KEY, stackedPoint.getDate( ) );
            jsonStackedPoint.put( ServiceKeys.ACCURACY_KEY, stackedPoint.getAccuracy( ) );
            jsonStackedPoint.put( ServiceKeys.BEARING_KEY, stackedPoint.getBearing( ) );
            jsonStackedPoint.put( ServiceKeys.SPEED_KEY, stackedPoint.getSpeed( ) );
            jsonStackedPoint.put( ServiceKeys.DISTANCE_COVERED_KEY, stackedPoint.getTotalDistance() );

            // Aunque el TIME_FLAG es una propiedad de TRACKED_POINT en la base
            // de datos, prefiero que quede guardada en OnTrackSystem
            // Sujeto a revisi�n
            jsonStackedPoint.put( ServiceKeys.TIME_FLAG_KEY, this.getTimeFlag( ) );
            jsonStackedPoint.put( ServiceKeys.DISTANCE_FLAG_KEY, this.getDistanceFlag( ) );
            jsonStackedPoint.put( ServiceKeys.STOP_FLAG_KEY, this.getStopFlag( ) );
            jsonStackedPoint.put(ServiceKeys.TRACKABLE_FLAG_KEY, this.getTrackableFlag());
            jsonStackedPoint.put( ServiceKeys.TIME_DELAYED_KEY, this.getTimeDelayed( ) );

            // jsonStop.put("ESTIMATED_TIME",timestamp);
            jsonObject.accumulate( "STACKED_POINTS", jsonStackedPoint );
        }
        // Log.d("Opci�n 1", jsonObject.toString());
        // Log.d("Opci�n 2", jsonObject.getString("STACKED_POINTS"));
        // Log.d("Opcion 3","{"+jsonObject.getString("TRACKED_STOPS")+"}");
        if( clonedPoints.size() == 1 )
            return "[" + jsonObject.getString( ServiceKeys.STACKED_POINTS_KEY ) + "]";
        else
            return jsonObject.getString( ServiceKeys.STACKED_POINTS_KEY );

    }

    public void deleteStackedPoints( )
    {
        this.stackedPoints.clear( );
    }

    // Objeto JSON con las notificaciones que llegaron por Pusher
    public String getSavedNotificationsJSONString( ) throws JSONException
    {
        List<Notification> notifications = notificationDao.queryBuilder( ).build( ).list( );
        JSONObject jsonObject = new JSONObject( );

        for( Notification notification : notifications )
        {
            JSONObject jsonNotification = new JSONObject( );
            jsonNotification.put( ServiceKeys.ID_KEY, "" + notification.getIdOnServer( ) );
            jsonNotification.put( ServiceKeys.DATE_ACK_KEY, notification.getDateAck( ) );

            // jsonStop.put("ESTIMATED_TIME",timestamp);
            jsonObject.accumulate( ServiceKeys.NOTIFICATIONS_KEY, jsonNotification );
        }
        // Log.d("Opci�n 1", jsonObject.toString());
        // Log.d("Opci�n 2",
        // jsonObject.getString(ServiceKeys.NOTIFICATIONS_KEY));
        // Log.d("Opcion 3","{"+jsonObject.getString("TRACKED_STOPS")+"}");
        if( notifications.size() == 1 )
            return "[" + jsonObject.getString( ServiceKeys.NOTIFICATIONS_KEY ) + "]";
        else if( notifications.size( ) == 0 )
            return null;
        else
            return jsonObject.getString( ServiceKeys.NOTIFICATIONS_KEY );
    }

    /*************** GETTERS, SETTERS ***********************/

    public void setNextStop( Stop nextStop )
    {
        this.nextStop = nextStop;
    }

    public void setCurrentStop( Stop currentStop )
    {
        this.currentStop = currentStop;

    }

    public String getToken( )
    {
        return token;
    }

    public void setToken( String token )
    {
        this.token = token;
    }

    public int getUserDeviceId( )
    {
        return userDeviceId;
    }

    public void setUserDeviceId( int userDeviceId )
    {
        this.userDeviceId = userDeviceId;
    }

    public boolean isStopped( )
    {
        return stopped;
    }

    public void setStopped( boolean stopped )
    {
        this.stopped = stopped;
    }

    public boolean isRouteFinished( )
    {
        return routeFinished;
    }

    public void setRouteFinished( boolean routeFinished )
    {
        this.routeFinished = routeFinished;
    }

    public boolean isSpeedExceeded( )
    {
        return speedExceeded;
    }

    public boolean isRouteReloaded( )
    {
        return routeReloaded;
    }

    public void setRouteReloaded( boolean routeReloaded )
    {
        this.routeReloaded = routeReloaded;
    }

    public void setSpeedExceeded( boolean speedExceeded )
    {
        this.speedExceeded = speedExceeded;
    }

    public boolean isAlertTime( )
    {
        return alertTime;
    }

    public void setAlertTime( boolean alertTime )
    {
        this.alertTime = alertTime;
    }

    public ArrayList<Route> getRoutes( )
    {
        return routes;
    }

    public void setRoutes( ArrayList<Route> routes )
    {
        this.routes = routes;
    }

    public User getUser( )
    {
        return user;
    }

    public void setUser( User user )
    {
        this.user = user;
    }

    public String getUserString( )
    {
        return userString;
    }

    public void setUserString( String userString )
    {
        this.userString = userString;
    }

    public int getGoogleDirectionsRequests( )
    {
        return googleDirectionsRequests;
    }

    public void setGoogleDirectionsRequests( int googleDirectionsRequests )
    {
        this.googleDirectionsRequests = googleDirectionsRequests;
    }

    public int getNextGoogleDirectionsRequest( )
    {
        return nextGoogleDirectionsRequest;
    }

    public void setNextGoogleDirectionsRequest( int nextGoogleDirectionsRequest )
    {
        OnTrackManager.nextGoogleDirectionsRequest = nextGoogleDirectionsRequest;
    }

    public int getSelectedRoute( )
    {
        return selectedRoute;
    }

    public void setSelectedRoute( int selectedRoute )
    {
        this.selectedRoute = selectedRoute;
    }

    public int getSelectedTrackable( )
    {
        return selectedTrackable;
    }

    public void setSelectedTrackable( int selectedTrackable )
    {
        this.selectedTrackable = selectedTrackable;
    }

    public Trackable getSelectedTrackableAsEntity( )
    {
        return this.getUser( ).getRoute( ).getAllTrackables( ).get( selectedTrackable );
    }

    public Stop getStopOfSelectedTrackable( )
    {
        Trackable trackable = getSelectedTrackableAsEntity( );
        ArrayList<Stop> stops = this.getUser( ).getRoute( ).getStops( );
        for( Stop stop : stops )
        {
            ArrayList<Trackable> trackables = stop.getTrackables( );
            for( Trackable stopTrackable : trackables )
            {
                if( trackable.equals( stopTrackable ) )
                    return stop;
            }
        }
        return null;
    }

    public int getTotalStoppedSeconds( )
    {
        return totalStoppedSeconds;
    }

    public void setTotalStoppedSeconds( int totalStoppedSeconds )
    {
        this.totalStoppedSeconds = totalStoppedSeconds;
    }

    public Chronometer getVehicleStoppedChronometer( )
    {
        return vehicleStoppedChronometer;
    }

    public Chronometer getTimeSinceLastStopChronometer( )
    {
        return timeSinceLastStopChronometer;
    }

    public Chronometer getTotalRouteTimeChronometer( )
    {
        return totalRouteTimeChronometer;
    }

    public Location getCurrentLocation( )
    {
        return currentLocation;
    }

    public void setCurrentLocation( Location currentLocation )
    {
        this.currentLocation = currentLocation;
    }

    public Location getReferenceLocation( )
    {
        return referenceLocation;
    }

    public void setReferenceLocation( Location referenceLocation )
    {
        this.referenceLocation = referenceLocation;
    }

    public void setCurrentLocationAsReferenceLocation( )
    {
        this.referenceLocation = new Location( currentLocation );
    }

    public Stop getCurrentStop( )
    {
        return currentStop;
    }

    public Stop getNextStop( )
    {
        return nextStop;
    }

    public ArrayList<Stop> getTrackedStops( )
    {
        return trackedStops;
    }

    public ArrayList<ChatCard> getChatCards( )
    {
        return chatCards;
    }

    public void setChatCards( ArrayList<ChatCard> chatCards )
    {
        this.chatCards = chatCards;
    }

    public void setTrackedStops( ArrayList<Stop> trackedStops )
    {
        this.trackedStops = trackedStops;
    }

    public void addTrackedPoint( TrackedPoint trackedPoint )
    {

        this.trackedPoints.add( trackedPoint );
        // Recalcula la distanca recorrida
        if( trackedPoints.size( ) > 1 )
        {
            TrackedPoint previousPoint = this.trackedPoints.get( trackedPoints.size( ) - 2 );
            float totalTrackedDistance = previousPoint.getTotalDistance( );
            Location dest = new Location( "" );
            dest.setLatitude( trackedPoint.getLatitude( ) );
            dest.setLongitude( trackedPoint.getLongitude( ) );
            Location src = new Location( "" );
            src.setLatitude( previousPoint.getLatitude( ) );
            src.setLongitude( previousPoint.getLongitude( ) );
            trackedPoint.setTotalDistance( totalTrackedDistance + src.distanceTo( dest ) );
        }
    }

    public void addStackedPoint( TrackedPoint stackedPoint )
    {
        this.stackedPoints.add( stackedPoint );
    }

    public List<Event> getEvents( )
    {
        return events;
    }

    public void setEvents( List<Event> events )
    {
        this.events = events;
    }

    public int getTimeFlag( )
    {
        return timeFlag;
    }

    public void setTimeFlag( int timeFlag )
    {
        this.timeFlag = timeFlag;
    }

    public int getDistanceFlag( )
    {
        return distanceFlag;
    }

    public int getTrackableFlag() {
        return trackableFlag;
    }

    public void setTrackableFlag(int trackableFlag) {
        this.trackableFlag = trackableFlag;
    }

    public void increaseTrackableFlag(){
        this.trackableFlag++;
    }

    public void setDistanceFlag( int stopFlag )
    {
        this.distanceFlag = distanceFlag;
    }

    public int getStopFlag( )
    {
        return stopFlag;
    }

    public void setStopFlag( int stopFlag )
    {
        this.stopFlag = stopFlag;
    }

    public void increaseDistanceFlag( )
    {
        this.distanceFlag++;
    }

    public int getDeviationCounter( )
    {
        return deviationCounter;
    }

    public void setDeviationCounter( int deviationCounter )
    {
        this.deviationCounter = deviationCounter;
    }

    public int getStartCycleState() {
        return startCycleState;
    }

    public void setStartCycleState(int startCycleState) {
        this.startCycleState = startCycleState;
    }

    public int getZeroSpeedCounter( )
    {
        return zeroSpeedCounter;
    }

    public void setZeroSpeedCounter( int zeroSpeedCounter )
    {
        this.zeroSpeedCounter = zeroSpeedCounter;
    }

    public int getNextStopSkipCounter( )
    {
        return nextStopSkipCounter;
    }

    public void setNextStopSkipCounter( int nextStopSkipCounter )
    {
        this.nextStopSkipCounter = nextStopSkipCounter;
    }

    public int getCurrentSpeedInKmH( )
    {
        return Math.round( this.currentLocation.getSpeed( ) * ( 36.0f / 10.0f ) );
    }

    public boolean isDelivery( )
    {
        return this.user.getRoute( ).getType( ) == Route.DELIVERY_CODE;
    }

    // Calcula la distancia total de la ruta, en metros
    public int totalDistanceOfRoute( )
    {
        ArrayList<Stop> stops = user.getRoute( ).getStops();
        int distance = 0;
        for( Stop stop : stops )
        {
            distance += stop.getDistance( );
        }
        return distance;
    }

    // Calcula el tiempo estimado de ruta hasta el paradero actual, en segundos
    public int getEstimatedTimeUntilCurrentStop( )
    {
        int currentStopOrder = currentStop.getOrder();
        int estimatedTimeUntilCurrentStop = 0;
        for( int i = 0; i <= currentStopOrder; i++ )
        {
            estimatedTimeUntilCurrentStop += this.user.getRoute( ).getStops( ).get( i ).getEstimatedTime( );
        }
        return estimatedTimeUntilCurrentStop;

    }

    // Calcula el tiempo estimado de ruta desde el paradero actual en adelanto
    public int getEstimatedTimeFromCurrentStop( )
    {
        int currentStopOrder = currentStop.getOrder( );
        int estimatedTimeFromCurrentStop = 0;
        for( int i = currentStopOrder + 1; i < this.user.getRoute( ).getStops( ).size( ); i++ )
        {
            estimatedTimeFromCurrentStop += this.user.getRoute( ).getStops( ).get( i ).getEstimatedTime( );
        }
        return estimatedTimeFromCurrentStop;

    }

    // Calcula el retardo acumulado en la ruta
    public int getTotalDelayInSeconds( )
    {
        // Ajustar el timeDelayed que se va a reportar, en minutos
        double routeTime = totalRouteTimeChronometer.getSeconds( );

        this.timeDelayed = ( int ) ( Math.round( ( routeTime - ( double )getEstimatedTimeUntilCurrentStop( ) ) / 60.0 ) );

        return ( int )routeTime - getEstimatedTimeUntilCurrentStop( );
    }

    // Recalcula tiempos estimados basados en el retraso
    public void recomputeEstimatedTimeBasedOnDelay( int delay )
    {
        nextStop.addDelayToEstimatedTime( delay );
    }

    // Obtiene un trackable por su posici�n
    public Trackable getTrackableByPosition( int position )
    {
        return this.user.getRoute( ).getAllTrackables( ).get( position );

    }

    // Obtiene un trackable por su id
    public Trackable getTrackableById( String id )
    {
        ArrayList<Trackable> trackables = this.user.getRoute( ).getAllTrackables();

        for( Trackable trackable : trackables )
        {
            if( trackable.getId( ).equals( id ) )
            {
                return trackable;
            }
        }
        return null;

    }

    // Obtener la hora estimada de llegada de la ruta al final de su recorrido
    public String getEstimatedArrivalTimeOfRoute( )
    {

        Calendar c = Calendar.getInstance();
        int seconds, minutes, hours, year, month, day;
        Date date;

        for( Stop stop : this.user.getRoute( ).getStops( ) )
        {
            JSONObject jsonStop = new JSONObject( );

            c.add( Calendar.SECOND, stop.getEstimatedTime( ) );

        }

        seconds = c.get( Calendar.SECOND );
        minutes = c.get( Calendar.MINUTE );
        hours = c.get( Calendar.HOUR_OF_DAY );
        date = c.getTime( );
        year = date.getYear( ) + 1900;
        month = date.getMonth( ) + 1;
        day = date.getDate( );

        String timestamp = "" + year + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + seconds;
        return timestamp;

    }

    public void cancelStop( Stop stop )
    {

        stop.setCancelled( true );

        stop.setDistance( 0 );
        stop.getSteps( ).clear( );

        if( stop.equals( nextStop ) )
        {
            int currentStopOrder = currentStop.getOrder( );
            if( currentStopOrder < user.getRoute( ).getStops( ).size( ) - 1 )
            {
                // Actualizar la siguiente parada solo si existe
                boolean noNextStop = true;
                for( int i = currentStopOrder + 1; i < user.getRoute( ).getStops( ).size( ); i++ )
                {
                    Stop candidateStop = user.getRoute( ).getStops( ).get( i );

                    if( candidateStop.trackablesToPickOrLeaveAtStop( ) && !candidateStop.isCancelled( ) )
                    {
                        nextStop = candidateStop;
                        this.referenceLocation = nextStop.getLocation( );
                        noNextStop = false;
                        break;
                    }
                }

                if( noNextStop )
                {
                    this.routeFinished = true;
                    listener.onRouteFinished( );

                    // Evento de finalizaci�n de ruta
                    Event finalEvent = new Event( null, Timestamp.completeTimestamp( ), Event.END_ROUTE_EVENT_CODE, GeneralMessages.ROUTE_HAS_FINISHED, 0, 1, "" + this.user.getFootprint( ), OnTrackManager.getInstance( ).getUser( ).getRoute( )
                            .getId( ), false );
                    // Guardar evento en dblocal
//                    eventDao.insert( finalEvent );

                    try
                    {
                        EventManager.getInstance( ).executeSendEventTask( OnTrackManagerAsyncTasks.URL_BASE_ADD_EVENTS, ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance( ).getToken( ), ServiceKeys.EVENTS_KEY,
                                finalEvent.getEventJSONString( Event.FK_FOOTPRINT_KEY ) );
                    }
                    catch( JSONException e )
                    {
                        e.printStackTrace( );
                    }
                }
                else
                {
                    OnTrackManagerAsyncTasks.decodePortionOfRoute( this.currentLocation, nextStop, false );
                }

                nextStop.addDelayToEstimatedTime( getTotalDelayInSeconds( ) );
            }
            else
            {

                this.routeFinished = true;
                listener.onRouteFinished( );

                // Evento de finalizaci�n de ruta
                Event finalEvent = new Event( null, Timestamp.completeTimestamp( ), Event.END_ROUTE_EVENT_CODE, GeneralMessages.ROUTE_HAS_FINISHED, 0, 1, "" + this.user.getFootprint( ), OnTrackManager.getInstance( ).getUser( ).getRoute( ).getId( ),
                        false );
                // Guardar evento en dblocal
//                eventDao.insert( finalEvent );

                try
                {
                    EventManager.getInstance( ).executeSendEventTask( OnTrackManagerAsyncTasks.URL_BASE_ADD_EVENTS, ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance( ).getToken( ), ServiceKeys.EVENTS_KEY,
                            finalEvent.getEventJSONString( Event.FK_FOOTPRINT_KEY ) );
                }
                catch( JSONException e )
                {
                    e.printStackTrace( );
                }
            }
        }
        else
        {
            stop.setEstimatedTime( 0 );

            // Trae los vecinos del paradero cancelado y conecta los puntos
            ArrayList<Stop> neighbours = this.user.getRoute( ).findNeighboursOfStop( stop );
            Stop stopBehind = neighbours.get( 0 );
            Stop stopAhead = neighbours.get( 1 );

            if( stopBehind != null && stopAhead != null )
            {

                OnTrackManagerAsyncTasks.decodePortionOfRoute( stopBehind.getLocation( ), stopAhead, true );
            }
        }
        listener.onEstimatedTimesChanged( );
        listener.onStopCancelled(stop);

    }

    public boolean isSimulationsAllowed( )
    {
        return simulationsAllowed;
    }

    public void setSimulationsAllowed( boolean simulationsAllowed )
    {
        this.simulationsAllowed = simulationsAllowed;
    }

    public boolean isCloseToNextStop( )
    {
        return closeToNextStop;
    }

    public void setCloseToNextStop( boolean closeToNextStop )
    {
        this.closeToNextStop = closeToNextStop;
    }

    public void increaseDeviationCounter( )
    {
        this.deviationCounter++;
        if( this.deviationCounter >= DEVIATION_COUNTER_LIMIT )
        {
            vehicleDeviatedFromRoute( );
        }
    }

    public String getPackageOfEventsJSONString( ArrayList<String> eventStrings )
    {
        String packageOfEvensJSONString = "";
        for( int i = 0; i < eventStrings.size(); i++ )
        {
            packageOfEvensJSONString += eventStrings.get( i );
            if( i < eventStrings.size( ) - 1 )
            {
                packageOfEvensJSONString += ",";
            }
        }
        return "[" + packageOfEvensJSONString + "]";
    }

    public boolean checkCorrectNextStop( int order )
    {
        boolean noNextStop = true;
        for( int i = order + 1; i < user.getRoute( ).getStops( ).size(); i++ )
        {
            Stop candidateStop = user.getRoute( ).getStops( ).get( i );

            if( candidateStop.trackablesToPickOrLeaveAtStop( ) && !candidateStop.isCancelled( ) && !candidateStop.didStopHere( ) )
            {
                nextStop = candidateStop;
                noNextStop = false;
                break;
            }
        }
        return noNextStop;
    }

    public boolean checkCorrectNextStopWithoutUpdating( int order )
    {
        boolean noNextStop = true;
        for( int i = order + 1; i < user.getRoute( ).getStops( ).size(); i++ )
        {
            Stop candidateStop = user.getRoute( ).getStops( ).get( i );

            if( candidateStop.trackablesToPickOrLeaveAtStop( ) && !candidateStop.isCancelled( ) && !candidateStop.didStopHere( ) )
            {
                noNextStop = false;
                break;
            }
        }
        return noNextStop;
    }


    public void analizeEstimatedTimes( int nextRequest )
    {
        Route route = this.user.getRoute( );

        if( nextRequest < route.getStops( ).size( ) )
        {
            Stop stop = this.user.getRoute( ).getStops( ).get( nextRequest );
            // Estrategia de tiempos estimados
            // Se usan los tiempos promedio, basados en footprints
            if( stop.isEnabledForEstimatedTimeFromAverage( ) )
            {
                ArrayList<Stop> neighbours = route.findNeighboursOfStop( stop );
                if( neighbours.get( 0 ) != null )
                {
                    Stop stopBehind = neighbours.get( 0 );
                    // Resumen del análisis de tiempos estimados:
                    // * Se mira que el paradero actual y el anterior tengan los tiempos estimados promedio cargados del servidor
                    // * Se decide qué tiempo estimado se le pone a cada paradro
                    // * Si se trata del paradero siguiente, el tiempo estimado es la diferencia entre la hora actual y la hora promedio del siguiente
                    // * Si se trata de otro paradero, el tiempo estimado es la diferencia entre las horas promedio de ambos paraderos


                    try {
                        if(!stopBehind.getAverageTime().equals("null")) {
                            double stopSeconds = Timestamp.getSecondsFromHHMMSS(stop.getAverageTime());
                            double stopBehindSeconds = Timestamp.getSecondsFromHHMMSS(neighbours.get(0).getAverageTime());
                            double nowSeconds = Timestamp.getCurrentTimeInSeconds();
                            if(stopBehind.didStopHere()) {
                                int difference = (int)(stopSeconds - nowSeconds);
                                if(difference > 0) {
                                    // En este caso, si la diferencia es positiva, se asigna
                                    stop.setEstimatedTime(difference);
                                }else{

                                    // Si no, se deja tiempo estimado 0
                                    stop.setEstimatedTime(0);
                                }
                            }else{
                                int difference = (int)(stopSeconds - stopBehindSeconds);
                                if(difference > 0) {
                                    // En este caso, si la diferencia es positiva, se asigna
                                    stop.setEstimatedTime(difference);
                                }else{
                                    // Si no, se usa la de google
                                    stop.setEstimatedTime(stop.getEstimatedTimeGoogle());
                                }
                            }
                            int difference = (int)(stopSeconds - stopBehindSeconds);
                            if(difference >0 ) {
                                stop.setOriginalEstimatedTime((int) stopSeconds - (int) stopBehindSeconds);
                            }else{
                                stop.setOriginalEstimatedTime(stop.getEstimatedTimeGoogle( ));
                            }
                        }
                    }catch (Exception ex){
                        ex.printStackTrace();
                    }
//                        }
//                    }
                }

            }else {
                ArrayList<Stop> neighbours = route.findNeighboursOfStop( stop );
                if( neighbours.get(0) != null ) {
                    Stop stopBehind = neighbours.get(0);
                    if (stop.getOrder() == 1) {
                        if(!routeReloaded) {
                            stop.setEstimatedTime(stop.getEstimatedTimeGoogle());
                        }else{
                            if(stopBehind.getRealTime()==null ) {
                                stop.setEstimatedTime(stop.getEstimatedTimeGoogle());
                            }
                        }
                        stop.setOriginalEstimatedTime(stop.getEstimatedTimeGoogle());
                    } else {
                        if(!routeReloaded) {
                            stop.setEstimatedTime(stop.getEstimatedTimeGoogle() + OnTrackManager.MAXIMUM_TIME_AT_STOP);
                        }else{
                            if(stopBehind.getRealTime()==null) {
                                stop.setEstimatedTime(stop.getEstimatedTimeGoogle() + OnTrackManager.MAXIMUM_TIME_AT_STOP);
                            }
                        }
                        stop.setOriginalEstimatedTime(stop.getEstimatedTimeGoogle() + OnTrackManager.MAXIMUM_TIME_AT_STOP);
                    }
                }
            }
        }

    }

    public TrackedPoint getLastTrackedPoint( )
    {
        // Se clona el arreglo de puntos rastreados para evitar problemas de
        // concurrencia
        ArrayList<TrackedPoint> clonedPoints = ( ArrayList<TrackedPoint> )this.trackedPoints.clone( );
        if( clonedPoints.size() > 0 )
        {
            return clonedPoints.get( clonedPoints.size( ) - 1 );
        }
        return null;
    }

    /***
     * M�TODOS LLAMADOS CUANDO SE DETECTA CAMBIO EN LA UBICACI�N DEL VEH�CULO
     *****/

    // Verifica excesos de velocidad
    public void checkSpeedExceeded( final ArrayList<String> eventStrings )
    {
        // Timer para el control de velocidad
        final Timer t = new Timer( );

        if( getCurrentSpeedInKmH() > MAXIMUM_SPEED_IN_KM_H )
        {
            listener.onSpeedExceeded( getCurrentSpeedInKmH( ), this.isSpeedExceeded( ) );

            if( !speedExceeded )
            {
                // Evento de exceso de velocidad

                t.schedule( new TimerTask( )
                {
                    int counter = 0;

                    public void run( )
                    {
                        counter++;
                        if( counter != 0 && counter % MAXIMUM_TIME_IN_EXCEEDED_SPEED == 0 )
                        {
                            if( speedExceeded )
                            {
                                listener.onSpeedReportSent( );

                                Event speedExceededEvent = new Event( null, Timestamp.completeTimestamp( ), Event.SPEED_EXCEEDED_CODE, GeneralMessages.EXCEEDING_SPEED_LIMIT, 0, 1, "" + OnTrackManager.getInstance( ).getUser( ).getFootprint( ),
                                        OnTrackManager.getInstance( ).getUser( ).getRoute( ).getId( ), false );

                                // EventManager.getInstance().getStackedEvents().add(event);

                                // Guardar evento en db local

                                EventManager.getInstance( ).getEvents( ).add( speedExceededEvent );
//                                eventDao.insert( speedExceededEvent );

                                // Notificar a los listener de eventos que
                                // hay una nueva notificaci�n
//                                EventManager.getInstance( ).notifyEventGenerated( speedExceededEvent.getId( ) );

                                // A�adir el evento de velocidad excedida a
                                // la lista de eventos que se enviar�n al
                                // final
                                try
                                {
                                    eventStrings.add( speedExceededEvent.getEventJSONStringWithoutSquaredBrackets( Event.FK_FOOTPRINT_KEY ) );
                                }
                                catch( JSONException e )
                                {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace( );
                                }
                            }

                            // Matar el timer
                            t.cancel( );
                            t.purge( );
                        }
                    }

                }, TIMER_PERIOD, TIMER_PERIOD );

            }

            speedExceeded = true;
        }
        else
        {
            listener.onSpeedNormal( getCurrentSpeedInKmH( ) );
            t.cancel( );
            t.purge( );
            speedExceeded = false;
        }
    }

    // Verifica si la precisi�n del GPS es baja
    public void checkGpsAccuraccyLow( final ArrayList<String> eventStrings )
    {
        if( !gpsAccuracyLow )
        {
            if( currentLocation.getAccuracy( ) > MAXIMUM_GPS_ACCURACY_ALLOWED )
            {

                Event gpsAccuracyLowEvent = new Event( null, Timestamp.completeTimestamp( ), Event.GPS_ACCURACY_LOW_CODE, GeneralMessages.GPS_ACCURACY_LOW, 0, 1, "" + OnTrackManager.getInstance( ).getUser( ).getFootprint( ), OnTrackManager
                        .getInstance( ).getUser( ).getRoute( ).getId( ), false );

                // A�adir el evento de velocidad excedida a la lista de
                // eventos que se enviar�n al final
                try
                {
                    eventStrings.add( gpsAccuracyLowEvent.getEventJSONStringWithoutSquaredBrackets( Event.FK_FOOTPRINT_KEY ) );
                }
                catch( JSONException e )
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace( );
                }

                gpsAccuracyLow = true;
            }
            else
            {
                gpsAccuracyLow = false;
            }
        }
    }

    // Registra un nuevo punto rastreado
    public TrackedPoint registerNewTrackedPoint( )
    {
        TrackedPoint trackedPoint = new TrackedPoint( currentLocation.getLongitude( ), currentLocation.getLatitude( ), currentLocation.getAltitude( ), Timestamp.completeTimestamp( ), currentLocation.getAccuracy( ), currentLocation.getSpeed( ),
                currentLocation.getBearing( ) );

        if( trackedPoint.getSpeed() == 0 )
        {
            zeroSpeedCounter++;
        }
        else
        {
            zeroSpeedCounter = 0;
        }

        if( this.stackedPoints.size() > MAXIMUM_STACKED_POINTS )
        {
            for( int i = 0; i < 5; i++ )
            {
                this.stackedPoints.remove( i );
            }
        }

        if( zeroSpeedCounter == 0 && this.newPointsGenerated % SEND_LOCATION_INTERVAL == 0 )
        {
            addStackedPoint( trackedPoint );
            addTrackedPoint( trackedPoint );
        }else if(zeroSpeedCounter > 0  && this.newPointsGenerated % SLOW_SEND_LOCATION_INTERVAL == 0){
            addStackedPoint( trackedPoint );
            addTrackedPoint( trackedPoint );
        }

        return trackedPoint;
    }

    // Analiza un potencial retraso antes de llegar al paradero 1
    public void analyzePotentialDelayBeforeFirstStop( final ArrayList<String> eventStrings )
    {

        double euclideanDistanceFromVehicleToStop = currentLocation.distanceTo( this.nextStop.getLocation( ) );
        double distanceFromOriginToFirstStop = this.nextStop.getOriginalDistance( );

        int originalEstimatedTime = this.nextStop.getOriginalEstimatedTime( );

        double distanceFactor = euclideanDistanceFromVehicleToStop / distanceFromOriginToFirstStop;

        if( this.currentStop.getOrder() == 0 )
        {
            // Se verifica la distancia eucl�dea entre el veh�culo el
            // paradero siguiente

            // Si la distancia es mayor al 5% de la que hay entre el
            // paradero 0 y el 1, se activa el estado de alerta para
            // tiempos
            if( euclideanDistanceFromVehicleToStop > 0.05 * distanceFromOriginToFirstStop )
            {
                this.alertTime = true;
            }
            else
            {
                this.alertTime = false;
            }

        }
        else
        {
            this.alertTime = false;
        }

        if( this.alertTime && this.nextStopSkipCounter == 0 ) {
            try {
                double deltaCurrentTimeAndNextStop = Timestamp.getSecondsFromHHMMSS(Timestamp.partialTimestamp()) - Timestamp.getSecondsFromHHMMSS(this.user.getRoute().getEstimatedTimeToStop(this.nextStop.getOrder()));

                // Cuando se detecta un retraso evidente, se reajusta el tiempo
                // estimado del paradero siguiente
                if (deltaCurrentTimeAndNextStop / 60.0 > 1) {
                    this.timeDelayed += (int) (Math.round((distanceFactor) * originalEstimatedTime / 60.0));
                    this.nextStop.increaseEstimatedTime((int) ((distanceFactor) * originalEstimatedTime));
                    this.nextStop.increaseAverageTime((distanceFactor) * originalEstimatedTime);

                    listener.onEstimatedTimesChanged();
                    this.alertTime = false;

                    // S�lo se env�a notificaci�n si el incremento en el
                    // tiempo
                    // estimado es significativo, de lo contrario no se notifica

                    if (Math.round((distanceFactor) * originalEstimatedTime / 60.0) >= 5) {

                        if(!this.delaySent) {

                            Event delayEvent = new Event(null, Timestamp.completeTimestamp(), Event.ROUTE_DELAYED_EVENT_CODE, GeneralMessages.ROUTE_IS_DELAYED, 0, 1, "" + this.user.getFootprint(), OnTrackManager.getInstance().getUser().getRoute()
                                    .getId(), false);

                            try {
                                eventStrings.add(delayEvent.getEventJSONStringWithoutSquaredBrackets(Event.FK_FOOTPRINT_KEY));
                                this.delaySent = true;
                            } catch (JSONException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }
                    }

                    try {
                        OnTrackManagerAsyncTasks.executeSendTrackedStopInfoTask(true, OnTrackManagerAsyncTasks.URL_BASE_TRACKED_STOP_INFO, ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance().getToken(), ServiceKeys.TRACKED_STOPS_KEY, OnTrackManager
                                .getInstance().getStopDataJSONString());
                    } catch (JSONException e) {
                        listener.onAsyncTaskError(GeneralMessages.JSON_ERROR);
                    }
                }
            } catch (Exception e){
                e.printStackTrace();
            }

        }
    }

    public void analyzePotentialAheadInTimeBeforeFirstStop( final ArrayList<String> eventStrings ) {
        if (currentStop.getOrder() == 0 && !isAheadInTime) {
            int originalEstimatedTime = nextStop.getOriginalEstimatedTime();
            int originalEstimatedDistance = nextStop.getOriginalDistance();

            if (originalEstimatedTime != 0) {
                // Velocidad medida en metros por segundo
                double constantSpeed = originalEstimatedDistance / originalEstimatedTime;

                if (constantSpeed != 0) {
                    // int distanceToFirstStop = nextStop.getDistance()
                    // - (int) trackedPoint.getTotalDistance();

                    int distanceToFirstStop = (int) currentLocation.distanceTo(nextStop.getLocation());

                    if (distanceToFirstStop < originalEstimatedDistance * 0.05) {
                        Calendar actualTime = Calendar.getInstance();

                        int timeToNextStopWithConstantSpeed = (int) (distanceToFirstStop / constantSpeed);

                        actualTime.add(Calendar.SECOND, timeToNextStopWithConstantSpeed);

                        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

                        try {

                            int timeInSecondsToNextStopWithConstantSpeed = (int) Timestamp.getSecondsFromHHMMSS(sdf.format(actualTime.getTime()));

                            int timeInSecondsToNextStop = (int) Timestamp.getSecondsFromHHMMSS(this.user.getRoute().getEstimatedTimeToStop(this.nextStop.getOrder()));

                            int deltaTime = timeInSecondsToNextStop - timeInSecondsToNextStopWithConstantSpeed;

                            if (deltaTime / 60 >= 5) {
                                isAheadInTime = true;
                                timeDelayed -= (int) Math.round(deltaTime / 60);
                                nextStop.increaseEstimatedTime(-deltaTime);
                                nextStop.increaseAverageTime(-deltaTime);

                                listener.onEstimatedTimesChanged();

                                if(!this.aheadSent) {
                                    Event aheadInTimeEvent = new Event(null, Timestamp.completeTimestamp(), Event.ROUTE_ADVANCED_EVENT_CODE, GeneralMessages.ROUTE_IS_ADVANCED, 0, 1, "" + this.user.getFootprint(), OnTrackManager.getInstance().getUser()
                                            .getRoute().getId(), false);

                                    try {
                                        eventStrings.add(aheadInTimeEvent.getEventJSONStringWithoutSquaredBrackets(Event.FK_FOOTPRINT_KEY));
                                        this.aheadSent = true;
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }

                                try {
                                    OnTrackManagerAsyncTasks.executeSendTrackedStopInfoTask(true, OnTrackManagerAsyncTasks.URL_BASE_TRACKED_STOP_INFO, ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance().getToken(), ServiceKeys.TRACKED_STOPS_KEY,
                                            OnTrackManager.getInstance().getStopDataJSONString());
                                } catch (JSONException e) {
                                    listener.onAsyncTaskError(GeneralMessages.JSON_ERROR);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }

    // Verifica si se deben generar eventos de tiempo y distancia
    public void checkPotentialDistanceAndTimeEvents( TrackedPoint trackedPoint, ArrayList<String> eventStrings )
    {

        Event eventTime = new Event( null, Timestamp.completeTimestamp( ), null, null, 0, 1, null, OnTrackManager.getInstance( ).getUser( ).getRoute( ).getId( ), false );

        Event eventTimeLong = new Event( null, Timestamp.completeTimestamp( ), null, null, 0, 1, null, OnTrackManager.getInstance( ).getUser( ).getRoute( ).getId( ), false );

        Event eventDistance = new Event( null, Timestamp.completeTimestamp( ), null, null, 0, 1, null, OnTrackManager.getInstance( ).getUser( ).getRoute( ).getId( ), false );

        for( Stop stop : this.user.getRoute( ).getStops( ) )
        {

            if( stop.trackablesToPickOrLeaveAtStop( ) && !stop.isCancelled( ) && !stop.didStopHere( ) && this.nextStopSkipCounter == 0 )
            {

                // Los eventos de distancia y tiempo s�lo se env�a si hay
                // estudiantes para recoger o dejar en el paradero, si no
                // est�
                // cancelado y si no se ha detenido ah�

                float distanceFromOriginToStop = ( float )OnTrackManager.getInstance( ).getUser( ).getRoute( ).getDistanceFromOriginToStop( stop.getOrder( ) );
                float distanceToStop = distanceFromOriginToStop - trackedPoint.getTotalDistance( );

                if( distanceFromOriginToStop >= 1000 )
                {
                    if( !stop.isReported1km( ) && distanceToStop <= 1000 )
                    {
                        eventDistance.setCategory( Event.VEHICLE_1KM_AWAY_FROM_STOP_CODE );
                        eventDistance.setDescription( GeneralMessages.VEHICLE_1KM_AWAY_FROM_STOP );
                        eventDistance.setFkDestination( "" + stop.getTrackedStopId( ) );
                        stop.setReported1km( true );
                    }
                }

                if( distanceFromOriginToStop >= 500 )
                {
                    if( !stop.isReported500m( ) && distanceToStop <= 500 )
                    {
                        eventDistance.setCategory( Event.VEHICLE_500M_AWAY_FROM_STOP_CODE );
                        eventDistance.setDescription( GeneralMessages.VEHICLE_500M_AWAY_FROM_STOP );
                        eventDistance.setFkDestination( "" + stop.getTrackedStopId( ) );
                        stop.setReported500m( true );
                    }
                }

                if( distanceFromOriginToStop >= 200 )
                {
                    if( !stop.isReported200m( ) && distanceToStop <= 200 )
                    {
                        eventDistance.setCategory( Event.VEHICLE_200M_AWAY_FROM_STOP_CODE );
                        eventDistance.setDescription( GeneralMessages.VEHICLE_200M_AWAY_FROM_STOP );
                        eventDistance.setFkDestination( "" + stop.getTrackedStopId( ) );
                        stop.setReported200m( true );
                    }
                }

                int timeFromOriginToStop = getUser( ).getRoute( ).getEstimatedTimeToStopInSeconds( stop.getOrder( ) );
                int timeEllapsed = getTimeSinceRouteStarted( );
                int timeToStop = timeFromOriginToStop - timeEllapsed;

                if( !this.alertTime )
                {
                    if( timeFromOriginToStop >= 5 * 60 )
                    {

                        if( !stop.isReported5min( ) && timeToStop <= 5 * 60 )
                        {
                            eventTime.setCategory( Event.VEHICLE_5MIN_AWAY_FROM_STOP_CODE );
                            eventTime.setDescription( GeneralMessages.VEHICLE_5MIN_AWAY_FROM_STOP );
                            eventTime.setFkDestination( "" + stop.getTrackedStopId( ) );
                            stop.setReported5min( true );
                        }
                    }

                    if( timeFromOriginToStop >= 10 * 60 )
                    {
                        if( !stop.isReported10min( ) && timeToStop <= 10 * 60 )
                        {

                            eventTimeLong.setCategory( Event.VEHICLE_10MIN_AWAY_FROM_STOP_CODE );
                            eventTimeLong.setDescription( GeneralMessages.VEHICLE_10MIN_AWAY_FROM_STOP );
                            eventTimeLong.setFkDestination( "" + stop.getTrackedStopId( ) );
                            stop.setReported10min( true );
                        }

                    }
                }

            }

        }

        if( eventDistance.getCategory( ) != null )
        {
            // A�adir el evento de distancia excedida a la lista de eventos
            // que
            // se enviar�n al final
            try
            {
                eventStrings.add( eventDistance.getEventJSONStringWithoutSquaredBrackets( Event.FK_TRACKED_STOP_KEY ) );
            }
            catch( JSONException e )
            {
                // TODO Auto-generated catch block
                e.printStackTrace( );
            }

            // Guardar evento en db local
            this.events.add( eventDistance );
//            this.eventDao.insert( eventDistance );

            // Notificar a los listener de eventos que hay una nueva
            // notificaci�n
//            EventManager.getInstance( ).notifyEventGenerated( eventDistance.getId( ) );
        }

        if( eventTime.getCategory( ) != null )
        {

            // A�adir el evento de distancia excedida a la lista de eventos
            // que
            // se enviar�n al final
            try
            {
                eventStrings.add( eventTime.getEventJSONStringWithoutSquaredBrackets( Event.FK_TRACKED_STOP_KEY ) );
            }
            catch( JSONException e )
            {
                // TODO Auto-generated catch block
                e.printStackTrace( );
            }

            // Guardar evento en db local
            this.events.add( eventTime );
//            this.eventDao.insert( eventTime );

            // Notificar a los listener de eventos que hay una nueva
            // notificaci�n
//            EventManager.getInstance( ).notifyEventGenerated( eventTime.getId( ) );
        }

        if( eventTimeLong.getCategory() != null )
        {

            // A�adir el evento de distancia excedida a la lista de eventos
            // que
            // se enviar�n al final
            try
            {
                eventStrings.add( eventTimeLong.getEventJSONStringWithoutSquaredBrackets( Event.FK_TRACKED_STOP_KEY ) );
            }
            catch( JSONException e )
            {
                // TODO Auto-generated catch block
                e.printStackTrace( );
            }

            // Guardar evento en db local
            this.events.add(eventTimeLong);
//            this.eventDao.insert(eventTimeLong);

            // Notificar a los listener de eventos que hay una nueva
            // notificaci�n
//            EventManager.getInstance( ).notifyEventGenerated( eventTimeLong.getId( ) );
        }
    }

    public ArrayList<Route> getMyRoutes( )
    {
        return myRoutes;
    }

    public ArrayList<Route> getMyRoutesFreeMode(){
        ArrayList<Route> myRoutesFreeMode = new ArrayList<Route>();
        for(Route route:this.myRoutes){
            if(route.getNumberOfStops()==0){
                myRoutesFreeMode.add(route);
            }
        }
        return myRoutesFreeMode;
    }

    public void setMyRoutes( ArrayList<Route> myRoutes )
    {
        this.myRoutes = myRoutes;
    }


    public void loadRefuges( Context context )
    {
        try
        {
            InputStream is = context.getAssets( ).open( "data/refuges.csv" );
            int size = is.available( );
            byte[] buffer = new byte[ size ];
            is.read( buffer );
            is.close( );
            String text = new String( buffer );
            String[] lines = text.split( "\n" );

            for( int i = 1; i < 214; i++ )
            {
                String line = lines[ i ];
                String[] parts = line.split( "\\," );

                Refuge refuge = new Refuge( parts[ 0 ], parts[ 4 ], parts[ 3 ], parts.length == 6 ? parts[ 5 ] : "N/A", Double.parseDouble( parts[ 2 ] ), Double.parseDouble( parts[ 1 ] ) );
                refuges.add( refuge );
            }
        }
        catch( IOException e )
        {
            String x = e.getMessage( );
            e.printStackTrace( );
        }
    }

    public ArrayList<Refuge> getRefuges( )
    {
        return refuges;
    }

    public Refuge getClosestRefuge( LatLng latLng )
    {
        double distance = 0;
        Refuge closestRefuge = null;

        for( Refuge refuge : refuges )
        {
            LatLng latLngRefuge = new LatLng( refuge.getLatitude( ), refuge.getLongitude( ) );

            if( closestRefuge == null || latLng.distanceTo( latLngRefuge ) < distance )
            {
                distance = latLng.distanceTo( latLngRefuge );
                closestRefuge = refuge;
            }
        }

        return closestRefuge;
    }

    public void showDirectionsToRefuge( Context context, LatLng location, Refuge refuge )
    {
        String queryString = "";

        queryString += OnTrackManagerAsyncTasks.URL_MAPBOX_DIRECTIONS_BASE;

        queryString += ""+location.getLongitude()+","+location.getLatitude()+";";

        queryString += ""+refuge.getLongitude() + ","
                + refuge.getLatitude();

        queryString += ".json?access_token="+ Parameters.MAP_BOX_TOKEN;
        queryString += "&geometry=polyline";

        otr.ontrack_onroad.freeModeTasks.GetDirectionsToRefugeTask getDirectionsToRefugeTask = new otr.ontrack_onroad.freeModeTasks.GetDirectionsToRefugeTask( context );
        getDirectionsToRefugeTask.execute( queryString );
    }

    public void showDirectionsToRefugeOnRoad( Context context, LatLng location, Refuge refuge )
    {
        String queryString = "";

        queryString += OnTrackManagerAsyncTasks.URL_MAPBOX_DIRECTIONS_BASE;

        queryString += ""+location.getLongitude()+","+location.getLatitude()+";";

        queryString += ""+refuge.getLongitude() + ","
                + refuge.getLatitude();

        queryString += ".json?access_token="+ Parameters.MAP_BOX_TOKEN;
        queryString += "&geometry=polyline";

        otr.ontrack_onroad.tasks.GetDirectionsToRefugeTask getDirectionsToRefugeTask = new otr.ontrack_onroad.tasks.GetDirectionsToRefugeTask( context, refuge.getName() );
        getDirectionsToRefugeTask.execute( queryString );
    }


    public void completeProcessLocation(Location location){
        OnTrackManager.getInstance().setCurrentLocation(location);
        if(OnTrackManager.getInstance().routeDidStart()) {
            double offset = OnTrackManager.getInstance().isHasLeftStop() ? 0
                    : Stop.MAXIMUM_DISTANCE_TO_STOP;
            ArrayList<Point> pointsForApproximation = new ArrayList<Point>();

            // Por asincronï¿½a, es posible que los steps queden
            // momentï¿½neamente
            // vacï¿½os,
            // lo cual es indeseable, acï¿½ se podrï¿½ hacer un control de
            // rutas
            // reales a ver si sirve el parche
            if (OnTrackManager.getInstance().getCurrentStop().getSteps()
                    .size() > 0) {
                pointsForApproximation.addAll(OnTrackManager.getInstance()
                        .getCurrentStop().getSteps());
            }

            if (OnTrackManager.getInstance().getNextStop().getSteps()
                    .size() > 0) {
                pointsForApproximation.addAll(OnTrackManager.getInstance()
                        .getNextStop().getSteps());
            }
            if (GeoPos.convertLocationToPointOnRoute(location,
                    pointsForApproximation, offset)) {
                OnTrackManager.getInstance().vehicleDeviatedFromRoute();
                if (GeoPos.isLocationExtremelyDeviatedFromRoute(location,
                        pointsForApproximation)) {
                    OnTrackManager.getInstance()
                            .vehicleExtremelyDeviatedFromRoute();
                }
            } else {
                OnTrackManager.getInstance().vehicleOnTrack();
            }
        }

        OnTrackManager.getInstance().vehicleLocationChanged(location);
    }

    public void sendSimulatedLocation(){
        if (!OnTrackManager.getInstance().isRouteFinished()) {
            List<SimLocation> list = OnTrackManager.getInstance()
                    .getRecordedPoints();
            Location location = list.get(
                    OnTrackManager.getInstance().getSimulationCounter())
                    .toLocation();

            OnTrackManager.getInstance().completeProcessLocation(location);
            listener.processSimulated(location);

            if (OnTrackManager.getInstance().getSimulationCounter() < OnTrackManager
                    .getInstance().getRecordedPoints().size() - 1) {
                new Handler().postDelayed(
                        new Runnable() {
                            @Override
                            public void run() {
                                OnTrackManager.getInstance()
                                        .increaseSimulationCounter();
                                sendSimulatedLocation();
                            }
                        },

                        list.get(
                                OnTrackManager.getInstance()
                                        .getSimulationCounter() + 1).getTime()
                                - list.get(
                                OnTrackManager.getInstance()
                                        .getSimulationCounter())
                                .getTime());
            }
        }
    }



}
