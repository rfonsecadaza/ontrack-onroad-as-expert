package otr.ontrack_onroad.managers;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONException;
import org.json.JSONObject;

import otr.ontrack_onroad.models.Organization;
import otr.ontrack_onroad.models.Refuge;
import otr.ontrack_onroad.models.TrackedPoint;
import otr.ontrack_onroad.parameters.ServiceKeys;
import otr.ontrack_onroad.utils.Timestamp;
import otr.ontrack_onroad_debug.activities.FreeMode;
import otr.ontrack_onroad_debug.activities.R;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import com.mapbox.mapboxsdk.annotations.Icon;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.annotations.PolylineOptions;
import com.mapbox.mapboxsdk.geometry.LatLng;
//import com.mapbox.mapboxsdk.overlay.Marker;
//import com.mapbox.mapboxsdk.overlay.PathOverlay;
import com.mapbox.mapboxsdk.views.MapView;

public class FreeModeLocationManager implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener
{
    private final static int MILLISECONDS_PER_SECOND = 1000;

    private final static int UPDATE_INTERVAL_IN_SECONDS = 1;

    private final static long UPDATE_INTERVAL = MILLISECONDS_PER_SECOND * UPDATE_INTERVAL_IN_SECONDS;

    private final static long SEND_LOCATION_PERIOD = 10000;

    private final static int MAXIMUM_STACKED_POINTS = 15;

    private static FreeModeLocationManager instance;

    private Context context;

    private LocationRequest gmLocationRequest;

    private GoogleApiClient gmLocationClient;

    private Location location;

    private Timer sendLocationTimer;

    private ArrayList<TrackedPoint> stackedPoints;

//    private com.mapbox.mapboxsdk.overlay.Marker vehicleMarker;

    private MarkerOptions vehicleMarker;

    private MapView mvMap;

    private ArrayList<LatLng> pointsToRefuge;

//    private PathOverlay line;

    private Integer currentTrackablePosition = 0;

    private Location locationOfLastReport = null;

    public static final int MINIMUM_DISTANCE_FOR_POSITIONS = 30; // En metros

    public FreeModeLocationManager( )
    {
        stackedPoints = new ArrayList<TrackedPoint>( );

        sendLocationTimer = new Timer( );
        sendLocationTimer.schedule( new TimerTask( )
        {
            public void run( )
            {
                if(OnTrackManager.getInstance().routeDidStart()) {
                    if (location != null) {
                        FreeModeEventsManager.getInstance().sendTrackedPoints();
                    }
                }
            }
        }, SEND_LOCATION_PERIOD, SEND_LOCATION_PERIOD );

    }

    public static FreeModeLocationManager getInstance( )
    {
        if( instance == null )
        {
            instance = new FreeModeLocationManager( );
        }

        return instance;
    }

    @Override
    public void onConnectionFailed( ConnectionResult result )
    {

    }

    @Override
    public void onConnected( Bundle connectionHint )
    {
        LocationServices.FusedLocationApi.requestLocationUpdates(gmLocationClient, gmLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended( int cause )
    {
        if( gmLocationClient.isConnected( ) )
        {
            LocationServices.FusedLocationApi.removeLocationUpdates(gmLocationClient, this);
        }

        gmLocationClient.disconnect();
    }

    @Override
    public void onLocationChanged( Location location )
    {
        boolean startAutomatically = ((FreeMode)context).getStartAutomatically();
        if(OnTrackManager.getInstance().isWaitingGPS() && OnTrackManager.getInstance().isRouteDetailsLoaded()) {
            if(!startAutomatically) {
                ((FreeMode) context).configureStartRouteFragmentForStart();
            }else{
                ((FreeMode) context).startRoute();
                OnTrackManager.getInstance().setWaitingGPS(false);
            }
        }else{
            LatLng position = new LatLng(location.getLatitude(), location.getLongitude());

            if (vehicleMarker == null) {
                IconFactory mIconFactory = IconFactory.getInstance(context);
                Drawable mIconDrawable = ContextCompat.getDrawable(context, R.drawable.position_arrow);
                Icon icon = mIconFactory.fromDrawable(mIconDrawable);

                vehicleMarker = new MarkerOptions().position(position)
                        .icon(icon);
                mvMap.addMarker(vehicleMarker);
            } else {
                mvMap.removeMarker(vehicleMarker.getMarker());
                IconFactory mIconFactory = IconFactory.getInstance(context);
                Drawable mIconDrawable = ContextCompat.getDrawable(context, R.drawable.position_arrow);
                Icon icon = mIconFactory.fromDrawable(mIconDrawable);

                vehicleMarker = new MarkerOptions().position(position)
                        .icon(icon);
                mvMap.addMarker(vehicleMarker);
            }

            mvMap.setLatLng(position);
            mvMap.setZoom(16.5);
            mvMap.setBearing(location.getBearing());
            mvMap.setTilt(45d, 0l);

            this.location = location;
            registerNewTrackedPoint();
        }
    }

    public void setup( MapView mvMap, Context context )
    {
        this.mvMap = mvMap;
        this.context = context;
        gmLocationRequest = LocationRequest.create( );
        gmLocationRequest.setPriority( LocationRequest.PRIORITY_HIGH_ACCURACY );
        gmLocationRequest.setInterval( UPDATE_INTERVAL );

        gmLocationClient = new GoogleApiClient.Builder( context ).addApi( LocationServices.API ).addConnectionCallbacks( this ).addOnConnectionFailedListener( this ).build( );
        gmLocationClient.connect( );

        Organization organization = OnTrackManager.getInstance( ).getUser( ).getOrganizationByID( OnTrackManager.getInstance( ).getRoutes( ).get( OnTrackManager.getInstance( ).getSelectedRoute( ) ).getOrganizationId( ) );
        if( organization.getCreatedBy( ).equals("superadmin@ontrackschool.ec") )
        {
            ( ( FreeMode )context ).showButtonGoToRefuge( );

            OnTrackManager.getInstance( ).loadRefuges(context);

            for (Refuge refuge : OnTrackManager.getInstance().getRefuges()) {

                IconFactory mIconFactory = IconFactory.getInstance(context);
                Drawable mIconDrawable = context.getResources().getDrawable(context.getResources().getIdentifier(
                        "refuge_marker",
                        "drawable", context.getPackageName()));
                Icon icon = mIconFactory.fromDrawable(mIconDrawable);
                MarkerOptions marker =new MarkerOptions()
                        .position(new LatLng(refuge.getLatitude(), refuge.getLongitude())).title(refuge.getName()).snippet(refuge.getType() + "-" + refuge.getResponsible() + "-Cel:" + refuge.getMobilePhone())
                        .icon(icon);

                mvMap.addMarker(marker);
            }
        }

    }

    public void registerNewTrackedPoint( )
    {
        TrackedPoint trackedPoint = new TrackedPoint( location.getLongitude( ), location.getLatitude( ), location.getAltitude( ), Timestamp.completeTimestamp( ), location.getAccuracy( ), location.getSpeed(), location.getBearing() );

        if( this.stackedPoints.size( ) > MAXIMUM_STACKED_POINTS )
        {
            for( int i = 0; i < 5; i++ )
            {
                this.stackedPoints.remove( i );
            }
        }

        stackedPoints.add(trackedPoint);
    }

    public String getStackedPointsJSONString( ) throws JSONException
    {
        JSONObject jsonObject = new JSONObject( );

        ArrayList<TrackedPoint> clonedPoints = ( ArrayList<TrackedPoint> )stackedPoints.clone( );

        for( TrackedPoint stackedPoint : clonedPoints )
        {
            JSONObject jsonStackedPoint = new JSONObject( );
            jsonStackedPoint.put( ServiceKeys.FK_FOOTPRINT_KEY, OnTrackManager.getInstance( ).getUser( ).getFootprint( ) );
            jsonStackedPoint.put( ServiceKeys.LONGITUDE_KEY, stackedPoint.getLongitude() );
            jsonStackedPoint.put( ServiceKeys.LATITUDE_KEY, stackedPoint.getLatitude() );
            jsonStackedPoint.put( ServiceKeys.ALTITUDE_KEY, stackedPoint.getAltitude() );
            jsonStackedPoint.put( ServiceKeys.DATE_KEY, stackedPoint.getDate() );
            jsonStackedPoint.put( ServiceKeys.ACCURACY_KEY, stackedPoint.getAccuracy() );
            jsonStackedPoint.put( ServiceKeys.BEARING_KEY, stackedPoint.getBearing() );
            jsonStackedPoint.put( ServiceKeys.SPEED_KEY, stackedPoint.getSpeed() );
            jsonStackedPoint.put( ServiceKeys.DISTANCE_COVERED_KEY, stackedPoint.getTotalDistance() );
            jsonStackedPoint.put(ServiceKeys.TRACKABLE_FLAG_KEY, OnTrackManager.getInstance().getTrackableFlag());

            //            jsonStackedPoint.put( ServiceKeys.TIME_FLAG_KEY, this.getTimeFlag( ) );
            //            jsonStackedPoint.put( ServiceKeys.DISTANCE_FLAG_KEY, this.getDistanceFlag( ) );
            //            jsonStackedPoint.put( ServiceKeys.STOP_FLAG_KEY, this.getStopFlag( ) );
            //            jsonStackedPoint.put( ServiceKeys.TIME_DELAYED_KEY, this.getTimeDelayed( ) );

            jsonObject.accumulate( "STACKED_POINTS", jsonStackedPoint );
        }

        if( clonedPoints.size( ) == 1 )
        {
            return "[" + jsonObject.getString( ServiceKeys.STACKED_POINTS_KEY ) + "]";
        }
        else
        {
            return jsonObject.getString( ServiceKeys.STACKED_POINTS_KEY );
        }
    }

    public ArrayList<TrackedPoint> getStackedPoints( )
    {
        return stackedPoints;
    }

    public ArrayList<LatLng> getPointsToRefuge( )
    {
        return pointsToRefuge;
    }

    public void setPointsToRefuge( ArrayList<LatLng> pointsToRefuge )
    {
        this.pointsToRefuge = pointsToRefuge;
    }

    public void drawRouteToRefuge( )
    {
//        if( line != null )
//        {
//            mvMap.getOverlays( ).remove( line );
//        }
//
//        line = new PathOverlay( context.getResources().getColor(android.R.color.holo_green_dark), 4 );
//
//        for( LatLng latLng : pointsToRefuge )
//        {
//            line.addPoint( latLng );
//        }
//
//        mvMap.getOverlays().add( line );

        if(pointsToRefuge.size()>0) {
            mvMap.addPolyline(new PolylineOptions().add(pointsToRefuge.toArray(new LatLng[pointsToRefuge.size()])).color(Color.parseColor("#669900")).width(8));
        }
    }

    public Location getLocation( )
    {
        return location;
    }

    public Integer getCurrentTrackablePosition() {
        return currentTrackablePosition;
    }

    public void setCurrentTrackablePosition(Integer currentTrackablePosition) {
        this.currentTrackablePosition = currentTrackablePosition;
    }

    public Location getLocationOfLastReport() {
        return locationOfLastReport;
    }

    public void setLocationOfLastReport(Location locationOfLastReport) {
        this.locationOfLastReport = locationOfLastReport;
    }

    public void checkIfLocationChangedNeeded(){
        if(currentTrackablePosition == 0){
            this.currentTrackablePosition = 1;
            this.locationOfLastReport = this.location;
        }else{
            if(this.locationOfLastReport != null && this.location != null){
                if(this.locationOfLastReport.distanceTo(this.location) > MINIMUM_DISTANCE_FOR_POSITIONS){
                    this.currentTrackablePosition++;
                    this.locationOfLastReport = this.location;
                }
            }
        }
    }


}