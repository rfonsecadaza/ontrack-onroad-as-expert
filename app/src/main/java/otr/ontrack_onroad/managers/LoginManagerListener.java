package otr.ontrack_onroad.managers;

public interface LoginManagerListener {
	public void onPrelogin();
	public void onLogin();
	public void onNotLogin();
	public void onLoginError(String error);
	public void onForgotPasswordRequest(String message);
}
