package otr.ontrack_onroad.managers;

import java.io.IOException;


import otr.ontrack_onroad.parameters.GeneralMessages;
import otr.ontrack_onroad.utils.WebRequestManager;
import android.os.AsyncTask;

public class CloseSessionManager {

	private static CloseSessionManager mInstance = null;

	private CloseSessionManagerListener listener;

	public static CloseSessionManager getInstance() {
		if (mInstance == null) {
			mInstance = new CloseSessionManager();
		}
		return mInstance;
	}

	public void addListener(CloseSessionManagerListener listener) {
		this.listener = listener;
	}

	// AsyncTask para cierre de sesiï¿½n
	private static class CloseSessionTask extends
			AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(final String... args) {
			String result;
			try {
				result = WebRequestManager.executeTask(args);
				return result;
			} catch (IOException e) {
				return e.getMessage();
			}
		}

		@Override
		protected void onPostExecute(String result) {
			if (result.equals(GeneralMessages.HTTP_CONNECTION_ERROR)
					|| result.contains(GeneralMessages.HTTP_NOT_FOUND_ERROR)) {
				if (OnTrackManager.getInstance().getListener() != null){
					OnTrackManager.getInstance().getListener()
							.onAsyncTaskError(result);
				}
			} else {

				CloseSessionManager.getInstance().getListener()
						.onSessionClosedInServer();
				OnTrackManager.getInstance().resetAllData();
			}
		}

	}

	public static void executeCloseSessionTask(String... args) {
		CloseSessionTask task = new CloseSessionTask();
		task.execute(args);
	}

	public CloseSessionManagerListener getListener() {
		// TODO Auto-generated method stub
		return this.listener;
	}

}
