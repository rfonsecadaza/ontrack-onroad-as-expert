package otr.ontrack_onroad.managers;

import java.io.IOException;

import org.json.JSONException;

import otr.ontrack_onroad.parameters.GeneralMessages;
import otr.ontrack_onroad.parameters.Parameters;
import otr.ontrack_onroad.parameters.PreferenceKeys;
import otr.ontrack_onroad.utils.JSONManager;
import otr.ontrack_onroad.utils.WebRequestManager;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;

public class AuthenticationManager {

	public static final String URL_BASE_DRIVER = "ApiServices/GetDriverDetail";

	private static AuthenticationManager mInstance = null;



	private AuthenticationManagerListener listener;

	private AuthenticationManager() {
	}

	public static AuthenticationManager getInstance() {
		if (mInstance == null) {
			mInstance = new AuthenticationManager();
		}
		return mInstance;
	}

	public AuthenticationManagerListener getListener() {
		return listener;
	}

	public void setListener(AuthenticationManagerListener listener) {
		this.listener = listener;
	}
	
//	public void removeListener(AuthenticationManagerListener listener){
//		this.listeners.remove(listener);
//	}

	// Se construirá un thread que va a sustituir el AsyncTask
	// Acá no es que haga mayor diferencia, pero pues para mantener la tradición
	private class LoadRouteThread extends Thread {
		private String[] params;
		private String response;

		public LoadRouteThread(String...params){
			this.params = params;
		}

		public void preExecute(){
			AuthenticationManager.this.getListener().onRoutePreloaded();
		}

		@Override
		public void run() {
			preExecute();
			try {
				this.response = WebRequestManager.executeTask(this.params);

				// Se carga toda la informaciï¿½n del usuario que se ha
				// autenticado, y la informaciï¿½n bï¿½sica de todas las rutas
				// que
				// tiene asignadas
				// Al cargar el usuario, el atributo 'routes' de OnTrackSystem
				// queda actualizado con la informaciï¿½n de las rutas y sus
				// itinerarios
				// JSONManager.loadUser(response, false);


			} catch (IOException e) {
				// for(AuthenticationManagerListener listener:
				// AuthenticationManager.this.listeners){
				// listener.onAuthenticationError(GeneralMessages.ERROR_LOADING_ROUTES);
				// }
				this.response = GeneralMessages.HTTP_NOT_FOUND_ERROR;
			}
			postExecute();
		}


		public void postExecute(){
			if( this.response.equals( GeneralMessages.HTTP_CONNECTION_ERROR ) || this.response.equals( GeneralMessages.HTTP_NOT_FOUND_ERROR ) )
			{
				AuthenticationManager.this.getListener().onAuthenticationError( response );
			}
			else
			{
				try
				{
					int status = JSONManager.loadUser( this.response, true );

					if( status == Parameters.STATUS_CORRECT )
					{
						SharedPreferences prefs = OnTrackManager.getInstance( ).getContext( ).getSharedPreferences( PreferenceKeys.PREFERENCES_KEY, Context.MODE_PRIVATE );
						String token = prefs.getString( PreferenceKeys.TOKEN_KEY, null );
						OnTrackManager.getInstance( ).setToken( token );


						AuthenticationManager.this.listener.onRouteLoaded( );
					}
					else
					{
						AuthenticationManager.this.listener.onRouteNotLoaded( );
					}
				}
				catch( JSONException e )
				{
					AuthenticationManager.this.listener.onAuthenticationError( GeneralMessages.JSON_ERROR );
				}
			}

		}

	}

	private class LoadRouteTask extends AsyncTask<String, Integer, String> {

		@Override
		protected void onPreExecute() {
			
				AuthenticationManager.this.listener.onRoutePreloaded();
			
		}

		@Override
		protected String doInBackground(String... args) {
			String response;
			try {
				response = WebRequestManager.executeTask(args);

				// Se carga toda la informaciï¿½n del usuario que se ha
				// autenticado, y la informaciï¿½n bï¿½sica de todas las rutas
				// que
				// tiene asignadas
				// Al cargar el usuario, el atributo 'routes' de OnTrackSystem
				// queda actualizado con la informaciï¿½n de las rutas y sus
				// itinerarios
				// JSONManager.loadUser(response, false);


			} catch (IOException e) {
				// for(AuthenticationManagerListener listener:
				// AuthenticationManager.this.listeners){
				// listener.onAuthenticationError(GeneralMessages.ERROR_LOADING_ROUTES);
				// }
				response = GeneralMessages.HTTP_NOT_FOUND_ERROR;
			}
			return response;

		}

        @Override
        protected void onPostExecute( String result )
        {
            if( result.equals( GeneralMessages.HTTP_CONNECTION_ERROR ) || result.equals( GeneralMessages.HTTP_NOT_FOUND_ERROR ) )
            {
                AuthenticationManager.this.listener.onAuthenticationError( result );
            }
            else
            {
                try
                {
                    int status = JSONManager.loadUser( result, true );

                    if( status == Parameters.STATUS_CORRECT )
                    {
                        SharedPreferences prefs = OnTrackManager.getInstance( ).getContext( ).getSharedPreferences( PreferenceKeys.PREFERENCES_KEY, Context.MODE_PRIVATE );
                        String token = prefs.getString( PreferenceKeys.TOKEN_KEY, null );
                        OnTrackManager.getInstance( ).setToken( token );


                        AuthenticationManager.this.listener.onRouteLoaded( );
                    }
                    else
                    {
                        AuthenticationManager.this.listener.onRouteNotLoaded( );
                    }
                }
                catch( JSONException e )
                {
                    AuthenticationManager.this.listener.onAuthenticationError( GeneralMessages.JSON_ERROR );
                }
            }
        }
    }

	public void executeLoadRouteTask(String... params) {
		LoadRouteTask task = new LoadRouteTask();
		task.execute(params);
//		LoadRouteThread task = new LoadRouteThread(params);
//		task.start();
	}

	private class RequestChatCardsThread extends Thread {

		private String[] params;
		private String response;

		public RequestChatCardsThread(String...params){
			this.params = params;
		}
		@Override
		public void run(){
			try {
				this.response = WebRequestManager.executeTask(this.params);
				JSONManager.loadChatCards(this.response);
			} catch (IOException e) {
				this.response = GeneralMessages.HTTP_NOT_FOUND_ERROR;
			} catch (JSONException e) {
				this.response = GeneralMessages.JSON_ERROR;
			}
			postExecute();
		}

		public void postExecute(){
			if (this.response.equals(GeneralMessages.JSON_ERROR)
					|| this.response.equals(GeneralMessages.HTTP_CONNECTION_ERROR)
					|| this.response.equals(GeneralMessages.HTTP_NOT_FOUND_ERROR)) {
				AuthenticationManager.this.listener.onChatcardsError(this.response);
			} else {

				AuthenticationManager.this.listener.onChatcardsLoaded();
			}
		}

	}

	private class RequestChatCardsTask extends
			AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... args) {
			String response = null;
			try {
				response = WebRequestManager.executeTask(args);
				JSONManager.loadChatCards(response);
			} catch (IOException e) {
				response = GeneralMessages.HTTP_NOT_FOUND_ERROR;
			} catch (JSONException e) {
				response = GeneralMessages.JSON_ERROR;
			}

			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			if (result.equals(GeneralMessages.JSON_ERROR)
					|| result.equals(GeneralMessages.HTTP_CONNECTION_ERROR)
					|| result.equals(GeneralMessages.HTTP_NOT_FOUND_ERROR)) {
				AuthenticationManager.this.listener.onChatcardsError(result);
			} else {
				
				AuthenticationManager.this.listener.onChatcardsLoaded();
			}
		}

	}
	
	public void executeRequestChatcardsTask(String...params){
		RequestChatCardsTask task = new RequestChatCardsTask();
		task.execute(params);
//		RequestChatCardsThread task = new RequestChatCardsThread(params);
//		task.start();
	}

}
