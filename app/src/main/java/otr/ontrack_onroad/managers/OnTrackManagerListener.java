package otr.ontrack_onroad.managers;

import android.location.Location;

import java.util.ArrayList;

import otr.ontrack_onroad.models.Point;
import otr.ontrack_onroad.models.Route;
import otr.ontrack_onroad.models.Stop;

/**
 * Created by Diego on 12/23/13...
 * But modified a lot by Rodrigo
 */
public interface OnTrackManagerListener
{
    public void onETASUpdate ();
    
    public void onAsyncTaskError(String error);
    
    public void onDetailedRouteLoaded(Route route);
    
    public void onRouteDecoded(ArrayList<Stop> stops);
    
    public void onPortionOfRouteDecoded(Stop stop);
    
    public void onVehicleJustStoppedAtStop(boolean isDelivery, Stop stop, int numberOfStops, int estimatedTimeFromCurrentStop);    
    
    public void onVehicleJustLeftCurrentStop(Stop nextStop); 
    
    public void onRouteStarted(int currentStopOrder);
    
    public void onRouteFinished();
    
    public void onSpeedExceeded(int speed, boolean isSpeedExceeded);
    
    public void onSpeedReportSent();
    
    public void onSpeedNormal(int speed);
    
    public void onVehicleApproachingToNextStop(boolean isDelivery, Stop nextStop, int numberOfStops);
    
    public void onDisconnectLocation();
    
    public void onLastInfoSent();
    
    public void onStopCancelled(Stop stop);
    
    public void onVehicleVeryCloseToNextStop(Stop nextStop);
    
    public void onEstimatedTimesChanged();
    
    public void onPreComputingPortionOfRoute();
    
    public void onVehicleMightStopAtStrangeStop(Stop stop);
    
    public void onVehicleMightCancelSkippedStop(Stop stop);
    
    public void onVehicleStartedMoving();
    
    public void onNoveltiesLoaded();

    public void processSimulated(Location location);
    
    
    
}
