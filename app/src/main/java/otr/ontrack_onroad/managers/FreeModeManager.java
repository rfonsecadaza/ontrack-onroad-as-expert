package otr.ontrack_onroad.managers;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import otr.ontrack_onroad.dao.Notification;
import otr.ontrack_onroad.models.Stop;
import otr.ontrack_onroad.models.Trackable;
import otr.ontrack_onroad.parameters.ServiceKeys;

public class FreeModeManager {
    private static FreeModeManager instance;

    private ArrayList<Trackable> routeTrackables;

    public FreeModeManager() {
        routeTrackables = new ArrayList<Trackable>();
    }

    public boolean isPreroute() {
        return preroute;
    }

    public void setPreroute(boolean preroute) {
        this.preroute = preroute;
    }

    private boolean preroute = false;

    public static FreeModeManager getInstance() {
        if (instance == null) {
            instance = new FreeModeManager();
        }

        return instance;
    }

    public void setup() {

    }

    public void reset() {
        this.routeTrackables = new ArrayList<Trackable>();
    }

    public ArrayList<Trackable> getRouteTrackables() {
        return routeTrackables;
    }

    public int getNumberOfUnknown() {
        int numberOfUnknown = 0;
        for (Trackable trackable : routeTrackables) {
            if (trackable.getPicked() == Trackable.UNKNOWN) {

                numberOfUnknown++;

            }
        }

        return numberOfUnknown;
    }

    public Trackable getRouteTrackableById(String id) {
        for (Trackable trackable : routeTrackables) {
            if (trackable.getId().equals(id)) {
                return trackable;
            }
        }

        return null;
    }

    public void setAllTrackablesAsUnknown() {
        for (Trackable trackable : this.routeTrackables) {
            trackable.setPicked(Trackable.UNKNOWN);
        }
    }

    public void setAllTrackablesAsNotAtSchool() {
        for (Trackable trackable : this.routeTrackables) {
            trackable.setPicked(Trackable.NOT_AT_SCHOOL);
        }
    }

    public String getSelectedTrackablesJSONString() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        ArrayList<Trackable> selectedTrackables = new ArrayList<Trackable>();

        for (Trackable trackable : this.routeTrackables) {
            if (trackable.getPicked() == Trackable.UNKNOWN) {
                JSONObject jsonTrackable = new JSONObject();
                jsonTrackable.put(ServiceKeys.ID_KEY, "" + trackable.getId());

                // jsonStop.put("ESTIMATED_TIME",timestamp);
                jsonObject.accumulate(ServiceKeys.SELECTED_TRACKABLES_KEY, jsonTrackable);
                selectedTrackables.add(trackable);
            }
        }
        // Log.d("Opci�n 1", jsonObject.toString());
        // Log.d("Opci�n 2",
        // jsonObject.getString(ServiceKeys.NOTIFICATIONS_KEY));
        // Log.d("Opcion 3","{"+jsonObject.getString("TRACKED_STOPS")+"}");
        if (selectedTrackables.size() == 1)
            return "[" + jsonObject.getString(ServiceKeys.SELECTED_TRACKABLES_KEY) + "]";
        else if (selectedTrackables.size() == 0)
            return "[]";
        else
            return jsonObject.getString(ServiceKeys.SELECTED_TRACKABLES_KEY);
    }
}
