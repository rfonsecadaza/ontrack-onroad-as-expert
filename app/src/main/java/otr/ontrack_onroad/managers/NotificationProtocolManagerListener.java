package otr.ontrack_onroad.managers;

public interface NotificationProtocolManagerListener {
	public void onProtocolExecuted();
}
