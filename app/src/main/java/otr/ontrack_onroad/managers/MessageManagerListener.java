package otr.ontrack_onroad.managers;

import java.util.ArrayList;

import otr.ontrack_onroad.dao.Event;
import otr.ontrack_onroad.models.Report;

public interface MessageManagerListener {
	public void onReceiveMessage(ArrayList<Long> idsOfModifiedChatCards);
	public void onReceivePusherEvent(Event event);
	public void onReceivePusherReport(Report report);
	public void onMessagesRead(long chatCardId);
}
