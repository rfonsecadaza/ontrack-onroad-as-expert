package otr.ontrack_onroad.managers;

import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import otr.ontrack_onroad.models.Report;
import otr.ontrack_onroad.parameters.GeneralMessages;
import otr.ontrack_onroad.parameters.ServiceKeys;
import otr.ontrack_onroad.utils.WebRequestManager;
import android.os.AsyncTask;

public class ReportManager {
	
	private static ReportManager mInstance = null;
	
	private ArrayList<Report> reports;
	private ArrayList<Report> stackedReports;
	
	private ReportManager(){
		this.reports = new ArrayList<Report>();
		this.stackedReports = new ArrayList<Report>();
	}

	public static ReportManager getInstance() {
		if (mInstance == null) {
			mInstance = new ReportManager();
		}
		return mInstance;
	}

	public ArrayList<Report> getReports() {
		return reports;
	}

	public void setReports(ArrayList<Report> reports) {
		this.reports = reports;
	}
	
	public ArrayList<Report> getStackedReports() {
		return stackedReports;
	}

	public void setStackedReports(ArrayList<Report> stackedReports) {
		this.stackedReports = stackedReports;
	}

	public String getStackedReportsJSONString(String selectedFkKey) throws JSONException{
		JSONObject jsonObject = new JSONObject();
    	for(Report report: this.stackedReports){
    		JSONObject jsonReport = new JSONObject();
    		jsonReport.put(Report.DATE_GENERATED_KEY,report.getDate());
    		jsonReport.put(Report.FK_CATEGORY_KEY, report.getCategory());
    		jsonReport.put(Report.DESCRIPTION_KEY, report.getDescription());
    		jsonReport.put(Report.LATITUDE_KEY, report.getLatitude());
    		jsonReport.put(Report.LONGITUDE_KEY,report.getLongitude());
    		
    		
    			jsonReport.put(selectedFkKey, report.getFkDestination());
    		
			
    		jsonObject.accumulate(Report.REPORTS_KEY, jsonReport);
    	}
//    	Log.d("Opción 1", jsonObject.toString());
//    	Log.d("Opción 2", jsonObject.getString("EVENTS"));
//    	Log.d("Opcion 3","{"+jsonObject.getString("TRACKED_STOPS")+"}");
    	
    	if(stackedReports.size()==1)
    		return "["+jsonObject.getString(Report.REPORTS_KEY)+"]";
    	else
    		return jsonObject.getString(Report.REPORTS_KEY);
	}
	
	public void deleteStackedReports(){
		this.stackedReports.clear();
	}
	
	// AsyncTask para el envío de eventos generados en la ruta

		private class SendReportTask extends AsyncTask<String, Integer, String> {

			@Override
			protected String doInBackground(final String... args) {
				String result;
				try {
					result = WebRequestManager.executeTask(args);
					return result;
				} catch (IOException e) {
					return e.getMessage();
				}
			}

			@Override
			protected void onPostExecute(String result) {
				if (result.equals(GeneralMessages.HTTP_CONNECTION_ERROR)
						|| result.contains(GeneralMessages.HTTP_NOT_FOUND_ERROR)) {
					
				} else {

					int status;
					try {

						JSONObject jsonResult = new JSONObject(result);
						status = jsonResult.getInt(ServiceKeys.STATUS_KEY);
						if (status != 1) {
							String error = jsonResult.getString(ServiceKeys.ERROR_KEY);
//							listener.onAsyncTaskError(error);
						} else {
							EventManager.getInstance().deleteStackedEvents();
						}
					} catch (JSONException e) {
						e.printStackTrace();
//						listener.onAsyncTaskError(e.getMessage());
					}

				}
			}

		}

		public AsyncTask<String, Integer, String> executeSendReportTask(
				String... args) {
			SendReportTask task = new SendReportTask();
			return task.execute(args);
		}

}
