package otr.ontrack_onroad.managers;

import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import otr.ontrack_onroad.dao.Event;
import otr.ontrack_onroad.parameters.GeneralMessages;
import otr.ontrack_onroad.parameters.ServiceKeys;
import otr.ontrack_onroad.utils.WebRequestManager;
import android.os.AsyncTask;
import android.util.Log;

public class EventManager {

    private static EventManager mInstance = null;

    private static ArrayList<EventManagerListener> listeners;

    private ArrayList<Event> events;
    private ArrayList<Event> stackedEvents;



    private Event lastAnnouncement = null;

    private EventManager() {
        this.setEvents(new ArrayList<Event>());
        this.setStackedEvents(new ArrayList<Event>());
        this.listeners = new ArrayList<EventManagerListener>();
    }

    public static EventManager getInstance() {
        if (mInstance == null) {
            mInstance = new EventManager();
        }
        return mInstance;
    }

    public ArrayList<Event> getEvents() {
        return events;
    }

    public void setEvents(ArrayList<Event> events) {
        this.events = events;
    }

    public ArrayList<Event> getStackedEvents() {
        return stackedEvents;
    }

    public void setStackedEvents(ArrayList<Event> stackedEvents) {
        this.stackedEvents = stackedEvents;
    }

    public String getStackedEventsJSONString(String selectedFkKey)
            throws JSONException {
        JSONObject jsonObject = new JSONObject();
        for (Event event : this.stackedEvents) {
            JSONObject jsonEvent = new JSONObject();
            jsonEvent.put(Event.DATE_GENERATED_KEY, event.getDate());
            jsonEvent.put(Event.FK_CATEGORY_KEY, event.getCategory());
            jsonEvent.put(Event.DESCRIPTION_KEY, event.getDescription());
            jsonEvent.put(Event.DESTINATION_ROLE_KEY,
                    event.getDestinationRole());
            jsonEvent.put(Event.COPY_COORDINATOR_KEY,
                    "" + event.getCopyCoordinator());

            jsonEvent.put(selectedFkKey, event.getFkDestination());

            jsonObject.accumulate(ServiceKeys.EVENTS_KEY, jsonEvent);
        }
        // Log.d("Opción 1", jsonObject.toString());
        // Log.d("Opción 2", jsonObject.getString("EVENTS"));
        // Log.d("Opcion 3","{"+jsonObject.getString("TRACKED_STOPS")+"}");

        if (stackedEvents.size() == 1)
            return "[" + jsonObject.getString(ServiceKeys.EVENTS_KEY) + "]";
        else
            return jsonObject.getString(ServiceKeys.EVENTS_KEY);
    }

    public void deleteStackedEvents() {
        this.stackedEvents.clear();
    }

    // AsyncTask para el envío de eventos generados en la ruta

    private class SendEventTask extends AsyncTask<String, Integer, String> {

//        long startTime;
//        long endTime;

        @Override
        protected String doInBackground(final String... args) {
//            String eventInfo = args[0]+"\n";
//            for(int i=1; i<args.length-1;i+=2){
//                eventInfo += args[i] + ": "+args[i+1]+"\n";
//            }
//            Log.d("eventInfo", eventInfo);

//            startTime = System.currentTimeMillis();
            String result;
            try {
                result = WebRequestManager.executeTaskGet(args);
                return result;
            } catch (IOException e) {
                return GeneralMessages.HTTP_NOT_FOUND_ERROR;
            }
        }

        @Override
        protected void onPostExecute(String result) {
            if (result.equals(GeneralMessages.HTTP_CONNECTION_ERROR)
                    || result.contains(GeneralMessages.HTTP_NOT_FOUND_ERROR)) {

            } else {

//                endTime = System.currentTimeMillis();
//
//                long difference = (endTime - startTime)/1000;
//                Log.d("eventInfo", "Diferencia: "+difference);
                int status;
                try {

                    JSONObject jsonResult = new JSONObject(result);
                    status = jsonResult.getInt(ServiceKeys.STATUS_KEY);
                    if (status != 1) {
                        String error = jsonResult
                                .getString(ServiceKeys.ERROR_KEY);
                        // listener.onAsyncTaskError(error);
                    } else {
                        EventManager.getInstance().deleteStackedEvents();
                    }
                    // OnTrackSystem.getInstance().updateEvents();
                    // listener.onEventGenerated();

                } catch (JSONException e) {
                    e.printStackTrace();
                    // listener.onAsyncTaskError(e.getMessage());
                }

            }
        }

    }

    public void addListener(EventManagerListener listener){
        listeners.add(listener);
    }

//	public AsyncTask<String, Integer, String> executeSendEventTask(
//			String... args) {
//		SendEventTask task = new SendEventTask();
//		return task.execute(args);
//	}

    public void executeSendEventTask(String...args){
        SendEventThread task = new SendEventThread(args);
        task.start();
    }

    public void notifyEventGenerated(long id) {
        for (EventManagerListener listener : listeners) {
            listener.onEventGenerated(id);
        }
    }

    private class SendEventThread extends Thread implements Runnable{

//        long startTime;
//        long endTime;

        private String[] params;
        private String result;

        public SendEventThread(String... params) {
            this.params = params;
        }

        @Override
        public void run() {
            String eventInfo = params[0]+"\n";
            for(int i=1; i<params.length-1;i+=2){
                eventInfo += params[i] + ": "+params[i+1]+"\n";
            }
//            Log.d("eventInfo", eventInfo);

//            startTime = System.currentTimeMillis();
            try {
                result = WebRequestManager.executeTaskGet(params);
            } catch (IOException e) {
                result =  GeneralMessages.HTTP_NOT_FOUND_ERROR;
            }
            postExecute();
        }

        public void postExecute() {
            if (result.equals(GeneralMessages.HTTP_CONNECTION_ERROR)
                    || result.contains(GeneralMessages.HTTP_NOT_FOUND_ERROR)) {

            } else {

//                endTime = System.currentTimeMillis();

//                long difference = (endTime - startTime)/1000;
//                Log.d("eventInfo", "Diferencia: "+difference);
                int status;
                try {

                    JSONObject jsonResult = new JSONObject(result);
                    status = jsonResult.getInt(ServiceKeys.STATUS_KEY);
                    if (status != 1) {
                        String error = jsonResult
                                .getString(ServiceKeys.ERROR_KEY);
                        // listener.onAsyncTaskError(error);
                    } else {
                        EventManager.getInstance().deleteStackedEvents();
                    }
                    // OnTrackSystem.getInstance().updateEvents();
                    // listener.onEventGenerated();

                } catch (JSONException e) {
                    e.printStackTrace();
                    // listener.onAsyncTaskError(e.getMessage());
                }

            }
        }

    }

    public Event getLastAnnouncement() {
        return lastAnnouncement;
    }

    public void setLastAnnouncement(Event lastAnnouncement) {
        this.lastAnnouncement = lastAnnouncement;
    }



    public void removeListener(EventManagerListener listener){
        listeners.remove(listener);
    }

}
