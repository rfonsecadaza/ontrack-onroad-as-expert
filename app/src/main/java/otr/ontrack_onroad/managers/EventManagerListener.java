package otr.ontrack_onroad.managers;

public interface EventManagerListener {
	public void onEventGenerated(long id);
}
