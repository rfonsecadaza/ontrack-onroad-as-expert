package otr.ontrack_onroad.managers;

import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONException;

import otr.ontrack_onroad.parameters.GeneralMessages;
import otr.ontrack_onroad.parameters.PreferenceKeys;
import otr.ontrack_onroad.parameters.ServiceKeys;
import otr.ontrack_onroad.utils.JSONManager;
import otr.ontrack_onroad.utils.Timestamp;
import otr.ontrack_onroad.utils.WebRequestManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

public class NotificationProtocolManager {
	// Obtiene todas las notificaciones nuevas desde una fecha determinada
	public static final String URL_BASE_GET_NOTIFICATIONS = "ApiNotification/getNotifications";

	// Envï¿½a la informaciï¿½n de notificaciones recibidas por pusher
	public static final String URL_BASE_ACK_NOTIFICATIONS = "ApiNotification/ackNotifications";

	// Entidad exclusiva para el manejo del protocolo de notificaciones
	private static NotificationProtocolManager mInstance = null;

	private static ArrayList<NotificationProtocolManagerListener> listeners;

	private NotificationProtocolManager() {
		listeners = new ArrayList<NotificationProtocolManagerListener>();
	}

	public static NotificationProtocolManager getInstance() {
		if (mInstance == null) {
			mInstance = new NotificationProtocolManager();
		}
		return mInstance;
	}

	public void addListener(NotificationProtocolManagerListener listener) {
		listeners.add(listener);
	}
	
	public void removeListener(NotificationProtocolManagerListener listener){
		this.listeners.remove(listener);
	}


	private static class NotificationProtocolTask extends
			AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(final String... args) {
			String result;
			try {
				result = WebRequestManager.executeTask(args);
				return result;
			} catch (IOException e) {
				return e.getMessage();
			}
		}

		@Override
		protected void onPostExecute(String result) {
			if (result.equals(GeneralMessages.HTTP_CONNECTION_ERROR)
					|| result.contains(GeneralMessages.HTTP_NOT_FOUND_ERROR)) {
				OnTrackManager.getInstance().getListener()
						.onAsyncTaskError(result);
			} else {
				RequestNewNotificationsTask task = new RequestNewNotificationsTask();
				SharedPreferences prefs = OnTrackManager
						.getInstance()
						.getContext()
						.getSharedPreferences(PreferenceKeys.PREFERENCES_KEY,
								Context.MODE_PRIVATE);
				String date = prefs.getString(
						PreferenceKeys.LAST_REQUEST_DATE_KEY, "");

				task.execute(URL_BASE_GET_NOTIFICATIONS, ServiceKeys.TOKEN_KEY,
						OnTrackManager.getInstance().getToken(),
						ServiceKeys.DATE_KEY, date);
				SharedPreferences.Editor editor = prefs.edit();
				editor.putString(PreferenceKeys.LAST_REQUEST_DATE_KEY,
						Timestamp.completeTimestamp());
				editor.commit();

			}
		}

	}

	public static void executeNotificationProtocolTask(String... params) {
		NotificationProtocolTask task = new NotificationProtocolTask();
		task.execute(params);
	}

	private static class RequestNewNotificationsTask extends
			AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(final String... args) {
			String result;
			try {
				result = WebRequestManager.executeTaskGet(args);
				JSONManager.loadNotifications(result);
				return result;
			} catch (IOException e) {
				return e.getMessage();
			} catch (JSONException e) {
				e.printStackTrace();
				return GeneralMessages.JSON_ERROR;
			}
		}

		@Override
		protected void onPostExecute(String result) {
			if (result.equals(GeneralMessages.HTTP_CONNECTION_ERROR)
					|| result.contains(GeneralMessages.HTTP_NOT_FOUND_ERROR)
					|| result.equals(GeneralMessages.JSON_ERROR)) {
				

			} else {
				
				for(NotificationProtocolManagerListener listener: listeners)
					listener.onProtocolExecuted();
				OnTrackManager.getInstance().deleteNotificationsFromLocalDB();
			}
		}

	}

	public static void executeRequestNewNotificationsTask(String... params) {
		RequestNewNotificationsTask task = new RequestNewNotificationsTask();
		task.execute(params);
	}

	public static void executeRequestNewNotificationsTask() {
		SharedPreferences prefs = OnTrackManager
				.getInstance()
				.getContext()
				.getSharedPreferences(PreferenceKeys.PREFERENCES_KEY,
						Context.MODE_PRIVATE);
		String date = prefs.getString(PreferenceKeys.LAST_REQUEST_DATE_KEY, "");

		executeRequestNewNotificationsTask(URL_BASE_GET_NOTIFICATIONS,
				ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance().getToken(),
				ServiceKeys.DATE_KEY, date);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(PreferenceKeys.LAST_REQUEST_DATE_KEY,
				Timestamp.completeTimestamp());
		editor.commit();
	}
}
