package otr.ontrack_onroad.managers;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.json.JSONException;

import otr.ontrack_onroad.dao.Event;
import otr.ontrack_onroad.models.Route;
import otr.ontrack_onroad.parameters.GeneralMessages;
import otr.ontrack_onroad.parameters.ServiceKeys;
import otr.ontrack_onroad.threads.SendLocationThread;
import otr.ontrack_onroad.utils.Timestamp;

public class FreeModeEventsManager
{
    private final static long SEND_LOCATION_TIMEOUT = 9000;

    private static FreeModeEventsManager instance;

    private Route route;

    public FreeModeEventsManager( )
    {

    }

    public static FreeModeEventsManager getInstance( )
    {
        if( instance == null )
        {
            instance = new FreeModeEventsManager( );
        }

        return instance;
    }

    public void setup( Route route )
    {
        this.route = route;
    }

//    public void sendRouteStartedEvent( )
//    {
//        try {
//            OnTrackManagerAsyncTasks.executeRequestFootprintTask2( OnTrackManagerAsyncTasks.URL_BASE_FOOTPRINT, ServiceKeys.FK_ROUTE_KEY, route.getId( ), ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance( ).getToken( ), ServiceKeys.DATE_KEY,
//                    Timestamp.completeTimestamp( ), ServiceKeys.SELECTED_TRACKABLES_KEY, FreeModeManager.getInstance().getSelectedTrackablesJSONString() );
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }

    public void sendTrackedPoints( )
    {

        // ACTUALIZACIÓN: Ahora se intentará enviar la localización usando Threads
        ExecutorService executor = Executors.newSingleThreadExecutor();

        try {
            executor.submit(new SendLocationThread(OnTrackManagerAsyncTasks.URL_BASE_TRACKED_POINT, ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance().getToken(), ServiceKeys.TRACKED_POINTS_KEY,
                    FreeModeLocationManager.getInstance().getStackedPointsJSONString())).get(SEND_LOCATION_TIMEOUT, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ExecutionException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (TimeoutException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        executor.shutdown();
    }

    public void sendRouteEndedEvent( )
    {
        Event event = new Event( null, Timestamp.completeTimestamp( ), Event.END_ROUTE_EVENT_CODE, GeneralMessages.ROUTE_HAS_FINISHED, 0, 1, "" + OnTrackManager.getInstance( ).getUser( ).getFootprint( ), OnTrackManager.getInstance( ).getUser( )
                .getRoute( ).getId( ), false );

        try
        {
            EventManager.getInstance( ).executeSendEventTask( OnTrackManagerAsyncTasks.URL_BASE_ADD_EVENTS, ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance( ).getToken( ), ServiceKeys.EVENTS_KEY, event.getEventJSONString( Event.FK_FOOTPRINT_KEY ) );
        }
        catch( JSONException e )
        {
            e.printStackTrace( );
        }
    }
}
