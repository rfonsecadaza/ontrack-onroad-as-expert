package otr.ontrack_onroad.threads;

import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;

import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.parameters.GeneralMessages;
import otr.ontrack_onroad.parameters.ServiceKeys;
import otr.ontrack_onroad.utils.WebRequestManager;

public class SendLocationThread extends Thread implements Runnable{

	private String[] params;
	private String result;

	public SendLocationThread(String... params) {
		this.params = params;
	}

	@Override
	public void run() {
		try {
			result = WebRequestManager.executeTask(this.params);
		} catch (IOException e) {
			result = GeneralMessages.HTTP_NOT_FOUND_ERROR;
		}
		postExecute();
	}

	public void postExecute() {

		if (result.contains(GeneralMessages.HTTP_NOT_FOUND_ERROR)) {
			OnTrackManager.getInstance().getListener().onAsyncTaskError(result);
		} else {

			int status;
			try {

				JSONObject jsonResult = new JSONObject(result);
				status = jsonResult.getInt(ServiceKeys.STATUS_KEY);
				if (status != 1) {
					String error = jsonResult.getString(ServiceKeys.ERROR_KEY);
					OnTrackManager.getInstance().getListener()
							.onAsyncTaskError(error);
				} else {
					OnTrackManager.getInstance().deleteStackedPoints();
				}
			} catch (JSONException e) {
				e.printStackTrace();
				OnTrackManager.getInstance().getListener()
						.onAsyncTaskError(e.getMessage());
			}

		}
	}

}
