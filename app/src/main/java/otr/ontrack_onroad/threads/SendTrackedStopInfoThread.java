package otr.ontrack_onroad.threads;

import java.io.IOException;

import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.parameters.GeneralMessages;
import otr.ontrack_onroad.utils.WebRequestManager;

/**
 * Created by rodrigoivanf on 26/10/2015.
 */
public class SendTrackedStopInfoThread extends Thread{
    private boolean isNewDistanceFlag;
    private String[] params;
    private String result;

    public SendTrackedStopInfoThread(boolean isNewDistanceFlag, String... params){
        this.isNewDistanceFlag = isNewDistanceFlag;
        this.params = params;
    }

    @Override
    public void run(){
        try {
            result = WebRequestManager.executeTask(params);
//				Log.d("result",result);
        } catch (IOException e) {
            result = e.getMessage();
        }
        postExecute();
    }


    public void postExecute(){
        try
        {
            if( result.equals( GeneralMessages.JSON_ERROR )
                    || result.equals( GeneralMessages.HTTP_CONNECTION_ERROR )
                    || result.contains( GeneralMessages.HTTP_NOT_FOUND_ERROR ) )
            {
                OnTrackManager.getInstance().getListener( )
                        .onAsyncTaskError( result );
            }
            else
            {
                if( this.isNewDistanceFlag )
                    OnTrackManager.getInstance( ).increaseDistanceFlag( );
            }
        }
        catch( Exception e )
        {
            OnTrackManager.getInstance( ).getListener( )
                    .onAsyncTaskError( "Error enviando la información de los paraderos" );
        }
    }

}
