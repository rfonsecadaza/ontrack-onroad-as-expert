package otr.ontrack_onroad.threads;

import java.io.IOException;

import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.parameters.GeneralMessages;
import otr.ontrack_onroad.utils.WebRequestManager;

/**
 * Created by rodrigoivanf on 10/11/2015.
 */
public class SendFTStatesThread extends Thread{
    private String[] params;
    private String result;

    public SendFTStatesThread(String... params) {
        this.params = params;
    }

    @Override
    public void run(){
        try {
            result = WebRequestManager.executeTask(params);
        } catch (IOException e) {
            result = GeneralMessages.HTTP_NOT_FOUND_ERROR;
        }
        postExecute();
    }

    public void postExecute(){
        if (result.contains(GeneralMessages.HTTP_NOT_FOUND_ERROR)) {
            OnTrackManager.getInstance().getListener()
                    .onAsyncTaskError(result);
        } else {
            // Acciones por decidir, en este caso
            // no se sabe cómo se enviará la información de los footprint
            // trackables por pusher todavía
            OnTrackManager.getInstance().increaseTrackableFlag();
        }
    }
}
