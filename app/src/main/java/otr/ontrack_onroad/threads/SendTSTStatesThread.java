package otr.ontrack_onroad.threads;


import java.io.IOException;

import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.parameters.GeneralMessages;
import otr.ontrack_onroad.utils.WebRequestManager;

/**
 * Created by rodrigoivanf on 24/09/2015.
 */
public class SendTSTStatesThread extends Thread implements Runnable {
    //        long startTime;
//        long endTime;

    private String[] params;
    private String result;

    public SendTSTStatesThread(String... params) {
        this.params = params;
    }

    @Override
    public void run() {
        try {
            result = WebRequestManager.executeTask(params);
        }  catch (IOException e) {
            result = GeneralMessages.HTTP_CONNECTION_ERROR;
        }
        postExecute();
    }

    public void postExecute() {
        if (result.equals(GeneralMessages.JSON_ERROR)
                || result.equals(GeneralMessages.HTTP_CONNECTION_ERROR)
                || result.contains(GeneralMessages.HTTP_NOT_FOUND_ERROR)) {
            OnTrackManager.getInstance().getListener()
                    .onAsyncTaskError(result);
        } else {
            // timeFlag avisa que ya estï¿½ lista la informaciï¿½n de los
            // tracked stops en el paradero actual
            if (!OnTrackManager.getInstance().isRouteFinished()) {

                OnTrackManager.getInstance().setTimeFlag(
                        OnTrackManager.getInstance().getCurrentStop()
                                .getOrder());
            } else {
                OnTrackManager.getInstance().getListener().onLastInfoSent();
                OnTrackManager.getInstance().resetRoute();
            }
        }
    }
}
