package otr.ontrack_onroad.parameters;

public class PusherParameters {
	public static final String API_KEY = "d79ba421a8f61676988b";
	
	public static final String EVENT_NEW_MESSAGE = "NEW_MESSAGE";
	public static final String EVENT_NEW_EVENT = "NEW_EVENT";
	public static final String EVENT_NEW_REPORT_RESPONSE = "NEW_REPORT_RESPONSE";
}
