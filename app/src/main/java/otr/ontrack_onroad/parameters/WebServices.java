package otr.ontrack_onroad.parameters;

public abstract class WebServices
{
    public final static String URL_GET_ROUTES_LIST = "ApiRouteBuilder/listRoutes";
    
    public final static String URL_CREATE_ROUTE = "ApiRouteBuilder/createNewRoute";
    
    public final static String URL_SEND_STOPS_INFO = "ApiRouteBuilder/createNewStops";
    
    public final static String URL_ORGANIZATION_INFO = "ApiRouteBuilder/basicInformationForRoute";
    
    public final static String URL_SET_DRIVER = "ApiRouteBuilder/setDriverOfRoute";
    
    public final static String URL_SET_MONITOR = "ApiRouteBuilder/setMonitorOfRoute";
    
    public final static String URL_SET_VEHICLE = "ApiRouteBuilder/setVehicleOfRoute";
    
    public final static String URL_SET_SCHEDULE = "ApiRouteBuilder/setScheduleOfRoute";
    
    public final static String URL_GET_TRACKABLES = "ApiRouteBuilder/listTrackables";
    
    public final static String URL_ASSIGN_TRACKABLE_TO_ROUTE = "ApiRouteBuilder/setStudentToRoute";
    
    public final static String URL_ASSIGN_TRACKABLE_TO_STOP = "ApiRouteBuilder/setStudentToStop";
    
    public final static String URL_REMOVE_TRACKABLE_FROM_ROUTE = "ApiRouteBuilder/deleteStudentFromRoute";

    public final static String URL_REMOVE_TRACKABLE_FROM_STOP = "ApiRouteBuilder/deleteStudentFromStop";
    
    public final static String URL_ROUTE_TRACKABLES = "ApiRouteBuilder/getStudentsOfRoute";
    
    public final static String URL_REMOVE_ROUTE = "ApiRouteBuilder/deleteRoute";
    
    public final static String URL_CHANGE_ROUTE_TYPE = "ApiRouteBuilder/changeRouteType";
    
    public final static String URL_CHANGE_STOP_TYPE = "ApiRouteBuilder/changeStopType";
    
    public final static String URL_GET_TRACKABLES_BY_LETTER = "ApiRouteBuilder/listTrackablesAlphabetic";
    
    public final static String URL_GET_TRACKABLES_BY_KEY_WORD = "ApiRouteBuilder/searchTrackablesByKeywords";
    
    public final static String URL_CREATE_VEHICLE = "ApiRouteBuilder/createVehicle";
    
    public final static String URL_EDIT_VEHICLE = "ApiRouteBuilder/updateVehicle";

    public final static String URL_CREATE_TRACKABLE = "ApiRouteBuilder/createTrackable";

    public final static String URL_EDIT_TRACKABLE = "ApiRouteBuilder/updateTrackable";
    
    public final static String URL_CREATE_SCHEDULE = "ApiRouteBuilder/createSchedule";

    public final static String URL_EDIT_SCHEDULE = "ApiRouteBuilder/updateSchedule";
    
    public final static String URL_CREATE_USER = "ApiRouteBuilder/createUser";

    public final static String URL_EDIT_USER = "ApiRouteBuilder/updateUser";
    
    public final static String URL_CHANGE_PROFILE_PICTURE_BY_TYPE = "ApiRouteBuilder/changeProfilePictureByType";
    
    public final static String URL_REMOVE_ELEMENT = "ApiRouteBuilder/deleteEntity";
    
    public final static String URL_ADD_NEW_EXCEPTION = "ApiExceptions/addNewException";
    
    public final static String URL_GET_CURRENT_APP_VERSION = "ApiVersion/currentVersion";

    public final static String URL_UPDATE_STOP_LOCATION = "ApiTracker/updateStopLocation";
    
    public final static String URL_ADD_STOP = "ApiTracker/addStop";

    public final static String URL_CHECK_TRACKABLE_REMOVAL_FROM_STOP = "ApiTracker/CheckTrackableRemovalFromStop";
    
    public final static String URL_GET_ALL_ROUTES_FOR_ORGANIZATION = "ApiServices/GetCompleteRouteListForDriver";

    public final static String URL_CHANGE_STOP = "ApiChange/CreateNewStopChanges";

    public final static String URL_CHANGE_STOP_TRACKABLES = "ApiChange/CreateNewTrackableChanges";
}