package otr.ontrack_onroad.parameters;

public class Parameters {

	//Mapbox
	public static final String MAP_BOX_TOKEN = "pk.eyJ1Ijoib250cmFja2NvbG9tYmlhIiwiYSI6IjRkNzJjN2U0MzgyNjA3NjUyYTNhNWYzYjcxZTlmZDM4In0.d7Iwk36U81wivsVs735PqA";

	// GCM
	public static final String GCM_SENDER_ID = "871652510934";
	public static final String GCM_MESSAGE_TAG = "Mensaje de GCM";

	// Status de salida cuando se hace un llamado a servicio web
	public static final int STATUS_CORRECT = 1;
	public static final int STATUS_INCORRECT = 0;
	public static final int STATUS_ERROR = -1;

	// Sistemas operativos mï¿½viles
	public static final String BRAND_ANDROID = "0";

	// Tipos de usuario
	public static final String USER_TYPE_DRIVER = "0";

	// Fuentes de letra
	public static final String FONT_TITLE = "fonts/fjalla_one.ttf";
	public final static String LATO_FONT = "fonts/Lato-Regular.ttf";
	public final static String LATO_FONT_BOLD = "fonts/Lato-Bold.ttf";

	// Simulaciones
	public static final String SIMULATIONS_DIALOG_TAG = "simulations";
	public static final String RECORD_SIMULATION_DIALOG_TAG = "record";
	public static final String REPORT_DIALOG_TAG = "report";
	public static final String CLOSE_SESSION_DIALOG_TAG = "closesession";
	public static final String WRONG_ROUTE_DIALOG_TAG = "wrongroute";
	public static final String CLOSEAPP_DIALOG_TAG = "closeapp";
	public static final String CANCELSTOP_DIALOG_TAG = "cancelapp";
	public static final String CONFIRMDELIVERYSTART_DIALOG_TAG = "confirmdeliverystart";
	public static final String CONFIRMPREROUTE_DIALOG_TAG = "confirmpreroute";
	public static final String ASSISTANCE_DIALOG_TAG = "assistance";

	// Menï¿½ lateral
	public static final int OPTION_ROUTE = 1;
	public static final int OPTION_SOFT_CHAT = 1;
	public static final int OPTION_SOFT_CLOSE_SESSION = 2;
	public static final int OPTION_EVENTS = 2;
	public static final int OPTION_REPORTS = 2;
	public static final int OPTION_CHAT = 3;
	public static final int OPTION_CONTACT = 4;
	public static final int OPTION_CLOSE_SESSION = 5;

	public static final String OPTION_ROUTE_TEXT = "Mi ruta";
	public static final String OPTION_REPORTS_TEXT = "Reportes";
	public static final String OPTION_CHAT_TEXT = "Mensajería";
	public static final String OPTION_CONTACT_TEXT = "Contacto";
	public static final String OPTION_CLOSE_SESSION_TEXT = "Cerrar sesión";

	// Rutas
	public static final String ROUTE_ACTION_BAR_TITLE = "Ruta";
	public static final String ROUTE_ACTION_BAR_IN_PROGRESS = "En progreso";
	public static final String ROUTE_DELIVERY_ORIGIN = "Institución educativa";
	public static final String ROUTE_PICK_UP_ORIGIN = "Terminal de vehículos";
	public static final String ROUTE_PICK_UP_DESTINATION = "Institución educativa";

	// Paraderos
	public static final String STOP_MARKER_TITLE = "Paradero";
	public static final String STOP_MARKER_MINUTES = "min";
	

	// Posiciï¿½n
	public static final String VEHICLE_MARKER_TITLE = "Bus";
	
	// Mensajerï¿½a
	public static final String ATTENTION_TITLE = "Atención";
	public static final int CURRENT_USER_LOCALDB_ID = -1;
	
	// Reportes
	public static final String REPORT_NO_DETAILS = "Sin detalles";
	
	// Notificaciones
	public static final int NOTIFICATION_TYPE_MESSAGE = 0;
	public static final int NOTIFICATION_TYPE_EVENT = 1;
	
	//Package Name
	public final static String APP_PACKAGE_NAME = "otr.ontrack_onroad.activities";
}
