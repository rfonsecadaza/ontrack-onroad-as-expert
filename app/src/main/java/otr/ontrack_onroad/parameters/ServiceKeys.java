package otr.ontrack_onroad.parameters;

public class ServiceKeys {

	// Respuesta general de los JSON
	public static final String STATUS_KEY = "STATUS";
	public static final String ERROR_KEY = "ERROR";

	// Generales
	public static final String ID_KEY = "ID";
	public static final String FK_ROUTE_KEY = "FK_ROUTE";
	public static final String FK_USER_KEY = "FK_USER";
	public static final String FK_STOP_KEY = "FK_STOP";
	public static final String DATE_KEY = "DATE";
	public static final String DATE_REPORTED_KEY = "DATE_REPORTED";
	public static final String USER_KEY = "USER";
	public static final String NAME_KEY = "NAME";
	public static final String IMAGE_KEY = "IMAGE";
	public static final String URL_IMAGE_KEY = "URL_IMAGE";
	public static final String ID_ROUTE_KEY = "ID_ROUTE";
	public static final String FOOTPRINT_KEY = "FOOTPRINT";
	public static final String ID_TRACKABLE_KEY = "ID_TRACKABLE";
	public static final String ID_STOP_KEY = "ID_STOP";
	public static final String ID_OLD_STOP_KEY = "ID_OLD_STOP";
	public static final String STOP_CHANGES_KEY = "STOP_CHANGES";
	public static final String TRACKABLE_CHANGES_KEY = "TRACKABLE_CHANGES";

	public static final String FK_TRACKED_STOP_KEY = "FK_TRACKED_STOP";
	public static final String FK_ORGANIZATION_KEY = "FK_ORGANIZATION";
	public static final String FK_TRACKED_STOP_TRACKABLE_KEY = "FK_TRACKED_STOP_TRACKABLE";
	public static final String FK_FOOTPRINT_KEY = "FK_FOOTPRINT";
	public static final String FK_USER_DEVICE_KEY = "FK_USER_DEVICE";
	public static final String FK_TRACKABLE_KEY = "FK_TRACKABLE";

	public static final String PAGE_KEY = "PAGE";
	public static final String QUANTITY_KEY = "QUANTITY";
	public static final String LETTER_KEY = "LETTER";
	public static final String TOTAL_PAGES_KEY = "TOTAL_PAGES";
	public static final String NUMBER_TRACKABLES_KEY = "NUMBER_TRACKABLES";
	public static final String KEYWORDS_KEY = "KEYWORDS";
	public static final String VERSION_KEY = "VERSION";
	public static final String REMOVE_KEY = "REMOVE";

	// Flags
	public static final String TIME_FLAG_KEY = "TIME_FLAG";
	public static final String DISTANCE_FLAG_KEY = "DISTANCE_FLAG";
	public static final String STOP_FLAG_KEY = "STOP_FLAG";
	public static final String TIME_DELAYED_KEY = "TIME_DELAYED";
	public static final String TRACKABLE_FLAG_KEY = "TRACKABLE_FLAG";


	// Autenticaciï¿½n
	public static final String BRAND_KEY = "BRAND";
	public static final String MODEL_KEY = "MODEL";
	public static final String DEVICE_KEY = "DEVICE";
	public static final String DEVICE_ID_KEY = "DEVICE_ID";
	public static final String DEVICE_TOKEN_KEY = "DEVICE_TOKEN";
	public static final String TYPE_KEY = "TYPE";
	public static final String ID_USER_KEY = "ID_USER";
	public static final String PASSWORD_KEY = "PASSWORD";
	public static final String TOKEN_KEY = "TOKEN";

	// Notificaciones
	public static final String ID_NOTIFICATION_KEY = "ID_NOTIFICATION";
	public static final String NOTIFICATION_KEY = "NOTIFICATION";
	public static final String NOTIFICATIONS_KEY = "NOTIFICATIONS";
	public static final String TITLE_KEY = "TITLE";
	public static final String DETAIL_KEY = "DETAIL";
	public static final String DATE_ACK_KEY = "DATE_ACK";

	// user_device
	public static final String USER_DEVICE_ID_KEY = "USER_DEVICE_ID";

	// Footprint
	public static final String NEW_FOOTPRINT_KEY = "NEW_FOOTPRINT";
	public static final String ID_FOOTPRINT_KEY = "ID_FOOTPRINT";

	// Eventos
	public static final String EVENT_KEY = "EVENT";
	public static final String EVENTS_KEY = "EVENTS";
	public static final String EVENT_USER_KEY = "EVENT_USER";
	public static final String FK_EVENT_USER_KEY = "FK_EVENT_USER";
	public static final String DESCRIPTION_KEY = "DESCRIPTION";
	public static final String CATEGORY_KEY = "CATEGORY";
	public static final String COPY_COORDINATOR_KEY = "COPY_COORDINATOR";
	public static final String DESTINATION_ROLE_KEY = "DESTINATION_ROLE";

	// Reports
	public static final String REPORTS_KEY = "REPORTS";

	// Mensajerï¿½a
	public static final String CHATCARDS_KEY = "CHATCARDS";

	public static final String MESSAGE_KEY = "MESSAGE";
	public static final String DATE_WROTE_KEY = "DATE_WROTE";
	public static final String DATE_SERVER_KEY = "DATE_SERVER";
	public static final String MESSAGES_KEY = "MESSAGES";
	public static final String MESSAGE_RECIPIENT_CHATCARD_KEY = "MESSAGE_RECIPIENT_CHATCARD";
	public static final String FK_MESSAGE_RECIPIENT_CHATCARD_KEY = "FK_MESSAGE_RECIPIENT_CHATCARD";
	public static final String SENDER_USER_KEY = "SENDER_USER";
	public static final String INDIVIDUAL_CHATCARD_KEY = "INDIVIDUAL_CHATCARD";
	public static final String MESSAGE_SENDER_CHATCARDS_KEY = "MESSAGE_SENDER_CHATCARDS";
	public static final String LAST_SEEN_KEY = "LAST_SEEN";
	public static final String GROUP_KEY = "GROUP";
	public static final String ROUTE_KEY = "ROUTE";

	// Tracked points
	public static final String TRACKED_POINTS_KEY = "TRACKED_POINTS";
	public static final String LONGITUDE_KEY = "LONGITUDE";
	public static final String LATITUDE_KEY = "LATITUDE";
	public static final String ALTITUDE_KEY = "ALTITUDE";
	public static final String BEARING_KEY = "BEARING";
	public static final String ACCURACY_KEY = "ACCURACY";
	public static final String SPEED_KEY = "SPEED";
	public static final String DISTANCE_COVERED_KEY = "DISTANCE_COVERED";
	public static final String POSITION_KEY = "POSITION";

	public static final String STACKED_POINTS_KEY = "STACKED_POINTS";

	// Tracked stops
	public static final String REAL_TIME_KEY = "REAL_TIME";
	public static final String TRACKED_STOPS_KEY = "TRACKED_STOPS";
	public static final String ESTIMATED_DISTANCE_KEY = "ESTIMATED_DISTANCE";
	public static final String ESTIMATED_TIME_KEY = "ESTIMATED_TIME";
	public static final String DEPARTURE_TIME_KEY = "DEPARTURE_TIME";
	public static final String TRACKEDSTOPTRACKABLES_KEY = "TRACKEDSTOPTRACKABLES";
	public static final String TRACKED_STOP_TRACKABLES_KEY = "TRACKED_STOP_TRACKABLES";
	public static final String FOOTPRINTTRACKABLES_KEY = "FOOTPRINTTRACKABLES";
	public static final String TRACKED_STOP_STEPS_KEY = "TRACKED_STOP_STEPS";

	public static final String PICKED_KEY = "PICKED";
	public static final String STATE_KEY = "STATE";
	public static final String SPECIAL_KEY = "SPECIAL";

	// Organizations
	public final static String ID_ORGANIZATION_KEY = "ID_ORGANIZATION";

	// Drivers
	public final static String ID_DRIVER_KEY = "ID_DRIVER";

	// Monitors
	public final static String ID_MONITOR_KEY = "ID_MONITOR";

	// Vehicles
	public final static String ID_VEHICLE_KEY = "ID_VEHICLE";

	// Schedules
	public final static String ID_SCHEDULE_KEY = "ID_SCHEDULE";

	// Google
	public final static String GOOGLE_ROUTES_KEY = "routes";

	public final static String GOOGLE_LEGS_KEY = "legs";

	public final static String GOOGLE_STEPS_KEY = "steps";

	public final static String GOOGLE_POLYLINE_KEY = "polyline";

	public final static String GOOGLE_POINTS_KEY = "points";

	public final static String GOOGLE_DURATION_KEY = "duration";

	public final static String GOOGLE_VALUE_KEY = "value";

	public final static String GOOGLE_DISTANCE_KEY = "distance";

	public final static String GOOGLE_END_ADDRESS_KEY = "end_address";

	public final static String GOOGLE_START_ADDRESS_KEY = "start_address";

	// Exceptions
	public final static String EXCEPTION_KEY = "EXCEPTION";

	public final static String APP_VERSION_KEY = "APP_VERSION";

	public final static String ANDROID_VERSION_KEY = "OS_VERSION";

	public final static String DEVICE_TYPE_KEY = "DEVICE_TYPE";

	public final static String LANGUAGE_KEY = "LANGUAGE";

	// Novedades
	public static final String NOVELTY_KEY = "NOVELTY";
	public static final String NOVELTIES_KEY = "NOVELTIES";
	public static final String FROM_DATE_KEY = "FROM_DATE";
	public static final String TO_DATE_KEY = "TO_DATE";
	public static final String MONDAY_KEY = "MONDAY";
	public static final String TUESDAY_KEY = "TUESDAY";
	public static final String WEDNESDAY_KEY = "WEDNESDAY";
	public static final String THURSDAY_KEY = "THURSDAY";
	public static final String FRIDAY_KEY = "FRIDAY";
	public static final String SATURDAY_KEY = "SATURDAY";
	public static final String SUNDAY_KEY = "SUNDAY";
	public static final String DAY_KEY = "DAY";
	public static final String PERIODICITY_KEY = "PERIODICITY";
	public static final String FK_ROUTE_ORIGIN_KEY = "FK_ROUTE_ORIGIN";
	public static final String FK_ROUTE_DESTINATION_KEY = "FK_ROUTE_DESTINATION";
	public static final String STOP_ORIGIN_KEY = "STOP_ORIGIN";
	public static final String STOP_DESTINATION_KEY = "STOP_DESTINATION";
	public static final String STOP_ADDRESS_KEY = "STOP_ADDRESS";
	public static final String LOOP_TYPE_KEY = "LOOP_TYPE";
	public static final String CREATED_BY_KEY = "CREATED_BY";
	public static final String CREATION_DATE_KEY = "CREATION_DATE";
	public static final String INTERMITTENCE_KEY = "INTERMITTENCE";
	public static final String TRACKABLE_KEY = "TRACKABLE";
	public static final String HOUR_KEY = "HOUR";
	public static final String PARENT_NAME_KEY = "PARENT_NAME";
	public static final String IDENTIFICATION_PARENT_KEY = "IDENTIFICATION_PARENT";

	// Modo libre
	public static final String SELECTED_TRACKABLES_KEY = "SELECTED_TRACKABLES";
}
