package otr.ontrack_onroad.parameters;

public class GeneralMessages {

	public static final String YES = "Sí";
	public static final String NO = "No";
	public static final String CHECK_PROFILE = "Ver perfil";
	public static final String OK = "De acuerdo";

	// Recï¿½lculo de rutas
	public static final String NEW_ROUTE_COMPUTED = "Se ha calculado una nueva ruta al próximo paradero";

	// JSON
	public static final String JSON_ERROR = "Hubo un problema cargando la información. Por favor contáctese de inmediato con servicio técnico";

	// Errores generales
	public static final String ERROR = "Error";
	public static final String BAD_CONSTRUCTED_ROUTE_ERROR = "Error: la ruta está mal construida. Por favor comuníquese con el coordinador de rutas";

	// Web Request
	public static final String HTTP_CONNECTION_ERROR = "Hay problemas con la conexión a internet, vuelva a intentarlo. De lo contrario, espere un momento. Si no se soluciona el problema póngase en contacto con servicio técnico.";
	//public static final String HTTP_NOT_FOUND_ERROR = "Servicio web no encontrado";
	public static final String HTTP_NOT_FOUND_ERROR = "Hay problemas con la conexión a internet, vuelva a intentarlo. De lo contrario, espere un momento. Si no se soluciona el problema póngase en contacto con servicio técnico.";

	public static final String HTTP_CONNECTION_ERROR_RETRY = "Hubo un error en la conexión a internet, por favor inténtelo de nuevo";

	// AsyncTask
	public static final String LOADING = "Cargando...";
	public static final String PLEASE_WAIT = "Un momento por favor...";
	public static final String FINISHING_ROUTE = "Finalizando ruta...";

	// Diï¿½logos
	public static final String DIALOG_TITLE_ERROR = "Error";
	public static final String DIALOG_SEND_MESSAGE = "Enviar mensaje";
	public static final String DIALOG_CLOSE_APP = "Cerrar";

	// GCM
	public static final String GCM_NO_VALID_APK = "No valid Google Play Services APK found.";
	public static final String GCM_NOT_COMPATIBLE = "Este dispositivo no es compatible con GCM.";
	public static final String GCM_REG_ID_NOT_FOUND = "Llave de registro no encontrada";
	public static final String GCM_APP_VERSION_CHANGED = "Versión de la aplicación cambiada.";
	public static final String GCM_PACKAGE_NOT_FOUND = "No se pudo encontrar el paquete: ";
	public static final String GCM_DEVICE_REGISTERED = "Dispositivo registrado";

	// Rutas
	public static final String VOICE_REPORT_TRACKABLE_NOT_IN_BUS = "Por favor verifique que todos los estudiantes en esta lista se encuentren en el vehículo";
	public static final String VOICE_REPORT_SELECT_ASSISTANCE_TYPE = "Esta es una ruta de regreso. Por favor seleccione una modalidad de llamado a lista";
	public static final String ROUTE = "Ruta";

	public static final String START_ROUTE = "INICIAR RUTA";
	public static final String STARTING_ROUTE_MESSAGE = "INICIANDO RUTA";
	public static final String ERROR_LOADING_ROUTES = "Error cargando las rutas";
	public static final String DIALOG_TITLE_CHANGE_ROUTE = "Cambio de ruta";
	public static final String ROUTE_IN_PROGRESS = "La ruta seleccionada ya está en progreso.";
	public static final String VOICE_EXCEEDING_SPEED = "Está excediendo los límites de velocidad. Por favor disminuya la velocidad";
	public static final String VOICE_SENDING_SPEED_REPORT = "Se ha enviado un reporte por exceso de velocidad";
	public static final String ROUTE_WAIT_UNTIL_DETAILS_LOADED = "Por favor espere a que carguen los detalles de la ruta";
	public static final String ROUTE_CHANGE = "¿Está seguro de que quiere cambiar la ruta? Se perderá toda la información de la ruta actual.";
	public static final String ROUTE_CHANGE_TITLE = "Cambio de ruta";

	// Paraderos
	public static final String ARRIVING_TO_STOP_HEADER = "Ha llegado a un paradero. Debe ";
	public static final String ARRIVING_TO_SPECIAL_STOP_HEADER = "Ha llegado a un paradero especial. Debe ";
	public static final String ARRIVING_TO_DESTINATION_HEADER = "Ha llegado a su destino.";
	public static final String ARRIVING_TO_LAST_STOP_HEADER = "Ha llegado al último paradero. Debe ";
	public static final String APROACHING_TO_STOP_HEADER = "Se está acercando al próximo paradero. Debe ";
	public static final String APROACHING_TO_SPECIAL_STOP_HEADER = "Se está acercando a un paradero especial. Debe ";
	public static final String REMEMBER_REPORT_DELIVERY = "Por favor no olvide reportar manualmente la salida del paradero cuando termine de dejar todos los estudiantes";
	public static final String REMEMBER_REPORT_PICK_UP = "Por favor no olvide reportar manualmente la salida del paradero cuando termine de recoger todos los estudiantes";
	public static final String DELIVERY = "dejar";
	public static final String PICK_UP = "recoger";
	public static final String NEXT_STOP_TO = "El próximo paradero está a ";
	public static final String LESS_THAN_ONE_MINUTE = "menos de un minuto";
	public static final String STOP_SINGULAR = "paradero";
	public static final String STOP_PLURAL = "paraderos";
	public static final String STOP_ETA = "HEA: ";
	public static final String STOP_ARRIVAL_TIME = "HR: ";
	public static final String STOP_DEPARTURE_TIME = "HR: ";
	public static final String APPROACHING_TO_DESTINATION_HEADER = "Se está acercando a su destino.";
	public static final String APPROACHING_TO_LAST_STOP_HEADER = "Se está acercando al último paradero. Debe ";

	// Trackables
	public static final String TRACKABLE_TAG_PLURAL = "estudiantes";
	public static final String TRACKABLE_TAG_SINGULAR = "estudiante";
	public static final String ONE_AS_DETERMINANT = "un";
	public static final String ONE_AS_DETERMINANT_FEMALE = "una";

	// Tiempos estimados
	public static final String MINUTES_SINGULAR = "minuto";
	public static final String MINUTES_PLURAL = "minutos";
	public static final String ETA = "HEA";

	// Mensajerï¿½a
	public static final String VOICE_NEW_MESSAGE_FROM = "Nuevo mensaje de ";

	// Reportes
	public static final String REPORT_FIRST_START_ROUTE = "Debe iniciar la ruta para poder enviar reportes";
	public static final String REPORT_COORDINATOR_REVIEW = "El coordinador de transporte ha revisado su reporte de ";
	public static final String REPORT_ACCIDENT = "accidente";
	public static final String REPORT_TRAFFIC = "tráfico";
	public static final String REPORT_MECHANICAL_FAILURE = "falla mecánica";
	public static final String REPORT_WRONG_ROUTE = "ruta incorrecta";
	public static final String REPORT_HEALTH_PROBLEM = "problemas de salud";
	public static final String REPORT_BEHAVIOURAL_PROBLEM = "comportamiento";
	public static final String REPORT_CHECKED = "Por favor comunicarse con el coordinador al finalizar la ruta";
	public static final String REPORT_ANSWER_IS = "y la respuesta es la siguiente";

	// Eventos
	public static final String HAS_ARRIVED_TO_STOP = "lo está esperando en su paradero.";
	public static final String HAS_ARRIVED_TO_SPECIAL_STOP = "esta muy cerca de la vivienda de su hijo(a). Por favor permanezca pendiente de la ubicación del bus en el mapa.";
	public static final String ROUTE_HAS_STARTED = "ha iniciado.";
	public static final String ROUTE_HAS_FINISHED = " ha finalizado.";
	public static final String ROUTE_IS_DELAYED = "presenta un retraso. Se han recalculado los tiempos estimados de llegada al paradero.";
	public static final String ROUTE_IS_ADVANCED = "está adelantada. Recomendamos estar pendientes de la posición del bus.";
	public static final String HAS_LEFT_STOP = "ha salido del paradero de su hijo(a).";
	public static final String DEVIATED_FROM_ROUTE = "se ha desviado del recorrido";
	public static final String GOING_TO_REFUGE = "se está dirigiendo al refugio más cercano. El refugio es ";
	public static final String CLOSE_TO_NEXT_STOP = "está llegando a su paradero.";
	public static final String EXCEEDING_SPEED_LIMIT = "está excediendo el límite de velocidad.";
	public static final String GPS_ACCURACY_LOW = "reporta baja precisión del GPS. Por favor permanezca atento a la llegada del vehículo a su paradero.";
	public static final String VEHICLE_1KM_AWAY_FROM_STOP = "está a menos de 1 kilómetro del paradero de su hijo(a).";
	public static final String VEHICLE_500M_AWAY_FROM_STOP = "está a menos de 500 metros del paradero de su hijo(a).";
	public static final String VEHICLE_200M_AWAY_FROM_STOP = "está a menos de 200 metros del paradero de su hijo(a).";
	public static final String VEHICLE_10MIN_AWAY_FROM_STOP = "está a menos de 10 minutos del paradero de su hijo(a).";
	public static final String VEHICLE_5MIN_AWAY_FROM_STOP = "está a menos de 5 minutos del paradero de su hijo(a).";

	public static final String EVENT_CATEGORY_UNKNOWN = "Evento";

	// Reportes
	public static final String REPORT_HEALTH_PROBLEMS_TITLE = "Estudiante con problemas de salud";
	public static final String REPORT_BEHAVIOUR_PROBLEMS_TITLE = "Estudiante con problemas de comportamiento";

	// Diï¿½logos
	public static final String DIALOG_NOT_DELIVERED_TITLE = "Reportar ausencia de acudiente";
	public static final String DIALOG_NOT_PICKEDUP_TITLE = "Reportar inasistencia";
	public static final String DIALOG_NOT_ATVEHICLE_TITLE = "Reportar inasistencia";
	public static final String DIALOG_REQUEST_TITLE = "Estudiante con solicitud de inasistencia";

	public static final String DIALOG_STUDENT_LIST_TITLE = "Lista de estudiantes";
	public static final String DIALOG_THE_STUDENT_OPEN_QUOTATION = "¿El estudiante ";
	public static final String DIALOG_THE_STUDENT_CLOSE_QUOTATION_PICKEDUP = " llegó al paradero?";
	public static final String DIALOG_THE_STUDENT_CLOSE_QUOTATION_DELIVERED = " fue recogido por algún acudiente?";
	public static final String DIALOG_THE_STUDENT_CLOSE_QUOTATION_ATVEHICLE = " se encuentra en el vehículo?";
	public static final String DIALOG_CARETAKERS_OF_STUDENT_OPEN_SENTENCE = "Los acudientes del estudiante ";
	public static final String DIALOG_CARETAKERS_OF_STUDENT_REQUEST_CLOSE_SENTENCE = " hicieron una solicitud de inasistencia que fue aprobada por el coordinador.";
	public static final String DIALOG_CANCEL_STOP_OPEN_QUOTATION = "¿Está seguro de que quiere cancelar el paradero número ";
	public static final String DIALOG_CANCEL_STOP_TITLE = "Cancelar paradero";

	public static final String DIALOG_CLOSE_APP_TITLE = "Cerrar OnTrack";
	public static final String DIALOG_CLOSE_APP_QUESTION = "¿Desea cerrar la aplicación OnTrack School?";

	public static final String DIALOG_CLOSE_SESSION_QUESTION = "¿Está seguro de que quiere cerrar sesión? Se perderá toda la información de la ruta actual y de usuario.";
	public static final String DIALOG_CLOSE_SESSION_TITLE = "Cerrar sesión";

	public static final String DIALOG_CONFIRM_DELIVERY_QUESTION = "¿Desea iniciar la ruta ahora? Se enviarán reportes a padres de familia de los estudiantes que se hayan reportado como ausentes.";
	public static final String DIALOG_CONFIRM_DELIVERY_TITLE = "Confirmación de inicio de ruta";

	// Detalles de paradero
	public static final String VEHICLE_AT_STOP = "YA ESTOY AQUÍ";
	public static final String VEHICLE_LEFT_STOP = "YA SALÍ DEL PARADERO";
	public static final String FINISH_ROUTE = "FINALIZAR RUTA";

	// Login
	public static final String WRONG_LOGIN = "Error en la autenticación. Por favor verifique su nombre de usuario y contraseña";
	
	// Novedades
	public static final String NOVELTY_TITLE = " novedad";
	public static final String NOVELTIES_TITLE = " novedades";
	public static final String NOVELTY_SUMMARY_HEADER = "Atención: el día de hoy la ruta presenta ";
	public static final String NOVELTY_NOT_ASSISTING_PLURAL = " no se transportarán en esta ruta el día de hoy.";
	public static final String NOVELTY_OUT_ROUTE_SINGULAR = " se transportará en otra ruta.";
	public static final String NOVELTY_OUT_ROUTE_PLURAL = " se transportarán en otra ruta.";
	public static final String NOVELTY_IN_ROUTE_SINGULAR = " adicional ingresará a esta ruta el día de hoy.";
	public static final String NOVELTY_IN_ROUTE_PLURAL = " adicionales ingresarán a esta ruta el día de hoy.";
	public static final String NOVELTY_CHANGE_STOP_DELIVERY_PLURAL = " serán dejados en un paradero diferente al usual.";
	public static final String NOVELTY_CHANGE_STOP_PICKUP_PLURAL = " serán recogidos en un paradero diferente al usual.";
	public static final String DIALOG_NOVELTY_DETAILS = "Estudiante con novedad";
	public static final String THE_STUDENT = "El estudiante ";
	public static final String HAS_A_NOVELTY = " presenta una novedad: ";
	public static final String NOVELTY_NOT_ASSISTING = " no se transportará en esta ruta el día de hoy.";
	public static final String NOVELTY_PARENT = "El acudiente encargado es ";
	public static final String NOVELTY_IDENTIFICATION_PARENT =  " con número de identificación ";
	public static final String NOVELTY_HOUR = ", y lo recogerá a las ";
	public static final String NOVELTY_CAR = " se transportará en carro el día de hoy.";
	public static final String NOVELTY_CAR_PLURAL = " se transportarán en carro el día de hoy.";
	public static final String NOVELTY_OUT_ROUTE = " el día de hoy será transportado en la ruta ";
	public static final String NOVELTY_IN_ROUTE = "será transportado en esta ruta el día de hoy.";
	public static final String NOVELTY_CHANGE_STOP_DELIVERY = " será dejado en un paradero diferente al usual.";
	public static final String NOVELTY_CHANGE_STOP_PICKUP = " será recogido en un paradero diferente al usual.";
	public static final String NOVELTY_DESCRIPTION = " A continuación se explican los detalles: ";

}
