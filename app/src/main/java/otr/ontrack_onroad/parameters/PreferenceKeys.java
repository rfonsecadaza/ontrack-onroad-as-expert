package otr.ontrack_onroad.parameters;

public class PreferenceKeys {
	public static final String PREFERENCES_KEY = "preferences";
	
	public static final String USERNAME_KEY = "username";
	public static final String PASSWORD_KEY = "password";
	public static final String TOKEN_KEY = "token";
	public static final String USER_DEVICE_ID_KEY = "user_device_id";
	public static final String PROPERTY_REG_ID_KEY = "registration";	
	public static final String PROPERTY_APP_VERSION_KEY = "appVersion";
	public static final String LAST_REQUEST_DATE_KEY = "last_request_date";
	public static final String EXCEPTION = "exception";
	public static final String ACTIVE_ROUTE = "activeRoute";
}
