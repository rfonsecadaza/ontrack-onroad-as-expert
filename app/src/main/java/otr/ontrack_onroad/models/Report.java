package otr.ontrack_onroad.models;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

public class Report {

	public static final String REPORTS_KEY = "REPORTS";
	public static final String DATE_GENERATED_KEY = "DATE_GENERATED";
	public static final String FK_CATEGORY_KEY = "FK_CATEGORY_REPORT";
	public static final String DESCRIPTION_KEY = "DESCRIPTION";
	public static final String LATITUDE_KEY = "LATITUDE";
	public static final String LONGITUDE_KEY = "LONGITUDE";
	public static final String FK_ROUTE_KEY = "FK_ROUTE";
	public static final String FK_STOP_KEY = "FK_STOP";
	public static final String FK_TRACKABLE_KEY = "FK_TRACKABLE";
	public static final String FK_FOOTPRINT_KEY = "FK_FOOTPRINT";
	public static final String FK_TRACKED_STOP_KEY = "FK_TRACKED_STOP";
	public static final String FK_TRACKED_STOP_TRACKABLE_KEY = "FK_TRACKED_STOP_TRACKABLE";
	public static final String FK_ORGANIZATION = "FK_ORGANIZATION";

	public static final String WRONG_ROUTE_REPORT_CODE = "REP-F-01";
	public static final String TRAFFIC_REPORT_CODE = "REP-F-02";
	public static final String MECHANICAL_FAILURE_REPORT_CODE = "REP-F-03";
	public static final String ACCIDENT_REPORT_CODE = "REP-F-04";

	public static final String HEALTH_PROBLEM_REPORT_CODE = "REP-TST-01";
	public static final String BEHAVIOUR_PROBLEM_REPORT_CODE = "REP-TST-02";

	public static final int REPORT_STATE_NOT_ANSWERED = 0;
	public static final int REPORT_STATE_PENDING = 1;
	public static final int REPORT_STATE_ANSWERED = 2;

	private Long id;
	private Long serverId;
	private String date;
	private String category;
	private String description;
	private Double latitude;
	private Double longitude;
	private Integer fkDestination;
	private Long fkFootprint;
	private String response;
	private Integer state;

	public Report() {
	}

	public Report(Long id) {
		this.id = id;
	}

	public Report(Long id, Long serverId, String date, String category,
			String description, Double latitude, Double longitude,
			Integer fkDestination, Long fkFootprint, String response,
			Integer state) {
		this.id = id;
		this.serverId = serverId;
		this.date = date;
		this.category = category;
		this.description = description;
		this.latitude = latitude;
		this.longitude = longitude;
		this.fkDestination = fkDestination;
		this.fkFootprint = fkFootprint;
		this.response = response;
		this.state = state;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getServerId() {
		return serverId;
	}

	public void setServerId(Long serverId) {
		this.serverId = serverId;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Integer getFkDestination() {
		return fkDestination;
	}

	public void setFkDestination(Integer fkDestination) {
		this.fkDestination = fkDestination;
	}

	public Long getFkFootprint() {
		return fkFootprint;
	}

	public void setFkFootprint(Long fkFootprint) {
		this.fkFootprint = fkFootprint;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public String getReportJSONString(String selectedFkKey)
			throws JSONException {
		JSONObject jsonReport = new JSONObject();
		jsonReport.put(DATE_GENERATED_KEY, this.getDate());
		jsonReport.put(FK_CATEGORY_KEY, this.getCategory());
		jsonReport.put(DESCRIPTION_KEY, this.getDescription());
		jsonReport.put(LATITUDE_KEY, this.getLatitude());
		jsonReport.put(LONGITUDE_KEY, this.getLongitude());

		jsonReport.put(selectedFkKey, this.getFkDestination());

		return "[" + jsonReport.toString() + "]";

	}
}
