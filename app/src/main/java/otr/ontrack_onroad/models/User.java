package otr.ontrack_onroad.models;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import otr.ontrack_onroad.utils.WebRequestManager;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

public class User {
    public static final String ID_KEY = "ID";
    public static final String OLD_ID_KEY = "OLD_ID";
    public static final String FIRST_NAME_KEY = "FIRST_NAME";
    public static final String LAST_NAME_KEY = "LAST_NAME";
    public static final String CONTACT_EMAIL_KEY = "CONTACT_EMAIL";
    public static final String CONTACT_ADDRESS_KEY = "CONTACT_ADDRESS";
    public static final String MOBILE_PHONE_KEY = "MOBILE_PHONE";
    public static final String LAN_PHONE_KEY = "LAN_PHONE";
    public static final String ROLE_KEY = "ROLE";
    public static final String ROUTES_KEY = "ROUTES";
    public static final String ROUTE_SCHEDULES_KEY = "ROUTE_SCHEDULES";
    public static final String ROUTE_ORGANIZATIONS_KEY = "ORGANIZATIONS";
    public static final String SCHEDULE_KEY = "SCHEDULE";
    public static final String IMAGE_KEY = "IMAGE";
    public static final String IMAGE_DEFAULT_KEY = "IMAGE_DEFAULT";
    public static final String DRIVER_KEY = "DRIVER";
    public static final String STATUS_KEY = "STATUS";
    public static final String USERS_ALT_KEY = "USERS_ALT";
    public static final String LICENSE_NUMBER_KEY = "LICENSE_NUMBER";
    public static final String TYPE_USER_KEY = "TYPE_USER";
    public static final String USER_KEY = "USER";
    public static final String MONITORS_KEY = "MONITORS";
    public static final String DRIVERS_KEY = "DRIVERS";

    public static final int ROLE_CONDUCTOR = 0;
    public static final int ROLE_PAPA = 1;
    public static final int ROLE_ESTUDIANTE = 2;
    public static final int ROLE_MONITORA = 3;

    private String id;
    private String firstName;
    private String lastName;
    private String contactAddress;
    private String mobilePhone;
    private String lanPhone;
    private String role;
    private Route route;
    private int footprint;
    private ArrayList<Organization> organizations;
    private String licenseNumber;
    private String imageURL;

    public User( String id, String firstName, String lastName, String contactAddress, String mobilePhone, String lanPhone, String role, String licenseNumber, String imageURL )
    {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.contactAddress = contactAddress;
        this.mobilePhone = mobilePhone;
        this.setLanPhone( lanPhone );
        this.role = role;
        this.footprint = -1;
        this.licenseNumber = licenseNumber;
        this.imageURL = WebRequestManager.URL_IMAGE_BASE + imageURL;
    }

    public User(String id, String firstName, String lastName,
                String contactAddress, String mobilePhone, String lanPhone,
                String role, Route route, String image, boolean asyncImageLoad) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.contactAddress = contactAddress;
        this.mobilePhone = mobilePhone;
        this.setLanPhone(lanPhone);
        this.role = role;

//        if (asyncImageLoad) {
//            (new DownloadImageTask()).execute(image);
//        } else {
//            URL url;
//            HttpURLConnection conn = null;
//
//            try {
//                url = new URL(image);
//                conn = (HttpURLConnection) url.openConnection();
//                setImage(BitmapFactory.decodeStream(conn.getInputStream()));
//            } catch (MalformedURLException e) {
//                e.printStackTrace();
//            } catch (IOException e) {
//                e.printStackTrace();
//            } finally {
//                if (conn != null)
//                    conn.disconnect();
//            }
//        }

        this.route = route;
        this.footprint = -1;
        this.imageURL = WebRequestManager.URL_IMAGE_BASE + image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFullName() {
        return this.firstName + " " + this.lastName;
    }

    public String getContactAddress() {
        return contactAddress;
    }

    public void setContactAddress(String contactAddress) {
        this.contactAddress = contactAddress;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String contactPhone) {
        this.mobilePhone = contactPhone;
    }

    public String getLanPhone() {
        return lanPhone;
    }

    public void setLanPhone(String lanPhone) {
        this.lanPhone = lanPhone;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Route getRoute() {
        return route;
    }

    public void setRoute(Route route) {
        this.route = route;
    }

    public int getFootprint() {
        return footprint;
    }

    public void setFootprint(int footprint) {
        this.footprint = footprint;
    }

    public ArrayList<Organization> getOrganizations( )
    {
        return organizations;
    }

    public void setOrganizations( ArrayList<Organization> organizations )
    {
        this.organizations = organizations;
    }

    public ArrayList<User> peopleInChargeOfRoute() {
        ArrayList<User> peopleInCharge = new ArrayList<User>();
        peopleInCharge.add(this);
        // peopleInCharge.add(this);
        // peopleInCharge.add(this);
        return peopleInCharge;
    }

    public ArrayList<String> getOrganizationsNames( )
    {
        ArrayList<String> organizationsNames = new ArrayList<String>( );

        for( Organization organization : organizations )
        {
            organizationsNames.add( organization.getTitle( ) );
        }

        return organizationsNames;
    }

    public Organization getOrganizationByTitle( String title )
    {
        for( Organization organization : organizations )
        {
            if( organization.getTitle( ).equals( title ) )
            {
                return organization;
            }
        }

        return null;
    }

    public Organization getOrganizationByID( String id )
    {
        for( Organization organization : organizations )
        {
            if( organization.getId( ).equals( id ) )
            {
                return organization;
            }
        }

        return null;
    }

    public Organization getOrganizationByRouteId( String id )
    {
        for( Organization organization : organizations )
        {
            if( organization.getRoutes( ) != null )
            {
                for( Route route : organization.getRoutes( ) )
                {
                    if( route.getId( ).equals( id ) )
                    {
                        return organization;
                    }
                }
            }
        }

        return null;
    }

    public String getLicenseNumber( )
    {
        return licenseNumber;
    }

    public void setLicenseNumber( String licenseNumber )
    {
        this.licenseNumber = licenseNumber;
    }

    public String getImageURL( )
    {
        return imageURL;
    }

    public void setImageURL( String imageURL )
    {
        this.imageURL = imageURL;
    }
}
