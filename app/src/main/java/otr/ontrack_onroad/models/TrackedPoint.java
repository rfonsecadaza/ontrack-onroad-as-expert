package otr.ontrack_onroad.models;

public class TrackedPoint extends Point{
	private String date;
	private double accuracy;
	private double speed;
	private float totalDistance;
	private double bearing;
	
	public TrackedPoint (double longitude, double latitude, double altitude, String date, double accuracy, double speed){
		super(longitude,latitude,altitude);
		this.date = date;
		this.accuracy = accuracy;
		this.speed = speed;
		this.totalDistance = 0;
	}
	
	public TrackedPoint (double longitude, double latitude, double altitude, String date, double accuracy, double speed, double bearing){
		super(longitude,latitude,altitude);
		this.date = date;
		this.accuracy = accuracy;
		this.speed = speed;
		this.totalDistance = 0;
		this.setBearing(bearing);
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public double getAccuracy() {
		return accuracy;
	}

	public void setAccuracy(double accuracy) {
		this.accuracy = accuracy;
	}

	public double getSpeed() {
		return speed;
	}

	public void setSpeed(double speed) {
		this.speed = speed;
	}

	public float getTotalDistance() {
		return totalDistance;
	}

	public void setTotalDistance(float totalDistance) {
		this.totalDistance = totalDistance;
	}

	public double getBearing() {
		return bearing;
	}

	public void setBearing(double bearing) {
		this.bearing = bearing;
	}
}
