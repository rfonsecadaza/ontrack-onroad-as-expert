package otr.ontrack_onroad.models;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Comparator;

//import com.google.android.gms.maps.model.LatLng;

import otr.ontrack_onroad.adapters.AssistanceStudentListItem;
import otr.ontrack_onroad.managers.FreeModeLocationManager;
import otr.ontrack_onroad.parameters.ServiceKeys;
import otr.ontrack_onroad.utils.ImageAdjustment;
import otr.ontrack_onroad.utils.Timestamp;
import otr.ontrack_onroad.utils.WebRequestManager;

import android.app.Service;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import com.mapbox.mapboxsdk.geometry.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Trackable implements AssistanceStudentListItem
{
    public static final String TRACKABLES_KEY = "TRACKABLES";
    public static final String TRACKABLE_KEY = "TRACKABLE";
    public static final String ID_KEY = "ID";
    public static final String OLD_ID_KEY = "OLD_ID";
    public static final String NAME_KEY = "NAME";
    public static final String SUBNAME_KEY = "SUB_NAME";
    public static final String DESCRIPTION_KEY = "DESCRIPTION";
    public static final String IMAGE_KEY = "IMAGE";
    public static final String IMAGE_DEFAULT_KEY = "IMAGE_DEFAULT";
    public static final String BIRTHDAY_KEY = "BIRTH_DATE";
    public static final String COURSE_KEY = "COURSE";
    public static final String LEVEL_KEY = "LEVEL";
    public static final String CONTACTPHONE_KEY = "CONTACT_PHONE";
    public static final String MOBILE_PHONE_KEY = "MOBILE_PHONE";
    public static final String CONTACTADDRESS_KEY = "CONTACT_ADDRESS";
    public static final String NEIGHBORHOOD_KEY = "NEIGHBORHOOD";
    public static final String DISTRICT_KEY = "DISTRICT";
    public static final String EMAIL_KEY = "CONTACT_EMAIL";
    public static final String USERS_KEY = "USERS";
    public static final String TRACKED_STOP_TRACKABLE_ID_KEY = "ID";
    public static final String TRACKED_STOP_TRACKABLE_FK_TRACKABLE_KEY = "FK_TRACKABLE";
    public static final String STATE_KEY = "STATE";
    public static final String LATITUDE_KEY = "LATITUDE";
    public static final String LONGITUDE_KEY = "LONGITUDE";

    public static final int UNKNOWN = -1;
    public static final int ASSISTING_DELIVERED = 1;
    public static final int NOT_ASSISTING_DELIVERED = 0;
    public static final int NOT_AT_SCHOOL = 2;

    public static final int STATE_ATTENDANCE = 0;
    public static final int STATE_NOT_ATTENDANCE = 1;
    
    public static final int NOVELTY_TYPE_NOT_ATTENDING = 0;
    public static final int NOVELTY_TYPE_CHANGE_ROUTE = 1;
    public static final int NOVELTY_TYPE_CAR = 3;

    private String id;
    private String name;
    private String subName;
    private String description;
    private String birthday;
    private String course;
    private String level;
    private String contactPhone;
    private String mobilePhone;
    private String eMail;
    private String contactAddress;
    private String neighborhood;
    private String district;
    private int trackedStopTrackableId;
    private int footprintTrackableId;
    private int picked;
    private int state;
    private ArrayList<User> users;
    private boolean selected;
    private String imageURL;
    private LatLng position;

    // Route creation purposes (gente gomela que hace comentarios en inglés)
    private int assignedStop;
    private boolean inRoute;
    
    // Novedad que tiene para este día
    private Novelty novelty;

    public Trackable( String id, String firstName, String lastName, String birthDate, String course, String level, String eMail, String phone, String mobilePhone, String address, String neighborhood, String district, String imageURL, LatLng position, boolean save )
    {
        assignedStop = -1;
        this.id = id;
        this.name = firstName;
        this.subName = lastName;
        this.birthday = birthDate;
        this.course = course;
        this.level = level;
        this.eMail = eMail;
        this.contactPhone = phone;
        this.mobilePhone = mobilePhone;
        this.contactAddress = address;
        this.neighborhood = neighborhood;
        this.district = district;
        this.imageURL = imageURL;
        this.position = position;
        
        this.footprintTrackableId = -1;
        
        this.novelty = null;
        this.imageURL = WebRequestManager.URL_IMAGE_BASE + imageURL;
    }

    public Trackable( String id, String name, String subName, String description, String birthday, String course, String contactPhone, String contactAddress, String image, ArrayList<User> users, int state, boolean asyncImageLoad )
    {
        assignedStop = -1;
        this.id = id;
        this.name = name;
        this.subName = subName;
        this.description = description;
        this.birthday = birthday;
        this.course = course;
        this.contactPhone = contactPhone;
        this.contactAddress = contactAddress;
        this.picked = UNKNOWN;
        this.state = state;
        this.setTrackedStopTrackableId( -1 );
        
        this.footprintTrackableId = -1;
//        if( asyncImageLoad )
//        {
//            ( new DownloadImageTask( ) ).execute( image );
//        }
//        else
//        {
//            try
//            {
//                setImage( ImageAdjustment.getScaledBitmapFromUrl( image, 100, 100 ) );
//            }
//            catch( MalformedURLException e )
//            {
//                e.printStackTrace( );
//            }
//            catch( IOException e )
//            {
//                e.printStackTrace( );
//            }
//        }

        this.setUsers( users );

        this.novelty = null;
        
        this.imageURL = WebRequestManager.URL_IMAGE_BASE + image;
    }

    public String getId( )
    {
        return id;
    }

    public void setId( String id )
    {
        this.id = id;
    }

    public String getName( )
    {
        return name;
    }

    public void setName( String name )
    {
        this.name = name;
    }

    public String getSubName( )
    {
        return subName;
    }

    public String getFullname( )
    {
        return this.name + " " + this.subName;
    }

    public void setSubName( String subName )
    {
        this.subName = subName;
    }

    public String getDescription( )
    {
        return description;
    }

    public void setDescription( String description )
    {
        this.description = description;
    }

    public String getBirthday( )
    {
        return birthday;
    }

    public void setBirthday( String birthday )
    {
        this.birthday = birthday;
    }

    public String getCourse( )
    {
        return course;
    }

    public void setCourse( String course )
    {
        this.course = course;
    }

    public String getContactPhone( )
    {
        return contactPhone;
    }

    public void setContactPhone( String contactPhone )
    {
        this.contactPhone = contactPhone;
    }

    public String getContactAddress( )
    {
        return contactAddress;
    }

    public void setContactAddress( String contactAddress )
    {
        this.contactAddress = contactAddress;
    }

    public int getPicked( )
    {
        return picked;
    }

    public void setPicked( int picked )
    {
        this.picked = picked;
    }

    public ArrayList<User> getUsers( )
    {
        return users;
    }

    public void setUsers( ArrayList<User> users )
    {
        this.users = users;
    }

    public int getTrackedStopTrackableId( )
    {
        return trackedStopTrackableId;
    }

    public void setTrackedStopTrackableId( int trackedStopTrackableId )
    {
        this.trackedStopTrackableId = trackedStopTrackableId;
    }

    public int getFootprintTrackableId() {
		return footprintTrackableId;
	}

	public void setFootprintTrackableId(int footprintTrackableId) {
		this.footprintTrackableId = footprintTrackableId;
	}

	public int getState( )
    {
        return state;
    }

    public void setState( int state )
    {
        this.state = state;
    }

    public boolean isRelevant( )
    {
    	// Último modelo: revisa si hay una novedad vigente de ausencia o de salidad de esta ruta
    	
    	boolean isNoveltyNotAttending = this.novelty!=null?novelty.isNotAttending()||novelty.isOutOfCurrentRoute()||novelty.isInCar():false;
        if( this.getState( ) != Trackable.STATE_NOT_ATTENDANCE && this.getPicked( ) != Trackable.NOT_AT_SCHOOL && this.getPicked( ) != NOT_ASSISTING_DELIVERED && !isNoveltyNotAttending)
            return true;
        return false;
    }

    public boolean isSelected( )
    {
        return selected;
    }

    public void setSelected( boolean selected )
    {
        this.selected = selected;
    }

    public int getAssignedStop( )
    {
        return assignedStop;
    }

    public void setAssignedStop( int assignedStop )
    {
        this.assignedStop = assignedStop;
    }

    public String getLevel( )
    {
        return level;
    }

    public void setLevel( String level )
    {
        this.level = level;
    }

    public String getMobilePhone( )
    {
        return mobilePhone;
    }

    public void setMobilePhone( String mobilePhone )
    {
        this.mobilePhone = mobilePhone;
    }

    public String geteMail( )
    {
        return eMail;
    }

    public void seteMail( String eMail )
    {
        this.eMail = eMail;
    }

    public String getNeighborhood( )
    {
        return neighborhood;
    }

    public void setNeighborhood( String neighborhood )
    {
        this.neighborhood = neighborhood;
    }

    public String getDistrict( )
    {
        return district;
    }

    public void setDistrict( String district )
    {
        this.district = district;
    }

    public String getImageURL( )
    {
        return imageURL;
    }

    public void setImageURL( String imageURL )
    {
        this.imageURL = imageURL;
    }

    public static class TrackableComparator implements Comparator<Trackable>
    {

        @Override
        public int compare( Trackable trackable1, Trackable trackable2 )
        {
            String trackable1order = trackable1.getSubName( );
            String trackable2order = trackable2.getSubName( );
            return trackable1order.compareTo( trackable2order );
        }
    }

    @Override
    public boolean isSection( )
    {
        // TODO Auto-generated method stub
        return false;
    }

    public LatLng getPosition( )
    {
        return position;
    }

    public void setPosition( LatLng position )
    {
        this.position = position;
    }

    public boolean isInRoute( )
    {
        return inRoute;
    }

    public void setInRoute( boolean inRoute )
    {
        this.inRoute = inRoute;
    }

	public Novelty getNovelty() {
		return novelty;
	}

	public void setNovelty(Novelty novelty) {
		this.novelty = novelty;
	}
	
	public String getFootprintTrackablePickedJSONArray(){
        // Se verifica si se debe cambiar la posición en la que fue recogido el trackable
        FreeModeLocationManager.getInstance().checkIfLocationChangedNeeded();

//        Log.d("ft", "[{" + ServiceKeys.ID_KEY + ":" + this.footprintTrackableId + "," + ServiceKeys.PICKED_KEY + ":" + this.picked + ","
//                + ServiceKeys.LATITUDE_KEY + ":" + FreeModeLocationManager.getInstance().getLocation().getLatitude() + "," +
//                ServiceKeys.LONGITUDE_KEY + ":" + FreeModeLocationManager.getInstance().getLocation().getLongitude() + "," +
//                ServiceKeys.POSITION_KEY + ":" + FreeModeLocationManager.getInstance().getCurrentTrackablePosition() + "," + ServiceKeys.DATE_REPORTED_KEY + ":'" + Timestamp.completeTimestamp() + "'}]");

		return "[{"+ServiceKeys.ID_KEY+":"+this.footprintTrackableId+","+ServiceKeys.PICKED_KEY+":"+this.picked+","
                   +ServiceKeys.LATITUDE_KEY+":"+ FreeModeLocationManager.getInstance().getLocation().getLatitude()+","+
                ServiceKeys.LONGITUDE_KEY+":"+FreeModeLocationManager.getInstance().getLocation().getLongitude()+","+
                ServiceKeys.POSITION_KEY+":"+FreeModeLocationManager.getInstance().getCurrentTrackablePosition()+","+ServiceKeys.DATE_REPORTED_KEY+":'"+ Timestamp.completeTimestamp()+"'}]";
	}

    public String getFootprintTrackablePickedJSONArrayNormalMode(){
        // Se verifica si se debe cambiar la posición en la que fue recogido el trackable
        FreeModeLocationManager.getInstance().checkIfLocationChangedNeeded();

//        Log.d("ft", "[{" + ServiceKeys.ID_KEY + ":" + this.footprintTrackableId + "," + ServiceKeys.PICKED_KEY + ":" + this.picked + ","
//                + ServiceKeys.LATITUDE_KEY + ":" + FreeModeLocationManager.getInstance().getLocation().getLatitude() + "," +
//                ServiceKeys.LONGITUDE_KEY + ":" + FreeModeLocationManager.getInstance().getLocation().getLongitude() + "," +
//                ServiceKeys.POSITION_KEY + ":" + FreeModeLocationManager.getInstance().getCurrentTrackablePosition() + "," + ServiceKeys.DATE_REPORTED_KEY + ":'" + Timestamp.completeTimestamp() + "'}]");

        return "[{"+ServiceKeys.ID_KEY+":"+this.footprintTrackableId+","+ServiceKeys.PICKED_KEY+":"+this.picked+","
                +ServiceKeys.LATITUDE_KEY+":0,"+
                ServiceKeys.LONGITUDE_KEY+":0,"+
                ServiceKeys.POSITION_KEY+":0,"+ServiceKeys.DATE_REPORTED_KEY+":'"+ Timestamp.completeTimestamp()+"'}]";
    }

    public String getTrackedStopTrackablePickedJSONArray() throws JSONException {
        JSONArray jsonArray = new JSONArray();
        JSONObject trackableObject = new JSONObject();
        trackableObject.put(ServiceKeys.ID_KEY,this.trackedStopTrackableId);
        trackableObject.put(ServiceKeys.PICKED_KEY,this.picked);
        jsonArray.put(0,trackableObject);
        return jsonArray.toString();

    }

}
