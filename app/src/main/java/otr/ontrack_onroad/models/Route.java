package otr.ontrack_onroad.models;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

import otr.ontrack_onroad.adapters.AssistanceStudentListHeader;
import otr.ontrack_onroad.adapters.AssistanceStudentListItem;
import otr.ontrack_onroad.managers.FreeModeManager;

import android.util.Log;

public class Route {
	public static final int GOOGLEMAPS_DIVIDER = 100000;
	public static final int MAPBOX_DIVIDER = 1000000;

	public static final String ID_KEY = "ID";
	public static final String POINTS_KEY = "POINTS";
	public static final String STOPS_KEY = "STOPS";
	public static final String STOPS_WITH_NOVELTIES_KEY = "STOPS_WITH_NOVELTIES";
	public static final String SCHEDULE_KEY = "SCHEDULE";
	public static final String VEHICLE_KEY = "VEHICLE";
	public static final String VEHICLES_KEY = "VEHICLES";
	public static final String TYPE_KEY = "TYPE";
	public static final String ROUTE_KEY = "ROUTE";
	public static final String ROUTESCHEDULES_KEY = "ROUTESCHEDULES";
	public static final String ROUTE_SCHEDULES_KEY = "ROUTE_SCHEDULES";
	public static final String ROUTETRACKABLES_KEY = "ROUTETRACKABLES";
	public static final String MODIFICATION_DATE_KEY = "MODIFICATION_DATE";
	public static final String MODIFIED_BY_KEY = "MODIFIED_BY";
	public static final String NUMBER_STOPS_IN_ROUTE_KEY = "NUMBER_STOPS_IN_ROUTE";
	public static final String FK_ORGANIZATION_KEY = "FK_ORGANIZATION";
	public static final String HAS_FINISHED_TODAY_KEY = "HAS_FINISHED_TODAY";


	public static final int DELIVERY_CODE = 0;
	public static final int PICK_UP_CODE = 1;

	private String id;
	private Schedule schedule;
	private Vehicle vehicle;
	private User monitor;
	private int type;
	private Calendar startTime;

	private ArrayList<Point> points = null;
	private ArrayList<Stop> stops;

	private boolean shouldBeRunning;

	private ArrayList<Trackable> trackables;

	private ArrayList<Trackable> trackablesFromNovelties;

	private ArrayList<Novelty> novelties;
	
	private Date modificationDate;
	
	private String modifiedBy;

	private int numberOfStops;

	private String organizationId;

	private boolean hasFinished;

	public Route(String id, Schedule schedule, int type, Date modificationDate, String modifiedBy, int numberOfStops, String organizationId, boolean hasFinished ) {
		this.setId(id);
		this.schedule = schedule;
		this.vehicle = null;
		this.setType(type);
		this.points = new ArrayList<Point>();
		this.trackablesFromNovelties = new ArrayList<Trackable>();
		this.modificationDate = modificationDate;
		this.modifiedBy = modifiedBy;
		this.numberOfStops = numberOfStops;
		this.organizationId = organizationId;
		this.hasFinished = hasFinished;
	}

	public Route(String id, Schedule schedule, Vehicle vehicle, int type) {
		this.setId(id);
		this.schedule = schedule;
		this.vehicle = vehicle;
		this.setType(type);
		this.points = new ArrayList<Point>();
		this.trackablesFromNovelties = new ArrayList<Trackable>();
		this.hasFinished = false;
	}

	public Route() {
		this(null, null, PICK_UP_CODE, null, null, 0, null, false);
		this.points = new ArrayList<Point>();
		this.stops = new ArrayList<Stop>();
		this.trackablesFromNovelties = new ArrayList<Trackable>();
	}

	public Route(String id, Schedule schedule, Vehicle vehicle,
			ArrayList<Point> points, ArrayList<Stop> stops, int type) {
		this(id, schedule, vehicle, type);
		this.points = points;
		this.stops = stops;
		this.trackablesFromNovelties = new ArrayList<Trackable>();
	}

	public Route(String id, Schedule schedule, Vehicle vehicle,
			ArrayList<Stop> stops, int type) {
		this(id, schedule, vehicle, type);
		this.points = new ArrayList<Point>();
		this.stops = stops;
		this.trackablesFromNovelties = new ArrayList<Trackable>();
	}

	// Source:
	// http://blog.rolandl.fr/1357-android-des-itineraires-dans-vos-applications-grace-a-lapi-google-direction
	public void decodeRoute(String encodedPoints) {
		int index = 0;
		int lat = 0, lng = 0;
		ArrayList<Point> tmpPoints = new ArrayList<Point>();

		while (index < encodedPoints.length()) {
			int b, shift = 0, result = 0;

			do {
				b = encodedPoints.charAt(index++) - 63;
				result |= (b & 0x1f) << shift;
				shift += 5;
			} while (b >= 0x20);

			int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
			lat += dlat;
			shift = 0;
			result = 0;

			do {
				b = encodedPoints.charAt(index++) - 63;
				result |= (b & 0x1f) << shift;
				shift += 5;
			} while (b >= 0x20);

			int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
			lng += dlng;

			tmpPoints.add(new Point((double) lng / MAPBOX_DIVIDER,
					(double) (lat) / MAPBOX_DIVIDER, 0));

		}
		if (points.isEmpty()) {
			points.addAll(tmpPoints);
		} else {
			tmpPoints.remove(0);
			points.addAll(tmpPoints);
		}
	}

	public void decodeRouteForStop(String encodedPoints, int stopOrder) {
		int index = 0;
		int order = 1;
		int lat = 0, lng = 0;
		ArrayList<Point> tmpPoints = new ArrayList<Point>();

		while (index < encodedPoints.length()) {
			int b, shift = 0, result = 0;

			do {
				b = encodedPoints.charAt(index++) - 63;
				result |= (b & 0x1f) << shift;
				shift += 5;
			} while (b >= 0x20);

			int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
			lat += dlat;
			shift = 0;
			result = 0;

			do {
				b = encodedPoints.charAt(index++) - 63;
				result |= (b & 0x1f) << shift;
				shift += 5;
			} while (b >= 0x20);

			int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
			lng += dlng;

			// Se va agregando el punto decodificado, pero vale la pena tener en cuenta ese "order"?
			tmpPoints.add(new Point((double) lng / MAPBOX_DIVIDER,
					(double) (lat) / MAPBOX_DIVIDER, 0, order));
			order++;

		}
		Stop stop = this.stops.get(stopOrder);
		stop.addSteps(tmpPoints);
	}

	public void decodeRouteForStopRouteCreation(String encodedPoints,
			int stopOrder) {
	    Stop stop = this.stops.get(stopOrder - 1);
	    
		int index = 0;
		int order = -1;
		
		if( stop.getSteps( ) == null || stop.getSteps( ).isEmpty( ) )
		{
		    order = 1;
		}
		else
		{
		    order = stop.getSteps( ).size( );
		} 
		
		int lat = 0, lng = 0;
		ArrayList<Point> tmpPoints = new ArrayList<Point>();

		while (index < encodedPoints.length()) {
			int b, shift = 0, result = 0;

			do {
				b = encodedPoints.charAt(index++) - 63;
				result |= (b & 0x1f) << shift;
				shift += 5;
			} while (b >= 0x20);

			int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
			lat += dlat;
			shift = 0;
			result = 0;

			do {
				b = encodedPoints.charAt(index++) - 63;
				result |= (b & 0x1f) << shift;
				shift += 5;
			} while (b >= 0x20);

			int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
			lng += dlng;

			tmpPoints.add(new Point((double) lng / MAPBOX_DIVIDER,
					(double) (lat) / MAPBOX_DIVIDER, 0, order));
			order++;

		}
		
		stop.addSteps(tmpPoints);
	}

	public void decodeRouteForStopAndClear(String encodedPoints, int stopOrder) {
		int index = 0;
		int lat = 0, lng = 0;
		ArrayList<Point> tmpPoints = new ArrayList<Point>();

		while (index < encodedPoints.length()) {
			int b, shift = 0, result = 0;

			do {
				b = encodedPoints.charAt(index++) - 63;
				result |= (b & 0x1f) << shift;
				shift += 5;
			} while (b >= 0x20);

			int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
			lat += dlat;
			shift = 0;
			result = 0;

			do {
				b = encodedPoints.charAt(index++) - 63;
				result |= (b & 0x1f) << shift;
				shift += 5;
			} while (b >= 0x20);

			int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
			lng += dlng;

			tmpPoints.add(new Point((double) lng / MAPBOX_DIVIDER,
					(double) (lat) / MAPBOX_DIVIDER, 0));
//			Log.d("point", "lng " + ((double) lng / MAPBOX_DIVIDER) + " lat " + ((double) (lat) / MAPBOX_DIVIDER));

		}

		Stop stop = this.stops.get(stopOrder);
		stop.addSteps(tmpPoints);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIdWithoutSuffix() {
		return this.id.split("-")[0];
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public ArrayList<Point> getPoints() {
		return points;
	}

	public void setPoints(ArrayList<Point> points) {
		this.points = points;
	}

	public ArrayList<Stop> getStops() {
		return stops;
	}

	public void setStops(ArrayList<Stop> stops) {
		this.stops = stops;
	}

	public Schedule getSchedule() {
		return schedule;
	}

	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	public Calendar getStartTime() {
		return startTime;
	}

	public void setStartTime(Calendar startTime) {
		this.startTime = startTime;
	}

	public ArrayList<Trackable> getTrackablesFromNovelties() {
		return trackablesFromNovelties;
	}

	public void setTrackablesFromNovelties(
			ArrayList<Trackable> trackablesWithoutStop) {
		this.trackablesFromNovelties = trackablesWithoutStop;
	}

	public ArrayList<Novelty> getNovelties() {
		return novelties;
	}

	public void setNovelties(ArrayList<Novelty> novelties) {
		this.novelties = novelties;
	}

	public Stop findStopById(int id) {
		for (Stop stop : this.stops) {
			if (stop.getId() == id) {
				return stop;
			}
		}
		return null;
	}

	public String stopsString() {
		String result = "";
		for (Stop stop : stops) {
			result += stop.toString();
		}
		return result;
	}

	public int numberOfStops() {
		// Se ignoran el origen y el destino
		// return this.stops.size() - 2;
		return this.stops.size();
	}

	public int numberOfStudents() {
		int number = 0;
		for (Stop stop : this.stops) {
			number += stop.getTrackables().size();
		}
		return number;
	}

	// Obtener la lista de todos los trackables en esta ruta
	public ArrayList<Trackable> getAllTrackables() {
		ArrayList<Trackable> trackables = new ArrayList<Trackable>();
		for (Stop stop : this.stops) {
			trackables.addAll(stop.getTrackables());
		}

		// Collections.sort(trackables, new Trackable.TrackableComparator());
		trackables.addAll(trackablesFromNovelties);
		return trackables;
	}

	public ArrayList<AssistanceStudentListItem> getAllTrackablesWithStopHeaders() {
		ArrayList<AssistanceStudentListItem> trackables = new ArrayList<AssistanceStudentListItem>();
		for (Stop stop : this.stops) {
			if (stop.getTrackables().size() > 0) {
				trackables.add(new AssistanceStudentListHeader("Paradero "
						+ stop.getOrder()));
				ArrayList<Trackable> stopTrackables = stop.getTrackables();
				Collections.sort(stopTrackables,
						new Trackable.TrackableComparator());
				trackables.addAll(stopTrackables);
			}
		}
		// Collections.sort(trackables, new Trackable.TrackableComparator());
		return trackables;
	}
	
	// Obtener un trackable por id de la lista de todos los trackables, incluyendo aquellos con novedades
	public Trackable getTrackableById(String id) {
		ArrayList<Trackable> trackables = this.getAllTrackables();
		for (Trackable trackable : trackables) {
			if (trackable.getId().equals(id))
				return trackable;
		}
		return null;
	}

	// Obtener la hora estimada de llegada a un paradero segï¿½n su orden
	public String getEstimatedTimeToStop(int order) {
		String estimatedTimeToStop = "";
		int accumulatedEstimatedTime = 0;
		for (int i = 0; i <= order; i++) {
			accumulatedEstimatedTime += this.stops.get(i).getEstimatedTime();

		}
		// Pinta el tiempo estimado de llegada al ï¿½ltimo paradero

		startTime.add(Calendar.SECOND, accumulatedEstimatedTime);
		int seconds = startTime.get(Calendar.SECOND);
		int minutes = startTime.get(Calendar.MINUTE);
		int hours = startTime.get(Calendar.HOUR_OF_DAY);
		estimatedTimeToStop = "" + hours + ":"
				+ (minutes < 10 ? "0" + minutes : minutes) + ":"
				+ (seconds < 10 ? "0" + seconds : seconds);
		startTime.add(Calendar.SECOND, -accumulatedEstimatedTime);

		return estimatedTimeToStop;
	}

	public int getEstimatedTimeToStopInSeconds(int order) {
		int accumulatedEstimatedTime = 0;
		for (int i = 0; i <= order; i++) {
			accumulatedEstimatedTime += this.stops.get(i).getEstimatedTime();

		}
		return accumulatedEstimatedTime;
	}

	// Obtener la distancia (del taxista) desde el origen de la ruta hasta el
	// paradero dado
	public int getDistanceFromOriginToStop(int order) {
		int distance = 0;
		for (int i = 0; i <= order; i++) {
			distance += this.stops.get(i).getDistance();
		}
		return distance;
	}

	public String getTrackedStopTrackablesJSONString2() throws JSONException {
		JSONObject jsonObject = new JSONObject();
		ArrayList<Trackable> trackables = FreeModeManager.getInstance()
				.getRouteTrackables();
		for (Trackable trackable : trackables) {
			JSONObject jsonTrackable = new JSONObject();
			jsonTrackable.put("ID", "" + trackable.getTrackedStopTrackableId());
			jsonTrackable.put("PICKED", "" + trackable.getPicked());

			jsonObject.accumulate("TRACKEDSTOPTRACKABLES", jsonTrackable);
		}
		// Log.d("Opciï¿½n 1", jsonObject.toString());
		// Log.d("Opciï¿½n 2", jsonObject.getString("TRACKED_STOPS"));
		// Log.d("Opcion 3","{"+jsonObject.getString("TRACKED_STOPS")+"}");
		if (trackables.size() == 1)
			return "[" + jsonObject.getString("TRACKEDSTOPTRACKABLES") + "]";
		else
			return jsonObject.getString("TRACKEDSTOPTRACKABLES");

	}

	public String getTrackedStopTrackablesJSONString() throws JSONException {
		JSONObject jsonObject = new JSONObject();
		ArrayList<Trackable> trackables = getAllTrackables();
		for (Trackable trackable : trackables) {
			JSONObject jsonTrackable = new JSONObject();
			jsonTrackable.put("ID", "" + trackable.getTrackedStopTrackableId());
			jsonTrackable.put("PICKED", "" + trackable.getPicked());

			jsonObject.accumulate("TRACKEDSTOPTRACKABLES", jsonTrackable);
		}
		// Log.d("Opciï¿½n 1", jsonObject.toString());
		// Log.d("Opciï¿½n 2", jsonObject.getString("TRACKED_STOPS"));
		// Log.d("Opcion 3","{"+jsonObject.getString("TRACKED_STOPS")+"}");
		if (trackables.size() == 1)
			return "[" + jsonObject.getString("TRACKEDSTOPTRACKABLES") + "]";
		else
			return jsonObject.getString("TRACKEDSTOPTRACKABLES");

	}

	// Obtener el ï¿½ltimo paradero en el que se detuvo
	public Stop getLastStopWhereVehicleStopped() {
		Stop lastStop = null;
		for (Stop stop : stops) {
			if (stop.didStopHere()) {
				lastStop = stop;
			} else {
				break;
			}
		}
		return lastStop;
	}

	public ArrayList<Stop> findNeighboursOfStop(Stop stop) {
		// Primero, se busca el paradero más cercano, hacia atrás
		ArrayList<Stop> neighbours = new ArrayList<Stop>();

		Stop stopBehind = null;
		if (stop.getOrder() > 0) {
			for (int i = stop.getOrder() - 1; i >= 0; i--) {
				if (!this.stops.get(i).isCancelled()
						&& this.stops.get(i).trackablesToPickOrLeaveAtStop()) {
					stopBehind = this.stops.get(i);
					break;
				}
			}
		}
		neighbours.add(stopBehind);

		Stop stopAhead = null;
		if (stop.getOrder() < this.stops.size() - 1) {
			for (int i = stop.getOrder() + 1; i < this.stops.size(); i++) {
				if (!this.stops.get(i).isCancelled()
						&& this.stops.get(i).trackablesToPickOrLeaveAtStop()) {
					stopAhead = this.stops.get(i);
					break;
				}
			}
		}

		neighbours.add(stopAhead);

		return neighbours;
	}

	public double getAverageTimeDeviationFromStops() {
		double sumDeviations = -1;
		int totalDeviations = 0;
		for (Stop stop : this.stops) {
			if (stop.getTimeDeviation() != -1) {
				sumDeviations += stop.getTimeDeviation();
				totalDeviations++;
			}
		}
		if (totalDeviations != 0) {
			return sumDeviations / totalDeviations;
		}
		return -1;
	}

	public void addStop(int order, boolean isSpecial, double latitude,
			double longitude) {
		stops.add(new Stop(order, isSpecial, latitude, longitude));
	}

	public void addStop(int order, boolean isSpecial, double latitude,
			double longitude, int stopPosition) {
		stops.add(stopPosition, new Stop(order, isSpecial, latitude, longitude));
	}

	public User getMonitor() {
		return monitor;
	}

	public void setMonitor(User monitor) {
		this.monitor = monitor;
	}

	public ArrayList<Stop> getStopsWhereVehicleHasntStoppedExceptingNext(
			Stop nextStop) {
		ArrayList<Stop> selectedStops = new ArrayList<Stop>();
		for (Stop stop : this.stops) {
			if (!stop.didStopHere() && !stop.equals(nextStop)
					&& stop.getOrder() != this.stops.size() - 1
					&& !stop.isCancelled()
					&& stop.trackablesToPickOrLeaveAtStop()) {
				selectedStops.add(stop);
			}
		}
		return selectedStops;
	}

	public boolean shouldBeRunning() {
		return shouldBeRunning;
	}

	public void setShouldBeRunning(boolean shouldBeRunning) {
		this.shouldBeRunning = shouldBeRunning;
	}

	// Métodos del última generación

	public void addNovelty(Novelty novelty) {
		this.novelties.add(novelty);
	}

	public void addTrackableWithoutStop(Trackable trackable) {
		if(this.trackablesFromNovelties == null){
			this.trackablesFromNovelties = new ArrayList<Trackable>();
		}
		this.trackablesFromNovelties.add(trackable);
	}

	public Trackable findTrackableById(String trackableId) {
		for (Stop stop : this.stops) {
			Trackable trackable = stop.getTrackableById(trackableId);

			if (trackable != null) {
				return trackable;
			}
		}
		return null;
	}

	public Trackable removeTrackableFromStopById(String trackableId) {
		for (Stop stop : this.stops) {
			Trackable trackable = stop.getTrackableById(trackableId);

			if (trackable != null) {
				stop.getTrackables().remove(trackable);
				return trackable;
			}
		}
		return null;
	}

	public void addTrackableToStop(Trackable trackable, int stopId) {
		Stop stop = findStopById(stopId);
		stop.getTrackables().add(trackable);
	}

	public int numberOfCarNovelties() {
		int total = 0;
		ArrayList<Trackable> trackables = this.getAllTrackables();

		for (Trackable trackable : trackables) {
			if (trackable.getNovelty() != null) {
				if (trackable.getNovelty().isInCar()) {
					total++;
				}
			}
		}
		return total;
	}

	public int numberOfNotAttendingNovelties() {
		int total = 0;
		ArrayList<Trackable> trackables = this.getAllTrackables();

		for (Trackable trackable : trackables) {
			if (trackable.getNovelty() != null) {
				if (trackable.getNovelty().isNotAttending()) {
					total++;
				}
			}
		}
		return total;
	}

	public int numberOfOutOfCurrentRouteNovelties() {
		int total = 0;
		ArrayList<Trackable> trackables = this.getAllTrackables();
		for (Trackable trackable : trackables) {
			if (trackable.getNovelty() != null) {
				if (trackable.getNovelty().isOutOfCurrentRoute()) {
					total++;
				}
			}
		}
		return total;
	}

	public int numberOfIntoCurrentRouteNovelties() {
		int total = 0;
		ArrayList<Trackable> trackables = this.getAllTrackables();
		for (Trackable trackable : trackables) {
			if (trackable.getNovelty() != null) {
				if (trackable.getNovelty().isIntoCurrentRoute()) {
					total++;
				}
			}
		}
		return total;
	}

	public int numberOfChangingStopNovelties() {
		int total = 0;
		ArrayList<Trackable> trackables = this.getAllTrackables();
		for (Trackable trackable : trackables) {
			if (trackable.getNovelty() != null) {
				if (trackable.getNovelty().isChangingStop()) {
					total++;
				}
			}
		}
		return total;
	}

	public int numberOfTotalNoveties() {
		return numberOfChangingStopNovelties()
				+ numberOfIntoCurrentRouteNovelties()
				+ numberOfOutOfCurrentRouteNovelties()
				+ numberOfNotAttendingNovelties()
				+ numberOfCarNovelties();
	}

	public int getNumberOfStopOfTrackable(String trackableId) {
		int numberOfStop = 0;
		if(stops != null){
    		for (Stop stop : this.stops) {
    			for (Trackable trackables : stop.getTrackables()) {
    				if (trackables.getId().equals(trackableId)) {
    					return numberOfStop;
    				}
    			}
    			numberOfStop++;
    		}
		}
		return -1;
	}

	public ArrayList<Trackable> getTrackables() {
		return trackables;
	}

	public void setTrackables(ArrayList<Trackable> trackables) {
		this.trackables = trackables;
	}
	
	public Stop getStop( int stopID )
	{
	    Stop stop = null;
	    
	    for( Stop actual : stops )
	    {
	        if( actual.getId( ) == stopID )
	        {
	            stop = actual;
	            break;
	        }
	    }
	    
	    return stop;
	}

    public Date getModificationDate( )
    {
        return modificationDate;
    }

    public void setModificationDate( Date modificationDate )
    {
        this.modificationDate = modificationDate;
    }

    public String getModifiedBy( )
    {
        return modifiedBy;
    }

    public void setModifiedBy( String modifiedBy )
    {
        this.modifiedBy = modifiedBy;
    }

	public int getNumberOfStops( )
	{
		return numberOfStops;
	}

	public void setAllTrackablesAsNotAsSchool(){
		for(Stop stop: this.stops){
			for(Trackable trackable: stop.getTrackables()){
				if(trackable.getNovelty() == null){
					trackable.setPicked(Trackable.NOT_AT_SCHOOL);
				}else if(!trackable.getNovelty().isOutOfCurrentRoute()) {
					if (!trackable.getNovelty().isOutOfCurrentRoute()) {
						trackable.setPicked(Trackable.NOT_AT_SCHOOL);
					}
				}
			}
		}

		for(Trackable trackable: this.trackablesFromNovelties){
			trackable.setPicked(Trackable.NOT_AT_SCHOOL);
		}
	}

	public int getNumberOfUnknownExceptingNovelties(){
		int numberOfUnknown = 0;
		for(Stop stop: this.stops){
			for(Trackable trackable: stop.getTrackables()){
				if(trackable.getPicked()==Trackable.UNKNOWN){
					if(trackable.getNovelty()==null) {
						numberOfUnknown++;
					}else{
						if(trackable.getNovelty().getType() == Trackable.NOVELTY_TYPE_CHANGE_ROUTE && !trackable.getNovelty().getFkRouteOrigin().equals(this.getId())){
							numberOfUnknown++;
						}
					}
				}
			}
		}
		for(Trackable trackable: this.trackablesFromNovelties){
			if(trackable.getPicked()==Trackable.UNKNOWN){
				numberOfUnknown++;
			}
		}
		return numberOfUnknown;
	}

	public void setAllTrackablesAsUnknown(){
		for(Stop stop: this.stops){
			for(Trackable trackable: stop.getTrackables()){
				trackable.setPicked(Trackable.UNKNOWN);
			}
		}

		for(Trackable trackable: this.trackablesFromNovelties){
			trackable.setPicked(Trackable.UNKNOWN);
		}
	}

	public String getOrganizationId( )
	{
		return organizationId;
	}

	public boolean isHasFinished() {
		return hasFinished;
	}

	public void setHasFinished(boolean hasFinished) {
		this.hasFinished = hasFinished;
	}


}
