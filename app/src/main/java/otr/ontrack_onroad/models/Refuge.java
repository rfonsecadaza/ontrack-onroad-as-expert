package otr.ontrack_onroad.models;

/**
 * Created by rafaelmunoz on 9/18/15.
 */
public class Refuge
{
    private String name;

    private String responsible;

    private String type;

    private String mobilePhone;

    private double latitude;

    private double longitude;

    public Refuge( String name, String responsible, String type, String mobilePhone, double latitude, double longitude )
    {
        this.name = name;
        this.responsible = responsible;
        this.type = type;
        this.mobilePhone = mobilePhone;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getName( )
    {
        return name;
    }

    public String getResponsible( )
    {
        return responsible;
    }

    public String getType( )
    {
        return type;
    }

    public String getMobilePhone( )
    {
        return mobilePhone;
    }

    public double getLatitude( )
    {
        return latitude;
    }

    public double getLongitude( )
    {
        return longitude;
    }

    public String toString( )
    {
        return name + "|" + responsible + "|" + type + "|" + mobilePhone + "|" + latitude + "|" + longitude;
    }
}
