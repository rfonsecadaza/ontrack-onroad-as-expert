package otr.ontrack_onroad.models;

import java.util.Comparator;

public class Point
{
    public static final String STEPS_KEY = "STEPS";
    public static final String LONGITUDE_KEY = "LONGITUDE";
    public static final String LATITUDE_KEY = "LATITUDE";
    public static final String ALTITUDE_KEY = "ALTITUDE";
    public static final String POSITION_KEY = "POSITION";

    private double longitude;
    private double latitude;
    private double altitude;



    private int order;

    public Point(double longitude, double latitude, double altitude)
    {
        this.longitude = longitude;
        this.latitude = latitude;
        this.altitude = altitude;
        this.order = -1;
    }

    public Point(double longitude, double latitude, double altitude, int order)
    {
        this.longitude = longitude;
        this.latitude = latitude;
        this.altitude = altitude;
        this.order = order;
    }

    public double getLongitude()
    {
        return longitude;
    }

    public void setLongitude(double longitude)
    {
        this.longitude = longitude;
    }

    public double getLatitude()
    {
        return latitude;
    }

    public void setLatitude(double latitude)
    {
        this.latitude = latitude;
    }

    public double getAltitude()
    {
        return altitude;
    }

    public void setAltitude(double altitude)
    {
        this.altitude = altitude;
    }

    public int getOrder()
    {
        return order;
    }

    public void setOrder(int order)
    {
        this.order = order;
    }

    public static class PointComparator implements Comparator<Point>
    {

        @Override
        public int compare(Point point1, Point point2)
        {
            Integer point1order = point1.getOrder();
            Integer point2order = point2.getOrder();
            return point1order.compareTo(point2order);
        }
    }

}
