package otr.ontrack_onroad.models;

import java.util.ArrayList;

public class Organization
{
    public final static String ORGANIZATION_KEY = "ORGANIZATION";

    public final static String ID_KEY = "ID";

    public final static String TITLE_KEY = "TITLE";

    public final static String DESCRIPTION_KEY = "DESCRIPTION";

    public final static String IMAGE_KEY = "IMAGE";

    public final static String MOBILE_PHONE_KEY = "MOBILE_PHONE";

    public final static String LOCAL_PHONE_KEY = "LOCAL_PHONE";

    public final static String EMAIL_KEY = "EMAIL";

    public final static String ADDRESS_KEY = "ADDRESS";

    public final static String LATITUDE_KEY = "LATITUDE";

    public final static String LONGITUDE_KEY = "LONGITUDE";

    public final static String CREATED_BY_KEY = "CREATED_BY";

    private String id;

    private String title;

    private String description;

    private String urlImage;

    private String mobilePhone;

    private String localPhone;

    private String eMail;

    private String address;

    private double latitude;

    private double longitude;

    private ArrayList<Trackable> trackables;

    private ArrayList<Route> routes;

    private ArrayList<Route> routesFreeMode;

    private String createdBy;

    public Organization( String id, String title, String description, String urlImage, String mobilePhone, String localPhone, String eMail, String address, double latitude, double longitude, String createdBy )
    {
        super( );
        this.id = id;
        this.title = title;
        this.description = description;
        this.urlImage = urlImage;
        this.mobilePhone = mobilePhone;
        this.localPhone = localPhone;
        this.eMail = eMail;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
        this.createdBy = createdBy;
        trackables = new ArrayList<Trackable>( );
        this.routesFreeMode = new ArrayList<Route>();
    }

    public String getId( )
    {
        return id;
    }

    public void setId( String id )
    {
        this.id = id;
    }

    public String getTitle( )
    {
        return title;
    }

    public void setTitle( String title )
    {
        this.title = title;
    }

    public String getDescription( )
    {
        return description;
    }

    public void setDescription( String description )
    {
        this.description = description;
    }

    public String getUrlImage( )
    {
        return urlImage;
    }

    public void setUrlImage( String urlImage )
    {
        this.urlImage = urlImage;
    }

    public String getMobilePhone( )
    {
        return mobilePhone;
    }

    public void setMobilePhone( String mobilePhone )
    {
        this.mobilePhone = mobilePhone;
    }

    public String getLocalPhone( )
    {
        return localPhone;
    }

    public void setLocalPhone( String localPhone )
    {
        this.localPhone = localPhone;
    }

    public String geteMail( )
    {
        return eMail;
    }

    public void seteMail( String eMail )
    {
        this.eMail = eMail;
    }

    public String getAddress( )
    {
        return address;
    }

    public void setAddress( String address )
    {
        this.address = address;
    }

    public double getLatitude( )
    {
        return latitude;
    }

    public void setLatitude( double latitude )
    {
        this.latitude = latitude;
    }

    public double getLongitude( )
    {
        return longitude;
    }

    public void setLongitude( double longitude )
    {
        this.longitude = longitude;
    }

    public ArrayList<Trackable> getTrackables( )
    {
        return trackables;
    }

    public void setTrackables( ArrayList<Trackable> trackables )
    {
        this.trackables = trackables;
    }

    public void addTrackable( Trackable trackable )
    {
        trackables.add( trackable );
    }

    public ArrayList<Route> getRoutes( )
    {
        return routes;
    }

    public void setRoutes( ArrayList<Route> routes )
    {
        this.routes = routes;
    }

    public String getCreatedBy( )
    {
        return createdBy;
    }

    public void configureRoutesFreeModeFromRoutes(){
        this.routesFreeMode.clear();
        for(Route route: this.routes){
            if(route.getNumberOfStops()==0){
                this.routesFreeMode.add(route);
            }
        }
    }

    public ArrayList<Route> getRoutesFreeMode(){
        return this.routesFreeMode;
    }

}
