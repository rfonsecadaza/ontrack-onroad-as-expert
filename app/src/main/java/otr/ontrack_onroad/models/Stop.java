package otr.ontrack_onroad.models;

import java.util.ArrayList;
import java.util.Comparator;

import org.json.JSONException;
import org.json.JSONObject;

//import com.google.android.gms.maps.model.Polyline;

import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.utils.Timestamp;
import otr.ontrack_onroad_debug.activities.R;

import android.content.Context;
import android.location.Location;
import android.util.Log;

import com.mapbox.mapboxsdk.annotations.Polyline;

public class Stop implements Comparable<Stop> {

	public static final String STOPS_KEY = "STOPS";
	public static final String STOP_KEY = "STOP";
	public static final String ID_KEY = "ID";
	public static final String LONGITUDE_KEY = "LONGITUDE";
	public static final String LATITUDE_KEY = "LATITUDE";
	public static final String ALTITUDE_KEY = "ALTITUDE";
	public static final String TRACKABLES_KEY = "TRACKABLES";
	public static final String POINTS_KEY = "POINTS";
	public static final String POSITION_KEY = "POSITION";
	public static final String AVERAGE_TIME_KEY = "AVERAGE_TIME";
	public static final String TIME_DEVIATION_KEY = "TIME_DEVIATION";
	public static final String FK_STOP_KEY = "FK_STOP";
	public static final String TRACKED_STOPS_KEY = "TRACKED_STOPS";
	public static final String TRACKED_STOP_TRACKABLES_KEY = "TRACKEDSTOPTRACKABLES";
	public static final String SPECIAL_KEY = "SPECIAL";
	public static final String STEPS_KEY = "STEPS";
	public static final String ESTIMATED_TIME_KEY = "ESTIMATED_TIME";
	public static final String ADDRESS_KEY = "ADDRESS";
	public static final String DISTANCE_KEY = "DISTANCE";

	public static final double MINIMUM_TIME_AT_STOP = 2; // Seconds
	public static final double APPROACH_DISTANCE_TO_STOP = 120; // Meters
	public static final double CLOSE_DISTANCE_TO_STOP = 60; // Meters
	public static final double VERY_CLOSE_DISTANCE_TO_STOP = 30; // Meters
	public static final double MAXIMUM_DISTANCE_TO_STOP = 30; // Meters
	public static final double MAXIMUM_TIME_UNTIL_DELAY = 120; // Seconds

	public static final double MAXIMUM_ALLOWED_DEVIATION = 10 * 60.0; // In
																		// seconds

	private int id;

	private int estimatedTime; // En segundos
	private int estimatedTimeGoogle; // En segundos
	private int originalEstimatedTime;

	private int distance; // En metros
	private int originalDistance;

	private String realTime; // Solo la hora a la que llegó el vehï¿½culo al
								// paradero.

	private String departureTime;

	private int trackedStopId;

	private double longitude;
	private double latitude;
	private double altitude;

	private boolean special;

	private int order;
	private ArrayList<Trackable> trackables;

	private ArrayList<Point> wayPoints;

	private ArrayList<Point> steps;

	private boolean didStopHere;

	private String direction;

	private int googleETAToStopFromLastStop;
	private int googleDistanceToStopFromLastStop;

	// Tags que informan si ya fueron enviadas las notificaciones de distancia
	private boolean reported200m;
	private boolean reported500m;
	private boolean reported1km;

	// Tags que informan si ya fueron enviadas las notificaciones de distancia
	private boolean reported5min;
	private boolean reported10min;

	// Statistics
	private int n;
	private int etaSinceRouteStartSum;

	// Variable para detectar si el paradero fue cancelado por el conductor
	private boolean cancelled;

	private String averageTime;
	private double timeDeviation;

	private ArrayList<Polyline> polylines;

	public Stop( )
	{
	    
	}
	
	public Stop(int order, boolean isSpecial, double latitude, double longitude) {
		this.order = order;
		this.special = isSpecial;
		this.latitude = latitude;
		this.longitude = longitude;
		this.steps = new ArrayList<Point>();
		this.polylines = new ArrayList<Polyline>();
		trackables = new ArrayList<Trackable>();
	}

	public Stop(int id, double longitude, double latitude, double altitude,
			ArrayList<Trackable> trackables, ArrayList<Point> wayPoints,
			int order, boolean special, String averageTime, double timeDeviation) {
		this.setId(id);
		this.longitude = longitude;
		this.latitude = latitude;
		this.altitude = altitude;
		this.trackables = trackables;
		this.wayPoints = wayPoints;
		this.order = order;
		this.special = special;

		googleDistanceToStopFromLastStop = -1;
		googleETAToStopFromLastStop = -1;

		didStopHere = false;

		realTime = null;

		this.departureTime = null;

		estimatedTime = 0;

		reported200m = false;
		reported500m = false;
		reported1km = false;

		reported5min = false;
		reported10min = false;

		this.cancelled = false;

		this.setSteps(new ArrayList<Point>());

		this.averageTime = averageTime;
		this.timeDeviation = timeDeviation;
		this.polylines = new ArrayList<Polyline>();
	}

	public Stop(int id, double longitude, double latitude, double altitude,
			ArrayList<Trackable> trackables, ArrayList<Point> steps,
			ArrayList<Point> wayPoints, int order, boolean special,
			String averageTime, double timeDeviation) {
		this.setId(id);
		this.longitude = longitude;
		this.latitude = latitude;
		this.altitude = altitude;
		this.trackables = trackables;
		this.steps = steps;
		this.computeDistanceFromSteps();
		this.wayPoints = wayPoints;
		this.order = order;
		this.special = special;

		googleDistanceToStopFromLastStop = -1;
		googleETAToStopFromLastStop = -1;

		didStopHere = false;

		realTime = null;

		this.departureTime = null;

		this.estimatedTime = 0;
		this.estimatedTimeGoogle = 0;

		reported200m = false;
		reported500m = false;
		reported1km = false;

		reported5min = false;
		reported10min = false;

		this.cancelled = false;

		this.averageTime = averageTime;
		this.timeDeviation = timeDeviation;
		this.polylines = new ArrayList<Polyline>();
	}

	public Stop(int id, double longitude, double latitude, double altitude,
			ArrayList<Trackable> trackables, ArrayList<Point> steps,
			ArrayList<Point> wayPoints, int order, boolean special,
			String averageTime, double timeDeviation, int estimatedTimeGoogle,
			String direction) {
		this.setId(id);
		this.longitude = longitude;
		this.latitude = latitude;
		this.altitude = altitude;
		this.trackables = trackables;
		this.steps = steps;
		this.computeDistanceFromSteps();
		this.wayPoints = wayPoints;
		this.order = order;
		this.special = special;

		googleDistanceToStopFromLastStop = -1;
		googleETAToStopFromLastStop = -1;

		didStopHere = false;

		realTime = null;

		this.departureTime = null;

		this.estimatedTime = 0;
		this.estimatedTimeGoogle = estimatedTimeGoogle;

		reported200m = false;
		reported500m = false;
		reported1km = false;

		reported5min = false;
		reported10min = false;

		this.cancelled = false;

		this.averageTime = averageTime;
		this.timeDeviation = timeDeviation;

		this.estimatedTime = estimatedTime;

		this.direction = direction;
		this.polylines = new ArrayList<Polyline>();
	}

	public Stop(double longitude, double latitude, double altitude,
			ArrayList<Trackable> trackables, ArrayList<Point> wayPoints,
			int order) {
		this.longitude = longitude;
		this.latitude = latitude;
		this.altitude = altitude;
		this.trackables = trackables;
		this.wayPoints = wayPoints;
		this.order = order;

		googleDistanceToStopFromLastStop = -1;
		googleETAToStopFromLastStop = -1;

		didStopHere = false;

		realTime = null;

		this.departureTime = null;

		this.estimatedTime = 0;
		this.estimatedTimeGoogle = 0;

		distance = 0;

		reported200m = false;
		reported500m = false;
		reported1km = false;

		this.setSteps(new ArrayList<Point>());
		this.polylines = new ArrayList<Polyline>();
	}

	public int getEstimatedTime() {
		return estimatedTime;
	}

	public void setEstimatedTime(int timeToStop) {
		this.estimatedTime = timeToStop;
	}

	public int getEstimatedTimeGoogle() {
		return estimatedTimeGoogle;
	}

	public void setEstimatedTimeGoogle(int estimatedTimeGoogle) {
		this.estimatedTimeGoogle = estimatedTimeGoogle;
	}

	public int getOriginalEstimatedTime() {
		return originalEstimatedTime;
	}

	public void setOriginalEstimatedTime(int originalEstimatedTime) {
		this.originalEstimatedTime = originalEstimatedTime;
	}

	public int getDistance() {
		return distance;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}

	public int getOriginalDistance() {
		return originalDistance;
	}

	public void setOriginalDistance(int originalDistance) {
		this.originalDistance = originalDistance;
	}

	public String getRealTime() {
		return realTime;
	}

	public void setRealTime(String realTime) {
		this.realTime = realTime;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public Location getLocation() {
		Location location = new Location("");
		location.setLatitude(latitude);
		location.setLongitude(longitude);

		return location;
	}

	public double getAltitude() {
		return altitude;
	}

	public void setAltitude(double altitude) {
		this.altitude = altitude;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public ArrayList<Trackable> getTrackables() {
		return trackables;
	}

	public void setTrackables(ArrayList<Trackable> trackables) {
		this.trackables = trackables;
	}

	public boolean isDidStopHere() {
		return didStopHere;
	}

	public void setDidStopHere(boolean didStopHere) {
		this.didStopHere = didStopHere;
	}

	public boolean didStopHere() {
		return didStopHere;
	}

	public boolean isSpecial() {
		return special;
	}

	public void setSpecial(boolean special) {
		this.special = special;
	}

	public boolean isReported200m() {
		return reported200m;
	}

	public void setReported200m(boolean reported200m) {
		this.reported200m = reported200m;
	}

	public boolean isReported500m() {
		return reported500m;
	}

	public void setReported500m(boolean reported500m) {
		this.reported500m = reported500m;
	}

	public boolean isReported1km() {
		return reported1km;
	}

	public void setReported1km(boolean reported1km) {
		this.reported1km = reported1km;
	}

	public boolean isReported5min() {
		return reported5min;
	}

	public void setReported5min(boolean reported5min) {
		this.reported5min = reported5min;
	}

	public boolean isReported10min() {
		return reported10min;
	}

	public void setReported10min(boolean reported10min) {
		this.reported10min = reported10min;
	}

	public boolean isCancelled() {
		return cancelled;
	}

	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}

	public ArrayList<Point> getSteps() {
		return steps;
	}

	public void setSteps(ArrayList<Point> points) {
		this.steps = points;
	}

	public void addSteps(ArrayList<Point> points) {
		if (this.steps.isEmpty()) {
			this.steps.addAll(points);
		} else {
			points.remove(0);
			this.steps.addAll(points);
		}
	}

	public void vehicleJustStoppedHere() {
		this.didStopHere = true;
		this.setRealTime(Timestamp.completeTimestamp());
		n++;
		etaSinceRouteStartSum += OnTrackManager.getInstance()
				.getTimeSinceRouteStarted();
	}

	public double getAverageEtaToThisStopFromRouteStart() {
		return n != 0 ? etaSinceRouteStartSum / n : 0;
	}

	public int getExpectedETAFromRouteStart() {
		return n != 0 ? etaSinceRouteStartSum / n : 0;
	}

	public int getGoogleETAToStopFromLastStop() {
		return googleETAToStopFromLastStop;
	}

	public void setGoogleETAToStopFromLastStop(int eta) {
		this.googleETAToStopFromLastStop = eta;
	}

	public int getGoogleDistanceToStopFromLastStop() {
		return googleDistanceToStopFromLastStop;
	}

	public void setGoogleDistanceToStopFromLastStop(
			int googleDistanceToStopFromLastStop) {
		this.googleDistanceToStopFromLastStop = googleDistanceToStopFromLastStop;
	}

	public int getGoogleETAToStopFromCurrentLocation() {
		return googleETAToStopFromLastStop
				- OnTrackManager.getInstance().getTimeSinceLastStop();
	}

	// Distancia euclideana
	public int getDistanceToStopFromCurrentLocation() {
		return (int) OnTrackManager.getInstance().getCurrentLocation()
				.distanceTo(getLocation());
	}

	public String getDirection() {
		return direction != null ? direction.split("Bogot")[0]
				: "Sin direcciï¿½n";
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public ArrayList<Point> getWayPoints() {
		return wayPoints;
	}

	public void setWayPoints(ArrayList<Point> wayPoints) {
		this.wayPoints = wayPoints;
	}

	public int getTrackedStopId() {
		return trackedStopId;
	}

	public void setTrackedStopId(int trackedStopId) {
		this.trackedStopId = trackedStopId;
	}

	public String getAverageTime() {
		return averageTime;
	}

	public void setAverageTime(String averageTime) {
		this.averageTime = averageTime;
	}

	public double getTimeDeviation() {
		return timeDeviation;
	}

	public void setTimeDeviation(double timeDeviation) {
		this.timeDeviation = timeDeviation;
	}

	public static class StopComparator implements Comparator<Stop> {

		@Override
		public int compare(Stop stop1, Stop stop2) {
			Integer stop1order = stop1.getOrder();
			Integer stop2order = stop2.getOrder();
			return stop1order.compareTo(stop2order);
		}
	}

	public int getNumberOfTrackables() {
		return trackables.size();
	}

	public String toString() {
		return "{id =" + this.id + "\ntrackedStopId = " + this.trackedStopId
				+ "}\nDidStopHere = " + didStopHere;
	}

	public int getEstimatedTimeInMinutes() {
		return this.estimatedTime / 60;
	}

	public Trackable getTrackableById(String id) {
		for (Trackable trackable : this.trackables) {
			if (trackable.getId().equals(id))
				return trackable;
		}
		return null;
	}

	public Trackable getTrackableByIndex(int index) {
		if (index < trackables.size()) {
			return trackables.get(index);
		}
		return null;
	}

	public String getTrackedStopTrackablesJSONString() throws JSONException {
		JSONObject jsonObject = new JSONObject();
		for (Trackable trackable : this.trackables) {
			JSONObject jsonTrackable = new JSONObject();
			jsonTrackable.put("ID", "" + trackable.getTrackedStopTrackableId());
			jsonTrackable.put("PICKED", "" + trackable.getPicked());

			jsonObject.accumulate("TRACKEDSTOPTRACKABLES", jsonTrackable);
		}

		if (trackables.size() == 1)
			return "[" + jsonObject.getString("TRACKEDSTOPTRACKABLES") + "]";
		else
			return jsonObject.getString("TRACKEDSTOPTRACKABLES");

	}

	public void changeUnknownToPickedOrDeliveredTrackables() {
		for (Trackable trackable : this.trackables) {
			if (trackable.getPicked() != Trackable.NOT_ASSISTING_DELIVERED
					&& trackable.getPicked() != Trackable.NOT_AT_SCHOOL
					&& trackable.getState() == Trackable.STATE_ATTENDANCE)
				trackable.setPicked(Trackable.ASSISTING_DELIVERED);
		}
	}

	public void addDelayToEstimatedTime(int delay) {
		this.estimatedTime += delay;
	}

	public boolean trackablesToPickOrLeaveAtStop() {
		if (this.trackables.size() == 0) {
			return true;
		}
		
		for (Trackable trackable : this.trackables) {
			Novelty novelty = trackable.getNovelty();
			boolean isNoveltyNotAttending = novelty!=null?novelty.isNotAttending()||novelty.isOutOfCurrentRoute()||novelty.isInCar():false;
			if (trackable.getState() != Trackable.STATE_NOT_ATTENDANCE
					&& trackable.getPicked() != Trackable.NOT_AT_SCHOOL && !isNoveltyNotAttending)
				return true;
		}

		return false;
	}

	public String namesOfTrackablesToPickOrLeaveAtStop() {
		//Para textos en ecuatoriano

		Organization organization = OnTrackManager.getInstance().getUser().getOrganizationByID(OnTrackManager.getInstance().getRoutes().get(OnTrackManager.getInstance().getSelectedRoute()).getOrganizationId());
		boolean ecuatorian = organization.getCreatedBy().equals("superadmin@ontrackschool.ec");

		Context context = OnTrackManager.getInstance().getContext();
		String names = "";
		if (numberOfTrackablesToPickOrLeaveAtStop() == 0) {

			names = context.getResources().getString(ecuatorian? R.string.dialog_cancel_stop_no_trackables_ecuador:R.string.dialog_cancel_stop_no_trackables);
		} else {
			names = context.getResources().getString(ecuatorian? R.string.dialog_cancel_stop_trackables_open_ecuador:R.string.dialog_cancel_stop_trackables_open);
			for (Trackable trackable : this.trackables) {
				names += trackable.getFullname() + "\n";
			}
		}
		return names;
	}

	public int numberOfTrackablesToPickOrLeaveAtStop() {
		int number = 0;
		for (Trackable trackable : this.trackables) {
			Novelty novelty = trackable.getNovelty();
			boolean isNoveltyNotAttending = novelty!=null?novelty.isNotAttending()||novelty.isOutOfCurrentRoute():false;
			if (trackable.getState() != Trackable.STATE_NOT_ATTENDANCE
					&& trackable.getPicked() != Trackable.NOT_AT_SCHOOL && !isNoveltyNotAttending)
				number++;
		}
		return number;
	}

	public boolean isEnabledForEstimatedTimeFromAverage() {
		return this.getOrder() == 0
				|| (timeDeviation != -1 && timeDeviation != 0
						&& !averageTime.equals("null") && timeDeviation <= MAXIMUM_ALLOWED_DEVIATION);
	}

	public void computeDistanceFromSteps() {
		double previousDistance = 0.0;
		if (this.steps.size() > 1) {
			for (int i = 0; i < steps.size() - 1; i++) {
				Point currentStep = steps.get(i);
				Point nextStep = steps.get(i + 1);
				Location currentStepLocation = new Location("");
				currentStepLocation.setLatitude(currentStep.getLatitude());
				currentStepLocation.setLongitude(currentStep.getLongitude());
				Location nextStepLocation = new Location("");
				nextStepLocation.setLatitude(nextStep.getLatitude());
				nextStepLocation.setLongitude(nextStep.getLongitude());
				previousDistance += nextStepLocation
						.distanceTo(currentStepLocation);
			}
		}
		this.distance = (int) Math.round(previousDistance);
		this.originalDistance = (int) Math.round(previousDistance);
	}

	public ArrayList<Polyline> getPolylines() {
		return polylines;
	}

	public void addPolyline(Polyline polyline) {
		polylines.add(polyline);
	}

	public void clearPolylines() {
		if (polylines != null) {
			for (Polyline polyline : polylines) {
				polyline.remove();
			}

			polylines.clear();
		}

	}

	public void increaseEstimatedTime(int delta) {
		this.estimatedTime += delta;
	}

	public void increaseAverageTime(double delta) {
		if (this.averageTime != "null") {
			double averageTimeSeconds = 0;
			try {
				averageTimeSeconds = Timestamp
                        .getSecondsFromHHMMSS(this.averageTime);
				averageTimeSeconds += delta;
				this.averageTime = Timestamp
						.getHHMMSSFromSeconds(averageTimeSeconds);
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	}

    @Override
    public int compareTo( Stop stop )
    {
        return order - stop.getOrder( );
    }

	public String getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}

	public boolean hasSteps(){
		boolean hasSteps = false;
		if(this.steps!= null){
			if(this.steps.size()>0){
				hasSteps = true;
			}
		}
		return hasSteps;
	}
}
