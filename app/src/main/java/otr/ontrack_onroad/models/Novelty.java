package otr.ontrack_onroad.models;

import otr.ontrack_onroad.managers.OnTrackManager;

public class Novelty {

	private int serverId;
	private String fkTrackable;
	private String fromDate;
	private String toDate;
	private boolean monday;
	private boolean tuesday;
	private boolean wednesday;
	private boolean thursday;
	private boolean friday;
	private boolean saturday;
	private boolean sunday;
	private int day;
	private String periodicity;
	private String fkRouteOrigin;
	private String fkRouteDestination;
	private String stopOrigin;
	private String stopDestination;
	private String stopAddress;
	private String description;
	private int loopType;
	private String createdBy;
	private int state;
	private String creationDate;
	private int type;
	private int intermittence;
	private String hour;
	private String parentName;
	private String identificationParent;



	public Novelty(int serverId, String fkTrackable, String fromDate,
			String toDate, boolean monday, boolean tuesday, boolean wednesday,
			boolean thursday, boolean friday, boolean saturday, boolean sunday,
			int day, String periodicity, String fkRouteOrigin,
			String fkRouteDestination, String stopOrigin,
			String stopDestination, String stopAddress, String description,
			int loopType, String createdBy, int state, String creationDate,
			int type, int intermittence, String hour, String parentName, String identificationParent) {
		this.serverId = serverId;
		this.fkTrackable = fkTrackable;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.monday = monday;
		this.tuesday = tuesday;
		this.wednesday = wednesday;
		this.thursday = thursday;
		this.friday = friday;
		this.saturday = saturday;
		this.sunday = sunday;
		this.day = day;
		this.periodicity = periodicity;
		this.fkRouteOrigin = fkRouteOrigin;
		this.fkRouteDestination = fkRouteDestination;
		this.stopOrigin = stopOrigin;
		this.stopDestination = stopDestination;
		this.stopAddress = stopAddress;
		this.description = description;
		this.loopType = loopType;
		this.createdBy = createdBy;
		this.state = state;
		this.creationDate = creationDate;
		this.type = type;
		this.intermittence = intermittence;
		this.hour = hour;
		this.parentName = parentName;
		this.identificationParent = identificationParent;
	}

	public int getServerId() {
		return serverId;
	}

	public void setServerId(int serverId) {
		this.serverId = serverId;
	}

	public String getFkTrackable() {
		return fkTrackable;
	}

	public void setFkTrackable(String fkTrackable) {
		this.fkTrackable = fkTrackable;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public boolean isMonday() {
		return monday;
	}

	public void setMonday(boolean monday) {
		this.monday = monday;
	}

	public boolean isTuesday() {
		return tuesday;
	}

	public void setTuesday(boolean tuesday) {
		this.tuesday = tuesday;
	}

	public boolean isWednesday() {
		return wednesday;
	}

	public void setWednesday(boolean wednesday) {
		this.wednesday = wednesday;
	}

	public boolean isThursday() {
		return thursday;
	}

	public void setThursday(boolean thursday) {
		this.thursday = thursday;
	}

	public boolean isFriday() {
		return friday;
	}

	public void setFriday(boolean friday) {
		this.friday = friday;
	}

	public boolean isSaturday() {
		return saturday;
	}

	public void setSaturday(boolean saturday) {
		this.saturday = saturday;
	}

	public boolean isSunday() {
		return sunday;
	}

	public void setSunday(boolean sunday) {
		this.sunday = sunday;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public String getPeriodicity() {
		return periodicity;
	}

	public void setPeriodicity(String periodicity) {
		this.periodicity = periodicity;
	}

	public String getFkRouteOrigin() {
		return fkRouteOrigin;
	}

	public void setFkRouteOrigin(String fkRouteOrigin) {
		this.fkRouteOrigin = fkRouteOrigin;
	}

	public String getFkRouteDestination() {
		return fkRouteDestination;
	}
	
	public String getFkRouteDestinationWithoutDash() {
		String [] fkRouteDestinationSplit = fkRouteDestination.split("-");
		return fkRouteDestinationSplit[0];
	}

	public void setFkRouteDestination(String fkRouteDestination) {
		this.fkRouteDestination = fkRouteDestination;
	}

	public String getStopAddress() {
		return stopAddress;
	}

	public void setStopAddress(String stopAddress) {
		this.stopAddress = stopAddress;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getLoopType() {
		return loopType;
	}

	public void setLoopType(int loopType) {
		this.loopType = loopType;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getIntermittence() {
		return intermittence;
	}

	public void setIntermittence(int intermittence) {
		this.intermittence = intermittence;
	}

	public String getStopOrigin() {
		return stopOrigin;
	}

	public void setStopOrigin(String stopOrigin) {
		this.stopOrigin = stopOrigin;
	}

	public String getStopDestination() {
		return stopDestination;
	}

	public void setStopDestination(String stopDestination) {
		this.stopDestination = stopDestination;
	}

	public boolean isNotAttending() {
		return this.type == Trackable.NOVELTY_TYPE_NOT_ATTENDING;
	}

	public boolean isInCar(){
		return this.type == Trackable.NOVELTY_TYPE_CAR;
	}

	public boolean isOutOfCurrentRoute() {
		if (!isNotAttending() && !isInCar()) {
			return this.fkRouteOrigin.equals(OnTrackManager.getInstance()
					.getUser().getRoute().getId())
					&& !this.fkRouteDestination.equals(OnTrackManager
							.getInstance().getUser().getRoute().getId());
		}
		return false;
	}
	
	public boolean isIntoCurrentRoute() {
		if (!isNotAttending()&& !isInCar()) {
			return !this.fkRouteOrigin.equals(OnTrackManager.getInstance()
					.getUser().getRoute().getId())
					&& this.fkRouteDestination.equals(OnTrackManager
							.getInstance().getUser().getRoute().getId());
		}
		return false;
	}
	
	public boolean isChangingStop(){
		if (!isNotAttending()&& !isInCar()) {
			return this.fkRouteOrigin.equals(this.fkRouteDestination);
		}
		return false;
	}

	public String getHour() {
		return hour;
	}

	public void setHour(String hour) {
		this.hour = hour;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public String getIdentificationParent() {
		return identificationParent;
	}

	public void setIdentificationParent(String identificationParent) {
		this.identificationParent = identificationParent;
	}


}