package otr.ontrack_onroad.models;

import com.mapbox.mapboxsdk.annotations.PolylineOptions;
import com.mapbox.mapboxsdk.views.MapView;

import java.util.ArrayList;

//import com.google.android.gms.maps.model.Marker;
//import com.google.android.gms.maps.model.Polyline;

//import com.mapbox.mapboxsdk.overlay.Marker;
//import com.mapbox.mapboxsdk.overlay.PathOverlay;

public class Portion implements Comparable<Portion> {

   	private PolylineOptions lines;
//	private Marker marker;
	private int order;
//
	public Portion( int order ){
		this.order = order;
	}
//
	public int getOrder( )
	{
	    return order;
	}

	public void setOrder( int order )
	{
	    this.order = order;
	}
//
	public Portion(PolylineOptions lines){
		this.lines = lines;
	}
//
//
	public PolylineOptions getLines() {
		return lines;
	}


	public void setLines(PolylineOptions lines) {
		this.lines = lines;
	}


//
//	public Marker getMarker() {
//		return marker;
//	}
//
//	public void setMarker(Marker marker) {
//		this.marker = marker;
//	}
//
//	public void addLine(Polyline line){
//		this.lines.add(line);
//	}
//
//	public void clear(){
//		for(Polyline line: this.lines){
//			line.remove();
//		}
//		this.lines.clear();
//	}
//
//    @Override
//    public int compareTo( Portion portion )
//    {
//        return order - portion.order;
//    }
//    private ArrayList<PathOverlay> lines;
//    private Marker marker;
//    private int order;
//
//    public Portion(int order) {
//        this.lines = new ArrayList<PathOverlay>();
//        this.order = order;
//    }
//
//    public int getOrder() {
//        return order;
//    }
//
//    public void setOrder(int order) {
//        this.order = order;
//    }
//
//    public Portion(ArrayList<PathOverlay> lines) {
//        this.lines = lines;
//    }
//
//
//    public ArrayList<PathOverlay> getLines() {
//        return lines;
//    }
//
//    public void setLines(ArrayList<PathOverlay> lines) {
//        this.lines = lines;
//    }
//
//    public Marker getMarker() {
//        return marker;
//    }
//
//    public void setMarker(Marker marker) {
//        this.marker = marker;
//    }
//
//    public void addLine(PathOverlay line) {
//        this.lines.add(line);
//    }
//
//    public void clear() {
//        for (PathOverlay line : this.lines) {
//            line.removeAllPoints();
//        }
//        this.lines.clear();
//    }

	public void deleteLinesFromMap(MapView mapView){
		if(lines != null) {
			mapView.removeAnnotation(lines.getPolyline());
		}
	}

    @Override
    public int compareTo(Portion portion) {
        return order - portion.order;
    }

}
