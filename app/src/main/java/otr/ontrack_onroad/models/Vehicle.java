package otr.ontrack_onroad.models;

import otr.ontrack_onroad.utils.WebRequestManager;

public class Vehicle
{
    public static final String VEHICLES_KEY = "VEHICLES";
    public static final String VEHICLE_KEY = "VEHICLE";
    public static final String ID_KEY = "ID";
    public static final String OLD_ID_KEY = "OLD_ID";
    public static final String YEAR_KEY = "YEAR";
    public static final String MANUFACTURER_KEY = "MANUFACTURER";
    public static final String MODEL_KEY = "MODEL";
    public static final String TYPE_KEY = "TYPE";
    public static final String CAPACITY_KEY = "CAPACITY";
    public static final String ENGINE_KEY = "ENGINE";
    public static final String IMAGE_KEY = "IMAGE";
    public static final String IMAGE_DEFAULT_KEY = "IMAGE_DEFAULT";

    private String id;
    private String year;
    private String manufacturer;
    private String model;
    private String type;
    private String capacity;
    private String engine;
    private String imageURL;

    public Vehicle( String id, String year, String manufacturer, String model, String type, String capacity, String engine, String image )
    {
        this.id = id;
        this.year = year;
        this.manufacturer = manufacturer;
        this.model = model;
        this.type = type;
        this.capacity = capacity;
        this.engine = engine;
        this.imageURL = WebRequestManager.URL_IMAGE_BASE + image;
    }

    public String getId( )
    {
        return id;
    }

    public void setId( String id )
    {
        this.id = id;
    }

    public String getYear( )
    {
        return year;
    }

    public void setYear( String year )
    {
        this.year = year;
    }

    public String getManufacturer( )
    {
        return manufacturer;
    }

    public void setManufacturer( String manufacturer )
    {
        this.manufacturer = manufacturer;
    }

    public String getModel( )
    {
        return model;
    }

    public void setModel( String model )
    {
        this.model = model;
    }

    public String getType( )
    {
        return type;
    }

    public void setType( String type )
    {
        this.type = type;
    }

    public String getCapacity( )
    {
        return capacity;
    }

    public void setCapacity( String capacity )
    {
        this.capacity = capacity;
    }

    public String getEngine( )
    {
        return engine;
    }

    public void setEngine( String engine )
    {
        this.engine = engine;
    }

    public String getImageURL( )
    {
        return imageURL;
    }

    public void setImageURL( String imageURL )
    {
        this.imageURL = imageURL;
    }
}
