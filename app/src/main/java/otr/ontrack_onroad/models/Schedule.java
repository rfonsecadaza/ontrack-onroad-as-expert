package otr.ontrack_onroad.models;

public class Schedule
{
    public static final String SCHEDULES_KEY = "SCHEDULES";
    public static final String SCHEDULE_KEY = "SCHEDULE";
    public static final String ID_KEY = "ID";
    public static final String NAME_KEY = "NAME";
    public static final String MONDAY_KEY = "MONDAY";
    public static final String TUESDAY_KEY = "TUESDAY";
    public static final String WEDNESDAY_KEY = "WEDNESDAY";
    public static final String THURSDAY_KEY = "THURSDAY";
    public static final String FRIDAY_KEY = "FRIDAY";
    public static final String SATURDAY_KEY = "SATURDAY";
    public static final String SUNDAY_KEY = "SUNDAY";
    public static final String START_TIME_KEY = "START_TIME";
    public static final String END_TIME_KEY = "END_TIME";
    public static final String FK_ROUTE_KEY = "FK_ROUTE";
    public static final String DAYS_KEY = "DAYS";

    private String id;
    private String name;
    private  boolean monday;
    private boolean tuesday;
    private boolean wednesday;
    private  boolean thursday;
    private  boolean friday;
    private  boolean saturday;
    private  boolean sunday;
    private   String startTime;
    private  String endTime;

    public Schedule(boolean monday, boolean tuesday,
                    boolean wednesday, boolean thursday, boolean friday,
                    boolean saturday, boolean sunday, String startTime, String endTime)
    {
        this.monday = monday;
        this.tuesday = tuesday;
        this.wednesday = wednesday;
        this.thursday = thursday;
        this.friday = friday;
        this.saturday = saturday;
        this.sunday = sunday;
        this.startTime = startTime;
        this.endTime = endTime;
    }
    
    public Schedule(String name, boolean monday, boolean tuesday,
            boolean wednesday, boolean thursday, boolean friday,
            boolean saturday, boolean sunday, String startTime, String endTime)
    {
        this.name = name;
        this.monday = monday;
        this.tuesday = tuesday;
        this.wednesday = wednesday;
        this.thursday = thursday;
        this.friday = friday;
        this.saturday = saturday;
        this.sunday = sunday;
        this.startTime = startTime;
        this.endTime = endTime;
    }
    
    public Schedule(String id, String name, boolean monday, boolean tuesday,
            boolean wednesday, boolean thursday, boolean friday,
            boolean saturday, boolean sunday, String startTime, String endTime)
    {
        this.id = id;
        this.name = name;
        this.monday = monday;
        this.tuesday = tuesday;
        this.wednesday = wednesday;
        this.thursday = thursday;
        this.friday = friday;
        this.saturday = saturday;
        this.sunday = sunday;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public boolean isMonday()
    {
        return monday;
    }

    public void setMonday(boolean monday)
    {
        this.monday = monday;
    }

    public boolean isTuesday()
    {
        return tuesday;
    }

    public void setTuesday(boolean tuesday)
    {
        this.tuesday = tuesday;
    }

    public boolean isWednesday()
    {
        return wednesday;
    }

    public void setWednesday(boolean wednesday)
    {
        this.wednesday = wednesday;
    }

    public boolean isThursday()
    {
        return thursday;
    }

    public void setThursday(boolean thursday)
    {
        this.thursday = thursday;
    }

    public boolean isFriday()
    {
        return friday;
    }

    public void setFriday(boolean friday)
    {
        this.friday = friday;
    }

    public boolean isSaturday()
    {
        return saturday;
    }

    public void setSaturday(boolean saturday)
    {
        this.saturday = saturday;
    }

    public boolean isSunday()
    {
        return sunday;
    }

    public void setSunday(boolean sunday)
    {
        this.sunday = sunday;
    }

    public String getStartTime()
    {
        return startTime;
    }

    public void setStartTime(String startTime)
    {
        this.startTime = startTime;
    }

    public String getEndTime()
    {
        return endTime;
    }

    public void setEndTime(String endTime)
    {
        this.endTime = endTime;
    }

    public String getScheduleString()
    {
        String scheduleString = "";
        scheduleString += monday ? "L " : "";
        scheduleString += tuesday ? "Ma " : "";
        scheduleString += wednesday ? "Mi " : "";
        scheduleString += thursday ? "J " : "";
        scheduleString += friday ? "V " : "";
        scheduleString += saturday ? "S " : "";
        scheduleString += sunday ? "D" : "";
        scheduleString += "\n" + startTime + "-" + endTime;

        return scheduleString;

    }
    
    public String getHoursString(){
    	String startTimeWithoutSeconds = startTime.substring(0, startTime.length() - 3);
    	String endTimeWithoutSeconds = endTime.substring(0, endTime.length() - 3);
    	String hoursString = startTimeWithoutSeconds + " - " + endTimeWithoutSeconds;
    	return hoursString;
    }
    
    public String getDaysString(){
    	 String daysString = "";
         daysString += monday ? "L " : "";
         daysString += tuesday ? "Ma " : "";
         daysString += wednesday ? "Mi " : "";
         daysString += thursday ? "J " : "";
         daysString += friday ? "V " : "";
         daysString += saturday ? "S " : "";
         daysString += sunday ? "D" : "";
         
         if(daysString.equals("L Ma Mi J V "))
        	 daysString = "ENTRE SEMANA";
         else if(daysString.equals("S D"))
        	 daysString = "FIN DE SEMANA";
         return daysString;
    }

    public String getId( )
    {
        return id;
    }

    public void setId( String id )
    {
        this.id = id;
    }

    public String getName( )
    {
        return name;
    }

    public void setName( String name )
    {
        this.name = name;
    }
}
