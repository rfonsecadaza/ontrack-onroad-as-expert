package otr.ontrack_onroad.models;

public class TrackedField
{
    public static final int DIRECTION_LABEL = 1;
    public static final int TIME_LABEL = 2;
    public static final int TRACKABLE_LABEL = 3;

    private int label;

    public int getLabel()
    {
        return label;
    }

    public void setLabel(int label)
    {
        this.label = label;
    }

    public Object getValue()
    {
        return value;
    }

    public void setValue(Object value)
    {
        this.value = value;
    }

    private Object value;

    public TrackedField(int label, Object value){
        this.label = label;
        this.value = value;
    }
}
