package otr.ontrack_onroad.utils;

import java.util.ArrayList;

public class MessageReceptionResult {
	private String resultText;
	private ArrayList<Long> idsArray;
	
	
	public String getResultText() {
		return resultText;
	}
	public void setResultText(String resultText) {
		this.resultText = resultText;
	}
	public ArrayList<Long> getIdsArray() {
		return idsArray;
	}
	public void setIdsArray(ArrayList<Long> idsArray) {
		this.idsArray = idsArray;
	}
}
