package otr.ontrack_onroad.utils;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class ImageAdjustment {
	public static int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			// Calculate the largest inSampleSize value that is a power of 2 and
			// keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) > reqHeight
					&& (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}

		return inSampleSize;
	}

	public static Bitmap getScaledBitmapFromUrl(String imageUrl,
			int requiredWidth, int requiredHeight) throws IOException {
		URL url;

		url = new URL(imageUrl);
		URLConnection conn = null;
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeStream(url.openConnection().getInputStream(), null,
				options);
		options.inSampleSize = calculateInSampleSize(options, requiredWidth,
				requiredHeight);
		options.inJustDecodeBounds = false;
		// don't use same inputstream object as in decodestream above. It
		// will
		// not work because
		// decode stream edit input stream. So if you create
		// InputStream is =url.openConnection().getInputStream(); and you
		// use
		// this in decodeStream
		// above and bellow it will not work!
		Bitmap bm;
		try {
			conn = url.openConnection();
			bm = BitmapFactory.decodeStream(conn.getInputStream(), null,
					options);
		} catch (IOException e) {
			return null;
		} finally {
			if (conn != null) {
				((HttpURLConnection) conn).disconnect();
			}
		}
		return bm;
	}

}
