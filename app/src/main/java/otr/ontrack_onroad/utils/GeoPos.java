package otr.ontrack_onroad.utils;

import java.util.ArrayList;

import otr.ontrack_onroad.models.Point;
import android.location.Location;

import com.mapbox.mapboxsdk.geometry.LatLng;

// Método para el cálculo del punto más cercano desde una ubicación al polyline

// Muchas gracias a quien sea que haya publicado este código abierto

// Fuente: https://github.com/msteiger/trackviewer/blob/master/trackviewer/src/main/GeoPos.java

public class GeoPos {
	public static final int MAXIMUM_TOLERANCE_FOR_ROUTE_DEVIATION = 20;
	public static final int MAXIMUM_TOLERANCE_FOR_EXTREME_ROUTE_DEVIATION = 40;
	public static final float MAXIMUM_TOLERANCE_FOR_BEARING_DEVIATION = 40;

	public static final double WGS84_EARTH_FLATTENING = 1.0 / 298.257223563;
	public static final double WGS84_EARTH_MAJOR = 6378137.0;
	public static final double WGS84_EARTH_MINOR = WGS84_EARTH_MAJOR
			* (1.0 - WGS84_EARTH_FLATTENING);
	public static final double WGS84_MEAN_RADIUS = (2 * WGS84_EARTH_MAJOR + WGS84_EARTH_MINOR) / 3.0;

	private double x, y, z;
	private double lat, lon;

	/**
	 * Construct a GeoPos from its latitude and longitude in degrees
	 * 
	 * @param lat
	 *            latitude in degrees
	 * @param lon
	 *            longitude in degrees
	 */
	public GeoPos(double lat, double lon) {
		double theta = (lon * Math.PI / 180.0);
		double rlat = geocentricLatitude(lat * Math.PI / 180.0);
		double c = Math.cos(rlat);

		this.lat = lat;
		this.lon = lon;

		this.x = c * Math.cos(theta);
		this.y = c * Math.sin(theta);
		this.z = Math.sin(rlat);
	}

	/**
	 * @return the latitude in degrees
	 */
	public double lat() {
		return lat;
	}

	/**
	 * @return the longitude in degrees
	 */
	public double lng() {
		return lon;
	}

	/**
	 * Convert from geographic to geocentric latitude (radians).
	 * 
	 * @param geographicLatitude
	 *            the geographic latitude
	 * @return the geocentric latitude in radians
	 */
	private double geocentricLatitude(double geographicLatitude) {
		double flattening = WGS84_EARTH_FLATTENING;
		double f = (1.0 - flattening) * (1.0 - flattening);
		return Math.atan((Math.tan(geographicLatitude) * f));
	}

	// /**
	// * Convert from geocentric to geographic latitude (radians)
	// * @param geocentricLatitude the geocentric Latitude
	// * @return the geographic latitude in radians
	// */
	// private static double geographicLatitude(double geocentricLatitude)
	// {
	// double flattening = 1.0 / earthFlattening;
	// double f = (1.0 - flattening) * (1.0 - flattening);
	// return Math.atan(Math.tan(geocentricLatitude) / f);
	// }

	/**
	 * Returns the two antipodal points of intersection of two great circles
	 * defined by the arcs geo1 to geo2 and geo3 to geo4. Returns a point as a
	 * {@link GeoPos}, use .antipode to get the other point
	 * 
	 * @param geo1
	 *            start of first arc
	 * @param geo2
	 *            end of first arc
	 * @param geo3
	 *            start of second arc
	 * @param geo4
	 *            end of second arc
	 * @return the intersection point (and its antipode)
	 */
	private static GeoPos getIntersection(GeoPos geo1, GeoPos geo2,
			GeoPos geo3, GeoPos geo4) {
		GeoPos geoCross1 = geo1.crossNormalize(geo2);
		GeoPos geoCross2 = geo3.crossNormalize(geo4);
		return geoCross1.crossNormalize(geoCross2);
	}

	private static double radiansToMeters(double rad) {
		return rad * WGS84_MEAN_RADIUS;
	}

	// private static double metersToRadians(double m)
	// {
	// return m / earthRadius;
	// }

	// private double getLatitudeRadians()
	// {
	// return (bdccGeoGeographicLatitude(Math.atan2(this.z, Math.sqrt((this.x *
	// this.x) + (this.y * this.y)))));
	// }
	//
	// private double getLongitudeRadians()
	// {
	// return (Math.atan2(this.y, this.x));
	// }

	private double dot(GeoPos b) {
		return ((this.x * b.x) + (this.y * b.y) + (this.z * b.z));
	}

	private double crossLength(GeoPos b) {
		double x = (this.y * b.z) - (this.z * b.y);
		double y = (this.z * b.x) - (this.x * b.z);
		double z = (this.x * b.y) - (this.y * b.x);
		return Math.sqrt((x * x) + (y * y) + (z * z));
	}

	private GeoPos scale(double s) {
		GeoPos r = new GeoPos(0, 0);
		r.x = this.x * s;
		r.y = this.y * s;
		r.z = this.z * s;

		r.lat = Math.atan2(r.z, Math.sqrt(r.x * r.x + r.y * r.y)) * 180
				/ Math.PI;
		r.lon = Math.atan2(r.y, r.x) * 180 / Math.PI;
		return r;
	}

	private GeoPos crossNormalize(GeoPos b) {
		double x = (this.y * b.z) - (this.z * b.y);
		double y = (this.z * b.x) - (this.x * b.z);
		double z = (this.x * b.y) - (this.y * b.x);
		double L = Math.sqrt((x * x) + (y * y) + (z * z));
		GeoPos r = new GeoPos(0, 0);
		r.x = x / L;
		r.y = y / L;
		r.z = z / L;

		r.lat = Math
				.toDegrees(Math.atan2(r.z, Math.sqrt(r.x * r.x + r.y * r.y)));
		r.lon = Math.toDegrees(Math.atan2(r.y, r.x));
		return r;
	}

	/**
	 * @return Point on opposite side of the world to this point
	 */
	private GeoPos antipode() {
		return this.scale(-1.0);
	}

	/**
	 * Distance in radians from this point to point v2
	 * 
	 * @param v2
	 *            the other point
	 * @return the distance in radians
	 */
	private double distance(GeoPos v2) {
		return Math.atan2(v2.crossLength(this), v2.dot(this));
	}

	/**
	 * Returns in meters the minimum of the perpendicular distance of this point
	 * from the line segment geo1-geo2 and the distance from this point to the
	 * line segment ends in geo1 and geo2
	 * 
	 * @param geo1
	 *            start of the line segment
	 * @param geo2
	 *            end of the line segment
	 * @return the distance in meters
	 */
	private double distanceToLineSegMtrs(GeoPos geo1, GeoPos geo2) {
		// point on unit sphere above origin and normal to plane of geo1,geo2
		// could be either side of the plane
		GeoPos p2 = geo1.crossNormalize(geo2);

		// intersection of GC normal to geo1/geo2 passing through p with GC
		// geo1/geo2
		GeoPos ip = getIntersection(geo1, geo2, this, p2);

		// need to check that ip or its antipode is between p1 and p2
		double d = geo1.distance(geo2);
		double d1p = geo1.distance(ip);
		double d2p = geo2.distance(ip);
		// window.status = d + ", " + d1p + ", " + d2p;
		if ((d >= d1p) && (d >= d2p))
			return radiansToMeters(this.distance(ip));
		else {
			ip = ip.antipode();

			d1p = geo1.distance(ip);
			d2p = geo2.distance(ip);
		}

		if ((d >= d1p) && (d >= d2p))
			return radiansToMeters(this.distance(ip));
		else
			return radiansToMeters(Math.min(geo1.distance(this),
					geo2.distance(this)));
	}

	/**
	 * Distance in meters from point to a polyline
	 * 
	 * @param poly
	 *            the polyline
	 * @param point
	 *            the point
	 * @return the distance in meters
	 */
	public static double distanceToPolyMtrs(ArrayList<GeoPos> poly, GeoPos point) {
		double d = Double.MAX_VALUE;
		int i;
		GeoPos p = new GeoPos(point.lat(), point.lng());
		for (i = 0; i < (poly.size() - 1); i++) {
			GeoPos p1 = poly.get(i);
			GeoPos l1 = new GeoPos(p1.lat(), p1.lng());
			GeoPos p2 = poly.get(i + 1);
			GeoPos l2 = new GeoPos(p2.lat(), p2.lng());
			double dp = p.distanceToLineSegMtrs(l1, l2);
			if (dp < d)
				d = dp;
		}
		return d;
	}

	/**
	 * Get a new {@link GeoPos} distanceMeters away on the compass bearing
	 * azimuthDegrees from the {@link GeoPos} point - accurate to better than
	 * 200m in 140km (20m in 14km) in the UK
	 * 
	 * @param point
	 *            the point
	 * @param distanceMeters
	 *            the distance in meters
	 * @param azimuthDegrees
	 *            the azimuth in degrees
	 * @return the new point
	 */
	// public static GeoPos pointAtRangeAndBearing(GeoPos point,
	// double distanceMeters, double azimuthDegrees) {
	// double latr = point.lat() * Math.PI / 180.0;
	// double lonr = point.lng() * Math.PI / 180.0;
	//
	// double coslat = Math.cos(latr);
	// double sinlat = Math.sin(latr);
	// double az = azimuthDegrees * Math.PI / 180.0;
	// double cosaz = Math.cos(az);
	// double sinaz = Math.sin(az);
	// double dr = distanceMeters / WGS84_MEAN_RADIUS; // distance in radians
	// double sind = Math.sin(dr);
	// double cosd = Math.cos(dr);
	//
	// double lat = Math.asin((sinlat * cosd) + (coslat * sind * cosaz))
	// * 180.0 / Math.PI;
	// double lon = Math.atan2((sind * sinaz), (coslat * cosd)
	// - (sinlat * sind * cosaz))
	// + lonr * 180.0 / Math.PI;
	//
	// return new GeoPos(lat, lon);
	// }

	public static GeoPos pointAtRangeAndBearing(GeoPos point,
			double distanceMeters, double azimuthDegrees) {
		double phi1 = point.lat * Math.PI / 180;
		// double lambda1 = point.lon * Math.PI / 180;
		double delta = distanceMeters / WGS84_MEAN_RADIUS;
		double theta = azimuthDegrees * Math.PI / 180;
		double phi2 = Math.asin(Math.sin(phi1) * Math.cos(delta)
				+ Math.cos(phi1) * Math.sin(delta) * Math.cos(theta));
		return new GeoPos((phi2 * 180 / Math.PI), point.lon
				+ Math.atan2(
						Math.sin(theta) * Math.sin(delta) * Math.cos(phi1),
						Math.cos(delta) - Math.sin(phi1) * Math.sin(phi2))
				* 180 / Math.PI);
	}

	// ROD'S STYLE

	// Polígono para Mapbox
	public static ArrayList<LatLng> polygonFromCenterDistanceAndSides(LatLng center, double distanceMeters, int sides){
		ArrayList<LatLng> polygon = new ArrayList<>();
		GeoPos centerGeoPos = convertMapboxLatLngToGeoPos(center);
		double degreesIncrement = 360/sides;
		for(int i=0; i< sides; i++){
			GeoPos vertex = pointAtRangeAndBearing(centerGeoPos,distanceMeters,degreesIncrement*i);
			polygon.add(new LatLng(vertex.lat(),vertex.lng()));
		}
		return polygon;
	}

	// Cálculo del ángulo de bearing
	// Fuente: http://www.movable-type.co.uk/scripts/latlong.html
	public static double bearing(GeoPos geo1, GeoPos geo2) {
		double deltaLambda = (geo2.lon - geo1.lon) * Math.PI / 180;
		double phi1 = geo1.lat * Math.PI / 180;
		double phi2 = geo2.lat * Math.PI / 180;
		double myBearing = modulus360((Math.atan2(
				Math.sin(deltaLambda) * Math.cos(phi2),
				Math.cos(phi1) * Math.sin(phi2) - Math.sin(phi1)
						* Math.cos(phi2) * Math.cos(deltaLambda)) * 180 / Math.PI));
		
		return myBearing;
	}

	// Convertir un objeto de tipo Location en GeoPos
	public static GeoPos convertLocationToGeoPos(Location location) {
		return new GeoPos(location.getLatitude(), location.getLongitude());
	}

	// Convertir un objeto de tipo LatLng en GeoPos
	public static GeoPos convertMapboxLatLngToGeoPos(LatLng latLng) {
		return new GeoPos(latLng.getLatitude(), latLng.getLongitude());
	}

	// Convertir una lista de Points en una lista de GeoPos
	public static ArrayList<GeoPos> convertPointsToGeoPosList(
			ArrayList<Point> points) {
		ArrayList<GeoPos> geoPosList = new ArrayList<GeoPos>();
		for (Point point : points) {
			GeoPos geoPos = new GeoPos(point.getLatitude(),
					point.getLongitude());

			geoPosList.add(geoPos);
		}
		return geoPosList;
	}

	private static class GeoIntersectionDistance {
		public GeoPos intersection;
		public GeoPos segmentStart;
		public GeoPos segmentEnd;
		public double distance;
		public double bearing;

		public GeoIntersectionDistance(double distance) {

			this.distance = distance;
		}

		public GeoIntersectionDistance(GeoPos intersection, double distance,
				double bearing) {
			this.intersection = intersection;
			this.distance = distance;
			this.bearing = bearing;
		}

		public GeoIntersectionDistance(GeoPos intersection, double distance,
				GeoPos segmentStart, GeoPos segmentEnd, double bearing) {
			this.intersection = intersection;
			this.distance = distance;
			this.segmentStart = segmentStart;
			this.segmentEnd = segmentEnd;
			this.bearing = bearing;
		}
	}

	private GeoIntersectionDistance geoIntersectionDistanceToLineSegMtrs(
			GeoPos geo1, GeoPos geo2) {

		// Azimuth test
		double azimuth = bearing(geo1, geo2);

		// point on unit sphere above origin and normal to plane of geo1,geo2
		// could be either side of the plane
		GeoPos p2 = geo1.crossNormalize(geo2);

		// intersection of GC normal to geo1/geo2 passing through p with GC
		// geo1/geo2
		GeoPos ip = getIntersection(geo1, geo2, this, p2);

		// need to check that ip or its antipode is between p1 and p2
		double d = geo1.distance(geo2);
		double d1p = geo1.distance(ip);
		double d2p = geo2.distance(ip);
		// window.status = d + ", " + d1p + ", " + d2p;
		if ((d >= d1p) && (d >= d2p))
			return new GeoIntersectionDistance(ip,
					radiansToMeters(this.distance(ip)), geo1, geo2, azimuth);
		else {
			ip = ip.antipode();
			d1p = geo1.distance(ip);
			d2p = geo2.distance(ip);
		}
		if ((d >= d1p) && (d >= d2p))
			return new GeoIntersectionDistance(ip,
					radiansToMeters(this.distance(ip)), geo1, geo2, azimuth);
		else if (geo1.distance(this) < geo2.distance(this))
			return new GeoIntersectionDistance(geo1,
					radiansToMeters(geo1.distance(this)), geo1, geo2, azimuth);
		else
			return new GeoIntersectionDistance(geo2,
					radiansToMeters(geo2.distance(this)), geo1, geo2, azimuth);
	}

	public static GeoIntersectionDistance geoIntersectionDistanceToPolyMtrs(
			ArrayList<GeoPos> poly, GeoPos point) {
		double d = Double.MAX_VALUE;
		GeoIntersectionDistance geoIntDis = new GeoIntersectionDistance(d);
		int i;
		GeoPos p = new GeoPos(point.lat(), point.lng());
		for (i = 0; i < (poly.size() - 1); i++) {
			GeoPos p1 = poly.get(i);
			GeoPos l1 = new GeoPos(p1.lat(), p1.lng());
			GeoPos p2 = poly.get(i + 1);
			GeoPos l2 = new GeoPos(p2.lat(), p2.lng());

			GeoIntersectionDistance geoIntDisP = p
					.geoIntersectionDistanceToLineSegMtrs(l1, l2);
			double dp = geoIntDisP.distance;
			if (dp < d) {
				d = dp;
				geoIntDis = geoIntDisP;
			}
		}
		return geoIntDis;
	}
	
	public static boolean isLocationExtremelyDeviatedFromRoute(Location location, ArrayList<Point> points){
		GeoPos geoPosLocation = convertLocationToGeoPos(location);
		ArrayList<GeoPos> geoPosPoints = convertPointsToGeoPosList(points);

		if (!points.isEmpty()) {
			GeoIntersectionDistance geoIntDis = geoIntersectionDistanceToPolyMtrs(
					geoPosPoints, geoPosLocation);
			double distance = geoIntDis.distance;
			return distance > location.getAccuracy()
					+ MAXIMUM_TOLERANCE_FOR_EXTREME_ROUTE_DEVIATION;
		}
		return false;
	}

	public static boolean convertLocationToPointOnRoute(Location location,
			ArrayList<Point> points, double offset) {
		GeoPos geoPosLocation = convertLocationToGeoPos(location);
		ArrayList<GeoPos> geoPosPoints = convertPointsToGeoPosList(points);

		if (!points.isEmpty()) {
			GeoIntersectionDistance geoIntDis = geoIntersectionDistanceToPolyMtrs(
					geoPosPoints, geoPosLocation);
			double distance = geoIntDis.distance;

			if (distance <= location.getAccuracy()
					+ MAXIMUM_TOLERANCE_FOR_ROUTE_DEVIATION + offset) {
				// Calcular el azimuth del segmento de aproximación
				float azimuth = (float) bearing(geoIntDis.segmentStart,
						geoIntDis.segmentEnd);
				

				// Hay dos puntos que podrían dar una buena aproximación, ambos
				// "perpendiculares" al segmento seleccionado

				double azimuthOrthogonal1 = azimuth + 90;
				double azimuthOrthogonal2 = azimuth - 90;

				GeoPos candidate1 = pointAtRangeAndBearing(geoPosLocation,
						distance, azimuthOrthogonal1);
				GeoPos candidate2 = pointAtRangeAndBearing(geoPosLocation,
						distance, azimuthOrthogonal2);

				GeoPos geoPos;

				if (candidate1.distanceToLineSegMtrs(geoIntDis.segmentStart,
						geoIntDis.segmentEnd) <= (candidate2
						.distanceToLineSegMtrs(geoIntDis.segmentStart,
								geoIntDis.segmentEnd)))
					geoPos = candidate1;
				else
					geoPos = candidate2;

				location.setLatitude(geoPos.lat());
				location.setLongitude(geoPos.lng());

				// Aproximación del bearing

				if (azimuth >= MAXIMUM_TOLERANCE_FOR_BEARING_DEVIATION
						&& azimuth <= 360f - MAXIMUM_TOLERANCE_FOR_BEARING_DEVIATION) {
					// Caso 1: el azimuth está dentro de los límites "normales"
					if (location.getBearing() >= azimuth
							- MAXIMUM_TOLERANCE_FOR_BEARING_DEVIATION
							&& location.getBearing() <= azimuth
									+ MAXIMUM_TOLERANCE_FOR_BEARING_DEVIATION) {
						location.setBearing(azimuth);
					}
				} else if (azimuth > 360f - MAXIMUM_TOLERANCE_FOR_BEARING_DEVIATION && azimuth <= 360) {
					// Caso 2: el azimuth está en la zona cercana a 0º, por la
					// izquierda
					if ((location.getBearing() >= azimuth
							- MAXIMUM_TOLERANCE_FOR_BEARING_DEVIATION && location.getBearing() <= 360f)
							|| (0.0f < location.getBearing() && location
									.getBearing() <= modulus360(azimuth + MAXIMUM_TOLERANCE_FOR_BEARING_DEVIATION))) {
						location.setBearing(azimuth);
					}
				} else if (azimuth <= MAXIMUM_TOLERANCE_FOR_BEARING_DEVIATION && azimuth > 0) {
					if ((location.getBearing() >= modulus360(azimuth - MAXIMUM_TOLERANCE_FOR_BEARING_DEVIATION) && location.getBearing()<=360f)
							
							||(location.getBearing()>0 && location.getBearing() <= azimuth
									+ MAXIMUM_TOLERANCE_FOR_BEARING_DEVIATION)) {
						location.setBearing(azimuth);
					}
				}

				// Retorna false si se hace la aproximación a la ruta
				return false;
			}
			return true;

		}
		return false;
	}
	
	public static float modulus360(float operand){
		float modulus = operand%360f;
		if(modulus <= 0.0f){
			modulus = modulus + 360f; 
		}
		return modulus;
	}
	
	public static double modulus360(double operand){
		double modulus = operand%360.0;
		if(modulus <= 0.0f){
			modulus = modulus + 360.0; 
		}
		return modulus;
	}
}
