package otr.ontrack_onroad.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

//import com.google.android.gms.maps.model.LatLng;

import otr.ontrack_onroad.dao.ChatCard;
import otr.ontrack_onroad.dao.ChatUser;
import otr.ontrack_onroad.dao.Event;
import otr.ontrack_onroad.dao.Message;
import otr.ontrack_onroad.dao.Notification;
import otr.ontrack_onroad.managers.FreeModeLocationManager;
import otr.ontrack_onroad.managers.FreeModeManager;
import otr.ontrack_onroad.managers.MessageManager;
import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.models.Novelty;
import otr.ontrack_onroad.models.Organization;
import otr.ontrack_onroad.models.Point;
import otr.ontrack_onroad.models.Report;
import otr.ontrack_onroad.models.Route;
import otr.ontrack_onroad.models.Schedule;
import otr.ontrack_onroad.models.Stop;
import otr.ontrack_onroad.models.Trackable;
import otr.ontrack_onroad.models.TrackedPoint;
import otr.ontrack_onroad.models.User;
import otr.ontrack_onroad.models.Vehicle;
import otr.ontrack_onroad.parameters.Parameters;
import otr.ontrack_onroad.parameters.PreferenceKeys;
import otr.ontrack_onroad.parameters.ServiceKeys;
import otr.ontrack_onroad_debug.activities.MainActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;

import com.mapbox.mapboxsdk.geometry.LatLng;

public class JSONManager {

	public static int login(String jsonString) throws JSONException {
		JSONObject response = new JSONObject(jsonString);
		int status = response.getInt(ServiceKeys.STATUS_KEY);

		if (status == 1) {

			SharedPreferences prefs = OnTrackManager
					.getInstance()
					.getContext()
					.getSharedPreferences(PreferenceKeys.PREFERENCES_KEY,
							Context.MODE_PRIVATE);

			SharedPreferences.Editor editor = prefs.edit();

			editor.putString(PreferenceKeys.TOKEN_KEY,
					response.getString(ServiceKeys.TOKEN_KEY).toString());
			editor.putInt(PreferenceKeys.USER_DEVICE_ID_KEY,
					response.getInt(ServiceKeys.USER_DEVICE_ID_KEY));
			editor.commit();

		}

		return status;
	}

	public static int loadUser(String jsonString, boolean asyncImageLoad)
			throws JSONException {
		JSONObject jsonObject;

		jsonObject = new JSONObject(jsonString);
		int status = jsonObject.getInt(User.STATUS_KEY);
		if (status == 1) {

			JSONObject jsonUser = jsonObject.getJSONObject(User.DRIVER_KEY);
			// Retrieve user info
			String userId = jsonUser.getString(User.ID_KEY);
			String firstName = jsonUser.getString(User.FIRST_NAME_KEY);
			String lastName = jsonUser.getString(User.LAST_NAME_KEY);
			String contactEMail = jsonUser.getString(User.CONTACT_EMAIL_KEY);
			String contactAddress = jsonUser
					.getString(User.CONTACT_ADDRESS_KEY);
			String mobilePhone = jsonUser.getString(User.MOBILE_PHONE_KEY);
			String lanPhone = jsonUser.getString(User.LAN_PHONE_KEY);
			String role = jsonUser.getString(User.ROLE_KEY);
			String imageURL = jsonUser.getString(User.IMAGE_KEY);
			//
			if (jsonUser.getString(User.IMAGE_KEY).equals(null)) {
				imageURL = jsonUser.getString(User.IMAGE_DEFAULT_KEY);
			} else {
				imageURL = jsonUser.getString(User.IMAGE_KEY);
			}

			User user = new User(userId, firstName, lastName, contactAddress,
					mobilePhone, lanPhone, role, null, imageURL, true);

			OnTrackManager.getInstance().setUser(user);

			JSONArray routesArray = new JSONArray(
					jsonUser.getString(User.ROUTE_SCHEDULES_KEY));
			ArrayList<Route> routes = loadRoutesArray(routesArray);
			OnTrackManager.getInstance().setRoutes(routes);

			if (jsonUser.has(User.ROUTE_ORGANIZATIONS_KEY)) {
				JSONArray organizationsArray = new JSONArray(
						jsonUser.getString(User.ROUTE_ORGANIZATIONS_KEY));
				ArrayList<Organization> organizations = loadOrganizationsArray(organizationsArray);

				user.setOrganizations(organizations);
			}
		}
		return status;
	}

	public static void loadRoute(String jsonString, int routePos)
			throws JSONException {
		JSONObject jsonObject;

		jsonObject = new JSONObject(jsonString);
		JSONObject jsonRoute = jsonObject.getJSONObject(ServiceKeys.ROUTE_KEY);

		Route route = OnTrackManager.getInstance().getRoutes().get(routePos);

		JSONArray stopsArray = new JSONArray(
				jsonRoute.getString(Route.STOPS_KEY));
		ArrayList<Stop> stops = loadStopsArray(stopsArray);

		if (stops.size() > 0) {
			route.setStops(stops);
			if (route.getType() == Route.DELIVERY_CODE) {
				route.getStops().get(0)
						.setDirection(Parameters.ROUTE_DELIVERY_ORIGIN);
			} else {
				route.getStops().get(0)
						.setDirection(Parameters.ROUTE_PICK_UP_ORIGIN);
			}
		}
		OnTrackManager.getInstance().getUser().setRoute(route);

		JSONArray routeTrackablesArray = new JSONArray(
				jsonRoute.getString(Route.ROUTETRACKABLES_KEY));
		ArrayList<Trackable> trackables = loadRouteTrackablesArray(routeTrackablesArray);
		route.setTrackables(trackables);
	}

	public static void loadRouteWithNovelties(String jsonString, int routePos)
			throws JSONException {
		JSONObject jsonObject;

		jsonObject = new JSONObject(jsonString);
		JSONObject jsonRoute = jsonObject.getJSONObject(ServiceKeys.ROUTE_KEY);

		Route route = OnTrackManager.getInstance().getRoutes().get(routePos);
		OnTrackManager.getInstance().getUser().setRoute(route);

		JSONArray stopsArray = new JSONArray(
				jsonRoute.getString(Route.STOPS_WITH_NOVELTIES_KEY));
		ArrayList<Stop> stops = loadStopsArrayWithNovelties(stopsArray);

		if (stops.size() > 0) {
			route.setStops(stops);
			if (route.getType() == Route.DELIVERY_CODE) {
				route.getStops().get(0)
						.setDirection(Parameters.ROUTE_DELIVERY_ORIGIN);
			} else {
				route.getStops().get(0)
						.setDirection(Parameters.ROUTE_PICK_UP_ORIGIN);
			}
		}


		JSONArray routeTrackablesArray = new JSONArray(
				jsonRoute.getString(Route.ROUTETRACKABLES_KEY));
		ArrayList<Trackable> trackables = loadRouteTrackablesArray(routeTrackablesArray);
		route.setTrackables(trackables);
	}

	public static void loadRouteEdition(String jsonString, int routePos)
			throws JSONException {
		JSONObject jsonObject;

		jsonObject = new JSONObject(jsonString);
		JSONObject jsonRoute = jsonObject.getJSONObject(ServiceKeys.ROUTE_KEY);

		// Find the light route in System with the specified
		// position
		Route route = OnTrackManager.getInstance().getRoutes().get(routePos);

		JSONArray stopsArray = new JSONArray(
				jsonRoute.getString(Route.STOPS_KEY));
		ArrayList<Stop> stops = loadStopsArray(stopsArray);

		route.setStops(stops);

		if (!route.getStops().isEmpty()) {
			if (route.getType() == Route.DELIVERY_CODE) {
				route.getStops().get(0)
						.setDirection(Parameters.ROUTE_DELIVERY_ORIGIN);
			} else {
				route.getStops().get(0)
						.setDirection(Parameters.ROUTE_PICK_UP_ORIGIN);
			}
		}

		JSONArray routeSchedulesArray = new JSONArray(
				jsonRoute.getString(Route.ROUTESCHEDULES_KEY));

		JSONObject jsonRouteSchedule = routeSchedulesArray.getJSONObject(0);

		route.setSchedule(loadSchedule(jsonRouteSchedule));

		route.setVehicle(loadVehicle(jsonRouteSchedule));

		route.setMonitor(loadMonitor(jsonRouteSchedule));

		OnTrackManager.getInstance().getUser().setRoute(route);

	}

	// Retrieve light routes
	public static ArrayList<Route> loadRoutesArray(JSONArray routesArray)
			throws JSONException {
		ArrayList<Route> routes = new ArrayList<Route>();
		for (int i = 0; i < routesArray.length(); i++) {
			JSONObject currentObject = routesArray.getJSONObject(i);

			// Revisa la informaciï¿½n de horario y la guarda en un objeto de
			// tipo
			// schedule
			String scheduleString = currentObject.getString(Route.SCHEDULE_KEY);

			Schedule schedule;
			if (!scheduleString.equals("[]")) {
				JSONObject scheduleObject = currentObject
						.getJSONObject(Route.SCHEDULE_KEY);
				boolean monday = scheduleObject.getInt(Schedule.MONDAY_KEY) == 1;
				boolean tuesday = scheduleObject.getInt(Schedule.TUESDAY_KEY) == 1;
				boolean wednesday = scheduleObject
						.getInt(Schedule.WEDNESDAY_KEY) == 1;
				boolean thursday = scheduleObject.getInt(Schedule.THURSDAY_KEY) == 1;
				boolean friday = scheduleObject.getInt(Schedule.FRIDAY_KEY) == 1;
				boolean saturday = scheduleObject.getInt(Schedule.SATURDAY_KEY) == 1;
				boolean sunday = scheduleObject.getInt(Schedule.SUNDAY_KEY) == 1;
				String startTime = scheduleObject
						.getString(Schedule.START_TIME_KEY);
				String endTime = scheduleObject
						.getString(Schedule.END_TIME_KEY);

				schedule = new Schedule(monday, tuesday, wednesday, thursday,
						friday, saturday, sunday, startTime, endTime);
			} else {
				schedule = null;
			}

			JSONObject routeObject = currentObject
					.getJSONObject(Route.ROUTE_KEY);
			int type = routeObject.getInt(Route.TYPE_KEY);
			String modificationDateStr = routeObject
					.getString(Route.MODIFICATION_DATE_KEY);
			String modifiedBy = routeObject.getString(Route.MODIFIED_BY_KEY);

			int numberOfStops = 0;

			if (routeObject.has(Route.NUMBER_STOPS_IN_ROUTE_KEY)) {
				numberOfStops = routeObject.getInt(Route.NUMBER_STOPS_IN_ROUTE_KEY);
			}

			boolean hasFinished = false;

			if (routeObject.has(Route.HAS_FINISHED_TODAY_KEY)) {
				hasFinished = routeObject.getInt(Route.HAS_FINISHED_TODAY_KEY) == 1;
			}

			String organizationId = routeObject.getString(Route.FK_ORGANIZATION_KEY);

			// Retrieve route id
			String routeId = currentObject.getString(Schedule.FK_ROUTE_KEY);

			Date modificationDate = null;

			if (!modificationDateStr.equals("null")) {
				try {
					modificationDate = new SimpleDateFormat(
							"yyyy-MM-dd hh:mm:ss").parse(modificationDateStr);
				} catch (ParseException e) {

				}
			}

			// Datos para posibles relevos
			// Últimas líneas de OnTrack School

			if(organizationId.equals("16")) {
				OnTrackManager.getInstance().setEnabledReplacement(true);
				JSONArray usersArray = currentObject.getJSONArray("USERS");
				for (int j = 0; j < usersArray.length(); j++) {
					JSONObject userObject = usersArray.getJSONObject(j);
					int role = userObject.getInt("ROLE");
					if (role == 0) {
						if (OnTrackManager.getInstance().getReplacementDriver() == null) {
							String firstName = userObject.getString("FIRST_NAME");
							String lastName = userObject.getString("LAST_NAME");
							OnTrackManager.getInstance().setReplacementDriver(firstName + " " + lastName);
						}
					} else if (role == 3) {
						if (OnTrackManager.getInstance().getReplacementMonitor() == null) {
							String firstName = userObject.getString("FIRST_NAME");
							String lastName = userObject.getString("LAST_NAME");
							OnTrackManager.getInstance().setReplacementMonitor(firstName + " " + lastName);
						}
					}
				}

				JSONArray vehiclesArray = currentObject.getJSONArray("VEHICLES");
				for (int j = 0; j < vehiclesArray.length(); j++) {
					JSONObject vehicleObject = vehiclesArray.getJSONObject(j);
					String plate = vehicleObject.getString("ID");
					if (OnTrackManager.getInstance().getReplacementVehiclePlate() == null) {
						OnTrackManager.getInstance().setReplacementVehiclePlate(plate);
					}
				}
			}


			// Create light route
			Route route = new Route(routeId, schedule, type, modificationDate,
					modifiedBy, numberOfStops, organizationId, hasFinished);

			// Add to light routes array
			routes.add(route);

		}
		return routes;
	}

	// Retrieve users array
	static ArrayList<User> loadUsersArray(JSONArray usersArray)
			throws JSONException {
		ArrayList<User> users = new ArrayList<User>();
		for (int k = 0; k < usersArray.length(); k++) {
			JSONObject currentUser = usersArray.getJSONObject(k);
			String userId = currentUser.getString(User.ID_KEY);
			String userFirstName = currentUser.getString(User.FIRST_NAME_KEY);
			String userLastName = currentUser.getString(User.LAST_NAME_KEY);
			String userContactAddress = currentUser
					.getString(User.CONTACT_ADDRESS_KEY);
			String userMobilePhone = currentUser
					.getString(User.MOBILE_PHONE_KEY);
			String userLanPhone = currentUser.getString(User.LAN_PHONE_KEY);
			String userRole = currentUser.getString(User.ROLE_KEY);
			String imageURL = WebRequestManager.URL_IMAGE_BASE
					+ currentUser.getString(User.IMAGE_KEY);
			if (currentUser.getString(User.IMAGE_KEY).equals("null")) {
				imageURL = WebRequestManager.URL_IMAGE_BASE
						+ currentUser.getString(User.IMAGE_DEFAULT_KEY);
			}
			User user = new User(userId, userFirstName, userLastName,
					userContactAddress, userMobilePhone, userLanPhone,
					userRole, null, imageURL, true);
			users.add(user);
		}
		return users;
	}

	// Retrieve trackables array
	static ArrayList<Trackable> loadTrackablesArray(JSONArray trackablesArray)
			throws JSONException {
		ArrayList<Trackable> trackables = new ArrayList<Trackable>();
		for (int j = 0; j < trackablesArray.length(); j++) {
			JSONObject currentTrackable = trackablesArray.getJSONObject(j);
			String trackableId = currentTrackable.getString(Trackable.ID_KEY);
			String trackableName = currentTrackable
					.getString(Trackable.NAME_KEY);
			String trackableSubName = currentTrackable
					.getString(Trackable.SUBNAME_KEY);
			String trackableDescription = currentTrackable
					.getString(Trackable.DESCRIPTION_KEY);
			String trackableBirthday = currentTrackable
					.getString(Trackable.BIRTHDAY_KEY);
			String trackableCourse = currentTrackable
					.getString(Trackable.COURSE_KEY);
			String trackableContactAddress = currentTrackable
					.getString(Trackable.CONTACTADDRESS_KEY);
			String trackableContactPhone = currentTrackable
					.getString(Trackable.CONTACTPHONE_KEY);
			String trackableImageURL = WebRequestManager.URL_IMAGE_BASE
					+ currentTrackable.getString(Trackable.IMAGE_KEY);
			int trackableState = currentTrackable.getInt(Trackable.STATE_KEY);
			if (currentTrackable.getString(Trackable.IMAGE_KEY).equals("null")) {
				trackableImageURL = currentTrackable
						.getString(Trackable.IMAGE_DEFAULT_KEY);
			} else {
				trackableImageURL = currentTrackable
						.getString(Trackable.IMAGE_KEY);
			}

			JSONArray usersArray = new JSONArray(
					currentTrackable.getString(Trackable.USERS_KEY));
			// Retrieve users from jsonObject
			ArrayList<User> users = loadUsersArray(usersArray);
			Trackable trackable = new Trackable(trackableId, trackableName,
					trackableSubName, trackableDescription, trackableBirthday,
					trackableCourse, trackableContactPhone,
					trackableContactAddress, trackableImageURL, users,
					trackableState, true);
			trackables.add(trackable);

		}

		return trackables;
	}

	// Retrieve trackables array
	static ArrayList<Trackable> loadTrackablesArrayWithNovelties(
			JSONArray trackablesArray) throws JSONException {
		ArrayList<Trackable> trackables = new ArrayList<Trackable>();
		for (int j = 0; j < trackablesArray.length(); j++) {
			JSONObject currentTrackable = trackablesArray.getJSONObject(j);
			String trackableId = currentTrackable.getString(Trackable.ID_KEY);
			String trackableName = currentTrackable
					.getString(Trackable.NAME_KEY);
			String trackableSubName = currentTrackable
					.getString(Trackable.SUBNAME_KEY);
			String trackableDescription = currentTrackable
					.getString(Trackable.DESCRIPTION_KEY);
			String trackableBirthday = currentTrackable
					.getString(Trackable.BIRTHDAY_KEY);
			String trackableCourse = currentTrackable
					.getString(Trackable.COURSE_KEY);
			String trackableContactAddress = currentTrackable
					.getString(Trackable.CONTACTADDRESS_KEY);
			String trackableContactPhone = currentTrackable
					.getString(Trackable.CONTACTPHONE_KEY);
			String trackableImageURL = WebRequestManager.URL_IMAGE_BASE
					+ currentTrackable.getString(Trackable.IMAGE_KEY);
			int trackableState = currentTrackable.getInt(Trackable.STATE_KEY);
			if (currentTrackable.getString(Trackable.IMAGE_KEY).equals("null")) {
				trackableImageURL = currentTrackable
						.getString(Trackable.IMAGE_DEFAULT_KEY);
			} else {
				trackableImageURL = currentTrackable
						.getString(Trackable.IMAGE_KEY);
			}

			JSONArray usersArray = new JSONArray(
					currentTrackable.getString(Trackable.USERS_KEY));
			// Retrieve users from jsonObject
			ArrayList<User> users = loadUsersArray(usersArray);
			Trackable trackable = new Trackable(trackableId, trackableName,
					trackableSubName, trackableDescription, trackableBirthday,
					trackableCourse, trackableContactPhone,
					trackableContactAddress, trackableImageURL, users,
					trackableState, true);

			// Procesar novedad
			if (!currentTrackable.getString(ServiceKeys.NOVELTY_KEY).equals(
					"null")) {
				JSONObject noveltyObject = currentTrackable
						.getJSONObject(ServiceKeys.NOVELTY_KEY);

				int serverId = noveltyObject.getInt(ServiceKeys.ID_KEY);
				String fkTrackable = noveltyObject
						.getString(ServiceKeys.FK_TRACKABLE_KEY);
				String fromDate = noveltyObject
						.getString(ServiceKeys.FROM_DATE_KEY);
				String toDate = noveltyObject
						.getString(ServiceKeys.TO_DATE_KEY);
				boolean monday = noveltyObject.getInt(ServiceKeys.MONDAY_KEY) == 1;
				boolean tuesday = noveltyObject.getInt(ServiceKeys.TUESDAY_KEY) == 1;
				boolean wednesday = noveltyObject
						.getInt(ServiceKeys.WEDNESDAY_KEY) == 1;
				boolean thursday = noveltyObject
						.getInt(ServiceKeys.THURSDAY_KEY) == 1;
				boolean friday = noveltyObject.getInt(ServiceKeys.FRIDAY_KEY) == 1;
				boolean saturday = noveltyObject
						.getInt(ServiceKeys.SATURDAY_KEY) == 1;
				boolean sunday = noveltyObject.getInt(ServiceKeys.SUNDAY_KEY) == 1;
				int day = noveltyObject.getInt(ServiceKeys.DAY_KEY);
				String periodicity = noveltyObject
						.getString(ServiceKeys.PERIODICITY_KEY);
				String fkRouteOrigin = noveltyObject
						.getString(ServiceKeys.FK_ROUTE_ORIGIN_KEY);
				String fkRouteDestination = noveltyObject
						.getString(ServiceKeys.FK_ROUTE_DESTINATION_KEY);
				String stopOrigin = noveltyObject
						.getString(ServiceKeys.STOP_ORIGIN_KEY);
				String stopDestination = noveltyObject
						.getString(ServiceKeys.STOP_DESTINATION_KEY);
				String stopAddress = noveltyObject
						.getString(ServiceKeys.STOP_ADDRESS_KEY);
				String description = noveltyObject
						.getString(ServiceKeys.DESCRIPTION_KEY);
				int loopType = noveltyObject.getInt(ServiceKeys.LOOP_TYPE_KEY);
				String createdBy = noveltyObject
						.getString(ServiceKeys.CREATED_BY_KEY);
				int state = noveltyObject.getInt(ServiceKeys.STATE_KEY);
				String creationDate = noveltyObject
						.getString(ServiceKeys.CREATION_DATE_KEY);
				int type = noveltyObject.getInt(ServiceKeys.TYPE_KEY);
				int intermittence = noveltyObject
						.getInt(ServiceKeys.INTERMITTENCE_KEY);

				String hour = noveltyObject
						.getString(ServiceKeys.HOUR_KEY);

				String parentName = noveltyObject
						.getString(ServiceKeys.PARENT_NAME_KEY);

				String identificationParent = noveltyObject
						.getString(ServiceKeys.IDENTIFICATION_PARENT_KEY);

				Novelty novelty = new Novelty(serverId, fkTrackable, fromDate,
						toDate, monday, tuesday, wednesday, thursday, friday,
						saturday, sunday, day, periodicity, fkRouteOrigin,
						fkRouteDestination, stopOrigin, stopDestination,
						stopAddress, description, loopType, createdBy, state,
						creationDate, type, intermittence, hour, parentName, identificationParent);
				trackable.setNovelty(novelty);
			}

			trackables.add(trackable);

		}

		return trackables;
	}

	// Retrieve trackables array
	static void loadTrackablesArrayFromExtraStop(
			JSONArray trackablesArray) throws JSONException {

		for (int j = 0; j < trackablesArray.length(); j++) {
			JSONObject currentTrackable = trackablesArray.getJSONObject(j);
			String trackableId = currentTrackable.getString(Trackable.ID_KEY);
			String trackableName = currentTrackable
					.getString(Trackable.NAME_KEY);
			String trackableSubName = currentTrackable
					.getString(Trackable.SUBNAME_KEY);
			String trackableDescription = currentTrackable
					.getString(Trackable.DESCRIPTION_KEY);
			String trackableBirthday = currentTrackable
					.getString(Trackable.BIRTHDAY_KEY);
			String trackableCourse = currentTrackable
					.getString(Trackable.COURSE_KEY);
			String trackableContactAddress = currentTrackable
					.getString(Trackable.CONTACTADDRESS_KEY);
			String trackableContactPhone = currentTrackable
					.getString(Trackable.CONTACTPHONE_KEY);
			String trackableImageURL = WebRequestManager.URL_IMAGE_BASE
					+ currentTrackable.getString(Trackable.IMAGE_KEY);
			int trackableState = currentTrackable.getInt(Trackable.STATE_KEY);
			if (currentTrackable.getString(Trackable.IMAGE_KEY).equals("null")) {
				trackableImageURL = currentTrackable
						.getString(Trackable.IMAGE_DEFAULT_KEY);
			} else {
				trackableImageURL = currentTrackable
						.getString(Trackable.IMAGE_KEY);
			}

			JSONArray usersArray = new JSONArray(
					currentTrackable.getString(Trackable.USERS_KEY));
			// Retrieve users from jsonObject
			ArrayList<User> users = loadUsersArray(usersArray);
			Trackable trackable = new Trackable(trackableId, trackableName,
					trackableSubName, trackableDescription, trackableBirthday,
					trackableCourse, trackableContactPhone,
					trackableContactAddress, trackableImageURL, users,
					trackableState, true);

			// Procesar novedad
			if (!currentTrackable.getString(ServiceKeys.NOVELTY_KEY).equals(
					"null")) {
				JSONObject noveltyObject = currentTrackable
						.getJSONObject(ServiceKeys.NOVELTY_KEY);

				int serverId = noveltyObject.getInt(ServiceKeys.ID_KEY);
				String fkTrackable = noveltyObject
						.getString(ServiceKeys.FK_TRACKABLE_KEY);
				String fromDate = noveltyObject
						.getString(ServiceKeys.FROM_DATE_KEY);
				String toDate = noveltyObject
						.getString(ServiceKeys.TO_DATE_KEY);
				boolean monday = noveltyObject.getInt(ServiceKeys.MONDAY_KEY) == 1;
				boolean tuesday = noveltyObject.getInt(ServiceKeys.TUESDAY_KEY) == 1;
				boolean wednesday = noveltyObject
						.getInt(ServiceKeys.WEDNESDAY_KEY) == 1;
				boolean thursday = noveltyObject
						.getInt(ServiceKeys.THURSDAY_KEY) == 1;
				boolean friday = noveltyObject.getInt(ServiceKeys.FRIDAY_KEY) == 1;
				boolean saturday = noveltyObject
						.getInt(ServiceKeys.SATURDAY_KEY) == 1;
				boolean sunday = noveltyObject.getInt(ServiceKeys.SUNDAY_KEY) == 1;
				int day = noveltyObject.getInt(ServiceKeys.DAY_KEY);
				String periodicity = noveltyObject
						.getString(ServiceKeys.PERIODICITY_KEY);
				String fkRouteOrigin = noveltyObject
						.getString(ServiceKeys.FK_ROUTE_ORIGIN_KEY);
				String fkRouteDestination = noveltyObject
						.getString(ServiceKeys.FK_ROUTE_DESTINATION_KEY);
				String stopOrigin = noveltyObject
						.getString(ServiceKeys.STOP_ORIGIN_KEY);
				String stopDestination = noveltyObject
						.getString(ServiceKeys.STOP_DESTINATION_KEY);
				String stopAddress = noveltyObject
						.getString(ServiceKeys.STOP_ADDRESS_KEY);
				String description = noveltyObject
						.getString(ServiceKeys.DESCRIPTION_KEY);
				int loopType = noveltyObject.getInt(ServiceKeys.LOOP_TYPE_KEY);
				String createdBy = noveltyObject
						.getString(ServiceKeys.CREATED_BY_KEY);
				int state = noveltyObject.getInt(ServiceKeys.STATE_KEY);
				String creationDate = noveltyObject
						.getString(ServiceKeys.CREATION_DATE_KEY);
				int type = noveltyObject.getInt(ServiceKeys.TYPE_KEY);
				int intermittence = noveltyObject
						.getInt(ServiceKeys.INTERMITTENCE_KEY);

				String hour = noveltyObject
						.getString(ServiceKeys.HOUR_KEY);

				String parentName = noveltyObject
						.getString(ServiceKeys.PARENT_NAME_KEY);

				String identificationParent = noveltyObject
						.getString(ServiceKeys.IDENTIFICATION_PARENT_KEY);

				Novelty novelty = new Novelty(serverId, fkTrackable, fromDate,
						toDate, monday, tuesday, wednesday, thursday, friday,
						saturday, sunday, day, periodicity, fkRouteOrigin,
						fkRouteDestination, stopOrigin, stopDestination,
						stopAddress, description, loopType, createdBy, state,
						creationDate, type, intermittence, hour, parentName, identificationParent);
				trackable.setNovelty(novelty);


			}


			OnTrackManager.getInstance().getUser().getRoute()
					.addTrackableWithoutStop(trackable);


		}


	}

	static ArrayList<Trackable> loadRouteTrackablesArray(
			JSONArray trackablesArray) throws JSONException {
		ArrayList<Trackable> trackables = new ArrayList<Trackable>();
		for (int j = 0; j < trackablesArray.length(); j++) {
			JSONObject currentTrackable = trackablesArray.getJSONObject(j);
			JSONObject routeTrackable = currentTrackable
					.getJSONObject(Trackable.TRACKABLE_KEY);
			String trackableId = routeTrackable.getString(Trackable.ID_KEY);
			String trackableName = routeTrackable.getString(Trackable.NAME_KEY);
			String trackableSubName = routeTrackable
					.getString(Trackable.SUBNAME_KEY);
			String trackableDescription = routeTrackable
					.getString(Trackable.DESCRIPTION_KEY);
			String trackableBirthday = routeTrackable
					.getString(Trackable.BIRTHDAY_KEY);
			String trackableCourse = routeTrackable
					.getString(Trackable.COURSE_KEY);
			String trackableContactAddress = routeTrackable
					.getString(Trackable.CONTACTADDRESS_KEY);
			String trackableContactPhone = routeTrackable
					.getString(Trackable.CONTACTPHONE_KEY);
			String trackableImageURL = WebRequestManager.URL_IMAGE_BASE
					+ routeTrackable.getString(Trackable.IMAGE_KEY);
			int trackableState = routeTrackable.getInt(Trackable.STATE_KEY);
			if (routeTrackable.getString(Trackable.IMAGE_KEY).equals("null")) {
				trackableImageURL = routeTrackable
						.getString(Trackable.IMAGE_DEFAULT_KEY);
			} else {
				trackableImageURL = routeTrackable
						.getString(Trackable.IMAGE_KEY);
			}

			JSONArray usersArray = new JSONArray(
					routeTrackable.getString(Trackable.USERS_KEY));
			// Retrieve users from jsonObject
			ArrayList<User> users = loadUsersArray(usersArray);
			Trackable trackable = new Trackable(trackableId, trackableName,
					trackableSubName, trackableDescription, trackableBirthday,
					trackableCourse, trackableContactPhone,
					trackableContactAddress, trackableImageURL, users,
					trackableState, true);
			trackables.add(trackable);

		}

		return trackables;
	}

	static ArrayList<Point> loadStepsArray(JSONArray stepsArray)
			throws JSONException {
		ArrayList<Point> steps = new ArrayList<Point>();
		for (int j = 0; j < stepsArray.length(); j++) {
			JSONObject currentStep = stepsArray.getJSONObject(j);
			double longitude = currentStep.getDouble(Point.LONGITUDE_KEY);
			double latitude = currentStep.getDouble(Point.LATITUDE_KEY);
			int order = currentStep.getInt(Point.POSITION_KEY);
			Point step = new Point(longitude, latitude, 2600.0, order);
			steps.add(step);

		}

		return steps;
	}

	// Retrieve stops array
	static ArrayList<Stop> loadStopsArray(JSONArray stopsArray)
			throws JSONException {
		ArrayList<Stop> stops = new ArrayList<Stop>();
		for (int i = 0; i < stopsArray.length(); i++) {
			JSONObject currentObject = stopsArray.getJSONObject(i);
			int id = currentObject.getInt(Stop.ID_KEY);
			double longitude = currentObject.getDouble(Stop.LONGITUDE_KEY);
			double latitude = currentObject.getDouble(Stop.LATITUDE_KEY);
			double altitude = currentObject.getDouble(Stop.LONGITUDE_KEY);
			int order = currentObject.getInt(Stop.POSITION_KEY);
			boolean special = currentObject.getInt(Stop.SPECIAL_KEY) == 1;
			JSONArray trackablesArray = new JSONArray(
					currentObject.getString(Stop.TRACKABLES_KEY));
			// Retrieve trackables from jsonObject
			ArrayList<Trackable> trackables = loadTrackablesArray(trackablesArray);
			JSONArray stepsArray = new JSONArray(
					currentObject.getString(Stop.STEPS_KEY));
			ArrayList<Point> steps = new ArrayList<Point>();
			if (order > 0) {
				steps = loadStepsArray(stepsArray);
			}

			JSONArray wayPointsArray = new JSONArray(
					currentObject.getString(Stop.POINTS_KEY));
			ArrayList<Point> wayPoints = loadWayPointsArray(wayPointsArray);

			String averageTime = currentObject.getString(Stop.AVERAGE_TIME_KEY);

			double timeDeviation = currentObject
					.getDouble(Stop.TIME_DEVIATION_KEY);

			int estimatedTimeGoogle = currentObject
					.getInt(Stop.ESTIMATED_TIME_KEY) * 60;

			String direction = currentObject.getString(Stop.ADDRESS_KEY);

			Stop stop = new Stop(id, longitude, latitude, altitude, trackables,
					steps, wayPoints, order, special, averageTime,
					timeDeviation, estimatedTimeGoogle, direction);
			stops.add(stop);
		}
		Collections.sort(stops, new Stop.StopComparator());

		return stops;
	}

	// Retrieve stops array
	static ArrayList<Stop> loadStopsArrayWithNovelties(JSONArray stopsArray)
			throws JSONException {
		ArrayList<Stop> stops = new ArrayList<Stop>();
		for (int i = 0; i < stopsArray.length(); i++) {
			JSONObject currentObject = stopsArray.getJSONObject(i);
			int id = currentObject.getInt(Stop.ID_KEY);
			if (id != -1) {
				double longitude = currentObject.getDouble(Stop.LONGITUDE_KEY);
				double latitude = currentObject.getDouble(Stop.LATITUDE_KEY);
				double altitude = currentObject.getDouble(Stop.LONGITUDE_KEY);
				int order = currentObject.getInt(Stop.POSITION_KEY);
				boolean special = currentObject.getInt(Stop.SPECIAL_KEY) == 1;
				JSONArray trackablesArray = new JSONArray(
						currentObject.getString(Stop.TRACKABLES_KEY));
				// Retrieve trackables from jsonObject
				ArrayList<Trackable> trackables = loadTrackablesArrayWithNovelties(trackablesArray);
				JSONArray stepsArray = new JSONArray(
						currentObject.getString(Stop.STEPS_KEY));
				ArrayList<Point> steps = new ArrayList<Point>();
				if (order > 0) {
					steps = loadStepsArray(stepsArray);
				}

				JSONArray wayPointsArray = new JSONArray(
						currentObject.getString(Stop.POINTS_KEY));
				ArrayList<Point> wayPoints = loadWayPointsArray(wayPointsArray);

				String averageTime = currentObject
						.getString(Stop.AVERAGE_TIME_KEY);
				double timeDeviation = currentObject
						.getDouble(Stop.TIME_DEVIATION_KEY);

				int estimatedTimeGoogle = currentObject
						.getInt(Stop.ESTIMATED_TIME_KEY) * 60;

				String direction = currentObject.getString(Stop.ADDRESS_KEY);

				Stop stop = new Stop(id, longitude, latitude, altitude,
						trackables, steps, wayPoints, order, special,
						averageTime, timeDeviation, estimatedTimeGoogle,
						direction);
				stops.add(stop);
			} else {
				JSONArray trackablesArray = new JSONArray(
						currentObject.getString(Stop.TRACKABLES_KEY));
				// Retrieve trackables from jsonObject
				loadTrackablesArrayFromExtraStop(trackablesArray);
			}
		}
		Collections.sort(stops, new Stop.StopComparator());

		return stops;
	}

	// Retrieve waypoints array
	static ArrayList<Point> loadWayPointsArray(JSONArray wayPointsArray)
			throws JSONException {
		ArrayList<Point> wayPoints = new ArrayList<Point>();
		for (int i = 0; i < wayPointsArray.length(); i++) {
			JSONObject currentObject = wayPointsArray.getJSONObject(i);
			double longitude = currentObject.getDouble(Point.LONGITUDE_KEY);
			double latitude = currentObject.getDouble(Point.LATITUDE_KEY);
			double altitude = currentObject.getDouble(Point.LONGITUDE_KEY);
			int order = currentObject.getInt(Point.POSITION_KEY);
			Point wayPoint = new Point(longitude, latitude, altitude, order);
			wayPoints.add(wayPoint);
		}
		Collections.sort(wayPoints, new Point.PointComparator());

		for (Point currentWaypoint : wayPoints) {
			System.out.println(currentWaypoint.getOrder());
		}
		return wayPoints;
	}

	public static void loadFootprint(String jsonString) throws JSONException {
		JSONObject jsonObject;

		jsonObject = new JSONObject(jsonString);
		JSONObject footprintObject = jsonObject
				.getJSONObject(ServiceKeys.FOOTPRINT_KEY);
		String footprint = footprintObject.getString(ServiceKeys.ID_KEY);

		// Guarda el id del tracked_stop asociado a un stop
		JSONArray trackedStopsArray = new JSONArray(
				jsonObject.getString(ServiceKeys.TRACKED_STOPS_KEY));

		boolean isNewFootprint = jsonObject
				.getInt(ServiceKeys.NEW_FOOTPRINT_KEY) == 0;
		OnTrackManager.getInstance().setRouteReloaded(!isNewFootprint);

		// Parámetro para indicar si ya se encontró el paradero siguiente
		// Explicado en LÓGICA DE RECARGA DE RUTA, CUANDO SE REINICIE POR FALLA TÉCNICA
		boolean nextStopFound = false;

		for (int i = 0; i < trackedStopsArray.length(); i++) {
			JSONObject currentStopObject = trackedStopsArray.getJSONObject(i);
			int trackedStopId = currentStopObject.getInt(ServiceKeys.ID_KEY);
			int stopId = currentStopObject.getInt(ServiceKeys.FK_STOP_KEY);
			Stop stop = OnTrackManager.getInstance().getUser().getRoute()
					.findStopById(stopId);
			stop.setTrackedStopId(trackedStopId);

			String realTime = currentStopObject
					.getString(ServiceKeys.REAL_TIME_KEY);
			if (!realTime.equals("null")) {
				stop.setRealTime(realTime);
				stop.setDidStopHere(true);
			} else {
                /* LÓGICA DE RECARGA DE RUTA, CUANDO SE REINICIE POR FALLA TÉCNICA */
				// * Se busca cual es el siguiente paradero, asumiendo que los tracked_stops vienen en orden
				// (Lo anterior depende del servidor)
				// * Para ese siguiente paradero se calcula el tiempo estimado basado en la hora actual y la
				// * hora estimada que quedó en el servidor
				// El paradero siguiente es el primero que se encuentre sin un real time
				if (!isNewFootprint) {
					if (!nextStopFound) {
						String serverEstimatedTimeString = currentStopObject
								.getString(ServiceKeys.ESTIMATED_TIME_KEY).split(" ")[1];
						try {
							double serverEstimatedTime = Timestamp.getSecondsFromHHMMSS(serverEstimatedTimeString);
							double nowTime = Timestamp.getCurrentTimeInSeconds();
							int difference = (int) (serverEstimatedTime - nowTime);
							if (difference > 0) {
								stop.setEstimatedTime(difference);
							} else {
								stop.setEstimatedTime(0);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						nextStopFound = true;
					}
				}
			}


			String departureTime = currentStopObject
					.getString(ServiceKeys.DEPARTURE_TIME_KEY);
			if (!departureTime.equals("null")) {
				stop.setDepartureTime(departureTime);
			}
			// Si el footprint es nuevo, se asigna como realtime del primer
			// paradero el paradero actual
			if (stop.getOrder() == 0 && isNewFootprint) {
				stop.setRealTime(Timestamp.completeTimestamp());
				stop.setDidStopHere(true);

			}

			if (stop.getOrder() == 0 && !isNewFootprint) {
				if (realTime.equals("null")) {
					stop.setRealTime(Timestamp.completeTimestamp());
					OnTrackManager.getInstance().setRouteReloaded(false);
				}
			}


			stop.setCancelled(currentStopObject.getInt(ServiceKeys.STATE_KEY) == 1);

			// Revisar el arreglo de Tracked Stop Trackables
			JSONArray trackedStopTrackablesArray = currentStopObject
					.getJSONArray(ServiceKeys.TRACKEDSTOPTRACKABLES_KEY);
			for (int j = 0; j < trackedStopTrackablesArray.length(); j++) {
				JSONObject currentTrackedStopTrackableObject = trackedStopTrackablesArray
						.getJSONObject(j);
				int trackedStopTrackableId = currentTrackedStopTrackableObject
						.getInt(ServiceKeys.ID_KEY);
				String fkTrackable = currentTrackedStopTrackableObject
						.getString(ServiceKeys.FK_TRACKABLE_KEY);
				int picked = currentTrackedStopTrackableObject
						.getInt(ServiceKeys.PICKED_KEY);
				int state = currentTrackedStopTrackableObject
						.getInt(ServiceKeys.STATE_KEY);
				Trackable trackable = stop.getTrackableById(fkTrackable);
				if (trackable != null) {
					trackable.setTrackedStopTrackableId(trackedStopTrackableId);
//					if (stop.didStopHere()) {
					if(!isNewFootprint) {
						trackable.setPicked(picked);
					}
//					}
					trackable.setState(state);
				}
			}

		}

		// Revisar el arreglo de FootprintTrackables
		JSONArray footprintTrackablesArray = jsonObject
				.getJSONArray(ServiceKeys.FOOTPRINTTRACKABLES_KEY);
		for (int j = 0; j < footprintTrackablesArray.length(); j++) {
			JSONObject currentFootprintTrackableObject = footprintTrackablesArray
					.getJSONObject(j);
			int footprintTrackableId = currentFootprintTrackableObject
					.getInt(ServiceKeys.ID_KEY);

			String fkTrackable = currentFootprintTrackableObject
					.getString(ServiceKeys.FK_TRACKABLE_KEY);
			int picked = currentFootprintTrackableObject
					.getInt(ServiceKeys.PICKED_KEY);

			Trackable trackable = OnTrackManager.getInstance().getUser()
					.getRoute().getTrackableById(fkTrackable);
			if (trackable != null) {
//				Log.d("footprintTrackable", "" + footprintTrackableId);
				trackable.setFootprintTrackableId(footprintTrackableId);
				trackable.setPicked(picked);
			}

		}

		OnTrackManager.getInstance().getUser()
				.setFootprint(Integer.parseInt(footprint));

	}

	public static void loadFootprint2(String jsonString) throws JSONException {
		JSONObject jsonObject;

		jsonObject = new JSONObject(jsonString);
		JSONObject footprintObject = jsonObject
				.getJSONObject(ServiceKeys.FOOTPRINT_KEY);
		String footprint = footprintObject.getString(ServiceKeys.ID_KEY);

		boolean isNewFootprint = jsonObject
				.getInt(ServiceKeys.NEW_FOOTPRINT_KEY) == 0;
		OnTrackManager.getInstance().setRouteReloaded(!isNewFootprint);

		// Revisar el arreglo de FootprintTrackables
		JSONArray footprintTrackablesArray = jsonObject
				.getJSONArray(ServiceKeys.FOOTPRINTTRACKABLES_KEY);
		for (int j = 0; j < footprintTrackablesArray.length(); j++) {
			JSONObject currentFootprintTrackableObject = footprintTrackablesArray
					.getJSONObject(j);
			int footprintTrackableId = currentFootprintTrackableObject
					.getInt(ServiceKeys.ID_KEY);

			String fkTrackable = currentFootprintTrackableObject
					.getString(ServiceKeys.FK_TRACKABLE_KEY);
			int picked = currentFootprintTrackableObject
					.getInt(ServiceKeys.PICKED_KEY);

			String position = currentFootprintTrackableObject
					.getString(ServiceKeys.POSITION_KEY);

			if (!position.equals("null")) {
				int positionInt = Integer.parseInt(position);
				if (FreeModeLocationManager.getInstance().getCurrentTrackablePosition() < positionInt) {
					FreeModeLocationManager.getInstance().setCurrentTrackablePosition(positionInt);
					double latitude = currentFootprintTrackableObject
							.getDouble(ServiceKeys.LATITUDE_KEY);
					double longitude = currentFootprintTrackableObject
							.getDouble(ServiceKeys.LONGITUDE_KEY);
					Location locationOfLastReport = FreeModeLocationManager.getInstance().getLocation();
					if(locationOfLastReport!=null) {
						locationOfLastReport.setLatitude(latitude);
						locationOfLastReport.setLongitude(longitude);
						FreeModeLocationManager.getInstance().setLocationOfLastReport(locationOfLastReport);
					}

				}
			}

			Trackable trackable = FreeModeManager.getInstance().getRouteTrackableById(fkTrackable);
			if (trackable != null) {
				trackable.setFootprintTrackableId(footprintTrackableId);
				trackable.setPicked(picked);
			}
		}


		OnTrackManager.getInstance().getUser()
				.setFootprint(Integer.parseInt(footprint));
	}

	public static void loadCompleteTrack(String jsonString)
			throws JSONException {
		JSONObject jsonObject = new JSONObject(jsonString);
		JSONArray trackedPointsArray = jsonObject
				.getJSONArray(ServiceKeys.TRACKED_POINTS_KEY);

		for (int i = 0; i < trackedPointsArray.length(); i++) {
			JSONObject trackedPointObject = trackedPointsArray.getJSONObject(i);
			double longitude = trackedPointObject
					.getDouble(ServiceKeys.LONGITUDE_KEY);
			double latitude = trackedPointObject
					.getDouble(ServiceKeys.LATITUDE_KEY);
			double altitude = trackedPointObject
					.getDouble(ServiceKeys.ALTITUDE_KEY);
			String date = trackedPointObject.getString(ServiceKeys.DATE_KEY);
			double accuracy = trackedPointObject
					.getDouble(ServiceKeys.ACCURACY_KEY);
			double speed = trackedPointObject.getDouble(ServiceKeys.SPEED_KEY);
			double bearing = trackedPointObject
					.getDouble(ServiceKeys.BEARING_KEY);
			TrackedPoint trackedPoint = new TrackedPoint(longitude, latitude,
					altitude, date, accuracy, speed, bearing);
			OnTrackManager.getInstance().addTrackedPoint(trackedPoint);
		}
	}

	public static void loadETACalculations(String jsonString,
										   Stop[] destinations) {
		JSONObject jsonObject;
		try {
			jsonObject = new JSONObject(jsonString);
			JSONArray routes = jsonObject.getJSONArray("routes");

			for (int i = 0; i < routes.length(); i++) {
				JSONObject route = routes.getJSONObject(i);

				int totalDuration = 0, totalDistance = 0;

				JSONArray legs = route.getJSONArray("legs");

				for (int j = 0; j < legs.length(); j++) {
					JSONObject leg = legs.getJSONObject(j);

					String direction = leg.getString("end_address");

					JSONObject distance = leg.getJSONObject("distance");
					JSONObject duration = leg.getJSONObject("duration");

					totalDuration += duration.getInt("value");
					totalDistance += distance.getInt("value");

					destinations[j]
							.setGoogleETAToStopFromLastStop(totalDuration);
					destinations[j]
							.setGoogleDistanceToStopFromLastStop(totalDistance);
					destinations[j].setDirection(direction);
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void loadRouteFromGoogleDirections(String jsonString,
													 int currentRequest, int nextRequest) {
		JSONObject jsonObject;
		try {
			int duration = 0;
			int distance = 0;
			String direction = "";

			Route route = OnTrackManager.getInstance().getUser().getRoute();

			jsonObject = new JSONObject(jsonString);
			JSONArray routesArray = jsonObject.getJSONArray("routes");
			if (nextRequest < OnTrackManager.getInstance().getUser().getRoute()
					.getStops().size()) {
				Stop stop = route.getStops().get(nextRequest);
				stop.getSteps().clear();
			}

			for (int i = 0; i < routesArray.length(); i++) {
				JSONObject routeObject = routesArray.getJSONObject(i);
				JSONArray legsArray = routeObject.getJSONArray("legs");
				for (int j = 0; j < legsArray.length(); j++) {
					JSONObject legObject = legsArray.getJSONObject(j);
					JSONArray stepsArray = legObject.getJSONArray("steps");
					for (int k = 0; k < stepsArray.length(); k++) {
						JSONObject stepObject = stepsArray.getJSONObject(k);
						JSONObject polylineObject = stepObject
								.getJSONObject("polyline");
						String encodedPoints = polylineObject
								.getString("points");
						route.decodeRouteForStop(encodedPoints, nextRequest);
					}

					// De cada leg, obtener el tiempo estimado
					JSONObject jsonDuration = legObject
							.getJSONObject("duration");
					int legDuration = jsonDuration.getInt("value");
					duration += legDuration;

					// De cada leg, obtener la distancia
					JSONObject jsonDistance = legObject
							.getJSONObject("distance");
					int legDistance = jsonDistance.getInt("value");
					distance += legDistance;

					// La direcciï¿½n del paradero siguiente es la del ï¿½ltimo
					// leg
					// de la ï¿½ltima ruta
					if (i == routesArray.length() - 1
							&& j == legsArray.length() - 1) {
						direction = legObject.getString("end_address");
					}
				}
			}

			if (nextRequest < OnTrackManager.getInstance().getUser().getRoute()
					.getStops().size()) {
				Stop stop = OnTrackManager.getInstance().getUser().getRoute()
						.getStops().get(nextRequest);

				stop.setEstimatedTimeGoogle(duration);
				stop.setDirection(direction);

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public static void loadRouteFromMapboxDirections(String jsonString,
													 int currentRequest, int nextRequest) {
		JSONObject jsonObject;
		try {
			int duration = 0;
			int distance = 0;
			String direction = "";

			Route route = OnTrackManager.getInstance().getUser().getRoute();


			if (nextRequest < OnTrackManager.getInstance().getUser().getRoute()
					.getStops().size()) {
				Stop stop = route.getStops().get(nextRequest);
				stop.getSteps().clear();
			}

			jsonObject = new JSONObject(jsonString);
			JSONArray routesArray = jsonObject.getJSONArray("routes");

			JSONObject routeObject = routesArray.getJSONObject(0);
			String encodedPoints = routeObject
					.getString("geometry");
			route.decodeRouteForStop(encodedPoints, nextRequest);
//					}

			// Obtener el tiempo estimado
			int legDuration = routeObject.getInt("duration");
			duration += legDuration;

//					// Obtener la distancia
			int legDistance = routeObject.getInt("distance");
			distance += legDistance;

			JSONObject destinationObject = jsonObject.getJSONObject("destination");
			JSONObject propertiesObject = destinationObject.getJSONObject("properties");

			direction = propertiesObject.getString("name");

			if (nextRequest < OnTrackManager.getInstance().getUser().getRoute()
					.getStops().size()) {
				Stop stop = OnTrackManager.getInstance().getUser().getRoute()
						.getStops().get(nextRequest);

				stop.setEstimatedTimeGoogle(duration);
				stop.setDirection(direction);

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}


	public static void loadPortionOfRouteFromGoogleDirections(
			String jsonString, Stop stop) {
		JSONObject jsonObject;
		try {
			int duration = 0;
			int distance = 0;

			Route route = OnTrackManager.getInstance().getUser().getRoute();

			route.getStops().get(stop.getOrder()).getSteps().clear();
			jsonObject = new JSONObject(jsonString);
			JSONArray routesArray = jsonObject.getJSONArray("routes");
			for (int i = 0; i < routesArray.length(); i++) {
				JSONObject routeObject = routesArray.getJSONObject(i);
				JSONArray legsArray = routeObject.getJSONArray("legs");
				for (int j = 0; j < legsArray.length(); j++) {
					JSONObject legObject = legsArray.getJSONObject(j);
					JSONArray stepsArray = legObject.getJSONArray("steps");
					for (int k = 0; k < stepsArray.length(); k++) {
						JSONObject stepObject = stepsArray.getJSONObject(k);
						JSONObject polylineObject = stepObject
								.getJSONObject("polyline");
						String encodedPoints = polylineObject
								.getString("points");
						route.decodeRouteForStop(encodedPoints, stop.getOrder());
					}

					// De cada leg, obtener el tiempo estimado
					JSONObject jsonDuration = legObject
							.getJSONObject("duration");
					int legDuration = jsonDuration.getInt("value");
					duration += legDuration;

					// De cada leg, obtener la distancia
					JSONObject jsonDistance = legObject
							.getJSONObject("distance");
					int legDistance = jsonDistance.getInt("value");
					distance += legDistance;

				}

				// Estrategia de tiempos estimados en versiï¿½n beta
				// Se usan los tiempos promedio, basados en footprints

				stop.setDistance(distance);

				// Estrategia de tiempos estimados en versiï¿½n beta
				// Se usan los tiempos promedio, basados en footprints
				if (stop.isEnabledForEstimatedTimeFromAverage()) {
					ArrayList<Stop> neighbours = route
							.findNeighboursOfStop(stop);
					if (neighbours.get(0) != null) {
						Stop stopBehind = neighbours.get(0);
						if (stopBehind.getOrder() == 0) {
							// Si se estï¿½ poniendo el tiempo estimado del
							// primer
							// paradero, se usa como referencia el tiempo
							// actual, a menos que
							// el bus salga despuï¿½s de la hora promedio al
							// primer paradero
							double stopSeconds = Timestamp
									.getSecondsFromHHMMSS(stop.getAverageTime());
							double nowSeconds = Timestamp
									.getCurrentTimeInSeconds();
							double stopBehindSeconds = Timestamp
									.getSecondsFromHHMMSS(stopBehind
											.getAverageTime());
							if (Math.abs(stopBehindSeconds - nowSeconds) > stopBehind
									.getTimeDeviation()) {
								// En este caso, puede que el bus haya salido
								// tarde, o que haya iniciado la ruta despuï¿½s
								// de
								// haber avanzado un largo trecho
								// La decisiï¿½n depende de la distancia del
								// vehï¿½culo con respecto al paradero inicial

								if (OnTrackManager.getInstance()
										.getCurrentLocation() != null) {
									Location currentLocation = OnTrackManager
											.getInstance().getCurrentLocation();
									if (currentLocation.distanceTo(stopBehind
											.getLocation()) < 0.05 * stop
											.getDistance()) {
										// En este caso, se asume que saliï¿½
										// tarde
										stop.setEstimatedTime((int) stopSeconds
												- (int) stopBehindSeconds);
									} else {
										// En este caso, se asume que se
										// notificï¿½ tarde el inicio de ruta
										stop.setEstimatedTime((int) stopSeconds
												- (int) nowSeconds);
									}
								}
							} else {
								// En este caso, quiere decir que se iniciï¿½ a
								// tiempo la ruta
								if (stopBehind
										.isEnabledForEstimatedTimeFromAverage()) {

									stop.setEstimatedTime((int) stopSeconds
											- (int) stopBehindSeconds);
								}

							}
						} else {

							if (stopBehind
									.isEnabledForEstimatedTimeFromAverage()) {
								double stopSeconds = Timestamp
										.getSecondsFromHHMMSS(stop
												.getAverageTime());
								double stopBehindSeconds = Timestamp
										.getSecondsFromHHMMSS(neighbours.get(0)
												.getAverageTime());
								stop.setEstimatedTime((int) stopSeconds
										- (int) stopBehindSeconds);
							}
						}
					}

				} else {
					if (stop.getOrder() == 1) {
						stop.setEstimatedTime(duration);
					} else {
						stop.setEstimatedTime(duration
								+ OnTrackManager.MAXIMUM_TIME_AT_STOP);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void loadPortionOfRouteFromMapboxDirections(
			String jsonString, Stop stop) {
		JSONObject jsonObject;
		try {
			int duration = 0;
			int distance = 0;


			Route route = OnTrackManager.getInstance().getUser().getRoute();

			// Se eliminan los steps correspondientes al stop involucrado en el recálculo de ruta
			// ¿Por qué no se borra directamente sobre el objeto stop que llega por parámetro?
			// Se intentará de esta manera a ver si cambia algo
			route.getStops().get(stop.getOrder()).getSteps().clear();
//			stop.getSteps().clear();

			// Acá se empieza a decodificar el json
			jsonObject = new JSONObject(jsonString);
			JSONArray routesArray = jsonObject.getJSONArray("routes");

			JSONObject routeObject = routesArray.getJSONObject(0);

			String encodedPoints = routeObject
					.getString("geometry");

			// Se envía la cadena codificada de puntos al método en la clase route que los decodifica

			route.decodeRouteForStop(encodedPoints, stop.getOrder());
//					}

			// Obtener el tiempo estimado
			int legDuration = routeObject.getInt("duration");
			duration += legDuration;

//					// Obtener la distancia
			int legDistance = routeObject.getInt("distance");
			distance += legDistance;


			// Estrategia de tiempos estimados en versiï¿½n beta
			// Se usan los tiempos promedio, basados en footprints

			stop.setDistance(distance);

			// Estrategia de tiempos estimados en versiï¿½n beta
			// Se usan los tiempos promedio, basados en footprints
			if (stop.isEnabledForEstimatedTimeFromAverage()) {
				ArrayList<Stop> neighbours = route
						.findNeighboursOfStop(stop);
				if (neighbours.get(0) != null) {
					Stop stopBehind = neighbours.get(0);
					if (stopBehind.getOrder() == 0) {
						// Si se estï¿½ poniendo el tiempo estimado del
						// primer
						// paradero, se usa como referencia el tiempo
						// actual, a menos que
						// el bus salga despuï¿½s de la hora promedio al
						// primer paradero
						double stopSeconds = Timestamp
								.getSecondsFromHHMMSS(stop.getAverageTime());
						double nowSeconds = Timestamp
								.getCurrentTimeInSeconds();
						double stopBehindSeconds = Timestamp
								.getSecondsFromHHMMSS(stopBehind
										.getAverageTime());
						if (Math.abs(stopBehindSeconds - nowSeconds) > stopBehind
								.getTimeDeviation()) {
							// En este caso, puede que el bus haya salido
							// tarde, o que haya iniciado la ruta despuï¿½s
							// de
							// haber avanzado un largo trecho
							// La decisiï¿½n depende de la distancia del
							// vehï¿½culo con respecto al paradero inicial

							if (OnTrackManager.getInstance()
									.getCurrentLocation() != null) {
								Location currentLocation = OnTrackManager
										.getInstance().getCurrentLocation();
								if (currentLocation.distanceTo(stopBehind
										.getLocation()) < 0.05 * stop
										.getDistance()) {
									// En este caso, se asume que saliï¿½
									// tarde
									stop.setEstimatedTime((int) stopSeconds
											- (int) stopBehindSeconds);
								} else {
									// En este caso, se asume que se
									// notificï¿½ tarde el inicio de ruta
									stop.setEstimatedTime((int) stopSeconds
											- (int) nowSeconds);
								}
							}
						} else {
							// En este caso, quiere decir que se iniciï¿½ a
							// tiempo la ruta
							if (stopBehind
									.isEnabledForEstimatedTimeFromAverage()) {

								stop.setEstimatedTime((int) stopSeconds
										- (int) stopBehindSeconds);
							}

						}
					} else {

						if (stopBehind
								.isEnabledForEstimatedTimeFromAverage()) {
							double stopSeconds = Timestamp
									.getSecondsFromHHMMSS(stop
											.getAverageTime());
							double stopBehindSeconds = Timestamp
									.getSecondsFromHHMMSS(neighbours.get(0)
											.getAverageTime());
							stop.setEstimatedTime((int) stopSeconds
									- (int) stopBehindSeconds);
						}
					}
				}

			} else {
				if (stop.getOrder() == 1) {
					stop.setEstimatedTime(duration);
				} else {
					stop.setEstimatedTime(duration
							+ OnTrackManager.MAXIMUM_TIME_AT_STOP);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void loadPortionOfRouteFromGoogleDirectionsForDeviation(
			String jsonString, Stop stop) {
		JSONObject jsonObject;
		try {
			// int duration = 0;
			int distance = 0;

			Route route = OnTrackManager.getInstance().getUser().getRoute();
			// double averageDeviation =
			// route.getAverageTimeDeviationFromStops();

			route.getStops().get(stop.getOrder()).getSteps().clear();
			jsonObject = new JSONObject(jsonString);
			JSONArray routesArray = jsonObject.getJSONArray("routes");
			for (int i = 0; i < routesArray.length(); i++) {
				JSONObject routeObject = routesArray.getJSONObject(i);
				JSONArray legsArray = routeObject.getJSONArray("legs");
				for (int j = 0; j < legsArray.length(); j++) {
					JSONObject legObject = legsArray.getJSONObject(j);
					JSONArray stepsArray = legObject.getJSONArray("steps");
					for (int k = 0; k < stepsArray.length(); k++) {
						JSONObject stepObject = stepsArray.getJSONObject(k);
						JSONObject polylineObject = stepObject
								.getJSONObject("polyline");
						String encodedPoints = polylineObject
								.getString("points");
						route.decodeRouteForStop(encodedPoints, stop.getOrder());
					}

					// // De cada leg, obtener el tiempo estimado
					// JSONObject jsonDuration = legObject
					// .getJSONObject("duration");
					// int legDuration = jsonDuration.getInt("value");
					// duration += legDuration;

					// De cada leg, obtener la distancia
					JSONObject jsonDistance = legObject
							.getJSONObject("distance");
					int legDistance = jsonDistance.getInt("value");
					distance += legDistance;

				}

				TrackedPoint lastTrackedPoint = OnTrackManager.getInstance()
						.getLastTrackedPoint();
				if (lastTrackedPoint != null
						&& OnTrackManager.getInstance().getCurrentStop()
						.getOrder() == 0) {
					stop.setDistance(distance
							+ (int) lastTrackedPoint.getTotalDistance());
				} else {
					stop.setDistance(distance);
				}

				// No se reajustan tiempos estimados
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void loadPortionOfRouteFromMapboxDirectionsForDeviation(
			String jsonString, Stop stop) {
		JSONObject jsonObject;
		try {
			// int duration = 0;
			int distance = 0;

			Route route = OnTrackManager.getInstance().getUser().getRoute();
			// double averageDeviation =
			// route.getAverageTimeDeviationFromStops();

			route.getStops().get(stop.getOrder()).getSteps().clear();
			jsonObject = new JSONObject(jsonString);
			JSONArray routesArray = jsonObject.getJSONArray("routes");

			JSONObject routeObject = routesArray.getJSONObject(0);

			String encodedPoints = routeObject
					.getString("geometry");
			route.decodeRouteForStop(encodedPoints, stop.getOrder());
//					}


//					// Obtener la distancia
			int legDistance = routeObject.getInt("distance");
			distance += legDistance;


			TrackedPoint lastTrackedPoint = OnTrackManager.getInstance()
					.getLastTrackedPoint();
			if (lastTrackedPoint != null
					&& OnTrackManager.getInstance().getCurrentStop()
					.getOrder() == 0) {
				stop.setDistance(distance
						+ (int) lastTrackedPoint.getTotalDistance());
			} else {
				stop.setDistance(distance);
			}

			// No se reajustan tiempos estimados

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void loadChatCards(String jsonString) throws JSONException {

		JSONObject jsonObject = new JSONObject(jsonString);
		JSONArray chatCardsArray = jsonObject
				.getJSONArray(ServiceKeys.CHATCARDS_KEY);

		// Este arreglo guarda los identificadores de los chatcards cargados
		// desde el servicios web
		ArrayList<Long> loadedIds = new ArrayList<Long>();

		for (int i = 0; i < chatCardsArray.length(); i++) {
			ChatCard chatCard;
			boolean validChatcard = true;
			JSONObject chatCardObject = chatCardsArray.getJSONObject(i);
			long serverId = chatCardObject.getLong(ServiceKeys.ID_KEY);

			JSONObject userObject = new JSONObject();
			JSONObject groupObject = new JSONObject();
			JSONObject routeObject = new JSONObject();

			// Se carga la informaciï¿½n de usuarios que corresponda;
			// Si es un chat de solo dos usuarios, se verifica la llave
			// USER_KEY
			// Si es un chat grupal, se verifica la llava GROUP_KEY

			try {
				userObject = chatCardObject.getString(ServiceKeys.USER_KEY)
						.equals("[]") ? new JSONObject() : chatCardObject
						.getJSONObject(ServiceKeys.USER_KEY);
			} catch (JSONException e) {
				e.printStackTrace();
			}

			try {
				groupObject = chatCardObject.getString(ServiceKeys.GROUP_KEY)
						.equals("[]") ? new JSONObject() : chatCardObject
						.getJSONObject(ServiceKeys.GROUP_KEY);
			} catch (JSONException e) {
				e.printStackTrace();
			}

			try {
				routeObject = chatCardObject.getString(ServiceKeys.ROUTE_KEY)
						.equals("[]") ? new JSONObject() : chatCardObject
						.getJSONObject(ServiceKeys.ROUTE_KEY);
			} catch (JSONException e) {
				e.printStackTrace();
			}

			ChatUser user = null;
			String name = null;

			ArrayList<ChatUser> users = new ArrayList<ChatUser>();
			String imageURL = null;
			ChatUser newUser;
			if (userObject.length() != 0) {
				imageURL = WebRequestManager.URL_IMAGE_BASE
						+ userObject.getString(ServiceKeys.IMAGE_KEY);
				// Leer el objeto user
				newUser = loadChatUser(userObject);
				name = newUser.getFullName();
				users.add(newUser);
				// No se puede cargar en la lista de usuarios el usuario que
				// estï¿½ autenticado, porque no deberï¿½a poder
				// chatear con ï¿½l mismo
				if (newUser.getUserId().equals(
						OnTrackManager.getInstance().getUser().getId()))
					validChatcard = false;

			} else if (groupObject.length() != 0) {
				// Leer el objeto group
				int groupId = groupObject.getInt(ServiceKeys.ID_KEY);
				name = groupObject.getString(ServiceKeys.NAME_KEY);

			} else if (routeObject.length() != 0) {
				// Completar el mï¿½todo para cargar informaciï¿½n de la ruta
			}

			if (validChatcard) {
				ChatCard candidate = OnTrackManager.getInstance()
						.getChatCardFromLocalDB(serverId);
				if (candidate == null) {
					chatCard = new ChatCard(null, name, serverId, imageURL);

					// Agrega el chatcard a la base de datos
					OnTrackManager.getInstance().insertChatCardToLocalDB(
							chatCard);
				} else {
					candidate.setImageUrl(imageURL);
					candidate.setName(name);
					OnTrackManager.getInstance().getChatCardDao()
							.update(candidate);

				}
			}

			loadedIds.add(serverId);
		}

		// Se revisan los identificadores de los chatcards que fueron cargados.
		// En caso de que haya uno sobrante, se borra
		List<ChatCard> chatCards = OnTrackManager.getInstance()
				.getChatCardListFromLocalDB();
		for (ChatCard chatCard : chatCards) {
			boolean isInLoadedIds = false;
			for (Long currentLoadedId : loadedIds) {
				if (chatCard.getServerId().equals(currentLoadedId)) {
					isInLoadedIds = true;
					break;
				}
			}
			if (!isInLoadedIds) {

				OnTrackManager.getInstance()
						.deleteChatCardFromLocalDB(chatCard);
			}
		}

		MessageManager.getInstance().setChatCards(
				OnTrackManager.getInstance().getChatCardListFromLocalDB());
	}

	public static ChatUser loadChatUser(JSONObject userObject)
			throws JSONException {

		// Leer el objeto user
		String userId = userObject.getString(User.ID_KEY);

		// Verificar primero si el usuario ya estï¿½ en la base de datos local
		ChatUser chatUser = OnTrackManager.getInstance()
				.getChatUserFromLocalDB(userId);
		String firstName = userObject.getString(User.FIRST_NAME_KEY);
		String lastName = userObject.getString(User.LAST_NAME_KEY);
		String contactAddress = userObject.getString(User.CONTACT_ADDRESS_KEY);
		String mobilePhone = userObject.getString(User.MOBILE_PHONE_KEY);
		String lanPhone = userObject.getString(User.LAN_PHONE_KEY);
		String role = userObject.getString(User.ROLE_KEY);
		String lastSeen = userObject.getString(ServiceKeys.LAST_SEEN_KEY);
		String imageURL = WebRequestManager.URL_IMAGE_BASE
				+ userObject.getString(ServiceKeys.IMAGE_KEY);

		if (chatUser == null) {
			// Insertar el ChatUser en la base de datos local
			chatUser = new ChatUser(null, userId, firstName, lastName,
					contactAddress, mobilePhone, lanPhone, role, lastSeen,
					imageURL);
			OnTrackManager.getInstance().insertChatUserToLocalDB(chatUser);
		} else {
			chatUser.setFirstName(firstName);
			chatUser.setLastName(lastName);
			chatUser.setContactAddress(contactAddress);
			chatUser.setMobilePhone(mobilePhone);
			chatUser.setLanPhone(lanPhone);
			chatUser.setRole(role);
			chatUser.setLastSeen(lastSeen);
			OnTrackManager.getInstance().getChatUser().update(chatUser);
		}
		return chatUser;
	}

	// public static ArrayList<Long> loadNewIncomingMessages(String jsonString)
	// throws JSONException {
	//
	// ArrayList<Long> idsOfModifiedChatCards = new ArrayList<Long>();
	// JSONObject jsonObject = new JSONObject(jsonString);
	// JSONArray chatCardsArray = jsonObject.getJSONArray("CHATCARDS");
	//
	// // Log.d("Ids de chatcards",
	// // ChatManager.getInstance().idsOfChatCards());
	//
	// for (int i = 0; i < chatCardsArray.length(); i++) {
	//
	// JSONObject chatCardObject = chatCardsArray.getJSONObject(i);
	//
	// String tmpUser = chatCardObject.getString(ServiceKeys.FK_USER_KEY);
	//
	// MessageManager.getInstance().idsOfChatCards();
	// long serverId = chatCardObject.getInt(ServiceKeys.ID_KEY);
	//
	// if (!tmpUser.equals("null")) {
	// // Ir profundo para saber el verdadero id, en caso de que el
	// // chat sea individual
	// JSONArray messagesArray = chatCardObject
	// .getJSONArray(ServiceKeys.MESSAGE_SENDER_CHATCARDS_KEY);
	// JSONObject sampleMessage = messagesArray.getJSONObject(0);
	// JSONObject senderUserMessage = sampleMessage
	// .getJSONObject(ServiceKeys.SENDER_USER_KEY);
	// JSONArray innerChatCardArray = senderUserMessage
	// .getJSONArray(ServiceKeys.CHATCARDS_KEY);
	// JSONObject innerChatCard = innerChatCardArray.getJSONObject(0);
	// serverId = innerChatCard.getLong(ServiceKeys.ID_KEY);
	//
	// }
	//
	// // Obtener el chatcard ya guardado en la base de datos local
	// // ChatCard chatCard = MessageManager.getInstance()
	// // .getChatCardById(serverId);
	// ChatCard chatCard = OnTrackSystem.getInstance()
	// .getChatCardFromLocalDB(serverId);
	//
	// JSONArray messagesArray = chatCardObject
	// .getJSONArray(ServiceKeys.MESSAGE_SENDER_CHATCARDS_KEY);
	// for (int j = 0; j < messagesArray.length(); j++) {
	// idsOfModifiedChatCards.add(serverId);
	// JSONObject messageObject = messagesArray.getJSONObject(j);
	//
	// JSONObject senderUserObject = messageObject
	// .getJSONObject(ServiceKeys.SENDER_USER_KEY);
	//
	// String senderId = senderUserObject.getString(User.ID_KEY);
	// String senderFirstName = senderUserObject
	// .getString(User.FIRST_NAME_KEY);
	// String senderLastName = senderUserObject
	// .getString(User.LAST_NAME_KEY);
	// String senderContactAddress = senderUserObject
	// .getString(User.CONTACT_ADDRESS_KEY);
	// String senderMobilePhone = senderUserObject
	// .getString(User.MOBILE_PHONE_KEY);
	// String senderLanPhone = senderUserObject
	// .getString(User.LAN_PHONE_KEY);
	// String senderRole = senderUserObject.getString(User.ROLE_KEY);
	// String senderLastSeen = senderUserObject
	// .getString(ServiceKeys.LAST_SEEN_KEY);
	// String senderImage = senderUserObject
	// .getString(ServiceKeys.IMAGE_KEY);
	//
	// // ChatUser sender = new ChatUser(senderId, senderFirstName,
	// // senderLastName, senderContactAddress,
	// // senderMobilePhone, senderLanPhone, senderRole, null,
	// // senderLastSeen);
	//
	// // Se verifica que el usuario que envï¿½a el mensaje ya estï¿½
	// // guardado en la base de datos local, de lo contrario se agrega
	//
	// ChatUser sender = OnTrackSystem.getInstance()
	// .getChatUserFromLocalDB(senderId);
	// if (sender == null) {
	// sender = new ChatUser(null, senderId, senderFirstName,
	// senderLastName, senderContactAddress,
	// senderMobilePhone, senderLanPhone, senderRole,
	// senderLastSeen, senderImage);
	// OnTrackSystem.getInstance().insertChatUserToLocalDB(sender);
	// }
	//
	// String messageText = messageObject
	// .getString(ServiceKeys.MESSAGE_KEY);
	//
	// String dateServer = messageObject
	// .getString(ServiceKeys.DATE_SERVER_KEY);
	// if (dateServer != null)
	// MessageManager.getInstance().setLastDateNewMessages(
	// dateServer);
	//
	// Message message = new Message(null, messageText, dateServer,
	// true, false, sender.getId(), chatCard.getId());
	// // Agregar el mensaje nuevo a la base de datos
	// OnTrackSystem.getInstance().insertMessageToLocalDB(message);
	//
	// chatCard.getMessages().add(message);
	// }
	// }
	//
	// return idsOfModifiedChatCards;
	// }

	public static ArrayList<Long> loadMessageFromNotification(String jsonString)
			throws JSONException {
		ArrayList<Long> ids = new ArrayList<Long>();

		JSONObject jsonObject = new JSONObject(jsonString);
		String notificationJSONString = jsonObject
				.getString(ServiceKeys.NOTIFICATION_KEY);
		notificationJSONString = notificationJSONString.replace("\\\"", "\"");

		JSONObject notificationObject = new JSONObject(notificationJSONString);

		// Obtener informaciï¿½n bï¿½sica de la notificaciï¿½n para agregarla a
		// la db
		// local
		long id = notificationObject.getLong(ServiceKeys.ID_KEY);
		String title = notificationObject.getString(ServiceKeys.TITLE_KEY);
		String detail = notificationObject.getString(ServiceKeys.DETAIL_KEY);
		String dateNotification = notificationObject
				.getString(ServiceKeys.DATE_KEY);
		String dateAck = Timestamp.completeTimestamp();
		OnTrackManager.getInstance().insertNotificationToLocalDB(
				new Notification(null, id, title, detail, dateNotification,
						dateAck));

		// Versiï¿½n temporal sujeta a cambios
		JSONObject messageRecipientObject = null;

		try {
			messageRecipientObject = notificationObject
					.getJSONObject(ServiceKeys.MESSAGE_RECIPIENT_CHATCARD_KEY);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		if (messageRecipientObject != null) {

			// Leer objeto mensaje
			JSONObject messageObject = messageRecipientObject
					.getJSONObject(ServiceKeys.MESSAGE_KEY);
			String messageText = messageObject
					.getString(ServiceKeys.MESSAGE_KEY);
			try {
				messageText = URLDecoder.decode(messageText, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String date = messageObject.getString(ServiceKeys.DATE_SERVER_KEY);

			JSONObject senderUserObject = messageObject
					.getJSONObject(ServiceKeys.SENDER_USER_KEY);
			JSONObject individualChatCardObject = senderUserObject
					.getJSONObject(ServiceKeys.INDIVIDUAL_CHATCARD_KEY);
			long chatCardId = individualChatCardObject
					.getLong(ServiceKeys.ID_KEY);

			// Obtener el chatcard de la base de datos, en caso de que no
			// exista, se agrega
			// TODO: Manejar el caso en que llegue un mensaje de un chatcard
			// nuevo
			ChatCard chatCard = OnTrackManager.getInstance()
					.getChatCardFromLocalDB(chatCardId);

			if (chatCard != null) {

				ids.add(chatCardId);

				// String userId = individualChatCardObject
				// .getString(ServiceKeys.FK_USER_KEY);

				ChatUser from = loadChatUser(individualChatCardObject
						.getJSONObject(ServiceKeys.USER_KEY));

				Message message = new Message(null, messageText, date, true,
						false, from.getId(), chatCard.getId());
				OnTrackManager.getInstance().insertMessageToLocalDB(message);

				chatCard.getMessages().add(message);
			}
		}

		return ids;
	}

	public static Event loadEvent(String jsonString) throws JSONException {

		JSONObject jsonObject = new JSONObject(jsonString);
		String notificationJSONString = jsonObject.getString( "NOTIFICATION" );
		notificationJSONString = notificationJSONString.replace("\\\"", "\"");

		JSONObject notificationObject = new JSONObject(notificationJSONString);

		// Obtener informaciï¿½n bï¿½sica de la notificaciï¿½n para agregarla a
		// la db
		// local
		long id = notificationObject.getLong(ServiceKeys.ID_KEY);
		String title = notificationObject.getString(ServiceKeys.TITLE_KEY);
		String detail = notificationObject.getString(ServiceKeys.DETAIL_KEY);
		String dateNotification = notificationObject
				.getString(ServiceKeys.DATE_KEY);
		String dateAck = Timestamp.completeTimestamp();
		OnTrackManager.getInstance().insertNotificationToLocalDB(
				new Notification(null, id, title, detail, dateNotification,
						dateAck));

		// Versiï¿½n temporal sujeta a cambios

		JSONObject eventUserObject = null;

		try {
			eventUserObject = notificationObject.getJSONObject( "EVENT_USER" );
		} catch (JSONException e) {
			e.printStackTrace();
		}

		if (eventUserObject != null) {
			JSONObject eventObject = eventUserObject
					.getJSONObject( ServiceKeys.EVENT_KEY );

			String description = "El coordinador de transporte "
					+ eventObject.getString(ServiceKeys.DESCRIPTION_KEY);
			String date = eventObject.getString(ServiceKeys.DATE_SERVER_KEY);
			String category = eventObject.getJSONObject(
					ServiceKeys.CATEGORY_KEY).getString(ServiceKeys.ID_KEY);
			String fkDestinations = "-1";

			Event event;

			if (category.equals(Event.GENERAL_MESSAGE_CODE)
					|| category.equals(Event.ROUTE_MESSAGE_CODE)) {

				// Solo carga de pusher eventos que corresponden a anuncios
				// generales
				if (category.equals(Event.GENERAL_MESSAGE_CODE)) {
					fkDestinations = eventObject.getString("FK_ORGANIZATION");
				} else if (category.equals(Event.ROUTE_MESSAGE_CODE)) {
					fkDestinations = eventObject.getString("FK_ROUTE");
				}
				event = new Event(null, date, category, description, 1, 0,
						fkDestinations, "-1", false);
//				long eventId = OnTrackManager.getInstance()
//						.insertEventToLocalDB(event);
				OnTrackManager.getInstance().getEvents( ).add( event );
//				return eventId;
				return event;
			}

		}

		return null;
	}

	public static Report loadReport(String jsonString) throws JSONException {
		ArrayList<Integer> ids = new ArrayList<Integer>();

		JSONObject jsonObject = new JSONObject(jsonString);
		String notificationJSONString = jsonObject
				.getString( ServiceKeys.NOTIFICATION_KEY );
		notificationJSONString = notificationJSONString.replace("\\\"", "\"");

		JSONObject notificationObject = new JSONObject(notificationJSONString);

		// Versiï¿½n temporal sujeta a cambios

		JSONObject reportObject = null;

		try {
			reportObject = notificationObject.getJSONObject( "REPORT" );
		} catch (JSONException e) {
			e.printStackTrace();
		}

		if (reportObject != null) {
			// TODO: Procesar la respuesta al reporte
			String date = Timestamp.partialTimestamp();
			String category = reportObject.getString("FK_CATEGORY_REPORT");
			String description = reportObject.getString("DESCRIPTION");
			double latitude = reportObject.getDouble( "LATITUDE" );
			double longitude = reportObject.getDouble("LONGITUDE");
			String response = reportObject.getString("RESPONSE_TEXT");
			int state = reportObject.getInt( "STATE" );

			// Report report = new Report(date, category, description, latitude,
			// longitude, "-1", response, state);

			Report report = new Report(null, null, date, category, description,
					latitude, longitude, null, null, response, state);
			return report;

		}

		return null;
	}

	public static int loadNotificationType(String jsonString)
			throws JSONException {

		JSONObject jsonObject = new JSONObject(jsonString);
		JSONObject notificationObject = jsonObject
				.getJSONObject( ServiceKeys.NOTIFICATION_KEY );
		String fkEventUser = notificationObject
				.getString(ServiceKeys.FK_EVENT_USER_KEY);
		String fkMessageRecipientChatCard = notificationObject
				.getString(ServiceKeys.FK_MESSAGE_RECIPIENT_CHATCARD_KEY);
		if (!fkMessageRecipientChatCard.equals("null")) {
			JSONObject messageRecipientChatCardObject = notificationObject
					.getJSONObject(ServiceKeys.MESSAGE_RECIPIENT_CHATCARD_KEY);
			JSONObject messageObject = messageRecipientChatCardObject
					.getJSONObject(ServiceKeys.MESSAGE_KEY);
			JSONObject senderUserObject = messageObject
					.getJSONObject(ServiceKeys.SENDER_USER_KEY);
			JSONObject individualChatCardObject = senderUserObject
					.getJSONObject(ServiceKeys.INDIVIDUAL_CHATCARD_KEY);
			int chatCardId = individualChatCardObject
					.getInt(ServiceKeys.ID_KEY);

			// Especificar el chatcard seleccionado en el sistema

			MessageManager.getInstance().setSelectedChatCard(chatCardId);
			return Parameters.NOTIFICATION_TYPE_MESSAGE;
		} else if (!fkEventUser.equals("null")) {
			return Parameters.NOTIFICATION_TYPE_EVENT;
		}
		return -1;
	}

	public static void loadNotification(JSONObject notificationObject)
			throws JSONException {

		String fkEventUser = notificationObject
				.getString(ServiceKeys.FK_EVENT_USER_KEY);
		String fkMessageRecipientChatCard = notificationObject
				.getString(ServiceKeys.FK_MESSAGE_RECIPIENT_CHATCARD_KEY);
		if (!fkMessageRecipientChatCard.equals("null")) {
			// Leer nuevo mensaje
			JSONObject messageRecipientChatCardObject = notificationObject
					.getJSONObject(ServiceKeys.MESSAGE_RECIPIENT_CHATCARD_KEY);
			JSONObject messageObject = messageRecipientChatCardObject
					.getJSONObject(ServiceKeys.MESSAGE_KEY);
			JSONObject senderUserObject = messageObject
					.getJSONObject(ServiceKeys.SENDER_USER_KEY);
			JSONObject individualChatCardObject = senderUserObject
					.getJSONObject(ServiceKeys.INDIVIDUAL_CHATCARD_KEY);
			int chatCardId = individualChatCardObject
					.getInt(ServiceKeys.ID_KEY);

			// Versiï¿½n temporal sujeta a cambios
			JSONObject messageRecipientObject = null;

			try {
				messageRecipientObject = notificationObject
						.getJSONObject(ServiceKeys.MESSAGE_RECIPIENT_CHATCARD_KEY);
			} catch (JSONException e) {
				e.printStackTrace();
			}

			if (messageRecipientObject != null) {

				String messageText = messageObject
						.getString(ServiceKeys.MESSAGE_KEY);
				try {
					messageText = URLDecoder.decode(messageText, "UTF-8");
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				String date = messageObject
						.getString(ServiceKeys.DATE_SERVER_KEY);

				// Obtener el chatcard de la base de datos, en caso de que no
				// exista, se agrega
				// TODO: Manejar el caso en que llegue un mensaje de un chatcard
				// nuevo
				ChatCard chatCard = OnTrackManager.getInstance()
						.getChatCardFromLocalDB(chatCardId);

				if (chatCard != null) {

					ChatUser from = loadChatUser(individualChatCardObject
							.getJSONObject(ServiceKeys.USER_KEY));

					Message message = new Message(null, messageText, date,
							true, false, from.getId(), chatCard.getId());
					OnTrackManager.getInstance()
							.insertMessageToLocalDB(message);

					// chatCard.getMessages().add(message);
				}
			}

		} else if (!fkEventUser.equals("null")) {
			JSONObject eventUserObject = notificationObject
					.getJSONObject(ServiceKeys.EVENT_USER_KEY);
			JSONObject eventObject = eventUserObject
					.getJSONObject(ServiceKeys.EVENT_KEY);
			JSONObject categoryObject = eventObject
					.getJSONObject(ServiceKeys.CATEGORY_KEY);

			String category = categoryObject.getString(ServiceKeys.ID_KEY);
			String date = eventObject.getString(ServiceKeys.DATE_SERVER_KEY);
			int destinationRole = eventObject
					.getInt(ServiceKeys.DESTINATION_ROLE_KEY);
			String description = eventObject
					.getString(ServiceKeys.DESCRIPTION_KEY);
			int copyCoordinator = eventObject
					.getInt(ServiceKeys.COPY_COORDINATOR_KEY);
			int fkDestination = -1;
			String fkRoute = "-1";
			if (!eventObject.getString(ServiceKeys.FK_TRACKED_STOP_KEY).equals(
					"null")) {
				fkDestination = eventObject
						.getInt(ServiceKeys.FK_TRACKED_STOP_KEY);
			} else if (!eventObject.getString(ServiceKeys.FK_ROUTE_KEY).equals(
					"null")) {
				fkDestination = eventObject.getInt(ServiceKeys.FK_ROUTE_KEY);
				fkRoute = eventObject.getString(ServiceKeys.FK_ROUTE_KEY);
			} else if (!eventObject.getString(ServiceKeys.FK_ORGANIZATION_KEY)
					.equals("null")) {
				fkDestination = eventObject
						.getInt(ServiceKeys.FK_ORGANIZATION_KEY);
			} else if (!eventObject.getString(
					ServiceKeys.FK_TRACKED_STOP_TRACKABLE_KEY).equals("null")) {
				fkDestination = eventObject
						.getInt(ServiceKeys.FK_TRACKED_STOP_TRACKABLE_KEY);
			} else if (!eventObject.getString(ServiceKeys.FK_FOOTPRINT_KEY)
					.equals("null")) {
				fkDestination = eventObject
						.getInt(ServiceKeys.FK_FOOTPRINT_KEY);
			} else if (!eventObject.getString(ServiceKeys.FK_USER_DEVICE_KEY)
					.equals("null")) {
				fkDestination = eventObject
						.getInt(ServiceKeys.FK_USER_DEVICE_KEY);
			} else if (!eventObject.getString(ServiceKeys.FK_TRACKABLE_KEY)
					.equals("null")) {
				fkDestination = eventObject
						.getInt(ServiceKeys.FK_TRACKABLE_KEY);
			}

			Event event = new Event(null, date, category, description,
					destinationRole, copyCoordinator, "" + fkDestination,
					fkRoute, false);
			OnTrackManager.getInstance().insertEventToLocalDB(event);
		}

	}

	public static void loadNotifications(String jsonString)
			throws JSONException {
		JSONObject jsonObject = new JSONObject(jsonString);
		JSONArray notificationsArray = jsonObject
				.getJSONArray( ServiceKeys.NOTIFICATIONS_KEY );
		for (int i = 0; i < notificationsArray.length(); i++) {
			JSONObject notificationObject = notificationsArray.getJSONObject(i);
			loadNotification(notificationObject);
		}
	}

	private static ArrayList<Organization> loadOrganizationsArray(
			JSONArray organizationsArray) throws JSONException {
		ArrayList<Organization> organizations = new ArrayList<Organization>();

		for (int i = 0; i < organizationsArray.length(); i++) {
			JSONObject currentObject = organizationsArray.getJSONObject(i);

			String id = currentObject.getString(Organization.ID_KEY);
			String title = currentObject.getString(Organization.TITLE_KEY);
			String description = currentObject
					.getString(Organization.DESCRIPTION_KEY);
			String urlImage = currentObject.getString(Organization.IMAGE_KEY);
			String mobilePhone = currentObject
					.getString(Organization.MOBILE_PHONE_KEY);
			String localPhone = currentObject
					.getString(Organization.LOCAL_PHONE_KEY);
			String eMail = currentObject.getString(Organization.EMAIL_KEY);
			String address = currentObject.getString(Organization.ADDRESS_KEY);
			double latitude = Double.parseDouble(currentObject
					.getString(Organization.LATITUDE_KEY));
			double longitude = Double.parseDouble(currentObject
					.getString(Organization.LONGITUDE_KEY));

			String createdBy = currentObject.getString(Organization.CREATED_BY_KEY);

			Organization organization = new Organization(id, title,
					description, urlImage, mobilePhone, localPhone, eMail,
					address, latitude, longitude, createdBy);
			organizations.add(organization);
		}

		return organizations;
	}

	public static void createRoute(String jsonString) throws JSONException {
		JSONObject jsonObject = new JSONObject(jsonString);
		String status = jsonObject.getString(ServiceKeys.STATUS_KEY);
		String error = jsonObject.getString(ServiceKeys.ERROR_KEY);
	}

	public static void createRouteFromMapboxDirectionsOnRoad(Context context, String jsonString) {
		JSONObject jsonObject;
		try {
			int duration = 0;
			int distance = 0;

			jsonObject = new JSONObject(jsonString);
			JSONArray routesArray = jsonObject.getJSONArray("routes");

			JSONObject routeObject = routesArray.getJSONObject(0);

			String encodedPoints = routeObject
					.getString("geometry");

			int index = 0;

			int lat = 0, lng = 0;
			ArrayList<com.mapbox.mapboxsdk.geometry.LatLng> tmpPoints = new ArrayList<>();

			while (index < encodedPoints.length()) {
				int b, shift = 0, result = 0;

				do {
					b = encodedPoints.charAt(index++) - 63;
					result |= (b & 0x1f) << shift;
					shift += 5;
				} while (b >= 0x20);

				int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
				lat += dlat;
				shift = 0;
				result = 0;

				do {
					b = encodedPoints.charAt(index++) - 63;
					result |= (b & 0x1f) << shift;
					shift += 5;
				} while (b >= 0x20);

				int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
				lng += dlng;

				tmpPoints.add(new com.mapbox.mapboxsdk.geometry.LatLng((double) (lat) / 1000000, (double) lng / 1000000));
			}

			((MainActivity) context).setPointsToRefuge( tmpPoints );
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public static void createRouteFromMapboxDirectionsFreeMode(String jsonString) {
		JSONObject jsonObject;
		try {
			int duration = 0;
			int distance = 0;

			jsonObject = new JSONObject(jsonString);
			JSONArray routesArray = jsonObject.getJSONArray("routes");

			JSONObject routeObject = routesArray.getJSONObject(0);

			String encodedPoints = routeObject
					.getString("geometry");

			int index = 0;

			int lat = 0, lng = 0;
			ArrayList<com.mapbox.mapboxsdk.geometry.LatLng> tmpPoints = new ArrayList<>();

			while (index < encodedPoints.length()) {
				int b, shift = 0, result = 0;

				do {
					b = encodedPoints.charAt(index++) - 63;
					result |= (b & 0x1f) << shift;
					shift += 5;
				} while (b >= 0x20);

				int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
				lat += dlat;
				shift = 0;
				result = 0;

				do {
					b = encodedPoints.charAt(index++) - 63;
					result |= (b & 0x1f) << shift;
					shift += 5;
				} while (b >= 0x20);

				int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
				lng += dlng;

				tmpPoints.add(new com.mapbox.mapboxsdk.geometry.LatLng((double) (lat) / 1000000, (double) lng / 1000000));
			}

			FreeModeLocationManager.getInstance().setPointsToRefuge(tmpPoints);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public static void getRouteTrackables(String jsonString) {
		try {
			JSONObject jsonObject = new JSONObject(jsonString);
			JSONArray jsonTrackables = jsonObject
					.getJSONArray(Trackable.TRACKABLES_KEY);

			for (int i = 0; i < jsonTrackables.length(); i++) {
				JSONObject jsonTrackable = jsonTrackables.getJSONObject(i);

				String id = jsonTrackable.getString(Trackable.ID_KEY);
				String firstName = jsonTrackable.getString(Trackable.NAME_KEY);
				String lastName = jsonTrackable
						.getString(Trackable.SUBNAME_KEY);
				String image = jsonTrackable.getString(Trackable.IMAGE_KEY);

				if (image.equals("null")) {
					image = jsonTrackable
							.getString(Trackable.IMAGE_DEFAULT_KEY);
				}

				String birthDate = jsonTrackable
						.getString(Trackable.BIRTHDAY_KEY);
				String course = jsonTrackable.getString(Trackable.COURSE_KEY);
				String level = jsonTrackable.getString(Trackable.LEVEL_KEY);
				String phone = jsonTrackable
						.getString(Trackable.CONTACTPHONE_KEY);
				String mobilePhone = jsonTrackable
						.getString(Trackable.MOBILE_PHONE_KEY);
				String address = jsonTrackable
						.getString(Trackable.CONTACTADDRESS_KEY);
				String neighborhood = jsonTrackable
						.getString(Trackable.NEIGHBORHOOD_KEY);
				String district = jsonTrackable
						.getString(Trackable.DISTRICT_KEY);
				String eMail = jsonTrackable.getString(Trackable.EMAIL_KEY);

				double latitude;

				try {
					latitude = jsonTrackable.getDouble(Trackable.LATITUDE_KEY);
				} catch (Exception e) {
					latitude = 0;
				}

				double longitude;

				try {
					longitude = jsonTrackable
							.getDouble(Trackable.LONGITUDE_KEY);
				} catch (Exception e) {
					longitude = 0;
				}

				Trackable trackable = new Trackable(id, firstName, lastName,
						birthDate, course, level, eMail, phone, mobilePhone,
						address, neighborhood, district, image, new LatLng(
						latitude, longitude), true);
				trackable.setPicked(Trackable.UNKNOWN);
				FreeModeManager.getInstance().getRouteTrackables()
						.add(trackable);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public static ArrayList<Trackable> loadTrackablesByLetter(
			JSONObject jsonObject) {
		try {
			ArrayList<Trackable> trackablesByLetter = new ArrayList<Trackable>();
			JSONArray jsonTrackables = jsonObject
					.getJSONArray(Trackable.TRACKABLES_KEY);

			for (int i = 0; i < jsonTrackables.length(); i++) {
				JSONObject jsonTrackable = jsonTrackables.getJSONObject(i);

				String id = jsonTrackable.getString(Trackable.ID_KEY);
				String firstName = jsonTrackable.getString(Trackable.NAME_KEY);
				String lastName = jsonTrackable
						.getString(Trackable.SUBNAME_KEY);
				String image = jsonTrackable.getString(Trackable.IMAGE_KEY);

				if (image.equals("null")) {
					image = jsonTrackable
							.getString(Trackable.IMAGE_DEFAULT_KEY);
				}

				String birthDate = jsonTrackable
						.getString(Trackable.BIRTHDAY_KEY);
				String course = jsonTrackable.getString(Trackable.COURSE_KEY);
				String level = jsonTrackable.getString(Trackable.LEVEL_KEY);
				String phone = jsonTrackable
						.getString(Trackable.CONTACTPHONE_KEY);
				String mobilePhone = jsonTrackable
						.getString(Trackable.MOBILE_PHONE_KEY);
				String address = jsonTrackable
						.getString(Trackable.CONTACTADDRESS_KEY);
				String neighborhood = jsonTrackable
						.getString(Trackable.NEIGHBORHOOD_KEY);
				String district = jsonTrackable
						.getString(Trackable.DISTRICT_KEY);
				String eMail = jsonTrackable.getString(Trackable.EMAIL_KEY);

				double latitude;

				try {
					latitude = jsonTrackable.getDouble(Trackable.LATITUDE_KEY);
				} catch (Exception e) {
					latitude = 0;
				}

				double longitude;

				try {
					longitude = jsonTrackable
							.getDouble(Trackable.LONGITUDE_KEY);
				} catch (Exception e) {
					longitude = 0;
				}

				Trackable trackable = new Trackable(id, firstName, lastName,
						birthDate, course, level, eMail, phone, mobilePhone,
						address, neighborhood, district, image, new LatLng(
						latitude, longitude), false);
				trackablesByLetter.add( trackable );
			}

			return trackablesByLetter;
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static ArrayList<Trackable> loadTrackablesByKeyWord(
			JSONObject jsonObject) {
		try {
			ArrayList<Trackable> trackablesByKeyWord = new ArrayList<Trackable>();
			JSONArray jsonTrackables = jsonObject
					.getJSONArray(Trackable.TRACKABLES_KEY);

			for (int i = 0; i < jsonTrackables.length(); i++) {
				JSONObject jsonTrackable = jsonTrackables.getJSONObject(i);

				String id = jsonTrackable.getString(Trackable.ID_KEY);
				String firstName = jsonTrackable.getString(Trackable.NAME_KEY);
				String lastName = jsonTrackable
						.getString(Trackable.SUBNAME_KEY);
				String image = jsonTrackable.getString(Trackable.IMAGE_KEY);

				if (image.equals("null")) {
					image = jsonTrackable
							.getString(Trackable.IMAGE_DEFAULT_KEY);
				}

				String birthDate = jsonTrackable
						.getString(Trackable.BIRTHDAY_KEY);
				String course = jsonTrackable.getString(Trackable.COURSE_KEY);
				String level = jsonTrackable.getString(Trackable.LEVEL_KEY);
				String phone = jsonTrackable
						.getString(Trackable.CONTACTPHONE_KEY);
				String mobilePhone = jsonTrackable
						.getString(Trackable.MOBILE_PHONE_KEY);
				String address = jsonTrackable
						.getString(Trackable.CONTACTADDRESS_KEY);
				String neighborhood = jsonTrackable
						.getString(Trackable.NEIGHBORHOOD_KEY);
				String district = jsonTrackable
						.getString(Trackable.DISTRICT_KEY);
				String eMail = jsonTrackable.getString(Trackable.EMAIL_KEY);

				double latitude;

				try {
					latitude = jsonTrackable.getDouble(Trackable.LATITUDE_KEY);
				} catch (Exception e) {
					latitude = 0;
				}

				double longitude;

				try {
					longitude = jsonTrackable
							.getDouble(Trackable.LONGITUDE_KEY);
				} catch (Exception e) {
					longitude = 0;
				}

				Trackable trackable = new Trackable(id, firstName, lastName,
						birthDate, course, level, eMail, phone, mobilePhone,
						address, neighborhood, district, image, new LatLng(
						latitude, longitude), false);
				trackablesByKeyWord.add(trackable);
			}

			return trackablesByKeyWord;
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	private static Schedule loadSchedule(JSONObject jsonRouteSchedule)
			throws JSONException {
		if (jsonRouteSchedule.optJSONObject(Route.SCHEDULE_KEY) != null) {
			JSONObject jsonSchedule = jsonRouteSchedule
					.getJSONObject(Route.SCHEDULE_KEY);

			String id = jsonSchedule.getString(Schedule.ID_KEY);
			String name = jsonSchedule.getString(Schedule.NAME_KEY);
			boolean monday = jsonSchedule.getString(Schedule.MONDAY_KEY)
					.equals("1") ? true : false;
			boolean tuesday = jsonSchedule.getString(Schedule.TUESDAY_KEY)
					.equals("1") ? true : false;
			boolean wednesday = jsonSchedule.getString(Schedule.WEDNESDAY_KEY)
					.equals("1") ? true : false;
			boolean thursday = jsonSchedule.getString(Schedule.THURSDAY_KEY)
					.equals("1") ? true : false;
			boolean friday = jsonSchedule.getString(Schedule.FRIDAY_KEY)
					.equals("1") ? true : false;
			boolean saturday = jsonSchedule.getString(Schedule.SATURDAY_KEY)
					.equals("1") ? true : false;
			boolean sunday = jsonSchedule.getString(Schedule.SUNDAY_KEY)
					.equals("1") ? true : false;
			String startTime = jsonSchedule.getString(Schedule.START_TIME_KEY);
			String endTime = jsonSchedule.getString(Schedule.END_TIME_KEY);

			Schedule schedule = new Schedule(id, name, monday, tuesday,
					wednesday, thursday, friday, saturday, sunday, startTime,
					endTime);
			return schedule;
		} else {
			return null;
		}
	}

	private static Vehicle loadVehicle(JSONObject jsonRouteSchedule)
			throws JSONException {
		if (jsonRouteSchedule.has(Route.VEHICLES_KEY)) {
			JSONArray vehiclesArray = jsonRouteSchedule
					.getJSONArray(Route.VEHICLES_KEY);

			if (vehiclesArray.length() > 0) {
				JSONObject jsonVehicle = vehiclesArray.getJSONObject(0);

				String id = jsonVehicle.getString(Vehicle.ID_KEY);
				String year = jsonVehicle.getString(Vehicle.YEAR_KEY);
				String manufacturer = jsonVehicle
						.getString(Vehicle.MANUFACTURER_KEY);
				String model = jsonVehicle.getString(Vehicle.MODEL_KEY);
				String type = jsonVehicle.getString(Vehicle.TYPE_KEY);
				String capacity = jsonVehicle.getString(Vehicle.CAPACITY_KEY);
				String engine = jsonVehicle.getString(Vehicle.ENGINE_KEY);
				String image = jsonVehicle.getString(Vehicle.IMAGE_KEY);

				if (image.equals("null")) {
					image = jsonVehicle.getString(Vehicle.IMAGE_DEFAULT_KEY);
				}

				Vehicle vehicle = new Vehicle(id, year, manufacturer, model,
						type, capacity, engine, image);
				return vehicle;
			}
		}

		return null;
	}

	private static User loadMonitor(JSONObject jsonRouteSchedule)
			throws JSONException {
		JSONArray usersAltArray = jsonRouteSchedule
				.getJSONArray(User.USERS_ALT_KEY);

		for (int i = 0; i < usersAltArray.length(); i++) {
			JSONObject jsonUserAlt = usersAltArray.getJSONObject(i);

			if (jsonUserAlt.getInt(User.ROLE_KEY) == User.ROLE_MONITORA) {
				String id = jsonUserAlt.getString(User.ID_KEY);
				String firstName = jsonUserAlt.getString(User.FIRST_NAME_KEY);
				String lastName = jsonUserAlt.getString(User.LAST_NAME_KEY);
				String phone = jsonUserAlt.getString(User.LAN_PHONE_KEY);
				String mobilePhone = jsonUserAlt
						.getString(User.MOBILE_PHONE_KEY);
				String address = jsonUserAlt
						.getString(User.CONTACT_ADDRESS_KEY);
				String imageURL = jsonUserAlt.getString(User.IMAGE_KEY);

				if (imageURL.equals("null")) {
					imageURL = jsonUserAlt.getString(User.IMAGE_DEFAULT_KEY);
				}

				User monitor = new User(id, firstName, lastName, address,
						mobilePhone, phone, String.valueOf(User.ROLE_MONITORA),
						"", imageURL);
				return monitor;
			}
		}

		return null;
	}

	public static void getRoutesList(JSONObject jsonObject) {
		try {
			JSONArray routeSchedules = jsonObject
					.getJSONArray(Route.ROUTE_SCHEDULES_KEY);
			ArrayList<Route> routes = loadRoutesArray(routeSchedules);

			for (Route route : routes) {
				boolean didFind = false;

				for (Route route2 : OnTrackManager.getInstance().getRoutes()) {
					if (route.getId().equals(route2.getId())) {
						didFind = true;
					}
				}

				if (!didFind) {
					OnTrackManager.getInstance().getRoutes().add(route);
				}
			}

			for (Route route : OnTrackManager.getInstance().getRoutes()) {
				boolean didFind = false;

				for (Route route2 : routes) {
					if (route.getId().equals(route2.getId())) {
						didFind = true;
					}
				}

				if (!didFind) {
					OnTrackManager.getInstance().getRoutes().remove(route);
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static String getVehicleDataJSONString(String id, String year,
												  String model, String manufacturer, String type, String capacity,
												  String engine, String imageURL) throws JSONException {
		JSONObject jsonObject = new JSONObject();

		JSONObject jsonVehicle = new JSONObject();

		jsonVehicle.put(Vehicle.ID_KEY, id);
		jsonVehicle.put(Vehicle.YEAR_KEY, year);
		jsonVehicle.put(Vehicle.MODEL_KEY, model);
		jsonVehicle.put(Vehicle.MANUFACTURER_KEY, manufacturer);
		jsonVehicle.put(Vehicle.TYPE_KEY, type);
		jsonVehicle.put(Vehicle.CAPACITY_KEY, capacity);
		jsonVehicle.put(Vehicle.ENGINE_KEY, engine);
		jsonVehicle.put(ServiceKeys.URL_IMAGE_KEY, imageURL);

		jsonObject.accumulate(Vehicle.VEHICLE_KEY, jsonVehicle);

		return jsonObject.getString(Vehicle.VEHICLE_KEY);
	}

	public static String getUserDataJSONString(String id, String firstName,
											   String lastName, String phone, String mobilePhone, String address,
											   String licenseNumber, String imageURL, int type)
			throws JSONException {
		JSONObject jsonObject = new JSONObject();

		JSONObject jsonUser = new JSONObject();

		jsonUser.put(User.ID_KEY, id);
		jsonUser.put(User.FIRST_NAME_KEY, firstName);
		jsonUser.put(User.LAST_NAME_KEY, lastName);
		jsonUser.put(User.LAN_PHONE_KEY, phone);
		jsonUser.put(User.MOBILE_PHONE_KEY, mobilePhone);
		jsonUser.put(User.CONTACT_ADDRESS_KEY, address);
		if (licenseNumber != null) {
			jsonUser.put(User.LICENSE_NUMBER_KEY, licenseNumber);
		} else {
			jsonUser.put(User.LICENSE_NUMBER_KEY, "");
		}
		jsonUser.put(ServiceKeys.URL_IMAGE_KEY, imageURL);
		jsonUser.put(User.TYPE_USER_KEY, type);

		jsonObject.accumulate(User.USER_KEY, jsonUser);

		return jsonObject.getString(User.USER_KEY);
	}

	public static String getScheduleDataJSONString(String name, boolean monday,
												   boolean tuesday, boolean wednesday, boolean thursday,
												   boolean friday, boolean saturday, boolean sunday, String startTime,
												   String endTime) throws JSONException {
		JSONObject jsonObject = new JSONObject();

		JSONObject jsonSchedule = new JSONObject();

		jsonSchedule.put(Schedule.NAME_KEY, name);
		jsonSchedule.put(Schedule.START_TIME_KEY, startTime);
		jsonSchedule.put(Schedule.END_TIME_KEY, endTime);

		JSONObject jsonDays = new JSONObject();

		jsonDays.put(Schedule.MONDAY_KEY, monday ? 1 : 0);
		jsonDays.put(Schedule.TUESDAY_KEY, tuesday ? 1 : 0);
		jsonDays.put(Schedule.WEDNESDAY_KEY, wednesday ? 1 : 0);
		jsonDays.put(Schedule.THURSDAY_KEY, thursday ? 1 : 0);
		jsonDays.put(Schedule.FRIDAY_KEY, friday ? 1 : 0);
		jsonDays.put(Schedule.SATURDAY_KEY, saturday ? 1 : 0);
		jsonDays.put(Schedule.SUNDAY_KEY, sunday ? 1 : 0);

		jsonSchedule.accumulate( Schedule.DAYS_KEY, jsonDays );

		jsonObject.accumulate(Schedule.SCHEDULE_KEY, jsonSchedule);

		return jsonObject.getString(Schedule.SCHEDULE_KEY);
	}

	public static String getTrackableDataJSONString(String id,
													String firstName, String lastName, String birthDate, String course,
													String level, String eMail, String phone, String mobilePhone,
													String address, String neighborhood, String district,
													String imageURL) throws JSONException {
		JSONObject jsonObject = new JSONObject();

		JSONObject jsonTrackable = new JSONObject();

		jsonTrackable.put(Trackable.ID_KEY, id);
		jsonTrackable.put(Trackable.NAME_KEY, firstName);
		jsonTrackable.put(Trackable.SUBNAME_KEY, lastName);
		jsonTrackable.put(Trackable.BIRTHDAY_KEY, birthDate);
		jsonTrackable.put(Trackable.COURSE_KEY, course);
		jsonTrackable.put(Trackable.LEVEL_KEY, level);
		jsonTrackable.put(Trackable.EMAIL_KEY, eMail);
		jsonTrackable.put(Trackable.CONTACTPHONE_KEY, phone);
		jsonTrackable.put(Trackable.MOBILE_PHONE_KEY, mobilePhone);
		jsonTrackable.put(Trackable.CONTACTADDRESS_KEY, address);
		jsonTrackable.put(Trackable.NEIGHBORHOOD_KEY, neighborhood);
		jsonTrackable.put(Trackable.DISTRICT_KEY, district);
		jsonTrackable.put( ServiceKeys.URL_IMAGE_KEY, imageURL );

		jsonObject.accumulate(Trackable.TRACKABLE_KEY, jsonTrackable);

		return jsonObject.getString(Trackable.TRACKABLE_KEY);
	}

	public static String getEditVehicleDataJSONString(String id, String oldId,
													  String year, String model, String manufacturer, String type,
													  String capacity, String engine, String imageURL)
			throws JSONException {
		JSONObject jsonObject = new JSONObject();

		JSONObject jsonVehicle = new JSONObject();

		jsonVehicle.put(Vehicle.ID_KEY, id);
		jsonVehicle.put(Vehicle.OLD_ID_KEY, oldId);
		jsonVehicle.put(Vehicle.YEAR_KEY, year);
		jsonVehicle.put(Vehicle.MODEL_KEY, model);
		jsonVehicle.put(Vehicle.MANUFACTURER_KEY, manufacturer);
		jsonVehicle.put(Vehicle.TYPE_KEY, type);
		jsonVehicle.put(Vehicle.CAPACITY_KEY, capacity);
		jsonVehicle.put(Vehicle.ENGINE_KEY, engine);
		jsonVehicle.put(ServiceKeys.URL_IMAGE_KEY, imageURL);

		jsonObject.accumulate(Vehicle.VEHICLE_KEY, jsonVehicle);

		return jsonObject.getString(Vehicle.VEHICLE_KEY);
	}

	public static String getEditScheduleDataJSONString(String id, String name,
													   boolean monday, boolean tuesday, boolean wednesday,
													   boolean thursday, boolean friday, boolean saturday, boolean sunday,
													   String startTime, String endTime) throws JSONException {
		JSONObject jsonObject = new JSONObject();

		JSONObject jsonSchedule = new JSONObject();

		jsonSchedule.put(Schedule.ID_KEY, id);
		jsonSchedule.put(Schedule.NAME_KEY, name);
		jsonSchedule.put(Schedule.START_TIME_KEY, startTime);
		jsonSchedule.put(Schedule.END_TIME_KEY, endTime);

		JSONObject jsonDays = new JSONObject();

		jsonDays.put(Schedule.MONDAY_KEY, monday ? 1 : 0);
		jsonDays.put(Schedule.TUESDAY_KEY, tuesday ? 1 : 0);
		jsonDays.put(Schedule.WEDNESDAY_KEY, wednesday ? 1 : 0);
		jsonDays.put(Schedule.THURSDAY_KEY, thursday ? 1 : 0);
		jsonDays.put(Schedule.FRIDAY_KEY, friday ? 1 : 0);
		jsonDays.put(Schedule.SATURDAY_KEY, saturday ? 1 : 0);
		jsonDays.put(Schedule.SUNDAY_KEY, sunday ? 1 : 0);

		jsonSchedule.accumulate( Schedule.DAYS_KEY, jsonDays );

		jsonObject.accumulate(Schedule.SCHEDULE_KEY, jsonSchedule);

		return jsonObject.getString(Schedule.SCHEDULE_KEY);
	}

	public static String getEditUserDataJSONString(String id, String oldId,
												   String firstName, String lastName, String phone,
												   String mobilePhone, String address, String licenseNumber,
												   String imageURL, int type) throws JSONException {
		JSONObject jsonObject = new JSONObject();

		JSONObject jsonUser = new JSONObject();

		jsonUser.put(User.ID_KEY, id);
		jsonUser.put(User.OLD_ID_KEY, oldId);
		jsonUser.put(User.FIRST_NAME_KEY, firstName);
		jsonUser.put(User.LAST_NAME_KEY, lastName);
		jsonUser.put(User.LAN_PHONE_KEY, phone);
		jsonUser.put(User.MOBILE_PHONE_KEY, mobilePhone);
		jsonUser.put(User.CONTACT_ADDRESS_KEY, address);
		if (licenseNumber != null) {
			jsonUser.put(User.LICENSE_NUMBER_KEY, licenseNumber);
		} else {
			jsonUser.put(User.LICENSE_NUMBER_KEY, "");
		}
		jsonUser.put(ServiceKeys.URL_IMAGE_KEY, imageURL);
		jsonUser.put( User.TYPE_USER_KEY, type );

		jsonObject.accumulate(User.USER_KEY, jsonUser);

		return jsonObject.getString(User.USER_KEY);
	}

	public static String getEditTrackableDataJSONString(String id,
														String oldId, String firstName, String lastName, String birthDate,
														String course, String level, String eMail, String phone,
														String mobilePhone, String address, String neighborhood,
														String district, String imageURL) throws JSONException {
		JSONObject jsonObject = new JSONObject();

		JSONObject jsonTrackable = new JSONObject();

		jsonTrackable.put(Trackable.ID_KEY, id);
		jsonTrackable.put(Trackable.OLD_ID_KEY, oldId);
		jsonTrackable.put(Trackable.NAME_KEY, firstName);
		jsonTrackable.put(Trackable.SUBNAME_KEY, lastName);
		jsonTrackable.put(Trackable.BIRTHDAY_KEY, birthDate);
		jsonTrackable.put(Trackable.COURSE_KEY, course);
		jsonTrackable.put(Trackable.LEVEL_KEY, level);
		jsonTrackable.put(Trackable.EMAIL_KEY, eMail);
		jsonTrackable.put(Trackable.CONTACTPHONE_KEY, phone);
		jsonTrackable.put(Trackable.MOBILE_PHONE_KEY, mobilePhone);
		jsonTrackable.put(Trackable.CONTACTADDRESS_KEY, address);
		jsonTrackable.put(Trackable.NEIGHBORHOOD_KEY, neighborhood);
		jsonTrackable.put(Trackable.DISTRICT_KEY, district);
		jsonTrackable.put(ServiceKeys.URL_IMAGE_KEY, imageURL);

		jsonObject.accumulate(Trackable.TRACKABLE_KEY, jsonTrackable);

		return jsonObject.getString(Trackable.TRACKABLE_KEY);
	}

	// Cargar las novedades del día
	public static void loadNovelties(String jsonString) throws JSONException {
		JSONObject jsonObject = new JSONObject(jsonString);
		int status = jsonObject.getInt(ServiceKeys.STATUS_KEY);
		if (status == 1) {
			// Cargar las novedades al OnTrackManager
			JSONArray noveltiesArray = jsonObject
					.getJSONArray(ServiceKeys.NOVELTIES_KEY);
			for (int i = 0; i < noveltiesArray.length(); i++) {
				JSONObject noveltyObject = noveltiesArray.getJSONObject(i);
				int serverId = noveltyObject.getInt(ServiceKeys.ID_KEY);
				String fkTrackable = noveltyObject
						.getString(ServiceKeys.FK_TRACKABLE_KEY);
				String fromDate = noveltyObject
						.getString(ServiceKeys.FROM_DATE_KEY);
				String toDate = noveltyObject
						.getString(ServiceKeys.TO_DATE_KEY);
				boolean monday = noveltyObject.getInt(ServiceKeys.MONDAY_KEY) == 1;
				boolean tuesday = noveltyObject.getInt(ServiceKeys.TUESDAY_KEY) == 1;
				boolean wednesday = noveltyObject
						.getInt(ServiceKeys.WEDNESDAY_KEY) == 1;
				boolean thursday = noveltyObject
						.getInt(ServiceKeys.THURSDAY_KEY) == 1;
				boolean friday = noveltyObject.getInt(ServiceKeys.FRIDAY_KEY) == 1;
				boolean saturday = noveltyObject
						.getInt(ServiceKeys.SATURDAY_KEY) == 1;
				boolean sunday = noveltyObject.getInt(ServiceKeys.SUNDAY_KEY) == 1;
				int day = noveltyObject.getInt(ServiceKeys.DAY_KEY);
				String periodicity = noveltyObject
						.getString(ServiceKeys.PERIODICITY_KEY);
				String fkRouteOrigin = noveltyObject
						.getString(ServiceKeys.FK_ROUTE_ORIGIN_KEY);
				String fkRouteDestination = noveltyObject
						.getString(ServiceKeys.FK_ROUTE_DESTINATION_KEY);
				String stopOrigin = noveltyObject
						.getString(ServiceKeys.STOP_ORIGIN_KEY);
				String stopDestination = noveltyObject
						.getString(ServiceKeys.STOP_DESTINATION_KEY);
				String stopAddress = noveltyObject
						.getString(ServiceKeys.STOP_ADDRESS_KEY);
				String description = noveltyObject
						.getString(ServiceKeys.DESCRIPTION_KEY);
				int loopType = noveltyObject.getInt(ServiceKeys.LOOP_TYPE_KEY);
				String createdBy = noveltyObject
						.getString(ServiceKeys.CREATED_BY_KEY);
				int state = noveltyObject.getInt(ServiceKeys.STATE_KEY);
				String creationDate = noveltyObject
						.getString(ServiceKeys.CREATION_DATE_KEY);
				int type = noveltyObject.getInt(ServiceKeys.TYPE_KEY);
				int intermittence = noveltyObject
						.getInt(ServiceKeys.INTERMITTENCE_KEY);

				String hour = noveltyObject
						.getString(ServiceKeys.HOUR_KEY);

				String parentName = noveltyObject
						.getString(ServiceKeys.PARENT_NAME_KEY);

				String identificationParent = noveltyObject
						.getString(ServiceKeys.IDENTIFICATION_PARENT_KEY);

				Novelty novelty = new Novelty(serverId, fkTrackable, fromDate,
						toDate, monday, tuesday, wednesday, thursday, friday,
						saturday, sunday, day, periodicity, fkRouteOrigin,
						fkRouteDestination, stopOrigin, stopDestination,
						stopAddress, description, loopType, createdBy, state,
						creationDate, type, intermittence, hour, parentName, identificationParent);
				processNovelty(novelty,
						noveltyObject.getJSONObject(ServiceKeys.TRACKABLE_KEY));

			}
		}
	}

	// Procesa el contenido de la novedad
	public static void processNovelty(Novelty novelty,
									  JSONObject currentTrackable) throws JSONException {

		String trackableId = currentTrackable.getString(Trackable.ID_KEY);
		Trackable trackable = OnTrackManager.getInstance().getUser().getRoute()
				.findTrackableById( trackableId );

		if (trackable == null) {

			String trackableName = currentTrackable
					.getString(Trackable.NAME_KEY);
			String trackableSubName = currentTrackable
					.getString(Trackable.SUBNAME_KEY);
			String trackableDescription = currentTrackable
					.getString(Trackable.DESCRIPTION_KEY);
			String trackableBirthday = currentTrackable
					.getString(Trackable.BIRTHDAY_KEY);
			String trackableCourse = currentTrackable
					.getString(Trackable.COURSE_KEY);
			String trackableContactAddress = currentTrackable
					.getString(Trackable.CONTACTADDRESS_KEY);
			String trackableContactPhone = currentTrackable
					.getString(Trackable.CONTACTPHONE_KEY);
			String trackableImageURL = WebRequestManager.URL_IMAGE_BASE
					+ currentTrackable.getString(Trackable.IMAGE_KEY);
			int trackableState = currentTrackable.getInt(Trackable.STATE_KEY);
			if (currentTrackable.getString(Trackable.IMAGE_KEY).equals("null")) {
				trackableImageURL = currentTrackable
						.getString(Trackable.IMAGE_DEFAULT_KEY);
			} else {
				trackableImageURL = currentTrackable
						.getString(Trackable.IMAGE_KEY);
			}

			JSONArray usersArray = new JSONArray(
					currentTrackable.getString(Trackable.USERS_KEY));
			// Retrieve users from jsonObject
			ArrayList<User> users = loadUsersArray(usersArray);
			trackable = new Trackable(trackableId, trackableName,
					trackableSubName, trackableDescription, trackableBirthday,
					trackableCourse, trackableContactPhone,
					trackableContactAddress, trackableImageURL, users,
					trackableState, true);
		}

		// Primero se procesa por tipo: si es de cambio de ruta, la cosa es más
		// complicada
		if (novelty.getType() == Trackable.NOVELTY_TYPE_CHANGE_ROUTE) {
			// Si la ruta de origen y destino son la misma, entonces es un
			// cambio de paradero
			if (novelty.getFkRouteOrigin().equals(
					novelty.getFkRouteDestination())) {
				// Si el paradero de destino es nulo, se remueve el trackable
				// del paradero previamente asignado y se agrega a la lista de
				// trackables sin paradero
				OnTrackManager.getInstance().getUser().getRoute()
						.removeTrackableFromStopById(trackable.getId());
				if (novelty.getStopDestination().equals("null")) {

					OnTrackManager.getInstance().getUser().getRoute()
							.addTrackableWithoutStop(trackable);
					// De lo contrario, se asigna al paradero especificado
				} else {
					OnTrackManager
							.getInstance()
							.getUser()
							.getRoute()
							.addTrackableToStop(
									trackable,
									Integer.parseInt(novelty
											.getStopDestination()));
				}
				// Si la ruta de destino es esta, se debe agregar el trackable
				// al paradero
			} else if (novelty.getFkRouteDestination().equals(
					OnTrackManager.getInstance().getUser().getRoute().getId())) {
				if (novelty.getStopDestination().equals("null")) {
					OnTrackManager.getInstance().getUser().getRoute()
							.addTrackableWithoutStop(trackable);
					// De lo contrario, se asigna al paradero especificado
				} else {
					OnTrackManager
							.getInstance()
							.getUser()
							.getRoute()
							.addTrackableToStop(
									trackable,
									Integer.parseInt(novelty
											.getStopDestination()));
				}
			}
			// En cualquier otro caso, la verdad es que no se hace nada porque
			// lo demás es de procesamiento gráfico
			// Estos son todos los casos en los que hay que agregar estudiantes
			// nuevos a un paradero
		}

		trackable.setNovelty(novelty);
	}

	public static String getJSONForTrackablesStopAddition(
			ArrayList<Trackable> trackables, ArrayList<Stop> oldStops) {
		try {
			JSONObject jsonObject = new JSONObject();
			JSONArray trackablesArray = new JSONArray();

			for (int i = 0; i < trackables.size(); i++) {
				Trackable trackable = trackables.get(i);
				Stop stop = oldStops.get(i);
				JSONObject jsonTrackable = new JSONObject();

				jsonTrackable.put(Trackable.ID_KEY, trackable.getId());

				if (stop != null) {
					jsonTrackable
							.put(ServiceKeys.ID_OLD_STOP_KEY, stop.getId());
				}

				trackablesArray.put(jsonTrackable);
			}

			jsonObject.put(Trackable.TRACKABLES_KEY, trackablesArray);

			return jsonObject.getString(Trackable.TRACKABLES_KEY);
		} catch (JSONException e) {
			return null;
		}
	}

	public static String getJSONForTrackablesStopRemoval(
			ArrayList<Trackable> trackables) {
		try {
			JSONObject jsonObject = new JSONObject();
			JSONArray trackablesArray = new JSONArray();

			for (int i = 0; i < trackables.size(); i++) {
				Trackable trackable = trackables.get(i);
				JSONObject jsonTrackable = new JSONObject();

				jsonTrackable.put(Trackable.ID_KEY, trackable.getId());

				trackablesArray.put(jsonTrackable);
			}

			jsonObject.put(Trackable.TRACKABLES_KEY, trackablesArray);

			return jsonObject.getString(Trackable.TRACKABLES_KEY);
		} catch (JSONException e) {
			return null;
		}
	}

	public static String getJSONForStopChange( String stopId, String footprintId, String type, String latitude, String longitude, String order, ArrayList<Trackable> trackables )
	{
		try
		{
			JSONArray jsonArray = new JSONArray( );
			JSONObject stopChange = new JSONObject( );
			if( stopId != null )
			{
				stopChange.put( ServiceKeys.FK_STOP_KEY, stopId );
			}
			stopChange.put( ServiceKeys.FK_FOOTPRINT_KEY, footprintId );
			stopChange.put( ServiceKeys.TYPE_KEY, type );
			stopChange.put( ServiceKeys.LATITUDE_KEY, latitude );
			stopChange.put( ServiceKeys.LONGITUDE_KEY, longitude );
			stopChange.put( ServiceKeys.POSITION_KEY, order );

			if( trackables != null && !trackables.isEmpty( ) )
			{
				JSONArray jsonTrackables = new JSONArray( );

				for( Trackable trackable : trackables )
				{
					JSONObject jsonTrackable = new JSONObject( );
					jsonTrackable.put( ServiceKeys.FK_TRACKABLE_KEY, trackable.getId( ) );

					jsonTrackables.put( jsonTrackable );
				}

				stopChange.put( Trackable.TRACKABLES_KEY, jsonTrackables );
			}

			jsonArray.put( stopChange );

			return jsonArray.toString( );
		}
		catch( JSONException e )
		{
			return null;
		}
	}

	public static String getJSONForTrackablesStopChange( String stopId, String footprintId, String type, ArrayList<Trackable> trackables )
	{
		try
		{
			if( trackables != null && !trackables.isEmpty( ) )
			{
				JSONArray jsonArray = new JSONArray( );

				for( Trackable trackable : trackables )
				{
					JSONObject jsonTrackableChanges = new JSONObject( );
					jsonTrackableChanges.put( ServiceKeys.FK_STOP_KEY, stopId );
					jsonTrackableChanges.put( ServiceKeys.FK_TRACKABLE_KEY, trackable.getId( ) );
					jsonTrackableChanges.put( ServiceKeys.FK_FOOTPRINT_KEY, footprintId );
					jsonTrackableChanges.put( ServiceKeys.TYPE_KEY, type );

					jsonArray.put( jsonTrackableChanges );
				}

				return jsonArray.toString( );
			}
			else
			{
				return null;
			}
		}
		catch( JSONException e )
		{
			return null;
		}
	}


}
