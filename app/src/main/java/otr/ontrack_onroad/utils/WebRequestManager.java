package otr.ontrack_onroad.utils;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import otr.ontrack_onroad.parameters.GeneralMessages;
import android.util.Log;

public class WebRequestManager

{
   public static final String URL_BASE = "http://dashboard.ontrackschool.co/production/";
   public static final String URL_IMAGE_BASE = "http://dashboard.ontrackschool.co/production";
//	public static final String URL_BASE = "http://development.ontrackschool.co/production/";
//    public static final String URL_IMAGE_BASE = "http://development.ontrackschool.co/production";

    public final static String USER_AGENT = "Android";

    public static String executeTaskGet( String... args ) throws IOException
    {
        String params = "";

        for( int i = 1; i < args.length; i += 2 )
        {
            if( i < args.length - 2 )
            {
                try
                {
                    params += URLEncoder.encode( args[ i ], "UTF-8" ) + "=" + URLEncoder.encode( args[ i + 1 ], "UTF-8" ) + "&";
                }
                catch( UnsupportedEncodingException e )
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace( );
                }
            }
            else
            {
                try
                {
                    params += URLEncoder.encode( args[ i ], "UTF-8" ) + "=" + URLEncoder.encode( args[ i + 1 ], "UTF-8" );
                }
                catch( UnsupportedEncodingException e )
                {
                    e.printStackTrace( );
                }
            }
        }

        URL url = new URL( URL_BASE + args[ 0 ] + "?" + params );
        HttpURLConnection urlConnection = ( HttpURLConnection )url.openConnection( );
        Log.d( "Solicitud http: ", URL_BASE + args[ 0 ] + "?" + params );

        urlConnection.setRequestProperty("User-Agent", USER_AGENT);

        int responseCode = urlConnection.getResponseCode( );
        String responseMessage = null;

        try {

            if (responseCode == HttpURLConnection.HTTP_OK) {

                BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                String inputLine;
                StringBuffer sbResponse = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    sbResponse.append(inputLine);
                }

                in.close();
                responseMessage = sbResponse.toString();
                return responseMessage;
            }else{
                throw new IOException(GeneralMessages.HTTP_CONNECTION_ERROR);
            }
        }catch (Exception e){
            throw new IOException(e);
        }finally {
            urlConnection.disconnect();
        }
    }

    public static String executeTask( String... args ) throws  IOException
    {
        String params = "";

        for( int i = 1; i < args.length; i += 2 )
        {
            if( i < args.length - 2 )
            {
                try
                {
                    params += URLEncoder.encode( args[ i ], "UTF-8" ) + "=" + URLEncoder.encode( args[ i + 1 ], "UTF-8" ) + "&";
                }
                catch( UnsupportedEncodingException e )
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace( );
                }
            }
            else
            {
                try
                {
                    params += URLEncoder.encode( args[ i ], "UTF-8" ) + "=" + URLEncoder.encode( args[ i + 1 ], "UTF-8" );
                }
                catch( UnsupportedEncodingException e )
                {
                    e.printStackTrace( );
                }
            }
        }
//        Log.d("params",params);

        URL url = new URL( URL_BASE + args[ 0 ] );
        HttpURLConnection urlConnection = ( HttpURLConnection )url.openConnection( );
//        Log.d( "Solicitud http: ", URL_BASE + args[ 0 ] + "?" + params );

        urlConnection.setRequestMethod("POST");
        urlConnection.setRequestProperty("User-Agent", USER_AGENT);

        urlConnection.setDoOutput(true);
        DataOutputStream dataOutputStream = new DataOutputStream( urlConnection.getOutputStream( ) );
        dataOutputStream.writeBytes(params);
        dataOutputStream.flush();
        dataOutputStream.close( );

        int responseCode = urlConnection.getResponseCode( );

        String responseMessage = null;
        try {

            if (responseCode == HttpURLConnection.HTTP_OK) {

                BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                String inputLine;
                StringBuffer sbResponse = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    sbResponse.append(inputLine);
                }

                in.close();
                responseMessage = sbResponse.toString();
                return responseMessage;
            }else{
                throw new IOException(GeneralMessages.HTTP_CONNECTION_ERROR);
            }
        }catch (Exception e){
            throw new IOException(e);
        }finally {
            urlConnection.disconnect();
        }
    }

    public static String executeTaskGetWithoutFormatting( String... args ) throws IOException
    {

        URL url = new URL( args[ 0 ] );
        HttpURLConnection urlConnection = ( HttpURLConnection )url.openConnection( );
//        Log.d( "Solicitud http: ",args[ 0 ] );

        urlConnection.setRequestProperty( "User-Agent", USER_AGENT );

        int responseCode = urlConnection.getResponseCode( );
        String responseMessage = null;

        try {

            if (responseCode == HttpURLConnection.HTTP_OK) {

                BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                String inputLine;
                StringBuffer sbResponse = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    sbResponse.append(inputLine);
                }

                in.close();
                responseMessage = sbResponse.toString();
                return responseMessage;
            }else{
                throw new IOException(GeneralMessages.HTTP_CONNECTION_ERROR);
            }
        }catch (Exception e){
            throw new IOException(e);
        }finally {
            urlConnection.disconnect();
        }
    }
}
