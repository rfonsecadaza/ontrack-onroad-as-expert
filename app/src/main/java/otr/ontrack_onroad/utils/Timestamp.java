package otr.ontrack_onroad.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.util.Log;

public class Timestamp
{
    public static String completeTimestamp( )
    {
        // Calendar c = Calendar.getInstance();
        // int seconds = c.get(Calendar.SECOND);
        // int minutes = c.get(Calendar.MINUTE);
        // int hours = c.get(Calendar.HOUR_OF_DAY);
        // Date date = c.getTime();
        // int year = date.getYear() + 1900;
        // int month = date.getMonth() + 1;
        // int day = date.getDate();
        // return "" + year + "-" + (month<10?"0"+month:month) + "-" + (day<10?"0"+day:day) + " "
        // + hours + ":" + (minutes<10?"0"+minutes:minutes) + ":" + (seconds<10?"0"+seconds:seconds);

        Calendar calendar = Calendar.getInstance( );
        SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );
        return sdf.format( calendar.getTime( ) );
    }
    
    public static String completeTimestampForDatabase( )
    {
        Calendar calendar = Calendar.getInstance( );
        SimpleDateFormat sdf = new SimpleDateFormat( "yyyyMMddKKmmssSSS" );
        return sdf.format( calendar.getTime( ) );
    }

    public static String partialTimestamp( )
    {
        // Calendar c = Calendar.getInstance();
        // int seconds = c.get(Calendar.SECOND);
        // int minutes = c.get(Calendar.MINUTE);
        // int hours = c.get(Calendar.HOUR_OF_DAY);
        // return "" + hours + ":" + (minutes<10?"0"+minutes:minutes) + ":" + (seconds<10?"0"+seconds:seconds);

        Calendar calendar = Calendar.getInstance( );
        SimpleDateFormat sdf = new SimpleDateFormat( "HH:mm:ss" );
        return sdf.format( calendar.getTime( ) );
    }

    public static final double getSecondsFromHHMMSS( String hhmmss ) throws Exception
    {
        String[] timeSplit = hhmmss.split( ":" );
        int hours = Integer.parseInt( timeSplit[ 0 ] );
        int minutes = Integer.parseInt( timeSplit[ 1 ] );
        int seconds = Integer.parseInt( timeSplit[ 2 ] );
        return hours * 3600 + minutes * 60 + seconds;
    }

    public static final String getHHMMSSFromSeconds( double seconds )
    {
        //        int hr = ( int )seconds / 3600;
        //        int rem = ( int )seconds % 3600;
        //        int mn = rem / 60;
        //        int sec = rem % 60;
        //        String hrStr = ( hr < 10 ? "0" : "" ) + hr;
        //        String mnStr = ( mn < 10 ? "0" : "" ) + mn;
        //        String secStr = ( sec < 10 ? "0" : "" ) + sec;
        //        return hrStr + ":" + mnStr + ":" + secStr;

        Calendar calendar = Calendar.getInstance( );
        SimpleDateFormat sdf = new SimpleDateFormat( "KK:mm:ss" );
        calendar.set( Calendar.HOUR, 0 );
        calendar.set( Calendar.MINUTE, 0 );
        calendar.set( Calendar.SECOND, 0 );
        calendar.add( Calendar.SECOND, ( int )seconds );
        return sdf.format( calendar.getTime( ) );
    }

    public static final double getCurrentTimeInSeconds( ) throws Exception
    {
        return getSecondsFromHHMMSS( partialTimestamp( ) );
    }
    
    public static final String convertToAmPm(String timeInMilitary){
    	 String[] timeSplit = timeInMilitary.split( ":" );
         int hours = Integer.parseInt( timeSplit[ 0 ] );
         int minutes = Integer.parseInt( timeSplit[ 1 ] );
         String meridian = hours>=12?"PM":"AM";
         if(hours==0){
        	 hours=12;
         }else if(hours>12){
        	 hours -=12;
         }
         return "" + hours + ":"
 				+ (minutes < 10 ? "0" + minutes : minutes) + meridian;
    }

}
