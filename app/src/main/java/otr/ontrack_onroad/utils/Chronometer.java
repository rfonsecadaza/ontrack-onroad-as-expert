package otr.ontrack_onroad.utils;

public class Chronometer
{
    private long begin, end;
    private boolean began, stopped;

    public Chronometer ()
    {
        began = false;
        stopped = true;
    }

    public void start()
    {
        stopped = false;
        began = true;
        begin = System.currentTimeMillis();
    }

    public void stop()
    {
        stopped = true;
        end = System.currentTimeMillis();
    }

    public long getTime()
    {
        if (!stopped) end = System.currentTimeMillis();

        return end - begin;
    }

    public long getMilliseconds()
    {
        if (began)
        {
            if (!stopped) end = System.currentTimeMillis();

            return end - begin;
        }
        else
        {
            return 0;
        }
    }

    public double getSeconds()
    {
        if (began)
        {
            if (!stopped) end = System.currentTimeMillis();

            return (end - begin) / 1000.0;
        }
        else
        {
            return 0;
        }
    }
    
    

    public double getMinutes()
    {
        if (began)
        {
            if (!stopped) end = System.currentTimeMillis();

            return (end - begin) / 60000.0;
        }
        else
        {
            return 0;
        }
    }

    public double getHours()
    {
        if (began)
        {
            if (!stopped) end = System.currentTimeMillis();

            return (end - begin) / 3600000.0;
        }
        else
        {
            return 0;
        }
    }
}
