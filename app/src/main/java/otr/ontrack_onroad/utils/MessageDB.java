package otr.ontrack_onroad.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class MessageDB extends SQLiteOpenHelper {

	public static final String DATABASE_NAME = "OnTrack.db";
	public static final String MESSAGES_TABLE_NAME = "messages";
	public static final String MESSAGES_COLUMN_ID = "id";
	public static final String MESSAGES_COLUMN_TEXT = "message";
	public static final String MESSAGES_COLUMN_DATE = "date";
	public static final String MESSAGES_COLUMN_STATE = "state";

	public MessageDB(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("create table " + MESSAGES_TABLE_NAME + " ("
				+ MESSAGES_COLUMN_ID + " integer primary key, "
				+ MESSAGES_COLUMN_TEXT + " text, " + MESSAGES_COLUMN_DATE
				+ " text, " + MESSAGES_COLUMN_STATE + " integer)");

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + DATABASE_NAME);
		onCreate(db);
	}

	public int insertMessage(String messageText, String date) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put(MESSAGES_COLUMN_TEXT, messageText);
		contentValues.put(MESSAGES_COLUMN_DATE,date);
		contentValues.put(MESSAGES_COLUMN_STATE, 0);
		return (int)db.insert(MESSAGES_TABLE_NAME, null, contentValues);
	}
	
	public void updateState(int id){

	      SQLiteDatabase db = this.getWritableDatabase();
	      ContentValues contentValues = new ContentValues();
	      contentValues.put(MESSAGES_COLUMN_STATE, 1);
	      db.update(MESSAGES_TABLE_NAME, contentValues, "id = ? ", new String[] { Integer.toString(id) } );
	      
	}

}
