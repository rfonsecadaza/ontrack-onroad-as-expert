package otr.ontrack_onroad.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.Region;
import android.util.AttributeSet;
import android.view.View;

public class ClippingView extends View {
	
	public static final float WHITE_STRIP_HEIGHT=80.0f;
	public ClippingView(Context context) {
		super(context);

	}

	public ClippingView(Context context, AttributeSet attrs) {
		super(context, attrs);
		
	}

	public ClippingView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		canvas.drawColor(0x00000000);
		Path path = new Path();
		path.addCircle(0.15f*getWidth(), getHeight() / 2, 70, Direction.CW);

		// fill with semi-transparent red
		canvas.clipPath(path, Region.Op.DIFFERENCE);
		canvas.drawColor(0xaa000000);
		Paint paint = new Paint();
		paint.setColor(0xff000000);
		canvas.drawRect(0.0f, 0.0f,
				(float) getWidth()*0.3f, (float) getHeight(), paint);

	}

	@Override
	protected void dispatchDraw(Canvas canvas) {
		// restore full canvas clip for any subsequent operations
		// canvas.clipRect(new Rect(0, 0, canvas.getWidth(),
		// canvas.getHeight()),
		// Region.Op.REPLACE);
		

	}
}