package otr.ontrack_onroad.tasks;

import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.managers.OnTrackManagerAsyncTasks;
import otr.ontrack_onroad.models.Point;
import otr.ontrack_onroad.models.Route;
import otr.ontrack_onroad.models.Stop;
import otr.ontrack_onroad.parameters.GeneralMessages;
import otr.ontrack_onroad.parameters.ServiceKeys;
import otr.ontrack_onroad.parameters.WebServices;
import otr.ontrack_onroad.utils.WebRequestManager;
import otr.ontrack_onroad_debug.activities.R;

import com.google.android.gms.maps.model.LatLng;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;

public class PrepareStopsForStopUpdateLocationTask extends AsyncTask<Void, Void, String>
{
    private Activity activity;

    private ProgressDialog progressDialog;

    private Stop stop;

    private Stop nextStop;

    private double latitude;

    private double longitude;

    private Route route;

    private boolean forward;

    private ArrayList<Stop> modifiedStops;

    private LatLng origin;

    private LatLng destination;

    private LatLng waypoint;
    
    private Stop firstStop;
    
    private Stop secondStop;

    public PrepareStopsForStopUpdateLocationTask( Activity activity, Stop stop )
    {
        this.activity = activity;
        progressDialog = new ProgressDialog( activity );
        this.latitude = OnTrackManager.getInstance( ).getCurrentLocation( ).getLatitude( );
        this.longitude = OnTrackManager.getInstance( ).getCurrentLocation( ).getLongitude( );
        route = OnTrackManager.getInstance( ).getUser( ).getRoute( );
        this.stop = stop;
        nextStop = OnTrackManager.getInstance( ).getNextStop( );
        forward = stop.getOrder( ) >= nextStop.getOrder( );
        modifiedStops = new ArrayList<Stop>( );
        modifyAffectedStops( );
    }

    @Override
    protected void onPreExecute( )
    {
        progressDialog.setTitle( activity.getResources( ).getString( R.string.loading ) );
        progressDialog.setMessage( activity.getResources( ).getString( R.string.please_wait ) );
        progressDialog.show( );
    }

    @Override
    protected String doInBackground( Void... params )
    {
        String response = "";
        try
        {
            response = WebRequestManager.executeTaskGetWithoutFormatting( getGoogleDirectionsQuery( ) );
        }
        catch( IOException e )
        {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(activity, GeneralMessages.HTTP_NOT_FOUND_ERROR, Toast.LENGTH_SHORT).show();
                }
            });
        }

        return response;
    }

    @Override
    protected void onPostExecute( String result )
    {
        progressDialog.dismiss( );
        decodeGoogleDirections( result );
        UpdateStopLocationTask updateStopLocationTask = new UpdateStopLocationTask( activity, modifiedStops );
        updateStopLocationTask.execute( WebServices.URL_UPDATE_STOP_LOCATION, ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance( ).getToken( ), Stop.STOPS_KEY, getStopsJSON( ) );
    }

    private ArrayList<Stop> getAffectedStops( )
    {
        ArrayList<Stop> affectedStops = new ArrayList<Stop>( );
        affectedStops.add( stop );

        if( forward )
        {
            int end = stop.getOrder( ) < route.getStops( ).size( ) - 1 ? stop.getOrder( ) + 1 : stop.getOrder( );

            for( int i = nextStop.getOrder( ); i <= end; i++ )
            {
                if( i != stop.getOrder( ) )
                {
                    affectedStops.add( route.getStops( ).get( i ) );
                }
            }
        }
        else
        {
            for( int i = stop.getOrder( ) + 1; i <= nextStop.getOrder( ); i++ )
            {
                if( i != stop.getOrder( ) )
                {
                    affectedStops.add( route.getStops( ).get( i ) );
                }
            }
        }

        return affectedStops;
    }

    private void modifyAffectedStops( )
    {
        for( Stop stop : getAffectedStops( ) )
        {
            Stop newStop = new Stop( );
            newStop.setId( stop.getId( ) );
            newStop.setOrder( -1 );

            if( forward )
            {
                if( stop.getOrder( ) == this.stop.getOrder( ) )
                {
                    newStop.setLatitude( latitude );
                    newStop.setLongitude( longitude );

                    if( stop.getOrder( ) != nextStop.getOrder( ) )
                    {
                        newStop.setOrder( nextStop.getOrder( ) );
                    }

                    origin = new LatLng( route.getStops( ).get( nextStop.getOrder( ) - 1 ).getLatitude( ), route.getStops( ).get( nextStop.getOrder( ) - 1 ).getLongitude( ) );
                    waypoint = new LatLng( latitude, longitude );
                    firstStop = newStop;
                }
                else if( stop.getOrder( ) == this.stop.getOrder( ) + 1 )
                {
                    if( stop.getOrder( ) != nextStop.getOrder( ) + 1 )
                    {
                        ArrayList<Point> steps = new ArrayList<Point>( );
                        steps.addAll( this.stop.getSteps( ) );
                        steps.addAll( stop.getSteps( ) );
                        newStop.setSteps( steps );
                    }
                    else
                    {
                        destination = new LatLng( route.getStops( ).get( nextStop.getOrder( ) + 1 ).getLatitude( ), route.getStops( ).get( nextStop.getOrder( ) + 1 ).getLongitude( ) );
                        secondStop = newStop;
                    }
                }
                else if( stop.getOrder( ) == nextStop.getOrder( ) && this.stop.getOrder( ) != nextStop.getOrder( ) )
                {
                    newStop.setOrder( stop.getOrder( ) + 1 );
                    destination = new LatLng( nextStop.getLatitude( ), nextStop.getLongitude( ) );
                    secondStop = newStop;
                }
                else
                {
                    newStop.setOrder( stop.getOrder( ) + 1 );
                }
            }
            else
            {
                if( stop.getOrder( ) == this.stop.getOrder( ) )
                {
                    newStop.setLatitude( latitude );
                    newStop.setLongitude( longitude );

                    if( stop.getOrder( ) != route.getStops( ).get( nextStop.getOrder( ) - 1 ).getOrder( ) )
                    {
                        newStop.setOrder( nextStop.getOrder( ) - 1 );
                    }

                    if( stop.getOrder( ) == 0 && nextStop.getOrder( ) == 1 )
                    {
                        origin = new LatLng( latitude, longitude );
                    }
                    else
                    {
                        if( stop.getOrder( ) + 1 == nextStop.getOrder( ) )
                        {
                            origin = new LatLng( route.getStops( ).get( stop.getOrder( ) - 1 ).getLatitude( ), route.getStops( ).get( stop.getOrder( ) - 1 ).getLongitude( ) );
                        }
                        else
                        {
                            origin = new LatLng( route.getStops( ).get( nextStop.getOrder( ) - 1 ).getLatitude( ), route.getStops( ).get( nextStop.getOrder( ) - 1 ).getLongitude( ) );
                        }

                        waypoint = new LatLng( latitude, longitude );
                    }
                    
                    firstStop = newStop;
                }
                else if( stop.getOrder( ) == nextStop.getOrder( ) )
                {
                    destination = new LatLng( nextStop.getLatitude( ), nextStop.getLongitude( ) );
                    secondStop = newStop;
                }
                else if( stop.getOrder( ) == this.stop.getOrder( ) + 1 )
                {
                    if( stop.getOrder( ) != nextStop.getOrder( ) )
                    {
                        newStop.setOrder( stop.getOrder( ) - 1 );
                    }

                    ArrayList<Point> steps = new ArrayList<Point>( );

                    if( this.stop.getOrder( ) != 0 )
                    {
                        steps.addAll(this.stop.getSteps( ) );
                        steps.addAll( route.getStops( ).get( this.stop.getOrder( ) + 1 ).getSteps( ) );
                    }

                    newStop.setSteps( steps );
                }
                else
                {
                    newStop.setOrder( stop.getOrder( ) - 1 );
                }
            }

            modifiedStops.add( newStop );
        }
    }

    private String getGoogleDirectionsQuery( )
    {
        String query = "";

        query += OnTrackManagerAsyncTasks.URL_GOOGLE_DIRECTIONS_BASE;

        query += "origin=" + origin.latitude + "," + origin.longitude;

        query += "&destination=" + destination.latitude + "," + destination.longitude;

        if( waypoint != null )
        {
            query += "&waypoints=" + waypoint.latitude + "," + waypoint.longitude;
        }

        query += "&sensor=true";

        return query;
    }

    private void decodeGoogleDirections( String jsonString )
    {
        JSONObject jsonObject;
        try
        {
            jsonObject = new JSONObject( jsonString );
            JSONArray routesArray = jsonObject.getJSONArray( ServiceKeys.GOOGLE_ROUTES_KEY );
            JSONObject routeObject = routesArray.getJSONObject( 0 );
            JSONArray legsArray = routeObject.getJSONArray( ServiceKeys.GOOGLE_LEGS_KEY );
            for( int i = 0; i < legsArray.length( ); i++ )
            {
                JSONObject legObject = legsArray.getJSONObject( i );
                JSONArray stepsArray = legObject.getJSONArray( ServiceKeys.GOOGLE_STEPS_KEY );
                for( int j = 0; j < stepsArray.length( ); j++ )
                {
                    JSONObject stepObject = stepsArray.getJSONObject( j );
                    JSONObject polylineObject = stepObject.getJSONObject( ServiceKeys.GOOGLE_POLYLINE_KEY );
                    String encodedPoints = polylineObject.getString( ServiceKeys.GOOGLE_POINTS_KEY );
                    decodeSteps( encodedPoints, i );
                }
                
                JSONObject durationJSON = legObject.getJSONObject( ServiceKeys.GOOGLE_DURATION_KEY );
                int duration = durationJSON.getInt( ServiceKeys.GOOGLE_VALUE_KEY );
                
                if( i == 0 )
                {
                    firstStop.setEstimatedTime( duration );
                }
                else
                {
                    secondStop.setEstimatedTime( duration );
                }
            }
        }
        catch( JSONException e )
        {
            e.printStackTrace( );
        }
    }
    
    private void decodeSteps( String encodedPoints, int stopOrder )
    {
        int index = 0;
        int order = 1;
        int lat = 0, lng = 0;
        ArrayList<Point> tmpPoints = new ArrayList<Point>( );

        while( index < encodedPoints.length( ) )
        {
            int b, shift = 0, result = 0;

            do
            {
                b = encodedPoints.charAt( index++ ) - 63;
                result |= ( b & 0x1f ) << shift;
                shift += 5;
            } while( b >= 0x20 );

            int dlat = ( ( result & 1 ) != 0 ? ~ ( result >> 1 ) : ( result >> 1 ) );
            lat += dlat;
            shift = 0;
            result = 0;

            do
            {
                b = encodedPoints.charAt( index++ ) - 63;
                result |= ( b & 0x1f ) << shift;
                shift += 5;
            } while( b >= 0x20 );

            int dlng = ( ( result & 1 ) != 0 ? ~ ( result >> 1 ) : ( result >> 1 ) );
            lng += dlng;

            tmpPoints.add( new Point( ( double )lng / 100000, ( double ) ( lat ) / 100000, 0, order ) );
            order++;

        }
        
        if( stopOrder ==0 )
        {
            if( firstStop.getSteps( ) == null )
            {
                firstStop.setSteps( new ArrayList<Point>( ) );
            }
            
            firstStop.addSteps( tmpPoints );
        }
        else
        {
            if( secondStop.getSteps( ) == null )
            {
                secondStop.setSteps( new ArrayList<Point>( ) );
            }
            
            secondStop.addSteps( tmpPoints );
        }
    }
    
    private String getStopsJSON( )
    {
        try
        {
            JSONObject objectJSON = new JSONObject( );
            JSONArray stopsArray = new JSONArray( );
            
            for( Stop stop : modifiedStops )
            {
                JSONObject stopJSON = new JSONObject( );
                
                stopJSON.put( Stop.ID_KEY, stop.getId( ) );
                
                if( stop.getLatitude( ) != 0.0 )
                {
                    stopJSON.put( Stop.LATITUDE_KEY, stop.getLatitude( ) );
                }
                if( stop.getLongitude( ) != 0.0 )
                {
                    stopJSON.put( Stop.LONGITUDE_KEY, stop.getLongitude( ) );
                }
                if( stop.getOrder( ) != -1 )
                {
                    stopJSON.put( Stop.POSITION_KEY, stop.getOrder( ) );
                }
                if( stop.getEstimatedTime( ) != 0 )
                {
                    stopJSON.put( Stop.ESTIMATED_TIME_KEY, stop.getEstimatedTime( ) );
                }
                if( stop.getSteps( ) != null )
                {
                    JSONArray stepsArray = new JSONArray( );
                    
                    for( Point step : stop.getSteps( ) )
                    {
                        JSONObject stepJSON = new JSONObject( );
                        stepJSON.put( Point.LATITUDE_KEY, step.getLatitude( ) );
                        stepJSON.put( Point.LONGITUDE_KEY, step.getLongitude( ) );
                        stepJSON.put( Point.POSITION_KEY, step.getOrder( ) );
                        stepsArray.put( stepJSON );
                    }
                    
                    stopJSON.put( Point.STEPS_KEY, stepsArray );
                }
                
                stopsArray.put( stopJSON );
            }
            
            objectJSON.put( Stop.STOPS_KEY, stopsArray );
            
            return objectJSON.getString( Stop.STOPS_KEY );
        }
        catch( JSONException e )
        {
            return null;
        }
    }
}
