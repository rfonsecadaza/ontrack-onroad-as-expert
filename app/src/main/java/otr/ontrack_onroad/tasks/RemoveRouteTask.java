package otr.ontrack_onroad.tasks;

import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;

import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.models.Route;
import otr.ontrack_onroad.parameters.GeneralMessages;
import otr.ontrack_onroad.parameters.ServiceKeys;
import otr.ontrack_onroad.utils.WebRequestManager;
import otr.ontrack_onroad_debug.activities.R;
import otr.ontrack_onroad_debug.activities.RouteList;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;

public class RemoveRouteTask extends AsyncTask<String, Void, String>
{
    private Activity activity;
    
    private ProgressDialog progressDialog;
    
    private String routeID;
    
    public RemoveRouteTask( Activity activity, String routeID )
    {
        this.activity = activity;
        this.routeID = routeID;
        progressDialog = new ProgressDialog( activity );
    }
    
    @Override
    protected void onPreExecute( )
    {
        progressDialog.setTitle( activity.getResources( ).getString( R.string.loading ) );
        progressDialog.setMessage( activity.getResources( ).getString( R.string.please_wait ) );
        progressDialog.show( );
    }
    
    @Override
    protected String doInBackground( String... params )
    {
        String response = "";
        try
        {
            response = WebRequestManager.executeTask( params );
        }
        catch( IOException e )
        {
            Toast.makeText( activity, GeneralMessages.HTTP_NOT_FOUND_ERROR, Toast.LENGTH_SHORT ).show( );
        }
        
        return response;
    }

    @Override
    protected void onPostExecute( String result )
    {
        try
        {
            JSONObject jsonObject = new JSONObject( result );
            
            int status = jsonObject.getInt( ServiceKeys.STATUS_KEY );
            
            if( status == 1 )
            {
                for( int i = 0; i < OnTrackManager.getInstance( ).getRoutes( ).size( ); i++ )
                {
                    Route route = OnTrackManager.getInstance( ).getRoutes( ).get( i );
                    
                    if( route.getId( ).equals( routeID ) )
                    {
                        OnTrackManager.getInstance( ).getRoutes( ).remove( i );
                        break;
                    }
                }
                ( ( RouteList )activity ).getAdapter( ).notifyDataSetChanged( );
                //GetRoutesListTask getRoutesListTask = new GetRoutesListTask( activity, progressDialog );
                //getRoutesListTask.execute( WebServices.URL_GET_ROUTES_LIST, ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance( ).getToken( ) );
            }
            else
            {
                String error = jsonObject.getString( ServiceKeys.ERROR_KEY );
                Toast.makeText( activity, error, Toast.LENGTH_SHORT ).show( );
            }
        }
        catch( JSONException e )
        {
            Toast.makeText( activity, e.getMessage( ), Toast.LENGTH_SHORT ).show( );
        }
        finally
        {
            progressDialog.dismiss( );
        }
    }
}
