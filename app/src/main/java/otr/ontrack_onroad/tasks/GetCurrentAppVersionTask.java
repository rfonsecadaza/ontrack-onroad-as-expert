package otr.ontrack_onroad.tasks;

import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;

import otr.ontrack_onroad.parameters.ServiceKeys;
import otr.ontrack_onroad.utils.WebRequestManager;
import otr.ontrack_onroad_debug.activities.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.widget.Toast;

public class GetCurrentAppVersionTask extends AsyncTask<String, Void, String> implements OnClickListener
{
    private Activity activity;
    
    public GetCurrentAppVersionTask( Activity activity )
    {
        this.activity = activity;
    }
    
    @Override
    protected String doInBackground( String... params )
    {
        String response = "";
        try
        {
            response = WebRequestManager.executeTask( params );
        }
        catch( IOException e )
        {
            e.printStackTrace();
        }
        
        return response;
    }

    @Override
    protected void onPostExecute( String result )
    {
        try
        {
            JSONObject jsonObject = new JSONObject( result );
            
            int status = jsonObject.getInt( ServiceKeys.STATUS_KEY );
            
            if( status == 1 )
            {
                String currentAppVersion = jsonObject.getString( ServiceKeys.VERSION_KEY );               
                String appVersion = "";

                try
                {
                    appVersion = activity.getPackageManager( ).getPackageInfo( activity.getPackageName( ), 0 ).versionName;
                }
                catch( NameNotFoundException e )
                {
                    appVersion = "N/A";
                }
                
                int intAppVersion = Integer.parseInt( appVersion.split( "\\." )[ 0 ] + appVersion.split( "\\." )[ 1 ] + appVersion.split( "\\." )[ 2 ] );
                int intCurrentAppVersion = Integer.parseInt( currentAppVersion.split( "\\." )[ 0 ] + currentAppVersion.split( "\\." )[ 1 ] + currentAppVersion.split( "\\." )[ 2 ] );

                if( intAppVersion < intCurrentAppVersion )
                {
                    AlertDialog.Builder builder = new AlertDialog.Builder( activity );
                    builder.setTitle(activity.getString(R.string.route_list_version_dialog_title));
                    builder.setMessage(activity.getString(R.string.route_list_version_dialog_message));
                    builder.setPositiveButton(activity.getString(R.string.route_list_version_dialog_update), this);
//                    builder.setCancelable( false );
                    AlertDialog alert = builder.create( );
                    alert.show( );
                }
            }
            else
            {
                final String error = jsonObject.getString( ServiceKeys.ERROR_KEY );
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(activity, error, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
        catch( JSONException e )
        {
            final String error = e.getMessage();
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(activity, error, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
    
    @Override
    public void onClick( DialogInterface dialog, int which )
    {
        if( which == -1 )
        {
//            try
//            {
//                activity.startActivity( new Intent( Intent.ACTION_VIEW, Uri.parse( "market://details?id=" + Parameters.APP_PACKAGE_NAME ) ) );
//            }
//            catch( android.content.ActivityNotFoundException anfe )
//            {
//                activity.startActivity( new Intent( Intent.ACTION_VIEW, Uri.parse( "https://play.google.com/store/apps/details?id=" + Parameters.APP_PACKAGE_NAME ) ) );
//            }
//
//            System.exit( 0 );
            dialog.dismiss();
        }
    }
}
