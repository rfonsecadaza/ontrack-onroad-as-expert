package otr.ontrack_onroad.tasks;

import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.managers.OnTrackManagerAsyncTasks;
import otr.ontrack_onroad.models.Point;
import otr.ontrack_onroad.models.Route;
import otr.ontrack_onroad.models.Stop;
import otr.ontrack_onroad.models.Trackable;
import otr.ontrack_onroad.parameters.GeneralMessages;
import otr.ontrack_onroad.parameters.ServiceKeys;
import otr.ontrack_onroad.parameters.WebServices;
import otr.ontrack_onroad.utils.WebRequestManager;
import otr.ontrack_onroad_debug.activities.R;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

public class PrepareStopsForStopAdditionTask extends AsyncTask<Void, Void, String>
{
    private Activity activity;

    private ProgressDialog progressDialog;

    private double latitude;

    private double longitude;

    private Route route;
    
    private ArrayList<Trackable> trackables;
    
    private ArrayList<Stop> modifiedStops;
    
    private LatLng origin;

    private LatLng destination;

    private LatLng waypoint;
    
    private Stop firstStop;
    
    private Stop secondStop;

    public PrepareStopsForStopAdditionTask( Activity activity, double latitude, double longitude, ArrayList<Trackable> trackables )
    {
        this.activity = activity;
        progressDialog = new ProgressDialog( activity );
        this.latitude = latitude;
        this.longitude = longitude;
        route = OnTrackManager.getInstance( ).getUser( ).getRoute( );
        this.trackables = trackables;
        modifiedStops = new ArrayList<Stop>( );
        modifyAffectedStops( );
    }

    @Override
    protected void onPreExecute( )
    {
        progressDialog.setTitle( activity.getResources( ).getString( R.string.loading ) );
        progressDialog.setMessage( activity.getResources( ).getString( R.string.please_wait ) );
        progressDialog.show( );
    }

    @Override
    protected String doInBackground( Void... params )
    {
        String response = "";
        
        try
        {
            response = WebRequestManager.executeTaskGetWithoutFormatting( getGoogleDirectionsQuery( ) );
        }
        catch( IOException e )
        {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(activity, GeneralMessages.HTTP_NOT_FOUND_ERROR, Toast.LENGTH_SHORT).show();
                }
            });
        }

        return response;
    }

    @Override
    protected void onPostExecute( String result )
    {
        progressDialog.dismiss( );
        decodeGoogleDirections( result );
        AddNewStopTask addNewStopTask = new AddNewStopTask( activity, modifiedStops );
        addNewStopTask.execute( WebServices.URL_ADD_STOP, ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance( ).getToken( ), ServiceKeys.ID_ROUTE_KEY, route.getId( ), Stop.STOPS_KEY, getStopsJSON( ) );
    }
    
    private ArrayList<Stop> getAffectedStops( )
    {
        ArrayList<Stop> affectedStops = new ArrayList<Stop>( );
        boolean add = false;
        Stop nextStop = OnTrackManager.getInstance( ).getNextStop( );

        if( nextStop != null )
        {
            for( Stop stop : route.getStops( ) )
            {
                if( stop.getId( ) == nextStop.getId( ) )
                {
                    add = true;
                }
                
                if( add )
                {
                    affectedStops.add( stop );
                }
            }
        }

        return affectedStops;
    }
    
    private void modifyAffectedStops( )
    { 
        ArrayList<Stop> affectedStops = getAffectedStops( );
        Stop additionalStop = new Stop( );
        additionalStop.setLatitude( latitude );
        additionalStop.setLongitude( longitude );
        additionalStop.setTrackables( trackables );
        
        if( OnTrackManager.getInstance( ).getNextStop( ) != null )
        {
            additionalStop.setOrder( OnTrackManager.getInstance( ).getNextStop( ).getOrder( ) );
        }
        else
        {
            additionalStop.setOrder( route.getStops( ).size( ) );
        }
        
        if( !affectedStops.isEmpty( ) )
        {
            for( int i = 0; i < affectedStops.size( ); i++ )
            {
                Stop stop = affectedStops.get( i );
                Stop newStop = new Stop( );
                newStop.setId( stop.getId( ) );
                newStop.setOrder( stop.getOrder( ) + 1 );
                
                if( i == 0 )
                {
                    if( stop.getOrder( ) == 0 )
                    {
                        origin = new LatLng( latitude, longitude );
                        firstStop = newStop;
                    }
                    else
                    {
                        Stop previousStop = route.getStops( ).get( stop.getOrder( ) - 1 );
                        origin = new LatLng( previousStop.getLatitude( ), previousStop.getLongitude( ) );
                        waypoint = new LatLng( latitude, longitude );
                        firstStop = additionalStop;
                        secondStop = newStop;
                    }
                    
                    destination = new LatLng( stop.getLatitude( ), stop.getLongitude( ) );
                }
                
                modifiedStops.add( newStop );
            }
        }
        else
        {
            if( !route.getStops( ).isEmpty( ) )
            {
                Stop lastStop = route.getStops( ).get( route.getStops( ).size( ) - 1 );
                origin = new LatLng( lastStop.getLatitude( ), lastStop.getLongitude( ) );
                destination = new LatLng( latitude, longitude );
                firstStop = additionalStop;
            }
        }
        
        modifiedStops.add( 0, additionalStop );
    }

    private String getGoogleDirectionsQuery( )
    {
        String query = "";

        query += OnTrackManagerAsyncTasks.URL_GOOGLE_DIRECTIONS_BASE;

        query += "origin=" + origin.latitude + "," + origin.longitude;

        query += "&destination=" + destination.latitude + "," + destination.longitude;

        if( waypoint != null )
        {
            query += "&waypoints=" + waypoint.latitude + "," + waypoint.longitude;
        }

        query += "&sensor=true";

        return query;
    }

    private void decodeGoogleDirections( String jsonString )
    {
        JSONObject jsonObject;
        try
        {
            jsonObject = new JSONObject( jsonString );
            JSONArray routesArray = jsonObject.getJSONArray( ServiceKeys.GOOGLE_ROUTES_KEY );
            JSONObject routeObject = routesArray.getJSONObject( 0 );
            JSONArray legsArray = routeObject.getJSONArray( ServiceKeys.GOOGLE_LEGS_KEY );
            for( int i = 0; i < legsArray.length( ); i++ )
            {
                JSONObject legObject = legsArray.getJSONObject( i );
                JSONArray stepsArray = legObject.getJSONArray( ServiceKeys.GOOGLE_STEPS_KEY );
                for( int j = 0; j < stepsArray.length( ); j++ )
                {
                    JSONObject stepObject = stepsArray.getJSONObject( j );
                    JSONObject polylineObject = stepObject.getJSONObject( ServiceKeys.GOOGLE_POLYLINE_KEY );
                    String encodedPoints = polylineObject.getString( ServiceKeys.GOOGLE_POINTS_KEY );
                    decodeSteps( encodedPoints, i );
                }
                
                JSONObject durationJSON = legObject.getJSONObject( ServiceKeys.GOOGLE_DURATION_KEY );
                int duration = durationJSON.getInt( ServiceKeys.GOOGLE_VALUE_KEY );
                
                if( i == 0 )
                {
                    firstStop.setEstimatedTime( duration );
                }
                else
                {
                    secondStop.setEstimatedTime( duration );
                }
            }
        }
        catch( JSONException e )
        {
            e.printStackTrace( );
        }
    }
    
    private void decodeSteps( String encodedPoints, int stopOrder )
    {
        int index = 0;
        int order = 1;
        int lat = 0, lng = 0;
        ArrayList<Point> tmpPoints = new ArrayList<Point>( );

        while( index < encodedPoints.length( ) )
        {
            int b, shift = 0, result = 0;

            do
            {
                b = encodedPoints.charAt( index++ ) - 63;
                result |= ( b & 0x1f ) << shift;
                shift += 5;
            } while( b >= 0x20 );

            int dlat = ( ( result & 1 ) != 0 ? ~ ( result >> 1 ) : ( result >> 1 ) );
            lat += dlat;
            shift = 0;
            result = 0;

            do
            {
                b = encodedPoints.charAt( index++ ) - 63;
                result |= ( b & 0x1f ) << shift;
                shift += 5;
            } while( b >= 0x20 );

            int dlng = ( ( result & 1 ) != 0 ? ~ ( result >> 1 ) : ( result >> 1 ) );
            lng += dlng;

            tmpPoints.add( new Point( ( double )lng / 100000, ( double ) ( lat ) / 100000, 0, order ) );
            order++;

        }
        
        if( stopOrder ==0 )
        {
            if( firstStop.getSteps( ) == null )
            {
                firstStop.setSteps( new ArrayList<Point>( ) );
            }
            
            firstStop.addSteps( tmpPoints );
        }
        else
        {
            if( secondStop.getSteps( ) == null )
            {
                secondStop.setSteps( new ArrayList<Point>( ) );
            }
            
            secondStop.addSteps( tmpPoints );
        }
    }
    
    private String getStopsJSON( )
    {
        try
        {
            JSONObject objectJSON = new JSONObject( );
            JSONArray stopsArray = new JSONArray( );
            
            for( Stop stop : modifiedStops )
            {
                JSONObject stopJSON = new JSONObject( );
                stopJSON.put( Stop.POSITION_KEY, stop.getOrder( ) );
                
                if( stop.getId( ) != 0 )
                {
                    stopJSON.put( Stop.ID_KEY, stop.getId( ) );
                }
                if( stop.getLatitude( ) != 0.0 )
                {
                    stopJSON.put( Stop.LATITUDE_KEY, stop.getLatitude( ) );
                }
                if( stop.getLongitude( ) != 0.0 )
                {
                    stopJSON.put( Stop.LONGITUDE_KEY, stop.getLongitude( ) );
                }
                if( stop.getEstimatedTime( ) != 0 )
                {
                    stopJSON.put( Stop.ESTIMATED_TIME_KEY, stop.getEstimatedTime( ) );
                }
                if( stop.getSteps( ) != null )
                {
                    JSONArray stepsArray = new JSONArray( );
                    
                    for( Point step : stop.getSteps( ) )
                    {
                        JSONObject stepJSON = new JSONObject( );
                        stepJSON.put( Point.LATITUDE_KEY, step.getLatitude( ) );
                        stepJSON.put( Point.LONGITUDE_KEY, step.getLongitude( ) );
                        stepJSON.put( Point.POSITION_KEY, step.getOrder( ) );
                        stepsArray.put( stepJSON );
                    }
                    
                    stopJSON.put( Point.STEPS_KEY, stepsArray );
                }
                if( stop.getTrackables( ) != null )
                {
                    JSONArray trackablesArray = new JSONArray( );
                    
                    for( Trackable trackable : stop.getTrackables( ) )
                    {
                        JSONObject trackableJSON = new JSONObject( );
                        trackableJSON.put( Trackable.ID_KEY, trackable.getId( ) );
                        trackablesArray.put( trackableJSON );
                    }
                    
                    stopJSON.put( Trackable.TRACKABLES_KEY, trackablesArray );
                }
                
                stopsArray.put( stopJSON );
            }
            
            objectJSON.put( Stop.STOPS_KEY, stopsArray );
            
            return objectJSON.getString( Stop.STOPS_KEY );
        }
        catch( JSONException e )
        {
            return null;
        }
    }
}
