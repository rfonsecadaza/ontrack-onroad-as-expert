package otr.ontrack_onroad.tasks;

import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.models.Trackable;
import otr.ontrack_onroad.parameters.GeneralMessages;
import otr.ontrack_onroad.parameters.ServiceKeys;
import otr.ontrack_onroad.parameters.WebServices;
import otr.ontrack_onroad.utils.JSONManager;
import otr.ontrack_onroad.utils.WebRequestManager;
import otr.ontrack_onroad_debug.activities.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.AsyncTask;
import android.widget.Toast;

public class CheckTrackableRemovalFromStopTask extends AsyncTask<String, Void, String> implements OnClickListener
{
    private Activity activity;
    
    private ProgressDialog progressDialog;
    
    private int stopID;
    
    private Trackable trackable;
    
    public CheckTrackableRemovalFromStopTask( Activity activity, int stopID, Trackable trackable )
    {
        this.activity = activity;
        progressDialog = new ProgressDialog( activity );
        this.stopID = stopID;
        this.trackable = trackable;
    }
    
//    @Override
//    protected void onPreExecute( )
//    {
//        progressDialog.setTitle( activity.getResources( ).getString( R.string.loading ) );
//        progressDialog.setMessage( activity.getResources( ).getString( R.string.please_wait ) );
//        progressDialog.show( );
//    }
    
    @Override
    protected String doInBackground( String... params )
    {
        String response = "";
        try
        {
            response = WebRequestManager.executeTask( params );
        }
        catch( IOException e )
        {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(activity, GeneralMessages.HTTP_NOT_FOUND_ERROR, Toast.LENGTH_SHORT).show();
                }
            });

        }
        
        return response;
    }

    @Override
    protected void onPostExecute( String result )
    {
        try
        {
            JSONObject jsonObject = new JSONObject( result );
            
            int status = jsonObject.getInt( ServiceKeys.STATUS_KEY );
            
            if( status == 1 )
            {
                boolean remove = jsonObject.getBoolean( ServiceKeys.REMOVE_KEY );
                
                if( remove )
                {
                    AlertDialog.Builder builder = new AlertDialog.Builder( activity );
                    builder.setTitle( activity.getResources( ).getString( R.string.help_trackables_stop_remove_title ) );
                    builder.setMessage( activity.getResources( ).getString( R.string.help_trackables_stop_remove_message ) );
                    builder.setPositiveButton( activity.getResources( ).getString( R.string.help_trackables_stop_remove_yes ), this );
                    builder.setNegativeButton( activity.getResources( ).getString( R.string.help_trackables_stop_remove_no ), this );
                    builder.setCancelable( false );

                    AlertDialog alert = builder.create( );
                    alert.show( );
                }
            }
            else
            {
                final String error = jsonObject.getString( ServiceKeys.ERROR_KEY );
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(activity, error, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
        catch( JSONException e )
        {
            final String error = e.getMessage();
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(activity, error, Toast.LENGTH_SHORT).show();
                }
            });
        }
        finally
        {
//            progressDialog.dismiss( );
        }
    }

    @Override
    public void onClick( DialogInterface dialog, int which )
    {
        if( which == -1 )
        {
            ArrayList<Trackable> trackables = new ArrayList<Trackable>( );
            trackables.add( trackable );
            
            RemoveTrackableFromStopTask removeTrackableFromStopTask = new RemoveTrackableFromStopTask( activity, stopID, trackables );
            removeTrackableFromStopTask.execute( WebServices.URL_REMOVE_TRACKABLE_FROM_STOP, ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance( ).getToken( ), ServiceKeys.ID_STOP_KEY, String.valueOf( stopID ), Trackable.TRACKABLES_KEY,
                    JSONManager.getJSONForTrackablesStopRemoval( trackables ) );
        }
    }
}
