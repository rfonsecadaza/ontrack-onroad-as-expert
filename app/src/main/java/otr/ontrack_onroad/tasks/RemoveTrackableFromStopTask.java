package otr.ontrack_onroad.tasks;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;

import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.models.Route;
import otr.ontrack_onroad.models.Stop;
import otr.ontrack_onroad.models.Trackable;
import otr.ontrack_onroad.parameters.GeneralMessages;
import otr.ontrack_onroad.parameters.ServiceKeys;
import otr.ontrack_onroad.utils.WebRequestManager;
import otr.ontrack_onroad_debug.activities.MainActivity;
import otr.ontrack_onroad_debug.activities.R;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;

public class RemoveTrackableFromStopTask extends AsyncTask<String, Void, String>
{
    private Activity activity;
    
    private ProgressDialog progressDialog;
    
    private ArrayList<Trackable> trackables;

    private int newStopID;
    
    public RemoveTrackableFromStopTask( Activity activity, int newStopID, ArrayList<Trackable> trackables )
    {
        this.activity = activity;
        progressDialog = new ProgressDialog( activity );
        this.trackables = trackables;
        this.newStopID = newStopID;
    }
    
    @Override
    protected void onPreExecute( )
    {
        progressDialog.setTitle( activity.getResources( ).getString( R.string.loading ) );
        progressDialog.setMessage( activity.getResources( ).getString( R.string.please_wait ) );
        progressDialog.show( );
    }

    @Override
    protected String doInBackground( String... params )
    {
        String response = "";
        try
        {
            response = WebRequestManager.executeTask( params );
        }
        catch( IOException e )
        {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(activity, GeneralMessages.HTTP_NOT_FOUND_ERROR, Toast.LENGTH_SHORT).show();
                }
            });
        }
        
        return response;
    }

    @Override
    protected void onPostExecute( String result )
    {
        try
        {
            JSONObject jsonObject = new JSONObject( result );
            
            int status = jsonObject.getInt( ServiceKeys.STATUS_KEY );
            
            if( status == 1 )
            {
                Route route = OnTrackManager.getInstance( ).getUser( ).getRoute( );
                Stop newStop = null;
                
                for( Stop stop : route.getStops( ) )
                {
                    if( stop.getId( ) == newStopID )
                    {
                        newStop = stop;
                    }
                }

                for( int i = 0; i < trackables.size( ); i++ )
                {
                    Trackable trackable = trackables.get( i );
                    
                    Iterator<Trackable> iterator = newStop.getTrackables( ).iterator( );
                    
                    while( iterator.hasNext( ) )
                    {
                        Trackable stopTrackable = iterator.next( );
                        
                        if( stopTrackable.getId( ).equals( trackable.getId( ) ) )
                        {
                            iterator.remove( );
                        }
                    }
                }
                
                ( ( MainActivity )activity ).updateStopDetailsFragmentRefreshTrackables( );
            }
            else
            {
                final String error = jsonObject.getString( ServiceKeys.ERROR_KEY );
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(activity, error, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
        catch( JSONException e )
        {
            final String error = e.getMessage();
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(activity, error, Toast.LENGTH_SHORT).show();
                }
            });
        }
        finally
        {
            progressDialog.dismiss( );
        }
    }
}
