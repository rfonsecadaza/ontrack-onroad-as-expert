package otr.ontrack_onroad.tasks;

import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import otr.ontrack_onroad.models.Organization;
import otr.ontrack_onroad.models.Route;
import otr.ontrack_onroad.parameters.GeneralMessages;
import otr.ontrack_onroad.parameters.ServiceKeys;
import otr.ontrack_onroad.utils.JSONManager;
import otr.ontrack_onroad.utils.WebRequestManager;
import otr.ontrack_onroad_debug.activities.R;
import otr.ontrack_onroad_debug.activities.RouteList;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;

public class GetAllRoutesForOrganizationTask extends AsyncTask<String, Void, String>
{
    private Activity activity;

    private ProgressDialog progressDialog;
    
    private Organization organization;

    public GetAllRoutesForOrganizationTask( Activity activity, Organization organization )
    {
        this.activity = activity;
        this.organization = organization;
        progressDialog = new ProgressDialog( activity );
    }

    @Override
    protected void onPreExecute( )
    {
        progressDialog.setTitle( activity.getResources( ).getString( R.string.loading ) );
        progressDialog.setMessage( activity.getResources( ).getString( R.string.please_wait ) );
        progressDialog.setCancelable( false );
        progressDialog.show( );
    }

    @Override
    protected String doInBackground( String... params )
    {
        String response = "";
        try
        {
            response = WebRequestManager.executeTask( params );
        }
        catch( IOException e )
        {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(activity, GeneralMessages.HTTP_NOT_FOUND_ERROR, Toast.LENGTH_SHORT).show();
                }
            });
        }

        return response;
    }

    @Override
    protected void onPostExecute( String result )
    {
        try
        {
            JSONObject jsonObject = new JSONObject( result );

            int status = jsonObject.getInt( ServiceKeys.STATUS_KEY );

            if( status == 1 )
            {
                JSONArray routeSchedules = jsonObject.getJSONArray( Route.ROUTE_SCHEDULES_KEY );
                ArrayList<Route> routes = JSONManager.loadRoutesArray( routeSchedules );
                organization.setRoutes( routes );
                organization.configureRoutesFreeModeFromRoutes();
                
                ( ( RouteList )activity ).routesLoaded( );
            }
            else
            {
                final String error = jsonObject.getString( ServiceKeys.ERROR_KEY );
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(activity, error, Toast.LENGTH_SHORT).show();
                    }
                });;
            }
        }
        catch( JSONException e )
        {
            final String error = e.getMessage();
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(activity, GeneralMessages.HTTP_NOT_FOUND_ERROR, Toast.LENGTH_SHORT).show();
                }
            });
        }
        finally
        {
            progressDialog.dismiss( );
        }
    }
}
