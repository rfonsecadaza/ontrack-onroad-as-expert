package otr.ontrack_onroad.tasks;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;

import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.models.Point;
import otr.ontrack_onroad.models.Portion;
import otr.ontrack_onroad.models.Route;
import otr.ontrack_onroad.models.Stop;
import otr.ontrack_onroad.parameters.GeneralMessages;
import otr.ontrack_onroad.parameters.ServiceKeys;
import otr.ontrack_onroad.utils.WebRequestManager;
import otr.ontrack_onroad_debug.activities.MainActivity;
import otr.ontrack_onroad_debug.activities.R;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;

public class UpdateStopLocationTask extends AsyncTask<String, Void, String>
{
    private Activity activity;

    private ProgressDialog progressDialog;
    
    private ArrayList<Stop> modifiedStops;

    public UpdateStopLocationTask( Activity activity, ArrayList<Stop> modifiedStops )
    {
        this.activity = activity;
        this.modifiedStops = modifiedStops;
        progressDialog = new ProgressDialog( activity );
    }

    @Override
    protected void onPreExecute( )
    {
        progressDialog.setTitle( activity.getResources( ).getString( R.string.loading ) );
        progressDialog.setMessage( activity.getResources( ).getString( R.string.please_wait ) );
        progressDialog.show( );
    }

    @Override
    protected String doInBackground( String... params )
    {
        String response = "";
        try
        {
            response = WebRequestManager.executeTask( params );
        }
        catch( IOException e )
        {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(activity, GeneralMessages.HTTP_NOT_FOUND_ERROR, Toast.LENGTH_SHORT).show();
                }
            });
        }

        return response;
    }

    @Override
    protected void onPostExecute( String result )
    {
        try
        {
            JSONObject jsonObject = new JSONObject( result );

            int status = jsonObject.getInt( ServiceKeys.STATUS_KEY );

            if( status == 1 )
            {
                Route route = OnTrackManager.getInstance( ).getUser( ).getRoute( );
                Stop nextStop = OnTrackManager.getInstance( ).getNextStop( );
                
                for( Stop modifiedStop : modifiedStops )
                {
                    Stop stop = route.getStop( modifiedStop.getId( ) );
                    
                    if( stop != null )
                    {
                        double latitude = modifiedStop.getLatitude( );
                        double longitude = modifiedStop.getLongitude( );
                        int order = modifiedStop.getOrder( );
                        ArrayList<Point> steps = modifiedStop.getSteps( );
                        Portion portion = ( ( MainActivity )activity ).getPortion( stop.getOrder( ) );
//                        Marker marker = portion.getMarker( );
                        
                        if( latitude != 0.0 && longitude != 0.0 )
                        {
                            stop.setLatitude( latitude );
                            stop.setLongitude( longitude );
//                            marker.setPosition( new LatLng( latitude, longitude ) );
                        }
                        
                        if( order != -1 )
                        {
                            stop.setOrder( order );
                            portion.setOrder( order );
                            
                            if( stop.isSpecial( ) )
                            {
//                                marker.setIcon( BitmapDescriptorFactory.fromResource( activity.getResources( ).getIdentifier( "marker_orange_" + order, "drawable", activity.getPackageName( ) ) ) );
                            }
                            else
                            {
//                                marker.setIcon( BitmapDescriptorFactory.fromResource( activity.getResources( ).getIdentifier( "marker_" + order, "drawable", activity.getPackageName( ) ) ) );
                            }
                            
                            if( order == nextStop.getOrder( ) && nextStop.getId( ) != modifiedStop.getId( ) )
                            {
                                OnTrackManager.getInstance( ).setNextStop( stop );
                            }
                        }
                        
                        if( steps != null )
                        {
//                            portion.clear( );
                            stop.setSteps( steps );
                            drawPolylines( stop );
                        }
                    }
                }
                
                Collections.sort( route.getStops( ) );
                Collections.sort( ( ( MainActivity )activity ).getPortions( ) );
                ( ( MainActivity )activity ).drawCircleAroundStop( OnTrackManager.getInstance( ).getNextStop( ) );
                ( ( MainActivity )activity ).updateStopListFragmentOrganizeButtons( );
                ( ( MainActivity )activity ).updateStopListFragmentBar( );
            }
            else
            {
                final String error = jsonObject.getString( ServiceKeys.ERROR_KEY );
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(activity, error, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
        catch( JSONException e )
        {
            final String error = e.getMessage();
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(activity, error, Toast.LENGTH_SHORT).show();
                }
            });
        }
        finally
        {
            progressDialog.dismiss( );
        }
    }
    
    private void drawPolylines( Stop stop )
    {
        LatLng first = new LatLng( stop.getSteps( ).get( 0 ).getLatitude( ), stop.getSteps( ).get( 0 ).getLongitude( ) );
        
        for( int i = 1; i < stop.getSteps( ).size( ); i++ )
        {
            Point point = stop.getSteps( ).get( i );
            LatLng last = new LatLng( point.getLatitude( ), point.getLongitude( ) );
            PolylineOptions rectOptions = new PolylineOptions( );
            rectOptions.add( first );
            rectOptions.add( last );
            rectOptions.color( activity.getResources( ).getColor( R.color.create_route_red_button ) );
            rectOptions.width( 10 );
            rectOptions.zIndex( 1000 );
//            Polyline polyline = ( ( MainActivity )activity ).getMap( ).addPolyline( rectOptions );
//            ( ( MainActivity )activity ).getPortion( stop.getOrder( ) ).addLine( polyline );
            first = last;
        }
    }
}
