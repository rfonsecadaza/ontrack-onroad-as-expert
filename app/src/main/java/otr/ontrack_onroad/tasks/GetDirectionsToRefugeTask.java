package otr.ontrack_onroad.tasks;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;


import org.json.JSONException;

import java.io.IOException;

import otr.ontrack_onroad.dao.Event;
import otr.ontrack_onroad.managers.EventManager;
import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.managers.OnTrackManagerAsyncTasks;
import otr.ontrack_onroad.parameters.GeneralMessages;
import otr.ontrack_onroad.parameters.ServiceKeys;
import otr.ontrack_onroad.utils.JSONManager;
import otr.ontrack_onroad.utils.Timestamp;
import otr.ontrack_onroad.utils.WebRequestManager;
import otr.ontrack_onroad_debug.activities.MainActivity;
import otr.ontrack_onroad_debug.activities.R;

/**
 * Created by rafaelmunoz on 9/24/15.
 */
public class GetDirectionsToRefugeTask extends AsyncTask<String, Void, String>
{
    private Context context;

    private ProgressDialog progressDialog;

    private String refugeName;

    public GetDirectionsToRefugeTask( Context context, String refugeName )
    {
        this.context = context;
        this.refugeName = refugeName;
        progressDialog = new ProgressDialog( context );
    }

    @Override
    protected void onPreExecute( )
    {
        progressDialog.setTitle( context.getResources( ).getString( R.string.loading ) );
        progressDialog.setMessage( context.getResources( ).getString( R.string.please_wait ) );
        progressDialog.show( );
        progressDialog.setCancelable( false );
    }

    @Override
    protected String doInBackground( String... params )
    {
        String response = "";

        try
        {
            response = WebRequestManager.executeTaskGetWithoutFormatting( params );
            return response;
        }
        catch( IOException e )
        {
            ((Activity)context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(context, GeneralMessages.HTTP_NOT_FOUND_ERROR, Toast.LENGTH_SHORT).show();
                }
            });
        }

        return response;
    }

    @Override
    protected void onPostExecute( String result )
    {
        JSONManager.createRouteFromMapboxDirectionsOnRoad( context, result );
        ( ( MainActivity )context ).drawRouteToRefuge( );


        Event event = new Event( null, Timestamp.completeTimestamp(), Event.REFUGE_CODE, GeneralMessages.GOING_TO_REFUGE+refugeName, 0, 1, "" + OnTrackManager.getInstance( ).getUser( ).getFootprint( ), OnTrackManager.getInstance( ).getUser( )
                .getRoute( ).getId( ), false );
        // EventManager.getInstance().getEvents().add(event);
        // EventManager.getInstance().getStackedEvents().add(event);

        // Guardar evento en db local
        OnTrackManager.getInstance().getEvents( ).add( event );
        OnTrackManager.getInstance( ).getEventDao( ).insert( event );

        // Notificar a los listener de eventos que hay una nueva
        // notificaci�n
        EventManager.getInstance().notifyEventGenerated(event.getId());

        try
        {
            EventManager.getInstance( ).executeSendEventTask( OnTrackManagerAsyncTasks.URL_BASE_ADD_EVENTS, ServiceKeys.TOKEN_KEY, OnTrackManager.getInstance( ).getToken( ), ServiceKeys.EVENTS_KEY,
                    event.getEventJSONString( Event.FK_FOOTPRINT_KEY ) );
        }
        catch( JSONException e )
        {
            // TODO Auto-generated catch block
            e.printStackTrace( );
        }


        progressDialog.dismiss( );
    }
}
