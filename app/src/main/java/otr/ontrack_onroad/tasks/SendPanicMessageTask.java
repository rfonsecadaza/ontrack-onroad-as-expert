package otr.ontrack_onroad.tasks;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import otr.ontrack_onroad.dao.Event;
import otr.ontrack_onroad.managers.EventManager;
import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.managers.OnTrackManagerAsyncTasks;
import otr.ontrack_onroad.managers.OnTrackManagerListener;
import otr.ontrack_onroad.parameters.GeneralMessages;
import otr.ontrack_onroad.parameters.ServiceKeys;
import otr.ontrack_onroad.utils.JSONManager;
import otr.ontrack_onroad.utils.Timestamp;
import otr.ontrack_onroad.utils.WebRequestManager;
import otr.ontrack_onroad_debug.activities.MainActivity;
import otr.ontrack_onroad_debug.activities.R;

/**
 * Created by rodrigoivanf on 07/12/2015.
 */
public class SendPanicMessageTask extends AsyncTask<String, Void, String>
{
    private Context context;

    private ProgressDialog progressDialog;

    boolean success = false;

    public SendPanicMessageTask( Context context )
    {
        this.context = context;
        progressDialog = new ProgressDialog( context );
    }

    @Override
    protected void onPreExecute( )
    {
        progressDialog.setTitle( context.getResources( ).getString( R.string.panic_sending) );
        progressDialog.setMessage( context.getResources( ).getString( R.string.please_wait ) );
        progressDialog.show( );
        progressDialog.setCancelable( false );
    }

    @Override
    protected String doInBackground( String... params )
    {
        String response = "";

        try
        {
            response = WebRequestManager.executeTask(params);
            success = true;
            return response;
        }
        catch( IOException e ) {
            ((OnTrackManagerListener) context).onAsyncTaskError(GeneralMessages.HTTP_NOT_FOUND_ERROR);
        }

        return response;
    }

    @Override
    protected void onPostExecute( String result )
    {
        try {
            JSONObject jsonObject = new JSONObject(result);
            int status = jsonObject.getInt(ServiceKeys.STATUS_KEY);
            if(status != 1){
                ((OnTrackManagerListener) context).onAsyncTaskError(jsonObject.getString(ServiceKeys.ERROR_KEY));
            }
        } catch (JSONException e) {
            ((OnTrackManagerListener) context).onAsyncTaskError(e.getMessage());
        }
        progressDialog.dismiss( );
    }
}
