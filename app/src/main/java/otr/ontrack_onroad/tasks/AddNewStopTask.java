package otr.ontrack_onroad.tasks;

import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.models.Portion;
import otr.ontrack_onroad.models.Route;
import otr.ontrack_onroad.models.Stop;
import otr.ontrack_onroad.parameters.GeneralMessages;
import otr.ontrack_onroad.parameters.ServiceKeys;
import otr.ontrack_onroad.utils.WebRequestManager;
import otr.ontrack_onroad_debug.activities.MainActivity;
import otr.ontrack_onroad_debug.activities.R;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

public class AddNewStopTask extends AsyncTask<String, Void, String>
{
    private Activity activity;

    private ProgressDialog progressDialog;

    private ArrayList<Stop> modifiedStops;

    private Route route;

    public AddNewStopTask( Activity activity, ArrayList<Stop> modifiedStops )
    {
        this.activity = activity;
        this.modifiedStops = modifiedStops;
        route = OnTrackManager.getInstance( ).getUser( ).getRoute( );
        progressDialog = new ProgressDialog( activity );
    }

    @Override
    protected void onPreExecute( )
    {
        progressDialog.setTitle( activity.getResources( ).getString( R.string.loading ) );
        progressDialog.setMessage( activity.getResources( ).getString( R.string.please_wait ) );
        progressDialog.show( );
    }

    @Override
    protected String doInBackground( String... params )
    {
        String response = "";
        try
        {
            response = WebRequestManager.executeTask( params );
        }
        catch( IOException e )
        {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                   Toast.makeText(activity, GeneralMessages.HTTP_NOT_FOUND_ERROR, Toast.LENGTH_SHORT).show();
                }
            });

        }

        return response;
    }

    @Override
    protected void onPostExecute( String result )
    {
        try
        {
            JSONObject jsonObject = new JSONObject( result );

            int status = jsonObject.getInt( ServiceKeys.STATUS_KEY );

            if( status == 1 )
            {
                Stop newStop = modifiedStops.get( 0 );
                int id = jsonObject.getInt( ServiceKeys.ID_KEY );
                newStop.setId( id );
                ArrayList<Portion> portions = ( ( MainActivity )activity ).getPortions( );
                boolean updateOrder = false;

                for( int i = 0; i < route.getStops( ).size( ); i++ )
                {
                    Stop stop = route.getStops( ).get( i );
                    Portion portion = portions.get( i );

                    if( updateOrder )
                    {
                        stop.setOrder( i );
                        portion.setOrder( i );

                        if( stop.isSpecial( ) )
                        {
//                            portion.getMarker( ).setIcon( BitmapDescriptorFactory.fromResource( activity.getResources( ).getIdentifier( "marker_orange_" + i, "drawable", activity.getPackageName( ) ) ) );
                        }
                        else
                        {
//                            portion.getMarker( ).setIcon( BitmapDescriptorFactory.fromResource( activity.getResources( ).getIdentifier( "marker_" + i, "drawable", activity.getPackageName( ) ) ) );
                        }
                    }
                    else if( stop.getOrder( ) == newStop.getOrder( ) )
                    {
                        route.getStops( ).add( i, newStop );
                        updateOrder = true;
//                        portion.clear( );
//                        stop.setSteps( modifiedStops.get( 1 ).getSteps( ) );
//                        drawPolylines( stop, portion );
//                        Portion newPortion = new Portion( i );
//                        portions.add( i, newPortion );
//                        drawPolylines( newStop, newPortion );

                        Marker marker;
                        if( newStop.isSpecial( ) )
                        {
//                             marker = ( ( MainActivity )activity ).getMap( ).addMarker(
//                                            new MarkerOptions( ).position( new LatLng( newStop.getLatitude( ), newStop.getLongitude( ) ) ).title( Parameters.STOP_MARKER_TITLE )
//                                                    .icon( BitmapDescriptorFactory.fromResource( activity.getResources( ).getIdentifier( "marker_orange_" + i, "drawable", activity.getPackageName( ) ) ) ).anchor( 0.5f, 1.0f ) );
                        }
                        else
                        {
//                            marker = ( ( MainActivity )activity ).getMap( ).addMarker(
//                                    new MarkerOptions( ).position( new LatLng( newStop.getLatitude( ), newStop.getLongitude( ) ) ).title( Parameters.STOP_MARKER_TITLE )
//                                            .icon( BitmapDescriptorFactory.fromResource( activity.getResources( ).getIdentifier( "marker_" + i, "drawable", activity.getPackageName( ) ) ) ).anchor( 0.5f, 1.0f ) );
                        }
                        
//                        newPortion.setMarker( marker );
                    }
                }

                OnTrackManager.getInstance( ).setNextStop( newStop );
                ( ( MainActivity )activity ).drawCircleAroundStop( newStop );
                ( ( MainActivity )activity ).updateStopListFragmentAddButton( newStop );
                ( ( MainActivity )activity ).updateStopListFragmentBar( );
            }
            else
            {
                final String error = jsonObject.getString( ServiceKeys.ERROR_KEY );
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(activity, error, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
        catch( JSONException e )
        {
            final String error = e.getMessage();
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(activity, error, Toast.LENGTH_SHORT).show();
                }
            });
        }
        finally
        {
            progressDialog.dismiss( );
        }
    }
    private void drawPolylines( Stop stop, Portion portion )
    {
        LatLng first = new LatLng( stop.getSteps( ).get( 0 ).getLatitude( ), stop.getSteps( ).get( 0 ).getLongitude( ) );

        for( int i = 1; i < stop.getSteps( ).size( ); i++ )
        {
//            Point point = stop.getSteps( ).get( i );
//            LatLng last = new LatLng( point.getLatitude( ), point.getLongitude( ) );
//            PolylineOptions rectOptions = new PolylineOptions( );
//            rectOptions.add( first );
//            rectOptions.add( last );
//            rectOptions.color( activity.getResources( ).getColor( R.color.create_route_red_button ) );
//            rectOptions.width( 10 );
//            rectOptions.zIndex( 1000 );
//            Polyline polyline = ( ( MainActivity )activity ).getMap( ).addPolyline( rectOptions );
//            portion.addLine( polyline );
//            first = last;
        }
    }
}
