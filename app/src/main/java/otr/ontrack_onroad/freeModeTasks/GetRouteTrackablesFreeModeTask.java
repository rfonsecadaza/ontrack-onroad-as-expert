package otr.ontrack_onroad.freeModeTasks;

import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;

import otr.ontrack_onroad.parameters.GeneralMessages;
import otr.ontrack_onroad.parameters.ServiceKeys;
import otr.ontrack_onroad.utils.JSONManager;
import otr.ontrack_onroad.utils.WebRequestManager;
import otr.ontrack_onroad_debug.activities.FreeMode;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;

public class GetRouteTrackablesFreeModeTask extends AsyncTask<String, Void, String>
{
    private Activity activity;
    
    private ProgressDialog progressDialog;
    
    public GetRouteTrackablesFreeModeTask( Activity activity )
    {
        this.activity = activity;
        this.progressDialog = new ProgressDialog( activity );
    }
    
//    @Override
//    protected void onPreExecute( )
//    {
//        progressDialog.setTitle( activity.getResources( ).getString( R.string.loading ) );
//        progressDialog.setMessage( activity.getResources( ).getString( R.string.please_wait ) );
//        progressDialog.show( );
//    }
    
    @Override
    protected String doInBackground( String... params )
    {
        String response = "";
        try
        {
            response = WebRequestManager.executeTask( params );
        }
        catch( IOException e )
        {
            Toast.makeText( activity, GeneralMessages.HTTP_NOT_FOUND_ERROR, Toast.LENGTH_SHORT ).show( );
        }
        
        return response;
    }

    @Override
    protected void onPostExecute( String result )
    {
        try
        {
            JSONObject jsonObject = new JSONObject( result );
            
            int status = jsonObject.getInt( ServiceKeys.STATUS_KEY );
            
            if( status == 1 )
            {
                JSONManager.getRouteTrackables(result);
//                FreeModeEventsManager.getInstance( ).sendRouteStartedEvent( );
//                ((FreeMode)activity).showAssistanceTypeDialog();
                ((FreeMode)activity).onDetailedRouteLoaded();
            }
            else
            {
                final String error = jsonObject.getString( ServiceKeys.ERROR_KEY );
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(activity, error, Toast.LENGTH_SHORT).show();
                    }
                });



            }
        }
        catch( JSONException e )
        {
            final String error = e.getMessage();
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(activity,error, Toast.LENGTH_SHORT).show();
                }
            });
        }
        finally
        {
            progressDialog.dismiss( );
        }
    }
}

