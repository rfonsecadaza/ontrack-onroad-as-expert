package otr.ontrack_onroad.freeModeTasks;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;


import java.io.IOException;

import otr.ontrack_onroad.managers.FreeModeLocationManager;
import otr.ontrack_onroad.parameters.GeneralMessages;
import otr.ontrack_onroad.utils.JSONManager;
import otr.ontrack_onroad.utils.WebRequestManager;
import otr.ontrack_onroad_debug.activities.R;

/**
 * Created by rafaelmunoz on 9/21/15.
 */
public class GetDirectionsToRefugeTask extends AsyncTask<String, Void, String>
{
    private Context context;

    private ProgressDialog progressDialog;

    public GetDirectionsToRefugeTask( Context context )
    {
        this.context = context;
        progressDialog = new ProgressDialog( context );
    }

    @Override
    protected void onPreExecute( )
    {
        progressDialog.setTitle( context.getResources( ).getString( R.string.loading ) );
        progressDialog.setMessage( context.getResources( ).getString( R.string.please_wait ) );
        progressDialog.show( );
        progressDialog.setCancelable( false );
    }

    @Override
    protected String doInBackground( String... params )
    {
        String response = "";

        try
        {
            response = WebRequestManager.executeTaskGetWithoutFormatting( params );
            return response;
        }
        catch( IOException e )
        {
            ((Activity)context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(context, GeneralMessages.HTTP_NOT_FOUND_ERROR, Toast.LENGTH_SHORT).show();
                }
            });
        }

        return response;
    }

    @Override
    protected void onPostExecute( String result )
    {
        JSONManager.createRouteFromMapboxDirectionsFreeMode( result );
        FreeModeLocationManager.getInstance( ).drawRouteToRefuge( );

        progressDialog.dismiss( );
    }
}
