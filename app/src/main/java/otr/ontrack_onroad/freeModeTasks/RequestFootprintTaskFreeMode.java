package otr.ontrack_onroad.freeModeTasks;

import android.app.Activity;
import android.os.AsyncTask;

import org.json.JSONException;

import java.io.IOException;

import otr.ontrack_onroad.dao.Event;
import otr.ontrack_onroad.managers.EventManager;
import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.parameters.GeneralMessages;
import otr.ontrack_onroad.parameters.ServiceKeys;
import otr.ontrack_onroad.utils.JSONManager;
import otr.ontrack_onroad.utils.Timestamp;
import otr.ontrack_onroad.utils.WebRequestManager;
import otr.ontrack_onroad_debug.activities.FreeMode;

/**
 * Created by rodrigoivanf on 10/11/2015.
 */

public class RequestFootprintTaskFreeMode extends AsyncTask<String,Integer,String> {

    // Envï¿½a un conjunto de eventos generados por el dispositivo
    public static final String URL_BASE_ADD_EVENTS = "ApiEvent/AddEvents";


    private Activity activity;

    public RequestFootprintTaskFreeMode(Activity activity) {
        this.activity = activity;
    }


    @Override
    protected String doInBackground(String... args) {
        String response = null;
        try {
            response = WebRequestManager.executeTask(args);
            JSONManager.loadFootprint2(response);
        } catch (IOException e) {
            response = GeneralMessages.HTTP_NOT_FOUND_ERROR;
        } catch (JSONException e) {
            e.printStackTrace();
            response = GeneralMessages.JSON_ERROR;
        }

        return response;
    }

    @Override
    protected void onPostExecute(String result) {
        // mProgressDialog.dismiss();

        if (result.equals(GeneralMessages.JSON_ERROR)
                || result.equals(GeneralMessages.HTTP_CONNECTION_ERROR)
                || result.equals(GeneralMessages.HTTP_NOT_FOUND_ERROR)) {
            OnTrackManager.getInstance().getListener()
                    .onAsyncTaskError(result);
        } else {

            // Cargar todos los puntos ya "trackeados"
//				executeRequestCompleteTrackTask(URL_BASE_GIVE_COMPLETE_TRACK,
//						ServiceKeys.DATE_KEY, Timestamp.completeTimestamp(),
//						ServiceKeys.ID_FOOTPRINT_KEY, ""
//								+ OnTrackManager.getInstance().getUser()
//								.getFootprint(), ServiceKeys.TOKEN_KEY,
//						OnTrackManager.getInstance().getToken());

            ((FreeMode)activity).onFootprintLoaded();

            OnTrackManager.getInstance().startRoute();

            if (!OnTrackManager.getInstance().isRouteReloaded()) {
                // Evento de inicio de ruta y envï¿½o
                Event event = new Event(null,
                        Timestamp.completeTimestamp(),
                        Event.START_ROUTE_EVENT_CODE,
                        GeneralMessages.ROUTE_HAS_STARTED, 0, 1, ""
                        + OnTrackManager.getInstance().getUser()
                        .getFootprint(),
                        OnTrackManager.getInstance().getUser().getRoute()
                                .getId(), false);
                // EventManager.getInstance().getEvents().add(event);
                // EventManager.getInstance().getStackedEvents().add(event);

                // Guardar evento en db local
                OnTrackManager.getInstance().getEvents().add(event);
                OnTrackManager.getInstance().getEventDao().insert(event);

                // Notificar a los listener de eventos que hay una nueva
                // notificaciï¿½n
                EventManager.getInstance().notifyEventGenerated(
                        event.getId());

                // try {
                try {
                    EventManager
                            .getInstance()
                            .executeSendEventTask(
                                    URL_BASE_ADD_EVENTS,
                                    ServiceKeys.TOKEN_KEY,
                                    OnTrackManager.getInstance().getToken(),
                                    ServiceKeys.EVENTS_KEY,
                                    event.getEventJSONString(Event.FK_FOOTPRINT_KEY));
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


            }
        }
    }
}
