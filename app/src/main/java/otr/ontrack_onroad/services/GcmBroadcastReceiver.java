package otr.ontrack_onroad.services;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

public class GcmBroadcastReceiver extends WakefulBroadcastReceiver{
	 @Override
	    public void onReceive(Context context, Intent intent) {
	        // Especificar que GcmIntentService se encargará del Intent
	        ComponentName comp = new ComponentName(context.getPackageName(),
	                GcmIntentService.class.getName());
	        // Inicar el servicio, manteniendo el dispositivo despierto mientras se ejecuta
	        startWakefulService(context, (intent.setComponent(comp)));
	        setResultCode(Activity.RESULT_OK);
	    }
}
