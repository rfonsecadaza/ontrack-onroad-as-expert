package otr.ontrack_onroad.services;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;

import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.models.Point;
import otr.ontrack_onroad.models.Stop;
import otr.ontrack_onroad.utils.GeoPos;

public class GPSService extends Service implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener{

    // Constantes para el manejo de GPS
    public static final long SEND_LOCATION_PERIOD = 10000;
    public static final long SEND_LOCATION_TIMEOUT = 9000;
    public static final int MAXIMUM_TIME_BETWEEN_LOCATION_UPDATES = 5;
    private static final int MILLISECONDS_PER_SECOND = 1000;
    public static final int UPDATE_INTERVAL_IN_SECONDS = 1; // Frecuencia de
    // actualizaciï¿½n
    // en
    // segundos
    private static final long UPDATE_INTERVAL = MILLISECONDS_PER_SECOND
            * UPDATE_INTERVAL_IN_SECONDS;
    private static final int FASTEST_INTERVAL_IN_SECONDS = 1; // La frecuencia
    // mï¿½s
    // rï¿½pida de
    // actualizaciï¿½n,
    // en segundos
    private static final long FASTEST_INTERVAL = MILLISECONDS_PER_SECOND
            * FASTEST_INTERVAL_IN_SECONDS;

    boolean mUpdatesRequested;

    private final IBinder mBinder = new LocalBinder();

    private LocationRequest mLocationRequest;
    private GoogleApiClient mLocationClient;
    private Location location;

    private GPSServiceListener listener;



//    @Override
//    public int onStartCommand(Intent intent, int flags, int startId) {
//        mLocationRequest = LocationRequest.create();
//        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
//        mLocationRequest.setInterval(UPDATE_INTERVAL);
//        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
//
//        mLocationClient = new GoogleApiClient.Builder(this)
//                .addApi(LocationServices.API).addConnectionCallbacks(this)
//                .addOnConnectionFailedListener(this).build();
//        mUpdatesRequested = true;
//
//        return super.onStartCommand(intent, flags, startId);
//    }

    public GPSService() {
    }

    public void setGPSServiceListener(GPSServiceListener listener){
        this.listener = listener;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);

        mLocationClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API).addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        mLocationClient.connect();
        mUpdatesRequested = true;
    }

    @Override
    public IBinder onBind(Intent intent) {
       return mBinder;
    }



    public class LocalBinder extends Binder {
        public GPSService getService() {
            return GPSService.this;
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        if (mUpdatesRequested) {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mLocationClient, mLocationRequest, this);
        }
    }



    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        OnTrackManager.getInstance().completeProcessLocation(location);
        if(listener != null) {
            listener.onServiceLocationChanged(location);
        }
    }

     // COPIA COMPLETA DE LO QUE ESTABA EN EL MAIN ACTIVITY
        // REQUIERE REVISIÓN

//        if(OnTrackManager.getInstance().isWaitingGPS() && routeReady){
//            if(!startAutomatically) {
//                ((StartRouteFragment) startRouteFragment).hideProgressBar();
//                ((StartRouteFragment) startRouteFragment)
//                        .setFragmentTest(getResources().getString(R.string.start_route));
//                OnTrackManager.getInstance().setWaitingGPS(false);
//                ((StartRouteFragment) startRouteFragment).setButtonEnabled(true);
//            }else{
//                startRoute();
//            }
//            OnTrackManager.getInstance().setWaitingGPS(false);
//        }
//
//        if (!OnTrackManager.getInstance().isSimulating()) {
//            if (ready && location != null) {
//                processLocation(location);
//            } else if (location != null) {
//                OnTrackManager.getInstance().setCurrentLocation(location);
//            }
//        }




    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }


}
