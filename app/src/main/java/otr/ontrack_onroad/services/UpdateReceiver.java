package otr.ontrack_onroad.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class UpdateReceiver extends BroadcastReceiver {
	@Override
	public void onReceive(Context context, Intent intent) {

		ConnectivityManager connectivityManager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo wifiActiveNetInfo = connectivityManager
				.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		NetworkInfo mobileActiveNetInfo = connectivityManager
				.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		boolean isConnected = mobileActiveNetInfo != null
				&& (mobileActiveNetInfo.isConnected() || wifiActiveNetInfo.isConnected());
		if (isConnected)
			// Hacer lo que sea necesario cuando se reestablezca la conexión
			Log.i("NET", "connected " + isConnected);
		else
			// Hacer lo que sea necesario cuando se pierda la conexión
			Log.i("NET", "not connected " + isConnected);
	}

}
