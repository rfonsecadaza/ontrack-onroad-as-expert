package otr.ontrack_onroad.services;

import android.location.Location;

/**
 * Created by rodrigoivanf on 22/10/2015.
 */
public interface GPSServiceListener {
    public void onServiceLocationChanged(Location location);
}
