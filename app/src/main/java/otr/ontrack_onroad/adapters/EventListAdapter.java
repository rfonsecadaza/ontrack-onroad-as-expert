package otr.ontrack_onroad.adapters;

import java.util.List;

import otr.ontrack_onroad.dao.Event;
import otr.ontrack_onroad.parameters.GeneralMessages;
import otr.ontrack_onroad_debug.activities.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class EventListAdapter extends BaseAdapter {
	private Context context;

	private List<Event> data;
	private static LayoutInflater inflater = null;

	public EventListAdapter(Context context, List<Event> data) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.data = data;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return data.get(data.size() - 1 - position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View vi = convertView;
		if (vi == null)
			vi = inflater.inflate(R.layout.event_row, null);
		Event event = data.get(data.size() - 1 - position);
		TextView tvEventTime = (TextView) vi.findViewById(R.id.tvEventTime);
		TextView tvEventDescription = (TextView) vi
				.findViewById(R.id.tvEventDescription);

		tvEventTime.setText(event.getDate());
		if (event.getFkRoute() == "-1") {
			tvEventDescription.setText(event.getDescription());
		} else {
			tvEventDescription.setText(GeneralMessages.ROUTE + " "
					+ event.getFkRoute() + ": " + event.getDescription());
		}

		int drawableId = Event.getImageIdFromCategory(event.getCategory());
		ImageView ivEventIcon = (ImageView) vi.findViewById(R.id.ivEventIcon);
		if (drawableId != -1) {
			ivEventIcon.setImageResource(drawableId);
		}
		

		return vi;
	}
}
