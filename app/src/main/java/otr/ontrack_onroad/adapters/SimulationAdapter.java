package otr.ontrack_onroad.adapters;

import java.util.List;

import otr.ontrack_onroad.dao.Simulation;
import otr.ontrack_onroad_debug.activities.R;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class SimulationAdapter extends BaseAdapter{

	private Context context;

	private List<Simulation> data;
	private static LayoutInflater inflater = null;

	public SimulationAdapter(Context context, List<Simulation> data) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.data = data;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return data.get(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View vi = convertView;
		if (vi == null)
			vi = inflater.inflate(R.layout.simulation_row, null);
		Typeface tf = Typeface.createFromAsset(context.getAssets(),
				"fonts/fjalla_one.ttf");
		TextView tvSimulationRoute = (TextView)vi.findViewById(R.id.tvSimulationRoute);
		tvSimulationRoute.setTypeface(tf);
		TextView tvSimulationDate = (TextView)vi.findViewById(R.id.tvSimulationDate);
		Simulation simulation = data.get(position);
		tvSimulationRoute.setText("Ruta "+simulation.getRouteId());
		tvSimulationDate.setText(simulation.getDate());
		
		return vi;
	}

}
