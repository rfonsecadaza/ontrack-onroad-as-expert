package otr.ontrack_onroad.adapters;

import java.util.ArrayList;

import otr.ontrack_onroad.models.User;
import otr.ontrack_onroad.utils.RoundedImageView;
import otr.ontrack_onroad_debug.activities.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

public class ProfileListAdapter extends BaseAdapter {

	private Context context;

    private ArrayList<User> data;
    private static LayoutInflater inflater = null;

    public ProfileListAdapter(Context context, ArrayList<User> data)
    {
        // TODO Auto-generated constructor stub
        this.context = context;
        this.data = data;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount()
    {
        // TODO Auto-generated method stub
        return data.size();
    }

    @Override
    public Object getItem(int position)
    {
        // TODO Auto-generated method stub
        return data.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        // TODO Auto-generated method stub
        View vi = convertView;
        if (vi == null)
            vi = inflater.inflate(R.layout.profile_row, null);
        User user = this.data.get(position);
        TextView tvUserName = (TextView)vi.findViewById(R.id.tvUserName);
        tvUserName.setText(user.getFullName());
        RoundedImageView rivUser = (RoundedImageView)vi.findViewById(R.id.rivUserImage);
        ImageLoader.getInstance( ).displayImage( user.getImageURL( ), rivUser );
        TextView tvAddress = (TextView)vi.findViewById(R.id.tvAddress);
        tvAddress.setText(user.getContactAddress());
        TextView tvLanPhone = (TextView)vi.findViewById(R.id.tvLanPhone);
        tvLanPhone.setText(user.getLanPhone());
        TextView tvMobilePhone = (TextView)vi.findViewById(R.id.tvMobilePhone);
        tvMobilePhone.setText(user.getMobilePhone());
        TextView tvEmail = (TextView)vi.findViewById(R.id.tvEMail);
        tvEmail.setText(user.getId());
        return vi;
    }
}
