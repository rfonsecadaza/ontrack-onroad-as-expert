package otr.ontrack_onroad.adapters;

import java.util.ArrayList;

import otr.ontrack_onroad.models.Trackable;
import otr.ontrack_onroad.utils.RoundedImageView;
import otr.ontrack_onroad_debug.activities.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

public class AssistanceStudentListAdapter extends BaseAdapter {
	private Context context;

	private ArrayList<AssistanceStudentListItem> data;
	private static LayoutInflater inflater = null;

	public AssistanceStudentListAdapter(Context context,
			ArrayList<AssistanceStudentListItem> data) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.data = data;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public void setData(ArrayList<AssistanceStudentListItem> data) {
		this.data = data;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		if (!data.get(position).isSection())
			return ((Trackable) data.get(position)).getTrackedStopTrackableId();
		return position;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View vi = convertView;
		final AssistanceStudentListItem i = this.data.get(position);
//		if (vi == null) {
//			if (!i.isSection()) {
//				
//			}else{
//				
//			}
//		}

		if (!i.isSection()) {
			vi = inflater.inflate(R.layout.element_trackable_card, null);
			// ImageView ivStudent= (ImageView) vi.findViewById(R.id.ivStudent);
			TextView tvStudentName = (TextView) vi
					.findViewById(R.id.element_trackable_card_name);
			TextView tvStudentState = (TextView) vi
					.findViewById(R.id.element_trackable_card_id);
			RoundedImageView rivStudentPicture = (RoundedImageView) vi
					.findViewById(R.id.element_trackable_card_image);
			if (!((Trackable) i).isRelevant()) {
				tvStudentState.setText("Ausente");
			} else {
				tvStudentState.setText("");
			}

			tvStudentName.setText(((Trackable) data.get(position)).getName() + "\n"
					+ ((Trackable) data.get(position)).getSubName());
			ImageLoader.getInstance( ).displayImage( ((Trackable) data.get(position)).getImageURL( ), rivStudentPicture );
		}else{
			vi = inflater.inflate(R.layout.element_trackable_card_header, null);
			TextView tvStopHeader = (TextView)vi.findViewById(R.id.tvStopHeader);
			tvStopHeader.setText(((AssistanceStudentListHeader)i).getHeader());
		}
		return vi;
	}
}
