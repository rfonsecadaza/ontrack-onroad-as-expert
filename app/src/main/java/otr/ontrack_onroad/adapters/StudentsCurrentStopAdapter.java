package otr.ontrack_onroad.adapters;

import java.util.ArrayList;

import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.models.Novelty;
import otr.ontrack_onroad.models.Organization;
import otr.ontrack_onroad.models.Trackable;
import otr.ontrack_onroad.parameters.Parameters;
import otr.ontrack_onroad.utils.RoundedImageView;
import otr.ontrack_onroad_debug.activities.R;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created with IntelliJ IDEA. User: usuario
 */
public class StudentsCurrentStopAdapter extends BaseAdapter {

    private Context context;

    public static final int ADAPTER_TYPE_NORMAL = 0;
    public static final int ADAPTER_TYPE_STOP_HEADERS = 1;

    private ArrayList<Trackable> data;

    private static LayoutInflater inflater = null;

    private int type;

    public StudentsCurrentStopAdapter(Context context,
                                      ArrayList<Trackable> data, int type) {
        // TODO Auto-generated constructor stub
        this.context = context;
        this.data = data;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.type = type;
    }

    public void setData(ArrayList<Trackable> data) {
        this.data = data;
    }

    public ArrayList<Trackable> getData() {
        return data;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return data.get(position).getTrackedStopTrackableId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //Para textos en ecuatoriano

        Organization organization = OnTrackManager.getInstance().getUser().getOrganizationByID(OnTrackManager.getInstance().getRoutes().get(OnTrackManager.getInstance().getSelectedRoute()).getOrganizationId());
        boolean ecuatorian = organization.getCreatedBy().equals("superadmin@ontrackschool.ec");

        // TODO Auto-generated method stub
        View vi = convertView;
        ViewHolder holder;
        Typeface tf = Typeface.createFromAsset(context.getAssets(),
                Parameters.LATO_FONT);
        if (vi == null) {
            vi = inflater.inflate(R.layout.student_list_bigcell, null);
            // ImageView ivStudent= (ImageView) vi.findViewById(R.id.ivStudent);

            holder = new ViewHolder();
            holder.tvStudentName = (TextView) vi.findViewById(R.id.tvStudentName);

//		if (this.data.size() > 6)
//			tvStudentName.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
//		else
//			tvStudentName.setTextSize(TypedValue.COMPLEX_UNIT_SP, 24);

            holder.rivStudentPicture = (RoundedImageView) vi
                    .findViewById(R.id.rivStudent);
            holder.rlStudentBigCell = (RelativeLayout) vi
                    .findViewById(R.id.rlStudentBigCell);
            holder.rlStudentState = (RelativeLayout) vi
                    .findViewById(R.id.rlStudentState);
            holder.ivStudentState = (ImageView) vi
                    .findViewById(R.id.ivStudentState);

            holder.tvStudentStopNumber = (TextView) vi
                    .findViewById(R.id.tvStudentStopNumber);

            vi.setTag(holder);
        } else {
            holder = (ViewHolder) vi.getTag();
        }
        holder.tvStudentName.setTypeface(tf);
        holder.tvStudentStopNumber.setTypeface(tf);
        if (this.type == ADAPTER_TYPE_STOP_HEADERS) {
            holder.tvStudentStopNumber.setVisibility(View.VISIBLE);
            int numberStop = OnTrackManager.getInstance().getUser().getRoute()
                    .getNumberOfStopOfTrackable(data.get(position).getId());
            if (numberStop != -1) {
                String stopText = context.getResources().getString(ecuatorian ? R.string.stop_singular_caps_ecuador : R.string.stop_singular_caps);
                stopText += " " + numberStop;
                holder.tvStudentStopNumber.setText(stopText);
            } else {
                String stopText = context.getResources().getString(ecuatorian ? R.string.stop_singular_caps_ecuador : R.string.stop_singular_caps);
                stopText += " extra";
                holder.tvStudentStopNumber.setText(stopText);
            }
        } else {
            holder.tvStudentStopNumber.setVisibility(View.GONE);
        }

        if (!data.get(position).isRelevant()) {
            holder.rlStudentState.setVisibility(View.VISIBLE);
            holder.rlStudentBigCell.setBackgroundColor(0xff555555);

            if (data.get(position).getPicked() == Trackable.NOT_ASSISTING_DELIVERED
                    || data.get(position).getPicked() == Trackable.NOT_AT_SCHOOL) {
                holder.ivStudentState.setImageResource(R.drawable.not_assisting);
            } else {
                Novelty novelty = data.get(position).getNovelty();
                if (novelty != null) {
                    if (novelty.isNotAttending()) {
                        holder.ivStudentState
                                .setImageResource(R.drawable.not_assisting);
                    } else if (novelty.isInCar()) {
                        holder.ivStudentState
                                .setImageResource(R.drawable.car);
                    } else if (novelty.isOutOfCurrentRoute()) {
                        holder.ivStudentState.setImageResource(R.drawable.out_route);
                    }
                }
            }
        } else {
            Novelty novelty = data.get(position).getNovelty();
            if (data.get(position).getPicked() == Trackable.ASSISTING_DELIVERED) {
                holder.ivStudentState.setImageResource(R.drawable.attending);
                holder.rlStudentState.setVisibility(View.VISIBLE);
            } else {
                if (novelty != null) {
                    if (novelty.isIntoCurrentRoute()) {
                        holder.ivStudentState.setImageResource(R.drawable.in_route);
                        holder.rlStudentState.setVisibility(View.VISIBLE);
                        holder.rlStudentBigCell.setBackgroundColor(0xff555555);
                    } else if (novelty.isChangingStop()) {
                        holder.ivStudentState.setImageResource(R.drawable.change_stop);
                        holder.rlStudentState.setVisibility(View.VISIBLE);
                        holder.rlStudentBigCell.setBackgroundColor(0xff555555);
                    }
                } else if (data.get(position).getPicked() == Trackable.UNKNOWN) {
                    holder.rlStudentState.setVisibility(View.INVISIBLE);
                    holder.rlStudentBigCell.setBackgroundColor(0x00ffffff);
                }
            }
        }

        holder.tvStudentName.setText(data.get(position).getName() + "\n"
                + data.get(position).getSubName());
        ImageLoader.getInstance().displayImage(data.get(position).getImageURL(), holder.rivStudentPicture);

        return vi;
    }

    // Patrón holder:
    static class ViewHolder {
        TextView tvStudentName;
        RoundedImageView rivStudentPicture;
        RelativeLayout rlStudentBigCell;
        RelativeLayout rlStudentState;
        ImageView ivStudentState;
        TextView tvStudentStopNumber;
    }
}
