package otr.ontrack_onroad.adapters;

import java.util.ArrayList;

import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.models.Organization;
import otr.ontrack_onroad.models.Stop;
import otr.ontrack_onroad.parameters.Parameters;
import otr.ontrack_onroad_debug.activities.R;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class UpdateStopLocationAdapter extends BaseAdapter
{
    private Context context;
    
    private ArrayList<Stop> stops;
    
    public UpdateStopLocationAdapter( Context context )
    {
        this.context = context;
        stops = OnTrackManager.getInstance( ).getRoutes( ).get( OnTrackManager.getInstance( ).getSelectedRoute( ) ).getStops( );
    }
    
    @Override
    public int getCount( )
    {
        return stops.size( );
    }

    @Override
    public Object getItem( int position )
    {
        return stops.get( position );
    }

    @Override
    public long getItemId( int position )
    {
        return position;
    }

    @Override
    public View getView( int position, View convertView, ViewGroup parent )
    {
        //Para textos en ecuatoriano
        Organization organization = OnTrackManager.getInstance().getUser().getOrganizationByID(OnTrackManager.getInstance().getRoutes().get(OnTrackManager.getInstance().getSelectedRoute()).getOrganizationId());
        final boolean ecuatorian = organization.getCreatedBy().equals("superadmin@ontrackschool.ec");

        LayoutInflater inflater = ( LayoutInflater )context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        Typeface tf = Typeface.createFromAsset( context.getAssets( ), Parameters.LATO_FONT );
        View view = null;

        if( convertView == null )
        {
            view = new View( context );
            view = inflater.inflate( R.layout.add_stop_before_row, null );
        }
        else
        {
            view = ( View )convertView;
        }
        
        Stop stop = stops.get( position );
        
        RelativeLayout rlRow = ( RelativeLayout )view.findViewById( R.id.add_stop_before_layout );
        TextView tvStopNumber = ( TextView )view.findViewById( R.id.add_stop_before_stop_number );
        
        if( stop.isSpecial( ) )
        {
            rlRow.setBackgroundColor( context.getResources( ).getColor( R.color.create_route_orange_button ) );
        }
        else
        {
            rlRow.setBackgroundColor( context.getResources( ).getColor( R.color.general_ontrack_blue ) );
        }
        
        tvStopNumber.setText( context.getResources( ).getString(ecuatorian? R.string.stop_singular_caps_ecuador:R.string.stop_singular_caps ) + " " + ( stop.getOrder( ) ) );
        tvStopNumber.setTypeface( tf );
        
        return view;
    }
}
