package otr.ontrack_onroad.adapters;

import java.util.ArrayList;

import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.models.Trackable;
import otr.ontrack_onroad.parameters.Parameters;
import otr.ontrack_onroad_debug.activities.R;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class SelectTrackablesForStopUpdateAdapter extends BaseAdapter
{
    private Context context;
    
    private ArrayList<Trackable> trackables;
    
    public SelectTrackablesForStopUpdateAdapter( Context context )
    {
        this.context = context;
        trackables = OnTrackManager.getInstance( ).getRoutes( ).get( OnTrackManager.getInstance( ).getSelectedRoute( ) ).getTrackables( );
    }
    
    @Override
    public int getCount( )
    {
        return trackables.size( );
    }

    @Override
    public Object getItem( int position )
    {
        return trackables.get( position );
    }

    @Override
    public long getItemId( int position )
    {
        return position;
    }

    @Override
    public View getView( int position, View convertView, ViewGroup parent )
    {
        LayoutInflater inflater = ( LayoutInflater )context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        Typeface tf = Typeface.createFromAsset( context.getAssets( ), Parameters.LATO_FONT );
        View view = null;

        if( convertView == null )
        {
            view = inflater.inflate( R.layout.add_stop_before_row, null );
        }
        else
        {
            view = ( View )convertView;
        }
        
        Trackable trackable = trackables.get( position );
        
        RelativeLayout rlRow = ( RelativeLayout )view.findViewById( R.id.add_stop_before_layout );
        TextView tvStopNumber = ( TextView )view.findViewById( R.id.add_stop_before_stop_number );
        
        tvStopNumber.setText( trackable.getFullname( ) );
        tvStopNumber.setTypeface( tf );
        
        if( trackable.isSelected( ) )
        {
            rlRow.setBackgroundColor( context.getResources( ).getColor( R.color.general_ontrack_blue ) );
        }
        else
        {
            rlRow.setBackgroundColor( context.getResources( ).getColor( R.color.general_ontrack_transparent ) );
        }
        
        return view;
    }
}
