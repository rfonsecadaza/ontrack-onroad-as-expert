package otr.ontrack_onroad.adapters;

import java.util.List;

import otr.ontrack_onroad.dao.ChatCard;
import otr.ontrack_onroad.dao.Message;
import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.utils.RoundedImageView;
import otr.ontrack_onroad_debug.activities.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ChatListAdapter extends BaseAdapter {
	private Context context;

	private List<ChatCard> data;
	private static LayoutInflater inflater = null;

	public ChatListAdapter(Context context, List<ChatCard> data) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.data = data;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return data.get(position).getServerId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View vi = convertView;
		if (vi == null)
			vi = inflater.inflate(R.layout.chatcard_row, null);
		ChatCard chatCard = data.get(position);
		RoundedImageView rivChatcardImage = (RoundedImageView) vi
				.findViewById(R.id.rivChatCardImage);

		// Chambonada, por ahora agrego la imagen del primer usuario del grupo
		// de chatcards
//		if (chatCard.getMessages().size() > 0)
//			rivChatcardImage.setImageBitmap(chatCard.getMessages().get(0)
//					.getChatUser().getImage());
		
		rivChatcardImage.setImageBitmap(chatCard.getImage());
		
		TextView tvChatCardName = (TextView) vi
				.findViewById(R.id.tvChatCardName);
		TextView tvChatCardType = (TextView) vi
				.findViewById(R.id.tvChatCardType);
		String name = "";
		String typeText = "";

		name = chatCard.getName();

		tvChatCardName.setText(name);
		tvChatCardType.setText(typeText);

		List<Message> unreadMessages = OnTrackManager.getInstance()
				.getAllUnreadMessagesInChatCard(chatCard.getId());
		int newMessages = unreadMessages == null ? 0 : unreadMessages.size();

		RelativeLayout rlNewMessages = (RelativeLayout) vi
				.findViewById(R.id.rlNewMessages);
		TextView tvNewMessages = (TextView) vi.findViewById(R.id.tvNewMessages);
		if (newMessages == 0) {
			rlNewMessages.setVisibility(View.INVISIBLE);
		} else {
			rlNewMessages.setVisibility(View.VISIBLE);
			tvNewMessages.setText("" + newMessages);
		}

		return vi;
	}
}
