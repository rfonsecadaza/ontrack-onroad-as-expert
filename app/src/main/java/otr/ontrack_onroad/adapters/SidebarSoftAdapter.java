package otr.ontrack_onroad.adapters;

import java.util.List;

import otr.ontrack_onroad.dao.Message;
import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.parameters.Parameters;
import otr.ontrack_onroad_debug.activities.R;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class SidebarSoftAdapter extends BaseAdapter {
	private Context context;

    private String[] textData;

    private static LayoutInflater inflater = null;

    public SidebarSoftAdapter( Context context, String[] textData )
    {
        // TODO Auto-generated constructor stub
        this.context = context;
        this.textData = textData;

        inflater = ( LayoutInflater )context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
    }

    @Override
    public int getCount( )
    {
        // TODO Auto-generated method stub
        return textData.length;
    }

    @Override
    public Object getItem( int position )
    {
        // TODO Auto-generated method stub
        return textData[ position ];
    }

    @Override
    public long getItemId( int position )
    {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView( int position, View convertView, ViewGroup parent )
    {
        // TODO Auto-generated method stub
        View vi = convertView;
        
        Typeface tf = Typeface.createFromAsset( context.getAssets( ), Parameters.LATO_FONT );

        if( position == 0 )
        {
            if( vi == null )
            {
                vi = inflater.inflate( R.layout.sidebar_header, null );
            }
            
            int selectedRoute = OnTrackManager.getInstance( ).getSelectedRoute( );
            
            TextView tvRouteName = ( TextView )vi.findViewById( R.id.sidebar_header_route_name );
            TextView tvRouteStatus = ( TextView )vi.findViewById( R.id.sidebar_header_route_status );
            
            tvRouteName.setTypeface( tf );
            tvRouteName.setText( OnTrackManager.getInstance( ).getUser().getFirstName() );
            tvRouteStatus.setTypeface( tf );
            tvRouteStatus.setText( OnTrackManager.getInstance( ).getUser().getLastName() );
        }
        else if( position == textData.length - 1 )
        {
            if( vi == null )
            {
                vi = inflater.inflate( R.layout.sidebar_footer, null );
            }
        }
        else
        {
            if( vi == null )
            {
                vi = inflater.inflate( R.layout.drawer_list_item, null );
            }
            
            ImageView ivIcon = ( ImageView )vi.findViewById( R.id.drawer_list_item_icon );
            
            TextView tvOption = ( TextView )vi.findViewById( R.id.tvOption );
            tvOption.setTypeface( tf );
            // TextView tvVehicle = (TextView) vi.findViewById(R.id.tvVehicle);

            tvOption.setText( textData[ position ] );
            // tvVehicle.setText(data.get(position).getVehicle().getId());

            RelativeLayout rlNewMessages = ( RelativeLayout )vi.findViewById( R.id.rlNewMessages );
            TextView tvNewMessages = ( TextView )vi.findViewById( R.id.tvNewMessages );
            tvNewMessages.setTypeface( tf );
            if( position == Parameters.OPTION_CHAT )
            {
                ivIcon.setImageDrawable( context.getResources( ).getDrawable( R.drawable.mensaje ) );
            }

            else if( position == Parameters.OPTION_CLOSE_SESSION )
            {
                ivIcon.setImageDrawable( context.getResources( ).getDrawable( R.drawable.cerrar_sesion_50px ) );
            }
            
            if( position == Parameters.OPTION_CHAT )
            {
                List<Message> unreadMessages = OnTrackManager.getInstance( ).getAllUnreadMessages( );
                int newMessages = unreadMessages == null ? 0 : unreadMessages.size( );
                if( newMessages == 0 )
                {
                    rlNewMessages.setVisibility( View.INVISIBLE );
                }
                else
                {
                    rlNewMessages.setVisibility( View.VISIBLE );
                    tvNewMessages.setText( "" + newMessages );
                }
            }
            else
            {
                rlNewMessages.setVisibility( View.INVISIBLE );
            }
        }
        return vi;
    }
}
