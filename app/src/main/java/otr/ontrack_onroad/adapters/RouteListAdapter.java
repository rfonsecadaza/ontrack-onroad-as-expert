package otr.ontrack_onroad.adapters;

import java.util.ArrayList;

import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad.models.Organization;
import otr.ontrack_onroad.models.Route;
import otr.ontrack_onroad.models.Schedule;
import otr.ontrack_onroad.parameters.Parameters;
import otr.ontrack_onroad_debug.activities.R;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class RouteListAdapter extends BaseAdapter
{
    private Context context;

    private ArrayList<Route> data;
    private static LayoutInflater inflater = null;

    public RouteListAdapter( Context context, ArrayList<Route> data )
    {
        this.context = context;
        this.data = data;
        inflater = ( LayoutInflater )context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
    }

    @Override
    public int getCount( )
    {
        return data.size( );
    }

    @Override
    public Object getItem( int position )
    {
        return data.get( position );
    }

    @Override
    public long getItemId( int position )
    {
        return position;
    }

    public String getItemIdAsString( int position )
    {
        return ( ( Route )getItem( position ) ).getId( );
    }

    @Override
    public View getView( int position, View convertView, ViewGroup parent )
    {
        ViewHolder holder;

        if( convertView == null )
        {
            Typeface tf = Typeface.createFromAsset( context.getAssets( ), Parameters.LATO_FONT );

            holder = new ViewHolder( );

            convertView = inflater.inflate( R.layout.route_list_row, null );

            holder.llDays = ( LinearLayout )convertView.findViewById( R.id.llDays );
            holder.rlColor = ( RelativeLayout )convertView.findViewById( R.id.rlColor );
            holder.tvSchedule = ( TextView )convertView.findViewById( R.id.tvHours );
            holder.tvRouteName = ( TextView )convertView.findViewById( R.id.tvRouteNameProfile );
            holder.tvStatus = ( TextView )convertView.findViewById( R.id.tvStatus );
            holder.tvMonday = ( TextView )convertView.findViewById( R.id.tvMonday );
            holder.tvTuesday = ( TextView )convertView.findViewById( R.id.tvTuesday );
            holder.tvWednesday = ( TextView )convertView.findViewById( R.id.tvWednesday );
            holder.tvThursday = ( TextView )convertView.findViewById( R.id.tvThursday );
            holder.tvFriday = ( TextView )convertView.findViewById( R.id.tvFriday );
            holder.tvSaturday = ( TextView )convertView.findViewById( R.id.tvSaturday );
            holder.tvSunday = ( TextView )convertView.findViewById( R.id.tvSunday );
            holder.tvOrganization = ( TextView )convertView.findViewById( R.id.tvOrganization );

            holder.tvSchedule.setTypeface( tf );
            holder.tvRouteName.setTypeface( tf );
            holder.tvStatus.setTypeface( tf );
            holder.tvMonday.setTypeface( tf );
            holder.tvTuesday.setTypeface( tf );
            holder.tvWednesday.setTypeface( tf );
            holder.tvThursday.setTypeface( tf );
            holder.tvFriday.setTypeface( tf );
            holder.tvSaturday.setTypeface( tf );
            holder.tvSunday.setTypeface( tf );
            holder.tvOrganization.setTypeface( tf );

            convertView.setTag( holder );
        }
        else
        {
            holder = ( ViewHolder )convertView.getTag( );
        }

        if( data.get( position ).shouldBeRunning( ) )
        {
            holder.rlColor.setBackgroundColor( context.getResources( ).getColor( R.color.general_ontrack_blue ) );
        }
        else
        {
            holder.rlColor.setBackgroundColor( context.getResources( ).getColor( R.color.general_ontrack_dark_gray ) );
        }

        Schedule schedule = data.get( position ).getSchedule( );

        if( schedule == null )
        {
            holder.llDays.setVisibility( View.INVISIBLE );
        }
        else
        {
            holder.llDays.setVisibility( View.VISIBLE );
        }

        if( OnTrackManager.getInstance( ).getSelectedRoute( ) == position )
        {
            // tvDays.setText("EN PROGRESO");
        }
        else
        {
            if( schedule != null )
            {
                // tvDays.setText(schedule.getDaysString());
                holder.tvSchedule.setText( schedule.getHoursString( ) );
            }
            else
            {
                holder.tvSchedule.setText( "" );
                // tvDays.setText("SIN HORARIO ASIGNADO");
            }
        }

        if( schedule != null )
        {
            if( !schedule.isMonday( ) )
            {
                holder.tvMonday.setTextColor( Color.GRAY );
            }
            else
            {
                holder.tvMonday.setTextColor( context.getResources( ).getColor( R.color.solid_blue ) );
            }
            if( !schedule.isTuesday( ) )
            {
                holder.tvTuesday.setTextColor( Color.GRAY );
            }
            else
            {
                holder.tvTuesday.setTextColor( context.getResources( ).getColor( R.color.solid_blue ) );
            }
            if( !schedule.isWednesday( ) )
            {
                holder.tvWednesday.setTextColor( Color.GRAY );
            }
            else
            {
                holder.tvWednesday.setTextColor( context.getResources( ).getColor( R.color.solid_blue ) );
            }
            if( !schedule.isThursday( ) )
            {
                holder.tvThursday.setTextColor( Color.GRAY );
            }
            else
            {
                holder.tvThursday.setTextColor( context.getResources( ).getColor( R.color.solid_blue ) );
            }
            if( !schedule.isFriday( ) )
            {
                holder.tvFriday.setTextColor( Color.GRAY );
            }
            else
            {
                holder.tvFriday.setTextColor( context.getResources( ).getColor( R.color.solid_blue ) );
            }
            if( !schedule.isSaturday( ) )
            {
                holder.tvSaturday.setTextColor( Color.GRAY );
            }
            else
            {
                holder.tvSaturday.setTextColor( context.getResources( ).getColor( R.color.solid_blue ) );
            }
            if( !schedule.isSunday( ) )
            {
                holder.tvSunday.setTextColor( Color.GRAY );
            }
            else
            {
                holder.tvSunday.setTextColor( context.getResources( ).getColor( R.color.solid_blue ) );
            }
        }

        holder.tvRouteName.setText( data.get( position ).getIdWithoutSuffix( ) );
        if( data.get( position ).getType( ) == 0 )
        {
            holder.tvStatus.setText( context.getString( R.string.create_route_route_type_to_school ) );
        }
        else
        {
            holder.tvStatus.setText( context.getString( R.string.create_route_route_type_from_school ) );
        }
        
        for( Organization organization : OnTrackManager.getInstance( ).getUser( ).getOrganizations( ) )
        {
            if( organization.getId( ).equals( data.get( position ).getId( ).split( "-" )[ 1 ] ) )
            {
                holder.tvOrganization.setText( organization.getTitle( ) );
                break;
            }
        }

        return convertView;
    }

    private class ViewHolder
    {
        public LinearLayout llDays;
        public RelativeLayout rlColor;
        public TextView tvSchedule;
        public TextView tvRouteName;
        public TextView tvStatus;
        public TextView tvMonday;
        public TextView tvTuesday;
        public TextView tvWednesday;
        public TextView tvThursday;
        public TextView tvFriday;
        public TextView tvSaturday;
        public TextView tvSunday;
        public TextView tvOrganization;
    }

    public void setData(ArrayList<Route> data){
        this.data = data;
    }
}
