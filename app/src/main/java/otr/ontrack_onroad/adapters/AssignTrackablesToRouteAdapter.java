package otr.ontrack_onroad.adapters;

import java.util.ArrayList;

import otr.ontrack_onroad.models.Trackable;
import otr.ontrack_onroad.parameters.Parameters;
import otr.ontrack_onroad.utils.RoundedImageView;
import otr.ontrack_onroad_debug.activities.R;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

public class AssignTrackablesToRouteAdapter extends BaseAdapter
{
    private Context context;

    private ArrayList<Trackable> routeTrackables;

    public AssignTrackablesToRouteAdapter( Context context, ArrayList<Trackable> routeTrackables )
    {
        this.context = context;
        this.routeTrackables = routeTrackables;
    }

    public View getView( int position, View convertView, ViewGroup parent )
    {
        LayoutInflater inflater = ( LayoutInflater )context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        Typeface tf = Typeface.createFromAsset( context.getAssets( ), Parameters.LATO_FONT );
        View gridView = null;

        Trackable trackable = ( Trackable )routeTrackables.get( position );

        if( convertView == null )
        {
            gridView = new View( context );
            gridView = inflater.inflate( R.layout.element_organization_trackable_card, null );
        }
        else
        {
            gridView = ( View )convertView;
        }

        RoundedImageView rivImage = ( RoundedImageView )gridView.findViewById( R.id.element_organization_trackable_card_image );
        TextView tvName = ( TextView )gridView.findViewById( R.id.element_organization_trackable_card_name );
        TextView tvID = ( TextView )gridView.findViewById( R.id.element_organization_trackable_card_id );

        tvName.setTypeface( tf );
        tvID.setTypeface( tf );

        if( trackable.getImageURL( ) != null )
        {
            ImageLoader.getInstance( ).displayImage( trackable.getImageURL( ), rivImage );
        }
        else
        {
            rivImage.setImageDrawable( context.getResources( ).getDrawable( R.drawable.pic_user ) );
        }

        tvName.setText( trackable.getSubName( ) + " " + trackable.getName( ) );

        tvID.setText( context.getResources( ).getString( R.string.create_route_elements_trackable_id ) + trackable.getId( ).split( "-" )[ 0 ] );

        RelativeLayout relativeLayout = ( RelativeLayout )gridView.findViewById( R.id.element_organization_trackable_card_layout );

        if( trackable.isInRoute( ) )
        {
            relativeLayout.setBackgroundColor( context.getResources( ).getColor( R.color.general_ontrack_blue ) );
        }
        else
        {
            relativeLayout.setBackgroundColor( context.getResources( ).getColor( R.color.general_ontrack_white ) );
        }

        return gridView;
    }

    @Override
    public int getCount( )
    {
        return routeTrackables.size( );
    }

    @Override
    public Object getItem( int position )
    {
        return routeTrackables.get( position );
    }

    @Override
    public long getItemId( int position )
    {
        return position;
    }
}
