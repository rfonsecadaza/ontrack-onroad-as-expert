package otr.ontrack_onroad.adapters;

import java.util.ArrayList;

import otr.ontrack_onroad.models.Trackable;
import otr.ontrack_onroad.utils.RoundedImageView;
import otr.ontrack_onroad_debug.activities.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created with IntelliJ IDEA.
 * User: usuario
 */
public class StudentListAdapter extends BaseAdapter
{

    private Context context;

    private ArrayList<Trackable> data;
    private static LayoutInflater inflater = null;

    public StudentListAdapter(Context context, ArrayList<Trackable> data)
    {
        // TODO Auto-generated constructor stub
        this.context = context;
        this.data = data;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    
    public void setData(ArrayList<Trackable> data){
    	this.data = data;
    }

    @Override
    public int getCount()
    {
        // TODO Auto-generated method stub
        return data.size();
    }

    @Override
    public Object getItem(int position)
    {
        // TODO Auto-generated method stub
        return data.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        // TODO Auto-generated method stub
        return data.get(position).getTrackedStopTrackableId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        // TODO Auto-generated method stub
        View vi = convertView;
        if (vi == null)
            vi = inflater.inflate(R.layout.student_list_cell, null);
//        ImageView ivStudent= (ImageView) vi.findViewById(R.id.ivStudent);
        TextView tvStudentName = (TextView) vi.findViewById(R.id.tvStudentName);
        RoundedImageView rivStudentPicture = (RoundedImageView)vi.findViewById(R.id.rivStudent);
        View vStudentMask = vi.findViewById(R.id.rlStudentState);
        if(!data.get(position).isRelevant()){
        	vStudentMask.setVisibility(View.VISIBLE);
        }else{
        	vStudentMask.setVisibility(View.INVISIBLE);
        }
        

        tvStudentName.setText(data.get(position).getName()+"\n"+data.get(position).getSubName());
        ImageLoader.getInstance( ).displayImage( data.get( position ).getImageURL( ), rivStudentPicture );
        return vi;
    }
}
