package otr.ontrack_onroad.adapters;

import java.util.List;

import otr.ontrack_onroad.dao.Message;
import otr.ontrack_onroad.managers.OnTrackManager;
import otr.ontrack_onroad_debug.activities.R;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MessageAdapter extends BaseAdapter {
	private Context context;

	private List<Message> data;
	private static LayoutInflater inflater = null;

	public MessageAdapter(Context context, List<Message> data) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.data = data;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;
		Message message = data.get(position);

		boolean isMine = message.getChatUser().getUserId().equals(
				OnTrackManager.getInstance().getUser().getId());

		if (convertView == null) {
			holder = new ViewHolder();
			convertView = LayoutInflater.from(this.context).inflate(R.layout.message_row, parent, false);
			holder.messageContainer = (LinearLayout) convertView
					.findViewById(R.id.llMessageContainer);
			holder.tvSender = (TextView) convertView.findViewById(R.id.tvSender);
			holder.tvMessage = (TextView) convertView
					.findViewById(R.id.tvMessage);
			holder.tvMessageDate = (TextView) convertView
					.findViewById(R.id.tvMessageDate);
			holder.pbSendingMessage = (ProgressBar) convertView
					.findViewById(R.id.pbSendingMessage);
			holder.ivMessageSent = (ImageView) convertView
					.findViewById(R.id.ivMessageSent);
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		
		holder.tvMessage.setText(message.getMessageText());
		holder.tvMessageDate.setText(message.getTime());
		
//		LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) holder.tvMessage
//				.getLayoutParams();

		if (isMine) {
			
//			holder.messageContainer.setBackgroundColor(0xff46b1e9);
//			holder.tvSender.setTextColor(0xffffffff);
			holder.tvSender.setText("Yo");
			holder.tvSender.setGravity(Gravity.RIGHT);
//			holder.tvMessage.setTextColor(0xffffffff);
			holder.tvMessage.setGravity(Gravity.RIGHT);
//			holder.tvMessageDate.setTextColor(0xffffffff);
			holder.tvMessageDate.setGravity(Gravity.RIGHT);
//			params.gravity = Gravity.RIGHT;
			
			if (message.getSent()) {
				holder.pbSendingMessage.setVisibility(View.GONE);
				holder.ivMessageSent.setImageResource(R.drawable.send);
			} else {
				holder.pbSendingMessage.setVisibility(View.VISIBLE);
				holder.ivMessageSent.setImageDrawable(null);
			}

		} else {
			holder.tvSender.setText(message.getChatUser().getFullName());
			holder.messageContainer.setBackgroundColor(0xffffffff);
			holder.pbSendingMessage.setVisibility(View.GONE);
			holder.ivMessageSent.setImageDrawable(null);
//			params.gravity = Gravity.LEFT;

//			holder.tvSender.setTextColor(0xff000000);
			holder.tvSender.setGravity(Gravity.LEFT);
//			holder.tvMessage.setTextColor(0xff000000);
			holder.tvMessage.setGravity(Gravity.LEFT);
//			holder.tvMessageDate.setTextColor(0xff000000);
			holder.tvMessageDate.setGravity(Gravity.LEFT);
		}
//		holder.tvSender.setLayoutParams(params);
//		holder.tvMessage.setLayoutParams(params);
		return convertView;
	}

	private static class ViewHolder {
		LinearLayout messageContainer;
		TextView tvMessage;
		TextView tvSender;
		TextView tvMessageDate;
		ProgressBar pbSendingMessage;
		ImageView ivMessageSent;

	}
}
