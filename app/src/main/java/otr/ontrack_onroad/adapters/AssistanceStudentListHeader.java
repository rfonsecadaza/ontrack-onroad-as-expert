package otr.ontrack_onroad.adapters;

public class AssistanceStudentListHeader implements AssistanceStudentListItem{

	private String header;
	
	public AssistanceStudentListHeader(String header){
		this.header = header;
	}
	
	
	
	@Override
	public boolean isSection() {
		return true;
	}



	public String getHeader() {
		return header;
	}



	public void setHeader(String header) {
		this.header = header;
	}

}
